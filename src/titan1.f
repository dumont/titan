       program titan106                                  ! 6 juin 2019
c v106d
c      BSD 3-Clause License
c      Copyright (c) 2018, Dumont Anne-marie
c      All rights reserved.
c
c      Redistribution and use in source and binary forms, with or without
c    modification, are permitted provided that the following conditions are met:
c
c     * Redistributions of source code must retain the above copyright notice,
c    this list of conditions and the following disclaimer.
c
c     * Redistributions in binary form must reproduce the above copyright
c    notice, this list of conditions and the following disclaimer in the
c    documentation and/or other materials provided with the distribution.
c
c     * Neither the name of the copyright holder nor the names of its
c    contributors may be used to endorse or promote products derived from this
c    software without specific prior written permission.
c
c     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
c    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
c    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
c    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
c    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
c    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
c    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
c    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
c    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
c    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
c    IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
c     
! help in english
! CALCULATION of IONIZATION and FLUX in a HIGH TEMPERATURE PHOTOIONIZED MEDIUM
! ALI TRANSFER for CONTINUUM and LINES
! several EXCITED LEVELS by ion with interlocking + resonant lines & UTA
! continuum and line comptonisation
! calculation of flux, temperature and ionisation degrees by iterations
!
! 2 modes: "given density" mode (GD), and "constant pressure" mode (CP)
!
! j is the number of element X (H,He,C,N,O,Ne,Mg,Si,S,Fe)
! i is ionisation state (1 for X0, 2:X+, 3:X++, Z:hydrogenoid, Z+1:naked ion)
! ionisation degree  r(i,j)
! n is the number of level - population of one ion den(nij)=ab(j)*r(ij)*po(n)
! nij for i,j,n
! kc is the number of continuum radiation frequencies,kr for lines,m for layers
! - the total number of level nivtots, the total number of lines nrais, the 
! total number of lines nrais, the total number of layers nzs, are given in 
! the commands "parameter"    
!      
!     input files: 
!     - a easy input file for "given density" mode (with keywords)
!              to run with ./executable <your_input_file >log_file
!     - OR TICPinput.dat for "constant pressure" mode (and "given density"
!              mode also) to run with ./executable >log_file
!              which provides the name xx for output files among other data
!     + according options: fli.xxx,mzt.xxx,iml.xxx,mzn.xxx
!              xxx=titan_model for GD mode, read for CP in TICPinput.dat
!     + atomic.dat (version 20)
!
! OPTIONS (print options are not all available for simple input with keywords)
!     inr=1 : 931 lines + 92 lines UTA - inr=2 : + 3118 lines from Cloudy
! INCIDENT RADIATION :
! incid=1 : incident spectrum power law
! incid=2 : incident spectrum black body multiplied by "wbb"
! incid=3 : incident spectrum from Laor et al. 1997, ApJ 477, 93
! incid=4 : incident spectrum "AGN" from Cloudy, Ferland al.1998,PASP,110,761
! incid=5 : incident spectrum from Netzer 1996, ApJ 473,781
! incid=6 : incident spectrum from Krolik & Kriss 1995, ApJ 447, 512
! incid=7 : incident spectrum NGC5548 after Collin et al. 1998, A&A, 331, 11
! incid=8 : incident spectrum NGC3783 from Kaspi et al. 2001, ApJ, 554, 226)
! incid=9 : incident spectrum in Lexington 2000 (X-ray thin, from Netzer)
! incid=10: incident spectrum given by points *A computed from csit
! incid=15: spectrum shape given in fli.xx: title/frequency number/hnuev F(nu)
! incid=16: spectrum shape given in fli.xx: title/frequ. number/hnuev nuF(nu)
!            (15-16: check the output file .atom)
! incid=17: incident spectrum power-law with exp cut-off (Rozanska)
! incid=18: double power-law with exp cut-off (Rozanska) hnumin1 <=hnumin2
!    ndir= 1 or 3 or 5 or 20 for a semi-isotropic illumination
!    ndirlu=103 (1+3=4 directions) ou 105 (1+5=6 dir.) for a normal incident
!       (directions are chosen for a best integration over angles,
!        except pour isotropical 20 directions and ndirlu=205 (6 dir.) )
!      incang=0 : semi-isotropic illumination
!      1<incang<ndir : illumination in a cone (hollow) around the angle i
!          (normal incident: incang=1)
!    irent=0 : no backward flux
!    irent=1 : backward flux: black body(T=Tbb)*wbb
!    irent=2 : backward flux: I-(H)= I+(H) + wbb*BB(Tbb)
!    irent=3 : backward flux: I-(H)= wbb*BB(Tbb) - I+(H)          Suzy
!    irent=4 : backward flux: I-(H)= I+(H) + wbb*BB(Tbb)*f(nu,T)  Bozena
!    irent=5 : backward flux = (Wien law T=Tbb)*wbb
!    irent=6 : backward flux = (incident flux)*wbb
!       irent>0 if "Teff" not zero: I-(first it)=I-(0)=(I+(0) + BB(t=Teff))*fii
! MODEL
! idens=0 : CONSTANT density in the entire cloud
! idens=1 : assigned density, power law in the cloud
! idens=5 : CONSTANT GAS PRESSURE in the cloud   - with iteration on density
! idens=8 : CONSTANT TOTAL PRESSURE n computed at the beginning (with T&J of 
!                 the previous flux iteration) searching T as heating-cooling=0
! idens=9 : CST TOTAL+TURBULENCE PRESS. with T&J of the previous flux iteration
! idens=10: CONSTANT TOTAL PRESSURE, COLD solution, nT computed (with T&J of
!   the previous flux iteration) searching T as heating-cooling=0, first T=7OOO
! idens=11: CONSTANT TOTAL PRESSURE, HOT solution, nT computed (with T&J of
!   the previous flux iteration) searching T as heating-cooling=0, first T=3e7
! idens=12: CONSTANT TOTAL PRESSURE, instable solution
!
!      itemp=0 : thermal equilibrium searched
!      itemp=1 : beyond z=0.98*H temperature = constante
!      itemp=10: NO thermal equilibrium : constant assigned temperature=Tinit
!      itemp=11: NO thermal equilibrium : read temperature (igrid=13-14)
!
! VARIOUS PHYSICAL OPTIONS
! plan-parallel slab
!   ietl=0 : far from LTE : all processes
!   ietl=1 : near LTE (no dielectronic recombinations...)
!        iprof=0 : Voigt profile
!        iprof=1 : Doppler profile (am=0) for some lines Hydrog.& Heliumoid.
!        iprof=2 : Doppler profile (damping cst=am=0)
!
! COLUMN-GRID :
! main datas : igrid,nstr,ichg
! add a data line at the end of input file if igrid=1 or 2, with 8 numbers :
!           ndcm,npd1,dcol1,divis,begincolfall,endcolfall,npdf,dcolf
!     ichg=2 : automatic change of the grid if necessary
!     ichg=0 : no change of the grid
!
! igrid=0 : grid in column-density: NSTR equal layers ONLY IF "coltot" < 1e22
! igrid=1 : grid in column-density
!           first decades: beginning NH=dcol1, npd1 layers by decade
!                       geometrical progression (layer m: dcol=dcol1*q1**(m-1))
!           last decade: dcol= coltot/ndcm
!                      if "divis" is non equal to 1, layers are divided
!                      by "divis" between "begincolfall" and "endcolfall"*
!           decreasing of last layers with npdf layers by decade till dcolf
! igrid=2 : grid in z
!           first decades: beginning dz=dcol1, npd1 layers by decade
!                       geometrical progression (layer m: dz=dcol1*q1**(m-1))
!           last decade: dcol= Htot/ndcm
!                      if "divis" is non equal to 1, layers are divided
!                      by "divis" between "colfall"/2 and "colfall"*2.5
!           decreasing of last layers with npdf layers by decade till dcolf
! for the following options the layer number is NSTR of dat.xx file
! igrid=10: NSTR layers: column-density is read in MZN file
!                  (first line=title, only 1 column read, with free format)
!                  ATTENTION to last layers: no equal column-densities
! igrid=11: NSTR layers:"density   column-density" are read in MZN file
!                  (first line=title, only 2 columns read, with free format)
!                  ATTENTION: no equal column-densities
! igrid=12: NSTR layers:"z density" are read in MZN file,ATTENTION: no equal z
!                  (first line=title, only 2 columns read, with free format)
! igrid=13: NO thermal equilibrium: m,z & temperature read (MZT.file)
!          (format of .res:'coldens' line,coldens nht T pbt m z,-1 at the end)
! igrid=14: NO thermal equilibrium : col density temperature nothing m read
!     (MZT.file,free format,2 lines not read,layer number=NSTR of dat.file)
!
! CALCULATION OPTIONS
!      inr=1 : 931 lines + 92 lines UTA - inr=2 : + 3118 lines from Cloudy
! iacc=0 : normal run
! iacc=1 : acceleration of convergence for ALI continuum
! iacc=2 : acceleration of convergence for lines with iterations *1,2,3,4
!      if nfmax<0 : the run do "nfmax" iterations
!      if nfmax>0 : iterations stop when temperature and line flux sum
!                   are stable
! output files:
!             ifich=0: files spe res te2 de2 log(for pb) an2(if ndir>1)
!                            + atom if iexpert=1
!             ifich=1: + files tem flu ang (intermediate iterations)
!                      + (if iexpert=1) bil txt pre (for constant pressure)
!             ifich=2: + file dio (for Noar)
!             ifich=3: + files fl2  tx2 (heatings and coolings)
!             ifich=4: + files txa  tr2  fort.85 (for fixing)
!             ifich=5: + files ali  fort  (transfer fixing)
!             ifich=6: + file pge (heatings and coolings at equilibrium)
!             ifich=7:+file pgt(cool.&heat.versus T,cst n,every nimpm&nimpf)
!             ifich=8:+file pgt(cool.&heat.versus T,cst(nT),every nimpm&nimpf)
!             ifich=9:ifich=0 + the file tr2 contains dtaur for all kr & m
!  (.te2, .fl2, .tr2, .tx2 et .de2, et .ime are the results of last iteration)
!   ifich=10: 0+ file cap: ionisation of iron & capabs (Zwicky)
!   ifich=11: 0+ file cap: abond.elem.& ions,capabs,dic&dir (Delphine)
!(results of the last iteration are in: res, te2, fl2, tr2, tx2, an2, de2, ime)
!      
c commentaires en francais
c CALCUL DE L'IONISATION ET DES FLUX DANS UN MILIEU HAUTE TEMPERATURE ECLAIRE
c TRANSFERT ALI avec subroutine Paletou pour le CONTINU et pour les RAIES
c plusieurs NIVEAUX EXCITES par ion avec interlocking + raies de resonance & UTA
c comptonisation des raies
c
c 2 modes: densit� imposee, constante ou loi de puissance, ou donnee dans
c                 un fichier (GD)
c          pression constante (CP)
c      
c fichiers de donnees:
c    - un fichier d'entree simple avec mots-cle
c                 (./executable <votre_fichier_entree >log)
c     - OU TITANinput.dat=ti9.dat qui donne le nom xx des fichiers en sortie
c                 (./executable >log_file)
ccc    - OU dat.web rempli a partir d'internet par Astrogrid
c    + selon option fli.xx,mzt.xx,iml.xx,mzn.xx
c    + fichier atomic.dat(version 20)
c fichiers resultats : 
c           ifich=0: fichiers spe res te2 de2 log(pour pb) an2(si ndir>1)
c                             + atom si expert
c           ifich=1: + fichiers tem flu ang (iterations intermediaires)
c                    + (si expert) bil txt pre       (pour pression cste)
c           ifich=2: + fichier dio (pour Noar)
c           ifich=3: + fichier fl2 tx2 (pertes et gains)
c           ifich=4: + fichiers txa tr2 fort.85 (depannage)
c           ifich=5: + fichiers ali fort (depannage transfert)
c           ifich=6: +fichier pge(pertes&gains a l'equil. ts m,ts les nimpf)
c           ifich=7: +fichier pgt(pertes&gains=f(T),n cst,ts les nimpm&nimpf)
c           ifich=8: +fichier pgt(pert&gains=f(T),(nT)cst,ts les nimpm&nimpf)
c   ifich=9:  0+ le fichier tr2 contient les dtaur pour tous kr et m (Agata)
c   ifich=10: 0+ le fichier cap: ionisation du fer et capabs (Zwicky)
c   ifich=11: 0+ le fichier cap: abond.elem.&ions,capabs,dic&dir (Delphine)
c (resultats de la derniere iteration: res, te2, fl2, tr2, tx2, an2, de2, ime)
c
c numeros des fichiers
c    6    8        11       12   13   14   18   30   32   34   17   19   33  
c  .log ti9.dat atomic.dat fli. mzn. .res .te2 .spe .de2 .an2 .tem .flu .ang 
c nom web:                           .res .tem .spe .deg .ang .tet .flt .ant
c    10   15   16   20   21   22   23   24   25   26   27   28   29 
c  .atom .txt .tx2 .fl2 .tr2 .txa .bil .dio .ime iml. mzt. .cap .ali
c   31   35   40    9  
c  .pge .pre .pgt debug
c
c      excitations par collision:Daresbury+formules de Mewe ou upsilon Tipbase
c      ionisations par collisions par les formules de Peq+Shull
c      ionisations radiatives:fits des valeurs de Reilman-Manson ou de TOPBASE
c      recombinaisons radiatives totales & dielectroniques:Badnell 2006
c      recombinaisons radiatives sur chaque niveau: ds FALFINT ou integration
c                 des sigmas de TOPBASE et fits a 4 parametres ds recombint.f
c ECLAIREMENT INCIDENT :
c incid=0 : nuage non illumine mais chauffe mecaniquement, T constant
c incid=1 : spectre incident en loi de puissance (FFLUXLP)
c incid=2 : spectre incident en forme de corps noir (FFLUXBB) attenue par wbb
c incid=3 : spectre incident calcule ds FFLUX (Laor1997ApJ477,93) - analytique
c incid=4 : spectre incident calcule ds FFLUX1 (AGN de Cloudy) - analytique
c incid=5 : spectre incident calcule ds FFLUX2 (Netzer 1996,ApJ 473,781)
c incid=6 : spectre incident calcule ds FFLUX3 (Krolik 1995,ApJ 447,512)
c incid=7 : spectre incident de NGC5548 calcule ds FFLUX4 (Collin1998A&A331,11)
c incid=8 : spectre incident de NGC3783 donne ds FFLUX5 (Kaspi ApJ2001,554,226)
c incid=9 : spectre incident FFLUX6 de Lexington 2000 (X-ray thin, from Netzer)
c incid=10: forme du spectre donne par points, *A ->csit (broken power law)
c           donnees dans le .dat: nb points, loghnuev,logflux, nb fois
c incid=11: spectre donne par fli.xx,tous les kc croissants, *1
c incid=12: forme du spectre donne par fli.xx,tous les kc croissants, *A ->csit
c       (pour 11 & 12: OBSOLETE :format libre,pas de titre, 
c             322 lignes: hnuev  flux +ligne vide a la fin)
c             au dessus de 26kev le spectre est une loi de puissance (flindex)
c incid=13: spectre donne par fli.xx copie d'un .res,colonne des sortants, *1
c incid=14: spectre donne par fli.xx copie d'un .res,colonne des reflechis, *1
c       (pour 13 et 14: renommer un xx.res en fli.xx, il faut les kc)
cc       de 11 a 14: AU DESSUS DE 26kev le spectre est une loi de puissance 
cc       d'index flindex se raccordant a la valeur du flux a 24.8182kev (kc=184)
c incid=15: forme du spectre donne par fli.xx: titre/nbre de freq/hnuev F(nu)
c incid=16: forme du spectre donne par fli.xx: titre/nbre de freq/hnuev nuF(nu)
c       (de 11 a 16: controler le fichier xx.atom en sortie)
c incid=17: spectre incident en loi de puissance avec coupure exponentielle
c          (FFLUXECO1LP-Agata)
c incid=18: double loi de puissance avec coupure exponentielle
c          hnumin1<=hnumin2  (FFLUXECO2LP-Agata)
c      incang=0 : eclairement semi-isotrope
c      1<incang<ndir : eclairement dans un cone (evide) autour du ieme angle
c   irent=0 : pas de flux "rentrant" a l'arriere du nuage
c   irent=1 : flux "rentrant" a l'arriere du nuage, (corps noir de T=Tbb)*wbb
c   irent=2 : I- "rentrant" a l'arriere du nuage=I-(H)= I+(H) + wbb*BB(Tbb)
c   irent=3 : I- "rentrant" arriere =I-(H)= wbb*BB(Tbb) - I+(H)          Suzy
c   irent=4 : I- "rentrant" arriere =I-(H)= I+(H) + wbb*BB(Tbb)*f(nu,T)Bozena
c   irent=5 : flux "rentrant" a l'arriere du nuage = (loi de Wien de T=Tbb)*wbb
c   irent=6 : flux "rentrant" a l'arriere du nuage = (flux incident)*wbb
c   irent>0 si 'tdebut' non nul: I-(1er tour)=I-(0)=(I+(0) + BB(t=tdebut)) *fii
c OPTIONS PHYSIQUES
c idens=0 : densite CONSTANTE dans le nuage
c idens=1 : densite imposee, en loi de puissance, dans le nuage
c idens=2 : PRESSION GAZEUSE CONSTANTE dans le nuage-calcul avec le strate m-1
c idens=3 : PRESSION TOTALE CONSTANTE dans le nuage -calcul avec le strate m-1
c idens=4 : PRESSION TOTALE CONSTANTE (sans les raies)-calcul avec le strate m-1
c idens=5 : PRESSION GAZEUSE CONSTANTE dans le nuage-iteration sur la densite
c idens=6 : PRESSION TOTALE CONSTANTE dans le nuage -iteration sur la densite
c idens=7 : PRESSION TOTALE CONSTANTE (sans les raies)-iteration sur la densite
c idens=8 : PRESSION TOTALE CONSTANTE, n est calcule pour un strate au debut
c           (avec T&J de l'it. precedente),on cherche T tel que perte-gain=0
c           solution dite "intermediaire"
c idens=9 : PRESS. TOTALE+TURBULENCE CSTE n calcule avecT&J de l'it. precedente
c idens=10: PRESSION TOTALE CONSTANTE, solution FROIDE, nT calcule avec T&J de
c     l'it. precedente, on cherche T tel que perte-gain=0 en partant de T=7000
c idens=11: PRESSION TOTALE CONSTANTE, solution CHAUDE, nT calcule avec T&J de
c     l'it. precedente, on cherche T tel que perte-gain=0 en partant de T=3e7
c idens=12: PRESSION TOTALE CSTE, solution instable
c idens=14: special Loic
c idens=20: PRESSION TOTALE CSTE, sol. froide idem, mais ss raies dans le prad
c idens=21: PRESSION TOTALE CSTE, sol. chaude idem, mais ss raies dans le prad
c     itemp=0 : recherche de l'equilibre thermique
c     itemp=1 : au dela de z=0.98*H on impose une temperature constante
c     itemp=10: PAS D'equilibre thermique: temperature imposee constante=Tinit
c     itemp=11: PAS D'equilibre thermique: temperature imposee lue(igrid=13-14)
c DIVERSES OPTIONS PHYSIQUES
c idilu=0 : nuage plan : pas de dilution
cc idilu=1 : halo spherique : dilution en 1/R2   PAS EN SERVICE
c   ietl=0 : calculs loin de l'ETL : tous les processus sont pris en compte:
c            calcul des recombinaisons radiatives par les formules de Verner
c   ietl=1 : calculs pres de l'ETL : suppressions des recombinaisons         !m
c            dielectroniques et calcul des recombinaisons radiatives
c            par integration de sigmav
c        iprof=0 : profil Voigt 
c        iprof=1 : profil Doppler (am=0) pour les raies 2-1 Hydrog.& Heliumoid.
c        iprof=2 : profil Doppler (amortissement=0)
c irecsup=0:ion a plusieurs niveaux excites: les seules recombinaisons sont
c        les recombinaisons radiatives sur chaque niveau
c irecsup=1:ion a plusieurs niveaux excites: les recombinaisons dielectroniques
c        + la difference entre les recombinaisons radiatives totales (Verner)
c        et la somme des recombinaisons radiatives sur tous les niveaux
c        (RDI + RRtot -sommeRRn) sont attribuees au(x) niveau(x) superieur(s)
c irecsup=2:ion a plusieurs niveaux excites: les recombinaisons supplementaires
c        (RDI + RRtot -sommeRRn) sont distribuees sur tous les niveaux
c   chaufsup>0 : nuage chauffe mecaniquement: chauffage supplementaire constant
c DECOUPAGE DES STRATES :                   
c donnees : igrid,nstr,ichg
c rajouter une ligne de donnees a la fin si igrid=1 ou 2 (entrer les 8 nombres)
c               ndcm,npd1,dcol1,diviseur,debchute,finchute,npdf,dcolf
c  ichg=2 (ou 1,plus ancien): changement automatique de decoupage si besoin
c  ichg=0 : pas de changement de grille
c     igrid=0 : (decoupage en colonne-densites)
c               NSTR strates egaux a CONDITION que coltot=<1e22 sinon stop
c     igrid=1 : (decoupage en colonne-densites)
c               1eres decades: a partir de NH=dcol1, npd1 strates par decade
c                    en progression geometrique (strate m: dc=dcol1*q1**(m-1))
c               derniere decade: strates egaux a coltot/ndcm
c                    si diviseur est different de 1, les strates
c                    sont divises par diviseur entre debchute et finchute
c               decroissance des derniers strates avec npdf strates par decade
c                    jusqu'a ce que dcol=dcolf
c     igrid=2 : (decoupage en z)
c               1eres decades: a partir de dz=dcol1, npd1 strates par decade
c                    en progression geometrique (strate m: dz=dcol1*q1**(m-1))
c               derniere decade: strates egaux a Htot/ndcm
c                    si diviseur est different de 1, les strates
c                    sont divises par diviseur entre debchute et finchute
c               decroissance des derniers strates avec npdf strates par decade
c                    jusqu'a ce que dz=dcolf
c pour les options suivantes le nombre de strates est le NSTR du dat.xx
c     igrid=10: NSTR strates dont les colonnes-densites sont lus dans MZN
c                      (1ere ligne=titre, 1 colonne lue, en format libre)
c                      ATTENTION aux derniers strates: pas de col egaux
c     igrid=11: NSTR strates dt les col-dens & les densites sont lus dans MZN
c                      (1ere ligne=titre, 2 colonnes lues, en format libre)
c     igrid=12: NSTR strates dt les z et nht sont lus dans MZN-pas de z egaux
c                      (1ere ligne=titre, 2 colonnes lues, en format libre)
c     igrid=13: PAS D'equilibre thermique: m,z et temper. imposee lus (.MZT)
c         (format du .res:ligne'coldens',puis rien rien T rien m z,-1 a la fin)
c     igrid=14: PAS D'equilibre thermique: col,temperature et densite imposes
c              (MZT.file: 1 ligne non lue,puis: col nht T rien m, format libre)
c OPTIONS DE CALCUL
c inr=1 : 931 raies + 92 raies UTA - inr=2 : + 3118 raies venant de Cloudy
c      incang=0 : eclairement semi-isotrope
c      1<incang<ndir : eclairement dans un cone (evide) autour du ieme angle
c    ndir= 1 ou 3 ou 5 ou 20 pour un eclairement semi-isotrope
c    ndirlu=103 (4 dir.) ou 105 (6 dir.) pour un eclairement unidirectionnel
c       (directions choisies pour une meilleure integration en omega,
c       sauf pour isotrope_20dir et ndirlu=205 (6 dir.) )
c          isuite=0 : 1er passage sans mettre les J en fichier
c          isuite=1 : 1er passage en sauvant les J dans le fichier .ime
c          isuite=2 : continuer les iterations en flux sans revenir au debut
c                   lire les z et les taus-continu (fichier.res renomme .MZT)
c                   et les J du fichier .iML (fichier.ime renomme)
c          isuite=3 : reprendre aussi les J des raies
c iacc=0 : deroulement normal
c iacc=1 : acceleration de convergence pour le continu (paletou)sur 4 iterations
c iacc=2 : accel. de convergence pour les raies kali=2 (ali-simple)
c iacc=3 : accel. de convergence pour les raies kali=1 avec les it. *1,2,3,4
c    isf=2 : interpolation parabolique de S dans la solution formelle de l'int.
c    isf=0 : interpol. lineaire de S dans la sol.formelle de l'int.(cont+raies)
c    isf=1 : interpol. lineaire de S dans la solution formelle de l'int.(raies)
c    isf=3 : interpol. lineaire si S passe par un maximum (raies)-marche mal
c      si nfmax<0 : on fait "nfmax" iterations
c      si nfmax>0 : les iterations s'arretent lorsque la temperature et 
c                   la somme des flux des raies sont devenus stables
c icompt=0 : pas de comptonisation des raies 
c icompt=1 : "comptonisation"=sortie du profil de raie si dEcompt>FWHM*icompt
c icompt=-1 : comptonisation des raies pour tout choc Thomson
c
c IMPRESSIONS
c               imp=0: impressions minimales
c               imp=1: tau I+ I-,  continu et raies, tous les 'nimpm' strates
c               imp=2: degres d'ion. & detail des pertes tous les nimp strates
c               imp=3: bord Lyman tous les 'nimpm' strates
c               imp=4: pb de convergence d'ionisation ou de temperature ->.log
c               imp=5: resume tous les strates,continu et raies les plus 
c                                            intenses tous les 'nimpm' strates
c               imp=6:+termes des equations d'ionisation de H tous les nimpm st
c               imp=7:+termes des equations d'ionisation tous les nimpm strates
c               imp=8: toutes les donnees raies a la derniere iteration fort.51
c donner une resolution si on veut le spectre melange continu-raies ds le .res
c mettre nimpff<0 si on veut imprimer en plus les 15 dernieres iterations
c
c si on veut imprimer les donnees atomiques de LECTATOM, y rajouter imp=10
c
c HISTORIQUE
c point de depart : le programme SERP3 de Suzy du 27 juin 1973 et 03 dec 83
c        modifications d'avril 1990 a mars 1991  ( Anne-Marie Dumont ) :
c        - introduction des profondeurs optiques
c        - calcul des photoionisations internes
c        - reactualisation des donnees (pas forcement exhaustive)
c        - fevrier 1992 : acceleration des calculs
c        - mars et juin : + Fe (donnees fittees sur M.Arnaud, Yakovlev...)
c        - avril : rayonnement exterieur
c        - juillet : chauffage Compton (formule de Tarter-Masnou-Collin)
c        - janvier 1993 : nouveaux facteurs de Gaunt                         !m
c        - corrections et ajouts dans les differents gains et pertes : echanges
c             de charges, gains free-free,recombinaisons dielectroniques et sur
c             les niveaux secondaires
c        - mars : echappement moyen de Canfield-Ricchiazzi dans ces pertes
c        - decembre : 6 niveaux + continu pour l'hydrogene
c        - janvier 1994: elargissement de la gamme des frequences: 0.01ev-26kev
c        - sept 94 : calcul des J et tau avant (et apres) la discontinuite
c       - janvier 1996 : au dessus de 26kev le chauffage Compton est calcule
c           a partir du fichier chaufcompt venant de NOAR avec la formule
c           gcomptx=(a*exp(-b*tauth) + c*exp(-d*tauth) + e*exp(-f*tauth))
c                    *flxdurincid(>26kev)*Netot*0.665e-24 /NeNh
c           tauth depuis le bord, a b c d e f dependent de la forme du spectre
c           (2e exp:jan98-3e exp:oct2001) cf Jean-Marie Hameury 1988 A&A 205,19
c        - mars : ionisation de H par electrons secondaires(issus d'ion.par X)
c        - decembre 1996 : 6 niveaux + continu pour l'hydrogene
c                     5 niveaux + continu pour tous les autres hydrogenoides
c        - autres ions que les Hydrogenoides : 1 recombinaison secondaire 
c                   donne 1 photon de resonance (1ere raie)
c        - aout 1999 : nouveaux decoupages en z et en hnu
c        - fev 2000-fev01: niveaux excites de tous les ions selon le atomic.dat
c        modifications pour le multi-couches :
c        - introduction d'une stratification (et flux restant apres chaque st.)
c        modifications pour le transfert
c        - fevrier 1994 : transfert du continu et
c        - fevrier 1995 : transfert des raies avec 15 frequences par raie
c        - resolution du transfert par la methode de Simone Dumont d'iteration:
c            calcul des T, em, cappa, I+, J, S en des points z espaces de dhaut
c             a l'aller, calcul des I- au retour et iteration
c        - avril 1996 : transfert des raies de fluorescence
c           la raie K du Fer 24 n'est plus calculee avec la raie 386 du Fer 25
c           car les hnu sont differents: c'est un autre niveau excite (juin98)
c        - janv-oct 2000: subroutine Paletou pour le transfert ALI du continu
c        - mai 2000-01: subroutine Paletou pour le transfert ALI dans les raies
c        - janv 2003: comptonisation des raies
c        - sept 2003: modification des entrees (atomic.dat) & sorties
c        - dec 2003: rajout automatique de strates aux chutes de degres d'ion.
c        - mai 2004: plusieurs directions ds le calcul du transfert
c        - nov 2004: iteration sur la densite ds les modes a pression cste
c        - solutions chaudes et froides pour le mode a pression cste
c        - rajout de 3000 raies de r�sonnance de Cloudy trait�es en ions a 2 niv
c nov06 & fev07: introduction des UTA des Fer I a XVI et Ne a Fe, traites
c             en 2 niveaux  (atomic16.hhe15libeo4feuta)
c 30/06/2006: Reactivate start from coldens,nht,T in mzt.toto (igrid=14).
c 24/06/2006: output .pre (pressure), heating-cooling several iterations
c 02/08/2006(v104.46p): new incid=9 as a generic broken power-law (FFLUX6,
c             applied to Lexington 2000 X-ray thin case)
c 02/08/2006: more output in header for incident spectrum quantities (F,U,Q),
c             U_x and U_ox 
c 20/09/2006(v104.47a): dielectr.recomb.of Badnell (atomic14.hhe15libeo4fe)
c        BUGS correction: .pge gcompts and gcomptx swaped, also added in .pgt,
c                         wrong ionization parameter from FFLUX6,
c                         bad integration of Fx, Ux (not 100eV to...)
c mars 2007(v104.47a): total radiative recomb.of Badnell (atomic17.)
c mars 2007: correction des profils de microturbulence
c avril 2007: 3118 raies de resonance supplementaires traitees a 2 niveaux
c    (inr=1 : 931 raies + 92 raies UTA - inr=2 : + 3118 raies de Cloudy)
c juin 2007 : plus de pertes dielectroniques
c sept 2007 : update deexcitations collisionnelles
c 22/11/2007: power-law, and double power-law with exponential cut-off by Agata
c             bug correction for computations of Tinit sometimes less then 0
c mars 2008 : calcul des gains-pertes Compton >26 kev par defaut (Tarter)
c nov 2008  : correction du calcul des gains-pertes Compton formule Tarter
c 2008:amenagement des entrees-sorties pour un usage public a travers ASTROGRID
c             option nuage non illumine mais chauffe mecaniquement, T constant
c             ou chauffageconstant - option de lecture du flux incident
c             - flux rentrant - option "suite"
c
c j est le numero de l'element X (H,He,C,N,O,Ne,Mg,Si,S,Fe)
c i est l'etat d'ionisation(1 pour X0, 2:X+, 3:X++, Z:hydrogenoide, Z+1:ion nu)
c degre d'ionisation r(i,j)
c n est le numero du niveau - population pour un ion den(nij)=ab(j)*r(ij)*po(n)
c nij pour i,j,n
c k numerote les frequences du rayonnement, m les strates
c
c LIMITATIONS
c le nombre de niveaux par ion est au maximum de 15
c le nombre total de niveaux, de raies, et de strates est a ajuster dans 
c les ordres "parameter" : nivtots et nivmax, nrais, nzs
!
! DECLARATIONS
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nrais=4200,ndirs=20,nfrqs=16,nivtots=330)
         double precision ne,nht                                             !m
       character titre*78,tab*1,ecrit1*10,ecrit2*8,ligne*80,txt*5,txt2*1
       character fiinput*16,fichatomic*51,fiato*30,fich*30,
     &      nomion*7,nomrai*7
         character *30 fifli,fimzn,fires,fitxt,fitx2,fitem,fite2,fiflu,
     &      fifl2,fitr2,fitxa,fibil,fidio,fide2,fiiml,fimzt,fispe,fiali,
     &      ficap,fipge,fipgt,fiime,fiang,fian2,fipre
         character langue*1
         integer ijour(3)
         dimension t1(nzs),t2(30),t3(30),mt(nzs),iw(220)
         logical ldebug                               ! LC variables (common)
       common/abond/ab(10)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &             kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/cst3/ee4,collg,hlog2
       common/nom/nomion(322),nomrai(0:nrais)
       common/lecentr/flym,resolution,fifli,fimzn
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/raiesuta/probuta(100),dnuta2(100),utanion(26,10),
     &                 juta(100),iuta(100)
       common/raiesadd/is(3200),js(3200)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/drugi/ flin2,hnumin2,hnumax2,fxdofo,fluxex2          !Agata
       common/flux7/yloghnu(10),ylogfnu(10),a(9),b(9),cpl,npy
       common/im/fii,itali
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/temp/T,VT,tk,tk1,templog
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2)      !m
c cimoins(322,0:nzs+2),
       common/fsource/sourcec(322,nzs+1),sourcer(16,nrais,nzs+1)
       common/lecdeg/eps1,eps2,sel
       common/degmt/hautsom,dtaucmax,hnumaxto,
     &             p_grait,p_gfree,p_gbound,g_pcompt,ptot,
     &             restflux(322),rtaucef(322),ttaucef(322),rtaucat(322)
       common/degmt2/exdtauc(322,0:nzs+1),exdtaur(16,nrais,0:nzs+1)
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/mec1/titre,fichatomic
       common/mec2/rayminrg,raymaxrg,hLbol,tauth,ndirlu
       common/tabm/exflux(322),thnur(nrais)
       common/stnua/prad(nzs),pradrai(nzs),pradrai1(nzs),rhep2
       common/mstpo/rtaur0(nrais),rtaurl(nrais),
     &             twr(nrais),tws(nrais),uhz(nrais),cjn(nrais)
       common/pgtt/ttpraie,ttgraie,ttpertefluo,ttpraieint,ttpgrai,
     &             ttpech,ttpfree,ttprecf,ttprsecdi,ttgfree,ttgion,
     &             ttgpcompt,ttgcomptx,rtauthx
       common/pgtt2/ttpgray                   !yyy
       common/lyman/rsomtely,rsomely,ssomtely,ssomely
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/flx/flxdurincid,acompt,bcompt,ccompt,dcompt,ecompt,fcompt,
     &            scomptx1,scomptx2,ftauth
       common/bb/Tbb,wbb
       common/mil/rh1smil,rh2pmil,rhe1mil,rhe2fmil,rhe22mil,rfe15mil
       common/comptonrai/dnuco(nrais),sourdnuco(2,nrais,nzs+1),
     &   ripdnuco(2,nrais),pripdnuco(2,nrais),rimdnuco(2,nrais,0:nzs+2)
       common/comptonrai2/ncomp(nrais),mdebcomp(nrais)
       common/cosw/csz(ndirs),wtdir(ndirs)
       common/cpal/temcac(322,nzs),tcapc(322,nzs),tcth(nzs),temc(322),
     &             pradc(nzs)
       common/mgcpal/cimoinsh(322),ciplush(322),cimoins0(322),
     &               fcra(ndirs,322),fcsa(ndirs,322)
       common/mgrpal/tflincident,fsor(2,nrais),frer(2,nrais),
     &               fasor(ndirs,nrais),farer(ndirs,nrais)
       common/rpal2/kali(nrais),npbali(nrais),lcompt(nzs,nrais)
       common/sporpal/tjr(nrais,nzs),tcjn(nrais,nzs),pradr(nrais,nzs)
       common/rpal5/tjrnu(nfrqs,nzs,nrais)
       common/talfdeg2/exphot(nivtots),exphotk(nivtots)
       common/mweb/Tcst,fich,fimzt
! LC COMMON - Loic Chevallier
! coltdrop is the column-density for which we compure p11 models below and
! p10 models above.
! it permits to force some intermediates models between p11 and p10.
! nitpg is the iteration number where extra Heating-Cooling curves are
! printed every iteration to .pgt when ifich=7 or 8.
! COMMON debug includes all flags relative to debug modes (verbose printing,
! etc.)
! ldebug: print some debugging messages
! iverbose: level of verbosity of debugging messages, 1: minimum, 2: details,
! 3: all       
! lcrates contain recombination, photoionisation rates, defined in MAIN and
! sNUAGE, to print output
! alphaij(m,k) contains total recombination on each ion k and each layer m
! of last iteration (printed unit=87)       common/lcrates/alphaij(nzs,120)
       common/lcf/coltdrop
       common/lci/nitpg
       common/debug/ldebug,iverbose
c main
       external idate
!LC init
        ldebug = .False.
        iverbose = 1
!
        call idate(ijour)
        pi=3.1415926535898d0
        pi4=12.5663706144d0
        V3=sqrt(3.d0)
        pi2v3=6.28318530718d0/V3                       ! 2pi/V3
        Vpi=1.77245385091d0
        chel=1.602192d-12                              ! e'
        esh=2.41797d14                                 ! e'/h : nu eV->Hz
        hca=1.986485d-8
        hcse=12398.54d0                                ! h*c/e'*1e8
        hcsb=1.43884d8                                 ! h*c/k en angstrom
        ckev=8.6171d-5                                 ! k/e'
        bhca=0.02654d0*1.986485d-8                     ! 0.02654*h*c angstrom
        ee4=expa(+4.d0)
        collg=1.3711d-7                                ! 17.e-4/hcse(=12398.54)
        hlog2=log10(2.d0)
        nf=15
c        tab=char(9)                                   ! tabulation
c        tab=';'
c        tab=' '
              ab(1)=1.d0
           do j=2,10
              ab(j)=0.d0
           enddo
           flym=0.d0
           hnumin=0.01d0
           hnumax=1.d5 ! for some printings
c
c LECTURE DES DONNEES dans le fichier ti9.dat  ! READING DATA in file mode CP
        fiinput='TICPinput.dat'                   ! ou ti9.dat
        OPEN(UNIT=8,FILE=fiinput,STATUS='OLD',ERR=7000)
        iexpert=1
c lecture des parametres du nuage ! reading cloud data                       !m
        READ(8,70) titre
 70       format(a78)
        READ(8,'(a)') fich                       ! output file name
        READ(8,*) csit,incid,ndirlu,incang,irco  ! illumination
           if(incid.eq.1.or.incid.eq.17.or.incid.eq.18)
     &  READ(8,*) flindex,hnumin,hnumax          ! illumination
           if(incid.eq.18)
     &  READ(8,*) flin2,hnumin2,hnumax2,fxdofo   ! hnumin <= hnumin2  Agata
           if(incid.eq.2)
     &  READ(8,*) Tbb,wbb 
           if(incid.eq.10)
     &  READ(8,*) npy,(yloghnu(i),ylogfnu(i),i=1,npy)
           if(irco.eq.1)
     &  READ(8,*) px26,acompt,bcompt,ccompt,dcompt,ecompt,fcompt
c                  ! gcompt prop. to aexp(-btauth)+cexp(-dtauth)+eexp(-ftauth)
        READ(8,*) irent,Tbbar,wbbar              ! backward illumination
           if(irent.ne.0) then
              Tbb=Tbbar
              wbb=wbbar
              tdebut=Tbb
           endif
        READ(8,*) coltot,densinit,dendex,Raymin  ! cloud
        READ(8,*) (ab(j),j=2,10)                 ! abundances/H
        READ(8,*) vturbkm,chaufsup               ! turbul.km/s,suppl.heating
        READ(8,*) idens,itemp,ietl               ! n/p cst,Tcalc/given,ETL=1
        READ(8,*) inr,iprof                      ! line options
           icompt=1
        READ(8,*) nfmax,resol,ifich,nimpm,nimpff  ! output
        READ(8,*) igrid,nstr,ichg                ! grid
        READ(8,*)
           if(igrid.lt.10.and.igrid.ge.1)
     &  READ(8,*)ndcm,npd1,dcol1,diviseur,debchute,finchute,npdf,dcolf
           if(igrid.eq.0) ndcm=nstr
c
!LC
          coltdrop = -1.d0
             if(idens.eq.14) then
                tdens=float(idens)
                coltdrop = tdens
             endif
! if ifich < 0, then we use ifich=7 or 8 (set below) to print .pgt curves
! every iteration
! from the first one for the computed heating-cooling, and we add extra
! curves every iteration starting at iteration -ifich.
          if(ifich.eq.8.and.(idens.eq.8.or.idens.eq.9)) ifich=7
          if(ifich.eq.7.and.idens.ge.10) ifich=8
          nitpg = -1
          if (ifich.lt.0) then
             nitpg = -ifich  ! ifich=7
             ifich = 8
             nimpff = 1
          endif
!LC
          langue='e'
          if(langue.eq.'f') then
             ila=0
             tab=';'
          else
             ila=1
             tab=' '
          endif
          ikitf=1
          if(nfmax.lt.0) then
             nfmax=-nfmax
             ikitf=0
          endif
          fichatomic='TitanAtomic.dat' ! = atomic20.4000col
          irecsup=2
          isf=2
          iacc=2
          isuite=0
          aint=2.5
          fdolim=1.e-12
          imp=0
       goto 7005
c
c LECTURE fichier dat.web, rempli sur internet avec Astrogrid
 7000  OPEN(UNIT=9,FILE='dat.web',STATUS='OLD',ERR=7001)
       iexpert=-1
          CALL LECTASTROGRID
       goto 7002
 7001  iexpert=0
c LECTURE fichier grand public lu par: ./executable < input (read(5)
          CALL LECTENTREETEXTE
c          titre=' TITAN given density model'
          fich='titan_model'
          ifich=0
c
 7002     langue='e'
          ila=1
          tab=' '
          fichatomic='TITANatomic.dat'                ! = atomic20.4000col
          ikitf=1
c donne par fichier <input ou fichier web :
c incid,(flindex,hnumin,hnumax),csit ou Flym,irent(Tbb,wbb)
c densinit,coltot,dendex(raymin),vturbkm,idens,(itemp),resol,9 abondances
c nfmax,ndirlu,incang,inr,(igrid,nstr),ifich,Tcst
c + titre,fich(web),fifli,fimzn
          resol=resolution
          chaufsup=0.d0        
          ietl=0
          irecsup=2
          isf=2
          iacc=2
          if(incid.eq.2) tdebut=Tbb
          isuite=0
          nimpm=50
          nimpff=10
          imp=0
c Compton
          px26=0.d0
          acompt=0.d0
          bcompt=0.d0
          ccompt=0.d0
          dcompt=0.d0
          ecompt=0.d0
          fcompt=0.d0
c raies
          iprof=1
          aint=2.5
          fdolim=1.e-12
          icompt=1
c grille en z
          ichg=2
          npd1=8
          npdf=8
          dcol1=1.e15
          dcolf=1.e12
          diviseur=1.d0
          debchute=1.d0
          finchute=1.d0
          if(igrid.eq.1) then    ! grille non definie dans le fichier d'entree
             if(coltot.le.1.d16) then
                igrid=0
                ndcm=30
             else if(coltot.le.1.d17) then
                ndcm=40
             else if(coltot.le.1.d19) then
                ndcm=50
             else if(coltot.le.1.d21) then
                ndcm=100
             else if(coltot.le.1.d22) then
                ndcm=200
             else
                ndcm=400
             endif
          endif
 7005  continue
c end reading files expert & public
c fii est defini plus loin
          if(itemp.eq.0) then
             if(csit.ge.1.d4) then
                Tinit=8.d6
             else if(csit.ge.3000.d0) then
                Tinit=3.d6
             else if(csit.ge.800.d0) then
                Tinit=1.d6
             else if(csit.ge.300.d0) then
                Tinit=7.d5
             else if(csit.ge.100.d0) then
                Tinit=3.d5
             else if(csit.ge.30.d0) then
                Tinit=7.d4
             else
                Tinit=4.d4
             endif
          endif
          if(itemp.eq.1.and.Tcst.gt.1.d0) Tinit=Tcst
c
                nc=index(fich,' ') - 1
        if(iexpert.eq.1) then
           fifli='fli.'//fich        ! 12
           fimzn='mzn.'//fich        ! 13
           fiato=fich(:nc)//'.atom'  ! 10
           fires=fich(:nc)//'.res'   ! 14
           fitxt=fich(:nc)//'.txt'   ! 15
           fitx2=fich(:nc)//'.tx2'   ! 16
           fitem=fich(:nc)//'.tem'   ! 17
           fite2=fich(:nc)//'.te2'   ! 18
           fiflu=fich(:nc)//'.flu'   ! 19
           fifl2=fich(:nc)//'.fl2'   ! 20
           fitr2=fich(:nc)//'.tr2'   ! 21
           fitxa=fich(:nc)//'.txa'   ! 22
           fibil=fich(:nc)//'.bil'   ! 23
           fidio=fich(:nc)//'.dio'   ! 24
           fide2=fich(:nc)//'.de2'   ! 32
           fiime=fich(:nc)//'.ime'   ! 25
           fiiml='iml.'//fich        ! 26
           fimzt='mzt.'//fich        ! 27
           ficap=fich(:nc)//'.cap'   ! 28
           fiali=fich(:nc)//'.ali'   ! 29
           fispe=fich(:nc)//'.spe'   ! 30
           fipge=fich(:nc)//'.pge'   ! 31
           fipgt=fich(:nc)//'.pgt'   ! 40
           fiang=fich(:nc)//'.ang'   ! 33
           fian2=fich(:nc)//'.an2'   ! 34
           fipre=fich(:nc)//'.pre'   ! 35
        else if(iexpert.le.0) then
           fires=fich(:nc)//'.res.txt'   ! 14
           fispe=fich(:nc)//'.spe.txt'   ! 30
           fite2=fich(:nc)//'.tem.txt'   ! 18
           fide2=fich(:nc)//'.deg.txt'   ! 32
           fian2=fich(:nc)//'.ang.txt'   ! 34
           fitem=fich(:nc)//'.tet.txt'   ! 17
           fiflu=fich(:nc)//'.flt.txt'   ! 19
           fiang=fich(:nc)//'.ant.txt'   ! 33
        endif
c
        OPEN(UNIT=11,FILE=fichatomic,STATUS='OLD')
        if(iexpert.eq.1) OPEN(UNIT=10,FILE=fiato,STATUS='unknown')
        if(ila.eq.0) WRITE(10,89) fichatomic
        if(ila*iexpert.eq.1) WRITE(10,989) fichatomic
 89     format(' DONNEES ATOMIQUES: fichier ',a51)
 989    format(' ATOMIC DATA file ',a51)
c if(incid.ge.11) OPEN(UNIT=12,FILE=fifli,STATUS='OLD')   voir plus loin
c if(igrid.ge.10) OPEN(UNIT=13,FILE=fimzn,STATUS='OLD')                ! mzn.
c if(isuite.ge.2) OPEN(UNIT=26,FILE=fiiml,STATUS='OLD')
c if(igrid.ge.13.or.isuite.ge.2) OPEN(UNIT=27,FILE=fimzt,STATUS='OLD') ! mzt.
           if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &  OPEN(UNIT=15,FILE=fitxt,STATUS='new')
c
! lecture des donnees atomiques - reading atomic data
        CALL LECTATOM
        CLOSE(11)
        fedd=0.d0                        ! fedd=L/Ledd
        if(nimpff.eq.0.and.nfmax.ge.10) nimpff=nfmax/10
        nimpf=abs(nimpff)                                      ! .tem,.flu,.dio
        nimpf2=nimpf                                           ! .txt,.tx2,.tr2
c        nimpf2=50
        dtaufixe=0.d0
        if(coltot.le.1.d23) then
           VTref=sqrt(Tinit/2.d0)
        else
           VTref=sqrt(Tinit/10.d0)
        endif
        idilu=0
c
c parametres supplementaires fixes au cours du programme
! other parameters given in the code
c test de la convergence des degres d'ionisation
! test of convergence of the ionisation degrees
!       eps1=0.0003                                                   lectatom
!            if(ecartdeg.le.eps1) kitd1=1                             degion
c nombre maximum d'iterations pour les degres d'ionisation
! maximum number of iterations for the ionisation degrees
!       nitdmax=20                                                    sstrate
!            if(incid.eq.0.or.csit.eq.0.d0) nitdmax=50
c determination de la temperature par iterations :
c pourcentages limite pour la temperature et les gains-pertes
! temperature calculation: limit percentages for temp. and heating-cooling
!       tlim=0.002d0                                                  snuage
!       pblim=0.01d0
!            if(idens.ge.3) : tlim=0.01d0 et pblim=0.05d0
!            if(nitt1.gt.2.and.            ! pbenef=pourcentage
!     &        (abs(T-ta)/T).lt.tlim.and.abs(pbenef).lt.pblim) kitt=1
c nombre maximum d'iterations pour la temperature
! maximum number of iterations for the temperature
!       nittmax=50                                                    snuage
!            if(idens.ge.3) nittmax=20
c conditions d'arret des iterations principales      (pvrai=pourcentage)
! conditions for stoping main iterations
!       if(ecart.lt.1.d-6.and.abs(pvrai).lt.1.d-2) kitf=1              main
c limites pour le changement de decoupage
! limits for z-grid change
!            if(kitf.ne.1.and.m.ge.10.and.tcol(m).lt.coltot*0.99d0.and.
!     &       (mbos(m-1).ge.1.or.mbos(m).ge.1.or.mbos(m+1).ge.1
!     &       .or.rapt.ge.1.25d0.or.rapt.le.0.9d0
!     &       .or.ro7.ge.2.d0.or.ro7s.ge.2.d0
!     &       .or.rc5.ge.3.d0.or.rne8.ge.3.d0.or.rne8s.ge.3.d0
!     &       .or.ro3.ge.3.d0.or.rc3.ge.3.d0.or.rhe.ge.2.d0
!     &       .or.ro2.ge.3.d0.or.ro2s.ge.3.d0.or.rc2.ge.3.d0)) then
! Ali: limits for iterations
!       dslim=1.d-4                                                   cpaletou
!            if(nitf.gt.50) dslim=min(1.d-4,eps(1))
!            if(dsmax.lt.dslim.and.nita.ge.3) kita=1
!       dslim=1.d-4    ! limite de variation de la fonction source    rpaletou1
!       dnlim=0.001d0  ! limite de variation des populations          rpaletou2
!            if((nita.gt.2.and.dnsnmax.lt.dnlim).or.nita.eq.99) kita1=1
c
c directions pour le calcul du tranfert (subroutines Paletou): cosinus et poids
! Ali transfer cosines and weights
        ndir=ndirlu
        if(ndirlu.eq.1) then
           csz(1)=0.577350269189d0
           csz(2)=0.d0
           csz(3)=0.d0
           wtdir(1)=0.5d0
           wtdir(2)=0.d0
           wtdir(3)=0.d0
           ndir=ndirlu
        else if(ndirlu.eq.3) then
           csz(1)=0.8872983346d0
           csz(2)=0.5d0
           csz(3)=0.1127016654d0
           wtdir(1)=0.13888888889d0         ! somme= 0.5
           wtdir(2)=0.22222222222d0
           wtdir(3)=0.13888888889d0
           ndir=ndirlu
c ndir=103: 3 directions + une 4eme tres proche de la normale
! ndir=103: 3 directions + 4th very near normal to the cloud surface 
        else if(ndirlu.eq.103) then
           csz(1)=0.9999995d0
           csz(2)=0.8872983346d0
           csz(3)=0.5d0
           csz(4)=0.1127016654d0
           wtdir(1)=0.000001d0              ! theta=2e-3
           wtdir(2)=0.13888788889d0
           wtdir(3)=0.22222222222d0         ! somme= 0.5
           wtdir(4)=0.13888888889d0                 ! on ne peut mettre cos=0
           ndir=4
        else if(ndirlu.eq.5) then
           csz(1)=0.953089922969331982d0                   ! 17.62        
           csz(2)=0.76923465505284155d0                    ! 39.715       
           csz(3)=0.5d0                                    ! 60           
           csz(4)=0.23076534494715845d0                    ! 76.66        
           csz(5)=0.046910077030668018d0                   ! 87.31degres  
           wtdir(1)=0.05923172126404727d0   ! somme(w)= 0.50
           wtdir(2)=0.11965716762484162d0   ! somme(cos*w)= 0.25
           wtdir(3)=0.14222222222222222d0
           wtdir(4)=0.11965716762484162d0
           wtdir(5)=0.05923172126404727d0
           ndir=ndirlu
c ndir=105: 5 directions + une 6eme tres proche de la normale
! ndir=105: 5 directions + 6th very near normal to the cloud surface 
        else  if(ndirlu.eq.105) then
           ndir=6
           csz(1)=0.9999995d0
           csz(2)=0.953089922969331982d0                   ! 17.62
           csz(3)=0.76923465505284155d0                    ! 39.715
           csz(4)=0.5d0                                    ! 60
           csz(5)=0.23076534494715845d0                    ! 76.66
           csz(6)=0.046910077030668018d0                   ! 87.31degres
           wtdir(1)=0.000001d0              ! theta=2e-3
           wtdir(2)=0.05923072126404727d0
           wtdir(3)=0.11965716762484162d0   ! somme(w)= 0.50
           wtdir(4)=0.14222222222222222d0
           wtdir(5)=0.11965716762484162d0
           wtdir(6)=0.05923172126404727d0
         else  if(ndirlu.eq.205) then
! ndir = 205 : 6 angles (5 mu-equidistant except small aperture near
! mu=1 for normal incident flux)
! used with NOAR (same grid) for Warm Absorbers
! angular grid (degree) : 5.73 (0.002 rad), 25.84, 45.57, 60, 72.54,84.26
! angular weights (sum is 0.5) : 1e-6, 0.1-1e-6, 0.1, 0.1, 0.1, 0.1
            ndir = 6
            csz(1)=0.9999995d0
            csz(2)=0.8999995d0
            csz(3)=0.7d0
            csz(4)=0.5d0
            csz(5)=0.3d0
            csz(6)=0.1d0
            wtdir(1)=0.000001d0
            wtdir(2)=0.099999d0
            wtdir(3)=0.1d0
            wtdir(4)=0.1d0
            wtdir(5)=0.1d0
            wtdir(6)=0.1d0
c 20 equidistant cos
        else if (ndirlu.eq.20) then
           ndir=ndirlu
           csz(1)=0.975d0
           csz(2)=0.925d0
           csz(3)=0.875d0
           csz(4)=0.825d0
           csz(5)=0.775d0
           csz(6)=0.725d0
           csz(7)=0.675d0
           csz(8)=0.625d0
           csz(9)=0.575d0
           csz(10)=0.525d0
           csz(11)=0.475d0
           csz(12)=0.425d0
           csz(13)=0.375d0
           csz(14)=0.325d0
           csz(15)=0.275d0
           csz(16)=0.225d0
           csz(17)=0.175d0
           csz(18)=0.125d0
           csz(19)=0.075d0
           csz(20)=0.025d0
           wtdir(1)=0.025d0
           wtdir(2)=0.025d0
           wtdir(3)=0.025d0
           wtdir(4)=0.025d0
           wtdir(5)=0.025d0
           wtdir(6)=0.025d0           ! somme(w)= 0.50
           wtdir(7)=0.025d0           ! somme(cos*w)= 0.25
           wtdir(8)=0.025d0
           wtdir(9)=0.025d0
           wtdir(10)=0.025d0
           wtdir(11)=0.025d0
           wtdir(12)=0.025d0
           wtdir(13)=0.025d0
           wtdir(14)=0.025d0
           wtdir(15)=0.025d0
           wtdir(16)=0.025d0
           wtdir(17)=0.025d0
           wtdir(18)=0.025d0
           wtdir(19)=0.025d0
           wtdir(20)=0.025d0
         else
       write(6,*)'nombre d''angles',ndir,'inconnu - not known in Titan'
         endif
c
! DECOUPAGE en strates si igrid <10 - col-GRID or z-grid
c      if(nitf.eq.1.and.isuite.le.1.and.itemp.le.10.and.igrid.lt.10)then
       if(igrid.lt.10.and.isuite.le.1) then
          CALL GRILLE
! lecture du decoupage (event. des densites) si igrid >=10 - read col-grid
       else if(igrid.ge.10) then
          if(igrid.eq.10.or.igrid.eq.11.or.igrid.eq.12) then
             OPEN(UNIT=13,FILE=fimzn,STATUS='OLD',ERR=50)
             goto 51
 50          continue
          WRITE(6,*)'the file ',fimzn,' is not correctly open - STOP'
             STOP
 51          continue
          endif
          if(igrid.ge.13) OPEN(UNIT=27,FILE=fimzt,STATUS='OLD')
          CALL LECGRILLE
       endif   ! igrid / 10
c
        if(incid.ge.11.and.incid.le.16) then
           OPEN(UNIT=12,FILE=fifli,STATUS='OLD',ERR=52)
           goto 53
 52        continue
        WRITE(6,*)'the file ',fifli,' is not correctly open - STOP'
           STOP
 53        continue
        endif
! lecture du FLUX INCIDENT si incid=11-12-read the INCIDENT FLUX if incid=11-12
                if(incid.eq.11.or.incid.eq.12) then
                WRITE(10,*)
                        do 20 kc=1,322                                       !m
        READ(12,*) hnuev,flu
        WRITE(10,*) hnuev,kc,flu
        exflux(kc)=flu
   20                        continue
                endif   ! incid= 11 ou 12
c lecture du FLUX INCIDENT si incid=13 ou 14: file .FLi (with format of .res)
                if(incid.eq.13.or.incid.eq.14) then
  175   READ(12,75) ecrit1
        nd=index(ecrit1,'h')
        if(nd.eq.0.or.ecrit1(nd:nd+4).ne.'hnuev') goto 175
c  ou                                if(ecrit1.ne.'     hnuev') goto 175
   75   format(a10)
                WRITE(10,*)
                        do 10 k=1,322
c        READ(12,76) hnuev,k,flrefl
c   76   format(f10.4,1x,i4,23x,e10.3)
        READ(12,'(a)') ligne
        npv1=index(ligne,';')
        READ(ligne(:npv1-1),*) hnuev
        npv2=index(ligne(npv1+1:),';') + npv1
        npv3=index(ligne(npv2+1:),';') + npv2
        READ(ligne(npv2+1:npv3-1),*) reflechi
        npv4=index(ligne(npv3+1:),';') + npv3
        READ(ligne(npv3+1:npv4-1),*) sortant
        npv5=index(ligne(npv4+1:),';') + npv4
        npv6=index(ligne(npv5+1:),';') + npv5
        npv7=index(ligne(npv6+1:),';') + npv6
        READ(ligne(npv6+1:npv7-1),*) kc
           if(incid.eq.13) then
           exflux(kc)=sortant
           else if(incid.eq.14) then
           exflux(kc)=reflechi
           endif   ! incid
c       if(k.ne.kc.and.ila.eq.0)write(6,*)' PB lecture des flux continus'
c         if(ifich.ge.1.and.ifich.le.8.and.k.ne.kc.and.ila.eq.0)
c     & write(15,*)'PB lecture des flux continus'
c       if(k.ne.kc.and.ila.eq.1)write(6,*) ' PB reading continuum flux'
c         if(ifich.ge.1.and.ifich.le.8.and.k.ne.kc.and.ila.eq.1)
c     & write(15,*)' PB reading continuum flux'
c        WRITE(10,*) hnuev,exflux(k),k
   10                        continue
                endif   ! incid=13 ou 14
c
c cas d'une SUITE : si isuite=2 et 3 :  lecture du decoupage en strates et
c des taus continus (pour les pertes sur niveaux secondaires)
! case of CONTINUATION of a previous run: (isuite=2 or 3) : reading grid and
! taus in continuum (for cooling by recombination on excited levels)
                if(isuite.ge.2) then
        OPEN(UNIT=27,FILE=fimzt,STATUS='OLD')
        OPEN(UNIT=26,FILE=fiiml,STATUS='OLD')
        READ(27,*)
  177   READ(27,77) ecrit2
   77   format(a8)
        nd=index(ecrit2,'s')
c       WRITE(6,*) ecrit2,'-',nd,'-',ecrit1(nd:nd+5)
        if(nd.eq.0.or.nd.gt.3.or.ecrit2(nd:nd+5).ne.'strate') goto 177
c                                ou    if(ecrit2.ne.' strate') goto 177
c        READ(27,*)
        if(igrid.ge.11) goto 179
        tz(1)=0.d0
        tcol(1)=0.d0
                        do mm=1,nzs
        READ(27,78) m,zz,tt,den,dcol
        if(m.le.0) goto 178
c        tz(m)=zz
        tcol(m+1)=tcol(m) + dcol
        tz(m+1)=tz(m) + dcol/den
        tnht(m)=den
        if(m.eq.1.and.(isuite.eq.2.or.isuite.eq.3)) Tinit=tt
        if(isuite.eq.5) temp(m)=tt
        if(m.ne.mm) write(6,*) ' PB lecture des z'
          if(ifich.ge.1.and.ifich.le.8.and.m.ne.mm)
     &  write(15,*) ' PB lecture des z'
        if(m.eq.1) WRITE(6,*) 'm   col   T'
        WRITE(6,*) m,tcol(m),tt
                        enddo                                                !m
   78   format(i3,1x,e9.3,1x,e10.3,3x,e10.3,1x,e10.3)
  178   continue
        npt=nzs
        if(m.lt.nzs) npt=mm-1
        write(6,*) 'npt iml=',npt
c
  179   READ(27,75) ecrit1
        nd=index(ecrit1,'h')
        if(nd.eq.0.or.ecrit1(nd:nd+4).ne.'hnuev') goto 179
c         if(ecrit1.ne.'     hnuev') goto 179
                        do 15 kc=1,322
        READ(27,79) k,to
       if(k.ne.kc.and.ila.eq.0)write(6,*)' PB lecture des flux continus'
         if(ifich.ge.1.and.ifich.le.8.and.k.ne.kc.and.ila.eq.0)
     & write(15,*)'PB lecture des flux continus'
       if(k.ne.kc.and.ila.eq.1)write(6,*) ' PB reading continuum flux'
         if(ifich.ge.1.and.ifich.le.8.and.k.ne.kc.and.ila.eq.1)
     & write(15,*)' PB reading continuum flux'
        if(kc.eq.1) WRITE(6,*) 'kc tau'
        WRITE(6,*) k,to
        ttaucef(k)=to
   15   continue
   79   format(11x,i4,45x,e10.3)
        READ(26,*)
        READ(26,*)
        do k=1,322
        READ(26,'(8(1pe10.3))') hkc,(tjc(k,m),m=1,npt)
        kc=int(hkc)
c        if(kc.eq.1) WRITE(6,'(2i4)') k,kc
c        if(kc.eq.1) WRITE(6,'(7(1pe10.3))') (tjc(k,m),m=1,npt)
        if(kc.ne.k.and.ila.eq.0) write(6,*)' PB lecture des I- continus'
        if(kc.ne.k.and.ila.eq.1) write(6,*)' PB reading continuum I-'
        enddo
                if(isuite.ge.3) then
        READ(26,*)
        READ(26,*)
        ierr=0
        do mm=1,npt
        READ(26,*) m
        if(mm.ne.m) ierr=ierr+1
        do k=1,nraimax
        READ(26,'(8(1pe10.3))') hkr,(rimoins(lf,k,m),lf=1,nf+1)
        kr=int(hkr)                                                          !m
c        if(kr.eq.2) WRITE(6,'(2i4)') mm,m,k,kr
c        if(kr.eq.2) WRITE(6,'(7(1pe10.3))') (rimoins(l,kr,m),l=1,nf+1)
c        if(kr.ne.k) ierr=ierr+1
        enddo   ! k
        if(m.eq.1) WRITE(6,*)'m  I- pour lf=1 kr=2,200,400,600,nraimax'
        if(m.eq.1.or.mod(m,10).eq.0.or.m.eq.npt)
     &  WRITE(6,'(i4,1p3e12.4)') m,rimoins(1,2,m),rimoins(1,200,m),
     &  rimoins(1,400,m),rimoins(1,600,m),rimoins(1,nraimax,m)
        enddo   ! mm
        if(ierr.ne.0.and.ila.eq.0) write(6,*)' PB lecture des I- raies'
        if(ifich.ge.1.and.ifich.le.8.and.ierr.ne.0.and.ila.eq.0)
     &  write(15,*)' PB lecture des I- raies'
        if(ierr.ne.0.and.ila.eq.1) write(6,*)' PB reading line I-'
        if(ifich.ge.1.and.ifich.le.8.and.ierr.ne.0.and.ila.eq.1)
     &  write(15,*)' PB reading line I-'
                endif        ! isuite=>3
                endif        ! isuite=>2
        CLOSE(8)
        if(incid.le.14) CLOSE(12)
        CLOSE(13)
        CLOSE(26)
        CLOSE(27)
c
! debut des ECRITURES - PRINTING
c parametre d'ionisation initial  csit=Lbol/nht*R**2=4pi*Ftotal/nht
        if(igrid.lt.10.and.igrid.ne.2.and.idens.le.1) then
                if(idens.eq.0) then
                   htot=coltot/densinit
                else if(dendex.eq.1.d0) then
                   htot=Raymin*(expa(coltot/densinit/Raymin)-1.d0)
                else
                   htot=Raymin *(1.d0-coltot*(dendex-1.d0)/
     &                  densinit/Raymin)**(-1.d0/(dendex-1.d0))
                endif   ! dendex
        endif   ! igrid idens
        if(Raymin.gt.0.d0) then
           hLbol=csit*densinit*Raymin*Raymin
           raymax=raymin + htot
           Rayminrg=Raymin/hLbol*1.d44*fedd /2.27094d+11                        
           Raymaxrg=Raymax/hLbol*1.d44*fedd /2.27094d+11
        endif
! epaisseur optique Thomson - electron scattering optical depth
c tauth=6.65e-25*integral of  nht(R)dR *about 1.18 (Ne)
        tauth=coltot*6.652d-25 *(1.01d0+ab(2)*2.d0)
        ftauth=1.d0 + tauth/(1.d0+tauth)
        fii=ftauth-0.9d0
        if(fii.gt.1.d0) fii=1.d0
        nht=densinit
c definition de fluxex0 et/ou csit
c        Ftotinit=hLbol/(pi4*raymin**2)
c        csit=Ftotinit*pi4/densinit                                          !m
                          if(incid.eq.2) then
        Ftotinit=5.6692d-5*tbb*tbb*tbb*tbb*wbb           ! sigma*T4 = piB
        csit=Ftotinit*pi4/densinit
        fluxex0=FFLUXBB(13.6d0)
                          else if(flym.eq.0.d0) then
        Ftotinit=csit*densinit/pi4
        if(incid.eq.0) csit=0.d0
        if(incid.eq.0) fluxex0=0.d0
        if(incid.eq.1) fluxex0=FFLUXLP(0.d0)
        if(incid.eq.3) fluxex0=FFLUX(0.d0)
        if(incid.eq.4) fluxex0=FFLUX1(0.d0)
        if(incid.eq.5) fluxex0=FFLUX2(0.d0)
        if(incid.eq.6) fluxex0=FFLUX3(0.d0)
        if(incid.eq.7) fluxex0=FFLUX4(0.d0)
        if(incid.eq.8) fluxex0=FFLUX5(0.d0)
        if(incid.eq.9) fluxex0=FFLUX6(13.6d0)
        if(incid.eq.10)fluxex0=FFLUX7(0.d0)
        if(incid.eq.11.or.incid.eq.12.or.incid.eq.13.or.incid.eq.14
     &     .or.incid.eq.15.or.incid.eq.16)
     &                 fluxex0=1.d0
        if(incid.eq.17) fluxex0=FFLUXECO1LP(0.d0)              !Agata
        if(incid.eq.18) fluxex0=FFLUXECO2LP(0.d0)              !Agata
c
                          else if(csit.eq.0.d0) then
        if(incid.le.8) then
           fluxex0=flym
           if(incid.eq.1) Ftotinit=fluxex0*3.2893d15*log(hnumax/hnumin)
           if(incid.eq.3) Ftotinit=fluxex0*2.41797d14*58.87576
           if(incid.eq.4) Ftotinit=fluxex0*2.41797d14*78.454298d0
           if(incid.eq.5) Ftotinit=fluxex0*2.41797d14*58.288143d0
           if(incid.eq.6) Ftotinit=fluxex0*3.2893d15*9.25236d0
           if(incid.eq.7) Ftotinit=fluxex0*3.2893d15*11.7533d0
           if(incid.eq.8) Ftotinit=fluxex0*2.41797d14*65.9316113d0
           csit=Ftotinit*pi4/densinit                                        !m
        else if(incid.eq.10) then
           Ftotinit=1.d0
           f1=FFLUX7(0.d0)
           Ftotinit=flym/f1
           cpl=cpl *flym/f1
           csit=Ftotinit*pi4/densinit
           fluxex0=flym
        else if(incid.eq.9.or.incid.eq.17) then
           Ftotinit=1.d0
           f1=FFLUXECO1LP(0.d0) 
           Ftotinit=flym/f1
           csit=Ftotinit*pi4/densinit
           fluxex0=flym
        else if(incid.eq.15.or.incid.eq.16) then
           fluxex0=1.d0
        endif
                            endif   ! csit/flym
c
c calcul de tableaux de SECTIONS de PHOTOIONISATION pour les raies et le
c continu, et des taux de photoionisation par le flux incident exterieur
! calculation of tables with photoionisation sections for lines and
! continuum, and photoionisation rates by incident flux
        CALL TABSIG
c
        if(incid.eq.15.or.incid.eq.16) CALL LECTINCID
c
c        write(6,'(i4,0pf10.3,1pe10.3)') (k,thnuc(k),exflux(k),k=1,10)
        tflincident=0.d0
        flincidion=0.d0
        photincidion=0.d0
!LC modified to compute also (Fx,Ux) and (Fox,Uox) instead of (F,Fion,U) only
        flincid_ux=0.d0
        photincid_ux=0.d0
        flincid_uox=0.d0
        photincid_uox=0.d0
!/LC
        flarriere=0.d0
                        do kc=1,322
        if(isuite.le.1) ttaucef(kc)=1000.d0
        if(kc.le.102) then
           dhnu=tdeb(kc)
           else        if(kc.le.220) then
           dhnu=tde(kc)
           else
           dhnu=tde(kc-220)-tdeb(kc-220)
        endif   ! kc
           dhnu1=dhnu
           if(kc.le.220.and.kc.ge.185) then
c???             if((incid.eq.1.or.incid.eq.3.or.incid.eq.17.or.incid.eq.18)
             if((incid.eq.1.or.incid.eq.3)
     &       .and.thnuc(kc)+tdeb(kc).gt.hnumin.and.thnuc(kc)-tde(kc)+
     &       tdeb(kc).lt.hnumin) dhnu1=thnuc(kc)+tdeb(kc) - hnumin      !Agata
           endif
        tflincident=tflincident + exflux(kc)*dhnu1
       if(thnuc(kc).ge.13.6d0.and.thnuc(kc).le.1.36d4.and.kc.ne.221)then
           flincidion=flincidion + exflux(kc)*dhnu 
           photincidion=photincidion + exflux(kc)*dhnu/thnuc(kc)
           endif
!LC modified to compute also (Fx,Ux) and (Fox,Uox) instead of (F,Fion,U) only
! dhnu2 is needed because kc grid does not include 100eV. Closest energy 
! is 104.0eV (kc=119) The factor 1.30422 s'arrange pour commencer a 100ev
      if(thnuc(kc).ge.100.d0.and.thnuc(kc).le.1.d4)then
           dhnu2=dhnu
           if(kc.eq.119) dhnu2=dhnu*1.30422d0
           flincid_ux=flincid_ux + exflux(kc)*dhnu2
           photincid_ux=photincid_ux + exflux(kc)*dhnu2/thnuc(kc)
      endif
      if(thnuc(kc).ge.540.d0.and.thnuc(kc).le.1.d4)then
           flincid_uox=flincid_uox + exflux(kc)*dhnu
           photincid_uox=photincid_uox + exflux(kc)*dhnu/thnuc(kc)
      endif
!/LC
        him(kc)=0.d0
        debim(kc)=0.d0
        if(irent.ne.0) then
           hnuev=thnuc(kc)
c au contraire des autres,ffluxbb (& wi & bbt) ne donne pas des flux mais B ->I
! fflux provides flux, except ffluxbb (& wi & bbt) which provides B ->I
           if(irent.le.3) him(kc)=FFLUXBB(hnuev)
           if(irent.eq.4) then
              x=hnuev/Tbb/8.6171d-5                            
         him(kc)=FFLUXBB(hnuev)*x*3.d0/(1-expa(-x))/16.d0
              endif   ! irent=4
           if(irent.eq.5) him(kc)=FFLUXWI(hnuev)
           if(irent.eq.6) him(kc)=exflux(kc)/pi2v3 *wbb
           if(tdebut.gt.0.d0) debim(kc)=FFLUXBBT(hnuev,tdebut)
           flarriere=flarriere + him(kc)*dhnu
           endif   ! irent
                        enddo   ! kc                                         !m
        if(px26.gt.0.d0) flxdurincid=px26*tflincident *esh
        tflincident=tflincident *esh + flxdurincid     ! esh=2.41797d14: ev->nu
        flincidion=flincidion *esh
        photincidion=photincidion/6.6262d-27
        u=photincidion/densinit/2.997925d10
!LC modified to compute also (Fx,Ux) and (Fox,Uox) instead of (F,Fion,U) only
        flincid_ux=flincid_ux *esh
        photincid_ux=photincid_ux/6.6262d-27
        u_x=photincid_ux/densinit/2.997925d10
        flincid_uox=flincid_uox *esh
        photincid_uox=photincid_uox/6.6262d-27
        u_ox=photincid_uox/densinit/2.997925d10
!/LC
       flarriere=flarriere*pi *esh
              if(incid.eq.11.or.incid.eq.13.or.incid.eq.14) then
              Ftotinit=tflincident
              csit=tflincident*pi4/densinit
              endif   ! incid=11,13 ou 14
       if(incid.eq.10.or.incid.eq.12.or.incid.eq.15.or.incid.eq.16)then
                fluxex0=csit*densinit/(tflincident*pi4)
                tflincident=tflincident *fluxex0
                flincidion=flincidion *fluxex0
                flxdurincid=flxdurincid *fluxex0
                photincidion=photincidion *fluxex0
                u=u *fluxex0
                Ftotinit=tflincident
                flincid_ux=flincid_ux *fluxex0
                photincid_ux=photincid_ux *fluxex0
                u_x=u_x *fluxex0
                flincid_uox=flincid_uox *fluxex0
                photincid_uox=photincid_uox/6.6262d-27
                u_ox=u_ox *fluxex0
                scomptx1=scomptx1 *fluxex0
                scomptx2=scomptx2 *fluxex0
                WRITE(10,*)' multiplie par ',fluxex0
                WRITE(10,*)'  kc     hnu(ev)    F_nu'
                  do kc=1,220
                     exflux(kc)=exflux(kc)*fluxex0
                WRITE(10,'(i5,0pf12.3,1pe12.3)')kc,thnuc(kc),exflux(kc)
                  enddo
                  do kc=221,322
                     exflux(kc)=exflux(kc)*fluxex0
        WRITE(10,'(i5,0pf12.3,1pe12.3)')kc,thnuc(kc)*0.9999d0,exflux(kc)
                  enddo
                  if(incid.eq.15.or.incid.eq.16)then
                     do nij=1,nivtots
                        exphot(nij)=exphot(nij)*fluxex0
                        exphotk(nij)=exphotk(nij)*fluxex0
                     enddo
                  endif
                endif   ! incid=10,12,15,16
c
        OPEN(UNIT=14,FILE=fires,STATUS='new')
        OPEN(UNIT=18,FILE=fite2,STATUS='new')
        OPEN(UNIT=30,FILE=fispe,STATUS='new')
        OPEN(UNIT=32,FILE=fide2,STATUS='new')
            if(ndir.ge.2) OPEN(UNIT=34,FILE=fian2,STATUS='new')
            if(ifich.ge.1.and.ifich.le.8)
     &  OPEN(UNIT=17,FILE=fitem,STATUS='new')
            if(ifich.ge.1.and.ifich.le.8)
     &  OPEN(UNIT=19,FILE=fiflu,STATUS='new')
            if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &  OPEN(UNIT=23,FILE=fibil,STATUS='new')
            if(ifich.ge.1.and.ifich.le.8.and.idens.ge.8)
     &  OPEN(UNIT=35,FILE=fipre,STATUS='new')
            if(ifich.ge.1.and.ifich.le.8.and.ndir.ge.2)
     &  OPEN(UNIT=33,FILE=fiang,STATUS='new')
            if(ifich.ge.3.and.ifich.le.8)
     &  OPEN(UNIT=16,FILE=fitx2,STATUS='new')
            if(ifich.ge.3.and.ifich.le.8)
     &  OPEN(UNIT=20,FILE=fifl2,STATUS='new')
            if((ifich.ge.4.and.ifich.le.8).or.ifich.eq.9)
     &  OPEN(UNIT=21,FILE=fitr2,STATUS='new')
            if(ifich.ge.5.and.ifich.le.8)
     &  OPEN(UNIT=29,FILE=fiali,STATUS='new')
            if(ifich.ge.6.and.ifich.le.8)
     &  OPEN(UNIT=31,FILE=fipge,STATUS='new')
            if(ifich.ge.7.and.ifich.le.8)
     &  OPEN(UNIT=40,FILE=fipgt,STATUS='new')
        aaprof=fprof(1000,1.d-5,x,cx)
! fff=F(13.6eV) to be printed in all headers
!        fff=fluxex0
!        if (incid.ne.1) fff=exflux(1)
!
        if(iexpert.eq.1) write(14,*)'passage expert'
                  if(ila.eq.0) then
        CALL sECRITUREf
                  else if(ila.eq.1.and.iexpert.eq.1) then
        CALL sECRITUREe
                  else 
        CALL sECRITUREp
                  endif
                  if(ila.eq.0) then
        WRITE(14,99) coltot,tauth
        WRITE(30,99) coltot,tauth
          if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,99) coltot,tauth
        WRITE(17,99) coltot,tauth
        WRITE(19,99) coltot,tauth
        WRITE(23,99) coltot,tauth
          endif
        WRITE(14,600) tflincident,flxdurincid
        WRITE(14,598) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
        WRITE(30,600) tflincident,flxdurincid
        WRITE(30,598) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
           if(ifich.ne.0.and.ifich.le.8) then
        WRITE(15,600) tflincident,flxdurincid
        WRITE(17,600) tflincident,flxdurincid
        WRITE(19,600) tflincident,flxdurincid
        WRITE(23,600) tflincident,flxdurincid
        WRITE(15,598) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
        WRITE(17,598) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
        WRITE(19,598) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
        WRITE(23,598) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
          endif
c
        if(irent.ge.1) then
           flth=5.6692d-5*tbb*tbb*tbb*tbb*wbb
           if(irent.eq.6) flth=0.d0
        WRITE(30,92) flarriere,flth,tdebut
        WRITE(14,92) flarriere,flth,tdebut
          if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,92) flarriere,flth,tdebut
        WRITE(17,92) flarriere,flth,tdebut
        WRITE(19,92) flarriere,flth,tdebut
        WRITE(23,92) flarriere,flth,tdebut
          endif
        endif
                  else   ! ila=1 (english)
        WRITE(14,999) coltot,tauth
        WRITE(30,999) coltot,tauth
           if(iexpert.eq.1.and.ifich.ne.0.and.ifich.le.8) then
        WRITE(15,999) coltot,tauth
        WRITE(17,999) coltot,tauth
        WRITE(19,999) coltot,tauth
        WRITE(23,999) coltot,tauth
           endif
        WRITE(14,601) tflincident,flxdurincid
        WRITE(30,601) tflincident,flxdurincid
        WRITE(14,599) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
        WRITE(30,599) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
           if(iexpert.eq.1.and.ifich.ge.1.and.ifich.le.8) then
        WRITE(15,601) tflincident,flxdurincid
        WRITE(17,601) tflincident,flxdurincid
        WRITE(19,601) tflincident,flxdurincid
        WRITE(23,601) tflincident,flxdurincid
        WRITE(15,599) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
        WRITE(17,599) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
        WRITE(19,599) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
        WRITE(23,599) flincidion,flincid_ux,flincid_uox,
     &   u,u_x,u_ox,photincidion,photincid_ux,photincid_uox
           endif
        if(irent.ge.1) then
           flth=5.6692d-5*tbb*tbb*tbb*tbb*wbb
           if(irent.eq.6) flth=0.d0
        WRITE(14,992) flarriere,flth
c        WRITE(14,992) flarriere,flth,tdebut
        WRITE(30,992) flarriere,flth
        WRITE(14,*)
        WRITE(30,*)
           if(iexpert.eq.1.and.ifich.ge.1.and.ifich.le.8) then
        WRITE(15,992) flarriere,flth
        WRITE(17,992) flarriere,flth
        WRITE(19,992) flarriere,flth
        WRITE(23,992) flarriere,flth
        WRITE(15,*)
        WRITE(17,'(a)') '#'
        WRITE(19,*)
        WRITE(23,*)
           endif
        endif   ! irent
           if(ndir.ge.2.and.ifich.ge.1.and.ifich.le.8)
     &  write(33,'(a,a)') '# ',titre
                              endif   ! ila
 99   format(' colonne-densite=',1pe10.3,' tau thomson environ',1pg10.3)
 90   format(' flux incident: tot=',1pe10.3,' ionis=',1pe10.3, 
     &          ' X>26kev=',1pe10.3,' U=',1pe10.3)
 92   format(' flux rentrant arriere=',1pe10.3,
     &         ' w*sigmaTbb4*2/V3=',1pe10.3)
c     &         ' w*sigmaTbb4*2/V3=',1pe10.3,' Tdebut=',1pe10.3)
 999  format('# column-density=',1pe10.3,' tau thomson about',1pg10.3)
 990  format('# incident flux: tot=',1pe10.3,' ionis=',1pe10.3, 
     &          ' X>26kev=',1pe10.3,' U=',1pe10.3)
 992  format('# backward flux=',1pe10.3,
     &         ' w*sigmaTbb4*2/V3=',1pe10.3)
c     &         ' w*sigmaTbb4*2/V3=',1pe10.3,' Tdebut=',1pe10.3)
 600   format(' ILLUMINATION flux incident:'/' Ftot(0,+inf)=nH*csit/',  !234567
     &  '4pi=',1pe10.3,'    FXdur(26keV,+inf)=',1pe10.3)
 598   format(4x,'Fion,U,Q:(1Ryd,1000Ryd)/',
     & 'Fx,Ux,Qx:(100eV,10keV)/Fox,Uox,Qox:(540eV,10keV)'/
     & 4x,'F=flux:        Fion=',1pe10.3,' Fx=',1pe10.3,' Fox=',1pe10.3/
     & 4x,'U=photons/nH/c:   U=',1pe10.3,' Ux=',1pe10.3,' Uox=',1pe10.3/
     &4x,'Q=photons:        Q=',1pe10.3,' Qx=',1pe10.3,' Qox=',1pe10.3/)
 601   format('#'/'# INCIDENT RADIATION :'/'# Ftot(0,+inf)=nH*csit/',
     &  '4pi=',1pe10.3,'    FhardX(26keV,+inf)=',1pe10.3)
 599   format('#',3x,'Fion,U,Q:(1Ryd,1000Ryd)/Fx,Ux,Qx:(100eV,10keV)/',
     & 'Fox,Uox,Qox:(540eV,10keV)'/'#',3x,'F=flux:',8x,'Fion=',1pe10.3,
     & ' Fx=',1pe10.3,' Fox=',1pe10.3/'#',3x,'U=photons/nH/c:   U=',
     & 1pe10.3,' Ux=',1pe10.3,' Uox=',1pe10.3/'#',3x,'Q=photons:',
     & '        Q=',1pe10.3,' Qx=',1pe10.3,' Qox=',1pe10.3/'#')
        CLOSE(10)
        CLOSE(14)                                                            !m
        CLOSE(15)
        CLOSE(16)
        CLOSE(17)
        CLOSE(35)
        CLOSE(18)
        CLOSE(19)
        CLOSE(20)
        CLOSE(21)
        CLOSE(23)
        CLOSE(27)
        CLOSE(29)
        CLOSE(30)
        CLOSE(31)
        CLOSE(32)
        CLOSE(33)
        CLOSE(34)
        CLOSE(40)
c
            if(ifich.ge.2.and.ifich.le.8)
     &  OPEN(UNIT=24,FILE=fidio,STATUS='new')
        CLOSE(24)
            if(ifich.ge.4.and.ifich.le.8) then
        OPEN(UNIT=22,FILE=fitxa,STATUS='new')
        CLOSE(22)
            endif
            if(ifich.ge.10) then
        OPEN(UNIT=28,FILE=ficap,STATUS='new')
        CLOSE(28)
            endif
            if(isuite.ge.1) then
        OPEN(UNIT=25,FILE=fiime,STATUS='new')
        CLOSE(25)
            endif
c
                  kra=0                                                      !m
                do 21 j=1,nelem
                  iz0=iz(j)
        vthref=VTref/sqrt(pmat(j)) *1.2845d12        !10**8*V(2kTref/Mion)
                do 21 i=1,iz0
                kri=kra+1
                kra=kra+nrai(i,j)
                if(j.eq.10.and.i.eq.iz0) kra=kra+24
                do 22 kr=kri,kra
           fsor(1,kr)=0.d0
           frer(1,kr)=0.d0
           fsor(2,kr)=0.d0
           frer(2,kr)=0.d0
        uhz(kr)=vthref/alo(kr)                  ! dnu-dopplerthermique(Tref)
           if(vturbkm.gt.0.d0)                               ! TURBULENCE
     &     uhz(kr)=sqrt(vthref**2 + (vturbkm*0.5d13)**2)/alo(kr)
   22           continue
   21           continue
                do ku=1,nuta
                   j=juta(ku)
                   kr=nraissfek+24 + ku
           fsor(1,kr)=0.d0
           frer(1,kr)=0.d0
           fsor(2,kr)=0.d0
           frer(2,kr)=0.d0
           vthref=VTref/sqrt(pmat(j)) *1.2845d12        !10**8*V(2kTref/Mion)
           uhz(kr)=vthref/alo(kr)                       ! dnuthermique(Tref)
                   if(vturbkm.gt.0.d0)                       ! TURBULENCE
     &     uhz(kr)=sqrt(vthref**2 + (vturbkm*0.5d13)**2)/alo(kr)
                enddo
                if(inr.eq.2) then
                   do ks=1,ns
                      j=js(ks)
                      kr=nraissfek+24+nuta + ks
           fsor(1,kr)=0.d0
           frer(1,kr)=0.d0
           fsor(2,kr)=0.d0
           frer(2,kr)=0.d0
           vthref=VTref/sqrt(pmat(j)) *1.2845d12        !10**8*V(2kTref/Mion)
           uhz(kr)=vthref/alo(kr)                       ! dnuthermique(Tref)
                      if(vturbkm.gt.0.d0)                    ! TURBULENCE
     &     uhz(kr)=sqrt(vthref**2 + (vturbkm*0.5d13)**2)/alo(kr)
                   enddo
                endif
c
        nitf=0                                                               !m
        itali=1
c                if(nitf.eq.1) itali=0 dans sSTRATE uniquement
c debut de l'ITERATION SUR LES FLUX ou J - beginning of the MAIN iteration
30000        continue
        nitf=nitf+1
        if(nitf.ge.nfmax) kitf=1
        kimpf=0
c     pour changer le nombre des dernieres iterations ecrites 
c        if(nimpff.lt.0.and.nitf.ge.nfmax-10.and.nitf.le.nfmax) kimpf=1
        if(nimpff.lt.0.and.nitf.ge.nfmax-15.and.nitf.le.nfmax) kimpf=1
        if(mod(nitf,nimpf).eq.0.or.kitf.eq.1.or.nitf.eq.1) kimpf=1
        if(nitf.eq.10.or.nitf.eq.20.or.nitf.eq.30) kimpf=1
c        if(nitf.ge.70.and.nitf.le.79) kimpf=1
        kimpf2=0
        if(mod(nitf,nimpf2).eq.0.or.kitf.eq.1.or.nitf.eq.1) kimpf2=1
c        if(nitf.eq.2.or.nitf.eq.5.or.nitf.eq.10.or.nitf.eq.20.or.
c     &     nitf.eq.30) kimpf2=1
            if(kitf.eq.1)
     &  OPEN(UNIT=14,FILE=fires,STATUS='old',position='append')       !2345678
            if(ifich.ge.1.and.ifich.le.8..and.iexpert.eq.1)
     &  OPEN(UNIT=15,FILE=fitxt,STATUS='old',position='append')
            if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &  OPEN(UNIT=23,FILE=fibil,STATUS='old',position='append')
            if(ifich.ge.3.and.ifich.le.8.and.(kimpf2.eq.1.or.imp.eq.4))
     &  OPEN(UNIT=16,FILE=fitx2,STATUS='old')
        if(kimpf2.eq.1.and.((ifich.ge.4.and.ifich.le.8).or.ifich.eq.9))
     &  OPEN(UNIT=21,FILE=fitr2,STATUS='old')
            if(ifich.ge.5.and.ifich.le.8)
     &  OPEN(UNIT=29,FILE=fiali,STATUS='old',position='append')
c        if(nitf.le.3) OPEN(UNIT=32,FILE=fide2,STATUS='old')
            if(mod(nitf,5).eq.0.or.nitf.eq.1.or.kitf.eq.1) then
        OPEN(UNIT=18,FILE=fite2,STATUS='old')
        OPEN(UNIT=32,FILE=fide2,STATUS='old')
            if(ifich.ge.3.and.ifich.le.8)
     &  OPEN(UNIT=20,FILE=fifl2,STATUS='old')
                endif
            if(ndir.ge.2.and.kimpf.eq.1)
     &  OPEN(UNIT=34,FILE=fian2,STATUS='old')
                if(kimpf.eq.1.and.ifich.le.8) then                           !m
            if(ifich.ne.0) then
        OPEN(UNIT=17,FILE=fitem,STATUS='old',position='append')
        OPEN(UNIT=19,FILE=fiflu,STATUS='old',position='append')
            if(ndir.ge.2)
     &  OPEN(UNIT=33,FILE=fiang,STATUS='old',position='append')
            endif
            if(idens.ge.8.and.ifich.ne.0)
     &  OPEN(UNIT=35,FILE=fipre,STATUS='old',position='append')
            if(ifich.ge.2)
     &  OPEN(UNIT=24,FILE=fidio,STATUS='old',position='append')
            if(ifich.ge.6)
     &  OPEN(UNIT=31,FILE=fipge,STATUS='old',position='append')
c            if(ifich.ge.7)
c     &  OPEN (UNIT=40,FILE=fipgt,STATUS='old')
            if(ifich.ge.7)
     &  OPEN(UNIT=40,FILE=fipgt,STATUS='old',position='append')
            if(ifich.ge.4)
     &  OPEN(UNIT=22,FILE=fitxa,STATUS='old',position='append')
                endif   ! kimpf=1
                if(ifich.ge.10.and.kimpf.eq.1)
     &  OPEN(UNIT=28,FILE=ficap,STATUS='old')
                if(kitf.eq.1)
     &  OPEN(UNIT=30,FILE=fispe,STATUS='old',position='append')
c        if(nitf.eq.1) write(6,*)
c
                     if(ila.eq.0) then
        write(6,*) ' ITERATION SUR LES J no',nitf
        if(kitf.eq.1) write(14,*) ' ITERATION SUR LES J no',nitf
        if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &  write(15,*) ' ITERATION SUR LES J no',nitf
                if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) then
        write(17,*) ' ITERATION SUR LES J no',nitf,'            ;',nitf
        write(19,*) ' ITERATION SUR LES J no',nitf,'            ;',nitf
                endif
        if(kitf.eq.1) WRITE(30,*) ' ITERATION SUR LES J no',nitf
                     else   ! ila=1 (english)
        write(6,*) ' MAIN ITERATION no',nitf
        if(kitf.eq.1) write(14,199) nitf
        if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &  write(15,*) ' MAIN ITERATION no',nitf                                !m
                if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) then
        if(iexpert.lt.1) write(17,'(a,a)') '# ',titre
        if(iexpert.lt.1) write(19,*) titre
        write(17,199) nitf
        write(19,199) nitf
        write(17,'(a)') '# '
        write(19,*)
                endif
            if(mod(nitf,5).eq.0.or.nitf.eq.1.or.kitf.eq.1) then
        if(iexpert.lt.1) write(18,'(a,a)') '# ',titre
        if(iexpert.lt.1) write(32,'(a,a)') '# ',titre
        write(18,199) nitf
        write(32,199) nitf
            endif
            if(ndir.ge.2.and.kimpf.eq.1) then
        write(34,'(a,a)') '# ',titre
        write(34,199) nitf
         if(ifich.ne.0.and.ifich.le.8) then
        write(33,'(a)') '#'
        write(33,199) nitf
         endif
            endif
        if(kitf.eq.1) WRITE(30,199) nitf
 199    format('# MAIN ITERATION no',i5)
                     endif   ! ila

        if(coltot.gt.0.d0) coltotlog=log10(coltot)
        if(csit.gt.0.d0) csitlog=log10(csit)
                if(idens.ge.2) then
           txt='Pcst'
                else if(itemp.ge.10) then
           txt='Timp'
                else if(idens.le.1) then
           txt='Ncst'
                endif
c                if(nitf.eq.1)
c     &  WRITE(6,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
c     &               flindex,hnumin,hnumax,incid,fii,ijour
           if(ifich.ge.1.and.ifich.le.8.and.idens.ge.8.and.kimpf.eq.1)
     &  WRITE(35,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
     &               flindex,hnumin,hnumax,incid,ijour
                if(ifich.ge.4.and.ifich.le.8.and.kimpf.eq.1)
     &  WRITE(22,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
     &               flindex,hnumin,hnumax,incid,ijour
c                if(nitf.le.3)
c     &  WRITE(32,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
c     &               flindex,hnumin,hnumax,incid,fii,ijour
            if(iexpert.eq.1) then
          if(mod(nitf,5).eq.0.or.nitf.eq.1.or.kitf.eq.1) then
        WRITE(18,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
     &               flindex,hnumin,hnumax,incid,ijour
        WRITE(32,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
     &               flindex,hnumin,hnumax,incid,ijour
          if(ifich.ge.3.and.ifich.le.8)
     &  WRITE(20,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
     &               flindex,hnumin,hnumax,incid,ijour
            endif
                endif
                if(nitf.eq.1.and.ifich.ge.5.and.ifich.le.8)
     &  WRITE(29,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
     &               flindex,hnumin,hnumax,incid,ijour
                if(kimpf2.eq.1.and.ifich.ge.3.and.ifich.le.8)
     &  WRITE(16,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
     &               flindex,hnumin,hnumax,incid,ijour
                if(kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.9) then
           if(ifich.ne.9) then
        write(21,*)
        write(21,*) ' les fint(lf)'
        write(21,'(8f9.3/4f9.3,4f10.2)') (fint(lf),lf=1,nf)
           endif
        WRITE(21,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
     &               flindex,hnumin,hnumax,incid,ijour
        if(ifich.eq.9)write(21,*)
     & ' m,tcol(m)/hnuev,kr,dhaut*V3*capal0,dhaut*V3*(capabs+cthomson)'
                endif
        if(kimpf.eq.1.and.nitf.ne.1.and.ifich.ge.2.and.ifich.le.8)
     &  WRITE(24,96) nitf,log10(densinit),txt,idens,coltotlog,csitlog,
     &               flindex,hnumin,hnumax,incid,ijour
                if(kimpf.eq.1) then
        if(ifich.ge.10) WRITE(28,96) nitf,log10(densinit),
     & txt,idens,coltotlog,csitlog,flindex,hnumin,hnumax,incid,ijour
        if(ifich.ge.6.and.ifich.le.8) WRITE(31,96)nitf,log10(densinit),
     & txt,idens,coltotlog,csitlog,flindex,hnumin,hnumax,incid,ijour
        if(ifich.ge.6.and.ifich.le.8) WRITE(31,196)
        if(ifich.ge.7.and.ifich.le.8) then
           WRITE(40,*)
           if(ifich.eq.7.and.ila.eq.0)WRITE(40,*)
     &                        ' pertes & gains=f(T), n cst (ifich=7)'
           if(ifich.eq.8.and.ila.eq.0)WRITE(40,*)
     &                        ' pertes & gains=f(T), nT cst (ifich=8)'
           if(ifich.eq.7.and.ila.eq.1)WRITE(40,'(a)')
     &          '# cooling and heating=f(T), density cste (ifich=7)'
           if(ifich.eq.8.and.ila.eq.1)WRITE(40,'(a)')
     &          '# cooling and heating=f(T), density*T cste (ifich=8)'
           WRITE(40,96) nitf,log10(densinit),txt,idens,
     &      coltotlog,csitlog,flindex,hnumin,hnumax,incid,ijour
        endif
        if(ndir.ge.2.and.ifich.ne.0.and.ifich.le.8)then
           if(iexpert.eq.1) WRITE(33,96) nitf,log10(densinit),txt,
     &        idens,coltotlog,csitlog,flindex,hnumin,hnumax,incid,ijour
           if(nitf.eq.1) then
              WRITE(33,198) ndir
c           WRITE(33,197) (csz(i),i=1,ndir)
              if(ndirlu.eq.3) WRITE(33,'(a/a/a)')
     &       '#    angles  27.5  60  83.5 degres',
     &       '#    cosinus= 0.8873  0.5  0.1127',
     &       '#    w=deltaOmega/4pi= 0.13889  0.22222  0.13889'
              if(ndirlu.eq.103) WRITE(33,'(a/a/a)')
     &       '#    angles 7 minutes  27.5  60  83.5 degres',
     &       '#    cosinus= 1  0.8873  0.5  0.1127',
     &       '#    w=deltaOmega/4pi= 1e-6  0.13889  0.22222  0.13889'
              if(ndirlu.eq.5) WRITE(33,'(a/a/a)')
     &       '#    angles 17.6  39.7  60  76.7  87.3 degres',
     &       '#    cosinus= 0.9531  0.7692  0.5  0.2308  0.0469',
     &       '#    w=deltaOmega/4pi= 0.05923  0.11966  0.14222  '//
     &          '0.11966  0.05923'
              if(ndirlu.eq.105) WRITE(33,'(a/a/a)')
     &       '#    angles 7 minutes  17.6  39.7  60  76.7  87.3 degres',
     &       '#    cosinus= 1  0.9531  0.7692  0.5  0.2308  0.0469',
     &       '#    w=deltaOmega/4pi= 1e-6  0.05923  0.11966  0.14222'//
     &         '  0.11966  0.05923'
              if(ndirlu.eq.205) WRITE(33,'(a/a/a)')
     &       '#    angles 7 minutes  25.8  45.6  60  72.5  84.3 degres',
     &       '#    cosinus= 1  0.9  0.7  0.5  0.3  0.1',
     &       '#    w=deltaOmega/4pi= 1e-6  0.1  0.1  0.1  0.1  0.1'
              if(ndirlu.eq.20) WRITE(33,'(a/a)')
     &       '#    angles 12.8,22.3,29.0,...,88.6 degres' ,
     &       '#    cosinus= 0.975 -> 0.025  w=deltaOmega/4pi= 0.025'
           endif   ! nitf=1
           WRITE(33,'(a)') '#'
           WRITE(33,'(a,a)') '# hnuev kc incid  refl1',
     &     ' refl2 refl3 refl4 refl5... out1 out2 out3 out4 out5...'
        endif
        if(ndir.ge.2) then
           if(iexpert.eq.1)WRITE(34,96) nitf,log10(densinit),txt,idens,
     &      coltotlog,csitlog,flindex,hnumin,hnumax,incid,ijour
           WRITE(34,198) ndir
c           WRITE(34,197) (csz(i),i=1,ndir)
           if(ndirlu.eq.3) WRITE(34,'(a/a/a)')
     &       '#    angles  27.5  60  83.5 degres',
     &       '#    cosinus= 0.8873  0.5  0.1127',
     &       '#    w=deltaOmega/4pi= 0.13889  0.22222  0.13889'
           if(ndirlu.eq.103) WRITE(34,'(a/a/a)')
     &       '#    angles 7 minutes  27.5  60  83.5 degres',
     &       '#    cosinus= 1  0.8873  0.5  0.1127',
     &       '#    w=deltaOmega/4pi= 1e-6  0.13889  0.22222  0.13889'
           if(ndirlu.eq.5) WRITE(34,'(a/a/a)')
     &       '#    angles 17.6  39.7  60  76.7  87.3 degres',
     &       '#    cosinus= 0.9531  0.7692  0.5  0.2308  0.0469',
     &       '#    w=deltaOmega/4pi= 0.05923  0.11966  0.14222  '//
     &          '0.11966  0.05923'
           if(ndirlu.eq.105) WRITE(34,'(a/a/a)')
     &       '#    angles 7 minutes  17.6  39.7  60  76.7  87.3 degres',
     &       '#    cosinus= 1  0.9531  0.7692  0.5  0.2308  0.0469',
     &       '#    w=deltaOmega/4pi= 1e-6  0.05923  0.11966  0.14222'//
     &          '  0.11966  0.05923'
           if(ndirlu.eq.205) WRITE(34,'(a/a/a)')
     &       '#    angles 7 minutes  25.8  45.6  60  72.5  84.3 degres',
     &       '#    cosinus= 1  0.9  0.7  0.5  0.3  0.1',
     &       '#    w=deltaOmega/4pi= 1e-6  0.1  0.1  0.1  0.1  0.1'
           if(ndirlu.eq.20) WRITE(34,'(a/a)')
     &       '#    angles 12.8,22.3,29.0,...,88.6 degres' ,
     &       '#    cosinus= 0.975 -> 0.025  w=deltaOmega/4pi= 0.025'
           WRITE(34,'(a)') '#'
           WRITE(34,'(a,a)') '# hnuev kc incid  refl1',
     &     ' refl2 refl3 refl4 refl5... out1 out2 out3 out4 out5...'
        endif
                endif   ! kimpf=1
cflux incident: tot=',1pe10.3, ionis=',1pe10.3, X>26kev= 2.456E+90 U= 2.456E+90
 96    format(/'#it',i3,' n',f4.1,a4,i1,' col',f4.1,' x',f4.2,'a',!TITAN106c
     & f3.1,'(',0pf5.2,1pe8.1,')inc',i2,' /v106c/',i2,i3,i5)
cit234 n12.0Pcst5 col25.4 x2.45a1.7(10.13 1.2e+05)inc12 /v105.03f/01 01 1998
cit234 n12.0Pcst5 col25.4 x2.45a1.7(10.13 1.2e+05)inc12 fii1.0 v105.01b/01011998
 196   format('# gains et pertes a l equilibre - heating and cooling ',
     &        'at equilibrium'/'# nitf m z col T nht gfree gion graie',
     &        ' gcompt gcomptx gcompts pfree precf ',
     &        'prsecdi pnetraip pcompt pertefluo praieint+pech benef')
 198   format('#'/'# Flux (CONTINUUM) versus',i3,' directions ',
     &         ': I*cosinus*w*4pi (erg cm-2 s-1 hz-1)')
 197   format('# cosinus=',7f10.7)
c
c initialisations pour l'aller - initializations
                do kc=1,322
        pciplus(kc)=exflux(kc)/pi
        ciplus(kc)=exflux(kc)/pi
c pb du pi2v3 pour un seul angle : harmoniser avec bc0-sortant-reflechi
        restflux(kc)=exflux(kc)
        exdtauc(kc,0)=1.d0
        rtaucef(kc)=0.d0
        rtaucat(kc)=0.d0
        do m=1,nzs
           temcac(kc,m)=0.d0
           tcapc(kc,m)=0.d0
           if(kc.eq.1) tcth(m)=0.d0
        enddo
                enddo        ! kc                                            !m
                do 100 kr=1,nraimax
        rtaur0(kr)=0.d0                                ! raie seule au centre
        rtaurl(kr)=0.d0                                ! raie seule dans l'aile
        twr(kr)=0.d0
        tws(kr)=0.d0
        hnuev=thnur(kr)
        kc=kcr(kr)
            do lf=1,nf+1
            priplus(lf,kr)=pciplus(kc)
            exdtaur(lf,kr,0)=1.d0                           ! raie seule
            enddo        ! lf
        cjn(kr)=pciplus(kc)*pi4 *alo(kr)/1.98648d-8        ! 4piJ/hnu
        ncomp(kr)=0
        mdebcomp(kr)=0
        pripdnuco(1,kr)=pciplus(kc)
        pripdnuco(2,kr)=pciplus(kc)
  100        continue   ! kr
        rtauthx=0.d0
c        rtaucmin=0.d0
        ttgcomptx=0.d0
        ttgpcompt=0.d0
        ttpraie=0.d0
        ttgraie=0.d0
        ttpgrai=0.d0
        ttpgray=0.d0                     !yyy
        ttpertefluo=0.d0
        ttpraieint=0.d0
        ttpech=0.d0
        ttpfree=0.d0
        ttprecf=0.d0
        ttprsecdi=0.d0
        ttgfree=0.d0
        ttgion=0.d0                                                          !m
        rsomtely=0.d0
        rsomely=0.d0
        ssomtely=0.d0
        ssomely=0.d0
c calcul de l'equilibre de temperature dans le nuage par iteration
! calculation of the temperature equilibrium in cloud by iteration
        CALL sNUAGE
c
c FIN DU NUAGE - CLOUD END
                if(mod(nitf,5).eq.0.or.kitf.eq.1) then
c        OPEN(UNIT=32,FILE=fide2,STATUS='old')
           if(ila.eq.0) then
        WRITE(32,*)
        WRITE(32,'(a78)') titre
        WRITE(32,*)
        WRITE(32,580) densinit,dendex,htot,csit,incid,nitf
        WRITE(32,581) npt,csit,flindex,hnumin,hnumax
        WRITE(32,582) 'ab=',(ab(j),j=1,nelem)
        WRITE(32,*)
        WRITE(32,682)
           else
           txt2='#'
        WRITE(32,'(a)') txt2//titre
        WRITE(32,'(a)') txt2
        WRITE(32,590) densinit,dendex,htot,csit,incid,nitf
        WRITE(32,591) txt2,npt,csit,flindex,hnumin,hnumax
        WRITE(32,592) '#ab=',(ab(j),j=1,nelem)
        WRITE(32,'(a)') txt2
        WRITE(32,683)                                                        !m
           endif
        do m=1,npt
           tauth=tcol(m)*(1.01d0+ab(2)*2.d0)*0.6652d-24
           WRITE(32,680) m,tab,tz(m),tab,tnht(m),tab,temp(m),tab,
     &                  tcol(m),tab,tauth,tab,(trij(m,k),tab,k=1,113)
        enddo
 590    format('# nht=',1pe9.2,'(-',f3.1,') htot=',1pe9.2,' csit=',
     &  1pe9.2,' incid=',i2,i5,' iterations'/'#npt,csit,flindex,hnumin',!234567
     &  ',hnumax then abund')
 591    format(a,i3,5(1pe10.3))
 592    format(a4,20(1pe10.3))
 682    format('m;z;nht;T;col;tauth;H1;H2;He1;He2;He3;C1;C2;C3;C4;C5;', 
     &  'C6;C7;N1;N2;N3;N4;N5;N6;N7;N8;O1;O2;O3;O4;O5;O6;O7;O8;O9;Ne1;',
     &  'Ne2;Ne3;Ne4;Ne5;Ne6;Ne7;Ne8;Ne9;Ne10;Ne11;Mg1;Mg2;Mg3;Mg4;',
     &  'Mg5;Mg6;Mg7;Mg8;Mg9;Mg10;Mg11;Mg12;Mg13;Si1;Si2;Si3;Si4;Si5;',
     &  'Si6;Si7;Si8;Si9;Si10;Si11;Si12;Si13;Si14;Si15;S1;S2;S3;S4;S5;',
     &  'S6;S7;S8;S9;S10;S11;S12;S13;S14;S15;S16;S17;Fe1;Fe2;Fe3;Fe4;',
     &  'Fe5;Fe6;Fe7;Fe8;Fe9;Fe10;Fe11;Fe12;Fe13;Fe14;Fe15;Fe16;Fe17;',
     &  'Fe18;Fe19;Fe20;Fe21;Fe22;Fe23;Fe24;Fe25;Fe26;Fe27;He+niv2;')
 683   format('# m z nht T col tauth H1 H2 He1 He2 He3 C1 C2 C3 C4 C5 ',
     &  'C6 C7 N1 N2 N3 N4 N5 N6 N7 N8 O1 O2 O3 O4 O5 O6 O7 O8 O9 Ne1 ',
     &  'Ne2 Ne3 Ne4 Ne5 Ne6 Ne7 Ne8 Ne9 Ne10 Ne11 Mg1 Mg2 Mg3 Mg4 ',
     &  'Mg5 Mg6 Mg7 Mg8 Mg9 Mg10 Mg11 Mg12 Mg13 Si1 Si2 Si3 Si4 Si5 ',
     &  'Si6 Si7 Si8 Si9 Si10 Si11 Si12 Si13 Si14 Si15 S1 S2 S3 S4 S5 ',
     &  'S6 S7 S8 S9 S10 S11 S12 S13 S14 S15 S16 S17 Fe1 Fe2 Fe3 Fe4 ',
     &  'Fe5 Fe6 Fe7 Fe8 Fe9 Fe10 Fe11 Fe12 Fe13 Fe14 Fe15 Fe16 Fe17 ',
     &  'Fe18 Fe19 Fe20 Fe21 Fe22 Fe23 Fe24 Fe25 Fe26 Fe27 He+niv2 ')
 680   format(i4,a,2(1pe10.3,a),1pe12.5,a,1pe15.8,a,114(1pe10.3,a))
c 680   format(i4,a,118(1pe10.3,a))
                endif   ! nitf
c
           if(nitf.gt.1) then
        Tmilp=Tmil
        tolyp=toly
        cim1p=cim1
        tflcrep=tflcontref 
        tflcsop=tflcontsor
        pttgpcompt=ttgpcompt
        tflraiep=tflraie                                                     !m
        ptflpartant=tflpartant
           endif
        Tmil=temp(npt/2)
        toly=rtaucef(1)
c
c RETOUR : fixation du I-(H) - defining backward I
                        if(nfin.eq.npt) then
              do kc=1,322
                 if(irent.eq.0.or.irent.eq.1.or.irent.ge.5) then
        cimoinsh(kc)=him(kc)
                 else if(irent.eq.3) then
        cimoinsh(kc)=him(kc) - ciplush(kc)
                 else    ! irent=2 ou 4
        cimoinsh(kc)=ciplush(kc) + him(kc)
                 endif   ! irent
              enddo        ! kc
              do kr=1,nraimax
                    kc=kcr(kr)
                 if(irent.eq.0.or.irent.eq.1.or.irent.ge.5) then
                    far=him(kc)
                 else if(irent.eq.3) then
                    far=him(kc) - ciplush(kc)
                 else    ! irent=2 ou 4
                    far=ciplush(kc) + him(kc)
                 endif   ! irent
           do lf=1,nf+1
        rimoins(lf,kr,npt+1)=far
           enddo        ! lf
        rimdnuco(1,kr,npt+1)=far
        rimdnuco(2,kr,npt+1)=far
c        if(kr.le.5) write(6,*) kr,hnuev,rimoins(1,kr,npt+1)
              enddo        ! kr
c
                        else if(irent.eq.0) then   ! nfin<npt
                 do m=nfin+1,npt
              do kr=1,nrais
           do lf=1,nf+1
        rimoins(lf,kr,m)=0.d0
           enddo        ! lf
        rimdnuco(1,kr,npt+1)=0.d0
        rimdnuco(2,kr,npt+1)=0.d0
c        if(kr.le.322) cimoins(kr,m)=0.d0     ! cimoins n'existe plus
              enddo        ! kr
                 enddo        ! m
                        else
        write(6,*) ' PROBLEME DE CONVERGENCE - STOP'
        if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &  write(15,*) ' PROBLEME DE CONVERGENCE - STOP'
        stop
                        endif        ! nfin
c reinitialisation des tau pour les precsec et pdiel
                        do k=1,322
        ttaucef(k)=rtaucef(k)
                        enddo
         cim1=cimoins0(1)
c
        ecarim=0.d0
        do m=1,nzs
           mbos(m)=0
        enddo
c
c TRANSFERT :
c ancien: calcul de I-(z) en partant de I-(H) (nomme tc2dir et tr2dir)
c (pour les raies kali=1 ou si itali=0)
c ou ALI: J est calcule a partir des emm et kappa dans les subroutines Paletou
c (itali=1): ALI-simple (kali=2) ou MALI (kali=3)
! TRANSFER :
! old transfer: calculation of I-(z) forward from I-(H) (named tc2dir & tr2dir)
! for lines kali=1
! or ALI: J is computed from emm and kappa with subroutines of Paletou :
! simple-ALI (kali=2) or MALI (kali=3)
c
c       if(itali.eq.0) call tc2dir               ! revoir si utilisation
c
c TRANSFERT Paletou  pour le continu - ALI transfer of continuum
        CALL cPALETOU
c=======
        nbr2=0
                     do 200 kr=2,nraimax
c les kali sont definis dans lectatom 
c kali=0:raie non transteree/kali=1:transfert 2dir/kali=2:ali simple/kali=3:mali
c kali=0:non transfered line/kali=1:transfer 2dir/kali=2:simple ali/kali=3:mali
                     if(fdo(kr).lt.fdolim) goto 200
                     if(itali.eq.1.and.kali(kr).eq.2) then
c TRANSFERT ALI-simple Paletou  pour les raies kali=2
! simple ALI TRANSFER for lines kali=2
                         nbr2=nbr2+1
                       if(nbr2.eq.1.and.ifich.ge.5.and.ifich.le.8) then
                         write(29,*) 'PALETOU1-raies: nitf=',nitf
                         write(2,*)  'PALETOU1-raies: nitf=',nitf
                         write(3,*)  'PALETOU1-raies: nitf=',nitf
                         write(4,*)  'PALETOU1-raies: nitf=',nitf
                         write(85,*) 'PALETOU1-raies: nitf=',nitf
                         endif
        CALL rPALETOU1(kr)
c=======
                     else if(itali.eq.0.or.kali(kr).eq.1) then
c TRANSFERT AM ancien pour les raies kali=1 - rough transfer for lines kali=1
                        do mm=1,nfin
        m=nfin-mm+1
                kc=kcr(kr)
                do lf=1,nf+1
                ddt=(1.d0-exdtaur(lf,kr,m)*exdtaur(lf,kr,m-1)
     &                  *exdtauc(kc,m)*exdtauc(kc,m-1))/2.d0
        rimoins(lf,kr,m)=(rimoins(lf,kr,m+1)*exdtaur(lf,kr,m-1)
     &                        *exdtauc(kc,m-1) + sourcer(lf,kr,m)*ddt)
                enddo   ! lf                                                 !m
                ddt=(1.d0-exdtauc(kc,m)*exdtauc(kc,m-1))/2.d0
        rimdnuco(1,kr,m)=(rimdnuco(1,kr,m+1)*exdtauc(kc,m-1)
     &                         + sourdnuco(1,kr,m)*ddt)
        rimdnuco(2,kr,m)=(rimdnuco(2,kr,m+1)*exdtauc(kc,m-1)
     &                         + sourdnuco(2,kr,m)*ddt)
                        enddo   ! mm
                     endif   ! itali - kali
 200                 continue   ! kr
c=======
c TRANSFERT MALI Paletou pour les ions multi-niveaux avec raies kali=3
c (ne pas oublier de mettre kali=3 dans le Lectatom)
! MALI TRANSFER (multi-level ions) for lines kali=3
c       if(itali.eq.0) goto 210
        CALL rPALETOU2(1,1)   ! H
        CALL rPALETOU2(2,1)   ! He0
        CALL rPALETOU2(2,2)   ! He+
        do j=3,10
           iz0=iz(j)
           do i=1,iz0
              if(nivion(i,j).gt.1) CALL rPALETOU2(j,i)
           enddo
        enddo
c=======
 210   continue
c
       rtauthx=0.d0                                                        !m
       do m=1,npt
          rtauthx=rtauthx + (tcol(m+1)-tcol(m))*sel *0.6652d-24
             if(nitf.eq.1) pradrai1(m)=0.d0
             pradrai(m)=pradrai1(m)
          do kr=1,nraimax
             pradrai(m)=pradrai(m) + pradr(kr,m)
          enddo
          pradx=0.d0
          if(acompt+ccompt+ecompt.gt.0.d0) pradx=
     &     (acompt*expa(-bcompt*rtauthx)+ccompt*expa(-dcompt*rtauthx)
     &     +ecompt*expa(-fcompt*rtauthx))/(acompt+ccompt+ecompt)
     &     *flxdurincid*2.22376d-11                           ! 2/3c
          prad(m)=pradc(m) + pradx
          if(idens.ne.4.and.idens.ne.7.and.idens.ne.20.and.idens.ne.21)
     &       prad(m)=prad(m) + pradrai(m)
       enddo
c
c IMPRESSIONS - PRINTINGS :
c .txt(15) .fl2(20) .flu(19) .res(14) .bil(23) .dio(24) .ime(25)
c pour 2 directions: reflechi=pi*cimoins0(kc), sortant=pi*ciplush(kc,npt)
c                     car bc0=exflux(kc)/pi        grave PB du pi ou 2pi/V3
c pour 2*3 directions: reflechi = 2piJ-exflux(k) = pi*cimoins0(kc)
c                      sortant = pi*(2J-cimoinsh(kc)) = pi*ciplush(kc,npt)
c
      if(kimpf2.eq.1.and.ifich.ne.0.and.ifich.le.8.and.iexpert.eq.1)then
        WRITE(15,*) '  hnuev: flux reflechi   sortant, avant ',
     &                'discontinuite:reflechi  sortant'
        p=pi
cv        if(itali.eq.0) p=pi2v3
        WRITE(15,81) thnuc(188),cimoins0(188)*p,ciplush(188)*p,         !balmer
     &        cimoins0(189)*p,ciplush(189)*p
                do k=1,220
                if(k.eq.1.or.k.eq.3.or.k.eq.24) then
        if(nitf.eq.1) WRITE(15,84) thnuc(k),k,exflux(k)
        WRITE(15,81) thnuc(k),cimoins0(k)*p,ciplush(k)*p,
     &                   cimoins0(k+220)*p,ciplush(k+220)*p
                endif   ! k
        if(k.eq.122.or.k.eq.138.or.k.eq.182.or.k.eq.195) then
        if(nitf.eq.1) WRITE(15,84) thnuc(k),k,exflux(k)
        WRITE(15,82) thnuc(k),cimoins0(k)*p,ciplush(k)*p
                endif   ! k
                enddo
                endif   ! nitf
   84   format(' hnuev=',1pg12.5,' kc=',i4,' flux entrant=',1pe10.3)
   81   format(1pg12.5,2(';',1pe10.3),16x,2(';',1pe10.3))
   82   format(1pg12.5,2(';',1pe10.3))
c hnuev: flux reflechi   sortant, avant discontinuite:reflechi  sortant 
c
        se=1.d0                                           ! idilu=0  nuage plan
        si=1.d0
c        if(idilu.eq.1) se=raymax**2 *pi4/hLbol           ! halo spherique
c        if(idilu.eq.1) si=raymin**2 *pi4/hLbol
c
             if(ila.eq.0) then
       if(ifich.ge.3.and.ifich.le.8.and.(mod(nitf,5).eq.0.or.kitf.eq.1))
     & WRITE(20,85)tab,tab,tab,tab,tab,tab,tab,tab
       if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8)
     & WRITE(19,85) tab,tab,tab,tab,tab,tab,tab,tab
       if(kitf.eq.1) then
          if(ifich.eq.9) then
             WRITE(14,*)
     &' transmis dans la direction de l''incident =incident*exp(-tauc)'
             if(incang.eq.0) then
                WRITE(14,*)'   avec tauc=somme(dz*V3*(Kabs+Kth))'
             else
                WRITE(14,*)'   avec tauc=somme(dz*(Kabs+Kth))'
             endif
          else
             WRITE(14,*)'   transmis-approche=incident*exp(-tauabs)'
             if(incang.eq.0) then             ! cas isotrope
                WRITE(14,*)'   tauc=somme(dz*sqrt(3Kabs*(Kabs+Kth)))'
             else                             ! cas de l'incidence normale
                WRITE(14,*)'   tauc=somme(dz*sqrt(Kabs*(Kabs+Kth)))'
             endif
          endif
          WRITE(14,85) tab,tab,tab,tab,tab,tab,tab,tab
       endif
             else   ! english
       if(ifich.ge.3.and.ifich.le.8.and.(mod(nitf,5).eq.0.or.kitf.eq.1))
     & WRITE(20,985)tab,tab,tab,tab,tab,tab,tab,tab
       if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8)
     & WRITE(19,985) tab,tab,tab,tab,tab,tab,tab,tab
       if(kitf.eq.1) then
          if(ifich.eq.9) then
             WRITE(14,*)
     & ' transmitted in the direction of incident=incident*exp(-tauc)'
             if(incang.eq.0) then
                WRITE(14,*)'   with tauc=sum(dz*V3*(Kabs+Kth))'
             else
                WRITE(14,*)'   with tauc=sum(dz*(Kabs+Kth))'
             endif
          else
             WRITE(14,*)
     &       '   transmitted = incident*exp(-tauabs)'
             if(incang.eq.0) then
                WRITE(14,*)'   tauc=sum(dz*sqrt(3*Kabs*(Kabs+cth)))'
             else
                WRITE(14,*)'   tauc=sum(dz*sqrt(Kabs*(Kabs+cth)))'
             endif
          endif
          WRITE(14,985) tab,tab,tab,tab,tab,tab,tab,tab
       endif
             endif   ! ila
c transmis dans la direction de l'incident =incident*exp(-tauc)
c   avec tauc=somme(dz*V3*(Kabs+Kth))
c transmis-approche=incident*exp(-tauabs) tauc=somme(dz*sqrt(3Kabs*(Kabs+Kth)))
c transmitted in the direction of incident =incident*exp(-tauc)
c   with tauc=sum(dz*V3*(Kabs+Kth))
c transmitted = incident*exp(-tauabs)
c   tauc=sum(dz*sqrt(3*Kabs*(Kabs+cth)))
 85    format('  CONTINU F_nu (erg cm-2 s-1 hz-1)'/
     &     '   hnuev   ',a,'Fincident ',a,'Freflechi ',a,'Fsortant ',a, !2345678
     &     'Ftransmi-ap ',a,'tauc     ',a,'kc',a,'bord-ion',a)
 985   format('  CONTINUUM F_nu (erg cm-2 s-1 hz-1)'/
     &     '   hnuev   ',a,'Fincident ',a,'Freflected',a,'Foutward ',a,
     &     'Ftransmitted',a,'tauc     ',a,'kc',a,'ion-edge',a)
        tflcontsor=0.d0
        tflcontref=0.d0
        tflcontrap=0.d0                                                      !m
c                        do 301 kc=1,322
        ntri=220
        call INDEXTRIr(ntri,thnuc,iw)
                         do itri=1,220
                            kco=iw(itri)
                            nk=1
                            if(kco.le.102) nk=2
                            do 301 ik=1,nk
                               kc=kco
                               if(nk.eq.2.and.ik.eq.1) kc=kco+220
        entrant=exflux(kc)
                if(kc.le.102) then
        hnuev=thnuc(kc)
                dhnu=tdeb(kc)
                else if(kc.le.220) then
        hnuev=thnuc(kc)
                dhnu=tde(kc)
                else        ! kc>220 
        hnuev=thnuc(kc-220)*0.9999d0
                dhnu=tde(kc-220)-tdeb(kc-220)
                endif   ! kc
        sortant=ciplush(kc)*pi
        reflechi=cimoins0(kc) *pi
        transmisap=restflux(kc)
        tau=rtaucef(kc)
              if(ifich.eq.9) then
                 tau=rtaucat(kc)
                 transmisap=exflux(kc)*expa(-tau)
              endif
        tflcontsor=tflcontsor + sortant*dhnu
        tflcontref=tflcontref + reflechi*dhnu
        tflcontrap=tflcontrap + transmisap*dhnu
        if(abs(entrant).le.1.d-99) entrant=0.d0
        if(abs(sortant).le.1.d-99) sortant=0.d0
        if(abs(reflechi).le.1.d-99) reflechi=0.d0
        if(abs(transmisap).le.1.d-99) transmisap=0.d0
                if(kc.lt.185) then                                           !m
        if(ifich.ge.3.and.ifich.le.8
     &     .and.(mod(nitf,5).eq.0.or.kitf.eq.1)) WRITE(20,86) 
     &     hnuev,tab,entrant,tab,reflechi,tab,sortant,tab,
     &     transmisap,tab,tau,tab,kc,tab,nomion(kc)
        if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) WRITE(19,86)
     &     hnuev,tab,entrant,tab,reflechi,tab,sortant,tab,
     &     transmisap,tab,tau,tab,kc,tab,nomion(kc)
        if(kitf.eq.1) WRITE(14,86) 
     &     hnuev,tab,entrant,tab,reflechi,tab,sortant,tab,
     &     transmisap,tab,tau,tab,kc,tab,nomion(kc)
                else if(hnuev.lt.1.d3) then
        if(ifich.ge.3.and.ifich.le.8
     &     .and.(mod(nitf,5).eq.0.or.kitf.eq.1)) WRITE(20,87)
     &     hnuev,tab,entrant,tab,reflechi,tab,sortant,tab,
     &     transmisap,tab,tau,tab,kc,tab,nomion(kc)
        if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) WRITE(19,87)
     &     hnuev,tab,entrant,tab,reflechi,tab,sortant,tab,
     &     transmisap,tab,tau,tab,kc,tab,nomion(kc)
        if(kitf.eq.1) WRITE(14,87) 
     &     hnuev,tab,entrant,tab,reflechi,tab,sortant,tab,
     &     transmisap,tab,tau,tab,kc,tab,nomion(kc)
                else
        if(ifich.ge.3.and.ifich.le.8
     &     .and.(mod(nitf,5).eq.0.or.kitf.eq.1)) WRITE(20,88)
     &     hnuev,tab,entrant,tab,reflechi,tab,sortant,tab,
     &     transmisap,tab,tau,tab,kc,tab,nomion(kc)
        if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) WRITE(19,88)
     &     hnuev,tab,entrant,tab,reflechi,tab,sortant,tab,
     &     transmisap,tab,tau,tab,kc,tab,nomion(kc)
        if(kitf.eq.1) WRITE(14,88) 
     &     hnuev,tab,entrant,tab,reflechi,tab,sortant,tab,
     &     transmisap,tab,tau,tab,kc,tab,nomion(kc)
                endif   ! kc
c
       if(ndir.ge.2.and.kimpf.eq.1) then
          write(34,'(0pf12.5,i5,41(1pe11.3))') hnuev,kc,exflux(kc),
     &      (fcra(idir,kc),idir=1,ndir),(fcsa(idir,kc),idir=1,ndir)
          if(ifich.ge.1.and.ifich.le.8)
     &    write(33,'(0pf12.5,i5,41(1pe11.3))') hnuev,kc,exflux(kc),
     &      (fcra(idir,kc),idir=1,ndir),(fcsa(idir,kc),idir=1,ndir)
       endif

 301                        continue
                         enddo   ! itri
 86     format(0pf10.3,5(a,1pe10.3),a,i4,1x,a,a7)
 87     format(0pf10.5,5(a,1pe10.3),a,i4,1x,a,a7)
 88     format(0pf10.4,5(a,1pe10.3),a,i4,1x,a,a7)
c
c        if(mod(nitf,5).eq.0)
c     &  WRITE(20,93) tab,tab,tab,tab,tab,tab,tab,tab,tab
c                if(kimpf.eq.1) then
c        if(icompt.eq.0) WRITE(19,93) tab,tab,tab,tab,tab,tab,tab,tab,tab
c        if(icompt.ge.1) WRITE(19,91) tab,tab,tab,tab,tab,tab,tab,tab,tab
c                endif
c                if(kitf.eq.1) then
c        if(icompt.eq.0) WRITE(14,93) tab,tab,tab,tab,tab,tab,tab,tab,tab
c        if(icompt.ge.1) WRITE(14,91) tab,tab,tab,tab,tab,tab,tab,tab,tab
c                endif
c   91   format(' hnuraie  ',a,'kr',a,'kali',a,'no',a,'lambda',a,'Fdnu',
c     &'sortant',a,'reflechi',a,'sortcompt',a,'reflcompt',a,'taur0rai;')
c   93   format(' hnuraie  ',a,'kr',a,'kali',a,'no',a,'lambda',a,'Fdnu',
c     &'sortant',a,'reflechi',a,'dtaufin',a,'1.8dnudop(1)',a,'taur0rai;')
chnuraie  ;kr;kali;no;lambda;Fdnusortant;reflechi;dtaufin;1.8dnudop(1);taur0rai;
c  10.19894;  2;3;0;1215.670; 4.036E+10;-1.305E+09; 1.49E-05; 1.21E-02;3.900E+03
chnuraie  ;kr;kali;no;lambda;Fdnusortant;reflechi;sortcompt;reflcompt;taur0rai;
c  10.19894;  2;3;0;1215.670; 3.643E+10;-1.304E+09; 2.56E+05; 1.32E+08;3.540E+03
                          if(incang.eq.0) then   ! isotrope
           if(ila.eq.0) then
       if(ifich.ge.3.and.ifich.le.8.and.(mod(nitf,5).eq.0.or.kitf.eq.1))
     &  WRITE(20,91) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
        if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) 
     &  WRITE(19,91) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
        if(kitf.eq.1) 
     &  WRITE(14,91) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
           else
       if(ifich.ge.3.and.ifich.le.8.and.(mod(nitf,5).eq.0.or.kitf.eq.1))
     &  WRITE(20,991) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
        if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8)
     &  WRITE(19,991) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
        if(kitf.eq.1.and.iexpert.eq.1)
     &  WRITE(14,991) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
        if(kitf.eq.1.and.iexpert.le.0)
     &  WRITE(14,991) nitf,tab,tab,tab,tab,tab,tab,tab
           endif
 91     format(' '/'   FLUX DES RAIES integre sur le profil',
     &  ' (erg cm-2 s-1)',6x,'iteration no ;',i4/
     &'   le tau(centre-raie) est calcule dans la direction arccos1/V3'/
     &  '   hnuraie   ',a,'lambda   ',a,'reflechi    ',a,'sortant  ',
     &  a,'taur0raie ',a,'kr',a,'kali',a,'pb',a,'ion',a)
 991    format(' '/'   LINE FLUX integrated on profile (erg cm-2 s-1)',
     &     10x,'iteration no',i4/
     & '   tau(line-center) is calculated in direction arccos1/V3'/
     &  '  line-hnu   ',a,'lambda   ',a,'reflected   ',a,'outward ',
     &  a,'line-taur0 ',a,'kr',a,'kali',a,'pb',a,'ion',a)
 891    format(' '/'   LINE FLUX integrated on profile (erg cm-2 s-1)',
     &     10x,'iteration no',i4/
     & '   tau(line-center) is calculated in direction arccos1/V3'/
     &  '  line-hnu   ',a,'lambda   ',a,'reflected   ',a,'outward ',
     &  a,'line-taur0 ',a,'kr',a,'ion',a)
c  Flux des raies integre sur le profil (erg cm-2 s-1)
c  le tau(centre-raie) est calcule dans la direction arccos1/V3
c
                          else             ! incident normal ou incline
           if(ila.eq.0) then
       if(ifich.ge.3.and.ifich.le.8.and.(mod(nitf,5).eq.0.or.kitf.eq.1))
     &  WRITE(20,80) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
        if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) 
     &  WRITE(19,80) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
        if(kitf.eq.1) 
     &  WRITE(14,80) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
           else
       if(ifich.ge.3.and.ifich.le.8.and.(mod(nitf,5).eq.0.or.kitf.eq.1))
     &  WRITE(20,980) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
        if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8)
     &  WRITE(19,980) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
        if(kitf.eq.1.and.iexpert.eq.1)
     &  WRITE(14,980) nitf,tab,tab,tab,tab,tab,tab,tab,tab,tab
        if(kitf.eq.1.and.iexpert.le.0)
     &  WRITE(14,890) nitf,tab,tab,tab,tab,tab,tab,tab
           endif
 80     format(' '/'   FLUX DES RAIES integre sur le profil',
     &  ' (erg cm-2 s-1)',6x,'iteration no ;',i4/
     &'   le tau(centre-raie) est calcule dans la direction normale'/
     &  '   hnuraie   ',a,'lambda   ',a,'reflechi    ',a,'sortant  ',
     &  a,'taur0raie ',a,'kr',a,'kali',a,'pb',a,'ion',a)
 980    format(' '/'   LINE FLUX integrated on profile (erg cm-2 s-1)',
     &     10x,'iteration no',i4/
     & '   tau(line-center) is calculated in the normal direction'/
     &  '  line-hnu   ',a,'lambda   ',a,'reflected   ',a,'outward ',
     &  a,'line-taur0 ',a,'kr',a,'kali',a,'pb',a,'ion',a)
 890    format(' '/'   LINE FLUX integrated on profile (erg cm-2 s-1)',
     &     10x,'iteration no',i4/
     & '   tau(line-center) is calculated in the normal direction'/
     &  '  line-hnu   ',a,'lambda   ',a,'reflected   ',a,'outward ',
     &  a,'line-taur0  ',a,'kr',a,'ion',a)
                          endif
c
        if(kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.8) then
c           WRITE(21,96) nitf,log10(densinit),txt,idens,coltotlog,
c     &                  csitlog,flindex,hnumin,hnumax,incid,fii,ijour
           if(ila.eq.0) then
              WRITE(21,93) tab,tab,tab,tab,tab,tab,tab,tab,tab
           else
              WRITE(21,993) tab,tab,tab,tab,tab,tab,tab,tab,tab
           endif
        endif
 93     format(/'  Complements raies: comptonisation de la raie si ',
     &  'dnucompton>FWHM*icompt'/'  nbc=nb de strates avec compt',
     &  './mdeb=1er strate avec compt./1.8*dnudop(z=0)ev'/
     &  ' hnuraie  ',a,'kr',a,'nbc',a,'mdeb',a,'reflechi',a,'reflcompt '
     &  ,a,'sortant ',a,'sortcompt',a,'dtaufin',a,'1.8dnudop',a)
 993    format(/'  Line complements: line comptonisation if ',
     &  'dnucompton>FWHM*icompt'/'  nbc=layer number with compt',
     &  './mdeb=1st layer with compt./1.8*dnudop(z=0)ev'/
     &  ' linehnu  ',a,'kr',a,'nbc',a,'mdeb',a,'reflected',a,'reflcompt'
     &  ,a,'outward ',a,'outcompt',a,'dtau-end',a,'1.8dnudop',a)
c
        nbrh=nrcum(1,1)
c        nraissfek=nrcum(26,10)
        tflraiesHem=0.d0                                                     !m
        tflraiesem=0.d0
        tflraiesHre=0.d0
        tflraiesre=0.d0
        tfluoso=0.d0
        tfluore=0.d0
        tfluore6p4=0.d0
        contraie=0.d0
        krimp=0
c facteur ad hoc de correction du au nombre de strates (mars 2009)
        fstr=1.d0
        if(ndcm.lt.100) fstr=float(ndcm)/(float(ndcm)-1) ! si igrid=0 ndcm=nstr
           if(iexpert.eq.1.and.ifich.ge.1.and.ifich.le.8)
     &  write(15,*) 'ndcm=',ndcm,' fstr=',fstr
        if(fstr.lt.1.d0) fstr=1.d0
c        shb=(riplus(8)*...+riplus(11)*...)*pi2v3*se
c        rhb=(rimoins(8,1)*...+rimoins(11,1)*...)*pi2v3*si
c
                          do 302 kr=2,nraimax
                if(fdo(kr).lt.fdolim) goto 302
                if(kr.eq.7.or.kr.eq.8.or.kr.eq.9) goto 302
        hnuev=thnur(kr)
        sortant=0.d0
        reflechi=0.d0
                         if(itali.eq.0.or.kali(kr).eq.1) then
                do lf=1,nf
        sortant=sortant + (riplus(lf,kr)-riplus(nf+1,kr)) *fint(lf)
        reflechi=reflechi+(rimoins(lf,kr,1)-rimoins(nf+1,kr,1))*fint(lf)
                enddo ! lf
        sortant=sortant *uhz(kr) *pi2v3 *fstr
        reflechi=reflechi *uhz(kr) *pi2v3 *fstr
           socomp=0.d0
           recomp=0.d0
        if(icompt.ne.0.and.ncomp(kr).ge.1) then
           socomp=(ripdnuco(1,kr)-ripdnuco(2,kr))
           recomp=(rimdnuco(1,kr,1)-rimdnuco(2,kr,1))
        endif   ! icompt>0
           if(kitf.eq.1.and.resol.gt.0.d0) then
           fsor(1,kr)=sortant + socomp
           frer(1,kr)=reflechi + recomp
           fsor(2,kr)=socomp
           frer(2,kr)=recomp
           endif   ! kitf=1
                         else   ! kali>1
        sortant= fsor(1,kr) *fstr
        reflechi=frer(1,kr) *fstr
           socomp=0.d0
           recomp=0.d0
        if(icompt.ne.0.and.ncomp(kr).ge.1) then
           socomp=fsor(2,kr) *fstr
           recomp=frer(2,kr) *fstr                                           !m
        endif   ! icompt>0
                         endif   ! kali
c
        tflraiesem=tflraiesem + sortant 
        tflraiesre=tflraiesre + reflechi
                if(kr.le.nbrh) then
        tflraiesHem=tflraiesHem + sortant
        tflraiesHre=tflraiesHre + reflechi
                endif   ! kr
                if(kr.eq.nrcum(24,10)+1.or.kr.eq.nrcum(25,10)+1
     &          .or.(kr.gt.nraissfek.and.kr.le.nraissfek+24)) then
        tfluoso=tfluoso + sortant
        tfluore=tfluore + reflechi
                endif   ! kr
                if(kr.gt.nraissfek.and.kr.le.nraissfek+18) then
        tfluore6p4=tfluore6p4 + reflechi
                endif   ! kr
           if(kitf.eq.1) then
        if(incid.eq.1) contraie=contraie + FFLUXLP(hnuev)*twr(kr)
        if(incid.eq.2) contraie=contraie + FFLUXBB(hnuev)*twr(kr)*pi
        if(incid.eq.3) contraie=contraie + FFLUX(hnuev)*twr(kr)
        if(incid.eq.4) contraie=contraie + FFLUX1(hnuev)*twr(kr)
        if(incid.eq.5) contraie=contraie + FFLUX2(hnuev)*twr(kr)
        if(incid.eq.6) contraie=contraie + FFLUX3(hnuev)*twr(kr)
        if(incid.eq.7) contraie=contraie + FFLUX4(hnuev)*twr(kr)
        if(incid.eq.8) contraie=contraie + FFLUX5(hnuev)*twr(kr)
        if(incid.eq.9) contraie=contraie + FFLUX6(hnuev)*twr(kr)
        if(incid.eq.10) contraie=contraie + FFLUX7(hnuev)*twr(kr)
        if(incid.eq.17) contraie=contraie + FFLUXECO1LP(hnuev)*twr(kr)   !Agata
        if(incid.eq.18) contraie=contraie + FFLUXECO2LP(hnuev)*twr(kr)   !Agata
           endif   ! kitf=1
           k4=nrcum(1,2)+1       ! He2-1  krimp=4
           k5=nrcum(2,3)+1       ! C3-1         5  
           k6=nrcum(3,3)+1       ! C4-1         6
           k7=nrcum(3,5)+2       ! O4-2         7
           k8=nrcum(5,5)+1       ! O6-1         8
           k9=nrcum(7,5)+1       ! O8-1         9                           !m
           k10=nrcum(14,10)+1    ! Fe15-1       10
           k11=nrcum(22,10)+1    ! Fe23-1       11
           k12=nrcum(24,10)+2    ! Fe25-w       12
           k13=nrcum(26,10)+15   ! FeK15        13
           k14=nrcum(26,10)+24   ! FeK24        14
        if(kr.eq.2.or.kr.eq.10.or.kr.eq.11.or.kr.eq.k4.or.kr.eq.k5
     &  .or.kr.eq.k6.or.kr.eq.k7.or.kr.eq.k8.or.kr.eq.k9.or.kr.eq.k10
     &  .or.kr.eq.k11.or.kr.eq.k12.or.kr.eq.k13.or.kr.eq.k14) then
                krimp=krimp+1
        t1(krimp)=sortant
        t2(krimp)=reflechi
        t3(krimp)=hnuev
                endif   ! kr
        if(abs(sortant).le.1.d-99) sortant=0.d0
        if(abs(reflechi).le.1.d-99) reflechi=0.d0
c        sraphb=0.d0
c        rraphb=0.d0
c        if(shb.gt.0.) sraphb=sortant/shb
c        if(rhb.gt.0.) rraphb=reflechi/rhb
c
        if(kr.eq.2.and.nraimax.gt.1024) then
           if(kitf.eq.1) WRITE(14,*)' FIRST SET OF LINES'
           if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8)
     &     WRITE(19,*)' FIRST SET OF LINES'
        endif
        if(kr.eq.1024.and.kitf.eq.1) WRITE(14,*)' SECOND SET OF LINES'
        if(kr.eq.1024.and.kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8)
     &  WRITE(19,*)' SECOND SET OF LINES'
                       if(alo(kr).lt.1.d4) then
       if(kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.8)WRITE(21,194)hnuev,
     &   tab,kr,tab,ncomp(kr),tab,mdebcomp(kr),tab,reflechi,tab,recomp,
     &       tab,sortant,tab,socomp,tab,rtaurl(kr),tab,twr(kr)/esh
       if(ifich.ge.3.and.ifich.le.8.and.(mod(nitf,5).eq.0.or.kitf.eq.1))
     &       WRITE(20,94) 
     &       hnuev,tab,alo(kr),tab,reflechi,tab,sortant,tab,rtaur0(kr),
     &       tab,kr,tab,kali(kr),tab,npbali(kr),tab,nomrai(kr)
       if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) WRITE(19,94) 
     &       hnuev,tab,alo(kr),tab,reflechi,tab,sortant,tab,rtaur0(kr),
     &       tab,kr,tab,kali(kr),tab,npbali(kr),tab,nomrai(kr)
       if(kitf.eq.1.and.(ila.eq.0.or.iexpert.eq.1)) WRITE(14,94) 
     &       hnuev,tab,alo(kr),tab,reflechi,tab,sortant,tab,rtaur0(kr),
     &       tab,kr,tab,kali(kr),tab,npbali(kr),tab,nomrai(kr)
       if(kitf.eq.1.and.ila.eq.1.and.iexpert.le.0) WRITE(14,994) 
     &       hnuev,tab,alo(kr),tab,reflechi,tab,sortant,tab,rtaur0(kr),
     &       tab,kr,tab,nomrai(kr)
                       else
       if(kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.8)WRITE(21,195)hnuev,
     &   tab,kr,tab,ncomp(kr),tab,mdebcomp(kr),tab,reflechi,tab,recomp,
     &       tab,sortant,tab,socomp,tab,rtaurl(kr),tab,twr(kr)/esh
       if(ifich.ge.3.and.ifich.le.8.and.(mod(nitf,5).eq.0.or.kitf.eq.1))
     &       WRITE(20,95)
     &       hnuev,tab,alo(kr),tab,reflechi,tab,sortant,tab,rtaur0(kr),     !m
     &       tab,kr,tab,kali(kr),tab,npbali(kr),tab,nomrai(kr)
       if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) WRITE(19,95) 
     &       hnuev,tab,alo(kr),tab,reflechi,tab,sortant,tab,rtaur0(kr),
     &       tab,kr,tab,kali(kr),tab,npbali(kr),tab,nomrai(kr)
       if(kitf.eq.1.and.(ila.eq.0.or.iexpert.eq.1)) WRITE(14,95) 
     &       hnuev,tab,alo(kr),tab,reflechi,tab,sortant,tab,rtaur0(kr),
     &       tab,kr,tab,kali(kr),tab,npbali(kr),tab,nomrai(kr)
       if(kitf.eq.1.and.ila.eq.1.and.iexpert.le.0) WRITE(14,995)
     &       hnuev,tab,alo(kr),tab,reflechi,tab,sortant,tab,rtaur0(kr),
     &       tab,kr,tab,nomrai(kr)
                       endif
 94    format(0pf10.3,a,0pf10.3,2(a,1pg11.3),a,1pe10.3,a,
     &       i4,a,i2,a,i2,1x,a,a7)
 95    format(0pf10.6,a,1pe10.3,2(a,1pg11.3),a,1pe10.3,a,
     &       i4,a,i2,a,i2,1x,a,a7)
 994   format(0pf10.3,a,0pf10.3,2(a,1pg11.3),a,1pe10.3,a,i4,1x,a,a7)
 995   format(0pf10.6,a,1pe10.3,2(a,1pg11.3),a,1pe10.3,a,i4,1x,a,a7)
c23456789 12345.789 12345678901  -3.567e+01  2.456e+90 1234 12 1 abcdefg
 194   format(0pf8.2,a,i4,2(a,i3),4(a,1pg9.2),2(a,1pe8.2))
 195   format(0pf8.5,a,i4,2(a,i3),4(a,1pg9.2),2(a,1pe8.2))
        if(kimpf.eq.1.and.ndir.ge.2) then
           if(kr.eq.2.and.ifich.ne.0.and.ifich.le.8) then
              WRITE(33,'(a)') '#'
              WRITE(33,'(a,i5,a,i5)')'# Flux (LINES) versus',
     &           ndir,' directions        iteration no',nitf
              WRITE(33,'(a,a)') '# hnuev kr refl1',
     &       ' refl2 refl3 refl4 refl5... out1 out2 out3 out4 out5... '
           endif
           if(kr.eq.2) then
              WRITE(34,'(a)') '#'
              WRITE(34,'(a,i5,a,i5)')'# Flux (LINES) versus',
     &           ndir,' directions        iteration no',nitf
              WRITE(34,'(a,a)') '# hnuev kr refl1',
     &       ' refl2 refl3 refl4 refl5... out1 out2 out3 out4 out5... '
              ka=1
c              ka=mod(ndir,5)
           endif
           if(ifich.ge.1.and.ifich.le.8)
     &     WRITE(33,'(0pf12.5,i5,40(1pe11.3))') hnuev,kr,
     &     (farer(ia,kr),ia=1,ndir,ka),(fasor(ia,kr),ia=1,ndir,ka)
           WRITE(34,'(0pf12.5,i5,40(1pe11.3))') hnuev,kr,
     &     (farer(ia,kr),ia=1,ndir,ka),(fasor(ia,kr),ia=1,ndir,ka)
        endif
 302                         continue   ! kr
        if(ila.eq.0.and.(mod(nitf,5).eq.0.or.nitf.eq.1.or.kitf.eq.1))
     &     WRITE(18,*)' '
        if(kitf.eq.1) WRITE(14,*)
        if(ifich.ge.1.and.ifich.le.8) then
           if(kimpf.eq.1.and.ila.eq.0) WRITE(17,*) ' '
           if(kimpf.eq.1) WRITE(19,*) ' '                                !m
           if(ifich.ge.3.and.(mod(nitf,5).eq.0.or.kitf.eq.1))WRITE(20,*)
        endif
c              if(kimpf2.eq.1.and.ifich.ne.0.and.ifich.le.8) then
c        WRITE(15,*) '  lambda     (hnuev) : flux reflechi   sortant ',
c     &                          '  taur0rai   tauaile'
c        WRITE(15,83)
c     &  alo(2),t3(1),t2(1),t1(1),rtaur0(2),rtaurl(2),                   ! Lya
c     &  alo(10),t3(2),t2(2),t1(2),rtaur0(10),rtaurl(10),                ! Halfa
c     &  alo(11),t3(3),t2(3),t1(3),rtaur0(11),rtaurl(11),                ! Hbeta
c     &  alo(k4),t3(4),t2(4),t1(4),rtaur0(k4),rtaurl(k4),                ! HeII
c     &  alo(k5),t3(5),t2(5),t1(5),rtaur0(k5),rtaurl(k5),                ! CIII
c     &  alo(k6),t3(6),t2(6),t1(6),rtaur0(k6),rtaurl(k6),                ! CIV
c     &  alo(k7),t3(7),t2(7),t1(7),rtaur0(k7),rtaurl(k7),                ! OIV
c     &  alo(k8),t3(8),t2(8),t1(8),rtaur0(k8),rtaurl(k8),                ! OVI
c     &  alo(k9),t3(9),t2(9),t1(9),rtaur0(k9),rtaurl(k9),                ! OVIII
c     &  alo(k10),t3(10),t2(10),t1(10),rtaur0(k10),rtaurl(k10),          ! FeXV
c     &  alo(k11),t3(11),t2(11),t1(11),rtaur0(k11),rtaurl(k11),          ! Fe23
c     &  alo(k13),t3(13),t2(13),t1(13),rtaur0(k13),rtaurl(k13),          ! Fek15
c     &  alo(k14),t3(14),t2(14),t1(14),rtaur0(k14),rtaurl(k14),          ! Fek24
c     &  alo(k12),t3(12),t2(12),t1(12),rtaur0(k12),rtaurl(k12)           ! Fe25
c           endif
   83  format(1pg12.5,';',1pg12.5,4(';',1pe10.3),'; Lyalfa'
     &        /1pg12.5,';',1pg12.5,4(';',1pe10.3),'; tHalfa'
     &  /1pg12.5,';',1pg12.5,4(';',1pe10.3),'; tHbeta'
     &        /1pg12.5,';',1pg12.5,4(';',1pe10.3),'; He2Lya'
     &  /1pg12.5,';',1pg12.5,4(';',1pe10.3),'; CIV-1'
     &        /1pg12.5,';',1pg12.5,4(';',1pe10.3),'; NIV-2'
     &  /1pg12.5,';',1pg12.5,4(';',1pe10.3),'; OIV-5'
     &  /1pg12.5,';',1pg12.5,4(';',1pe10.3),'; OVI-1'
     &  /1pg12.5,';',1pg12.5,4(';',1pe10.3),'; O8Lya'
     &  /1pg12.5,';',1pg12.5,4(';',1pe10.3),'; FeXV-1',
     &  /1pg12.5,';',1pg12.5,2(';',1pe10.3),2(';',1pe9.2),';Fe23-1',
     &  /1pg12.5,';',1pg12.5,2(';',1pe10.3),2(';',1pe9.2),';FeKXV',
     &  /1pg12.5,';',1pg12.5,2(';',1pe10.3),2(';',1pe9.2),';FeKXXIV',!234567
     &  /1pg12.5,';',1pg12.5,4(';',1pe10.3),';FeXXV-1')
c lambda: flux reflechi   sortant   dnu(mdeb)  dnu(mfin)  en ev
c lambda     (hnuev) : flux reflechi   sortant   taur0rai   tauaile
c100000.0   ;  10000.    ; 2.456E-90; 2.456E-90; 2.456E-90; 2.456E-90; Lyalfa 
        tflcontsor=tflcontsor *esh                                            !m
        tflcontref=tflcontref *esh
        tflcontrap=tflcontrap *esh
        ttpp=ttpraieint+ttpech
        tflraie=tflraiesem  + tflraiesre 
        tflpartant=tflraie+ttpp+tflcontsor+tflcontref+ttprsecdi
c     &        -ttgpcompt                         ! finalement c'est mieux sans
        contraie=contraie*fluxex0
                         if(nitf.ne.1) then
        if(Tmil.gt.0.d0) pte=(Tmil-Tmilp)/Tmil *100.d0
        if(cim1.gt.0.d0) pim=(cim1-cim1p)/cim1 *100.d0
        if(toly.gt.0.d0) pto=(toly-tolyp)/toly *100.d0
        if(tflcontref.gt.0.) pref=(tflcontref-tflcrep)/tflcontref*100.d0!234567
        if(tflcontsor.gt.0.) psor=(tflcontsor-tflcsop)/tflcontsor*100.d0
        if(ttgpcompt.gt.0.)pcomp=(ttgpcompt-pttgpcompt)/ttgpcompt*100.d0
        if(tflraie.gt.0.d0) pvrai=(tflraie-tflraiep)/tflraie *100.d0
        if(tflpartant.gt.0.d0)
     &           ppart=(tflpartant-ptflpartant)/tflpartant*100.d0
c        WRITE(6,97) ecart*100.d0,ecarim*100.d0,pim,pto,pref,pvrai
                         endif   ! nitf>1
c
           if(kimpf2.eq.1.and.nitf.ne.1.and.ifich.ne.0.and.ifich.le.8
     &     .and.iexpert.eq.1)
     &  WRITE(15,97) ecart*100.d0,ecarim*100.d0,pim,pto,pref,pvrai           !m
   97   format(' EVOLUTION:',42x,'sqrt(Som((dT/T)**2)/n)'/
     & '   ecart moyen de la temperature:',0pf10.5,'% et du flux',      !234567
     & ' reflechi:',0pf10.5,'%'/'   variation du flux reflechi et du ',
     & 'tau a 13.6ev:',0pf10.5,'%,',0pf10.5,'%'/'    du flux reflechi ',
     & 'total et du total des raies:',0pf10.5,'%,',0pf10.5,'%') 
        tflii=tflincident
        if(irent.eq.1.or.irent.ge.5) tflii=tflincident+flarriere
        if(irent.eq.2.or.irent.eq.4) tflii=tflii+flarriere+tflcontsor
        if(irent.eq.3) tflii=tflincident+flarriere-tflcontsor
               if(kimpf2.eq.1) then
c        WRITE(6,180)         tflincident,tflpartant,
c     &          tflcontsor,tflraiesem,tfluoso,tflraiesHem,
c     &          tflcontref,tflraiesre,tfluore,tflraiesHre,
c     &          ttprsecdi,tflcontrap,ttgcomptx
        if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &  WRITE(15,180) tflii,tflpartant,
     &          tflcontsor,tflraiesem,tfluoso,tflraiesHem,
     &          tflcontref,tflraiesre,tfluore,tflraiesHre,
     &          ttprsecdi,tflcontrap,ttgcomptx
        if(kitf.eq.1.and.ila.eq.0) WRITE(14,180) tflii,tflpartant,
     &          tflcontsor,tflraiesem,tfluoso,tflraiesHem,
     &          tflcontref,tflraiesre,tfluore,tflraiesHre,
     &          ttprsecdi,tflcontrap,ttgcomptx
  180   format(' Bilan des FLUX: total des flux incident=',1pe10.3,
     &    ',partant=',1pe10.3,' sscompt'/'  sortant:  continu=',1pe10.3,!234567
     &    ' raies=',1pe10.3,' (tFeK=',1pe10.3,' H=',1pe10.3,')'/
     &    '  reflechi: continu=',1pe10.3,' raies=',1pe10.3,' (tFeK=',
     &    1pe10.3,' H=',1pe10.3,')'/'  rec.sur niv.second.=',1pe10.3,
     &    ', transmis approx=',1pe10.3,'/gcomptx=',1pe10.3/)
        total=ttpgrai+ttpertefluo+ttpraieint+ttpech+ttpfree+
     &                ttprecf+ttprsecdi-ttgfree-ttgion-ttgraie-ttgpcompt
        if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &  WRITE(15,183) ttpgrai,ttpraie,ttpertefluo,ttpraieint,ttpech,
     &                ttpfree,ttprecf,ttprsecdi,ttgfree,ttgion,ttgraie,
     &                ttgpcompt,total
  183   format(' Bilan thermique des pertes et gains sommes sur tous ',
     &      'les strates:'/'  total des pertes : raies permises(net)=',
     &      1pe10.3,' (pertesraies NA=',1pe10.3,')'/'  fluorescence=',
     &      1pe10.3,',interdites=',1pe10.3,',echange de charges=',
     &      1pe10.3/'  free-free=',1pe10.3,',rec. sur fond.=',1pe10.3,
     &      ',rec.sur niv.second.=',1pe10.3/'  total des gains:',
     &      ' free-free=',1pe10.3,',ionisations=',1pe10.3,',raies=',
     &      1pe10.3/'  gains-pertes Compton=',1pe10.3,20x,                   !m
     &      ' p-g total=',1pe10.3)
      if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)WRITE(15,783)ttpgray
  783   format('  total des pertes-gains raies hydrogenoides(sauf H)=',
     &          1pe10.3)
        if(kitf.eq.1.and.ifich.ne.0.and.ifich.le.8.and.iexpert.eq.1)then
        rTmoyly=rsomtely/rsomely
        sTmoyly=0.d0
        if(ssomely.ne.0.d0) sTmoyly=ssomtely/ssomely
        WRITE(15,98) contraie,rTmoyly,sTmoyly,restflux(221),restflux(1)
        endif
   98   format(' Divers : flux incident sous-jacent aux raies=',1pe10.3/
     &     '  T moyen au bord Lyman : cote reflechi=',1pe10.3,
     &     ', cote emis=',1pe10.3/'  (F0expa(-ttaucef) avant,apres le ',
     &     'bord Lyman :',1pe10.3,',',1pe10.3,')')
                          endif    ! nitf
c
        if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1) then
           pour=100.d0*(tflpartant-tflii)/tflii
           if(irent.eq.4.or.irent.eq.2)
     &        pour=100.d0*(tflpartant-tflii)/(tflii-tflcontsor)
           if(nitf.eq.1.and.ila.eq.0) WRITE(23,184) 
           if(nitf.eq.1.and.ila.eq.1) WRITE(23,884) 
           if(ila.eq.0) WRITE(23,185) 
     &  nitf,ecart*100.d0,tflcontref,tflcontsor,tflraie,ttgpcompt,
     &  tflpartant,ttgcomptx,ttprsecdi,ttpech,ciplush(1)*pi,(ciplush(1)
     &  -ciplush(221))/ciplush(221)*100.d0,cimoins0(1)*pi,(cimoins0(1)   !234567
     &  -cimoins0(221))/cimoins0(221)*100.d0,cimoins0(102)*pi,
     &  (cimoins0(102)-cimoins0(322))/cimoins0(322)*100.d0,rtaucef(1),
     &  rtaucef(3),rtaucef(24),rtaucef(102),rtaur0(nrcum(1,2)+1),t1(1),
     &  t2(1),t1(3),t2(3),t1(4),t2(4),t1(5),t2(5),t1(7),t2(7),t2(8),
     &  t2(9),t1(10),t2(11),tfluoso,tfluore,tfluore6p4,t2(13),t2(14),
     &  t2(12),rh1smil,rh2pmil,rhe1mil,rhe2fmil,rhe22mil,rfe15mil,pour
           if(ila.eq.1) WRITE(23,885) 
     &  nitf,ecart*100.d0,tflcontref,tflcontsor,tflraie,ttgpcompt,
     &  tflpartant,ttgcomptx,ttprsecdi,ttpech,ciplush(1)*pi,(ciplush(1)
     &  -ciplush(221))/ciplush(221)*100.d0,cimoins0(1)*pi,(cimoins0(1)   !234567
     &  -cimoins0(221))/cimoins0(221)*100.d0,cimoins0(102)*pi,
     &  (cimoins0(102)-cimoins0(322))/cimoins0(322)*100.d0,rtaucef(1),
     &  rtaucef(3),rtaucef(24),rtaucef(102),rtaur0(nrcum(1,2)+1),t1(1),
     &  t2(1),t1(3),t2(3),t1(4),t2(4),t1(5),t2(5),t1(7),t2(7),t2(8),
     &  t2(9),t1(10),t2(11),tfluoso,tfluore,tfluore6p4,t2(13),t2(14),
     &  t2(12),rh1smil,rh2pmil,rhe1mil,rhe2fmil,rhe22mil,rfe15mil,pour
        if(kitf.eq.1.and.ila.eq.0) WRITE(23,181) tflpartant,
     &     flxdurincid-ttgcomptx,tflii,pour,tflii-flxdurincid+ttgcomptx
        if(kitf.eq.1.and.ila.eq.1) WRITE(23,882) tflpartant,tflii,pour
        endif
 181    format('  flux partant calcule total=',1pe10.3,'  (X>26kev:in',
     &  'c.-gcompt=',1pe10.3,')'/'bornes: flux incident total=',
     &  1pe10.3,'  (partant-incident)/incident=',0pf7.2,'%'/
     &  '# & incident<26kev+gcomptX   =',1pe10.3)                       !m
 882    format(/'# titan escaping flux(outward+reflected,without ',
     &  'flux>26kev)=',1pe10.3/'# total incident flux=',1pe10.3,
     &  '      (escaping_t-incident)/incident=',0pf7.2,'%')
c 184   format(' RESUME :'/'no it',6x,';Tmilieu;totreflcont;totsortcont',
 184  format(' RESUME :'/'no it',6x,';relvarT%;totreflcont;totsortcont',
     & ';totraies;g-pCompt;totpartantssco ;gComptx  ;ttprsecdi ;ttpech',
     & '    ;sF(13.6) ;sdiscLy%;rF(13.6) ;rdiscLy%;rF(Fe26) ;rdiscfe  ',
     & ';tauefLy  ;tauHeII   ;tauO8     ;tauFe26   ;taur0HeLya;sHLya  ',
     & '   ;rHLya  ','   ;sHbeta    ;rHbeta    ;sHeII     ;rHeII     ;',
     & 'sCIII     ;rCIII     ;',
     & 's0IV      ;rOIV      ;rOVI      ;rOVIII    ;sFe15     ;',
     & 'rFe23     ;stfluo    ;','rtfluo    ;rtfluo6.4 ;rFeK15    ;',
     & 'rFeK24    ;rFe25     ;rh1smil','   ;rh2pmil   ;rhe1mil   ;',
     & 'rhe2fmil  ;rhe22mil  ;rfe15mil;%inc;')
c mettre aussi sans ; 184 et 185 ?
 884   format('# SUMMARY:'/'#no it',5x,' relvarT% totreflcont totoutco',!234567
     & 'nt totlines g-pCompt total-titan     gComptx   ttprsecdi  ttpe',
     & 'ch     sF(13.6)  sdiscLy% rF(13.6)  rdiscLy% rF(Fe26)  rdiscfe',
     & '   tauefLy   tauHeII    tauO8      tauFe26    taur0HeLya sHLya',
     & '      rHLya  ','    sHbeta     rHbeta     sHeII      rHeII    ',
     & '  sCIII      rCIII      ',
     & 's0IV       rOIV       rOVI       rOVIII     sFe15      ',
     & 'rFe23      stfluo     ','rtfluo     rtfluo6.4  rFeK15     ',
     & 'rFeK24     rFe25      rh1smid','    rh2pmid    rhe1mid    ',
     & 'rhe2fmid   rhe22mid   rfe15mid %inc ')
c 185    format(i10,9(';',1pe10.3),3(';',1pe10.3,';',0pf7.2),
 185    format(i10,';',0pf10.5,8(';',1pe10.3),3(';',1pe10.3,';',0pf7.2),
     &  5(';',1pg10.3),26(';',1pe10.3),';',0pf12.2)
 885    format(i10,' ',0pf10.5,8(' ',1pe10.3),3(' ',1pe10.3,' ',0pf7.2),
     &  5(' ',1pg10.3),26(' ',1pe10.3),' ',0pf12.2)
co it       ;Tmilieu;totreflcont;totsortcont;totraies;g-pCompt;totpartantssco;gComptx  ;ttprsecdi ;ttpech    ;sF(13.6) ;sdiscLy% ;rF(13.6) ;rdiscLy%;rF(Fe26) ;rdiscfe  ;tauefLy  ;tauHeII   ;tauO8     ;tauFe26   ;taur0heLya;sHLya     ;rHLya     ;sHbeta    ;rHbeta    ;sHeII     ;rHeII     ;s0IV      ;rOIV      ;rHOVI     ;rHOVIII    ;sFe5      ;stfluo    ;rtfluo    ;rtfluo6.4 ;rFeK14    ;sFeK23    ;rFeK24    ;rFe25     ;rh1smil   ;rh2pmil   ;rhe1mil   ;rhe2fmil  ;rhe22mil  ;rfe15mil;;
c       123; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 234.67; 2.456E+90; 234.67; 2.456E+90; 234.67; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90;
c flux partant calcule total= 2.456E+90  (X>26kev:inc.-gcompt= 2.456E+90)
cbornes: flux incident total= 2.456E+90  (partant-incident)/incident=  34.67%
c& incident<26kev+gcomptX   = 2.456E+90
      if(kimpf2.eq.1.and.ifich.ne.0.and.ifich.le.8.and.iexpert.eq.1)then
        WRITE(15,186)
        WRITE(15,187)
     &  nitf,Tmil,tflcontref,tflcontsor,tflraie,ttgpcompt,tflpartant
        if(nitf.ne.1) WRITE(15,188) pte,pref,psor,pvrai,
     &         pcomp,ppart,pim,pto,ecart*100.d0,ecarim*100.d0
 186    format(' RESUME :'/'   no it;Tmilieu  ;totreflcont;totsortcont',
     &  ';totraies  ;gCompt ;totpartantssco;;')
 187    format(i8,6(';',1pe10.3),';;')
 188    format(' varia- ;Tmilieu;ttreflcont;ttsortcont;ttraies;',
     &  'g-pCompt;ttpartant;reflLy;tauLy'/'  tion%',8(';',0pf8.5)/
     &  ' ecart%;Temperature;Fcontrefl'/7x,2(';',0pf8.5)/)
        if(nitf.eq.1) WRITE(15,*)
               endif   ! nitf
cvaria- ;Tmilieu;ttreflcont;ttsortcont;ttraies;g-pCompt;ttpartant;reflLy;tauLy
c tion%;   -2.45;   -2.45;   -2.45;   -2.45;   -2.45;   -2.45;   -2.45;   -2.45
cecart%;Temperature;Fcontrefl
c      ;   -2.45;   -2.45
c                                                                            !m
        if(kimpf.eq.1.and.nitf.ne.1.and.ifich.ge.2.and.ifich.le.8)then
c        OPEN(UNIT=24,FILE=fidio,STATUS='old',position='append')
           WRITE(24,'(a78)') titre
           WRITE(24,580) densinit,dendex,htot,csit,incid,nitf
           WRITE(24,581) npt-1,csit,flindex,hnumin,hnumax
           WRITE(24,582) 'ab=',(ab(j),j=1,nelem)
                do m=1,npt-1
                mt(m)=m
                t1(m)=tz(m+1)-tz(m)
                   enddo   ! m
        WRITE(24,584) 'm= ',(mt(m),m=1,npt-1),npt,npt
        WRITE(24,582) 'nh=',(tnht(m),m=1,npt-1)
        WRITE(24,582) 'dh=',(t1(m),m=1,npt-1)
        WRITE(24,582) 'T= ',(temp(m),m=1,npt-1)
           k=0
           do j=1,nelem
              iz1=iz(j)+1
              do i=1,iz1
                 k=k+1
                 WRITE(24,583) j,i,(trij(m,k),m=1,npt-1)
                 enddo   ! i
              enddo   ! j
        j=2                                        ! niveau 2 de He+
        i=3
        k=k+1
                 WRITE(24,583) j,i,(trij(m,k),m=1,npt-1)
        WRITE(24,582) 'z= ',(tz(m),m=1,npt-1)
c
c           ndio=npt/nimpm
c              if(mod(npt,nimpm).gt.nimpm/2) ndio=ndio+1
c           WRITE(24,581) ndio,csit,flindex,hnumin,hnumax
c           WRITE(24,582) 'ab=',(ab(j),j=1,nelem)
c                mdeb=1
c                do mm=1,ndio
c                mt(mm)=mdeb+nimpm/2-1
c                t1(mm)=0.d0
c                t2(mm)=0.d0
c                t3(mm)=0.d0
c                mf=mdeb+nimpm
c                if(mf.gt.npt) mf=npt
c                mfin=mf-1                                                   !m
c                t1(mm)=tz(mf)-tz(mdeb)
c                   do m=mdeb,mfin
c                   ddh=tz(m+1)-tz(m)
c                   t2(mm)=t2(mm)+tnht(m)*ddh
c                   t3(mm)=t3(mm)+temp(m)*ddh
c                   enddo   ! m
c                mdeb=mfin+1
c                enddo   ! mm
c                do mm=1,ndio
c                   t2(mm)=t2(mm)/t1(mm)
c                   t3(mm)=t3(mm)/t1(mm)
c                enddo   ! mm
c        WRITE(24,584) 'm= ',(mt(mm),mm=1,ndio)
c        WRITE(24,582) 'nh=',(t2(mm),mm=1,ndio)
c        WRITE(24,582) 'dh=',(t1(mm),mm=1,ndio)
c        WRITE(24,582) 'T= ',(t3(mm),mm=1,ndio)
c           k=0
c           do j=1,nelem
c              iz1=iz(j)+1
c              do i=1,iz1
c                 k=k+1
c                 WRITE(24,583) j,i,(trij(mm,k),mm=1,ndio)
c                 enddo   ! i
c              enddo   ! j
           endif   ! imp dio
 580    format(' nht=',1pe9.2,'(-',f3.1,') htot=',1pe9.2,' csit=',1pe9
     &  .2,' incid=',i2,' iteration no',i4/'npt,csit,flindex,hnumin,',  !234567
     &  'hnumax then abund')
 581    format(i3,5(1pe10.3))
 582    format(a3,1000(1pe10.3))
 583    format(2i3,1000(1pe10.3))
 584    format(a3,1002i4)
c                                                                            !m
                     if(isuite.ge.1.and.kimpf.eq.1.and.nitf.ne.1) then
        OPEN(UNIT=25,FILE=fiime,STATUS='old')
        WRITE(25,96) nitf,log10(densinit),txt,idens,coltotlog,
     &               csitlog,flindex,hnumin,hnumax,incid,ijour
        do kc=1,322
           do m=1,npt
              t1(m)=tjc(kc,m)
              if(t1(m).lt.1.d-99) t1(m)=0.d0
           enddo
           WRITE(25,'(8(1pe10.3))') float(kc),(t1(m),m=1,npt)
        enddo    ! kc
        WRITE(25,96) nitf,log10(densinit),txt,idens,coltotlog,
     &               csitlog,flindex,hnumin,hnumax,incid,ijour
        do m=1,npt
        WRITE(25,'(i5)') m
        do 303 kr=1,nraimax
                if(fdo(kr).lt.fdolim) goto 303
        WRITE(25,'(8(1pe10.3))') float(kr),(tjrnu(lf,m,kr),lf=1,nf+1)
 303    continue   ! kr
        enddo    ! m
        CLOSE(25)
                     endif   ! isuite
c
c CHANGEMENT DE DECOUPAGE - grid change
        kchg=0
        if(mod(nitf,5).eq.0) kchg=1
        if(coltot.gt.1.d25.and.nitf.lt.14) kchg=0
        if(nitf.gt.40.and.mod(nitf,20).ne.0) kchg=0
        if(ichg.eq.0.or.kitf.eq.1.or.kchg.eq.0) goto 30002

c variations maximum d'un strate a l'autre - from one layer to the other
           raptmax=1.d0
           ro7max=1.d0
           rc5max=1.d0
           rhemax=1.d0
           ro3max=1.d0
           rc3max=1.d0
           ro2max=1.d0
           rc2max=1.d0
c           rs14max=1.d0
           mtmax=0
           momax=0
           mcmax=0
           memax=0
c           msmax=0
        do m=1,npt
              rapt=0.d0
           if(temp(m+1).gt.0.d0) rapt=temp(m)/temp(m+1)
              mm=m
              CALL sRAPPORTS(mm,mm+1,ro7,rc5,rne8,ro3,rc3,rhe,ro2,rc2,
     &                       ro7s,ro3s,ro2s)
c           if(mod(m,50).eq.0.or.m.eq.1.or.m.eq.npt)
c     &     write(6,'(i4,1p8e10.3)') mm,ro7,rc5,rne8,ro3,rc3,rhe,ro2,rc2
           if(rapt.gt.raptmax) then
              raptmax=rapt
              mtmax=m+1
           endif
           if(ro7.gt.ro7max) then
              ro7max=ro7
              momax=m
           endif
           if(rc5.gt.rc5max) then
              rc5max=rc5
              mcmax=m
           endif
           if(rhe.gt.rhemax) then
              rhemax=rhe
              memax=m
           endif
           if(ro3.gt.ro3max) then
              ro3max=ro3
           endif
           if(rc3.gt.rc3max) then
              rc3max=rc3
           endif
           if(ro2.gt.ro2max) then
              ro2max=ro2
           endif
           if(rc2.gt.rc2max) then
              rc2max=rc2
           endif
        enddo   ! m
        if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1) then
           if(ila.eq.0) then
        write(15,881)(raptmax-1.d0)*100.d0,mtmax,(rhemax-1.d0)*100.d0,
     &          memax,(ro7max-1.d0)*100.d0, momax
           else
        write(15,981)(raptmax-1.d0)*100.d0,mtmax,(rhemax-1.d0)*100.d0,
     &          memax,(ro7max-1.d0)*100.d0, momax
           endif
        endif
 881    format('     d''un strate a l''autre : variation max de la ',
     &  'temperature=',f6.1,'% a m=',i4/12x,'variation max de He+=',
     &  f6.1,'% a m=',i4,' et de OVII=',f6.1,'% a m=',i4)
 981    format('   from one layer to the other: variation max of ',
     &  'temperature=',f6.1,'% at m=',i4/11x,'variation max of He+='
     &  ,f6.1,'% at m=',i4,' and of OVII=',f6.1,'% at m=',i4)
c
        if(npt.ge.nzs-10) goto 30002
        if(raptmax.le.1.25d0.and.ro7max.le.2.d0.and.rc5max.le.3.d0
     &       .and.rhemax.le.2.d0.and.ro3max.le.3.d0.and.rc3max.le.3.d0
     &       .and.ro2max.le.3.d0.and.rc2max.le.3.d0) goto 30002

        npto=npt
        if(ichg.eq.1) then
           CALL CHAGRILLE1
        else
           CALL CHAGRILLE2
        endif
        if(npt.ne.npto) then
           OPEN(UNIT=10,FILE=fiato,STATUS='old',position='append')
           WRITE(10,*)
           if(ila.eq.0) then
              write(10,'(a,i5,a,i5)')
     &  'CHANGEMENT DE DECOUPAGE :',npt,' strates a l''iteration',nitf
              write(6,'(a,i5,a,i5,a)')
     &        '  CHANGEMENT DE DECOUPAGE de',npto,' a',npt,' strates'  
              if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &        write(15,'(a,i5,a,i5,a)')
     &        '  CHANGEMENT DE DECOUPAGE de',npto,' a',npt,' strates'
c write(23,'(a,i5,a,i5,a)')'CHANGEMENT DE DECOUPAGE de',npto,' a',npt,' strates'
           else
              write(10,'(a,i5,a,i5)')
     &           'GRID CHANGE :',npt,' layers at iteration',nitf
              write(6,'(a,i5,a,i5,a)')
     &        '   GRID CHANGE from',npto,'  to',npt,' layers'
              if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &        write(15,'(a,i5,a,i5,a)')
     &        '   GRID CHANGE from',npto,'  to',npt,' layers'
c write(23,'(a,i5,a,i5,a)')'   GRID CHANGE from',npto,'  to',npt,' layers'
           endif
           write(6,*)
           if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1) write(15,*)
           write(10,*)
     &     '       col                  nht             m       dcol'
           write(10,'(1pe23.15,1pe14.5,i10,1pe15.6)')
     &          (tcol(m),tnht(m),m,tcol(m+1)-tcol(m),m=1,npt)
           close(10)
        endif    ! changement
c
30002   continue
        CLOSE(14)
        CLOSE(16)
        CLOSE(17)
        CLOSE(35)
        CLOSE(18)
        CLOSE(19)
        CLOSE(20)
        CLOSE(21)
        CLOSE(22)
        CLOSE(23)
        CLOSE(24)
        CLOSE(26)
        CLOSE(29)
        CLOSE(31)
        CLOSE(32)
        CLOSE(33)
        CLOSE(34)
        CLOSE(40)
        if(ila.eq.0.and.nitf.ne.1) then
           write(6,780) pvrai,ecart*100.d0
        else if(nitf.ne.1) then
           write(6,781) pvrai,ecart*100.d0
        endif
        if(kitf.eq.1) goto 30001
c TEST de SORTIE des ITERATIONS en flux - variation d'une iteration a l'autre:
c ecart=ecart-type de dT/T(m), pvrai=pourcentage dFtotrai/Ftotrai*100
! output CHECKING for main iterations-variation from one iteration to the other
                       if(nitf.eq.1) goto 30000
        if(ikitf.eq.1.and.ecart.lt.1.d-6.and.abs(pvrai).lt.1.d-2)kitf=1
        if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1) then
           if(ila.eq.0) then
              write(15,780) pvrai,ecart*100.d0
           else
              write(15,781) pvrai,ecart*100.d0
           endif
        endif
 780    format(' variation du Ftot-raies:',f9.4,'% et de T:',f9.5,
     &         '% par rap. a la preced.it.'/)
 781    format(' variation of the sum of all line fluxes compared ',
     &         'with the previous iteration:'/f9.4,'%,',
     &         ' and averaged relative variation of T:',f9.5,'%'/)
        CLOSE(15)
        goto 30000
c
c FIN DE L ITERATION sur les flux - END of the MAIN ITERATION
30001        continue                                ! gagne !
c preparation du fichier du spectre continu+raies 
! making file with continuum+lines spectrum
        if(resol.gt.0.d0) then
        OPEN(UNIT=14,FILE=fires,STATUS='old',position='append')
c       OPEN(UNIT=30,FILE=fispe,STATUS='old',position='append') deja fait
        CALL GRAF(resol,Tmil)
        endif
c
        stop
        end                                                              ! main
c--============================================================================
        subroutine sNUAGE

c boucle sur les strates et si besoin calcul de la densite

       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200)
         double precision ne,nht
         character tab*1,txt*5
c         dimension tte(220),tben(220)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/temp/T,VT,tk,tk1,templog
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2)
       common/abond/ab(10)
       common/lecnu/sab,cmas
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/degmt/hautsom,dtaucmax,hnumaxto,                              !nu
     &            p_grait,p_gfree,p_gbound,g_pcompt,ptot,
     &            restflux(322),rtaucef(322),ttaucef(322),rtaucat(322)
       common/degmt2/exdtauc(322,0:nzs+1),exdtaur(16,nrais,0:nzs+1)
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/stnua/prad(nzs),pradrai(nzs),pradrai1(nzs),rhep2
       common/deglosm/pnenht,praieint,r(27,10)
       common/tabm/exflux(322),thnur(nrais)
       common/mec2/rayminrg,raymaxrg,hLbol,tauth,ndirlu
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/bb/Tbb,wbb
       common/comptonrai/dnuco(nrais),sourdnuco(2,nrais,nzs+1),
     &   ripdnuco(2,nrais),pripdnuco(2,nrais),rimdnuco(2,nrais,0:nzs+2)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/nutem/noconv,nbrnoconv
       save /deglosm/,/stnua/,/gnv/
! LC COMMON
! coltdrop is the column-density for which we compure p11 models below and
! p10 models above.
! it permits to force some intermediates models between p11 and p10.
! nitpg is the iteration number where extra Heating-Cooling curves are
! printed every iteration  to .pgt when ifich=7 or 8.
! lcrates contain recombination, photoionisation rates, defined in MAIN and
! sNUAGE, to print output
! alphaij(m,k) contains total recombination on each ion k and each layer m
! of last iteration (printed unit=87)       common/lcrates/alphaij(nzs,120)
       common/lcf/coltdrop
       common/lci/nitpg
c snuage
c        pi4=12.5663706144d0
c        tab=char(9)                                                !tabulation
c        tab=';'
c        ckev=8.6171d-5
        ni=nimpm
        nstrmax=nzs
        at=0.d0
                if(ni.eq.0.or.ni.gt.nstrmax) ni=50
c
           if(ifich.ge.1.and.ifich.le.8.and.nitf.eq.1.and.ila.eq.0)then
        WRITE(17,'(a)')
     &' pbT=1: PB deg.ion.  pbT=2: benefmin<0.1%  pbT=3: benefmin>0.1%'
        WRITE(17,'(a)')' pbT=4: T<8000K  ==> T=8000K'
c        WRITE(17,*)'pbT=4: T<6000 ou pbT=5: T>T(m-1)+10% ==> T=T(m-1)'
           endif   ! nitf=1
        if(mod(nitf,5).eq.0.or.nitf.eq.1.or.kitf.eq.1)
     &     WRITE(18,991) tab,tab,tab,tab,tab,tab
        if(kitf.eq.1) WRITE(14,991) tab,tab,tab,tab,tab,tab
        if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8)
     &  WRITE(17,91) tab,tab,tab,tab,tab,tab,tab,tab
 91     format('#   coldens            ',a,'nht        ',a,'T     ',a,
     &      'pbT ',a,'m    ',a,'z       ',a,'dcol   ',a,'Prad',a)
 991    format(/'#   coldens            ',a,'nht        ',a,'T     ',a,
     &      '  m    ',a,'z       ',a,'dcol',a)
        T=Tinit
          Ti1=Tinit
          tm1=Tinit
          rm1=raymin
          tm2=Tinit
          rm2=raymin
          hautsom=0.d0
          dilutot=1.d0
          nbrnoconv=0
          nfin=npt
          vart=0.d0
          nc=0
          col=0.d0
          cpg=1.38062d-16*(sab + ne/nht)                     ! pgaz=T*nht*cpg
c          cpg=1.38062d-16*(2.01d0 +
c     &    ab(2)*3.d0+ab(3)+ab(4)+ab(5)+ab(6)+ab(7)+ab(8)+ab(9)+ab(10))
          cturb=cmas*vturbkm**2
c          mdio=0
! set special mode idens=14: =8 if nitf<=20, if nitf>20: =11 if col<=coltdrop,
! =10 is col>coltdrop
!        print*, 'sNUAGE-1: nitf,m,col,idens,coltdrop=',
!     &    nitf,m,tcol(m),idens,coltdrop
 
        if ((idens.eq.14).and.(nitf.le.-1)) idens = 8
c
c calcul de la densite dans le CAS DE PRESSION TOTALE CONSTANTE idens=8,9   !nu
! density computation if CONSTANT TOTAL PRESSURE idens=8 or 9
       if(idens.eq.8.or.idens.eq.9) then
          do m= 1,npt
             if(m.le.2.or.nitf.eq.1) then
                nht=densinit
            else 
c pgaz=T*nht*cpg    cpg~2.25k    
c pturb=nht*mH*(1+ab(2)pmat(2)+...) *vturb**2/2 = nht*cturb
c n(m)(kT(m)*2.25 +cturb) + prad(m) = n0(kT0*2.25 +cturb) + prad0
                if(idens.eq.9) then
                   nht=(densinit*(temp(1)*cpg+cturb)+(prad(1)-prad(m)))
     &                  /(temp(m)*cpg+cturb)
                else
                   nht=(densinit*temp(1)+(prad(1)-prad(m))/cpg)/temp(m)
                endif   ! idens
c                                           if(nht.lt.densinit) nht=densinit
c                                           if(nht.lt.densinit/2.d0) then
                          if(nht.le.0.d0) then
                             write(6,92) m,prad(1),prad(m),tnht(m-1),nht
                             nht=tnht(m-1)
                          endif
             endif   ! m
             tnht(m)=nht
          enddo   ! m
       endif   ! idens=8,9
c
c BOUCLE sur les STRATES - LOOP on LAYERS                                   !nu
                                do 20000 m=1,npt
!LC set special mode idens=14: =8 if nitf<=20,
! if nitf>20: =11 if col<=coltdrop, =10 is col>coltdrop
        idens_old = idens
        if (idens.eq.14) then
             if (nitf.le.-1) then
                idens = 8
             else
                if (tcol(m).le.coltdrop) then
                   idens = 11
                else
                   idens = 10
                endif
             endif
        endif
!        print*, 'sNUAGE-2: nitf,m,col,idens_old,idens,coltdrop=',
!     &    nitf,m,tcol(m),idens_old,idens,coltdrop
!
        imppgt=0
        dilu=1.d0
c                             if(idilu.eq.1) dilu=(ray/(raymin+tz(m+1)))**2
           kimpm=0
        if(m.eq.1.or.m.eq.2.or.m.eq.(npt-1).or.m.eq.npt
     &                                     .or.mod(m,ni).eq.0) kimpm=1
c       if(m.ge.380.and.m.le.420.and.mod(m,5).eq.0) kimpm=1
c       if(m.eq.35.or.m.eq.50.or.m.eq.65.or.m.eq.80.or.m.eq.95.or.m.eq.
c     &    110.or.m.eq.125.or.m.eq.160.or.m.eq.200.or.m.eq.240) kimpm=1
c
          nitn=0
          kitn=1
        if(idens.eq.5.or.idens.eq.6.or.idens.eq.7) kitn=0
c debut de l'ITERATION EN DENSITE si idens=5 & 6 & 7
15000  nitn=nitn+1
       if(nitn.ge.30) kitn=1
c definition de nht - calculation of nht
       if(idens.le.1.or.idens.eq.8.or.idens.eq.9) then
          nht=tnht(m)      ! nht est defini ci-dessus sauf cas de pression cste
c
       else if(idens.eq.2.or.(idens.eq.5.and.nitn.eq.1)) then
c pression gazeuse cste, nht calcule avec m-1 (initialisation pour idens=5)
          if(m.le.2) then
             nht=densinit
             anht=densinit
          else
             nht=densinit*temp(1)/temp(m-1)
             anht=tnht(m-1)
          endif
          tnht(m)=nht
       else if(idens.eq.3.or.idens.eq.4.or.
     &   (idens.eq.6.and.nitn.eq.1).or.(idens.eq.7.and.nitn.eq.1)) then
c pression totale cste, nht calcule avec m-1 (initialisation pour idens=6,7)
          if(m.le.2) then
             nht=densinit
             anht=densinit
          else                              ! pgaz=T*nht*cpg      cpg~2.25k
c n(m)kT(m-1)*2.25 + prad(m-1) = n0kT0*2.25 + prad0
             pp=prad(m-1)
             nht=(densinit*temp(1)+(prad(2)-pp)/cpg)/temp(m-1)
c si on tenait compte de la pression de turbulence :
c n(m)kT(m-1)*2.25 + n(m)cturb + prad(m-1) = n0kT0*2.25 + n0cturb + prad0
c             nht=(densinit*(temp(1)*cpg+cturb) + (prad(2)-pp))
c     &               /(temp(m-1)*cpg+cturb)
c                                       if(nht.lt.densinit) nht=densinit
c                     if(nht.lt.densinit/2.d0) then
                          if(nht.le.0.d0) then
                             write(6,92) m,prad(2),pp,tnht(m-1),nht
 92                          format('PB a m=',i3,' pour NHT(prad1&m=',
     &                       1p2e10.3,')',1pe10.3,' remplace',1pe10.3)
                             nht=tnht(m-1)
                          endif
cPB a m= 23 pour nht(prad1&m= 2.456e+90 2.456e+90) 2.456e+90 remplace 2.456e+90
             anht=tnht(m-1)
          endif   ! m
          tnht(m)=nht
c
       else if(idens.eq.5) then
c pression gazeuse cste, nht calcule par iteration                          !nu
          if(m.le.2) then
             nht=densinit
             anht=densinit
          else
             nht=densinit*temp(1)/T
          endif
          anht=tnht(m)
          tnht(m)=nht
       else if(idens.eq.6.or.idens.eq.7) then
c pression totale cste, nht calcule par iteration
          if(m.le.2) then
             nht=densinit
             anht=densinit
          else                              ! pgaz=T*nht*cpg      cpg~2.25k
c n(m)kT(m-1)*2.25 + prad(m-1) = n0kT0*2.25 + prad0
             pp=prad(m)
             nht=(densinit*temp(1)+(prad(2)-pp)/cpg)/T
c si on tenait compte de la pression de turbulence :
c n(m)kT(m-1)*2.25 + n(m)cturb + prad(m-1) = n0kT0*2.25 + n0cturb + prad0
c             nht=(densinit*(temp(1)*cpg+cturb) + (prad(2)-pp))
c     &               /(T*cpg+cturb)
c                                       if(nht.lt.densinit) nht=densinit
c                                       if(nht.lt.densinit/2.d0) then
                          if(nht.le.0.d0) then
                             write(6,92) m,prad(2),pp,tnht(m-1),nht
                             nht=tnht(m-1)
                          endif
          endif
          anht=tnht(m)
          tnht(m)=nht
c
c++++++++++++++++++
c pertes et gains en fonction de T en vue de trouver les sol.froide et chaude
       else if(idens.ge.10.and.idens.ne.14) then
          if(nitf.eq.1.or.m.le.2) then
             nht=densinit
             tnht(m)=densinit
          else
             dent=densinit*temp(1) + (prad(1)-prad(m))/cpg
                          if(dent.le.0.d0) then
                 write(6,92) m,prad(1),prad(m),tnht(m-1),dent/temp(m-1)
                             dent=tnht(m-1)*temp(m-1)
                          endif
             if(idens.eq.10.or.idens.eq.20) then       ! solution froide
                qt=10.d0**(1.d0/dfloat(5))
c                qt=10.d0**(1.d0/dfloat(20))
                T0=7.d3/qt
                T=T0
                do ntemp=1,220
                   ta=T                                                    !nu
                   da=nht
                   ba=benef
                   T=T*qt
                   nht=dent/T
                   kitt=0                                 ! ne pas oublier
                   nitt=0
                   CALL DATOM
                   CALL sSTRATE
                   benef=g_pcompt-p_grait-p_gbound-p_gfree+chaufsup ! g-p
                   if(ntemp.eq.1.and.benef.le.0.d0) 
     &                  write(6,*)"anomalie: benef<0 a T petit"
                   if(benef.lt.1.d-23) qt=10.d0**(1.d0/dfloat(20))
                   if(benef.lt.4.d-24) qt=10.d0**(1.d0/dfloat(60))
                   if(benef.le.0.d0) then
c                      WRITE(6,280) m,ntemp,T0,qt,ta,da,ba,T,nht,benef
                      tb=T
                      bb=benef
                      T=(T*ba-ta*benef)/(ba-benef)
                      if(T.le.8000) then
                         T=8000
                         noconv=4
                         goto 21
                      endif
                      nht=dent/T
                      goto 20
                   endif   ! benef<0
                enddo   ! ntemp
             else if(idens.eq.11.or.idens.eq.21) then    ! solution chaude
                qt=10.d0**(1.d0/dfloat(60))
                T0=temp(m-1)*1.1d0
                if(m.eq.1) T0=3.d7*qt
                T=T0
                do ntemp=1,1500
                   ta=T                                                    !nu
                   da=nht
                   ba=benef
                   T=T/qt
                   if(T.le.8000) then
                      T=8000
                      noconv=4
                      goto 21
                   endif
                   nht=dent/T
                   kitt=0                                    ! ne pas oublier
                   nitt=0
                   CALL DATOM
                   CALL sSTRATE
                   benef=g_pcompt-p_grait-p_gbound-p_gfree+chaufsup ! g-p
                   if(ntemp.eq.1.and.benef.ge.0.d0) then
                      write(6,*)"anomalie: benef>0 a T grand"
                      if(T.gt.2.9e7) goto 21
                      T=3.d7
                      goto 22
                   endif
c                        if(benef.gt.-3.d-24) qt=10.d0**(1.d0/dfloat(60))
                   if(benef.ge.0.d0) then
c                      WRITE(6,280) m,ntemp,T0,qt,ta,da,ba,T,nht,benef
                      tb=T
                      bb=benef
                      T=(T*ba-ta*benef)/(ba-benef)
                      nht=dent/T
                      goto 20
                   endif   ! benef
 22                continue
                enddo   ! ntemp
c
             else if(idens.eq.12) then                 ! solution instable
                qt=10.d0**(1.d0/dfloat(60))
                T0=temp(m-1)*1.1d0
                if(m.eq.1) T0=3.d7*qt
                T=T0
                ib=-1
                t1=0.d0
                do ntemp=1,300
                   ta=T                                                    !nu
                   da=nht
                   ba=benef
                   T=T/qt
                   if(T.le.8000) then
                      if(T1.gt.0.d0) then    ! ici la solution est unique
                         txt='uniqu'
c                         WRITE(6,282)m,txt,ntemp,T
                         ta=ta1
                         ba=ba1
                         tb=T1
                         bb=b1
                         T=(T1*ba1-ta1*b1)/(ba1-b1)
                         nht=dent/T
c                         WRITE(6,*)'c interpol=> T,nht ',T,nht
                         goto 20
                      else
                         T=8000
                         noconv=4
                         goto 21
                      endif
                   endif
                   nht=dent/T
                   kitt=0                                    ! ne pas oublier
                   nitt=0
                   CALL DATOM
                   CALL sSTRATE
                   benef=g_pcompt-p_grait-p_gbound-p_gfree+chaufsup ! g-p
                             if(ntemp.eq.1.and.benef.ge.0.d0) then
c                                write(6,*)"anomalie: benef>0 a T grand"
                                if(T.gt.2.9e7) goto 21
                                T=T0*10.d0
                                T0=T                                       !nu
                                goto 23
                             endif
c                            if(benef.gt.-3.d-24) qt=10.d0**(1.d0/dfloat(60))
                   if(benef.ge.0.d0.and.ib.eq.-1) then ! sol.chaude ou unique
c                      txt='chaud'
c                      WRITE(6,282)m,txt,ntemp,T0,qt,ta,da,ba,T,nht,benef
                      ib=+1
                      ta1=ta
                      ba1=ba
                      da1=da
                      T1=T
                      b1=benef
                      d1=nht
                   endif
                   if(benef.le.0.d0.and.ib.eq.1) then  ! solution instable
c                      txt='INSTA'
c                      WRITE(6,282)m,txt,ntemp,T0,qt,ta,da,ba,T,nht,benef
                      tb=T
                      bb=benef
                      T=(T*ba-ta*benef)/(ba-benef)
                      nht=dent/T
                      goto 20
                   endif   ! benef
 23                continue
                enddo   ! ntemp                                            !nu
c
             endif   ! idens=10, 11 ou 12
 20          continue
             kitt=0                                          ! ne pas oublier
             nitt=0
             CALL DATOM
             CALL sSTRATE
             benef=g_pcompt-p_grait-p_gbound-p_gfree+chaufsup ! g-p

c             WRITE(6,*)'c call    => T,nht,benef ',T,nht,benef
             if(ba*benef.lt.0.d0) T=(T*ba-ta*benef)/(ba-benef)
             if(bb*benef.lt.0.d0) T=(T*bb-tb*benef)/(bb-benef)
c             WRITE(6,*)'interpol=> T,nht ',T,dent/T
 21          nht=dent/T
             at=temp(m)
             temp(m)=T
             tnht(m)=nht
          endif   ! m/2 et nitf/1
c
       endif   ! idens=10 11 12 20 21
c
c acceleration de la convergence de nht - acceleration of convergence of nht
       if(idens.eq.5.or.idens.eq.6.or.idens.eq.7) then
          if(mod(nitn,10).eq.4) den2=nht
          if(mod(nitn,10).eq.5) den1=nht
          if(mod(nitn,10).eq.6.and.kitn.eq.0) then
             d0=nht - den1
                if(d0.eq.0.d0) goto 10
             d1=den1 - den2
             if((d0.gt.0.d0.and.d1.gt.0.d0.and.d1-d0.gt.0.d0)
     &          .or.(d0.lt.0.d0.and.d1.lt.0.d0.and.d1-d0.lt.0.d0)) then
             den=(nht*d1-den2*d0)/(d1-d0)
             nht=den
             endif
c         if(m.le.10) write(6,'(2i3,1p7e13.5)') m,nitn,den2,den1,nht,den
             tnht(m)=nht
          endif
       endif   ! idens=5,6,7
 10    continue
c
c on a decoupe en colonne, il faut traduire en z - calculation of z         !nu
       if(idens.ge.2) then
          if(igrid.le.1.or.igrid.eq.10) then
             if(m.eq.1) then
                tz(m)=0.d0
                hautsom=tz(m)
                if(igrid.le.1) dhaut=dcol1/densinit
                if(igrid.eq.10) dhaut=tcol(2)/densinit
             else
                tz(m)=tz(m-1) + (tcol(m) - tcol(m-1))/tnht(m-1)
                hautsom=tz(m)
                dhaut=(tcol(m+1)-tcol(m))/tnht(m)
             endif
             if(m.eq.npt) then
                dhaut=0.d0        ! npt points de calcul
                htot=tz(npt)
                tz(npt+1)=tz(npt)
             endif
          else if(igrid.eq.2) then
c on a decoupe en z, il faut traduire en colonne - calculation of column
             if(m.eq.1) then
                tcol(m)=0.d0
                hautsom=tz(m)
                dhaut=tz(2)
             else
                tcol(m)=tcol(m-1) + (tz(m) - tz(m-1))*tnht(m-1)
                hautsom=tz(m)                                               !nu
                dhaut=(tz(m+1)-tz(m))
             endif
             if(m.eq.npt) then
                dhaut=0.d0      ! npt points de calcul
                htot=tz(npt)
                coltot=tcol(npt)
                tcol(npt+1)=tcol(npt)
             endif
          endif   ! igrid
c        else if(itemp.ge.11.or.igrid.eq.12) then
       else if(igrid.ge.12) then
           dhaut=tz(m+1)-tz(m)
           hautsom=tz(m)
       else   ! idens=0,1 ou igrid=10,11
           dhaut=(tcol(m+1) - tcol(m))/tnht(m)               ! =tz(m+1)-tz(m)
           hautsom=tz(m)
       endif   ! idens
c
c++++++++++++++++++
c pertes et gains en fonction de T          - supplement -                  !nu
c
c       OPEN(UNIT=40,FILE=fipgt,STATUS='old',position='append')
       if(ifich.ge.7.and.ifich.le.8.and.nitf.ge.nitpg.and.kimpf.eq.1
     &    .and.kimpm.eq.1.and.kitn.eq.1.and.m.ne.2.and.m.ne.npt-1)then
          imppgt=1
          if(m.eq.1) WRITE(40,93)
 93       format('# T  nitf  m  col nht gfree gion graie gcompt ',
     &    'gcomptx gcompts pfree ',
     &    'precf prsecdi pnetraip pcompt pertefluo praieint+pech benef')
             qt=10.d0**(1.d0/dfloat(30))
             T=7.d3/qt
c ou            if(m.eq.1) then
c              T=Tinit/10.d0
c           else
c              T=temp(m-1)/10.d0
c           endif
             do ntemp=1,110
c???           if(m.eq.1) kini=0
                T=T*qt
                if(ifich.eq.7) then
                   nht=tnht(m)
                else if(ifich.eq.8) then              ! a pression constante
                   if(m.eq.1.or.nitf.eq.1) then
                      nht=densinit*Tinit/T
                   else
                      nht=tnht(m)*temp(m)/T                               !nu
                   endif
                endif   ! ifich
                kitt=0          ! ne pas oublier
                nitt=0
                CALL DATOM
                CALL sSTRATE
c           benef=g_pcompt - p_grait - p_gbound - p_gfree + chaufsup ! g-p
c           WRITE(70,'(2i4,1p6e12.3)')
c     &     nitf,m,T,g_pcompt,p_grait,p_gbound,p_gfree,benef
             enddo   ! temp
          imppgt=0
       endif   ! ifich=7,8
c++++++++++++++++++
       if(nitf.gt.1.and.m.gt.2.and.(idens.ge.10.and.idens.ne.14)) then
          T=temp(m)
          nht=tnht(m)
          kitt=1
          CALL DATOM
          CALL sSTRATE
c         mettre datom et sstrate ici a la fin pour avoir les bons J
          benef=g_pcompt-p_grait-p_gbound-p_gfree+chaufsup ! g-p
c          if(ifich.ge.1.and.ifich.le.8)WRITE(6,281) T,nht,benef
 280    format(' m=',i3, ' nb.it.T=',i3,' Tinit=',1pe10.3,' q=',1pe10.4/
     &           6x,' T=',1pe10.3,' nht=',1pe10.3,' g-p=',1pe10.3/
     &           6x,' T=',1pe10.3,' nht=',1pe10.3,' g-p=',1pe10.3)
 281    format('   ==> T=',1pe13.6,' nht=',1pe13.6,' g-p=',1pe10.3)
 282   format(' m=',i3,a,' nb.it.T=',i3,' Tinit=',1pe10.3,' q=',1pe10.4/
     &           6x,' T=',1pe10.3,' nht=',1pe10.3,' g-p=',1pe10.3/
     &           6x,' T=',1pe10.3,' nht=',1pe10.3,' g-p=',1pe10.3)
          goto 15002
       endif   ! idens=10 ou 11                                           !nu
c
       nht=tnht(m)
       CALL sTEMP1
c sTEMP1 definit la temperature puis appelle DATOM puis sSTRATE - cas normal
c
       if(idens.eq.5.or.idens.eq.6.or.idens.eq.7) then
       if(m.eq.1.and.nitn.eq.1.and.ifich.ne.0.and.ifich.le.8
     &   .and.iexpert.eq.1) WRITE(15,*) 'm nitn   anht'//
     & '        nht         T           pgaz       prad(m)     ptot'
           pgaz=T*1.38062d-16*(ne+nht*sab)
           pturb=nht*cturb
       if(kimpm.eq.1.and.ifich.ne.0.and.ifich.le.8)
     &    WRITE(6,'(2i3,1p6e12.4)')
     &    m,nitn,anht,nht,T,pgaz,prad(m),pgaz+prad(m)+pturb
           if(kitn.eq.1) goto 15002
           if(abs(nht-anht)/nht.le.0.0003d0) kitn=1
           goto 15000
       endif   ! idens=5 6 ou 7
15002  continue ! sortie de l'iteration en densite - end of density iteration
c
        if(m.eq.1.and.T.le.8000.d0) then
           print*,
     &'STOP: T<8000K, sorry, Titan cannot manage so small illumination'
           stop
        endif
        if(nitd.ge.20.and.noconv.eq.0) noconv=1
        if(nbrnoconv.le.2.and.noconv.eq.0) nbrnoconv=0
                if(m.eq.1) Tinit=T
                if(nitf.eq.1) temp(m)=0.d0
                        if(noconv.lt.4) then
                if(at.le.1.d0)then
        vart=vart + ((T-temp(m))/T)*((T-temp(m))/T)                        !nu
                else
        vart=vart + ((T-at)/T)*((T-at)/T)
                endif
                nc=nc+1
                        endif
        temp(m)=T
                do kr=1,nraimax
                do lf=1,nf+1
        priplus(lf,kr)=riplus(lf,kr)
                enddo
        pripdnuco(1,kr)=ripdnuco(1,kr)
        pripdnuco(2,kr)=ripdnuco(2,kr)
                enddo
                do kc=1,322
        pciplus(kc)=ciplus(kc)
                enddo
        if(kimpf.eq.1) then
c
           k=0
           do j=1,nelem
              iz1=iz(j)+1
              do i=1,iz1
                 k=k+1
                 if(j.le.2.or.r(i,j).ge.1.d-40) then
c                     trij(mdio,k)=r(i,j)
                    trij(m,k)=r(i,j)
                 else
c                     trij(mdio,k)=0.d0
                    trij(m,k)=0.d0
                 endif
              enddo   ! i
           enddo   ! j
                 k=k+1
                 nij=nivcum(1,2) + 2
                 trij(m,k)=rhep2              ! He+ niveau 2
           ijmax=k
           endif   ! imp=5
c                                                                           !nu
        col=tcol(m)
        dcol=tcol(m+1)-tcol(m)
        if(igrid.eq.2.or.igrid.eq.13) dcol=(tz(m+1)-tz(m))*tnht(m)
        if(mod(nitf,5).eq.0.or.nitf.eq.1.or.kitf.eq.1) WRITE(18,382) 
     &         col,tab,nht,tab,T,tab,m,tab,tz(m),tab,dcol
        if(kitf.eq.1) WRITE(14,382) col,tab,nht,
     &         tab,T,tab,m,tab,tz(m),tab,dcol
        if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) WRITE(17,381) col,
     &     tab,nht,tab,T,tab,noconv,tab,m,tab,tz(m),tab,dcol,tab,prad(m)
 381    format(1pe20.13,a,1pe10.3,a,1pe10.3,a,i2,a,i3,a,1pe9.3,a,
     &         1pe9.3,a,1pe9.3)
 382    format(1pe20.13,a,1pe10.3,a,1pe10.3,a,i3,a,1pe10.3,a,1pe10.3)
c
        if(m.ge.npt.and.m.le.nstrmax) goto 20001 
        if(m.ge.nstrmax) then
                write(6,*)' NOMBRE DE STRATES TROP GRAND !'
                if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &          write(15,*) ' NOMBRE DE STRATES TROP GRAND !'
                goto 20001
                endif
        tm1=T                                                               !nu
! special mode idens=14 to set the real idens(m) properly
20000   continue                             ! fin boucle sur les strates m
        if (coltdrop.gt.0.d0) idens = 14 !???? avant??? apres???
! Loic: 20000 est la fin de boucle do- ne pas mettre 20000 if (coltdrop.gt....
c
20001   continue                             ! fin: R=Raymax ou m=nstrmax
        if(kitf.eq.1) WRITE(14,*) 
        if(kimpf.eq.1.and.ifich.ne.0.and.ifich.le.8) then
           if(ila.eq.0) WRITE(17,*)
           if(ila.eq.1) WRITE(17,'(a)') '#'
        endif
        ecart=sqrt(vart/nc)
        if(nbrnoconv.ge.15.and.nitf.ge.50.and.tz(nfin).lt.htot/4.)kitf=1
        return
        end                                                             !snuage
c
c                                                                       !snuage
c------------------------------------------------------------------------------
        subroutine sTEMP1

c calcul de la temperature par iteration
c methode de la secante (d'apres Numerical Receipes p.248)

       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200)
         double precision ne,nht
         dimension ff(10)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/temp/T,VT,tk,tk1,templog
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/degmt/hautsom,dtaucmax,hnumaxto,                             !te1
     &            p_grait,p_gfree,p_gbound,g_pcompt,ptot,
     &            restflux(322),rtaucef(322),ttaucef(322),rtaucat(322)
       common/bb/Tbb,wbb
       common/nutem/noconv,nbrnoconv
       save /nutem/
       data ff/2.d0,3.d0,4.d0,2.5d0,3.5d0,2.3d0,2.8d0,3.3d0,3.8d0,2.1d0/
c stemp1
        nittmax=50
        ba=0.d0                                        ! Agata 
        ta=0.d0                                        ! Agata

        if(idens.ge.3) nittmax=20
        if(nitf.eq.1.and.m.eq.1) T=Tinit               ! meme si isuite=3
        if(nitf.gt.1.and.m.eq.1) T=temp(1)
        if(m.ge.2) T=temp(m-1)
 999    nitt=0
        nitt1=0
        kitt=0
        vbenmin=1.d0
        noconv=0                                                           !te1
        ie=0
c          if(m.eq.1) kini=0
          if(m.ge.2) Ti1=T
               if(incid.eq.0.or.itemp.eq.10) then
               T=Tinit
               kitt=1
               endif
                    if((nitf.eq.1.and.isuite.eq.5).or.itemp.ge.11) then
                    T=temp(m)
                    kitt=1
                    endif
                         if((itemp.eq.1)
     &                      .and.tz(m).gt.htot*0.98d0) then
                         T=temp(m-1)
                         kitt=1
                         endif
c
c debut de l'ITERATION EN TEMPERATURE - beginning of iterations on temperature
11000   nitt=nitt+1                            ! changement de temperature
        nitt1=nitt1+1
c        if(nitt.eq.2) kini=1
        if(m.eq.npt.and.kitt.eq.1) kimpm=1     ! dernier strate et dernier tour
c
c calcul des donnees atomiques dependant de la temperature                 !te1
! calculation of atomic data depending on temperature
        CALL DATOM
c
c calcul des DEGRES D IONISATION par iterations ainsi que des pertes et gains
! calculation of ionisation degrees and heating and cooling
        CALL sSTRATE
c
c DATOM et sSTRATE sont appeles a chaque temperature de chaque strate
! DATOM et sSTRATE are called for each temperature of each layer
c
        benef=g_pcompt - p_grait - p_gbound - p_gfree + chaufsup      ! g-p
        pbenef=benef/ptot *100.d0
             if(abs(benef).lt.vbenmin) then
            vbenmin=abs(benef)
            tbenmin=T
            pbenefmin=pbenef
            endif
        if(imp.eq.4) then
                WRITE(6,280) m,T,nitt,nitd,pbenef
c               WRITE(6,283) g_pcompt,p_gbound,p_gfree,p_grait
c                WRITE(16,280) m,T,nitt,nitd,pbenef,dhaut
                endif                                                      !te1
  280   format(' m=',i4,' T=',1pe13.6,' it.temp.no',i3,' nbr.it.deg.=',
     1        i3,' g-p%/p=',1pg10.3)
  283   format(' g-p Compton=',1pe10.3,
     2     ' p-g bound=',1pe10.3,' &free=',1pe10.3,' pgtrai=',1pe10.3)
c
        if(kitt.eq.1) goto 11001                ! sortie de l'iteration en T
c
        tlim=0.002d0
        pblim=0.01d0
        if(idens.eq.3.or.idens.eq.6.or.idens.eq.7) then
           tlim=0.01d0
           pblim=0.05d0
        endif
c        if(kitt.eq.0.and.nitt1.ge.21) tlim=0.01d0
c        if(kitt.eq.0.and.nitt1.ge.21) pblim=0.5d0
        if(nitt1.gt.2.and.
     &        (abs(T-ta)/T).lt.tlim.and.abs(pbenef).lt.pblim) kitt=1
c
c calcul du T suivant, par interpolation lineaire
c
        taa=ta
        baa=ba
        ta=T
        ba=benef
                if(nitt1.eq.1.and.kitt.eq.0) then       ! premier essai
                        ptt=1.01d0                                         !te1
                        if(m.eq.1) ptt=1.1d0
                        T=ta/ptt
                        if(benef.gt.0.d0) T=ta*ptt
                else if(kitt.eq.1.and.nitt1.ge.21) then
                        T=ta
                else                                         ! cas normal
        if((baa-ba).ne.0.d0) T=(baa*ta-ba*taa)/(baa-ba)
c        if (T.lt.0.d0) then   ! solution for T<0   by Agata 
c          write(*,*) m,T      ! solution only for benef < 0
c          T=ta/2.d0
c          ta=0.d0
c          ba=0.d0
c          goto 999
c        endif               !  by Agata
                endif        ! nitt1
c        write(*,*) 'change ? ',' T ',T,'nitt1',nitt1,'baa',baa      ! Agata
c     &          ,' ta ',ta,
c     &          ' ba ',ba,' taa ',taa,' benef ',benef,
c     &          ' mianownik ',baa-ba,' licznik ',(baa*ta-ba*taa)
c                 pause 
                if(kitt.eq.1.and.T.lt.8000.d0) then
                   noconv=4
                   if(irent.eq.0) then
        write(6,*) 'm=',m,' T=',T,' trop petit apres',nitt,'it: T=8000'
        if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1) write(15,*)
     &             'm=',m,' T=',T,' trop petit apres',nitt,'it: T=8000'
                      T=8000.d0                               ! chang. v42k
                   else
                      T=Tbb
                   endif
                endif   ! T<8000
c
c problemes de convergence ! convergence problems
c
c cas pas normal : T hors bornes   !  Temperature out of boundaries
                      if(m.eq.1) then
                         tm1=Tinit/10.d0
                      else
                         tm1=temp(m-1)
                      endif
                      if(T.gt.1.1d9.or.T.lt.2.d3
     &                .or.(m.gt.1.and.T.lt.tm1/100.d0)) then
                      Ti1=Ti1/1.5d0
                            if(Ti1.lt.2.d3) then
                            ie=ie+1
                            Ti1=tm1*ff(ie)          ! tm1=T du strate precedent
                            endif
c                      write(6,281) m,nitt,T,Ti1
c                      write(15,281) m,nitt,T,Ti1
                      T=Ti1
                      nitt1=0
                      endif ! T
  281   format(' m=',i4,' nitt=',i3,' T=',1pe10.3,
     &                                ' nouveau T initial=',1pe10.3)
c nombre d'iterations trop eleve :
        if(nitt.ge.nittmax) then
            kitt=1                                                         !te1
            T=tbenmin
                if(abs(pbenefmin).le.0.1d0) then
                   noconv=2
                else
                   noconv=3
                   nbrnoconv=nbrnoconv+1
                endif
            write(6,181) m,nittmax,tbenmin,pbenefmin
              if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &      write(15,181) m,nittmax,tbenmin,pbenefmin
           if(irent.ge.1.and.m.gt.250.and.T.lt.Tbb.and.wbb.gt.0.9d0)then
                      T=Tbb
                      noconv=6
                      write(6,*) 'm=',m,' T ne converge pas: T=Tbb=',T
                      if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &                write(15,*) 'm=',m,' T ne converge pas: T=Tbb=',T
                      endif
            endif   ! nitt=nittmax                                         !te1
  181       format(' m=',i3,':T ne converge pas en',i3,
     &      'it., T=',1pe10.4,' (gains-pertes minima',0pf7.2,'%/p)')
c
        if(nitt.ge.nittmax.and.T.lt.8000.d0) then
             T=8000.d0                      ! chang. v36n
c             T=6000.d0                      ! chang. v34n=36c
c             T=tm1                         ! tm1=T du strate precedent
             noconv=4
             kimpm=1
             write(6,*) 'm=',m,' T trop petit apres',nitt,'it: T=',T
               if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &       write(15,*) 'm=',m,' T trop petit apres',nitt,'it: T=',T
             endif   ! T<8000
        goto 11000                          ! vers le debut de l'iteration en T
c                                                                          !te1
c resultats pour un strate - results at the end of iterations on T
11001        continue                              ! sortie de l'iteration en T
        return
        end                                                             !stemp1
c--============================================================================
        subroutine sSTRATE

c calcul des DEGRES D IONISATION et des coefficients d'absorption et d'emission
c par iteration
c
c j est le numero de l'element X (H,He,C,N,O,Ne,Mg,Si,S,Fe)
c i est l'etat d'ionisation(1 pour X0, 2:X+, 3:X++, Z:hydrogenoide, Z+1:ion nu)
c n est le numero du niveau dans l'ion - nij pour tous les ions     n<11
c
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15,ndirs=20)
         double precision ne,nht
       dimension dir(nrais),pij(27,11)
       dimension itz(6),iti(6),t1(6),t2(6),t3(6),t4(6),t5(27),ikr(nrais)
         character tab*1,txt*13
       common/abond/ab(10)
       common/lecnu/sab,cmas
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/raies2/g1rt(nrais),g2rt(nrais),n1rt(nrais),n2rt(nrais)
       common/raies3/A21(nrais),A21hnu(nrais),A21hnus4pi(nrais),
     &               B12(nrais),B12hnus4pi(nrais),B12hnu2s4pi(nrais)
       common/raiesuta/probuta(100),dnuta2(100),utanion(26,10),
     &                 juta(100),iuta(100)
       common/raiesadd/is(3200),js(3200)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/im/fii,itali
       common/temp/T,VT,tk,tk1,templog
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/stdeg/popsec(26,10),dpech(26,10),ecartdeg,nitdmax,
     &              jmp1(10),jmp2(10)
       common/stpho/dabsh,demh,dabshep,demhep,dic(322)
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2)
       common/fsource/sourcec(322,nzs+1),sourcer(16,nrais,nzs+1)
       common/degmt/hautsom,dtaucmax,hnumaxto,                                !s
     &          p_grait,p_gfree,p_gbound,g_pcompt,ptot,
     &          restflux(322),rtaucef(322),ttaucef(322),rtaucat(322)
       common/degmt2/exdtauc(322,0:nzs+1),exdtaur(16,nrais,0:nzs+1)
       common/sst/dtaurmax0,dtaurmaxH,krtormax0,krtormaxH
       common/flx/flxdurincid,acompt,bcompt,ccompt,dcompt,ecompt,fcompt,
     &            scomptx1,scomptx2,ftauth
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/lecdeg/eps1,eps2,sel
       common/deglosm/pnenht,praieint,r(27,10)
       common/datdeg/alpha(26,10),alfdi(26,10),edi(26,10),cio(nivtots),
     &          ech(26,10),eche(26,10),alfrad(26,10),
     &          teec(220),tgaufrec(220)
       common/dastpo/alfint(nivtots),cioe(nivtots),
     &          texcoll(nrais,nzs+1),teer(nrais,nzs)
       common/talfdeg/sigmac(220,nivtots),sigmacv(220,nivtots),
     &          sigmack(220,nivtots),sigmark(nrais,nivtots)
       common/talfdeg2/exphot(nivtots),exphotk(nivtots)
       common/stdalf/tekc(220,nivtots)
       common/tabstpo/sigmar(nrais,nivtots)
       common/tabm/exflux(322),thnur(nrais)
       common/mstpo/rtaur0(nrais),rtaurl(nrais),
     &          twr(nrais),tws(nrais),uhz(nrais),cjn(nrais)
       common/stpo/tphot(nivtots),tphotk(26,10),dens(nivtots),
     &          alfinduit(nivtots),zfree,cthomson
       common/pgtt/ttpraie,ttgraie,ttpertefluo,ttpraieint,ttpgrai,
     &          ttpech,ttpfree,ttprecf,ttprsecdi,ttgfree,ttgion,
     &          ttgpcompt,ttgcomptx,rtauthx
       common/pgtt2/ttpgray                    !yyy
       common/lyman/rsomtely,rsomely,ssomtely,ssomely
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/stnua/prad(nzs),pradrai(nzs),pradrai1(nzs),rhep2
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/pop/po1(nivmax+1,26),sompo1(26),f(27,10),
     &             ph0ne,phpne,phe0ne,phepne,sech1,sech2,seche1,seche2
       common/stpg/pfree,gfree,precf,gion,precsec,pdiel,pech,
     &             scompt1,scompt2,sjdnuev
       common/comptonrai/dnuco(nrais),sourdnuco(2,nrais,nzs+1),
     &   ripdnuco(2,nrais),pripdnuco(2,nrais),rimdnuco(2,nrais,0:nzs+2)
       common/comptonrai2/ncomp(nrais),mdebcomp(nrais)
       common/cpal/temcac(322,nzs),tcapc(322,nzs),tcth(nzs),temc(322),
     &             pradc(nzs)
       common/rpal/twnu(16,nrais,nzs),tphi(16,nrais,nzs),
     &        tcapr(nrais,nzs),dcapr(nrais,nzs),dsl(nrais,nzs),
     &        tas(nrais,nzs),tbs(nrais,nzs),tne(nzs+1),tdensf(nzs,26,10)
       common/rpal2/kali(nrais),npbali(nrais),lcompt(nzs,nrais)
       common/sporpal/tjr(nrais,nzs),tcjn(nrais,nzs),pradr(nrais,nzs)
       common/mgrpal/tflincident,fsor(2,nrais),frer(2,nrais),
     &               fasor(ndirs,nrais),farer(ndirs,nrais)
       common/strat2/nerrpho
          save /atom/,/nuamd/,/lecdeg/,/deglosm/,/datdeg/,/talfdeg/,
     &         /stnua/,/tabstpo/,/stpo/,/dastpo/,/stdalf/,/strat2/,
     &     /raies2/,/raies3/,/gnv/,/pop/,/cpal/,/rpal/,/rpal2/,/sporpal/
c                                                      combinaison tau pt & gd
c sstrate
c        tab=char(9)                                     ! tabulation
c        tab=';'
c        pi=3.1415926535898d0
c        pi4=12.5663706144d0
c        v3=sqrt(3.d0)
c        Vpi=1.77245385091d0
c        pi2v3=6.28318530718d0/V3                        ! 2pi/V3
c        chel=1.602192d-12                               ! e'
c        hcse=12398.54d0                                 ! h*c/e'*1e8
c        hcsb=1.43884d8                                  ! h*c/k en angstrom
c        hca=1.986485d-8
c        ckev=8.6171d-5                                  ! k/e'
c        bhca=0.02654d0*1.986485d-8                      ! 0.02654*h*c angstrom
c        esh=2.41797d14                                   ! e'/h : nu eV->Hz
c        tk=ckev*T
c        tk1=1.d0/tk
        saha=2.07079d-16/T/VT
        hicompt=float(icompt)
c        nraissfek=nrcum(26,10)
           k4=nrcum(1,2)+1       ! He2-1  krimp=4
           k5=nrcum(2,3)+1       ! C3-1         5
           k6=nrcum(3,3)+1       ! C4-1         6
           k7=nrcum(3,5)+2       ! O4-2         7
           k8=nrcum(5,5)+1       ! O6-1         8
           k9=nrcum(7,5)+1       ! O8-1         9
           k10=nrcum(14,10)+1    ! Fe15-1       10
           k11=nrcum(22,10)+1    ! Fe23-1       11
           k12=nrcum(24,10)+2    ! Fe25-w       12
           k13=nrcum(26,10)+15   ! FeK15        13
           k14=nrcum(26,10)+24   ! FeK24        14
c tk1=1/kT en ev      saha=1/constante de la loi de saha=1/2 *h3/(2pimekT)**3/2
        itali=1
           if(nitf.eq.1) itali=0 
c utilisation du fii indispensable pour le 1er passage
c
           nitdmax=20
           if(incid.eq.0.or.csit.eq.0.d0) nitdmax=50
           nitd=0
           kitd=0
           kitd1=0
c debut de l'ITERATION SUR LES DEGRES D IONISATION
! beginning of the ITERATION on the IONISATION DEGREES
12000   continue
           nitd=nitd+1
           if(nitd.ge.nitdmax) kitd=1
        pech=0.d0
        CALL DEGION(kitd1)
           hnne=nht*ne
           tcth(m)=cthomson                                                 !sd
c
c CONTINU: 
c------------
c traitement des X durs (hnuev>26kev) - hard X-ray
c ici le traitement des X durs au dessus de 26kev est global
                       if(kitd.eq.1) then
c       if(igrid.ge.10.or.itemp.eq.12
c     &    .or.(itemp.ge.2.and.itemp.le.9)) goto 510
       if(idens.ge.2.or.(igrid.ge.10.and.igrid.ne.13)) goto 510
                if(idens.eq.0) then
       rtauthx=nht*sel *tz(m) *0.6652d-24
                else if(dendex.eq.1.d0) then
       rtauthx=densinit*raymin*log((raymin+tz(m))/raymin)*sel*0.6652d-24!234567
                else
       rtauthx=densinit*Raymin/(dendex-1.d0)*(1.d0-(Raymin/
     1         (Raymin+tz(m))) **(dendex-1.d0)) *sel *0.6652d-24
                endif   ! dendex
 510   continue   ! igrid,idens
           gcomptx=0.d0
       if(acompt.gt.0.d0.and.bcompt.gt.0.d0) gcomptx=
     &     (acompt*expa(-bcompt*rtauthx)+ccompt*expa(-dcompt*rtauthx)
     &     +ecompt*expa(-fcompt*rtauthx)) *flxdurincid*sel*0.6652d-24/ne
                       endif   ! kitd
c------------
        if(ifich.ge.3.and.ifich.le.8.and.kimpf2.eq.1.and.kitt.ge.1.and.
     &  kitd.eq.1.and.kimpm.eq.1.and.(imp.eq.5.or.imp.eq.6.or.imp.eq.7))
     &  WRITE(16,580)
  580   format(/' CONTINU    gain et perte divises par de'
     1         /2('  hnu(eV) dtauc   gain   pertebf  pfree'))               !sc
c
        pfree=0.d0
        gfree=0.d0
        precf=0.d0
        precsec=0.d0
        pdiel=0.d0
        gion=0.d0
c        pcfap=0.d0
        scompt1=0.d0
        scompt2=0.d0
        sjdnuev=0.d0
        sjdnuevrai=0.d0
        dtaucmax=0.d0
c         dtaucmin=1.d250
        rapmax=0.d0
        mimp=0
        sbt=0.d0
        skbt=0.d0
c        s2jdnu=0.d0
c        sfdnu=0.d0
c        skfdnu=0.d0
                do k=1,322
        dic(k)=0.d0
                enddo
c
                             do 511 kc=1,220
c                                                                           !sc
c    l'integration sur les frequences se fait par la methode des trapezes :
c    I = y1(x2-x1)/2 + ... + yi(x(i+1)-x(i-1)) + ... i.e.
c    I = y1*deb + ....... + yi*de ou tdhnu ou hnu*2*(q-1)/q+1)
c      toutes les frequences sont exprimees en ev : frequences hnuev egales aux
c      kifond=pkiev de 7.64 a 3700 ev, + qques valeurs pour les "trous", et en
c     progression geometrique de facteur 1.2 et 1.1 en dehors de cet intervalle
c                            dhnuev note' en raccourci de
! quadrature with an extended trapezoidal method - all photon energies in eV.
! 322 photon energies are chosen to be equal to ground ionisation energies + 
! energies just before the threshold + other values in geometrical progression
          hnuev=thnuc(kc)
          de=tde(kc)
          deb=tdeb(kc)
          dea=de-deb
                        if(kc.le.102) then
c ces frequences sont egales a des energies d'ionisation du fondamental d'ions
          ic=ict(kc)
          jc=jct(kc)
                        endif                                               !sc
c
c calcul des coef.d'absorption et des emissivites et
c          de dic=4piJdnu/hnu pour obtenir le nombre de photoionisations
! computation of kappas & emissivities & "dic" to get the photoionis. number
c  I+ et I- (au point z), J= (I+ + I-)/2, S=(em+cth*J)/(cap+cth)
c
        CALL PHOCONT(kc)
c
                if(kitd.eq.0) goto 511
c
c pertes et gains du continu - heating and cooling of CONTINUUM
c bound-free, free-free et Compton
c
        CALL PGCONT(kc,jc,ic)
c
                if(kitt.eq.0.or.kitn.eq.0) goto 511
c
c au point z, si les iterations d & T ont terminees, ces valeurs sont
c communiquees a la subroutine cpaletou qui calcule le J :
! if iterations are finished, these values are sended to subroutine cpaletou
        if(nitf.eq.1) sourcec(kc,m)=
     &     (temc(kc) + cthomson *tjc(kc,m))/(tcapc(kc,m) + cthomson)
        temcac(kc,m)=temc(kc)/tcapc(kc,m)
        dtoeff=dhaut*sqrt(tcapc(kc,m)*(tcapc(kc,m)+cthomson))
        if(incang.eq.0) dtoeff=dtoeff*V3
        rtaucef(kc)=rtaucef(kc) + dtoeff
        rtaucat(kc)=rtaucat(kc) + dhaut*(tcapc(kc,m)+cthomson)
        if(incang.eq.0)
     &  rtaucat(kc)=rtaucat(kc) + dhaut*V3*(tcapc(kc,m)+cthomson)
        restflux(kc)=restflux(kc)*expa(-dhaut*tcapc(kc,m))          ! 8nov1999
                if(kc.le.102) then
        if(nitf.eq.1) sourcec(kc+220,m)=(temc(kc+220)
     &     +cthomson*tjc(kc+220,m))/(tcapc(kc+220,m)+cthomson)
        temcac(kc+220,m)=temc(kc+220)/tcapc(kc+220,m)
        dtoeffa=dhaut
     &        *sqrt(3.d0*tcapc(kc+220,m)*(tcapc(kc+220,m)+cthomson))
        rtaucef(kc+220)=rtaucef(kc+220) + dtoeffa
        rtaucat(kc+220)=rtaucat(kc+220) 
     &        + dhaut*V3*(tcapc(kc+220,m)+cthomson)
c    restflux(kc+220)=restflux(kc+220)*expa(-dhaut*(tcapc(kc+220,m)+cthomson))
        restflux(kc+220)=restflux(kc+220)*expa(-dhaut*tcapc(kc+220,m)) !8nov1999
                endif        ! kc<102
c
c IMPRESSIONS du CONTINU si les iterations d & T ont terminees
! writings for continuum if iterations are finished
        if(kimpf2.eq.1.and.ifich.eq.9.and.kc.eq.1.and.m.ne.npt)
     &       WRITE(21,*) m,tcol(m)
        if(imp.ge.1.and.kc.eq.1.and.kimpm.eq.1) then
           if(kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.8) then
              WRITE(21,*)
              WRITE(21,*) '  m; nitf  ;T        ',
     &                ';dhaut     ;cthomson   ;zfree   ;tphot(1)  ;z'
              WRITE(21,586) m,nitf,T,dhaut,cthomson,zfree,tphot(1),tz(m) !sc
           endif   ! nitf
           if(ifich.ge.4.and.ifich.le.8.and.kimpf.eq.1) then
              WRITE(22,*)
              WRITE(22,*) ' m=',m
           endif   ! ifich
        endif   ! imp
  586   format(i4,';',i3,6(';',1pe10.3))
7771    format(i4,8(1pe9.2)/i4,8(1pe9.2))
c
                if(kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.8) then
        if(imp.ge.1.and.kc.eq.1.and.kimpm.eq.1) WRITE(21,*)'  kc   ;I+',!2345678
     &  '       ;emm     ;capabs   ;dtauc    ;rtaucef  ;source'
        if(imp.ge.1.and.kimpm.eq.1.and.(kc.eq.1.or.kc.eq.3.or.kc.eq.24
     &        .or.kc.eq.122.or.kc.eq.138.or.kc.eq.156
     &  .or.kc.eq.182.or.kc.eq.101)) WRITE(21,583) 
     &  kc,ciplus(kc)+sourcec(kc,m)*(1.d0-exdtauc(kc,m-1))/2.d0,
c     &  cimoins(kc,m+1)+sourcec(kc,m)*(1.d0-exdtauc(kc,m))/2.d0,
     &  temc(kc),tcapc(kc,m),dhaut*V3*(tcapc(kc,m)+cthomson),
     &  rtaucef(kc)-dtoeff,sourcec(kc,m)                                    !sc
                endif   ! nitf
                if(kimpf.eq.1.and.ifich.ge.4.and.ifich.le.8) then
        if(imp.ge.1.and.kc.eq.1.and.kimpm.eq.1) WRITE(22,*)'  kc   ;I+',
     &  '       ;emm     ;capabs   ;dtauc    ;rtaucef  ;source'
        if(imp.ge.1.and.kimpm.eq.1.and.(kc.eq.1.or.kc.eq.3.or.kc.eq.24
     &  .or.kc.eq.122.or.kc.eq.138.or.kc.eq.156
     &  .or.kc.eq.182.or.kc.eq.101)) WRITE(22,583) 
     &  kc,ciplus(kc)+sourcec(kc,m)*(1.d0-exdtauc(kc,m-1))/2.d0,
c     &  cimoins(kc,m+1)+sourcec(kc,m)*(1.d0-exdtauc(kc,m))/2.d0,
     &  temc(kc),tcapc(kc,m),dhaut*V3*(tcapc(kc,m)+cthomson),
     &  rtaucef(kc)-dtoeff,sourcec(kc,m)
                  endif
  583   format(i4,7(';',1pe9.2))
c
cv transfert du rayonnement : au strate suivant on aura :
cv                                I+(m+1)=nouveau cip + source(m+1)/2
                ddt=(1.d0-exdtauc(kc,m)*exdtauc(kc,m-1))/2.d0
        ciplus(kc)=(pciplus(kc)*exdtauc(kc,m)+sourcec(kc,m)*ddt)
        bt=hnuev*hnuev        ! pas la peine de * par 2*k/c2 *e'3/h3=4.343255d6
        if((1.d0-ee).gt.1.d-6) bt=bt*ee *(hnuev*tk1/(1.d0-ee))**2
           de1=de                                                           !sc
           if(kc.le.102) de1=deb
        sbt=sbt + bt*de1
        skbt=skbt + bt*de1/(tcapc(kc,m) + cthomson)
        hicp=pciplus(kc) + sourcec(kc,m)*(1.d0-exdtauc(kc,m-1))/2.d0
c        hicm=cimoins(kc,m+1) + sourcec(kc,m)*(1.d0-exdtauc(kc,m))/2.d0
c        s2jdnu=s2jdnu + de1*tjc(kc,m)*2.d0
c        sfdnu=sfdnu + de1*(hicp-hicm)   ! F n'est pas calcule pour le continu
c        skfdnu=skfdnu + (tcapc(kc,m)+cthomson)*de1*(hicp-hicm)
c
                if(kc.le.102) then
c
        if(imp.ge.1.and.kimpm.eq.1.and.kimpf2.eq.1.and.ifich.ge.4.and.
     &     ifich.le.8.and.(kc.eq.1.or.kc.eq.3.or.kc.eq.24.or.kc.eq.101))
     &  WRITE(21,583) kc+220,ciplus(kc+220)+sourcec(kc+220,m)*(1.d0-
     &  exdtauc(kc+220,m))/2.d0,
c cimoins(kc+220,m+1)+sourcec(kc+220,m)*(1.d0-exdtauc(kc+220,m))/2.d0,
     &  temc(kc+220),tcapc(kc+220,m),dhaut*V3*(tcapc(kc+220,m)+cthomson)
     &  ,rtaucef(kc+220)-dtoeffa,sourcec(kc+220,m)
                if(ifich.ge.4.and.ifich.le.8) then
        if(kimpf.eq.1.and.imp.ge.1.and.kimpm.eq.1
     &     .and.(kc.eq.1.or.kc.eq.3.or.kc.eq.24.or.kc.eq.101))
     &  WRITE(22,583) kc+220,ciplus(kc+220)+sourcec(kc+220,m)*(1.d0-
     &  exdtauc(kc+220,m))/2.d0,
c cimoins(kc+220,m+1)+sourcec(kc+220,m)*(1.d0-exdtauc(kc+220,m))/2.d0,
     &  temc(kc+220),tcapc(kc+220,m),dhaut*V3*(tcapc(kc+220,m)+cthomson)
     &  ,rtaucef(kc+220)-dtoeffa,sourcec(kc+220,m)
                endif
c
                ddta=(1.d0-exdtauc(kc+220,m)*exdtauc(kc+220,m-1))/2.d0
        ciplus(kc+220)=(pciplus(kc+220)*exdtauc(kc+220,m)
     &                                        + sourcec(kc+220,m)*ddta)
        sbt=sbt + bt*dea
        skbt=skbt + bt*dea/(tcapc(kc+220,m) + cthomson)
        hicp=pciplus(kc+220) +
     &       sourcec(kc+220,m)*(1.d0-exdtauc(kc+220,m-1))/2.d0
                endif   ! kc<102                                            !sc
c
                if(kc.eq.1) then
c refaire si idilu=1        
        clya=tcapc(kc+220,m)
        elya=temc(kc+220)
        rdely= (temc(kc)*expa(-rtaucef(1)) 
     &        - temc(kc+220)*expa(-rtaucef(221))) *dhaut
        sdely=0.d0
        st1=ttaucef(1)-rtaucef(1)
        if(st1.lt.0.d0) st1=0.d0
        sta1=ttaucef(221)-rtaucef(221)
        if(sta1.lt.0.d0) sta1=0.d0
        sdely= (temc(kc)*expa(-st1) - temc(kc+220)*expa(-sta1))*dhaut
        rsomtely= (rsomtely + T*rdely)
        rsomely= (rsomely + rdely)
        ssomtely= (ssomtely + T*sdely)
        ssomely= (ssomely + sdely)
                endif                ! bord Lyman
  511        continue
c------------------------------------------------------------------------------
c  CALCUL DU SPECTRE DE L'HYDROGENE                                         !sh
c gains dans les raies:                 gain=N(n1)*B(n1n2)*hnu*H*J/(H*Ne*Nh)
c pertes dues aux raies:       perte=(N(n2)*A(n2n1)-N(n1)*B(n1n2)J)*hnu/(Ne*Nh)
c pertes en volume                                      *exp(-dtauc)???
       if(kitt.eq.0.or.kitn.eq.0.or.kitd.eq.0.or.imp.lt.5.or.imp.gt.7
     &         .or.kimpm.eq.0) goto 300
c               if(nitt.lt.50) goto 300
          if(kimpf2.eq.1.and.ifich.ge.3.and.ifich.le.8) WRITE(16,380) m
          if(ifich.ge.4.and.ifich.le.8.and.kimpf2.eq.1) WRITE(22,380) m
 380    format (/' RAIES PERMISES',6x,'gngn=N2g1/N1g2',2x,
     1    '(population du niveau excite)     m=',i4
     2    /2('  kr  i   am     gngn    dpgr    dgain '))
 300          continue
        do k=1,nrais
        dir(k)=0.d0
        enddo
        praiperm=0.d0
c        praieap=0.d0
        p_graip=0.d0
        graie=0.d0
        p_graih=0.d0
        graih=0.d0  
        p_graihe=0.d0
        graihe=0.d0  
        p_graio7=0.d0
        graio7=0.d0  
        p_graio8=0.d0
        graio8=0.d0  
        p_graiy=0.d0 
        graiy=0.d0
        p_grait=0.d0
        pertefluo=0.d0
        pf6p4kev=0.d0
        do j=1,11
           do i=1,27
              pij(i,j)=0.d0
           enddo
        enddo
        if(m.eq.1) dtaurmax0=0.d0
        if(m.eq.npt-1) dtaurmaxH=0.d0
           nivh=nivion(1,1)
        nlaser=0
        mimp=0
                kr=0
        do 301 n1=1,nivh-1                                     ! n1 niveau bas
                gn1=gps(n1)
                nij1=n1
        do 302 n2=n1+1,nivh                                  ! n2 niveau haut
                gn2=gps(n2)                              ! boucle sur les raies
                nij2=n2
                kr=kr+1                                                     !sh
                if(kr.eq.1) goto 302
                if(n1.eq.2.and.n2.ge.4) then
                   dir(kr)=0.d0
                   do lf=1,nf+1
                   sourcer(lf,kr,m)=0.d0
                   exdtaur(lf,kr,m)=1.d0
                   enddo
                   goto 302                      !hhh
                endif
        alambda=alo(kr)
        hnuev=hcse/alambda
        dpgr=0.d0
        dgainr=0.d0
        dperte=0.d0
        demt=0.d0
        capalt=0.d0
c  largeur de la raie whz (en Hz)
c  am est la constante d'amortissement = A/4pi/dnudoppler
c                        (il faudrait somme A au lieu de A)
        dnudoppler=vth(1)/alambda
        u0=uhz(kr)
        dnut=dnudoppler
           if(vturbkm.gt.1.d-3) then                              ! TURBULENCE
           dnut=sqrt(dnudoppler**2+(vturbkm*1.d13/alambda)**2)
           cx=u0/dnut
           endif
           if(iprof.le.1) then                ! Voigt (sauf les Lya si iprof=1)
        am=A21(kr) /(pi4*dnut)
              if(iprof.eq.1.and.kam(kr).eq.0) am=0.d0
           else if(iprof.eq.2) then                   ! Doppler thermique
              am=0.d0
           endif   ! iprof
              whz=dnut*Vpi
        if(m.eq.1) twr(kr)=whz                                              !sh
        tws(kr)=whz
           lcomp=0     ! dEcompt/E=(4kT-E)/mc2;lcomp=1 si dnucomp>ic*dnudoppler
           decomp=hnuev*(4.d0*tk-hnuev)*4.731792d8         ! 2.42e14/511kev
           if(nitf.gt.1.and.icompt.ge.1.and.
     &        abs(decomp).ge.whz*hicompt) lcomp=1
           if(nitf.gt.1.and.icompt.eq.-1) lcomp=1

        if(icompt.ne.0.and.kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.8
     &     .and.kimpm.eq.1.and.kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1
     &     .and.(kr.eq.2.or.kr.eq.10)) then
           write(21,741)
           write(21,'(2i4,0pf8.2,1p6e10.3)') m,kr,hnuev,T,dens(n1)/nht,
     &        am,dnudoppler/2.418d14,dnut/2.418d14,u0/2.418d14
           write(21,742)
           write(21,'(i21,1p3e11.3)')
     &        lcomp,hnuev**2/5.11d5,hnuev*4.d0*tk/5.11d5,decomp/2.418d14
           txt='(nu-nu0)/dnut'
           write(21,'(a,7f9.3/8f9.1)')txt,(fx(lf)*cx,lf=1,15)
        endif
 741       format('   m  kr   hnuev   T        r(i,j)     am     ',
     &     'dnudoppler  dnut     u0    (ev)')
 742       format(17x,'lcomp hnu**2/511k 4kthnu/511 dEcompev')

c  profondeur optique et emissivite au centre de la raie
        rz=r(1,1)
        capalt=dens(n1)*B12hnus4pi(kr)*(1.d0-gn1*dens(n2)/gn2/dens(n1))
      if(capalt.lt.0.d0)capalt=dens(n1)*B12hnus4pi(kr)*(1.d0-teer(kr,m))!2345678
c on traite les 2 raies (issues des niveaux 2s et 2p) ensemble
                if(n1.eq.3) then
       ca=dens(2)*B12hnus4pi(kr-nivh+3)*(1.d0-2.d0*dens(n2)/gn2/dens(2)) ! H2p
        if(ca.lt.0.d0)
     &  ca=dens(2)*B12hnus4pi(kr-nivh+3)*(1.d0-teer(kr-nivh+3,m))
        capalt=capalt + ca
                endif
c        if(capalt.lt.0.d0) then
c                capalt=0.d0
c                nlaser=nlaser+1
c if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1)write(16,*)'effet laser pour kr=',kr
c                endif                                         ! 0.667e16*hc/4pi
        capal0=capalt/whz
        demt=dens(n2)*A21hnus4pi(kr)
c        if(n1.eq.1.and.n2.eq.2) demt=0.d0         ! cas traite avec le continu
        if(n1.eq.3) demt=demt + dens(n2)*A21hnus4pi(kr-nivh+3)
        dem0=demt/whz
        gngn=dens(n2)*gn1/(dens(n1)*gn2)
c
        kc=kcr(kr)
        capabs=tcapc(kc,m)
        emmc=temc(kc)
c                                                                           !sh
           if(kali(kr).eq.1.or.(itali.eq.0.and.isuite.le.1))then
! indispensable pour nitf=1
        CALL TR2dirA(capalt,demt,am,u0,cx,kc,kr,lcomp)
c                      cjn(kr)=(hjc + somjn/som) *pi4 /hnuev/chel
c                      dir(kr)=(somjr*u0 + djcodnu) *pi4 /hnuev/chel
c                      tcjn(kr,m)=cjn(kr)
c                      tjr(kr,m)=somjr*u0 + djcodnu
           endif   ! kali - itali
        dir(kr)=tjr(kr,m)  *pi4 /hnuev/chel
        cjn(kr)=tcjn(kr,m)
c
                        if(kitd.eq.0) goto 302
c on ne calcule pas pertes=N2A21*hnu/(NhNe)=4pi*(emm0*whz)/(NhNe)
c et gains=(N1B12-N2B21)*J*hnu/(NhNe)=4pi*(kappa0*whz)*J/(NhNe)
c mais dpgr=perte nette=(N2A21-(N1B12-N2B21)*J)*hnu/(NhNe)
c et dgainr= ionisation par les raies, =capabs*(Jtot-Jc)*4pi/(NhNe)
        dpgr=(dens(n2)*A21hnu(kr) -
     &        dens(n1)*B12hnu2s4pi(kr)*cjn(kr)*(1.d0-gngn))/hnne
             if(n1.eq.3) then                                     !hhh
             gngn2=dens(n2)*2.d0/(dens(2)*gn2)
             dpgr=dpgr + (dens(n2)*A21hnu(kr-nivh+3) -
     &       dens(2)*B12hnu2s4pi(kr-nivh+3)*cjn(kr)*(1.d0-gngn2))/hnne
             endif   ! n1=3
        p_graip=p_graip + dpgr                                              !sh
        dperte=demt *pi4/hnne
        praiperm=praiperm + dperte                              ! pour info
c        praieap=praieap + dpgr
        dgainr=capabs*tjr(kr,m)*pi4/hnne
        graie=graie + dgainr
c
        if(ifich.ge.4.and.ifich.le.8.and.
     &     kimpf.eq.1.and.kimpm.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
       if(kr.eq.2)write(85,*)'nitf m kr tjr dgainr dpgr dens1 dens2 cjn'
        if(abs(dgainr).gt.3.d-25.or.dpgr.gt.3.d-25)
     &  write(85,'(2i4,i5,1p6e11.3)') nitf,m,kr,tjr(kr,m),dgainr,dpgr,
     &        dens(n1),dens(n2),cjn(kr)
        endif
c
        j=1
            p_graih=p_graih + dpgr
            graih=graih + dgainr
c
c calcul du chauffage Compton
       altar=1.d0/(1.d0+ hnuev*8.6675d-6 + hnuev*hnuev*3.827d-12)
       scompt1=scompt1 + dir(kr)*hnuev*6.6262d-27/pi4 *altar*hnuev   ! Jr*dnuev
       scompt2=scompt2 + dir(kr)*hnuev*6.6262d-27/pi4 *altar*
     1         (1.d0- altar*hnuev*(8.6675d-6 + hnuev*7.655d-12)/4.d0)
       sjdnuev=sjdnuev + dir(kr)*hnuev*chel/pi4/esh
          if(kali(kr).eq.1.or.nitf.eq.1)
     & sjdnuevrai=sjdnuevrai+dir(kr)*hnuev*chel/pi4/esh
c
                        if(kitt.eq.0.or.kitn.eq.0) goto 302
c
              if(capalt.lt.0.d0) capalt=0.d0
        dcapr(kr,m)=capalt
        dsl(kr,m)=0.d0
        if(capalt.gt.0.d0) dsl(kr,m)=demt/capalt
        lcompt(m,kr)=lcomp
        if(lcomp.eq.1) then
           if(ncomp(kr).eq.0) mdebcomp(kr)=m
           ncomp(kr)=ncomp(kr) + 1
        endif
        som=0.d0
        do lf=1,nf
           prof=FPROF(lf,am,x,cx)
           som=som + prof *fint(lf)
           tphi(lf,kr,m)=prof
        enddo
        do lf=1,nf
           tphi(lf,kr,m)=tphi(lf,kr,m)/som/u0
           twnu(lf,kr,m)=fint(lf)*u0
        enddo
           tphi(nf+1,kr,m)=0.d0
           twnu(nf+1,kr,m)=0.d0                                             !sh
c
        pij(1,1)=pij(1,1) + dpgr
        if(incang.eq.0) then
           rtaur0(kr)=rtaur0(kr) + dhaut*V3*capal0 ! raie seule au centre
        else
           rtaur0(kr)=rtaur0(kr) + dhaut*capal0 ! raie seule au centre
        endif
        if(m.eq.1) then
           dto=dhaut*capal0
           if(dto.gt.dtaurmax0) then
              dtaurmax0=dto
              krtormax0=kr
           endif
c           write(6,'(i4,1pe12.4,i4,1pe12.4)')kr,dto,krtormax0,dtaurmax0
        else if(m.eq.npt-1) then
           dto=dhaut*capal0
           if(dto.gt.dtaurmaxH) then
              dtaurmaxH=dto
              krtormaxH=kr
           endif
c           write(6,'(i4,1pe12.4,i4,1pe12.4)')kr,dto,krtormaxH,dtaurmaxH
        endif

        if(m.eq.npt-1) then
           rtaurl(kr)=dhaut*V3*capal0
           if(rtaurl(kr).le.1.d-40) rtaurl(kr)=0.d0
        endif
c
           if(kali(kr).eq.1.or.(itali.eq.0.and.isuite.le.1))
     &  CALL TR2dirB(kc,kr,lcomp)
c
        if(kimpf2.eq.1.and.ifich.eq.9.and.m.ne.npt)
     &     WRITE(21,'(f10.3,i6,1p3e13.4)') hnuev,kr,
     &     dhaut*V3*capal0,dhaut*V3*(capabs+cthomson)
c
        if(imp.eq.8.and.nitf.eq.nfmax.and.kimpm.eq.1.and.kitd.eq.1
     &             .and.kitt.eq.1.and.kitn.eq.1.and.kr.eq.2)
     &  WRITE(51,*) '  m   ;z         ;T        ;kr   ;lambda     ;',
     &  'hnuev      ;f        ;kr    ;r(i,j)  ;g1N2/g2N1  ;e(-E/kT)',
     &  '   ;aa        ;bb        ;cc        ;dd  ;A21/C21Nertau0 ;kr',
     &  '   ;am        ;emmr0     ;emmc     ;capal0   ;capacont  ;ctho',
     &  'mson   ;kr   ;dtaur0   ;rtaur0   ;rtauraile  ;rtaucont  ;kr',
     &  ' ;dp-grnet ;dperte(NA) ;dgainr(io) ;kr   ;J0        ;Jmoy',
     &  '     ;Jcont      ;B'
        if(imp.eq.8.and.nitf.eq.nfmax.and.kimpm.eq.1
     &             .and.kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
        act=0.d0
        ee=teer(kr,m)                                         ! exp(-hnuev*tk1)
        if(cc.gt.0.d0.and.rtaur0(kr).gt.0.d0) act=aa/cc/rtaur0(kr)
        WRITE(51,490) m,tz(m),T,kr,alo(kr),hnuev,fdo(kr),
     &  kr,dens(n1)/nht,gngn,ee,aa,bb,cc,dd,act,
     &  kr,am,dem0,emmc,capal0,capabs,cthomson,
     &  kr,dhaut*V3*capal0,rtaur0(kr),rtaurl(kr),rtaucef(kc),
     &  kr,dpgr,dperte,dgainr,
     &  kr,cjn(kr)*hnuev*chel/pi4,tjc(kc,m),                            ! hj0
     &  hnuev*hnuev*hnuev*ee/(1.d0-ee) *2.084506d-4
        endif   ! imp=8
c
                if(kimpf2.eq.0) goto 302
                if(imp.lt.5.or.imp.gt.7.or.kimpm.eq.0) goto 302             !sh
c               if(nitt.lt.50) goto 302
c               if(dperte.lt.praiperm/1.d3) goto 302
        mimp=mimp+1
        jimp=2-mod(mimp,2)
        itz(jimp)=iz0
        iti(jimp)=n1
        ikr(jimp)=kr
        t1(jimp)=alambda
        if(alambda.ge.99999.5) t1(jimp)=0.d0
        if(jimp.eq.1.and.alambda.ge.9999.5) t1(jimp)=mod(alambda,1.d4)
        t2(jimp)=am
        t3(jimp)=gngn
        t4(jimp)=dpgr
        t5(jimp)=dgainr                                         !gngn
        if(jimp.eq.2.and.ifich.ge.3.and.ifich.le.8) WRITE(16,481)
     1    (ikr(k),iti(k),t2(k),t3(k),t4(k),t5(k),k=1,2)
        if(ifich.ge.4.and.ifich.le.8.and.jimp.eq.2) WRITE(22,481)
     1    (ikr(k),iti(k),t2(k),t3(k),t4(k),t5(k),k=1,2)
 302       continue
 301          continue
c
c------------------------------------------------------------------------------
c  CALCUL DU SPECTRE DES RAIES DES IONS A PLUSIEURS NIVEAUX                 !sr
c
                                do 350 j=2,nelem
        iz0=iz(j)
                                do 351 i=1,iz0
        niv=nivion(i,j)
        if(niv.eq.1) goto 351
                nij0=nivcum(i-1,j)
                nijp=nivcum(i,j)+1
c on a un certain nombre de raies donnees dans le atomic.dat
c on ne transfere pas les raies interdites - c.a.d. les raies ou fdo<fdolim
                 kri=nrcum(i-1,j) + 1
                 krf=nrcum(i,j)
                                do 352 kr=kri,krf
                 if(fdo(kr).lt.fdolim) goto 352
                 nij1=nij0 + n1rt(kr)
                 nij2=nij0 + n2rt(kr)
                 gn1=g1rt(kr)                                   ! gps(nij1)
                 gn2=g2rt(kr)                                   ! gps(nij2)
        alambda=alo(kr)
        hnuev=hcse/alambda                                                  !sr
        dpgr=0.d0
        dgainr=0.d0
        dperte=0.d0
        demt=0.d0
        capalt=0.d0
c  largeur de la raie whz (en Hz)
c  am est la constante d'amortissement = sommeAji/4pi/dnudoppler
        dnudoppler=vth(j)/alambda

        u0=uhz(kr)
        cx=1.d0
        dnut=dnudoppler
           if(vturbkm.gt.1.d-3) then                              ! TURBULENCE
              dnut=sqrt(dnudoppler**2+(vturbkm*1.d13/alambda)**2)
              cx=u0/dnut
           endif
           if(iprof.le.1) then                ! Voigt (sauf les Lya si iprof=1)
        am=A21(kr) /(pi4*dnut)
              if(iprof.eq.1.and.kam(kr).eq.0) am=0.d0
           else if(iprof.eq.2) then                   ! Doppler thermique
              am=0.d0
           endif   ! iprof
              whz=dnut*Vpi

        if(m.eq.1) twr(kr)=whz                                              !sr
        tws(kr)=whz
           lcomp=0     ! dEcompt/E=(E-4kT)/mc2;lcomp=1 si dnucomp>ic*dnudoppler
           decomp=hnuev*(4.d0*tk-hnuev)*4.731792d8         ! 2.42e14/511kev
           if(nitf.gt.1.and.icompt.ge.1.and.
     &        abs(decomp).ge.whz*hicompt) lcomp=1
           if(nitf.gt.1.and.icompt.eq.-1) lcomp=1
c
        if(icompt.ne.0.and.kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.8
     &     .and.kimpm.eq.1.and.kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1
     &     .and.(kr.eq.k4.or.kr.eq.k6.or.kr.eq.k7
     &     .or.kr.eq.k9.or.kr.eq.k12.or.kr.eq.nrcum(13,8)+1)) then
           write(21,741)
           write(21,'(2i4,0pf8.2,1p6e10.3)') m,kr,hnuev,T,r(i,j),am,
     &        dnudoppler/2.418d14,dnut/2.418d14,u0/2.418d14
           write(21,742)
           write(21,'(i21,1p3e11.3)')
     &        lcomp,hnuev**2/5.11d5,hnuev*4.d0*tk/5.11d5,decomp/2.418d14
           write(21,'(a,7f9.1/8f9.1)')txt,(fx(lf)*cx,lf=1,15)
        endif
c
c  profondeur optique et emissivite au centre de la raie
        rz=r(i,j)
                capalt=0.d0
                gngn=0.d0
        if(dens(nij1).gt.0.d0) gngn=gn1*dens(nij2)/gn2/dens(nij1)
        capalt=dens(nij1)*B12hnus4pi(kr)*(1.d0-gngn)
c              capalt=dens(nij1)*fdo(kr)*0.02654d0*(1.d0-gngn)      ! 0.667/8pi
        if(capalt.lt.0.d0) then
                capalt=dens(nij1)*B12hnus4pi(kr)*(1.d0-teer(kr,m))
                endif                                         ! 0.667e16*hc/4pi
        demt=dens(nij2)*A21hnus4pi(kr)
c        demt=dens(nij2)*gn1/gn2*fdo(kr)/(alambda**3) *bhca*2.d16
        capal0=capalt/whz
        dem0=demt/whz
        kc=kcr(kr)
        capabs=tcapc(kc,m)
        emmc=temc(kc)
c                                                                           !sr
           if(kali(kr).eq.1.or.(itali.eq.0.and.isuite.le.1))then
! indispensable pour nitf=1
        CALL TR2dirA(capalt,demt,am,u0,cx,kc,kr,lcomp)
            endif   ! kali - nitf/itali
        dir(kr)=tjr(kr,m)  *pi4 /hnuev/chel
        cjn(kr)=tcjn(kr,m)
c
                        if(kitd.eq.0) goto 352
c on ne calcule pas pertes=N2A21*hnu/(NhNe)=4pi*(emm0*whz)/(NhNe)
c et gains=(N1B12-N2B21)*J*hnu/(NhNe)=4pi*(kappa0*whz)*J/(NhNe)
c mais dpgr=perte nette=(N2A21-(N1B12-N2B21)*J)*hnu/(NhNe)
c et dgainr= ionisation par les raies, =capabs*(Jtot-Jc)*4pi/(NhNe)
c        aan=gngn*(pi4*2.d16/(alambda**2) + cjn(kr))         ! bhca=0.02654*hca
c        dpgr=dens(nij1)/nht*fdo(kr)/alambda/ne*bhca*(aan - cjn(kr))
c        if(kitt.eq.1.and.kitn.eq.1.and.aan.gt.0.d0.and
c     &  .abs((aan - cjn(kr))/aan).le.1.d-12) write(6,*) 'pour m=',m,
c     &  ' et kr=',kr,' %dif(N2A21-N1B12J)<1e-10',(aan - cjn(kr))/aan
c
        dpgr=(dens(nij2)*A21hnu(kr) -
     &        dens(nij1)*B12hnu2s4pi(kr)*cjn(kr)*(1.d0-gngn))/hnne
        p_graip=p_graip + dpgr
        dperte=demt *pi4/hnne
        praiperm=praiperm + dperte                               ! pour info
c        praieap=praieap + dpgr
        dgainr=capabs*tjr(kr,m)*pi4/hnne                                    !sr
        graie=graie + dgainr
c
        if(ifich.ge.4.and.ifich.le.8
     &     .and.kimpf.eq.1.and.kimpm.eq.1.and.kitt.eq.1.and.kitn.eq.1
     &     .and.(abs(dgainr).gt.3.d-25.or.dpgr.gt.3.d-25))
     &     write(85,'(2i4,i5,1p6e11.3)') nitf,m,kr,tjr(kr,m),dgainr,
     &        dpgr,dens(nij1),dens(nij2),cjn(kr)
c
            if(j.eq.2) then
            p_graihe=p_graihe + dpgr
            graihe=graihe + dgainr
            endif   ! j=2
            if(j.eq.5) then
               if(i.eq.7) then
            p_graio7=p_graio7 + dpgr
            graio7=graio7 + dgainr
               else if(i.eq.8) then
            p_graio8=p_graio8 + dpgr
            graio8=graio8 + dgainr
               endif
            endif   ! j=5
c
c calcul du chauffage Compton
       altar=1.d0/(1.d0+ hnuev*8.6675d-6 + hnuev*hnuev*3.827d-12)
       scompt1=scompt1 + dir(kr)*hnuev*6.6262d-27/pi4 *altar*hnuev   ! Jr*dnuev
       scompt2=scompt2 + dir(kr)*hnuev*6.6262d-27/pi4 *altar*
     1         (1.d0- altar*hnuev*(8.6675d-6 + hnuev*7.655d-12)/4.d0)
       sjdnuev=sjdnuev+dir(kr)*hnuev*chel/pi4/esh  !som(J(nu)-Jc)dnuev=tjr/esh
          if(kali(kr).eq.1.or.nitf.eq.1)
     & sjdnuevrai=sjdnuevrai+dir(kr)*hnuev*chel/pi4/esh
c
                        if(kitt.eq.0.or.kitn.eq.0) goto 352                 !sr
c
              if(capalt.lt.0.d0) capalt=0.d0
        dcapr(kr,m)=capalt
        dsl(kr,m)=0.d0
        if(capalt.gt.0.d0) dsl(kr,m)=demt/capalt
        lcompt(m,kr)=lcomp
        if(lcomp.eq.1) then
           if(ncomp(kr).eq.0) mdebcomp(kr)=m
           ncomp(kr)=ncomp(kr) + 1
        endif
        som=0.d0
        do lf=1,nf
           prof=FPROF(lf,am,x,cx)
           som=som + prof *fint(lf)
           tphi(lf,kr,m)=prof
        enddo
        do lf=1,nf
           tphi(lf,kr,m)=tphi(lf,kr,m)/som/u0
           twnu(lf,kr,m)=fint(lf)*u0
        enddo
           tphi(nf+1,kr,m)=0.d0
           twnu(nf+1,kr,m)=0.d0                                             !sr
c
        if(incang.eq.0) then
           rtaur0(kr)=rtaur0(kr) + dhaut*V3*capal0 ! raie seule au centre
        else
           rtaur0(kr)=rtaur0(kr) + dhaut*capal0 ! raie seule au centre
        endif
        if(m.eq.1) then
           dto=dhaut*capal0
           if(dto.gt.dtaurmax0) then
              dtaurmax0=dto
              krtormax0=kr
           endif
        else if(m.eq.npt-1) then
           dto=dhaut*capal0
           if(dto.gt.dtaurmaxH) then
              dtaurmaxH=dto
              krtormaxH=kr
           endif
        endif

        if(m.eq.npt-1) then
           rtaurl(kr)=dhaut*V3*capal0
           if(rtaurl(kr).le.1.d-40) rtaurl(kr)=0.d0
        endif
        pij(i,j)=pij(i,j) + dpgr                                            !sr
c
           if(kali(kr).eq.1.or.(itali.eq.0.and.isuite.le.1))
     &  CALL TR2dirB(kc,kr,lcomp)
c
        if(kimpf2.eq.1.and.ifich.eq.9.and.m.ne.npt)
     &     WRITE(21,'(f10.3,i6,1p3e13.4)') hnuev,kr,
     &     dhaut*V3*capal0,dhaut*V3*(capabs+cthomson)
        if(imp.ge.1.and.kimpm.eq.1.and.kimpf2.eq.1.and.ifich.ne.9
     &  .and.(kr.eq.k4.or.kr.eq.k6.or.kr.eq.k8.or.kr.eq.k9
c     &  .or.kr.eq.311.or.kr.eq.312.or.kr.eq.348.or.kr.eq.350
c     &  .or.kr.eq.361.or.kr.eq.362
     &  .or.kr.eq.k11.or.kr.eq.k12.or.kr.eq.nrcum(13,8)+1)) then
        dgainr1=capabs*tjr(kr,m)*pi4/hnne
        dpgr1=dpgr
       hn2a21hnudz=dens(nij2)*A21hnu(kr)*dhaut
       hnrb=0.d0                                                            !sr
       if(dens(nij2).gt.0.d0) hnrb=(dens(nij2)*A21(kr) - dens(nij1)*
     &    B12hnus4pi(kr)*cjn(kr)*(1.d0-gngn))/(dens(nij2)*A21(kr))

       if(ifich.ge.4.and.ifich.le.8) then
       if(kr.eq.k4)WRITE(21,*)'a  kr  m  col  T  dz  N(nij1)  N(nij2)',
     & '  N2A21hnudz  nrb  dpgr  dgainr  emmc  emmr0  emmrt  kappac  ',
     & 'kappar0  kappart  rtaur0rai  dnudoppler  Jc  Jphi      Jr'
       WRITE(21,489) kr,m,tcol(m),T,dhaut,dens(nij1),dens(nij2),        ! @
     &      hn2a21hnudz,hnrb,dpgr1,dgainr1,emmc,dem0,demt,capabs,capal0,
     &      capalt,rtaur0(kr)-dhaut*V3*capal0,dnudoppler,
     &      tjc(kc,m),cjn(kr)*hnuev*chel/pi4,tjr(kr,m)
 489   format('@',2i4,22(1pe12.4))
       endif
c       WRITE(21,*) '  kr   m  N(nij1)   N(nij2)  N2A21hnudz   nrb'//
c     &             '       dpgr      dgainr'
c       WRITE(21,486) kr,m,dens(nij1),dens(nij2),hn2a21hnudz,hnrb,      ! @
c     &               dpgr1,dgainr1
 486   format('@',i4,i4,4(1pe10.3),2(1pe11.3))
c       WRITE(21,*) '  kr   m   emmc     emmr0     emmrt     '//
c     &             'kappac    kappar0   kappart  rtaur0rai'
c       WRITE(21,487) kr,m,emmc,dem0,demt,capabs,capal0,capalt,         ! #
c     &               rtaur0(kr)-dhaut*V3*capal0
 487   format('#',i4,i4,7(1pe10.3))
c       WRITE(21,*) '  kr   m    am    dnudoppler   whz       u0'//
c     &             '        Jc        Jphi      Jr'
c       WRITE(21,488) kr,m,am,dnudoppler,whz,u0,tjc(kc,m),              ! $
c     &               cjn(kr)*hnuev*chel/pi4,tjr(kr,m)
 488   format('$',i4,i4,6(1pe10.3),1pe11.3)
                 endif   ! imp si kr=20  24  117  118                       !sr
c
        if(imp.eq.8.and.nitf.eq.nfmax.and.kimpm.eq.1
     &             .and.kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
                aa=A21(kr)
                bb=cjn(kr)*B12hnus4pi(kr)                       ! B12J
                cc=ne*texcoll(kr,m)                             ! C21gg
                dd=0.d0
                ee=teer(kr,m)
        act=0.d0
        if(cc.gt.0.d0.and.rtaur0(kr).gt.0.d0) act=aa/cc/rtaur0(kr)
        WRITE(51,490) m,tz(m),T,kr,alo(kr),hnuev,fdo(kr),
     &  kr,dens(nij1),gngn,ee,aa,bb,cc,dd,act,
     &  kr,am,dem0,emmc,capal0,capabs,cthomson,
     &  kr,dhaut*V3*capal0,rtaur0(kr),rtaurl(kr),rtaucef(kc),
     &  kr,dpgr,dperte,dgainr,
     &  kr,cjn(kr)*hnuev*chel/pi4,tjc(kc,m),                            ! hj0,
     &  hnuev*hnuev*hnuev*ee/(1.d0-ee) *2.084506d-4
        endif   ! imp=8
c
                if(kimpf2.eq.0) goto 352
                if(imp.lt.5.or.imp.gt.7.or.kimpm.eq.0) goto 352             !sr
                if(dperte.lt.1.d-40) goto 352
c               if(nitt.lt.50) goto 352
        mimp=mimp+1
        jimp=2-mod(mimp,2)
        itz(jimp)=i
        iti(jimp)=nij1-nij0
        ikr(jimp)=kr
        t1(jimp)=alambda
        if(alambda.ge.99999.5) t1(jimp)=0.d0
        if(jimp.eq.1.and.alambda.ge.9999.5) t1(jimp)=mod(alambda,1.d4)
        t2(jimp)=am
        t3(jimp)=gngn
        t4(jimp)=dpgr
        t5(jimp)=dgainr                                       
        if(jimp.eq.2.and.ifich.ge.3.and.ifich.le.8) WRITE(16,481)
     1    (ikr(k),iti(k),t2(k),t3(k),t4(k),t5(k),k=1,2)
        if(ifich.eq.2.and.jimp.eq.2.and.ifich.ge.4.and.ifich.le.8)
     1    WRITE(22,481) (ikr(k),iti(k),t2(k),t3(k),t4(k),t5(k),k=1,2)
  352   continue   ! kr
  351   continue   ! i
  350                           continue  ! j
c                                                                           !sr
c------------------------------------------------------------------------------
c  CALCUL DU SPECTRE DES RAIES DES IONS A 1 SEUL NIVEAU
c        dir(kr)=4pi*Somme(Jr(nu)*dnu)/hnu
c le calcul de l'intensite des 24 RAIES DE FLUORESCENCE emises apres ionisation
c de la couche K des ions du Fer est inclus : kr > nrcum(26,10)=nraissfek
c                                                                          !sr0
           pgsup=0.d0
           jsup=nelem
           if(inr.eq.2) jsup=nelem + 1
        do 401 jr=2,jsup
           if(jr.eq.nelem + 1) then
              iz0=1
           else
              iz0=iz(jr)
           endif
c        if(m.eq.npt/2.and.kitd*kitt*kitf.eq.1) print*,'jr,Z=',jr,iz0
        do 402 ir=1,iz0
           niv=1
           if(jr.le.nelem) niv=nivion(ir,jr)
c        if(m.eq.npt/2.and.kitd*kitt*kitf.eq.1) print*,'ir,jr,niv=',ir,jr,niv
           if(jr.eq.10.and.ir.eq.iz0) then                    ! Fer 26
              kri=nrcum(iz0,jr) + 1
              kra=nrcum(iz0,jr) + 24      ! pour inclure les raies FeK 
              if(niv.eq.1) then          ! niv(fe26)=1 n'est pas le cas general
                 kri=nrcum(iz0-1,jr) + 1
                 kra=nrcum(iz0,jr) + 24 
              endif
           else if(jr.eq.nelem + 1) then
              kri=nraissfek+24+nuta + 1
              kra=nraissfek+24+nuta + ns
           else             ! ions avec raies de resonance mais "1 seul niveau"
              if(niv.gt.1) goto 402               ! raies deja traitees
              kri=nrcum(ir-1,jr) + 1
              kra=nrcum(ir,jr)
           endif
c   if(m.eq.npt/2.and.kitd*kitt*kitf.eq.1)print*,'ir,jr,kri,kra=',ir,jr,kri,kra
        do 403 kr=kri,kra                                                  !sr0
c           if(kr.le.nraissfek+50.and.m.eq.npt/2.and.kitd*kitt*kitf.eq.1)
c     &     print*,'kr,jr,ir,kri,kra',kr,jr,ir,kri,kra
c           if(kr.ge.nraissfek+50.and.m.eq.npt/2.and.kitd*kitt*kitf.eq.1
c     &  .and.mod(kr,500).eq.0)print*,'kr,jr,ir,kri,kra',kr,jr,ir,kri,kra
           dpgr=0.d0
           dgainr=0.d0
           dperte=0.d0
           demt=0.d0
           capalt=0.d0
        alambda=alo(kr)
        if(kr.le.nraissfek) then
           j=jr
           i=ir
           ife=0
           nij1=nivcum(ir-1,j) + 1
           nijfe=0
           hnuev=hcse/alambda
c           if(m.eq.npt/2.and.kitd*kitt*kitf.eq.1.and.mod(kr,50).eq.0)
c     &     print*,'a1 kr,j,i,nij1,hnuev',kr,j,i,nij1,hnuev
        else if(kr.le.nraissfek+24) then     ! raies de fluorescence K du fer
           j=10
           ife=kr-nraissfek             ! ion ionise par photons haute energie
           i=ife+1                      ! ion final
           nij1=nivcum(i-1,j) + 1
           nijfe=nivcum(ife-1,j) + 1
           hnuev=efluo(ife)
c           if(m.eq.npt/2.and.kitd*kitt*kitf.eq.1.and.mod(kr,5).eq.0)
c     &     print*,'a2 kr,j,ife,nij1,hnuev',kr,j,ife,nij1,hnuev
        else if(inr.eq.2) then          ! raies de resonance supplementaires
           ks=kr - (nraissfek+24+nuta)
           j=js(ks)
           i=is(ks)
           nij1=nivcum(i-1,j) + 1
           nijfe=0
           hnuev=hcse/alambda
c           if(m.eq.npt/2.and.kitd*kitt*kitf.eq.1.and.mod(kr,500).eq.0)
c     &     print*,'a4 kr,ks,j,i,nij1,hnuev',kr,ks,j,i,nij1,hnuev
        endif     ! kr
c  whz=largeur (en Hz) de la raie                                          !sr0
c  am est la constante d'amortissement = sommeA/4pi/dnudoppler g2/g1moyen=2
        dnudoppler=vth(j)/alambda
        u0=uhz(kr)
        cx=1.d0
        dnut=dnudoppler
           if(vturbkm.gt.1.d-3) then                              ! TURBULENCE
           dnut=sqrt(dnudoppler**2+(vturbkm*1.d13/alambda)**2)
           cx=u0/dnut
           endif
           if(iprof.le.1) then                ! Voigt (sauf les Lya si iprof=1)
        am=A21(kr) /(pi4*dnut) *0.5d0         ! 0.5 val.moy.de gn1/gn2 ?
           if(kr.gt.nraissfek+24+nuta) am=am*g1rt(kr)/g2rt(kr)
              if(iprof.eq.1.and.kam(kr).eq.0) am=0.d0
           else if(iprof.eq.2) then                   ! Doppler thermique
              am=0.d0
           endif   ! iprof
              whz=dnut*Vpi
c if(kr.gt.nraissfek)whz=hnuev*0.003d0*esh !???elargissement cf Band 90
            if(m.eq.1) twr(kr)=whz
            tws(kr)=whz
        u0=uhz(kr)
           lcomp=0     ! dEcompt/E=(E-4kT)/mc2;lcomp=1 si dnucomp>ic*dnudoppler
           decomp=hnuev*(4.d0*tk-hnuev)*4.731792d8         ! 2.42e14/511kev
c           if(icompt.ge.1.and.abs(decomp).ge.dnudoppler*hicompt)
c     &     lcomp=1
           if(nitf.gt.1.and.icompt.ge.1.and.
     &        abs(decomp).ge.whz*hicompt) lcomp=1
           if(nitf.gt.1.and.icompt.eq.-1) lcomp=1
c
        if(icompt.ne.0.and.kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.8
     &     .and.kimpm.eq.1.and.kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1
     &     .and.(kr.eq.nrcum(10,10)+1.or.kr.eq.nrcum(12,10)+1
     &     .or.kr.eq.nrcum(14,10)+1)) then
           write(21,741)
           write(21,'(2i4,0pf8.2,1p6e10.3/i21)') m,kr,hnuev,T,r(i,j),am,
     &        dnudoppler/2.418d14,dnut/2.418d14,u0/2.418d14
           write(21,742)
           write(21,'(i21,1p3e11.3)')
     &        lcomp,hnuev**2/5.11d5,hnuev*4.d0*tk/5.11d5,decomp/2.418d14
           write(21,'(a,7f9.1/8f9.1)')txt,(fx(lf)*cx,lf=1,15)
        endif
c
c  calcul du nombre d'atomes excites N2 par iteration ainsi que
c  du J et du dir=4pi*Somme(Jrdnu)/hnu en vue du nombre de photoionisations
c  N2/N1=(NeC12+B12*J~+N(i+1)Rsec)/(A21+NeC21+B21*J~)       si profil(centre)=1
c  avec cjn=4piJ~/hnu=(Jcont+Somme(Jr(nu)profil(nu)dnu)/whz) *4pi/hnu
c  emm(nu)=emmcont + N2A21hnu/(4piwhz)*profil(nu)        Somme(profildnu)/whz=1
c  kapa(nu)=kapacont + N1B12hnu/(4piwhz) *(1-gngn) *profil(nu)   gngn=g1N2/g2N1
c  S=(emm+cthomson*J)/(kapa+cthomson)
c
        if(kr.le.nraissfek.or.kr.gt.nraissfek+24+nuta) then !RAIES DE RESONANCE
                ee=teer(kr,m)
                aa=A21(kr)                                    ! A21g2/g1
                bb=cjn(kr)*B12hnus4pi(kr)                     ! B12J
                cc=ne*texcoll(kr,m)                           ! NeC21g2/g1
                   dd=0.d0
                   if(kr.eq.kri.and.kr.le.nraissfek.and.r(i,j).gt.0.d0)
     &          dd=popsec(i,j)/r(i,j)          ! N(i+1)/N1 *Ne*alfasec
c toutes les recomb. sur niv.second. donnent a la fin un photon de la 1ere raie
        gngn=(bb + ee*cc + dd)/(aa + cc + bb)                      ! g1N2/g2N1
        demt=gngn*dens(nij1)*A21hnus4pi(kr)              ! ici c'est A21*g2/g1
        capalt=dens(nij1)*B12hnus4pi(kr) *(1.d0-gngn)
                   if(capalt.lt.0.d0) then
                   capalt=dens(nij1)*B12hnus4pi(kr) *(1.d0-teer(kr,m))     !sr0
                   nlaser=nlaser+1
c if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1)write(16,*)'effet laser pour kr=',kr
                   endif    ! capalt<0
        dem0=demt/whz
        capal0=capalt/whz
c
        else if(kr.le.nraissfek+16) then    ! RAIES DE FLUORESCENCE K du fer
                aa=A21(kr)
                bb=0.d0
                cc=0.d0
                dd=0.d0
                ee=0.d0
                gngn=0.d0
c                eee=ee
        capalt=0.d0
        demt=dens(nijfe)*tphotk(ife,j)*pfluo(ife)*hnuev*chel/pi4
        capal0=0.d0
        dem0=demt/whz
c if(r(i,j).gt.0.)dd=r(ife,j)/r(i,j)*tphotk(ife,j)   !impression
c                eee=whz
        else if(kr.le.nraissfek+24) then    ! avec possibilite de reabsorption
c 17<=ife<=24 
                am=am/pfluo(ife)                            ! ???
                aa=A21(kr)                                    ! A21g2/g1
                bb=cjn(kr)*B12hnus4pi(kr)                     ! B12J
                cc=0.d0
                dd=0.d0
                if(r(i,j).gt.0.) dd=r(ife,j)/r(i,j)*tphotk(ife,j)
        gngn=(dd + bb)/(aa/pfluo(ife) + bb)
        demt=gngn*dens(nij1)*A21hnus4pi(kr)              ! ici c'est A21*g2/g1
        capalt=dens(nij1)*B12hnus4pi(kr) *(1.d0-gngn)
                   if(capalt.lt.0.d0) then
                   capalt=dens(nij1)*B12hnus4pi(kr) *(1.d0-teer(kr,m))
                   endif    ! capalt<0
        dem0=demt/whz
        capal0=capalt/whz
        endif      ! kr/nraissfek
        kc=kcr(kr)
        capabs=tcapc(kc,m)
        emmc=temc(kc)
c
           if(kali(kr).eq.1.or.(itali.eq.0.and.isuite.le.1))then
! indispensable pour nitf=1
        CALL TR2dirA(capalt,demt,am,u0,cx,kc,kr,lcomp)                      !sr0
c                      cjn(kr)=(hjc + somjn/som) *pi4 /hnuev/chel
c                      dir(kr)=(somjr*u0 + djcodnu) *pi4 /hnuev/chel
c                      tcjn(kr,m)=cjn(kr)
c                      tjr(kr,m)=somjr*u0 + djcodnu
           endif   ! kali - nitf/itali
        dir(kr)=tjr(kr,m)  *pi4 /hnuev/chel
        cjn(kr)=tcjn(kr,m)
c
                        if(kitd.eq.0) goto 403
c
c pertes=4pi*emm/(NhNe) et gains=4pi*kappa*J/(NhNe),on calcule dpgr=perte nette
c dpgr=(N2(A21+B21J)-N1B12J)*hnu/NeNh a partir de l'equation d'equilibre
c     =(N1C12Ne-N2C21Ne [+NionR2Ne] [-N2Aa])*hnu/NeNh 
                if(kr.le.nraissfek.or.kr.gt.nraissfek+24+nuta) then
                bb=cjn(kr)*B12hnus4pi(kr)                    ! B12J
        gngn=(bb + ee*cc + dd)/(aa + cc + bb)
        dpgr=dens(nij1)/nht *(texcoll(kr,m)*(ee-gngn)+dd/ne)*hca/alambda
        dperte=demt *pi4/hnne                                ! N2A21hnu
        pij(i,j)=pij(i,j) + dpgr
        p_graip=p_graip + dpgr
        if(kr.gt.nraissfek+24+nuta) pgsup=pgsup + dpgr
        praiperm=praiperm + dperte                                  ! pour info
c       praieap=praieap+dens(nij1)/nht*ee*texcoll(kr,m)*hca/alambda
c
                else if(kr.le.nraissfek+16) then     ! raie K du fer I a XVI
        dpgr=dens(nijfe)*tphotk(ife,j)*pfluo(ife)
     &                     *(pkiek(ife,j)-pkiev(ife,j))*chel/hnne
        pertefluo=pertefluo + dpgr
        pij(ife,11)=dpgr
        if(kitt.ge.1) pf6p4kev=pf6p4kev + dpgr
c
                else if(kr.le.nraissfek+24) then ! raie K du fer XVII a XXIV
                bb=cjn(kr)/alambda *0.02654d0                ! B12J/flambda
        gngn=(dd + bb)/(aa/pfluo(ife) + bb)
        dpgr=(dens(nijfe)*tphotk(ife,j) - dens(nij1)*gngn
     &       *(1.d0-pfluo(ife))/pfluo(ife) *A21(kr))*hnuev*chel/hnne
        pertefluo=pertefluo + dpgr
        pij(ife,11)=dpgr
        if(ife.le.18.and.kitt.ge.1) pf6p4kev=pf6p4kev + dpgr
                endif   ! kr/nraissfek
c
c        if(ifich.ge.4.and.ifich.le.8.and.kitt.eq.1.and.kitn.eq.1.and.
c     &  m.le.80.and.(m.eq.1.or.m.eq.2.or.m.eq.35.or.mod(m,20).eq.0))then
c        if(kr.eq.nraissfek+17) write(85,*) 'nitf nitt m kr ife '//
c     &  'aa bb cc dd gngn tjr dens(nijfe) dens(nij1) cjn tphotk dpgr'
c        write(85,'(i2,i3,2i4,i5,1p12e11.3)') nitf,nitt,m,kr,ife,aa,bb,cc,
c     &  dd,gngn,tjr(kr,m),dens(nijfe)/nht/ab(jfe),dens(nij1)/nht/ab(jfe),
c     &  cjn(kr),tphotk(ife,j),dpgr
c                 endif   ! imp
c
        dgainr=capabs*tjr(kr,m)*pi4/hnne
        graie=graie + dgainr
c
        if(ifich.ge.4.and.ifich.le.8
     &     .and.kimpf.eq.1.and.kimpm.eq.1.and.kitt.eq.1.and.kitn.eq.1
     &     .and.(abs(dgainr).gt.3.d-25.or.dpgr.gt.3.d-25))
     &     write(85,'(2i4,i5,1p6e11.3)') nitf,m,kr,tjr(kr,m),dgainr,
     &           dpgr,dens(nij1),gngn*dens(nij1),cjn(kr)
c
c calcul du chauffage Compton                        faut-il le mettre ???
c      dans scompt1 les induits du Compton ne sont pas mis pour 
c      les photons des raies car ils font intervenir J et non Jdnu
       altar=1.d0/(1.d0+ hnuev*8.6675d-6 + hnuev*hnuev*3.827d-12)
       scompt1=scompt1 + dir(kr)*hnuev*6.6262d-27/pi4 *altar*hnuev     !Jdnuev
       scompt2=scompt2 + dir(kr)*hnuev*6.6262d-27/pi4 *altar*
     1         (1.d0- altar*hnuev*(8.6675d-6 + hnuev*7.655d-12)/4.d0)
       sjdnuev=sjdnuev + dir(kr)*hnuev*chel/pi4/esh
          if(kali(kr).eq.1.or.nitf.eq.1)
     & sjdnuevrai=sjdnuevrai+dir(kr)*hnuev*chel/pi4/esh
c      scompt3=scompt3 + hj*de *hnuev
c
                        if(kitt.eq.0.or.kitn.eq.0) goto 403
c
        if(kimpf2.eq.1.and.ifich.eq.9.and.m.ne.npt)
     &     WRITE(21,'(f10.3,i6,1p3e13.4)') hnuev,kr,
     &     dhaut*V3*capal0,dhaut*V3*(capabs+cthomson)
        if(imp.ge.1.and.kimpm.eq.1.and.(kr.eq.63.or.kr.eq.68 
     &     .or.kr.eq.137.or.kr.eq.142.or.kr.eq.508)) then
                if(kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.8) then
        dgainr1=capabs*tjr(kr,m)*pi4/hnne
        dpgr1=dpgr
       hn2a21hnudz=gngn*dens(nij1)*A21hnu(kr)*dhaut
                        hnrb=0.d0
       if(gngn.gt.0.d0) hnrb=(gngn*A21(kr) - B12hnus4pi(kr)
     &                 *cjn(kr)*(1.d0-gngn))/(gngn*A21(kr))

       WRITE(21,*)'a  kr  m  col  T  dz  N(nij1)  N(nij2)  nrb  ', 
     & 'dpgr  dgainr  emmc  emmr0  emmrt  cthomson  kappac  kappar0  ',
     & 'kappart  rtaur0rai  dnudoppler  Jc  Jphi      Jr'
       WRITE(21,489) kr,m,tcol(m),T,dhaut,dens(nij1),gngn*dens(nij1),   ! @
     &      hnrb,dpgr1,dgainr1,emmc,dem0,demt,cthomson,capabs,capal0,
     &      capalt,rtaur0(kr),dnudoppler,
     &      tjc(kc,m),cjn(kr)*hnuev*chel/pi4,tjr(kr,m)
c 489   format('@',2i4,23(1x,1pe17.10))
       if(kali(kr).eq.1) then
          do lf=1,nf
          hirp=hirp+(priplus(lf,kr)
     &       +sourcer(lf,kr,m)*(1.d0-exdtauc(kc,m-1))/2.d0)*fint(lf)*u0
          hirm=hirm+(rimoins(lf,kr,m+1)
     &       +sourcer(lf,kr,m)*(1.d0-exdtauc(kc,m))/2.d0)*fint(lf)*u0
          prof=FPROF(lf,am,x,cx)
          phirp=phirp+(priplus(lf,kr) + sourcer(lf,kr,m)
     &       *(1.d0-exdtauc(kc,m-1))/2.d0)*fint(lf)*u0*prof/whz
          phirm=phirm+(rimoins(lf,kr,m+1) + sourcer(lf,kr,m)
     &       *(1.d0-exdtauc(kc,m))/2.d0)*fint(lf)*u0*prof/whz
          enddo
       hicp=priplus(nf+1,kr)+sourcer(nf+1,kr,m)
     &      *(1.d0-exdtauc(kc,m-1))/2.d0
       hicm=rimoins(nf+1,kr,m+1)+sourcer(nf+1,kr,m)
     &      *(1.d0-exdtauc(kc,m))/2.d0
       WRITE(21,4891) kr,m,hirp,hirm,phirp,phirm,capalt*(phirm-hicm),   !2345678
     &capalt*hicm,capabs*(hirm-hicm),cthomson/2.d0*(hirm-hirp-hicm+hicp)
 4891  format('TAM',2i4,10(1x,1pe17.10))
       endif
                endif
        if(ifich.ge.4.and.ifich.le.8.and.kimpf.eq.1.and.kr.eq.k5) then
        WRITE(22,*) 'nitd,kr,dens(nij1),   am,     somphi,   emmc,',
     &          '    dem0,   capabs,  capal0,  dgainr'
        WRITE(22,'(2i3,8(1pe9.2))') nitd,kr,dens(nij1),am,emmc,
     &  dem0,capabs,capal0,dgainr1
        WRITE(22,*)' m, kr,  Jmoy,    hjc,    I+(nf+1),',
     &  'I-(nf+1),dtau0rai,rtaur0rai,rtaurl,  dpgr'
        WRITE(22,'(i4,i3,8(1pe9.2))') m,kr,cjn(kr)*hnuev*chel/pi4,
     &  tjc(kc,m),hicp,hicm,dhaut*V3*capal0,rtaur0(kr),rtaurl(kr),dpgr1
           endif  ! ifich
        endif  ! imp kr                                                    !sr0
        if(imp.eq.8.and.nitf.eq.nfmax.and.kimpm.eq.1.and.kitd.eq.1
     &                    .and.kitt.eq.1.and.kitn.eq.1) then
        act=0.d0
        if(cc.gt.0.d0.and.rtaur0(kr).gt.0.d0) act=aa/cc/rtaur0(kr)
        WRITE(51,490) m,tz(m),T,kr,alo(kr),hnuev,fdo(kr),
     &  kr,dens(nij1),gngn,ee,aa,bb,cc,dd,act,
     &  kr,am,dem0,emmc,capal0,capabs,cthomson,
     &  kr,dhaut*V3*capal0,rtaur0(kr),rtaurl(kr),rtaucef(kc),
     &  kr,dpgr,dperte,dgainr,
     &  kr,cjn(kr)*hnuev*chel/pi4,tjc(kc,m),                            ! hj0,
     &  hnuev*hnuev*hnuev*ee/(1.d0-ee) *2.084506d-4
        endif   ! imp=8
 490    format(i4,2(';',1pe10.3),';',i4,3(';',1pg11.4),';',i4,8(';',
     &  1pe10.3),';',i4,6(';',1pe10.3),';',
     &  i4,4(';',1pe10.3),';',i4,3(';',1pe11.3),';',i4,4(';',1pe10.3))
c
        if(incang.eq.0) then
           rtaur0(kr)=rtaur0(kr) + dhaut*V3*capal0 ! raie seule au centre
        else
           rtaur0(kr)=rtaur0(kr) + dhaut*capal0 ! raie seule au centre
        endif
c
        if(m.eq.1) then
           dto=dhaut*capal0
           if(dto.gt.dtaurmax0) then
              dtaurmax0=dto
              krtormax0=kr
           endif
        else if(m.eq.npt-1) then
           dto=dhaut*capal0
           if(dto.gt.dtaurmaxH) then
              dtaurmaxH=dto
              krtormaxH=kr
           endif
        endif

        if(m.eq.npt-1) then
           rtaurl(kr)=dhaut*V3*capal0
           if(rtaurl(kr).le.1.d-40) rtaurl(kr)=0.d0
        endif
c
           if(kali(kr).eq.1.or.(itali.eq.0.and.isuite.le.1))
     &  CALL TR2dirB(kc,kr,lcomp)
c
c if(kali(kr).gt.1.and.itali.eq.1)then
              if(capalt.lt.0.d0) capalt=0.d0
        dcapr(kr,m)=capalt
              dsl(kr,m)=0.d0
        if(kr.le.nraissfek.or.kr.gt.nraissfek+24+nuta) then
           if(capalt.gt.0.d0) dsl(kr,m)=demt/capalt
c les dd sont grands et donnent parfois eps<0, donc on calcule 1-eps=As=tas
           tas(kr,m)=aa/(aa + cc*(1.d0-ee) - dd)
           tbs(kr,m)=hnuev/(alambda**2) *2.d16*chel               !2hnu3/c2
     &              *(cc*ee + dd)/(aa + cc*(1.d0-ee) - dd)
        else if(kr.le.nraissfek+16) then
           dsl(kr,m)=demt                              ! dsl=emm ds ce cas
           tas(kr,m)=0.d0
           tbs(kr,m)=0.d0
        else if(kr.gt.nraissfek+16) then
           if(capalt.gt.0.d0) dsl(kr,m)=demt/capalt
           tas(kr,m)=aa/(aa/pfluo(ife) - dd)
           tbs(kr,m)=hnuev/(alambda**2) *2.d16*chel
     &              *dd/(aa/pfluo(ife) - dd)
        endif   ! kr/nraissfek
           if(tbs(kr,m).lt.0.d0) then
           endif
        lcompt(m,kr)=lcomp
        if(lcomp.eq.1) then
           if(ncomp(kr).eq.0) mdebcomp(kr)=m
           ncomp(kr)=ncomp(kr) + 1
        endif
        som=0.d0
        do lf=1,nf
           prof=FPROF(lf,am,x,cx)
           som=som + prof *fint(lf)
           tphi(lf,kr,m)=prof
        enddo
        do lf=1,nf
           tphi(lf,kr,m)=tphi(lf,kr,m)/som/u0
           twnu(lf,kr,m)=fint(lf)*u0
        enddo
           tphi(nf+1,kr,m)=0.d0
           twnu(nf+1,kr,m)=0.d0                                            !sr0
c endif   ! kali - nitf>itali
c
                if(kimpf2.eq.0) goto 403
                if(imp.lt.5.or.imp.gt.7.or.kimpm.eq.0) goto 403
                if(dpgr.lt.1.d-40) goto 403
        mimp=mimp+1
        jimp=2-mod(mimp,2)
        itz(jimp)=iz(j)
        iti(jimp)=i
        ikr(jimp)=kr
        t1(jimp)=alambda
        t2(jimp)=am
        t3(jimp)=gngn
        t4(jimp)=dpgr
        t5(jimp)=dgainr
        if(jimp.eq.2.and.ifich.ge.3.and.ifich.le.8) WRITE(16,481)
     &    (ikr(k),iti(k),t2(k),t3(k),t4(k),t5(k),k=1,2)
        if(ifich.eq.2.and.jimp.eq.2.and.ifich.ge.4.and.ifich.le.8)
     &    WRITE(22,481) (ikr(k),iti(k),t2(k),t3(k),t4(k),t5(k),k=1,2)
  481   format(i4,i3,4(1pe8.1),i4,i3,4(1pe8.1))
  403        continue
  402        continue
  401        continue
       if(kimpf2.eq.0) goto 404
       if(kitd.eq.0.or.kitt.eq.0.or.kitn.eq.0.or.imp.lt.5.or.imp.gt.7
     &                   .or.kimpm.eq.0) goto 404
        if(ifich.ge.3.and.ifich.le.8) WRITE(16,482)
        if(ifich.ge.4.and.ifich.le.8.and.ila.eq.0) WRITE(22,482)
        if(ifich.ge.4.and.ifich.le.8.and.ila.eq.1) WRITE(22,982)
  482   format (/20x,'pertes raies permises par ion/(Ne*H*Nh)')
  982   format (/20x,'permitted line cooling per ion/(Ne*H*Nh)')
        do j=1,nelem
        iz0=iz(j)
       if(ifich.ge.3.and.ifich.le.8)WRITE(16,287) iz0,(pij(i,j),i=1,iz0)
 287    format(' z=',i2,1x,8(1pe9.2)/6x,8(1pe9.2)/6x,8(1pe9.2)/6x,
     &       8(1pe9.2))                                                 !sd
       if(ifich.ge.4.and.ifich.le.8)WRITE(22,287) iz0,(pij(i,j),i=1,iz0)
        enddo
  404        continue                                                      !sr0
c
                if(kitd.eq.0) goto 454
        if(ifich.ge.3.and.ifich.le.8.and.imp.ge.5.and.imp.le.7
     &            .and.kimpf2.eq.1.and.kitt.ge.1.and.kimpm.eq.1) then
        WRITE(16,485) pertefluo,pf6p4kev
        if(ifich.ge.4) WRITE(22,485) pertefluo,pf6p4kev
  485   format(/'pertes raies fluo.Fer/(HNeNh) : total=',1pe12.3,
     1                        ' a 6.4kev (j<19)=',1pe12.3)
        WRITE(16,287) iz0,(pij(i,11),i=1,24)
        if(ifich.ge.4) WRITE(22,287) iz0,(pij(i,11),i=1,24)
        endif
  454        continue                                                      !sr0
c
        if(ifich.eq.11.and.kimpf.eq.1.and.kitt.eq.1.and.kitn.eq.1
     &                    .and.kitd.eq.1) then
        WRITE(28,*)' 4piJdnu/hnu raies'
        WRITE(28,'(8(1pe10.3))') (dir(k),k=1,nrais)
        endif   ! ifich=11
        if(imp.eq.2.and.kitd*kitt*kitn*kimpf2.eq.1.and.kimpm.eq.1
     &     .and.ifich.ge.3.and.ifich.le.8) then
c     &     (mod(m,5).eq.0.or.m.eq.1.or.m.eq.npt)) then
           WRITE(16,482)
           do j=1,nelem
              iz0=iz(j)
              WRITE(16,287) iz0,(pij(i,j),i=1,iz0)
           enddo
           WRITE(16,287) iz0,(pij(i,11),i=1,24)
        endif
        if(imp.ge.5.and.imp.le.7.and.kimpf2.eq.1.and.ifich.ge.3.and.
     &     ifich.le.8.and.kitd.ge.1.and.kitt.ge.1.and.kimpm.eq.1) then
        WRITE(16,'(a,1pe10.3)')
     &  ' pertes nettes dues aux 3118 raies supplementaires/(HNeNh): ',
     &    pgsup
           if(ifich.ge.4)
     &  WRITE(22,'(a,1pe10.3)')
     &  ' pertes nettes dues aux 3118 raies supplementaires/(HNeNh): ',
     &    pgsup
        endif
c------------------------------------------------------------------------------
c  CALCUL DU SPECTRE DES RAIES UTA (saut d'un electron de la couche interne)
c        dir(kr)=4pi*Somme(Jr(nu)*dnu)/hnu
c nuta raies dites UTA des ions du FeI a FeXVI et quelques ions de Ne a Fe
c (transitions des couches L a M)
c traitees "1 seul niveau" (sans interactions avec les autres niveaux)
c kr > nraissfek+24
c
           guta=0.d0
        do j=1,nelem
           iz0=iz(j)
           do i=1,iz0
              utanion(i,j)=0.d0
           enddo
        enddo
        do 410 ku=1,nuta
           j=juta(ku)
           i=iuta(ku)
           kr=nraissfek+24 + ku
           dpgr=0.d0
           dgainr=0.d0                                                     !sru
           dperte=0.d0
           demt=0.d0
           capalt=0.d0
           alambda=alo(kr)
           nij1=nivcum(i-1,j) + 1
           hnuev=hcse/alambda
c  whz=largeur (en Hz) de la raie
c  am est la constante d'amortissement = sommeA/4pi/dnudoppler g2/g1moyen=2
        dnudoppler=vth(j)/alambda
        u0=uhz(kr)
        cx=1.d0
        dnut=sqrt(dnudoppler**2+dnuta2(ku))
           if(vturbkm.gt.1.d-3) then                              ! TURBULENCE
        dnut=sqrt(dnudoppler**2+dnuta2(ku)+(vturbkm*1.d13/alambda)**2)
              cx=u0/dnut
           endif
           if(iprof.le.1) then                      ! Voigt 
        am=A21(kr) /(pi4*dnut) *0.5d0               ! 0.5 val.moy.de gn1/gn2 ?
           else if(iprof.eq.2) then                   ! Doppler thermique
              am=0.d0
           endif   ! iprof
           whz=dnut*Vpi
        if(m.eq.1) twr(kr)=whz                                         !sru
        tws(kr)=whz
           lcomp=0     ! dEcompt/E=(E-4kT)/mc2;lcomp=1 si dnucomp>ic*dnudoppler
           decomp=hnuev*(4.d0*tk-hnuev)*4.731792d8         ! 2.42e14/511kev
           if(nitf.gt.1.and.icompt.ge.1.and.
     &        abs(decomp).ge.whz*hicompt) lcomp=1
           if(nitf.gt.1.and.icompt.eq.-1) lcomp=1
c  calcul du nombre d'atomes excites N2 : N2/N1=(B12J+C12Ne)/(A21+Aa)
                ee=teer(kr,m)
                aa=A21(kr)                                    ! A21g2/g1
                bb=cjn(kr)*B12hnus4pi(kr)                     ! B12J
                am=aa*g1rt(kr)/g2rt(kr)/probuta(ku) /(pi4*dnudoppler)
                cc=ne*texcoll(kr,m)                           ! NeC21g2/g1
                   dd=0.d0
        gngn=(bb + ee*cc)*probuta(ku)/aa                      ! g1N2/g2N1
        demt=gngn*dens(nij1)*A21hnus4pi(kr)                   ! c'est A21*g2/g1
        capalt=dens(nij1)*B12hnus4pi(kr) *(1.d0-gngn)
                   if(capalt.lt.0.d0)
     &             capalt=dens(nij1)*B12hnus4pi(kr) *(1.d0-ee)
        dem0=demt/whz
        capal0=capalt/whz
        utanion(i,j)=utanion(i,j) + (bb + ee*cc)*(1.d0-probuta(ku))  ! N2Aa/N1
        kc=kcr(kr)
        capabs=tcapc(kc,m)                                                 !sru
        emmc=temc(kc)
c
           if(kali(kr).eq.1.or.(itali.eq.0.and.isuite.le.1))then
! indispensable pour nitf=1
        CALL TR2dirA(capalt,demt,am,u0,cx,kc,kr,lcomp)
           endif   ! kali - nitf/itali
        dir(kr)=tjr(kr,m)  *pi4 /hnuev/chel
        cjn(kr)=tcjn(kr,m)
c
                        if(kitd.eq.0) goto 410
c
c pertes=4pi*emm/(NhNe) et gains=4pi*kappa*J/(NhNe),on calcule dpgr=perte nette
c dpgr=(N2(A21+B21J)-N1B12J)*hnu/NeNh a partir de l'equation d'equilibre
c     =(N1C12Ne-N2C21Ne [+NionR2Ne] [-N2Aa])*hnu/NeNh
                bb=cjn(kr)*B12hnus4pi(kr)                    ! B12J
        gngn=(bb + ee*cc)*probuta(ku)/aa                     ! g1N2/g2N1
        dpgr=dens(nij1)/hnne *(cc*(ee-gngn)
     &        - (bb+ee*cc)*(1.d0-probuta(ku))) *hca/alambda     ! -N2Aa/N1
        dperte=demt *pi4/hnne                                 ! N2A21hnu
        p_graip=p_graip + dpgr
        praiperm=praiperm + dperte                            ! pour info
        guta=guta - dpgr                                      ! pour info
        dgainr=capabs*tjr(kr,m)*pi4/hnne                                   !sru
        graie=graie + dgainr
c
        if(ifich.ge.4.and.ifich.le.8
     &     .and.kimpf.eq.1.and.kimpm.eq.1.and.kitt.eq.1.and.kitn.eq.1
     &     .and.(abs(dgainr).gt.3.d-25.or.abs(dpgr).gt.3.d-25))
     &     write(85,'(2i4,i5,1p6e11.3)') nitf,m,kr,tjr(kr,m),dgainr,
     &           dpgr,dens(nij1),gngn*dens(nij1),cjn(kr)
c
c calcul du chauffage Compton
       altar=1.d0/(1.d0+ hnuev*8.6675d-6 + hnuev*hnuev*3.827d-12)
       scompt1=scompt1 + dir(kr)*hnuev*6.6262d-27/pi4 *altar*hnuev     !Jdnuev
       scompt2=scompt2 + dir(kr)*hnuev*6.6262d-27/pi4 *altar*
     1         (1.d0- altar*hnuev*(8.6675d-6 + hnuev*7.655d-12)/4.d0)
       sjdnuev=sjdnuev + dir(kr)*hnuev*chel/pi4/esh
          if(kali(kr).eq.1.or.nitf.eq.1)
     & sjdnuevrai=sjdnuevrai+dir(kr)*hnuev*chel/pi4/esh
c
                        if(kitt.eq.0.or.kitn.eq.0) goto 410
c
        if(incang.eq.0) then
           rtaur0(kr)=rtaur0(kr) + dhaut*V3*capal0 ! raie seule au centre
        else
           rtaur0(kr)=rtaur0(kr) + dhaut*capal0 ! raie seule au centre
        endif
        if(m.eq.1) then
           dto=dhaut*capal0
           if(dto.gt.dtaurmax0) then
              dtaurmax0=dto
              krtormax0=kr
           endif
        else if(m.eq.npt-1) then                                           !sru
           dto=dhaut*capal0
           if(dto.gt.dtaurmaxH) then
              dtaurmaxH=dto
              krtormaxH=kr
           endif
        endif

        if(m.eq.npt-1) then
           rtaurl(kr)=dhaut*V3*capal0
           if(rtaurl(kr).le.1.d-40) rtaurl(kr)=0.d0
        endif
c
           if(kali(kr).eq.1.or.(itali.eq.0.and.isuite.le.1))
     &  CALL TR2dirB(kc,kr,lcomp)
c
              if(capalt.lt.0.d0) capalt=0.d0
        dcapr(kr,m)=capalt
              dsl(kr,m)=0.d0
           if(capalt.gt.0.d0) dsl(kr,m)=demt/capalt
           tas(kr,m)=probuta(ku)
           tbs(kr,m)=hnuev/(alambda**2) *2.d16*chel
     &              *cc*ee*probuta(ku)/aa                                  !sru
           if(tbs(kr,m).lt.0.d0.and.ifich.ge.5.and.ifich.le.8) then
              write(3,'(2i4,1p6e11.3)') kr,m,aa,bb,cc,dd,ee,tbs(kr,m)
              tbs(kr,m)=0.d0
           endif
        lcompt(m,kr)=lcomp
        if(lcomp.eq.1) then
           if(ncomp(kr).eq.0) mdebcomp(kr)=m
           ncomp(kr)=ncomp(kr) + 1
        endif
        som=0.d0
        do lf=1,nf
           prof=FPROF(lf,am,x,cx)                                          !sru
           som=som + prof *fint(lf)
           tphi(lf,kr,m)=prof
        enddo
        do lf=1,nf
           tphi(lf,kr,m)=tphi(lf,kr,m)/som/u0
           twnu(lf,kr,m)=fint(lf)*u0
        enddo
           tphi(nf+1,kr,m)=0.d0
           twnu(nf+1,kr,m)=0.d0
c
 410    continue
c
                if(kitd.eq.0) goto 411
        if(imp.ge.5.and.imp.le.7.and.ifich.ge.3.and.ifich.le.8
     &           .and.kimpf2.eq.1.and.kitt.ge.1.and.kimpm.eq.1) then
        WRITE(16,'(a,1pe10.3)')
     &       ' gains nets dus aux raies UTA/(HNeNh): ',guta
           if(ifich.ge.4)
     &  WRITE(22,'(a,1pe10.3)')
     &       ' gains nets dus aux raies UTA/(HNeNh): ',guta
        endif
 411    continue                                                           !sru
c------------------------------------------------------------------------------
c
                          if(kitd.eq.0) then
c
c   CALCUL DU NOMBRE DE PHOTOIONISATIONS INTERNES TPHOT(nij)
        esionh=0.d0
                do 600 jn=1,nelem                                           !sp
        inz0=iz(jn)
                do 600 in=1,inz0
        if(r(in,jn).le.0.d0) goto 600
           niv=nivion(in,jn)
                do 601 nn=1,niv
           nij=nivcum(in-1,jn) + nn
        tphot(nij)=0.d0
        alfinduit(nij)=0.d0
           akiev=hkin(nij)
           uu=gps(nij)/gfond(in+1,jn)
c                endif
c  calcul du nombre de photoionisations dues aux raies
        sphok=0.d0
        sphor=0.d0
        salfinduit=0.d0
        kra=0
        do 610 jr=1,nelem
        irz0=iz(jr)
        do 610 ir=1,irz0
        kri=kra+1
        kra=kra+nrai(ir,jr)
        do 611 kr=kri,kra
           if(kr.eq.nrcum(irz0,jr)+1) goto 611       !???
        alambda=alo(kr)
        hnuev=hcse/alambda
        if(hnuev.lt.akiev.or.dir(kr).eq.0.d0)goto 611
        sphor=sphor+ sigmar(kr,nij)*dir(kr)
c                         if(sigmar(kr,nij)*dir(kr).lt.0.d0)then
c                          kcc=kcr(kr)
c                          if(nij.eq.6.and.nerrpho.le.20.and.kitt.eq.1
c     &                    .and.kitd1.eq.1.and.dir(kr)+dic(kcc).lt.0.d0) 
c     &                    write(6,'(2i4,1p5e11.3)') m,kr,sigmar(kr,nij),1234567
c     &                       dic(kcc),dir(kr),tjc(kcc,m),tjr(kr,m)
c                          endif
                if(((jn.ge.3.and.in.le.(inz0-3)).or.
     &  (jn.eq.10.and.in.eq.(inz0-2))).and.hnuev.ge.pkiek(in,jn))
     &        sphok=sphok + sigmark(kr,nij)*dir(kr)
  611        continue                                                ! kr
  610        continue                                                ! ir,jr
c  calcul du nombre de photoionisations dues aux raies de fluorescence du Fer
        jfe=nelem
        ifz3=iz(jfe)-3
        do 615 ife=1,ifz3                                                   !sp
           hnuev=efluo(ife)
           kr=nraissfek+ife
           if(dir(kr).eq.0.d0.or.hnuev.lt.akiev) goto 615
           sphor=sphor+ sigmar(kr,nij)*dir(kr)
                if(jn.ge.3.and.jn.le.9.and.in.le.(inz0-3).and.
     &  hnuev.ge.pkiek(in,jn)) sphok=sphok + sigmark(kr,nij)*dir(kr)
  615   continue        ! ife
c  calcul du nombre de photoionisations dues aux raies UTA
        do 616 ku=1,nuta
           kr=nraissfek+24 + ku
           hnuev=hcse/alo(kr)
           if(dir(kr).eq.0.d0.or.hnuev.lt.akiev) goto 616
           sphor=sphor+ sigmar(kr,nij)*dir(kr)
                if(jn.ge.3.and.jn.le.9.and.in.le.(inz0-3).and.
     &  hnuev.ge.pkiek(in,jn)) sphok=sphok + sigmark(kr,nij)*dir(kr)
 616    continue        ! ku
c  calcul du nombre de photoionisations dues aux 3000 raies supplementaires
        if(inr.eq.2) then
           do 617 ks=1,ns
              kr=nraissfek+24+nuta + ks
              hnuev=hcse/alo(kr)
              if(dir(kr).eq.0.d0.or.hnuev.lt.akiev) goto 617
              sphor=sphor+ sigmar(kr,nij)*dir(kr)
                if(jn.ge.3.and.jn.le.9.and.in.le.(inz0-3).and.
     &  hnuev.ge.pkiek(in,jn)) sphok=sphok + sigmark(kr,nij)*dir(kr)
 617       continue           ! ks
        endif
c
c  calcul du nombre de photoionisations dues au continu
        sphoc=0.d0
        do 620 kc=1,184
           hnuev=thnuc(kc)
           deb=tdeb(kc)
           if(hnuev+deb.le.akiev) goto 620
           de=tde(kc)
           dea=de-deb
c           eem=tekc(kc,nij)
                di=1.d0
                if(tk.lt.de) di=tk/de
                         if(kc.le.102) then
           if(dic(kc).le.0.d0.and.dic(kc+220).le.0.d0) goto 620
        dict=dic(kc)+dic(kc+220)
                ic=ict(kc)
                jc=jct(kc)
           if(nn.eq.1.and.jc.eq.jn.and.ic.eq.in) then                       !sp
              dict=dic(kc)
              di=min(1.d0,tk/deb)
           endif                ! ic,jc=in,jn
           if(nn.gt.1.and.hnuev-dea.le.akiev) then    ! seuil dans l'intervalle
              di=min(1.d0,tk/(hnuev+deb-akiev))
              if(hnuev.lt.akiev) then
                 dict=dic(kc)*(hnuev+deb-akiev)/deb
              else
                 dict=dic(kc+220)*(hnuev-akiev)/dea + dic(kc)
              endif
           endif
                         else   ! kc>102
           if(dic(kc).le.0.d0) goto 620
        dict=dic(kc)
        if(nn.gt.1.and.hnuev-dea.le.akiev) then       ! seuil dans l'intervalle
           di=min(1.d0,tk/(hnuev+deb-akiev))
           dict=dict*(hnuev+deb-akiev)/de
        endif
                         endif  ! kc/102
        sphoc=sphoc+sigmac(kc,nij)*dict
        salfinduit=salfinduit + sigmacv(kc,nij)*dict*di
     &                                *tekc(kc,nij)*uu*saha
c        salfinduit=salfinduit + csigv(kc,nij)*dict*eem
c
                dk=1.d0
        if(jn.ge.3.and.in.le.(inz0-2)
     &                         .and.(hnuev+deb).ge.pkiek(in,jn)) then
        if(jn.ne.10.and.in.eq.(inz0-2)) goto 623  !pas d'Auger,fluo pour le fer
        if((hnuev-dea).lt.pkiek(in,jn))dk=(hnuev+deb-pkiek(in,jn))/de
        if(kc.eq.184) dk=dk*3.4d0         
c tres approximatif : tient compte du reste du flux au-dessus de 26 kev
        sphok=sphok + sigmack(kc,nij)*dict*dk
c        if(jn.eq.10.and.(in.eq.6.or.in.eq.7.or.in.eq.24))write(6,'(3i4,
c    &  5(1pe10.3))')jn,in,kc,hnuev,sigmack(kc,nij),dict,dk,sphok 
                endif    ! hnuev+deb
  623        continue
c
                enel=hnuev-akiev
        if(ph0ne.gt.1.d-3.and.enel.ge.13.6d0) then
                ds=1.d0                                                     !sp
c        if(kc.le.102.and.jc.eq.jn.and.ic.eq.in.and.nn.eq.1)
c     & ds=(tdeb(kc)-13.6d0)/de   plutot max(0.d0,(tdeb(kc)-13.6d0)/de)
        esionh=esionh + sigmac(kc,nij)*dict*ds*GFI(enel,ph0ne)
                     if(in.le.inz0-2) then
                     enelk=hnuev-pkiek(in,jn)
                     if(enelk.ge.13.6d0) 
     &  esionh=esionh + sigmack(kc,nij)*dict*GFI(enelk,ph0ne)
                     endif   ! k
                endif   ! ph0ne
c
  620        continue   ! jc
        do 625 kc=185,199
                if(dic(kc).le.0.d0) goto 625
           hnuev=thnuc(kc)
           deb=tdeb(kc)
           if((hnuev+deb).le.akiev) goto 625
           de=tde(kc)
           dea=de-deb
                di=1.d0
                if(tk.lt.de) di=tk/de            ! de est presque toujours < tk
                dp=1.d0
                ee1=tekc(kc,nij)     ! enlever95 si csigv
        if((hnuev-dea).le.akiev) then                 ! seuil dans l'intervalle
                dp=(hnuev+deb-akiev)/de
                di=min(di,dp)
                endif
        sphoc=sphoc+sigmac(kc,nij)*dic(kc)*dp
c      if(m.eq.1.and.kitt.eq.1.and.kitn.eq.1)write(77,'(3i4,0p2f7.3,1p4e10.3)')
c     &  nij,kc,nitd,hnuev,hnuev+deb,sigmac(kc,nij),dic(kc),dp
        salfinduit=salfinduit + sigmacv(kc,nij)*dic(kc)*di
     &                                *ee1*uu*saha
  625   continue                ! jc
        tphot(nij)=sphor + sphoc
        if(nn.eq.1) tphotk(in,jn)=sphok
        alfinduit(nij)=salfinduit
c
        if(m.eq.1) nerrpho=0
        if(tphot(nij).lt.0.d0) then
           if(kitt.eq.1.and.kitn.eq.1.and.kitd1.eq.1) then
              nerrpho=nerrpho+1
              if(nerrpho.le.20) write(6,670) 
     &     m,jn,in,nn,nij,nitd,nitt,T,kitt,tphot(nij),sphor,sphoc
 670       format(' PB tphot<0 a m=',i3,' j=',i2,' i=',i2,' n=',i2,
     &    ' nij=',i3,' nitd=',i2,' nitt=',i2,' T=',1pe10.3,' kitt=',i1/
     &    ' tphot=',1pe11.3,' sphor=',1pe11.3,' sphoc=',1pe11.3)
           endif
              tphot(nij)=sphoc
        endif   ! tphot<0
c
  601   continue
  600        continue                                                       !sp
        tphot(1)=tphot(1) + esionh
        if(kitd1.eq.1) kitd=1
        go to 12000
c
                          else if(kitd.eq.1) then
c
        praieint=0.d0
        if(ietl.eq.0) CALL LOSSA
c bilan des pertes et gains
        p_grait=p_graip + praieint + pertefluo - graie
        p_gfree=pfree - gfree
        p_gbound=precf + precsec + pdiel - gion + pech
        ptot=precf + precsec + pdiel + pfree + pech + p_grait + graie
c
c calcul du chauffage et refroidissement Compton
c formule de Tarter (communication privee, vers 1985)
c calcul d'apres Ferland et Rees 1988 ApJ332,p146 = Hazy de Cloudy p.273
c (a partir de nov 2008-titan105v01d.f)
c     chauf/Hnenh=(sigth/mec2)*(1/nht)*(somme de 4piJ_nu*hnu*dnu *altar)
c     perte/Hnenh=(sigth/mec2)*(1/nht)*4kT*(somme de 4piJ_nu*dnu *altar*betar)
c ch-pe/Hnenh=(sigth/mec2/nht)*(somme de 4piJ_nu*dnu*altar*betar)*4k*(Tcompt-T)
c     Tcompt=somme de (J*hnuev*alfa)/4k*somme de (J*alfa*beta)
c     avec alfa=1/(1+a1*hnu+a2*hnu**2)=altar beta=(1-alfa*hnu(a1+2*a2*hnu)/4)
c     a1=1.1792e-4/13.6   a2=7.084e-10/13.6^2
c         esh=2.41797d14  4k=5.52248d-16  4pisigth/me*c2=1.02099d-17
          sx1=scomptx1*expa(-rtauthx*0.65d0)      ! attenuation de flxdurincid
          sx2=scomptx2*expa(-rtauthx*0.65d0)
c scomptx1-2 contient un facteur ftauth prenant en compte le reflechi...
c contribution des hnu>26kev(O.65 est la pente habituelle des resultats de Noar)
c
       if(acompt.eq.0.d0.and.bcompt.eq.0.d0) then ! approx.TARTER sur tt le sp.
          pcompt=T*5.52248d-16*(scompt2+sx2)*esh/nht*1.02099d-17
c          gcompt=(scompt1+sx1)*chel*esh/nht*1.02099d-17                  zzz
          gcompts=scompt1*chel*esh/nht*1.02099d-17 ! <26keV Compton heating zzz
          gcomptx=sx1*chel*esh/nht*1.02099d-17 !     >26keV Compton heating zzz
          gcompt=gcompts + gcomptx                                      ! zzz
                    Tcompt1=0.d0
                    if(scompt2+sx2.gt.0.d0)
     &    Tcompt1=(scompt1+sx1)/(scompt2+sx2)*chel/5.52248d-16
c
       else          ! avec les coef. tires de NOAR pour les energies >26kev
          pcompt=T*5.52248d-16*scompt2*esh/nht*1.02099d-17
c le refroidissement Compton par les photons>26kev est inclus ds le gcomptx Noar
          gcompts=scompt1*chel*esh/nht*1.02099d-17 ! <26keV Compton heating
          gcompt=gcompts + gcomptx
          Tcompt1=gcompt*nht/(scompt2+sx2)/esh*1.7735538d+32 !me*c2/(4k4pisigth)
       endif
       g_pcompt=gcompt - pcompt
c        goto 12001 
c si kitd=1 sortie de l'iteration sur les degres d'ionisation
                          endif   ! kitd
c
c   FIN DE L'ITERATION sur les degres d'ionisation                          !st
12001        continue
       if(ifich.ge.3.and.ifich.le.8) then
          if((imp.eq.4.and.imppgt.eq.0).or.
     &       (m.eq.1.and.nitf.eq.1.and.nitn.eq.1.and.nitt.eq.1)) then
             pbenef=(g_pcompt-p_grait-p_gbound-p_gfree)/ptot *100.d0   ! g-p/p
                if(ila.eq.0) then
             WRITE(16,780)  tz(m),T,m,nht,ecartdeg*100.d0,pbenef,
     &            gfree,gion,g_pcompt,graie,praiperm,
     &       pfree,precf,precsec+pdiel,p_graip,praieint+pertefluo+pech
                else
             WRITE(16,980)  tz(m),T,m,nht,ecartdeg*100.d0,pbenef,
     &       gfree,gion,g_pcompt,graie,pfree,precf,precsec+pdiel,p_graip
                endif
             WRITE(16,*)
          endif   ! imp=4
          if((imp.ge.5.and.imp.le.7).or.                                    !st
     &       (imp.ge.1.and.kitt.eq.1.and.kitn.eq.1.and.kimpm.eq.1)) then
             pbenef=(g_pcompt-p_grait-p_gbound-p_gfree)/ptot *100.d0   ! g-p/p
             if(kimpf2.eq.1.and.imppgt.eq.0) then
                if(ila.eq.0)WRITE(16,780) tz(m),T,m,nht,ecartdeg*100.d0, !.tx2
     &             pbenef,gfree,gion,g_pcompt,graie,praiperm,
     &       pfree,precf,precsec+pdiel,p_graip,praieint+pertefluo+pech
                if(ila.eq.1)WRITE(16,980) tz(m),T,m,nht,ecartdeg*100.d0,
     &             pbenef,gfree,gion,g_pcompt,graie,
     &             pfree,precf,precsec+pdiel,p_graip
             endif     ! nitf
             if(ifich.ge.4.and.kimpf2.eq.1.and.imppgt.eq.0)then
                if(ila.eq.0)WRITE(22,780) tz(m),T,m,nht,ecartdeg*100.d0, !.txa
     &             pbenef,gfree,gion,g_pcompt,graie,praiperm,
     &       pfree,precf,precsec+pdiel,p_graip,praieint+pertefluo+pech
                if(ila.eq.1)WRITE(22,980) tz(m),T,m,nht,ecartdeg*100.d0,
     &             pbenef,gfree,gion,g_pcompt,graie,
     &             pfree,precf,precsec+pdiel,p_graip
             endif     ! ifich=4
           endif     ! imp=5-6-7
        endif   ! ifich
 780    format(/' z=',1pe9.3,' T=',1pe9.3,' m=',i3,' nht=',1pe9.3,
     &                   ' ec.deg%=',0pf8.4,' p-g/p%=',0pf10.4/
     &     ' gain fre=',1pe10.3,',io=',1pe10.3,',g-pCo=',1pe10.3,
     &     ',r=',1pe9.2,'/prai=',1pe10.3/' perte fre=',1pe10.3,',fo=',
     &       1pe10.3,',sec=',1pe10.3,',p-gr=',1pe9.2,',div=',1pe9.2)
 980    format(/' z=',1pe9.3,' T=',1pe9.3,' m=',i3,' nht=',1pe9.3,
     &                   ' ec.deg%=',0pf8.4,' c-h/c%=',0pf10.4/
     &     ' heat free=',1pe10.3,',ion=',1pe10.3,',h-cComp=',1pe10.3,
     &     ',line=',1pe9.2/' cool free=',1pe10.3,',radrec=',
     &       1pe10.3,',+',1pe10.3,',line-net=',1pe9.2)
c
                if(imppgt.eq.1) then
        benef=g_pcompt-p_grait-p_gbound-p_gfree                            !st
        WRITE(40,'(1pe11.3,2i4,1p17e11.3)') T,nitf,m,tcol(m),nht,
     &  gfree,gion,graie,gcompt,gcomptx,gcompts,pfree,precf,precsec+
     &  pdiel,p_graip,gcompt-g_pcompt,pertefluo,praieint+pech,benef
                endif
c
                          if(kitt.eq.0) goto 700
c
c   FIN DE L'ITERATION sur les temperatures
                if(kimpf.eq.1.and.ifich.ge.6.and.ifich.le.8) then
        benef=g_pcompt-p_grait-p_gbound-p_gfree                      !.pge
        WRITE(31,'(2i4,1pe13.5,1p17e11.3)') nitf,m,tz(m),tcol(m),T,nht,
     &    gfree,gion,graie,gcompt,gcomptx,gcompts,pfree,precf,precsec+
     &    pdiel,p_graip,gcompt-g_pcompt,pertefluo,praieint+pech,benef
                endif
           if(idens.ge.2.or.(igrid.ge.10.and.igrid.ne.13)) 
     &  rtauthx=rtauthx + nht*sel*dhaut *0.6652d-24
        ttpraie=ttpraie + praiperm*dhaut*hnne
        ttgraie=ttgraie + graie*dhaut*hnne
        ttpgrai=ttpgrai + p_graip*dhaut*hnne
        ttpgray=ttpgray + (p_graiy-graiy)*dhaut*hnne
        ttpertefluo=ttpertefluo + pertefluo*dhaut*hnne
        ttpraieint=ttpraieint + praieint*dhaut*hnne
        ttpech=ttpech + pech*dhaut*hnne
        ttpfree=ttpfree + pfree*dhaut*hnne
        ttprecf=ttprecf + precf*dhaut*hnne
        ttprsecdi=ttprsecdi + (precsec+pdiel)*dhaut*hnne
        ttgfree=ttgfree + gfree*dhaut*hnne
        ttgion=ttgion + gion*dhaut*hnne                                     !st
        ttgcomptx=ttgcomptx + gcomptx*dhaut*hnne
        ttgpcompt=ttgpcompt + g_pcompt*dhaut*hnne
                if(imp.lt.2.or.kimpm.eq.0.or.kitn.eq.0) goto 701
        if(kimpf2.eq.1.and.ila.eq.0.and.ifich.ge.3.and.ifich.le.8)then
        WRITE(16,781) m,nht,tcol(m),tz(m),T,ne                         !.tx2
        WRITE(16,782) gion,precf,precsec,pdiel,gfree,pfree
        WRITE(16,783) graie,p_graip,-guta,pertefluo,pf6p4kev,praieint
        WRITE(16,784) pech,gcompt,gcompt-g_pcompt,gcomptx,Tcompt1
        hnut1=sqrt((tz(m))*hnne*zfree*3./(T*VT) *3.0261d-31)  !b0h3/e'2
        WRITE(16,785) hnut1,dtaucmax,hnumaxto
        WRITE(16,786) rtaucef(1),rtaucef(24),rtaucef(97),rtaucef(102)
        WRITE(16,787) rdely,sdely,
     &                clya,dabsh,dabshep,elya,demh*ne,demhep*ne
        WRITE(16,788) tphot(1),esionh
        WRITE(16,789) 
     &  p_graih,graih,p_graihe,graihe,p_graio7,graio7,p_graio8,graio8
                endif
                if(ifich.ge.4.and.ifich.le.8.and.kimpf2.eq.1) then
        WRITE(22,781) m,nht,tcol(m),tz(m),T,ne                         !.txa
        WRITE(22,782) gion,precf,precsec,pdiel,gfree,pfree
        WRITE(22,783) graie,p_graip,-guta,pertefluo,pf6p4kev,praieint
        WRITE(22,784) pech,gcompt,gcompt-g_pcompt,gcomptx,Tcompt1           !st
        WRITE(22,786) rtaucef(1),rtaucef(24),rtaucef(97),rtaucef(102)
        WRITE(22,788) tphot(1),esionh
                endif   ! ifich=4
 781   format(' m=',i4,' Nht=',1pe10.3,' col=',1pe10.3,' z=',1pe10.3,
     &   ' T=',1pe10.3,' Ne=',1pe10.3)
 782   format(' GAINS erg/s/cm3 /(H*Ne*Nh)',7x,'PERTES erg/s/cm3 /',
     &   '(H*Ne*Nh)'/' dus aux ionisations=',1pe10.3,3x,
     &   'recombinaisons sur le fondamental=',1pe10.3/
     &   34x,'recombin. sur les niveaux excites=',1pe10.3/
     &   34x,'recombinaisons dielectroniques   =',1pe10.3/
     &   ' dus au free-free=   ',1pe10.3,'   free-free=',24x,1pe10.3)
 783   format(' ionis.par les raies=',1pe10.3,'   raies permises',
     &   ' (net)=',13x,1pe10.3/36x,'(dont raies UTA=',1pe10.3,')'/
     &   34x,'raies de fluorescence du Fer=',5x,1pe10.3/
     &   36x,'(dont a 6.4kev (j<19)=',1pe10.3,')'/
     &   34x,'raies interdites=',17x,1pe10.3)
 784   format(34x,'dues aux echanges de charges=',5x,1pe10.3/1x,
     &   'gains Compton =',5x,1pe10.3,3x,'pertes Compton=',19x,1pe10.3/
     &   '   (dont X>26kev=',1pe10.3,')',10x,
     &   '(Tcompton environ',1pe10.3,')')
  785   format(/' tau free-free=1 a',1pg10.3,'ev'/' le dtauc maximum',
     &   ' au dessus de 0.5ev est de',1pg10.3,' a',1pg10.3,'eV')
  786   format(' rtaueff a 13.6ev=',1pg10.3,
     1           ' a O8=',1pg10.3,' Fe21=',1pg10.3,' Fe26=',1pg10.3)
  787   format(' participation a la discontinuite Lyman : dreflechi=',
     &     1pe10.3,' demis=',1pe10.3/'kap av',1pe9.2,' H',1pe9.2,           !st
     &     ' He+',1pe9.2,' eps av',1pe9.2,' H',1pe9.2,' He+',1pe9.2)
  788   format(' ionisation de H1s par photons',1pe10.3,
     &                ' par electrons secondaires',1pe10.3)
  789   format(' pertes-net,gains pour les raies H ',2(1pe11.3)/
     &    ' He2 ',2(1pe11.3),' O7 ',2(1pe11.3),' O8 ',2(1pe11.3))
                if(kimpf2.eq.1.and.ifich.ge.3.and.ifich.le.8) then
!LC summary as in ION code
        gtotc= gion + gfree + graie + gcompt
        pcompt=gcompt-g_pcompt
        ptotc=precf +precsec+pdiel +pfree +pech +p_graip+graie +pcompt 
! corrige en sept 2014 - manquait pcompt dans ptot
        benefc= ptotc - gtotc
        WRITE(16,978) m,nitf,tcol(m),nht,T
        if(m.eq.1) WRITE(16,979)
        WRITE(16,981) gtotc,ptotc,benefc,
     &       gion/gtotc,(precf+precsec+pdiel+pech)/ptotc,p_gbound/ptotc,
     &       precf/ptotc,precsec/ptotc,pdiel/ptotc,pech/ptotc,
     &       graie/gtotc,(p_graip+pertefluo+praieint)/ptotc,
     &       p_grait/ptotc,p_graip/ptotc,pertefluo/ptotc,praieint/ptotc,
     &       gfree/gtotc,pfree/ptotc,p_gfree/ptotc,
     &       gcompt/gtotc,pcompt/ptotc,-g_pcompt/ptotc,
     &       gcomptx/gtotc,gcompts/gtotc,
     &       (precf+precsec+pdiel+pfree+pech+p_graip+pcompt)/ptotc,
     &       benefc/ptotc,praiperm,praiperm/ptotc
 978    format(/' HEATING AND COOLING PROCESSES for m=',i4,' ite',
     &  'ration no',i4/3x,'col=',1pe10.3,' nht=',1pe10.3,' T=',1pe10.3)
 979    format('---'/'Results are in 3 columns: HEAT, COOL, COOL-HEAT'/ !2345678
     & 'All rates are volumic power/(NeNh), in units (erg/s/cm^3)/cm^-6'
     & /'BF= Bound-Free'/'FB= Free-Bound = Recombination,',
     & ' Rec.f.= Recombination on the fundamental level'/
     & ' Rec.s.= Recombination on excited levels,',
     & ' Ch.Exch.= Charge exchanges processes'/
     & 'LINE= processes involving spectral lines'/
     & ' ion.=photoionization by line photons,',
     & 'Perm.net.= net cooling by permitted lines'/
     & ' Forbid.= forbidden lines, Fluo.Fe.= fluorescence Fe lines'/
     & 'FF= Free-Free'/'COMPT= Compton processes'/' X.>26keV= hard Co',
     & 'mpton photons > 26keV,  X.<26keV= soft photons < 26 keV'/'---')
 981    format(1p,'total values/(NeNh) :'/'HEATING',8x,e10.3,3x,
     &  'COOLING',7x,e10.3,3x,'COOL-HEAT',4x,e10.3/'ratios :'/
     &  ' HEATING/total HEATING       COOLING/total COOLING     ',
     &  'COOL-HEAT/total COOLING'/'BF',13x,e10.3,3x,'FB total',
     &  6x,e10.3,3x,'FB-BF',8x,e10.3/28x,'FB Rec.f.',5x,e10.3/
     &  28x,'FB Rec.s.',5x,e10.3/28x,'FB Rec.diel.',2x,e10.3/
     &  28x,'FB Ch.Exch.',3x,e10.3/
     &  'LINE ionis.',4x,e10.3,3x,'LINE total',4x,e10.3,3x,
     &  'LINE',9x,e10.3/28x,'LINE Perm.net ',e10.3/
     &  28x,'LINE Fluo.Fe. ',e10.3/28x,'LINE Forbid.  ',e10.3/
     &  'FF',13x,e10.3,3x,'FF',12x,e10.3,3x,'FF',11x,e10.3/
     &  'COMPT total',4x,e10.3,3x,'COMPT',9x,e10.3,3x,'COMPT',8x,
     &  e10.3/'COMPT X.>26keV ',e10.3/'COMPT S.<26keV ',e10.3/
     &  'TOTAL/totHEAT   1.000E+00   TOTAL/totCOOL ',e10.3,3x,
     &  'TOTAL/totCOOL',e10.3//'additional information :'/
     &   'COOL LINE Spontaneous emission/(NeNh):',e10.3,
     &  '  /total COOLING:',e10.3/'***')
                 endif
!LC
  701   continue   ! imp & kimp & kitn
c
c calcul de la pression de radiation
                       if(nitf.gt.1.and.kitn.eq.0) goto 700
          pradx=0.d0
          if(acompt+ccompt+ecompt.gt.0.d0) pradx=
     &     (acompt*expa(-bcompt*rtauthx)+ccompt*expa(-dcompt*rtauthx)
     &     +ecompt*expa(-fcompt*rtauthx))/(acompt+ccompt+ecompt)
     &     *flxdurincid*2.22376d-11                           ! 2/3c
          if(nitf.eq.1) then
             pradc(m)=sjdnuev*33784.6d0                      ! 4pi/3c *e/h
             pradrai(m)=sjdnuevrai*33784.6d0   !calcule pour kali=1 ou nitf=1
             prad(m)=pradc(m) + pradx + pradrai(m)
          endif
           pgaz=T*1.38062d-16*(ne+nht*sab)
           pturb=nht*cmas*vturbkm**2
              if(m.eq.1.and.ifich.ne.0.and.ifich.le.8.and.iexpert.eq.1)
     &     WRITE(15,794) pgaz,pradc(m)+pradx,pradrai(m),prad(m)
  794      format('a z=0 pgaz,prad-continu,-raie,-tot=',1p4e11.3)
c
c        if(nitf.eq.1) OPEN(UNIT=35,FILE=fipre,STATUS='new')
        if(kitn.eq.1.and.kimpf.eq.1.and.idens.ge.8
     &              .and.ifich.ne.0.and.ifich.le.8) then
           if(m.eq.1) WRITE(35,795) nitf
  795      format('# it ',i4,'  z        col         nht        ',
     &        ' T        pgas       prad    prad-raies    pturb')        !st
           WRITE(35,791) m,tz(m),tcol(m),nht,T,pgaz,prad(m),
     &                   pradrai(m),pturb
        endif   ! nitf=k*nimpf
  791   format(' ',i4,8(' ',1pe10.3))
c
           pradrai1(m)=sjdnuevrai*33784.6d0   !calcule pour kali=1 ou nitf=1
           if(nitf.eq.1) pradrai1(m)=0.d0

        if(m.eq.npt.and.kitn.eq.1.and.kimpf.eq.1) then
              if(ila.eq.0) then
           write(6,793) dtaurmax0,krtormax0,dtaurmaxH,krtormaxH
           if(nitf.eq.1) write(6,*)
           if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &     write(15,793) dtaurmax0,krtormax0,dtaurmaxH,krtormaxH
              else
           if(iexpert.eq.1)
     &     write(6,993) dtaurmax0,krtormax0,dtaurmaxH,krtormaxH
           if(iexpert.eq.1.and.nitf.eq.1) write(6,*)
           if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &     write(15,993) dtaurmax0,krtormax0,dtaurmaxH,krtormaxH
              endif
 793       format('    premier strate: le dtau_raie_au_centre maximum',!2345678
     &     ' est',1pe10.3,' pour kr=',i4/'    dernier strate: le dtau',
     &     '_raie_au_centre maximum est',1pe10.3,' pour kr=',i4)
 993       format('     first layer: maximum dtau_line_at_center ',
     &     'is',1pe10.3,' for kr=',i4/'     last layer: maximum ',
     &     'dtau_line_at_center is',1pe10.3,' for kr=',i4)
c
           if(dtaurmax0.ge.1.d-2.or.dtaurmaxH.ge.1.d-2) then
              if(iexpert.eq.1.or.igrid.eq.10.or.igrid.eq.0) then
                 write(6,*)
     &'WARNING: the thickness of the first or the last layer is too big'
                 write(6,*)
     &           '        this maximum should have to be < 10-3'
c              else if(iexpert.eq.0) then ??
              endif
           endif   ! dtaurmax trop grand
        endif      ! m=npt
  700   continue   ! kitt                                                   !st
c
        return
        end                                                            !sstrate
c--============================================================================
       subroutine GRILLE

c decoupage en strates au premier tour - column grid or z-grid

        implicit double precision (a-h,o-z)
       parameter (nzs=999)
         double precision ne,nht
         dimension tdcolf(nzs/2+2)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
cgrille
        nstrmax=nzs
        if(diviseur.eq.0.d0) diviseur=1.d0
c         q1=10.d0**(1.d0/dfloat(npd1))
         npt=nstrmax
                        goto(90,100,200,100) igrid+1
c++++++++++++++
 90   continue     ! igrid=0 ndcm strates egaux a CONDITION que coltot < 1e22
             if(coltot.gt.1.d19) then
                if(ila.eq.0) then
      write(6,*) 'COLONNE-DENSITE TROP GRANDE pour cette option igrid=0'
      if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1) write(15,*)
     &           'COLONNE-DENSITE TROP GRANDE pour cette option igrid=0'
                else
      write(6,*) 'COLUMN-DENSITY TOO BIG for this option igrid=0'
      if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1) write(15,*)
     &           'COLUMN-DENSITY TOO BIG for this option igrid=0'
                endif
      stop
             endif
       npt=ndcm
       dcol=coltot/npt
       tcol(1)=dcol
       do 20009 m=2,npt
          tcol(m)=tcol(m-1) + dcol
20009  continue
       tcol(npt+1)=tcol(npt)
c
       if(idens.le.1) then
          tz(1)=0.d0
          tnht(1)=densinit
          do m=2,npt
             if(idens.eq.0) then
                tnht(m)=densinit
             else if(dendex.eq.1.d0) then
                tnht(m)=densinit*expa(-tcol(m)/densinit/raymin)
             else
                cd=densinit*raymin/(dendex-1.d0)
             tnht(m)=densinit*(1.d0-tcol(m)/cd)**(dendex/(dendex-1.d0))
             endif
             tz(m)=tz(m-1) + (tcol(m) - tcol(m-1))/tnht(m-1)
          enddo
          tz(npt+1)=tz(npt)
       endif   ! idens=0-1
       goto 20050
c++++++++++++++
 100   continue                                       ! igrid=1
             do m=1,nstrmax
                tcol(m)=0.d0
             enddo   ! m
       dclim=coltot/dfloat(ndcm)
c       write(6,*) dcolf,dclim,dclim/diviseur
! fin du nuage - end of the cloud
       qf=10.d0**(1.d0/dfloat(npdf))
          npt2=nstrmax
          dcmax=dclim
          if(diviseur.gt.1.d0.and.abs(finchute-coltot).lt.0.02d0*coltot)
     &       dcmax=dclim/diviseur
          dcol=dcolf
          tdcolf(1)=dcol
          colfin=coltot
       do 10020 m=2,nstrmax
          dcol=dcol*qf
          colfin=colfin - dcol
             if(dcol.gt.dcmax) then
                npt2=m-1
                goto 10021
                endif   ! dcol > dclim
          tdcolf(m)=dcol
c          write(10,'(i4,1pe20.10,1p2e14.5)') m,colfin,dcol
10020  continue   ! m
10021  continue
c       write(6,*) 'npt2=',npt2
c
       q1=10.d0**(1.d0/dfloat(npd1))
       q2=10.d0**(1.d0/20.d0)
          npt3=nstrmax
          dcol=dcol1
          tcol(1)=0.d0
          tcol(2)=dcol
          dcmax=dclim
       do 10022 m=3,nstrmax
          if(diviseur.eq.1.d0) then
             if(tcol(m-1).lt.colfin) then
                dcmax=dclim
                dcol=min(dcol*q1,dcmax)
                tcol(m)=tcol(m-1) + dcol
             else
                npt3=m-1
                goto 10023
             endif
c
          else   ! diviseur different de 0 ou 1
! debut du nuage - cloud begining
             if(tcol(m-1).lt.debchute) then
                dcmax=dclim
                dcol=min(dcol*q1,dcmax)
                tcol(m)=tcol(m-1) + dcol
                ca=tcol(m)
! partie divisee - divided part
            else if(tcol(m-1).ge.debchute.and.tcol(m-1).lt.finchute)then!2345678
                dcmax=dclim/diviseur
                dcol=max(dcol/q2,dcmax)
                tcol(m)=tcol(m-1) + dcol
                if(dcol.gt.dcmax) cb=tcol(m)
                cc=tcol(m)
c milieu du nuage - cloud middle
             else if(tcol(m-1).ge.finchute.and.tcol(m-1).lt.colfin)then
                dcmax=dclim
                dcol=min(dcol*q2,dcmax)
                tcol(m)=tcol(m-1) + dcol
             else
                npt3=m-1
                goto 10023
             endif
          endif   ! diviseur

10022  continue
10023  continue
       npt=npt2 + npt3
       do m=npt3+1,npt
          tcol(m)=tcol(m-1) + tdcolf(npt-m+1)
       enddo
       tcol(npt+1)=tcol(npt)
          if(npt.gt.nzs) then
                write(6,*)'NOMBRE DE STRATES TROP GRAND !  npt=',npt
                write(6,*)'NUMBER OF LAYERS TOO BIG !  npt=',npt
                if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &          write(15,*)' NOMBRE DE STRATES TROP GRAND !  npt=',npt
                stop
                endif
c
       if(idens.le.1) then
          tz(1)=0.d0
          tnht(1)=densinit
          do m=2,npt
             if(idens.eq.0) then
                tnht(m)=densinit
             else if(dendex.eq.1.d0) then
                tnht(m)=densinit*expa(-tcol(m)/densinit/raymin)
             else
                cd=densinit*raymin/(dendex-1.d0)
             tnht(m)=densinit*(1.d0-tcol(m)/cd)**(dendex/(dendex-1.d0))
             endif
             tz(m)=tz(m-1) + (tcol(m) - tcol(m-1))/tnht(m-1)
          enddo
          tz(npt+1)=tz(npt)
       endif   ! idens=0-1
       goto 20050
c++++++++++++++
c decoupage en z - z-grid
c dans ce cas les coltot,ndcm,npd1,dcol1,diviseur,debchute,finchute,npdf,dcolf
c sont en realite des ztot,dz1,...
 200   continue                                       ! igrid=2
             do m=1,nstrmax
                tz(m)=0.d0
             enddo   ! m
       dclim=coltot/dfloat(ndcm)
c       write(6,*) dcolf,dclim,dclim/diviseur
! fin du nuage - end of the cloud
       qf=10.d0**(1.d0/dfloat(npdf))
          npt2=nstrmax
          dcmax=dclim
          dcol=dcolf
          tdcolf(1)=dcol
          colfin=coltot
       do 20020 m=2,nstrmax
          dcol=dcol*qf
          colfin=colfin - dcol
             if(dcol.gt.dclim) then
                npt2=m-1
                goto 20021
                endif   ! dcol > dclim
          tdcolf(m)=dcol
c          write(10,'(i4,1pe20.10,1p2e14.5)') m,colfin,dcol
20020  continue   ! m
20021  continue
c       write(6,*) 'npt2=',npt2
c
       q1=10.d0**(1.d0/dfloat(npd1))
       q2=10.d0**(1.d0/20.d0)
          npt3=nstrmax
          dcol=dcol1
          tz(1)=0.d0
          tz(2)=dcol
          dcmax=dclim
       do 20022 m=3,nstrmax
          if(diviseur.eq.1.d0) then
             if(tz(m-1).lt.colfin) then
                dcmax=dclim
                dcol=min(dcol*q1,dcmax)
                tz(m)=tz(m-1) + dcol
             else
                npt3=m-1
                goto 20023
             endif
c
          else   ! diviseur different de 0 ou 1
! debut du nuage - cloud begining
             if(tz(m-1).lt.debchute) then
                dcmax=dclim
                dcol=min(dcol*q1,dcmax)
                tz(m)=tz(m-1) + dcol
                ca=tz(m)
! partie divisee - divided part
            else if(tz(m-1).ge.debchute.and.tz(m-1).lt.finchute)then!2345678
                dcmax=dclim/diviseur
                dcol=max(dcol/q2,dcmax)
                tz(m)=tz(m-1) + dcol
                if(dcol.gt.dcmax) cb=tz(m)
                cc=tz(m)
c milieu du nuage - cloud middle
             else if(tz(m-1).ge.finchute.and.tz(m-1).lt.colfin)then
                dcmax=dclim
                dcol=min(dcol*q2,dcmax)
                tz(m)=tz(m-1) + dcol
             else
                npt3=m-1
                goto 20023
             endif
          endif   ! diviseur

20022  continue
20023  continue
       npt=npt2 + npt3
       do m=npt3+1,npt
          tz(m)=tz(m-1) + tdcolf(npt-m+1)
       enddo
       tz(npt+1)=tz(npt)
          if(npt.gt.nzs) then
                write(6,*)'NOMBRE DE STRATES TROP GRAND !  npt=',npt
                write(6,*)'NUMBER OF LAYERS TOO BIG !  npt=',npt
                if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &          write(15,*)' NOMBRE DE STRATES TROP GRAND !  npt=',npt
                stop
                endif
c
          tcol(1)=0.d0
          tnht(1)=densinit
       if(idens.le.1) then
          do m=2,npt
             if(idens.eq.0) then
                tnht(m)=densinit
             else if(dendex.eq.1.d0) then
                tnht(m)=densinit*raymin/(raymin+tz(m))
             else
                tnht(m)=densinit* (raymin/(raymin+tz(m)))**dendex
             endif
             tcol(m)=tcol(m-1) + (tz(m) - tz(m-1))*tnht(m-1)
          enddo
          tcol(npt+1)=tcol(npt)
          coltot=tcol(npt)
       endif   ! idens=0-1
c       goto 20050
c++++++++++++++
20050   continue
       htot=tz(npt)
c
        return
        end                                                             !grille
c------------------------------------------------------------------------------
       subroutine LECGRILLE

c lecture du decoupage (event. des densites et des temperatures) si igrid >=10
! read col-grid if igrid >=10

        implicit double precision (a-h,o-z)
       parameter (nzs=999)
         double precision ne,nht
         character ecrit2*12
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot

        if(igrid.eq.10) then
c           OPEN(UNIT=13,FILE=fimzn,STATUS='OLD')
                READ(13,*)                               ! titre
                WRITE(10,*)
                WRITE(10,*) '        col                 m        dcol'
           tz(1)=0.d0
           tnht(1)=densinit
           READ(13,*) tcol(1)
           do m=2,nstr
              READ(13,*) tcol(m)
       WRITE(10,'(1pe23.15,i10,1pe15.6)')tcol(m-1),m-1,tcol(m)-tcol(m-1)
              if(tcol(m).le.tcol(m-1)) then
       if(ila.eq.0) write(6,*)' PB lecture des colonnes-densites a m=',m
       if(ila.eq.0.and.ifich.ne.0.and.ifich.le.8.and.iexpert.eq.1)
     & write(15,*)' PB lecture des colonnes-densites a m=',m
       if(ila.eq.1) write(6,*)' PB reading column-densities at m=',m
       if(ila.eq.1.and.ifich.ne.0.and.ifich.le.8.and.iexpert.eq.1)
     & write(15,*)' PB reading column-densities at m=',m
                   stop
              endif
           enddo   ! mm
           npt=nstr
              tcol(npt+1)=tcol(npt)
              coltot=tcol(npt)
           WRITE(10,'(1pe23.15,i10,1pe15.6)')
     &                tcol(npt),npt,tcol(npt+1)-tcol(npt)
           if(idens.le.1) then
              do m=2,npt
                 tz(m)=tz(m-1) + (tcol(m) - tcol(m-1))/tnht(m-1)
                 if(idens.eq.0) then
                    tnht(m)=densinit
                 else
                    tnht(m)=densinit*(raymin/(raymin+tz(m)))**dendex
                 endif
              enddo
              tz(npt+1)=tz(npt)
              htot=tz(npt)
           endif
        endif   ! igrid=10
c
        if(igrid.eq.11) then
c             OPEN(UNIT=13,FILE=fimzn,STATUS='OLD')
             READ(13,*)                                 ! titre - title
                WRITE(10,*)
      WRITE(10,*)'        col             nht             m        dcol'!234567
             tz(1)=0.d0
                READ(13,*) tcol(1),tnht(1)
                m=1
c                WRITE(10,'(1pe23.15,1pe14.5,i10)') tcol(1),tnht(1),m
             do m=2,nstr
                READ(13,*) cc,dd
                tcol(m)=cc
                tnht(m)=dd
c                WRITE(10,'(1pe23.15,1pe14.5,i10,1pe15.6)')
c     &                tcol(m),tnht(m),m,tcol(m)-tcol(m-1)
                WRITE(10,'(1pe23.15,1pe14.5,i10,1pe15.6)')
     &                tcol(m-1),tnht(m-1),m-1,tcol(m)-tcol(m-1)
                tz(m)=tz(m-1) + (tcol(m) - tcol(m-1))/tnht(m-1)
             enddo   ! m
             densinit=tnht(1)
             npt=nstr
             tz(npt+1)=tz(npt)
             tcol(npt+1)=tcol(npt)
             htot=tz(npt)
             coltot=tcol(npt)
                WRITE(10,'(1pe23.15,1pe14.5,i10,1pe15.6)')
     &                tcol(npt),tnht(npt),npt,tcol(npt+1)-tcol(npt)
             endif   ! igrid=11
c
        if(igrid.eq.12) then
c             OPEN(UNIT=13,FILE=fimzn,STATUS='OLD')
             READ(13,*)                               ! titre
                WRITE(10,*)
             WRITE(10,*)'     z          nht        m      col     dcol'
             tcol(1)=0.d0
                READ(13,*) tz(1),tnht(1)
                m=1
                WRITE(10,'(1pe20.12,1pe14.5,i10)') tz(1),tnht(1),m
             do m=2,nstr
                READ(13,*) tz(m),tnht(m)
                tcol(m)=tcol(m-1)+tnht(m-1)*(tz(m)-tz(m-1))
                WRITE(10,'(1pe20.12,1pe14.5,i10,1pe19.12,1pe13.5)')
     &                tz(m),tnht(m),m,tcol(m),tcol(m)-tcol(m-1)
             enddo   ! mm
             densinit=tnht(1)
             npt=nstr
             tz(npt+1)=tz(npt)
             tcol(npt+1)=tcol(npt)
             htot=tz(npt)
             coltot=tcol(npt)
             endif   ! igrid=12
c lecture - reading - de la TEMPERATURE (et  densite) si igrid>12
c        if(igrid.ge.13) OPEN(UNIT=27,FILE=fimzt,STATUS='OLD')
        if(igrid.eq.13) then
           READ(27,*)
  277      READ(27,77) ecrit2
   77      format(a12)
           nd=index(ecrit2,'c')
c                         WRITE(10,*) ecrit2,';',nd
           if(nd.eq.0.or.ecrit2(nd:nd+6).ne.'coldens') goto 277
           WRITE(10,*)
           tnht(1)=densinit
           do mm=1,nzs
              READ(27,278) rien,rien2,tt,rien3,m,zz
 278          format(i3,1x,e9.3,1x,e10.3)
              if(m.le.0) goto 279
              tz(m)=zz
              temp(m)=tt
              if(m.ne.mm.and.ila.eq.0) write(6,*) ' PB lecture des z'
              if(m.ne.mm.and.ila.eq.0.and.ifich.ne.0.and.ifich.le.8
     &        .and.iexpert.eq.1) write(15,*)' PB lecture des z'
              if(m.ne.mm.and.ila.eq.1) write(6,*) ' PB reading z'
              if(m.ne.mm.and.ila.eq.1.and.ifich.ne.0.and.ifich.le.8
     &        .and.iexpert.eq.1) write(15,*)' PB reading z'
              WRITE(10,*) m,tz(m),tt
c                 if(m.gt.1) coltot=coltot + densinit*(tz(m)-tz(m-1))
              if(idens.le.1)then
                 if(idens.eq.0) then
                    tnht(m)=densinit
                 else
                    tnht(m)=densinit*(raymin/(raymin+tz(m)))**dendex
                 endif
                 tcol(m)=tcol(m-1) + tnht(m-1)*(tz(m)-tz(m-1))
              endif
           enddo                                                            !m
 279       continue
           npt=nzs
           if(m.lt.nzs) npt=mm-1
           write(6,*) 'npt=',npt
           tcol(npt+1)=tcol(npt)
           coltot=tcol(npt)
           htot=tz(npt)
           CLOSE(27)
        endif    ! igrid=13
c
        if(igrid.eq.14) then
           READ(27,*) 
           npt=nstr
c           write(6,*) 'npt=',npt
           WRITE(10,*)
           tz(1)=0.d0
           do m=1,npt
              READ(27,*) col,dd,tt,rien
c                         tz(m)=zz
              tcol(m)=col
              temp(m)=tt
              tnht(m)=dd
        if(m.gt.1) tz(m)=tz(m-1) + (tcol(m)-tcol(m-1))/tnht(m-1)
        WRITE(10,*) m,tz(m),temp(m),tnht(m),tcol(m)
                        enddo                                                !m
        densinit=tnht(1)
             tz(npt+1)=tz(npt)
             tcol(npt+1)=tcol(npt)
             htot=tz(npt)
             coltot=tcol(npt)
        if(ila.eq.0) write(6,*) ' fichier .MZT : npt=',npt
        if(ila.eq.1) write(6,*) ' file .MZT : npt=',npt
        CLOSE(27)
                endif   ! igrid=14
c
        ndcm=npt
        return
        end                                                          !lecgrille
c tcol(m) tz(m) tnht(m)
c igrid densinit raymin dendex nstr npt coltot htot idens
c------------------------------------------------------------------------------
       subroutine CHAGRILLE1

c CHANGEMENT DE DECOUPAGE - grid change

         implicit double precision (a-h,o-z)
       parameter (nzs=999,nrais=4200)
         double precision ne,nht
         character txt*5
         dimension ptcol(nzs+1),ptnht(nzs),ptz(nzs+1),ptemp(nzs),
     &   pprad(nzs),ptjc(322,nzs),ptjr(nrais,nzs),ptcjn(nrais,nzs)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &             kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2) !234567
       common/sporpal/tjr(nrais,nzs),tcjn(nrais,nzs),pradr(nrais,nzs)
       common/stnua/prad(nzs),pradrai(nzs),pradrai1(nzs),rhep2
c chagrille1
        do m=1,npt
           ptcol(m)=tcol(m)
           ptz(m)=tz(m)
           ptnht(m)=tnht(m)
           ptemp(m)=temp(m)
           pprad(m)=prad(m)
           do kc=1,322
              ptjc(kc,m)=tjc(kc,m)
           enddo   !kc
           do kr=1,nrais
              ptjr(kr,m)=tjr(kr,m)
              ptcjn(kr,m)=tcjn(kr,m)
           enddo   !kr
        enddo   ! m
           ptcol(npt+1)=tcol(npt+1)
           ptz(npt+1)=tz(npt+1)
c
        mm=0
c3       ka=0
        do m=1,npt
           mm=mm+1
           tcol(mm)=ptcol(m)
           tz(mm)=ptz(m)
           tnht(mm)=ptnht(m)
           temp(mm)=ptemp(m)
           prad(mm)=pprad(m)
           do kc=1,322
              tjc(kc,mm)=ptjc(kc,m)
           enddo   !kc
           do kr=1,nrais
              tjr(kr,mm)=ptjr(kr,m)
              tcjn(kr,mm)=ptcjn(kr,m)
           enddo   !kr

           rapt=0.d0
           ro7=0.d0
           ro7s=0.d0
           rc5=0.d0
c           rc5s=0.d0
           rne8=0.d0
           rne8s=0.d0
           ro3=0.d0
           ro3s=0.d0
           rc3=0.d0
           rhe=0.d0
           ro2=0.d0
           ro2s=0.d0
           rc2=0.d0
c           rs14=0.d0
           if(temp(m+1).gt.0.d0) rapt=temp(m)/temp(m+1)
c2           if(temp(m+2).gt.0.d0) rapt=temp(m+1)/temp(m+2)
           if(trij(m+1,27).ge.trij(m,27).and.trij(m,27).gt.1.d-4)
     &        ro7=trij(m+1,27)/trij(m,27)
           if(trij(m+1,27).lt.trij(m,27).and.trij(m+1,27).gt.1.d-4)
     &        ro7=trij(m,27)/trij(m+1,27)
           if(trij(m+2,27).ge.trij(m+1,27).and.trij(m+1,27).gt.1.d-4)
     &        ro7s=trij(m+2,27)/trij(m+1,27)
           if(trij(m+2,27).lt.trij(m+1,27).and.trij(m+2,27).gt.1.d-4)
     &        ro7s=trij(m+1,27)/trij(m+2,27)
           if(trij(m+1,10).ge.trij(m,10).and.trij(m,10).gt.1.d-4)
     &        rc5=trij(m+1,10)/trij(m,10)
           if(trij(m+1,10).lt.trij(m,10).and.trij(m+1,10).gt.1.d-4)
     &        rc5=trij(m,10)/trij(m+1,10)
c           if(trij(m+2,10).ge.trij(m+1,10).and.trij(m+1,10).gt.1.d-4)
c     &        rc5s=trij(m+2,10)/trij(m+1,10)
c           if(trij(m+2,10).lt.trij(m+1,10).and.trij(m+2,10).gt.1.d-4)
c     &        rc5s=trij(m+1,10)/trij(m+2,10)
c           if(trij(m+3,10).ge.trij(m+2,10).and.trij(m+2,10).gt.1.d-4)
c     &        rc5b=trij(m+3,10)/trij(m+2,10)
c           if(trij(m+3,10).lt.trij(m+2,10).and.trij(m+3,10).gt.1.d-4)
c     &        rc5b=trij(m+2,10)/trij(m+3,10)
           if(trij(m+1,37).ge.trij(m,37).and.trij(m,37).gt.1.d-4)
     &        rne8=trij(m+1,37)/trij(m,37)
           if(trij(m+1,37).lt.trij(m,37).and.trij(m+1,37).gt.1.d-4)
     &        rne8=trij(m,37)/trij(m+1,37)
           if(trij(m+2,37).lt.trij(m+1,37).and.trij(m+2,37).gt.1.d-4)
     &        rne8s=trij(m+1,37)/trij(m+2,37)
           if(trij(m+1,23).ge.trij(m,23).and.trij(m,23).gt.1.d-4)
     &        ro3=trij(m+1,23)/trij(m,23)
           if(trij(m+2,23).ge.trij(m+1,23).and.trij(m+1,23).gt.1.d-4)
     &        ro3s=trij(m+2,23)/trij(m+1,23)
           if(trij(m+1,8).ge.trij(m,8).and.trij(m,8).gt.1.d-4)
     &        rc3=trij(m+1,8)/trij(m,8)
           if(trij(m+1,4).ge.trij(m,4).and.trij(m,4).gt.1.d-6)
     &        rhe=trij(m+1,4)/trij(m,4)
           if(trij(m+1,4).lt.trij(m,4).and.trij(m+1,4).gt.1.d-6)
     &        rhe=trij(m,4)/trij(m+1,4)
           if(trij(m+1,22).ge.trij(m,22).and.trij(m,22).gt.1.d-4)
     &        ro2=trij(m+1,22)/trij(m,22)
           if(trij(m+2,22).ge.trij(m+1,22).and.trij(m+1,22).gt.1.d-4)
     &        ro2s=trij(m+2,22)/trij(m+1,22)
           if(trij(m+1,7).ge.trij(m,7).and.trij(m,7).gt.1.d-4)
     &        rc2=trij(m+1,7)/trij(m,7)
c           if(trij(m+1,82).ge.trij(m,82).and.trij(m,82).gt.1.d-4)
c     &        rs14=trij(m+1,82)/trij(m,82)
c           if(trij(m+1,82).lt.trij(m,82).and.trij(m+1,82).gt.1.d-4)
c     &        rs14=trij(m,82)/trij(m+1,82)
c2           if(m.lt.npt-20.and.
c2     &       (rapt.ge.1.25d0.or.ro7.ge.2.d0.or.ka.eq.1)) then
c             if(m.ge.10.and.m.lt.npt-20.and.(mbos(m-2).ge.1.or.
c     &       mbos(m-1).ge.1.or.mbos(m+1).ge.1.or.mbos(m+2).ge.1
c     &       .or.mbos(m).ge.1.or.rapt.ge.1.25d0.or.ro7.ge.2.d0)) then
c     &       .or.rc5.ge.3.d0.or.rc5s.ge.3.d0.or.rc5b.ge.3.d0)) then
c     &       .or.rs14.ge.3.d0)) then

c3 .or.ka.eq.1  si on veut ajouter 1 strate apres
           if(kitf.ne.1.and.m.ge.10.and.tcol(m).lt.coltot*0.99d0.and.
     &       (mbos(m-1).ge.1.or.mbos(m).ge.1.or.mbos(m+1).ge.1
     &       .or.rapt.ge.1.25d0.or.rapt.le.0.9d0
     &       .or.ro7.ge.2.d0.or.ro7s.ge.2.d0.or.rhe.ge.2.d0
     &       .or.rc5.ge.3.d0.or.rne8.ge.3.d0.or.rne8s.ge.3.d0
     &       .or.ro3.ge.3.d0.or.ro3s.ge.3.d0.or.rc3.ge.3.d0
     &       .or.ro2.ge.3.d0.or.ro2s.ge.3.d0.or.rc2.ge.3.d0)) then
           if(mm+npt-m.ge.nzs-2) then
       if(ila.eq.0)write(6,*)' pas possible d''ajouter un strate a m=',m
           if(ila.eq.1) write(6,*)' not possible to add a layer at m=',m
           if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1) then
      if(ila.eq.0)write(15,*)' pas possible d''ajouter un strate a m=',m
           if(ila.eq.1)write(15,*)' not possible to add a layer at m=',m
           endif
           else if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1) then
c il n'y a pas encore trop de strates
              if(mbos(m-1).ge.1.or.mbos(m).ge.1.or.mbos(m+1).ge.1) then
                 if(ila.eq.0) txt='bosse'
                 if(ila.eq.1) txt='bump '
                 if(ila.eq.0) write(15,882) m,tcol(m),rapt,txt
                 if(ila.eq.1) write(15,982) m,tcol(m),rapt,txt
              else if(rapt.ge.1.25d0.or.rapt.le.0.9d0
     &           .or.ro7.ge.2.d0.or.rc5.ge.3.d0
     &           .or.rne8.ge.3.d0.or.ro3.ge.3.d0.or.rc3.ge.3.d0
     &           .or.rhe.ge.2.d0.or.ro2.ge.3.d0.or.rc2.ge.3.d0) then
                 if(ila.eq.0) then
                       if(rhe.ge.2.d0) write(15,883)m,tcol(m),rapt,rhe
       if(rc5.ge.3.d0.or.rne8.ge.3.d0)write(15,886)m,tcol(m),rc5,rne8
       if(ro7.ge.2.d0.or.rapt.ge.1.25d0)write(15,887)m,tcol(m),rapt,ro7
       if(rc2.ge.3.d0.or.rc3.ge.3.d0) write(15,888)m,tcol(m),rc2,rc3
       if(ro2.ge.3.d0.or.ro3.ge.3.d0) write(15,884)m,tcol(m),ro2,ro3
                 else
                       if(rhe.ge.2.d0) write(15,983)m,tcol(m),rapt,rhe
       if(rc5.ge.3.d0.or.rne8.ge.3.d0)write(15,986)m,tcol(m),rc5,rne8
       if(ro7.ge.2.d0.or.rapt.ge.1.25d0)write(15,987)m,tcol(m),rapt,ro7
       if(rc2.ge.3.d0.or.rc3.ge.3.d0) write(15,988)m,tcol(m),rc2,rc3
       if(ro2.ge.3.d0.or.ro3.ge.3.d0) write(15,994)m,tcol(m),ro2,ro3
                 endif
              else
                 txt='     '
                 if(ila.eq.0) write(15,882) m,tcol(m),rapt,txt
                 if(ila.eq.1) write(15,982) m,tcol(m),rapt,txt

 882             format(' ajout d''un strate a m=',i3,' col=',1pe8.2,
     &                  ': rapport m/m+1 T=',0pf7.3,2x,a5)
 883             format(' ajout d''un strate a m=',i3,' col=',1pe8.2,
     &                  ': rapport m/m+1 T, He+ =',0pf7.3,0pf7.3)
 886             format(' ajout d''un strate a m=',i3,' col=',1pe8.2,
     &                  ': rapport m+1/m CV,NeVIII=',0pf7.3,0pf7.3)
 887             format(' ajout d''un strate a m=',i3,' col=',1pe8.2,
     &                  ': rapport m/m+1 T, OVII=',0pf7.3,0pf7.3)
 888             format(' ajout d''un strate a m=',i3,' col=',1pe8.2,
     &                  ': rapport m+1/m CII, CIII=',0pf7.3,0pf7.3)
 884             format(' ajout d''un strate a m=',i3,' col=',1pe8.2,
     &                  ': rapport m+1/m OII, OIII=',0pf7.3,0pf7.3)
 982             format(' layer added at m=',i4,' col=',1pe8.2,
     &                  ': ratio m/m+1 T=',0pf7.3,2x,a5)
 983             format(' layer added at m=',i4,' col=',1pe8.2,
     &                  ': ratio m/m+1 T, He+ =',0pf7.3,0pf7.3)
 986             format(' layer added at m=',i4,' col=',1pe8.2,
     &                  ': ratio m+1/m CV,NeVIII=',0pf7.3,0pf7.3)
 987             format(' layer added at m=',i4,' col=',1pe8.2,
     &                  ': ratio m/m+1 T, OVII=',0pf7.3,0pf7.3)
 988             format(' layer added at m=',i4,' col=',1pe8.2,
     &                  ': ratio m+1/m CII, CIII=',0pf7.3,0pf7.3)
 994             format(' layer added at m=',i4,' col=',1pe8.2,
     &                  ': ratio m+1/m OII, OIII=',0pf7.3,0pf7.3)
              endif   ! mbos et autres pbs/1
c
              mm=mm+1
              tcol(mm)=(ptcol(m)+ptcol(m+1))/2.d0
              tnht(mm)=(ptnht(m)+ptnht(m+1))/2.d0
              tz(mm)=  (ptz(m) + ptz(m+1))/2.d0
              temp(mm)=(ptemp(m)+ptemp(m+1))/2.d0
              prad(mm)=(pprad(m)+pprad(m+1))/2.d0
              do kc=1,322
                 tjc(kc,mm)=(ptjc(kc,m)+ptjc(kc,m+1))/2.d0
              enddo   ! kc
              do kr=1,nrais
                 tjr(kr,mm)=(ptjr(kr,m)+ptjr(kr,m+1))/2.d0
                 tcjn(kr,mm)=(ptcjn(kr,m)+ptcjn(kr,m+1))/2.d0
              enddo   ! kr
           endif   ! mm/nzs
                          endif   ! rapports trop grands
        enddo   ! m
c
                         if(mm.ne.npt) then
        npt=mm
        tcol(npt+1)=tcol(npt)
        tz(npt+1)=tz(npt)
                         endif    ! changement
c
        return
        end                                                         !chagrille1
c ptcol(m) ptz(m) ptnht(m) ptemp(m) pprad(m) ptjr(kr,m) ptcjn(kr,m)
c tcol(m) tz(m) tnht(m) temp(m) prad(m) tjr(kr,m) tcjn(kr,m) trij(m,nij)
c npt ila
c------------------------------------------------------------------------------
       subroutine CHAGRILLE2

c CHANGEMENT DE DECOUPAGE - grid change

         implicit double precision (a-h,o-z)
       parameter (nzs=999,nrais=4200)
         double precision ne,nht
         character txt*5
         dimension ptcol(nzs+1)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &             kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/chag/ptz(nzs+1),ptnht(nzs),ptemp(nzs),pprad(nzs),
     &             ptjc(322,nzs),ptjr(nrais,nzs),ptcjn(nrais,nzs)
c
       if(igrid.eq.1) then
          dq1=10.d0**(1.d0/dfloat(npd1)) - 1.d0
       else
          dq1=0.25d0      ! environ 10 strates/decade
          nstr=npt
       endif
          ff=0.d0
       do m=1,npt
          ptcol(m)=tcol(m)
          mm=m
          CALL sVALEURS(mm,mm,0,ff)
       enddo   ! m
          ptcol(npt+1)=tcol(npt+1)
          ptz(npt+1)=tz(npt+1)
c
          m=1
          mo=1
          tcol(1)=ptcol(1)
          CALL sVALEURS(1,1,1,ff)      ! sVALEURS(mm,m1,ival,ff): y(mm)=py(m1)
c          write(6,'(a,1pe12.5)')'0   1',tcol(1)
       do ii=1,npt
          dcol=ptcol(mo+1)-ptcol(mo)
          rapt=0.d0
          if(ptemp(mo+1).gt.0.d0) rapt=ptemp(mo)/ptemp(mo+1)
          CALL sRAPPORTS(mo,mo+1,ro7,rc5,rne8,ro3,rc3,rhe,ro2,rc2,
     &                   ro7s,ro3s,ro2s)
c                      x(mf)/x(mi)
             dcmax=min(coltot/nstr,ptcol(mo)*dq1)*0.24d0
c
c strates trop serres - layers too much thin
          if(ptcol(mo).lt.coltot*0.9d0.and.dcol.lt.dcmax
     &       .and.rapt.lt.1.03d0.and.ro7.lt.1.5d0.and.rc5.lt.1.5d0
     &       .and.rne8.lt.1.5d0.and.rhe.lt.1.5d0.and.ro3.lt.1.5d0
     &       .and.rc3.lt.1.5d0.and.ro2.lt.1.5d0.and.rc2.lt.1.5d0) then
             do im=2,10           
                dcol=ptcol(mo+im)-ptcol(mo)
                rapt=0.d0
                if(ptemp(mo+im).gt.0.d0) rapt=ptemp(mo)/ptemp(mo+im)
                CALL sRAPPORTS(mo,mo+im,ro7,rc5,rne8,ro3,rc3,rhe,ro2,
     &                   rc2,ro7s,ro3s,ro2s)
                if(dcol.gt.dcmax
     &          .or.rapt.gt.1.03d0.or.ro7.gt.1.5d0.or.rc5.gt.1.5d0
     &          .or.rne8.gt.1.5d0.or.rhe.gt.1.5d0.or.ro3.gt.1.5d0
     &          .or.rc3.gt.1.5d0.or.ro2.gt.1.5d0.or.rc2.gt.1.5d0)goto 10
             enddo
 10          m=m+1
             mo=mo+im
             tcol(m)=ptcol(mo)
c             write(6,'(a,3i5,2f7.3,1p2e13.5)')
c     &               '1',m,mo,im,rapt,ro7,dcol,tcol(m) 
             mm=m
             CALL sVALEURS(mm,mo,1,ff)
c
c strate d'avant - layer before
          else if(m.ge.10.and.tcol(m).lt.coltot*0.95d0.and.
     &            ro7s.ge.2.d0.and.ro7.lt.2.d0.and.
     &            ro3s.ge.3.d0.and.ro3.lt.3.d0.and.
     &            ro2s.ge.3.d0.and.ro2.lt.3.d0) then
             if(m+npt-mo.ge.nzs-2) then
       if(ila.eq.0)write(6,*)' pas possible d''ajouter un strate a m=',m
           if(ila.eq.1) write(6,*)' not possible to add a layer at m=',m
                m=m+1
                mo=mo+1
                tcol(m)=ptcol(mo)
                mm=m
                CALL sVALEURS(mm,mo,1,ff)
             else                    ! il n'y a pas encore trop de strates
                ff=0.5d0
                m=m+1
                tcol(m)=ptcol(mo) + (ptcol(mo+1)-ptcol(mo)) *ff
                mm=m
                CALL sVALEURS(mm,mo,2,ff) ! y(mm)=py(mo)+(py(mo+1)-py(mo))*ff
c                write(6,'(a,2i4,3f7.3,1p2e12.5)')'3',m,mo,rapt,ro7,ro7s,
c     &            dcol/2.d0,tcol(m) 
             endif
             m=m+1
             mo=mo+1
             tcol(m)=ptcol(mo)
             mm=m
             CALL sVALEURS(mm,mo,1,ff)
c             write(6,'(a,2i4,3f7.3,1p2e12.5)')'3',m,mo,rapt,ro7,ro7s,
c     &            dcol/2.d0,tcol(m) 
c
c chute brutale de T ou de degre d'ionisation - drop of T or ionisation degree
          else if(m.ge.10.and.tcol(m).lt.coltot*0.95d0.and.
     &            (rapt.ge.1.25d0.or.rapt.le.0.9d0
     &            .or.ro7.ge.2.d0
     &            .or.rc5.ge.3.d0.or.rne8.ge.3.d0
     &            .or.ro3.ge.3.d0.or.rc3.ge.3.d0.or.rhe.ge.2.d0
     &            .or.ro2.ge.3.d0.or.rc2.ge.3.d0)) then
c
             if(m+npt-mo.ge.nzs-10) then
       if(ila.eq.0)write(6,*)' pas possible d''ajouter un strate a m=',m
           if(ila.eq.1) write(6,*)' not possible to add a layer at m=',m
                m=m+1
                mo=mo+1
                tcol(m)=ptcol(mo)
                mm=m
                CALL sVALEURS(mm,mo,1,ff)
c
             else                         ! il n'y a pas encore trop de strates
                km1=max(nint(rapt/1.25d0),nint(ro7/2.d0),nint(rc5/3.d0),
     &          nint(rne8/3.d0),nint(ro3/3.d0),nint(rc3/3.d0),
     &          nint(rhe/3.d0),nint(ro2/3.d0),nint(rc2/3.d0))
                km=min(km1,10)
                do im=1,km
                   m=m+1
                   ff=float(im)/float(km+1)
                   tcol(m)=ptcol(mo) + (ptcol(mo+1)-ptcol(mo)) *ff
                   mm=m
                   CALL sVALEURS(mm,mo,2,ff) !y(mm)=py(mo)+(py(mo+1)-py(mo))*ff
c                   write(6,'(a,3i4,5f7.3,1p2e12.5)')'2',m,mo,km,rapt,
c     &                   ro7,rc5,ro3,ro2,dcol,tcol(m)
                enddo
                if(rapt.ge.1.25d0.or.rapt.le.0.9d0
     &             .or.ro7.ge.2.d0.or.rc5.ge.3.d0
     &             .or.rne8.ge.3.d0.or.ro3.ge.3.d0.or.rc3.ge.3.d0
     &             .or.rhe.ge.2.d0.or.ro2.ge.3.d0.or.rc2.ge.3.d0) then
                if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1) then
                   if(ila.eq.0) then
                     if(rhe.ge.2.d0) write(15,883)km,mo,tcol(m),rapt,rhe
      if(rc5.ge.3.d0.or.rne8.ge.3.d0)write(15,886)km,mo,tcol(m),rc5,rne8
      if(ro7.ge.2.d0.or.rapt.ge.1.25d0)
     &                               write(15,887)km,mo,tcol(m),rapt,ro7!2345678
      if(rc2.ge.3.d0.or.rc3.ge.3.d0) write(15,888)km,mo,tcol(m),rc2,rc3
      if(ro2.ge.3.d0.or.ro3.ge.3.d0) write(15,884)km,mo,tcol(m),ro2,ro3
                   else
                     if(rhe.ge.2.d0) write(15,983)km,mo,tcol(m),rapt,rhe
      if(rc5.ge.3.d0.or.rne8.ge.3.d0)write(15,986)km,mo,tcol(m),rc5,rne8
      if(ro7.ge.2.d0.or.rapt.ge.1.25d0)
     &                               write(15,987)km,mo,tcol(m),rapt,ro7
      if(rc2.ge.3.d0.or.rc3.ge.3.d0) write(15,988)km,mo,tcol(m),rc2,rc3
      if(ro2.ge.3.d0.or.ro3.ge.3.d0) write(15,994)km,mo,tcol(m),ro2,ro3
                   endif
                else if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)then
                   txt='     '
                   if(ila.eq.0) write(15,882) km,mo,tcol(m),rapt,txt
                   if(ila.eq.1) write(15,982) km,mo,tcol(m),rapt,txt
                endif
 882        format(' ajout de',i2,' strates a m=',i3,' col=',1pe8.2,
     &           ': rapport m/m+1 T=',0pf7.3,2x,a5)
 883        format(' ajout de',i2,' strates a m=',i3,' col=',1pe8.2,
     &           ': rapport m/m+1 T, He+ =',0pf7.3,0pf7.3)
 886        format(' ajout de',i2,' strates a m=',i3,' col=',1pe8.2,
     &           ': rapport m+1/m CV,NeVIII=',0pf7.3,0pf7.3)
 887        format(' ajout de',i2,' strates a m=',i3,' col=',1pe8.2,
     &           ': rapport m/m+1 T, OVII=',0pf7.3,0pf7.3)
 888        format(' ajout de',i2,' strates a m=',i3,' col=',1pe8.2,
     &           ': rapport m+1/m CII,CIII=',0pf7.3,0pf7.3)
 884        format(' ajout de',i2,' strates a m=',i3,' col=',1pe8.2,
     &           ': rapport m+1/m OII,OIII=',0pf7.3,0pf7.3)
 982        format(i2,' layers added at m=',i4,' col=',1pe8.2,
     &           ': ratio m/m+1 T=',0pf7.3,2x,a5)
 983        format(i2,' layers added at m=',i4,' col=',1pe8.2,
     &           ': ratio m/m+1 T, He+ =',0pf7.3,0pf7.3)
 986        format(i2,' layers added at m=',i4,' col=',1pe8.2,
     &           ': ratio m+1/m CV,NeVIII=',0pf7.3,0pf7.3)
 987        format(i2,' layers added at m=',i4,' col=',1pe8.2,
     &           ': ratio m/m+1 T, OVII=',0pf7.3,0pf7.3)
 988        format(i2,' layers added at m=',i4,' col=',1pe8.2,
     &           ': ratio m+1/m CII, CIII=',0pf7.3,0pf7.3)
 994        format(i2,' layers added at m=',i4,' col=',1pe8.2,
     &           ': ratio m+1/m OII, OIII=',0pf7.3,0pf7.3)
                endif
             endif
             m=m+1
             mo=mo+1
             tcol(m)=ptcol(mo)
             mm=m
             CALL sVALEURS(mm,mo,1,ff)
c      write(6,'(a,3i4,2f7.3,1p2e12.5)')'2',m,mo,km,rapt,ro7,dcol,tcol(m)
c
c sans modification - without modification
          else
             m=m+1
             mo=mo+1
             tcol(m)=ptcol(mo)
             mm=m
             CALL sVALEURS(mm,mo,1,ff)
c       write(6,'(a,2i4,2f7.3,1p2e12.5)')'0',m,mo,rapt,ro7,dcol,tcol(m)
c                     le dcol ecrit est celui de m-1
          endif
          if(mo.eq.npt) goto 20
       enddo   ! ii
 20    continue
       npt=m
       tcol(npt+1)=tcol(npt)
       tz(npt+1)=tz(npt)
c
        return
        end                                                        !schagrille2
c tcol(m) ptcol(m) tz(m) ptz(m) ptemp(m)
c igrid npd1
c------------------------------------------------------------------------------
       subroutine sVALEURS(mm,m1,ival,ff)

         implicit double precision (a-h,o-z)
         parameter (nzs=999,nrais=4200)
       common/chag/ptz(nzs+1),ptnht(nzs),ptemp(nzs),pprad(nzs),
     &             ptjc(322,nzs),ptjr(nrais,nzs),ptcjn(nrais,nzs)
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2) !234567
       common/sporpal/tjr(nrais,nzs),tcjn(nrais,nzs),pradr(nrais,nzs)
       common/stnua/prad(nzs),pradrai(nzs),pradrai1(nzs),rhep2
c
       if(ival.eq.0) then
           ptz(mm)=tz(mm)
           ptnht(mm)=tnht(mm)
           ptemp(mm)=temp(mm)
           pprad(mm)=prad(mm)
           do kc=1,322
              ptjc(kc,mm)=tjc(kc,mm)
           enddo   !kc
           do kr=1,nrais
              ptjr(kr,mm)=tjr(kr,mm)
              ptcjn(kr,mm)=tcjn(kr,mm)
           enddo   !kr
       endif
c
       if(ival.eq.1) then
          tz(mm)=ptz(m1)
          tnht(mm)=ptnht(m1)
          temp(mm)=ptemp(m1)
          prad(mm)=pprad(m1)
          do kc=1,322
             tjc(kc,mm)=ptjc(kc,m1)
          enddo   ! kc
          do kr=1,nrais
             tjr(kr,mm)=ptjr(kr,m1)
             tcjn(kr,mm)=ptcjn(kr,m1)
          enddo   ! kr
       endif
c
       if(ival.eq.2) then
          tz(mm)=ptz(m1) + (ptz(m1+1)-ptz(m1)) *ff
          tnht(mm)=ptnht(m1) + (ptnht(m1+1)-ptnht(m1)) *ff
          temp(mm)=ptemp(m1) + (ptemp(m1+1)-ptemp(m1)) *ff
          prad(mm)=pprad(m1) + (pprad(m1+1)-pprad(m1)) *ff
          do kc=1,322
             tjc(kc,mm)=ptjc(kc,m1) + (ptjc(kc,m1+1)-ptjc(kc,m1)) *ff
          enddo                 ! kc
          do kr=1,nrais
             tjr(kr,mm)=ptjr(kr,m1) + (ptjr(kr,m1+1)-ptjr(kr,m1)) *ff
             tcjn(kr,mm)=ptcjn(kr,m1)+(ptcjn(kr,m1+1)-ptcjn(kr,m1))*ff
          enddo   ! kr
       endif
c
        return
        end                                                           !svaleurs
c------------------------------------------------------------------------------
       subroutine sRAPPORTS(mi,mf,ro7,rc5,rne8,ro3,rc3,rhe,ro2,rc2,
     &                      ro7s,ro3s,ro2s)

         implicit double precision (a-h,o-z)
         parameter (nzs=999)
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &          Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &          tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
c      
           ro7=0.d0
           rc5=0.d0
           rne8=0.d0
           ro3=0.d0
           rc3=0.d0
           rhe=0.d0
           ro2=0.d0
           rc2=0.d0
c           rs14=0.d0
c           ro7s=0.d0
c           rne8s=0.d0
c           rc5s=0.d0
c           ro2s=0.d0
        if(trij(mf,27).ge.trij(mi,27).and.trij(mi,27).gt.1.d-4)
     &     ro7=trij(mf,27)/trij(mi,27)
        if(trij(mf,27).lt.trij(mi,27).and.trij(mf,27).gt.1.d-4)
     &     ro7=trij(mi,27)/trij(mf,27)
        if(trij(mf,10).ge.trij(mi,10).and.trij(mi,10).gt.1.d-4)
     &     rc5=trij(mf,10)/trij(mi,10)
        if(trij(mf,10).lt.trij(mi,10).and.trij(mf,10).gt.1.d-4)
     &     rc5=trij(mi,10)/trij(mf,10)
        if(trij(mf,37).ge.trij(mi,37).and.trij(mi,37).gt.1.d-4)
     &     rne8=trij(mf,37)/trij(mi,37)
        if(trij(mf,37).lt.trij(mi,37).and.trij(mf,37).gt.1.d-4)
     &     rne8=trij(mi,37)/trij(mf,37)
        if(trij(mf,23).ge.trij(mi,23).and.trij(mi,23).gt.1.d-4)
     &     ro3=trij(mf,23)/trij(mi,23)
        if(trij(mf,8).ge.trij(mi,8).and.trij(mi,8).gt.1.d-4)
     &     rc3=trij(mf,8)/trij(mi,8)
        if(trij(mf,4).ge.trij(mi,4).and.trij(mi,4).gt.1.d-6)
     &     rhe=trij(mf,4)/trij(mi,4)
        if(trij(mf,4).lt.trij(mi,4).and.trij(mf,4).gt.1.d-6)
     &     rhe=trij(mi,4)/trij(mf,4)
        if(trij(mf,22).ge.trij(mi,22).and.trij(mi,22).gt.1.d-4)
     &     ro2=trij(mf,22)/trij(mi,22)
        if(trij(mf,7).ge.trij(mi,7).and.trij(mi,7).gt.1.d-4)
     &     rc2=trij(mf,7)/trij(mi,7)
        if(trij(mi+1,27).ge.trij(mf+1,27).and.trij(mf+1,27).gt.1.d-4)
     &     ro7s=trij(mi+1,27)/trij(mf+1,27)
        if(trij(mi+1,27).lt.trij(mf+1,27).and.trij(mi+1,27).gt.1.d-4)
     &     ro7s=trij(mf+1,27)/trij(mi+1,27)
        if(trij(mi+1,23).ge.trij(mf+1,23).and.trij(mf+1,23).gt.1.d-4)
     &     ro3s=trij(mi+1,23)/trij(mf+1,23)
        if(trij(mi+1,22).ge.trij(mf+1,22).and.trij(mf+1,22).gt.1.d-4)
     &     ro2s=trij(mi+1,22)/trij(mf+1,22)
c        if(trij(mf,82).ge.trij(mi,82).and.trij(mi,82).gt.1.d-4)
c     &     rs14=trij(mf,82)/trij(mi,82)
c        if(trij(mf,82).lt.trij(mi,82).and.trij(mf,82).gt.1.d-4)
c     &     rs14=trij(mi,82)/trij(mf,82)
c        if(trij(mi+2,37).lt.trij(mf,37).and.trij(mi+2,37).gt.1.d-4)
c     &     rne8s=trij(mf,37)/trij(mi+2,37)
c        if(trij(mi+2,10).ge.trij(mf,10).and.trij(mf,10).gt.1.d-4)
c     &     rc5s=trij(mi+2,10)/trij(mf,10)
c        if(trij(mi+2,10).lt.trij(mf,10).and.trij(mi+2,10).gt.1.d-4)
c     &     rc5s=trij(mf,10)/trij(mi+2,10)
c        if(trij(mi+3,10).ge.trij(mi+2,10).and.trij(mi+2,10).gt.1.d-4)
c     &     rc5b=trij(mi+3,10)/trij(mi+2,10)
c        if(trij(mi+3,10).lt.trij(mi+2,10).and.trij(mi+3,10).gt.1.d-4)
c     &     rc5b=trij(mi+2,10)/trij(mi+3,10)
c
        return
        end                                                          !srapports
c---===========================================================================
       subroutine DEGION(kitd1)

c CALCUL DES DEGRES D'IONISATION - IONIZATION DEGREES CALCULATION

       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15)
         double precision ne,nht
         dimension pr(27,10),somf(10)
         dimension itz(6),iti(6),t1(6),t5(27)
         dimension fsaha(0:27),rsaha(0:27),posaha(nivmax,27)
       common/abond/ab(10)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/temp/T,VT,tk,tk1,templog
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/raiesuta/probuta(100),dnuta2(100),utanion(26,10),
     &                 juta(100),iuta(100)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/deglosm/pnenht,praieint,r(27,10)
       common/talfdeg2/exphot(nivtots),exphotk(nivtots)
       common/lecdeg/eps1,eps2,sel
       common/datdeg/alpha(26,10),alfdi(26,10),edi(26,10),cio(nivtots),
     &          ech(26,10),eche(26,10),alfrad(26,10),
     &          teec(220),tgaufrec(220)
       common/dastpo/alfint(nivtots),cioe(nivtots),
     &          texcoll(nrais,nzs+1),teer(nrais,nzs)
       common/stpo/tphot(nivtots),tphotk(26,10),dens(nivtots),
     &          alfinduit(nivtots),zfree,cthomson
       common/pop/po1(nivmax+1,26),sompo1(26),f(27,10),
     &             ph0ne,phpne,phe0ne,phepne,sech1,sech2,seche1,seche2
       common/stdeg/popsec(26,10),dpech(26,10),ecartdeg,nitdmax,
     &              jmp1(10),jmp2(10)
       common/rpal/twnu(16,nrais,nzs),tphi(16,nrais,nzs),
     &        tcapr(nrais,nzs),dcapr(nrais,nzs),dsl(nrais,nzs),
     &        tas(nrais,nzs),tbs(nrais,nzs),tne(nzs+1),tdensf(nzs,26,10)
       common/mil/rh1smil,rh2pmil,rhe1mil,rhe2fmil,rhe22mil,rfe15mil
c degion
c        tk=ckev*T
c        tk1=1.d0/tk
        saha=2.07079d-16/T/VT
           nivh=nivion(1,1)
           nhe1=nivcum(0,2)+1
           nhe2=nivcum(1,2)+1
c initialisation du degre d ionisation
                if(m.gt.1.or.nitt.gt.1.or.nitd.gt.1) goto 100
        f21=(exphot(1)/nht+cio(1))/alfrad(1,1)
        f22=(exphot(nhe1)/nht+cio(nhe1))/alpha(1,2)
        f32=f22*(exphot(nhe2)/nht+cio(nhe2))/alfrad(2,2)
        pnenht=f21/(1.d0+f21)+ab(2)*(f22+2.d0*f32)/(1.d0+f22+f32)
        ne=nht*pnenht
        f53=f32*f32*1.d-150
        if(f53.eq.0.d0) f53=1.d-200
        f54=f53
        r(1,1)=1.d0/(1.d0+f21)
        r(2,1)=f21*r(1,1)
        r(1,2)=1.d0/(1.d0+f22+f32)
        r(2,2)=f22*r(1,2)
        ph0ne= r(1,1)/pnenht
        phpne= r(2,1)/pnenht
        phe0ne=ab(2)*r(1,2)/pnenht
        phepne=ab(2)*r(2,2)/pnenht
        dens(1)=r(1,1)*nht
        dens(2)=dens(1)*expa(-10.2d0*tk1)/100.d0                           !deg
        dens(3)=dens(2)*3.d0
        dh2p=dens(3)
        do n=4,nivh
        dens(n)=dens(3)
        enddo
        dh4=dens(5)
        zfree=r(2,1)+(r(2,2)+4.d0*(1.d0-r(1,2)-r(2,2)))*ab(2)
        do 101 j=1,nelem
           iz0=iz(j)
           do 101 i=1,iz0
              nij0=nivcum(i-1,j)
              niv=nivion(i,j)
              do n=1,niv
                 alfinduit(nij0+n)=0.d0
                 tphot(nij0+n)=exphot(nij0+n)
              enddo
              tphotk(i,j)=exphotk(nij0+n)
  101          continue
  100          continue
c        print*,'tphot(1),exphot(1)',tphot(1),exphot(1)                   !deg
                if(imp.eq.4.and.imppgt.eq.0) then
        if(nitd.eq.1) then
           WRITE(6,*)
           WRITE(6,*) '  m nd nt Ne/Nh    H0/Nh    H2p/Nh',
     &                '     H+/H0   He++/He0   C4+/C0   ecart%moy'
           if(m.eq.1.and.nitt.eq.1) then
              WRITE(6,281) m,nitd,nitt,pnenht,dens(1)/nht,dens(3)/nht,
     &               f21,f32,f53*1.d150,ecartdeg*100.d0
           else
              WRITE(6,281) m,nitd,nitt,pnenht,dens(1)/nht,
     &          dens(3)/nht,f(2,1),f(3,2),f(5,3)*1.d150,ecartdeg*100.d0
           endif
        endif
c        if(nitt.eq.1) WRITE(6,*) '  m  nd nt    T       Ne/Nh   H0/Nh', !234567
c     &                '    H2p/Nh     H3/Nh     H+/Nh     H+/H0'
c        WRITE(6,281) m,nitd,nitt,T,pnenht,dens(1)/nht,dens(3)/nht,
c     &               dens(4)/nht,r(2,1),f21
c        WRITE(16,280)
c        WRITE(16,282) nitd,dens(3)/nht,f21,f32,f53*1.d150,f54*1.d150,ph0ne,
c     1           tphot(1)
c  280   format(' no it.  H2p/H+    H+/H0    He2+/He0   C4+/C0    ',
c     1         'N4+/N0    H0/Ne  tphot(1)')
                endif
c
c CALCUL DES DEGRES D'IONISATION - ionization degrees calculation
        sech1=0.d0
        sech2=0.d0
        seche1=0.d0
        seche2=0.d0
            dpech(1,1)=0.d0
            dpech(1,2)=0.d0
            dpech(2,2)=0.d0
        do j=1,nelem
           iz1=iz(j)+1
           do i=1,iz1
              pr(i,j)=r(i,j)
           enddo
        enddo
c ionisation des elements sauf H et He - except H and He
c obligation d'utiliser f(i,j) et de commencer par i=1, a cause des Auger
           jfo=0
                          do 201 j=3,nelem
           kfij=0
           ifo=0
           iz0=iz(j)                                                       !deg
           iz1=iz(j)+1
           f(1,j)=1.d-150                
                   do i=1,26             
                      f(i+1,j)=0.d0     
                   enddo
                            if(ab(j).eq.0.d0) then
                               do i=1,iz0
                                  r(i,j)=0.d0
                                  niv=nivion(i,j)
                                  nij0=nivcum(i-1,j)
                                  do n=1,niv
                                     nij=nij0+n
                                     dens(nij)=0.d0
                                  enddo
                               enddo
                               r(iz0+1,j)=0.d0
                               goto 201
                            endif   ! ab=0
                   sf1=0.d0
                   do i=1,iz0    ! premier passage pour avoir une idee des rij
                      CALL sPoP0(i,j)
                      sf1=sf1 + f(i,j)
                   enddo
                   sf1=sf1 + f(iz0+1,j)
                   d11=nht*ab(j)/sf1
                   do i=1,iz0 
                      r(i,j)=f(i,j)/sf1 
                   enddo
                   do i=1,26              ! r(i,j)   =N(t,i,j)/N(j) frac.ionis.
                      sompo1(i)=0.d0      ! sompo1(i)=som(Nn)/N1 pour 1 ion 
                      f(i+1,j)=0.d0       ! po1(n,i) =Nn/N1
                   enddo
           sf1=0.d0
        do 202 i=1,iz0
                do n=1,nivmax
                   po1(n,i)=0.d0                                           !deg
                enddo
                sompo1(i)=0.d0
                if(f(i,j).eq.0.d0) goto 203
                dpech(i,j)=0.d0
           niv=nivion(i,j)
           if(niv.eq.1.or.r(i,j).lt.1.d-20) then
              CALL sPoP0(i,j)
           else 
              CALL sPoP2(i,j)
c                 if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1.and.kimpf.eq.1.and.
c     &           j.eq.5.and.(i.eq.6.or.i.eq.8)) CALL sPoP2(i,j)
           endif
           sf1=sf1 + f(i,j)*sompo1(i)
                        if(sf1.ge.1.d+250) then         ! fij tres grands
                           sf1=0.d0
                           do ii=1,i+1
                              f(ii,j)=f(ii,j)*1.d-100
                              sf1=sf1+f(ii,j)*sompo1(ii)
                           enddo
                           kfij=1
                        endif   ! fij>1e250
                        if(f(i+1,j).lt.0.d0.and.f(i,j).gt.0.d0) then
                           kfij=-1
                           jfo=j
                           ifo=i
                        endif
  202   continue   ! i
 203    continue                                                           !deg
        sf1=sf1 + f(iz0+1,j)
c           WRITE(10,*) ' les f de j=',j
c           WRITE(10,'(8(1pe9.2))') (f(i,j),i=1,iz0)

        d11=nht*ab(j)/sf1                                        ! N11j/f(1,j)
        do i=1,iz0
           r(i,j)=sompo1(i)*f(i,j)/sf1     ! degres d'ionisation N(t,i,j)/N(j)
           niv=nivion(i,j)
           nij0=nivcum(i-1,j)
           do n=1,niv 
              nij=nij0+n
              dens(nij)=d11*f(i,j)*po1(n,i)                      ! N(n,i,j)
           enddo
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) 
     &     tdensf(m,i,j)=dens(nij0+1)
        enddo
        r(iz0+1,j)=f(iz0+1,j)/sf1
        dens(nij+1)=d11*f(iz0+1,j)                        ! ion nu N(1,iz0+1,j)

c        if(m.eq.1.and.nitf.eq.1.and.nitt.eq.1) then
c        write(75,'(8(1pe9.2))') (r(i,j),i=1,iz0+1)
c        write(75,'(8(1pe9.2))') (dens(nij),nij=nij0+1,nij0+niv+1)
c        endif

                           if(kitt.eq.0.or.kitn.eq.0) goto 204
        somr=0.d0
        do i=1,iz1
        somr=somr+r(i,j)                                                   !deg
        enddo
                if(somr.le.0.97.or.somr.ge.1.03) then
c                write(15,294) m,j,nitd,nitt,ne,T
                write(6,294) m,j,nitd,nitt,ne,T
                write(6,'(7(1pe10.3))') (f(i,j),i=1,iz1)
                write(6,'(7(1pe10.3))') (r(i,j),i=1,iz1)
                write(6,'(7(1pe10.3))') sf1,somr
 294            format(' PB somr a m=',i4,' j=',i3,' nitd=',i3,
     &          ' nitt=',i3,' ne=',1pe10.3,' T=',1pe10.3,' f&r&s')
                endif   ! somr
                   if(kfij.eq.1) then            ! fij tres grands
c                   write(15,295) m,j,nitd,nitt,ne,T
                   write(6,295) m,j,nitd,nitt,ne,T
                   write(6,'(7(1pe10.3))') (f(i,j),i=1,iz1)
                   write(6,'(7(1pe10.3))') (r(i,j),i=1,iz1)
                   write(6,'(7(1pe10.3))') sf1,somr
 295               format(' fij reajuste a m=',i4,' j=',i3,' nitd=',i3,
     &             ' nitt=',i3,' ne=',1pe10.3,' T=',1pe10.3,' f&r&s')
                   kfij=0
                   endif   ! kfij=1
                      if(kfij.eq.-1.and.j.eq.jfo) then          ! fij<0
c                        if((kfij.eq.-1.and.j.eq.jfo)
c     &                  .or.(kimpm.eq.1.and.j.eq.5.and.i.eq.6)) then
c                         if(kitd.eq.1) then                               !deg
                      if(kitd.eq.1.and.abs(r(ifo,j)).ge.1.d-20) then
c                      write(15,296) m,j,ifo,nitd,nitt,ne,T
                      write(6,296)  m,j,ifo,nitd,nitt,ne,T
                      write(6,'(7(1pe11.3))') (f(i,j),i=1,iz1)
                      write(6,'(7(1pe11.3))') (r(i,j),i=1,iz1)
c                     write(6,'(7(1pe11.3))') sf1,somr
 296              format(' PB fij<0 a m=',i3,' j=',i2,' i=',i2,' nitd=',
     &            i2,' nitt=',i2,' ne=',1pe10.3,' T=',1pe10.3,' f&r')
                      endif   ! kitd=1
                      endif   ! kfij=-1 <== un fij<0
c
 204    continue    ! fin kitt=1
c
        if(kfij.eq.-1) then
           somr=0.d0
           do i=1,iz1
              if(r(i,j).lt.0.d0) r(i,j)=0.d0
              somr=somr+r(i,j)                                             !deg
           enddo
           do i=1,iz1
              r(i,j)=r(i,j)/somr
           enddo
           do i=1,iz0
              niv=nivion(i,j)
              nij0=nivcum(i-1,j)
              do n=1,niv
                 nij=nij0+n
                 dens(nij)=0.d0
                    if(sompo1(i).gt.0.d0)
     &           dens(nij)=nht*ab(j)*r(i,j)*po1(n,i)/sompo1(i)    ! N(n,i,j)
                 if(dens(nij).lt.0.d0) dens(nij)=0.d0
              enddo
           enddo
           dens(nij+1)=nht*ab(j)*r(iz1,j)
           if(dens(nij+1).lt.0.d0) dens(nij+1)=0.d0
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1)
     &        write(6,'(7(1pe11.3))') (r(i,j),i=1,iz1)
        endif
                do i=1,iz0
        nij1=nivcum(i,j) +1
        sech1=sech1 + ech(i,j)*dens(nij1)
        seche1=seche1 + eche(i,j)*dens(nij1)
           if(pkiev(i,j).gt.13.6d0) dpech(i,j)=
     &        ech(i,j)*dens(nij1)*ph0ne*(pkiev(i,j)-13.6d0)/nht*chel
           if(pkiev(i,j).gt.24.58d0) dpech(i,j)=dpech(i,j) +
     &        eche(i,j)*dens(nij1)*phe0ne*(pkiev(i,j)-24.58d0)/nht*chel
                enddo
 201        continue   ! j                                                 !deg

        sech1=sech1/ne
        seche1=seche1/ne
        nij15=nivcum(0,5) +1
        nij28=nivcum(1,8) +1
        nij38=nivcum(2,8) +1
        sech2=
     &    (dens(nij15)*ech(1,5)*expa(tk1*(13.6d0-pkiev(1,5)))*8.d0/9.d0
     &   + dens(nij28)*ech(2,8)*expa(tk1*(13.6d0-pkiev(2,8)))/3.d0)/ne  !234567
        seche2=dens(nij38)*eche(3,8)*expa(tk1*(24.58d0-pkiev(3,8)))/ne
c ionisation de l'helium
           f(1,2)=1.d0
           sf1=0.d0
                   do i=1,26
                      sompo1(i)=0.d0
                      f(i+1,2)=0.d0
                   enddo
        do i=1,2
                   do n=1,nivmax
                      po1(n,i)=0.d0
                   enddo
           niv=nivion(i,2)
           if(niv.eq.1) then
              CALL sPoP0(i,2)
           else
              CALL sPoP2(i,2)
           endif
           sf1=sf1 + f(i,2)*sompo1(i)
        enddo   ! i
        sf1=sf1 + f(3,2)
        d11=nht*ab(2)/sf1                                                  !deg
        do i=1,2
           r(i,2)=sompo1(i)*f(i,2)/sf1     ! degres d'ionisation N(t,i,j)/N(j)
           niv=nivion(i,2)
           nij0=nivcum(i-1,2)
           do n=1,niv
              nij=nij0+n
              dens(nij)=d11*f(i,2)*po1(n,i)                        !  N(n,i,j)
           enddo   ! n
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1)
     &     tdensf(m,i,2)=dens(nij0+1)
        enddo   ! i
        r(3,2)=f(3,2)/sf1
        dens(nij+1)=d11*f(3,2)                        ! ion nu N(1,iz0+1,j)
        rhep2=po1(2,2)*f(2,2)/sf1                     ! He+ niveau 2 -> trij

c        if(m.eq.1.and.nitf.eq.1.and.nitt.eq.1) then
c        write(75,'(8(1pe9.2))') (f(i,2),i=1,3),sf1
c        write(75,'(8(1pe9.2))') (r(i,2),i=1,3)
c        write(75,'(8(1pe9.2))') (dens(nij),nij=nij0+1,nij0+niv+1)
c        endif

                   f(1,1)=1.d0
                   do i=1,26
                      sompo1(i)=0.d0
                      f(i+1,1)=0.d0
                   enddo
                   do n=1,nivmax
                      po1(n,1)=0.d0                                        !deg
                   enddo
                   sf1=0.d0
        CALL sPoP2(1,1)
c           nivh=nivion(1,1)
           sf1=sompo1(1)+po1(nivh+1,1)
           dens(1)=nht/sf1
           do n=2,nivh+1
              dens(n)=dens(1)*po1(n,1)                        !  N(n,i,j)
           enddo
           r(1,1)=sompo1(1)/sf1
           r(2,1)=po1(nivh+1,1)/sf1
           f(2,1)=po1(nivh+1,1)
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1)
     &     tdensf(m,1,1)=dens(1)

c        if(m.eq.1.and.nitf.eq.1.and.nitt.eq.1) then
c        write(75,'(8(1pe9.2))') (f(i,1),i=1,2),sf1
c        write(75,'(8(1pe9.2))') (r(i,1),i=1,2)
c        write(75,'(8(1pe9.2))') (dens(n),n=1,nivh+1)
c        endif

c   calcul des limites ou les degres d'ionisation sont >1E-08
c   calculation of limits where ionisation degrees are >1E-08
        do 211 j=3,nelem
        iz0=iz(j)
        kjmp1=0
        jmp1(j)=1
        jmp2(j)=iz0
        do 212 i=1,iz0
        if(r(i,j).lt.1.d-8) then                                           !deg
                if(kjmp1.eq.0) go to 212
                jmp2(j)=i-1
                go to 211
        else
                if(kjmp1.eq.1) go to 212
                jmp1(j)=i
                kjmp1=1
        endif
  212   continue
  211   continue
        jmp1(1)=1
        jmp2(1)=1
        jmp1(2)=1
        jmp2(2)=2
c
c  tests sur les degres d'ionisation
        if(nitd.eq.1) goto 220
           vardeg=0.d0
           nb=0
        do j=1,nelem                                                       !deg
           iz1=iz(j)+1
           do i=1,iz1
c if((j.eq.1.or.(j.eq.2.and.i.eq.2).or.r(i,j).ge.1.d-4).and.r(i,j).le.0.999d0)
              if(r(i,j).gt.0.d0.and.(j.eq.1.or.r(i,j).ge.1.d-5)
     &             .and.r(i,j).le.0.999d0)then
                 vardeg=vardeg + ((r(i,j)-pr(i,j))/r(i,j))**2
                 nb=nb+1
              endif
           enddo
        enddo
        ecartdeg=sqrt(vardeg/nb)
        if(ecartdeg.le.eps1) kitd1=1
 220    continue
c        f21=f(2,1)
c        dh2p=dens(3)
c        dh4=dens(5)
c        f32=f(3,2)
c        f53=f(5,3)
c        f54=f(5,4)
c  calcul de la densite electronique ne et de ph0ne=Nhneutre/Ne
        ssom=r(2,1)+(r(2,2)+2.d0*r(3,2))*ab(2)
        zfree=r(2,1)+(r(2,2)+4.d0*r(3,2))*ab(2)
        do 231 j=3,nelem
           iz1=iz(j)+1
           somf(j)=0.d0
           do 232 i=2,iz1
              hi1=dfloat(i-1)
              zfree=zfree+r(i,j)*ab(j)*hi1*hi1
  232      somf(j)=somf(j)+r(i,j)*hi1
  231   ssom=ssom+somf(j)*ab(j)
        pnenht=ssom
        ne=nht*pnenht                                                      !deg
        if(kitd.eq.1.and.kitt.eq.1) tne(m)=ne
        ph0ne=dens(1)/ne
        nij21=nivcum(1,1) + 1
        phpne=dens(nij21)/ne
        nij12=nivcum(0,2) + 1
        phe0ne=dens(nij12)/ne
        nij22=nivcum(1,2) + 1
        phepne=dens(nij22)/ne
        cthomson=ne*0.6652d-24
         if(imp.eq.4.and.imppgt.eq.0)
     &   WRITE(6,281) m,nitd,nitt,pnenht,dens(1)/nht,
     &          dens(3)/nht,f(2,1),f(3,2),f(5,3)*1.d150,ecartdeg*100.d0
  281    format(i4,2i3,0pf7.4,1p5e10.3,0pf10.4)
  282    format(i6,7(1pe10.3))
c        WRITE(15,283) nitd-1,ne
  283    format (' nbre iterations sur les degres d ionis.=',i3,
     1           ' densite electronique Ne=',1pe11.4)
c
c traitement des
c recombinaisons sur un niveau secondaire (cas des ions a 1 niveau+continu)
c et recombinaisons dielectroniques (cas de tous les ions) :
c chaque recombinaison donne lieu a une cascade et donc finalement a une raie,
c qu'on suppose etre la premiere de la liste de l'ion
c popsec sert a rajouter un photon a la 1ere raie
! for the ions with one level+continuum: recombinations onto an excited level,
! for all the ions : dielectronic recombinations :
! each recombination gives cascade, leading consequently to a "resonant" line
! the first one of the list is chosen
        if(ietl.eq.0) then                                               !deg
           do kc=1,102
                ic=ict(kc)
                jc=jct(kc)
              nivc=nivion(ic,jc)
              popsec(ic,jc)=0.d0
              if(nivc.eq.1) then
                 nij1c=nivcum(ic-1,jc) + 1
                 alf=alpha(ic,jc)-alfint(nij1c)   !contient les dielectroniques
                 popsec(ic,jc)=r(ic+1,jc)*ne*alf
              endif
           enddo
        endif   ! ietl=0
c
c IMPRESSIONS
                if(kitt.eq.0.or.kitn.eq.0.or.kitd.eq.0) goto 250
        if(nitd.ge.nitdmax) WRITE(6,284) nitdmax,m,ecartdeg*100.d0
        if(nitd.ge.nitdmax.and.ifich.ne.0.and.ifich.le.8
     &  .and.iexpert.eq.1) WRITE(15,284) nitdmax,m,ecartdeg*100.d0
  284   format (' PB:les fract.d ionis. ne convergent pas en',i3,
     1           'it. pour m=',i3,'(ec.deg%=',f8.4,')')
c
                        if(m.eq.npt/2) then
           rh1smil=dens(1)/nht
           rh2pmil=dens(3)/nht                                             !deg
              nhe2=nivcum(1,2)+1
           rhe1mil=r(1,2)
           if(ab(2).gt.0.d0) rhe2fmil=dens(nhe2)/nht/ab(2)
           if(ab(2).gt.0.d0) rhe22mil=dens(nhe2+1)/nht/ab(2)
           rfe15mil=r(15,10)
c        if(kimpf2.eq.1) then
c        WRITE(15,292) m,T
c 292    format(' degres d ionisation au milieu : m=',i4,' T=',1pe10.3)
c        WRITE(15,286)(dens(n)/nht,n=1,nivh+1),(rsaha(n),n=0,nivh),
c     &               (r(i,2),i=1,3),rsahahe1,rsahahe2,rsahahe3,
c     &               (dens(nij)/nht/ab(2),nij=nivcum(1,2)+1,nivcum(2,2))
c        do j=3,nelem
c        iz0=iz(j)
c        niv=nivion(iz0,j)
c        if(j.lt.5) WRITE(15,883) iz(j),(r(i,j),i=1,iz0+1)
c        if(j.ge.5) WRITE(15,287) iz(j),(r(i,j),i=1,iz0+1)
c        if(nivion(iz0-2,j).gt.1) WRITE(15,885)
c     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-3,j)+1,nivcum(iz0-2,j))
c        if(nivion(iz0-1,j).gt.1) WRITE(15,884) 
c     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-2,j)+1,nivcum(iz0-1,j))
c        WRITE(15,293)
c     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-1,j)+1,nivcum(iz0,j))
c        enddo
c        endif   ! nitf
                        endif   ! m=npt/2                                  !deg
c
       if(kimpf2.eq.0.or.kimpm.eq.0.or.ifich.lt.3.or.ifich.gt.8)goto 250
        fsaha(nivh)=1.d0/(ne*expa(13.6d0*tk1)*2.d0*saha)               !NH+/Nf
                soma=fsaha(nivh)+1.d0
                do n=2,nivh-1
                hnbq=dfloat(n)
                gn=2.d0*dfloat(n)*dfloat(n)
                if(n.eq.2) gn=6.d0
        fsaha(n)=gn/2.d0*expa(-13.6d0*tk1*(1.d0-1.d0/hnbq/hnbq))       !Nn/Nf
                soma=soma+ fsaha(n)
                enddo
        fsaha(1)=fsaha(2)/3.d0
                soma=soma+ fsaha(1)
        rsaha(0)=1.d0/soma
                do n=1,nivh
        rsaha(n)=fsaha(n)/soma
                enddo
        he21=2.d0*teec(2) /(ne*saha)                               ! He+/He0
        he32=teec(3) /(ne*2.d0*saha)                               ! He++/He+
        rsahahe1=1.d0/(1.d0+he21*(1.d0+he32))
        rsahahe2=he21*rsahahe1
        rsahahe3=he21*he32*rsahahe1
        WRITE(16,*)
            if(imp.ge.2) then
        WRITE(16,*) ' m,z=',m,tz(m)                                        !deg
        WRITE(16,285)
        if(ab(2).gt.0.d0)
     &  WRITE(16,286) (dens(n)/nht,n=1,nivh+1),(rsaha(n),n=0,nivh),
     &               (r(i,2),i=1,3),rsahahe1,rsahahe2,rsahahe3,
     &               (dens(nij)/nht/ab(2),nij=nivcum(1,2)+1,nivcum(2,2))
                         if(ifich.ge.4) then
        WRITE(22,*) ' m,z=',m,tz(m)
        if(ila.eq.0) WRITE(22,285)
        if(ila.eq.1) WRITE(22,985)
        if(ab(2).gt.0.d0)
     &  WRITE(22,286) (dens(n)/nht,n=1,nivh+1),(rsaha(n),n=0,nivh),
     &               (r(i,2),i=1,3),rsahahe1,rsahahe2,rsahahe3,
     &               (dens(nij)/nht/ab(2),nij=nivcum(1,2)+1,nivcum(2,2))
                         endif   ! ifich
                         endif   ! imp
  285   format (5x,'abondance fractionnelle nxi/nxtotal',
     &                          ' (H : niv.=1,2s,2p,3,4,5 et ion)')
  985   format (5x,'fractional abundance nxi/nxtotal',
     &                          ' (H : niv.=1,2s,2p,3,4,5 et ion)')
  286   format (' Hydr ',7(1pe10.3)/' Saha ',7(1pe10.3)/' He   ',3(1pe
     &  10.3),'   He Saha',3(1pe10.3)/' He+(1,2,3,4,5) ',5(1pe10.3))
                        if(ifich.ge.3.and.ifich.le.8.and.
     &                     (imp.eq.1.or.imp.eq.3.or.imp.eq.4)) then
c        WRITE(16,882)                                                     !deg
  882   format(6('   z  i      '))
        mimp=0
        do 800 j=1,nelem
        iz1=iz(j)+1
        do 800 i=1,iz1
        if(r(i,j).lt.0.1) goto 801
        mimp=mimp+1
        jimp=mod(mimp,6)
        if(jimp.eq.0) jimp=6
        itz(jimp)=iz1-1
        iti(jimp)=i
        t1(jimp)=r(i,j)
        if(jimp.ge.6) WRITE(16,881) (itz(k),iti(k),t1(k),k=1,6)
  801        continue
        if(jimp.lt.6.and.i.eq.27)
     1       WRITE(16,881) (itz(k),iti(k),t1(k),k=1,jimp)
  881   format(6(i4,i3,f6.3))
        WRITE(16,*)
  800   continue
             else if(imp.eq.2.or.imp.eq.5.or.imp.eq.6.or.imp.eq.7) then
        do j=3,nelem
        iz0=iz(j)
        niv=nivion(iz0,j)
           do i=1,iz0+1
           t5(i)=max(r(i,j),1.d-99)
           enddo   ! i
        if(j.lt.5) WRITE(16,883) iz(j),(t5(i),i=1,iz0+1)                   !deg
        if(j.ge.5) WRITE(16,287) iz(j),(t5(i),i=1,iz0+1)
        if(nivion(iz0-4,j).gt.1.and.ab(j).gt.0.d0) WRITE(16,887)
     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-5,j)+1,nivcum(iz0-4,j))
        if(nivion(iz0-3,j).gt.1.and.ab(j).gt.0.d0) WRITE(16,886)
     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-4,j)+1,nivcum(iz0-3,j))
        if(nivion(iz0-2,j).gt.1.and.ab(j).gt.0.d0) WRITE(16,885)
     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-3,j)+1,nivcum(iz0-2,j))
        if(nivion(iz0-1,j).gt.1.and.ab(j).gt.0.d0) WRITE(16,884) 
     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-2,j)+1,nivcum(iz0-1,j))
        if(ab(j).gt.0.d0) WRITE(16,293) 
     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-1,j)+1,nivcum(iz0,j))
                         if(ifich.ge.4) then
        if(j.lt.5) WRITE(22,883) iz(j),(t5(i),i=1,iz0+1)
        if(j.ge.5) WRITE(22,287) iz(j),(t5(i),i=1,iz0+1)
        if(nivion(iz0-4,j).gt.1.and.ab(j).gt.0.d0) WRITE(22,887)
     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-5,j)+1,nivcum(iz0-4,j))
        if(nivion(iz0-3,j).gt.1.and.ab(j).gt.0.d0) WRITE(22,886)
     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-4,j)+1,nivcum(iz0-3,j))
        if(nivion(iz0-2,j).gt.1.and.ab(j).gt.0.d0) WRITE(22,885)
     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-3,j)+1,nivcum(iz0-2,j))
        if(nivion(iz0-1,j).gt.1.and.ab(j).gt.0.d0) WRITE(22,884) 
     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-2,j)+1,nivcum(iz0-1,j))
        if(ab(j).gt.0.d0) WRITE(22,293) 
     &  (dens(nij)/nht/ab(j),nij=nivcum(iz0-1,j)+1,nivcum(iz0,j))
                         endif   ! ifich
        enddo   ! j

        if(imp.ne.7.or.ifich.lt.3.or.ifich.gt.8) goto 240                  !deg
        WRITE(16,*)
        WRITE(16,*)' rapport des '//
     &  'abondances fractionnelles a Saha(T) et des pop(n)/popion/saha'
c rapport des abondances fractionnelles a Saha(T) et des pop(n)/popion/saha
        do j=2,nelem
        iz0=iz(j)
           fsaha(1)=1.d-150
           do i=1,26 
              fsaha(i+1)=0.d0
              sompo1(i)=0.d0
           enddo
           sfs=0.d0
           do i=1,iz0
              niv=nivion(i,j)
              if(niv.eq.1) then
                 sompo1(i)=1.d0
              else
                 nij0=nivcum(i-1,j)
                 posaha(1,i)=1.d0
                 sompo1(i)=1.d0
                 do n=2,niv
                    nij=nij0+n
                    posaha(n,i)=gps(nij)/gps(nij0+1)
     &                     *expa(-(pkiev(i,j)-hkin(nij))*tk1)
                    sompo1(i)=sompo1(i) + posaha(n,i)                      !deg
                 enddo   ! n
              endif   ! niv/1
              fsaha(i+1)=fsaha(i)/ne *gfond(i+1,j)/gfond(i,j)
     &                    *expa(-pkiev(i,j)*tk1) /saha
              sfs=sfs + fsaha(i) *sompo1(i)
                                  if(fsaha(i+1).gt.1.d200) then
                                     do ii=1,i+1
                                        fsaha(ii)=fsaha(ii)/1.d100
                                     enddo
                                     sfs=sfs/1.d100
                                  endif
           enddo   !i
           sfs=sfs + fsaha(iz0+1)
           do i=1,iz0
                   rsaha(i)=0.d0
                   if(fsaha(i)*sompo1(i).gt.0.d0.and.
     &             (log10(r(i,j))+log10(sfs)-log10(fsaha(i))).le.300.)
     &        rsaha(i)=r(i,j)/(fsaha(i)*sompo1(i)) *sfs      ! N(i,j)/Nijsaha
              t5(i)=max(rsaha(i),1.d-99)
           enddo   !i
                     rsaha(iz0+1)=0.d0
                     if(fsaha(iz0+1).gt.0.d0)
     &        rsaha(iz0+1)=r(iz0+1,j)/fsaha(iz0+1)*sfs
              t5(iz0+1)=max(rsaha(iz0+1),1.d-99)
           if(j.lt.5) WRITE(16,883) iz(j),(t5(i),i=1,iz0+1)
           if(j.ge.5) WRITE(16,287) iz(j),(t5(i),i=1,iz0+1)
           do 241 i=1,iz0
              niv=nivion(i,j)
              if(niv.eq.1) goto 241
              nij1=nivcum(i,j) + 1
              do n=1,niv
                 nij=nivcum(i-1,j) + n                                     !deg
                 t5(n)=0.d0
                 if(fsaha(i)*posaha(n,i)*dens(nij1).gt.0.d0) t5(n)=
     &           dens(nij)/dens(nij1) *fsaha(i+1)/fsaha(i)/posaha(n,i)
              enddo
              if(i.eq.iz0-4) WRITE(16,887) (t5(n),n=1,niv)
              if(i.eq.iz0-3) WRITE(16,886) (t5(n),n=1,niv)
              if(i.eq.iz0-2) WRITE(16,885) (t5(n),n=1,niv)
              if(i.eq.iz0-1) WRITE(16,884) (t5(n),n=1,niv)
              if(i.eq.iz0)   WRITE(16,293) (t5(n),n=1,niv)
  241      continue

        enddo   ! j
  883   format(' z=',i2,1x,8(1pe9.2))
  287   format(' z=',i2,1x,8(1pe9.2)/6x,8(1pe9.2)/6x,8(1pe9.2)/6x,
     1           8(1pe9.2))
  293   format(' hydr=',7(1pe9.2))
  884   format(' heli=',8(1pe9.2))
  885   format(' lith=',8(1pe9.2))
  886   format(' bery=',8(1pe9.2))
  887   format(' bore=',8(1pe9.2))
                                  endif                     ! imp
 240                              continue
        if(imp.ne.7.or.ifich.lt.3.or.ifich.gt.8) goto 250
        WRITE(16,288)
        if(ietl.eq.1) WRITE(16,289)                                        !deg
        WRITE(16,290)
  288   format (/21x,'termes des equations d ionisation')
  289   format (' pres ETL : radsec=dielec=0 mais on donne les',
     1     ' valeurs de ce qui a ete neglige')
  290   format (7x,'ionisations',13x,' recombinaisons',27x,'i ou r'/
     2     ' z i n collis  tphot/ne Auger   radfond  radsec  dielect ',
     3     'triples induit ec.charg')
        do 251 j=1,nelem
           iz0=iz(j)
           z=dfloat(iz0)
           i1=jmp1(j)
           i2=jmp2(j)
c        do 251 i=i1,i2
        do 251 i=1,iz0
           nij0=nivcum(i-1,j)
           niv=nivion(i,j)
           salfn=0.d0
        do 251 n=1,niv
           nij=nij0+n
        x2=tphot(nij)/ne
        x4=alfint(nij)
        x8=alfinduit(nij)
                salfn=salfn+x4
                if(n.gt.1) then
                gn=gps(nij)
        x1=cioe(nij)*expa(-hkin(nij)*tk1)
                x5=0.d0                                                   !deg
                x6=0.d0
        x7=ne*cioe(nij)*gn*saha
                x9=0.d0
                else   ! n=1
        x1=cio(nij)
                if(i.eq.iz0) x1=cioe(nij)*expa(-hkin(nij)*tk1)
        x5=alfrad(i,j)-x4
        x6=alpha(i,j)-alfrad(i,j)
                uu=gfond(i,j)/gfond(i+1,j)
        x7=ne*cioe(nij)*uu*saha
        x9=ph0ne*ech(i,j)
                endif
                if(niv.gt.1.and.n.eq.niv) then
        x5=alfrad(i,j)-salfn
        x6=alpha(i,j)-alfrad(i,j)
                endif
                x3=0.d0
                if(j.ne.1.and.i.ne.1.and.n.eq.1.and.i.le.(iz0-2).and.
     1  f(i,j).gt.0.d0) x3=(f(i-1,j)/f(i,j))*tphotk(i-1,j)/ne
        if(j.eq.10.and.i.ne.1.and.i.le.(iz0-1)) x3=x3*(1.d0-pfluo(i-1))
                if(j.eq.1.and.i.eq.1) then
                   if(n.eq.1) x9=sech1
                   if(n.eq.2) x9=sech2
                endif
                if (j.eq.2.and.i.eq.1.and.n.eq.1) x9=seche1
                if (j.eq.2.and.i.eq.2.and.n.eq.1) x9=seche2
c if (ietl.eq.0) x6=x5=0 mais on donne les valeurs de ce qui a ete neglige
        WRITE(16,291) iz(j),i,n,x1,x2,x3,x4,x5,x6,x7,x8,x9
  251   continue
  291   format(i2,2i2,2(1pe8.1,1pe9.2),5(1pe8.1))                          !deg
        WRITE(16,*)' termes d autoionisation dus aux photons UTA'//
     &             ' (se comparent aux tphot/ne)'
        do j=1,nelem
           iz0=iz(j)
              WRITE(16,'(8(1pe9.2))') (utanion(i,j)/ne,i=1,iz0)
        enddo
c
        WRITE(16,*)
        WRITE(16,*)'                                en nombre/ne'
        WRITE(16,290)
        do 261 j=1,nelem
           iz0=iz(j)
           z=dfloat(iz0)
           i1=jmp1(j)
           i2=jmp2(j)
        do 261 i=1,iz0
           nij0=nivcum(i-1,j)
           nij1=nivcum(i,j) + 1
           niv=nivion(i,j)
           salfn=0.d0
        do 261 n=1,niv
           nij=nij0+n
        x2=tphot(nij)/ne*dens(nij)
        x4=alfint(nij)*dens(nij1)
        x8=alfinduit(nij)*dens(nij1)
                salfn=salfn+x4
                if(n.gt.1) then
                gn=gps(nij)
        x1=cioe(nij)*expa(-hkin(nij)*tk1)*dens(nij)
                x5=0.d0
                x6=0.d0
        x7=ne*cioe(nij)*gn*saha*dens(nij1)
                x9=0.d0
                else   ! n=1                                               !deg
        x1=cio(nij)*dens(nij)
                if(i.eq.iz0) x1=cioe(nij)*expa(-hkin(nij)*tk1)*dens(nij)
        x5=alfrad(i,j)*dens(nij1)-x4
        x6=(alpha(i,j)-alfrad(i,j))*dens(nij1)
                uu=gfond(i,j)/gfond(i+1,j)
        x7=ne*cioe(nij)*uu*saha*dens(nij1)
        x9=ph0ne*ech(i,j)*dens(nij1)
                endif
                if(niv.gt.1.and.n.eq.niv) then
        x5=alfrad(i,j)*dens(nij1)-salfn
        x6=(alpha(i,j)-alfrad(i,j))*dens(nij1)
                endif
                x3=0.d0
                if(j.ne.1.and.i.ne.1.and.n.eq.1.and.i.le.(iz0-2).and.
     1  f(i,j).gt.0.) x3=(f(i-1,j)/f(i,j))*tphotk(i-1,j)/ne*dens(nij)
        if(j.eq.10.and.i.ne.1.and.i.le.(iz0-1)) x3=x3*(1.d0-pfluo(i-1))
                if(j.eq.1.and.i.eq.1) then
                   if(n.eq.1) x9=sech1*dens(nij)
                   if(n.eq.2) x9=sech2*dens(nij1)
                endif
                if (j.eq.2.and.i.eq.1.and.n.eq.1) x9=seche1*dens(nij)
                if (j.eq.2.and.i.eq.2.and.n.eq.1) x9=seche2*dens(nij1)
        WRITE(16,291) iz(j),i,n,x1,x2,x3,x4,x5,x6,x7,x8,x9
 261   continue
 250   continue
       return
       end                                                             !degion
c------------------------------------------------------------------------------
       subroutine PHOCONT(kc)
c
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15)
         character tab*1
         double precision ne,nht
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/temp/T,VT,tk,tk1,templog
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/deglosm/pnenht,praieint,r(27,10)
       common/stdalf/tekc(220,nivtots)
       common/degmt/hautsom,dtaucmax,hnumaxto,
     &          p_grait,p_gfree,p_gbound,g_pcompt,ptot,
     &          restflux(322),rtaucef(322),ttaucef(322),rtaucat(322)
       common/degmt2/exdtauc(322,0:nzs+1),exdtaur(16,nrais,0:nzs+1)
       common/talfdeg/sigmac(220,nivtots),sigmacv(220,nivtots),
     &          sigmack(220,nivtots),sigmark(nrais,nivtots)
       common/im/fii,itali
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2)
       common/cpal/temcac(322,nzs),tcapc(322,nzs),tcth(nzs),temc(322),
     &             pradc(nzs)
       common/stpo/tphot(nivtots),tphotk(26,10),dens(nivtots),
     &          alfinduit(nivtots),zfree,cthomson
       common/datdeg/alpha(26,10),alfdi(26,10),edi(26,10),cio(nivtots),
     &          ech(26,10),eche(26,10),alfrad(26,10),
     &          teec(220),tgaufrec(220)
       common/stdeg/popsec(26,10),dpech(26,10),ecartdeg,nitdmax,
     &              jmp1(10),jmp2(10)
       common/stpho/dabsh,demh,dabshep,demhep,dic(322)
       common/phopg/absfree,cemfree,sabsa,dabsb,sema,demb,demh2s
c phocont
c        tk=ckev*T
        saha=2.07079d-16/T/VT
          hnuev=thnuc(kc)
          de=tde(kc)
          deb=tdeb(kc)
          dea=de-deb
                        if(kc.le.102) then
c ces frequences sont egales a des energies d'ionisation du fondamental d'ions
          ic=ict(kc)
          jc=jct(kc)
                        endif
c calcul du coef d'absorption et de diverses sommes bound-free pour hnu<26kev
        sabsa=0.d0
        dabsb=0.d0
        sema=0.d0
        demb=0.d0
        do 513 jn=1,nelem
           inz0=iz(jn)
           i1=jmp1(jn)-1
           if(i1.le.0) i1=1
           i2=jmp2(jn)
        do 513 in=i1,i2
           niv=nivion(in,jn)                                               !pc
           nij0=nivcum(in-1,jn)
           nijp=nivcum(in,jn)+1
        do 513 nn=1,niv
           nij=nij0 + nn
           akiev=hkin(nij)
c
c        if((kc.eq.1).and.m.eq.1.and.kitd.eq.1. !23456
c     &  and.nitf.eq.1.and.nitt.eq.1)write(73,'(i4,9i3,0p3f8.2,1pe11.3)')
c     &  kc,jn,in,nn,nij,niv,jmp1(jn),jmp2(jn),i1,i2,hnuev,akiev,
c     &  gps(nij),r(in,jn)
c
        if(hnuev+deb.le.akiev) go to 513
           ee1=tekc(kc,nij)                           ! exp((akiev-hnuev)*tk1)
           uu=gps(nij)/gfond(in+1,jn)
           rami=dens(nij) - ne*dens(nijp)*uu*ee1*saha                !ee/bij
           ria=dens(nijp)
           rr=dens(nij)
                        if(rami.lt.0.d0) rami=0.d0        ! effet laser ? zzz
        dabs=sigmac(kc,nij)*rami
        dem=ria*sigmacv(kc,nij)*uu*ee1*hnuev**3 *saha*2.08451d-4  ! 2e'3/(c2h2)
            if(kc.le.102.and.jc.eq.jn.and.ic.eq.in.and.nn.eq.1) then
                dabsb=dabsb+dabs
                if(tk.lt.deb) dem=dem*tk/deb
                demb=demb+dem                                               !pc
                if(kc.eq.1) dabsh=dabs
                if(kc.eq.1) demh=dem
            else if(kc.eq.1.and.jn.eq.2.and.in.eq.2.and.nn.eq.2)then ! khi=13.6
                   dabsb=dabsb+dabs                              ! He+ niveau 2
                   demb=demb+dem
                   dabshep=dabs
                   demhep=dem
            else if(nn.gt.1.and.akiev.ge.hnuev.and.kc.le.102) then
                   dd=min(tk,hnuev+deb-akiev)
                   dabsb=dabsb+dabs*(hnuev+deb-akiev)/deb
                   demb=demb+dem*dd/deb
            else if(nn.gt.1.and.akiev.ge.hnuev-dea) then
                   dd=min(tk,hnuev+deb-akiev)
                   sabsa=sabsa+dabs*(hnuev+deb-akiev)/de
                   sema=sema+dem*dd/de
            else                                         ! cas normal
        sabsa=sabsa+dabs
                if(tk.lt.de) dem=dem*tk/de
        sema=sema+dem
            endif   ! ic jc

c        if((kc.eq.1.or.kc.eq.185).and.m.eq.1.and.kitd.eq.1. !234567
c     &     and.nitf.eq.1.and.nitt.eq.1)
c     & write(73,'(i3,2i3,0pf8.2,0pf7.3,1p5e11.3)')kc,jn,nij,akiev,uu,rr/!234567
c     & nht/ab(jn),rami/nht/ab(jn),ria/nht/ab(jn),sigmac(kc,nij),dabs/nht

c        if(kc.eq.3.and.(nitd.eq.1.or.kitd.eq.1))
c     &  write(6,'(2i3,1pg10.3,1pg10.3,1pg10.3,                             !pc
c     &  1pg10.3,1pg10.3)') jn,in,rr,r(inz0+1,jn),rami,ee1,uu
c        if(kitd.eq.1.and.rr.gt.0.1) WRITE(10,'(3i3,i4,1pg12.5,2i3,
c     &  3(1pe10.3))') nitt,jc,ic,kc,hnuev,jn,in,rr,sigmac(kc,nij),dabs
c        if(m.eq.100.and.kitt.eq.1.and.kitn.eq.1.and.ic.eq.in.and.nn.eq.1
c     &  .and.jc.eq.jn.and.(kc.eq.1.or.kc.eq.3.or.kc.eq.8.or.kc.eq.94.or.
c     &  kc.eq.102.or.kc.eq.103)) WRITE(10,7771)
c     &          kc,hnuev,ab(jc),r(ic,jc),rami,sigmac(kc,nij),dabs,sabsa,ee1,
c     &          ic,r(ic+1,jc),dem,sema,tk,de,deb,nht,ne
c
                if(kimpf2.eq.0) goto 513
        if(imp.eq.3.and.kitd.eq.1.and.kitt.ge.1.and.kimpm.eq.1.and.ifich
     &     .ge.3.and.ifich.le.8.and.(kc.eq.1.or.kc.eq.76.or.kc.eq.101))
     &     WRITE(16,582) hnuev,jn,in,sigmac(kc,nij),rr,dabs,dem*ne
c  515   continue                                                ! nn
c  514   continue                                                ! jn
  513    continue                                                ! in
  582    format(' hnu=',0pf8.2,2i3,' sig=',1pe9.2,'  r=',1pe9.2,
     &                '   nsig=',1pe9.2,' eps=',1pe9.2 )
chnu= 2345.56 10 24 sig= 2.45E+89  r= 2.45E+89   nsig= 2.45E+89 eps= 2.45E+89
c
c calcul des cappas : contribution free-free et diffusion Thomson et
c calcul de l'emissivite (y compris le continu 2 photons du au niveau 2s de H)
c a: Avant discontinuite -  b: apres le Bord de la discontinuite
c capabsa,emma : avant discontinuite - capabs,emm : apres la discontinuite
                ee=teec(kc)
                cfree=zfree*tgaufrec(kc)                !Som(n(q)*q2) *gaunt/VT
        absfree=(1.d0-ee)/hnuev/hnuev/hnuev*cfree*2.6076d-35*nht    ! b0kh3/e'3
        cemfree=ee*cfree *5.435d-39*nht                             ! 2b0kh/c2
c continu 2 photons venant du niveau 2s de H
        demh2s=0.d0
        if(hnuev.lt.10.2d0) demh2s=dens(2)*8.23d0 *6.6262d-27 /pi4
     &     *12.d0*(hnuev/10.2d0)*(hnuev/10.2d0)*(1.d0-hnuev/10.2d0)/ne
                        if(kc.le.102) then
        capabsa=sabsa + absfree*ne
        dtoa=dhaut*V3*(capabsa + cthomson)                                  !pc
        exdtoa=expa(-dtoa)
        capabs=capabsa + dabsb
        dto=dhaut*V3*(capabs + cthomson)
        exdto=expa(-dto)
        emma=(sema + cemfree + demh2s)*ne
        emm=emma + demb*ne
        exdtauc(kc,m)=exdto
        exdtauc(kc+220,m)=exdtoa
        tcapc(kc,m)=capabs
        tcapc(kc+220,m)=capabsa
        temc(kc)=emm
        temc(kc+220)=emma
        pexdto=exdtauc(kc,m-1)
        pexdtoa=exdtauc(kc+220,m-1)
                        else        ! kc compris entre 103 et 184
        capabs=sabsa + absfree*ne
        dto=dhaut*V3*(capabs + cthomson)
        exdto=expa(-dto)
        emm=(sema + cemfree + demh2s)*ne
        exdtauc(kc,m)=exdto
        tcapc(kc,m)=capabs
        temc(kc)=emm
        pexdto=exdtauc(kc,m-1)
                capabsa=0.d0
                emma=0.d0
                dtoa=0.d0
                exdtoa=1.d0
                pexdtoa=1.d0
                        endif        ! kc                                   !pc

c calcul du J et du dic=4piJdnu/hnu en vue du nombre de photoionisations
c le choix du 1er tour est important
                        if(itali.eq.0.and.isuite.le.1) then
! indispensable pour nitf=1
             ai=pciplus(kc)
             fi=fii
             if(nitf.eq.1.and.isuite.le.1) then
                if(hnuev.ge.1.d3) then
                   fi=0.d0
                   bi=0.d0
                else
                   fi=fii
                   bi=(ai + debim(kc)) *fii
                endif    ! hnuev>1000
             endif   ! nitf=1
        hj=FJ(exdto,pexdto,capabs,emm,cthomson,ai,bi,fi)
           if(hj.lt.0.d0) hj=0.d0
           tjc(kc,m)=hj
                if(kc.le.102) then
        dic(kc)=hj*deb *pi4/hnuev /6.6262d-27
             ai=pciplus(kc+220)
                if(nitf.eq.1.and.isuite.le.1.and.hnuev.lt.1.d3)
     &          bi=(ai+debim(kc+220))*fii
        hja=FJ(exdtoa,pexdtoa,capabsa,emma,cthomson,ai,bi,fi)
           if(hja.lt.0.d0) hja=0.d0
           tjc(kc+220,m)=hja
c                hjde=hja*dea + hj*deb
        dic(kc+220)=hja*dea *pi4/hnuev /6.6262d-27
                else   ! kc>102
        dic(kc)=hj*de *pi4/hnuev /6.6262d-27
                hja=0.d0
c                hjde=hj*de
                endif        ! kc
                        else   ! itali=1 (nitf>1)
                if(kc.le.102) then
                hj=tjc(kc,m)
                if(hj.lt.0.d0) hj=0.d0
        dic(kc)=hj*deb *pi4/hnuev /6.6262d-27
                hja=tjc(kc+220,m)
                if(hja.lt.0.d0) hja=0.d0
        dic(kc+220)=hja*dea *pi4/hnuev /6.6262d-27
c                hjde=hja*dea + hj*deb
                else   ! kc>102
                hj=tjc(kc,m)
                if(hj.lt.0.d0) hj=0.d0
        dic(kc)=hj*de *pi4/hnuev /6.6262d-27                                !pc
                hja=0.d0
c                hjde=hj*de
                endif   ! kc
                        endif   ! nitf/itali
c
        if(kc.eq.1) dtaucLy=dto
                if(dto.gt.dtaucmax.and.hnuev.gt.1.d0) then
        dtaucmax=dto
        hnumaxto=hnuev
        endif
c
        if(kimpf.eq.1.and.kitt.eq.1.and.kitn.eq.1.and.kitd.eq.1) then
          if(ifich.eq.10.and.(m.eq.1.or.m.eq.npt.or.mod(m,5).eq.0))then
        WRITE(28,*)' no strate,z/abondance fer/kappas-continu'
        WRITE(28,'(i4,1pe10.3)') m,tz(m)
        WRITE(28,'(8(1pe10.3))') (r(i,10),i=1,27)
        WRITE(28,'(8(1pe10.3))') (tcapc(k,m),k=1,322)
           endif   ! ifich=10
           if(ifich.eq.11) then
        WRITE(28,*)' no strate,z'
        WRITE(28,'(i4,1pe10.3)') m,tz(m)
        WRITE(28,*)' abondance relative des ions'                          !pc
        do j=1,nelem
        iz1=iz(j)+1
        WRITE(28,'(8(1pe10.3))') (r(i,j),i=1,iz1)
        enddo
        WRITE(28,*)' kappas-continu'
        WRITE(28,'(8(1pe10.3))') (tcapc(k,m),k=1,322)
        WRITE(28,*)' 4piJdnu/hnu continu'
        WRITE(28,'(8(1pe10.3))') (dic(k),k=1,322)
           endif   ! ifich=11
        endif   ! kit et nitf
c
       return
       end                                                             !phocont
c T VT V3 pi4 ne cfree dhaut itali isuite fii
c thnuc(kc) tde(kc) tdeb(kc) ict(kc) jct(kc) iz(j) jmp1(j) jmp2(j)
c nivion(i,j) nivcum(i,j) hkin(nij) gps(nij) gfond(i,j) tgaufrec(kc)
c dens(nij) sigmac(kc,nij) sigmacv(kc,nij) tekc(kc,nij) teec(kc) exdtauc(kc,m)
c tcapc(kc,m) temc(kc) pciplus(kc) debim(kc) tjc(kc,m) dic(kc)
c dtaucLy dtaucmax hnumaxto  m tz(m) r(i,j) 
c------------------------------------------------------------------------------
       subroutine PGCONT(kc,jc,ic)
c 
c pertes et gains du continu - heating and cooling of CONTINUUM
c bound-free, free-free et Compton
c   perte=4pi*emissivite*dnu/(Ne*Nh) somme sur i,j,nu
c   gain=4pi*coef.absorption*J*dnu/(Ne*Nh) somme sur i,j,nu
c
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15)
         character tab*1
         double precision ne,nht
       common/imp/t1(6),t2(6),t3(6),t4(6),t5(27),mimp
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/temp/T,VT,tk,tk1,templog
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/abond/ab(10)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/tabm/exflux(322),thnur(nrais)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),                                    !pgc
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2)
       common/deglosm/pnenht,praieint,r(27,10)
       common/degmt/hautsom,dtaucmax,hnumaxto,
     &          p_grait,p_gfree,p_gbound,g_pcompt,ptot,
     &          restflux(322),rtaucef(322),ttaucef(322),rtaucat(322)
       common/datdeg/alpha(26,10),alfdi(26,10),edi(26,10),cio(nivtots),
     &          ech(26,10),eche(26,10),alfrad(26,10),
     &          teec(220),tgaufrec(220)
       common/dastpo/alfint(nivtots),cioe(nivtots),
     &          texcoll(nrais,nzs+1),teer(nrais,nzs)
       common/stpo/tphot(nivtots),tphotk(26,10),dens(nivtots),
     &          alfinduit(nivtots),zfree,cthomson
       common/cpal/temcac(322,nzs),tcapc(322,nzs),tcth(nzs),temc(322),
     &             pradc(nzs)
       common/stdeg/popsec(26,10),dpech(26,10),ecartdeg,nitdmax,
     &              jmp1(10),jmp2(10)
       common/phopg/absfree,cemfree,sabsa,dabsb,sema,demb,demh2s
       common/stpg/pfree,gfree,precf,gion,precsec,pdiel,pech,
     &             scompt1,scompt2,sjdnuev
c pgcont
          cind=hcse*hcse/chel/2.d+16
          hnuev=thnuc(kc)
          de=tde(kc)
          deb=tdeb(kc)
          dea=de-deb
          if(kc.eq.1) mimp=0
c calcul des pertes par recombinaisons dielectroniques et sur les niveaux secon
c -daires (formule de Canfield-Ricchiazzi 1980 cite dans Collin-Dumont A&A1989)
c (ici sommation sur les ions et non integration sur les frequences)
c pour tous les ions :
c il faut prendre en compte l'energie "perdue" ion+electron-dernier_niveau
        if(ietl.eq.0.and.kc.le.102) then
           rt=rtaucef(kc)
           ttt=(3.d0*rt/(hnuev*tk1))**0.25d0               ! hnuev=pkiev=kifond
           tt=1.d0
           if(ttt.gt.1.d0) tt=ttt
           rbetamoy=0.5d0*expa(-rt/tt/tt/tt-hnuev*tk1*(tt-1.d0))/tt
           if(rt.lt.1.d-4) rbetamoy=0.5d0
                sbetamoy=0.d0
                if(ttaucef(kc).gt.0.d0) then
                st=ttaucef(kc)-rtaucef(kc)
                if(st.lt.0.d0) st=0.d0
                   ttt=(3.d0*st/(hnuev*tk1))**0.25d0
                tt=1.d0
                if(ttt.gt.1.d0) tt=ttt
                sbetamoy=0.5d0*expa(-st/tt/tt/tt-hnuev*tk1*(tt-1.d0))/tt!234567
                if(st.lt.1.d-4) sbetamoy=0.5d0
                endif
           betamoy=rbetamoy+sbetamoy                                       !pgc
c
              nivc=nivion(ic,jc)
              nij0c=nivcum(ic-1,jc)
              salfn=0.d0
           do n=1,nivc
              salfn=salfn + alfint(nij0c+n)
           enddo   ! n
c           alf=alpha(ic,jc)-salfn          ! contient les dielectroniques
           alf=alpha(ic,jc)-alfdi(ic,jc)-salfn
              if(alf.le.0.d0) alf=0.d0
c contient les recomb.rad. sur les niv.superieurs & les diel.basse temperature
                             if(nivc.eq.1) then
              kr1=nrcum(ic-1,jc)+1
           hnudif=hnuev-thnur(kr1)                     ! hnuev=pkiev(ic,jc)
                             else
           hnudif=hkin(nij0c+nivc)    ! pas tout a fait vrai si irecsup=2
                             endif
        dprecsec=ab(jc)*r(ic+1,jc)*(hnudif+tk)*alf*betamoy*chel   ! e'
        precsec=precsec + dprecsec
        dpdiel=0.d0
        if(ic.ne.iz(jc)) dpdiel=ab(jc)*r(ic+1,jc)
     &               *(hnudif*alfdi(ic,jc) + edi(ic,jc))*betamoy*chel
        pdiel=pdiel + dpdiel
        pech=pech + dpech(ic,jc)*betamoy
c        emoy=0.d0
c        if(alfdi(ic,jc).gt.0.d0) emoy=edi(ic,jc)/alfdi(ic,jc)
c        if(m.eq.10.and.kitf.eq.1)
c     &  write(6,'(2i3,1p5e11.3,0p3f8.2,1p4e11.3)') ic,jc,r(ic+1,jc),
c     &   alfrad(ic,jc),alf,alfdi(ic,jc),edi(ic,jc),hnuev,hnudif,emoy,
c     &   dprecsec,dpdiel,ab(jc)*r(ic+1,jc)*edi(ic,jc)*betamoy*chel
        endif   ! kc<=102
c
c calcul des pertes et gains ionisations-recombinaisons et free-free
        dpertefree=cemfree*de/nht *3.0385d15                        !4pie'/h
                        if(kc.le.102) then
           hjde=tjc(kc+220,m)*dea + tjc(kc,m)*deb
           hj=hjde/de
        dgainfree=absfree*hjde/nht *3.0385d15
        dperte=(sema*de + demh2s*de + demb*deb)/nht *3.0385d15
        dgainion=(sabsa*hjde+dabsb*tjc(kc,m)*deb)/nht/ne *3.0385d15        !pgc
                        else        ! kc > 102
           hjde=tjc(kc,m)*de
           hj=tjc(kc,m)
        dgainfree=absfree*hjde/nht *3.0385d15
        dperte=(sema*de + demh2s*de)/nht *3.0385d15
        dgainion=sabsa/nht/ne *hjde *3.0385d15
                        endif        ! kc
        pfree=pfree + dpertefree
        gfree=gfree + dgainfree
        precf=precf + dperte
        gion=gion + dgainion

c        if(m.eq.1.and.nitf.eq.1.and.nitt.eq.1)write(74,'(i4,8(1pe9.2))')
c     &     kc,dea,deb,tjc(kc,m),tjc(kc+220,m),sema,sabsa,demb,dabsb,
c     &     dperte,dgainion
c        if(dgainion.gt.1.d-24) write(6,*) m,nitt,nitd,kc,dgainion
c   calcul de J/B
c        cjsb=0.d0
cOK       if(ee.gt.1.e-200) cjsb=hj/hnuev**3*(1.d0/ee-1.d0)*4797.3 !h2c2/(2e'3)
c calcul du chauffage Compton                               J=Jinterne+Fexterne
c   formule de Tarter (communication privee, vers 1985) + Ferland Rees 1988
c   chauf-pert/Hnenh=(sigth/mec2)*(1/nht)*(somme de Fnu*dnu)*4k*(Tcompt-T)
c Tcompt=somme de (alfa*hnu*J)/4k*somme de (alfa*J*(1-alfa*hnu(a1+2*a2*hnu)/4))
c   avec alfa=1/(1+a1*hnu+a2*hnu**2)   a1=1.1792e-4/13.6   a2=7.084e-10/13.6^2
        altar=1.d0/(1.d0+ hnuev*8.6675d-6 + hnuev*hnuev*3.827d-12)
        scompt1=scompt1 + hjde *altar*hnuev 
     &                   *(1.d0 + hj/(hnuev**3)*cind*(1.d0-teec(kc)))
        scompt2=scompt2 + hjde *altar*
     1          (1.d0- altar*hnuev*(8.6675d-6 + hnuev*7.655d-12)/4.d0)
        sjdnuev=sjdnuev + hjde
c
                if(kitt.eq.0.or.kimpf2.eq.0) goto 511
c        if(kimpm.eq.1)print'(i4,f10.2,1p5e12.4)',kc,hnuev,hj,
c     & hj/(hnuev**3)*cind*(1.d0-teec(kc)),hj*hnuev,hj*hj/(hnuev**2)*cind
c     & ,hj*hj/(hnuev**2)*cind*(1.d0-teec(kc))
                if(kimpm.eq.0.or.imp.lt.5.or.imp.gt.7) goto 511
                if(ifich.lt.3.or.ifich.gt.8) goto 511
        if(dperte/de.lt.1.d-27.and.dpertefree/de.lt.1.d-27
     &          .and.dgainion/de.lt.1.d-27) goto 511
        mimp=mimp+1
        jimp=2-mod(mimp,2)
        t1(jimp)=hnuev
        t2(jimp)=dhaut*V3*(tcapc(kc,m) + cthomson)
        t3(jimp)=dgainion/de
        t4(jimp)=dperte/de
        t5(jimp)=dpertefree/de
        if(jimp.eq.2)
     &          WRITE(16,581) (t1(k),t2(k),t3(k),t4(k),t5(k),k=1,2)
  581   format(0pf9.1,1pe8.1,1pe8.1,1pe8.1,1pe7.0,
     &         0pf8.1,1pe8.1,1pe8.1,1pe8.1,1pe7.0)
  511        continue
c           txt='cont1'
c        if(kitd*kitt*kimpm.eq.1.and.kimpf2.eq.1)
c     &     WRITE(10,'(a5,2i4,6(1pe11.3))') txt,nitf,m,sbt,skbt,
c     &     sjdnuev*esh*pi*2.d0,sfdnu*esh*pi,skfdnu*esh*pi,s2jdnu*esh*pi
c
             hnne=nht*ne
        if(imp.eq.3.and.kitt.ge.1.and.hnuev.gt.3..and.hnuev.lt.4.5
     &     .and.kimpf2.eq.1) write(16,585) absfree*hnne,cemfree*hnne,
     &     dhaut*V3*(tcapc(kc,m)+cthomson),(sema+cemfree)*hnne             !pgc
  585   format(43x,' absfree=',1pe9.2,' epsfree=',1pe9.2/' dtauc=',
     &        1pe9.2,' SEPS=',1pe9.2)
c
        return
        end                                                             !pgcont
c tk1 ab(j) chel nht ne sema demb sabsa dabsb absfree cthomson
c nivion(ic,jc) nivcum(ic,jc) nrcum(ic,jc) alfint(nij) alpha(ic,jc) hkin(nij)
c thnuc(kc) tde(kc) tdeb(kc) rtaucef(kc) ttaucef(kc) thnur(kr)
c r(i,j) dpech(i,j) tjc(kc,m) teec(kc) hj pi4
c pfree gfree precf gion scompt1 scompt2 sjdnuev cind
c--============================================================================
       subroutine cPALETOU
c
c TWO LEVEL ATOM IN A SEMI-INFINITE MEDIUM 1D!!!        F.Paletou le 1-11-1999
c     ITERATIVE SOLUTION OF  S = (1-EPS)*LAMBDA(S) + EPS*B
c     Simple ALI (Olson, Auer & Buchler) code (2-level, CRD)
c
c     envoye par F.Paletou le 12 nov 1999 - modifie pour le continu (ncfrq=1)
c     ndirs angles (1,3 ou 5) cos(angle)=csz  0<csz<1     sum (wtdir) = 0.5
c     pour l'acceleration nord=3
c
        implicit double precision (a-h,o-z)
       parameter (nzs=999,ncfrqs=1,ndirs=20,nord=3,nrais=4200)
          logical ng,accel,a0
          character txt*11,tab*1
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/tabm/exflux(322),thnur(nrais)
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2)
       common/fsource/sourcec(322,nzs+1),sourcer(16,nrais,nzs+1)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/cpal/temcac(322,nzs),tcapc(322,nzs),tcth(nzs),temc(322),
     &             pradc(nzs)
       common/mgcpal/cimoinsh(322),ciplush(322),cimoins0(322),
     &               fcra(ndirs,322),fcsa(ndirs,322)
c
      double precision kappa(nzs),lstar(nzs),jayx(ncfrqs,nzs)
      dimension chil(nzs),chie(nzs),s(ncfrqs,nzs),phij(nzs),
     &          bb(nzs),eps(nzs),epsm1(nzs),epsb(nzs),
     &          bc0(ncfrqs,ndirs),bcd(ncfrqs,ndirs)
      dimension zerobc(ncfrqs,ndirs),xlambda(ncfrqs,nzs)
      dimension sl(ncfrqs,nzs),ss(nzs),p(nzs*(nord+1))
        dimension ps(nzs),pphij(nzs),pdpradc(nzs)
      common/cquad2/f0s4pi,fHs4pi,fe,fra(ndirs),fsa(ndirs),dpradc(nzs)
      common/cquad/phic(ncfrqs),wctnu(ncfrqs),wtnuc(ncfrqs),
     .             ncfrq,nfcore
      common/cosw/csz(ndirs),wtdir(ndirs)
         save /cpal/,/mgcpal/,/cquad/,/cquad2/,/cosw/
c cpaletou
       nz=npt
       ncfrq=1
       phic(1)=1.d0
       wctnu(1)=1.d0
       do m=1,npt
          pradc(m)=0.d0
       enddo
c
                       if(kimpf.eq.1.and.ifich.ge.5.and.ifich.le.8)
     &                 write(29,*) ' PALETOU-continu: nitf=',nitf
c
                              do kc=1,322
       fe=exflux(1)
       do 1030 m = 1, npt
          bb(m) = temcac(kc,m)                          ! emm/capabs
          chil(m) = 0.d0
          kappa(m) = tcapc(kc,m)                        ! capabs
          chie(m) = tcth(m)                             ! ne*0.6652d-24
          eps(m)=kappa(m)/(kappa(m)+tcth(m))
          epsm1(m)=tcth(m)/(kappa(m)+tcth(m))           ! 1-epsilon
          epsb(m)=eps(m)*bb(m)                     ! emm/(capabs+sigmath)=eps"B"
 1030  continue
       do 1120 idir = 1, ndir
          do 1130 ifrq = 1, ncfrq
          if(incang.eq.0) then
                bc0(ifrq,idir)=exflux(kc)/pi
             else if(idir.eq.incang) then                                   !pc
             bc0(ifrq,idir)=exflux(kc)/(4.d0*pi*csz(idir)*wtdir(idir))
             else
                bc0(ifrq,idir)=0.d0
             endif
             bcd(ifrq,idir) = cimoinsh(kc)
             zerobc(ifrq,idir)=0.d0
 1130     continue
 1120  continue
       dslim=1.d-4
       if(nitf.gt.50) dslim=min(1.d-4,eps(1))
c
c INITIALISATION de la fonction source
       do 1031 k = 1, nz
          do 1031 ifrq = 1, ncfrq
             s(ifrq,k) = sourcec(kc,k)
 1031     continue
c
c EVALUATE LSTAR AND XLAMBDA (MONOCHROMATIC DIAGONAL OPERATOR)
c 1er appel :  phij(id) = l(id,id) = lambda(s(jd)=delta function(id,jd))
c Paletou original      CALL cT1D(tz,chil,chie,kappa,s,1,zerobc,zerobc,.false.
c
       CALL cT1D(chil,chie,kappa,s,1,zerobc,zerobc,.false.
     .          ,nz,.true.,lstar,.false.,.false.,xlambda,.true.,kc)
c
         if(kimpf.eq.1) then                                                !pc
            txt='bosse lstar'
            do k=10,nz-10
               if(ifich.ge.5.and.ifich.le.8.and.
     &       (lstar(k)-lstar(k-1))*(lstar(k+1)-lstar(k)).lt.-0.1d0)then
         write(29,'(a11,2i4,1p4e11.3,i4)')txt,kc,k,lstar(k-1),lstar(k),
     &      lstar(k+1),(lstar(k)-lstar(k-1))*(lstar(k+1)-lstar(k)),nitf
                  mbos(k)=1
               endif
            enddo
         endif   ! kimpf
c         if(kc.eq.1.and.kimpf.eq.1)
c     &   write(29,700) 'lstar 1str./5 pour kc=',kc,(lstar(k),k=1,nz,5)
 700     format(a22,i4/(1p7g10.3,1pg9.2))
c
       do 1131 k = 1, nz
          do 1131 ifrq=1,ncfrq
             sl(ifrq,k) = sourcec(kc,k)
 1131        s(ifrq,k) = sl(1,k)
c original sl(ifrq,k)=1.d0 1131 s(ifrq,k)=(1.d0-eps(k))*sl(ifrq,k)+eps(k)*bb(k)
c ou sl(ifrq,k) = exflux(kc)/pi 1131 s(ifrq,k)=epsm1(k)*exflux(kc)/pi+epsb(k)
c                                                                           !pc
c calcul de phij = integral(phic(nu)*lambda nu(s))dnu)
         a0=NG(0,ss,nz,p)     ! pour preparer l'acceleration-original: CALL NG0
         kita=0
c
      do 1135 nita = 1, 100
                            pf0s4pi=f0s4pi
                            pfHs4pi=fHs4pi
                         do k = 1, nz
                            pphij(k)=phij(k)
                            ps(k)=sl(1,k)
                            pdpradc(k)=dpradc(k)
                         enddo
c-1999 solution formelle avec caracteristiques courtes
c original    CALL cT1D(tz,chil,chie,kappa,s,ncfrq,bc0,bcd,.false.,nz,.false.,

      CALL cT1D(chil,chie,kappa,s,ncfrq,bc0,bcd,.false.,nz,.false.,
     .      phij,.false.,.false.,jayx,.false.,kc)
c
       if(kita.eq.1) goto 1136
c
                         kpb=0
                         jpb=0
                         mpb=0
                         mpb1=0
         dsmax=0.d0                                                         !pc
      do 1132 k = 1, nz
         dsk = ((epsm1(k))*phij(k)+epsb(k) - sl(1,k))
     .         /(1.d0-epsm1(k)*lstar(k))
                         pslk = sl(1,k)
         sl(1,k) = sl(1,k) + dsk
c         if(nitf.eq.1.and.nita.eq.1.and.k.eq.1)write(80,*)kc,fe
                      if(nita.gt.1.and.nitf.gt.1.and.
     &                   (sl(1,k).lt.-1.d-5*fe.or.phij(k).lt.-1.d-5*fe
     &                   .or.sl(1,k).gt.1.d4*fe               ! ajout v34m-36a
     &                   .or.(dsk.gt.1.d-5*fe.and.sl(1,k).gt.1.d0*fe
     &                                    .and.dsk.ge.5.d0*pslk)
     &                   .or.(sourcec(kc,k).gt.1.d-5*fe.and.sl(1,k)  !v34n-36c
     &                             .gt.100.d0*sourcec(kc,k) ))) then
                         jpb=jpb+1
                         if(jpb.eq.1) mpb1=k
                         mpb=k
                               if(phij(k).lt.-1.d-5*fe) kpb=1
                               if(sl(1,k).lt.-1.d-5*fe) kpb=2
                               if(sl(1,k).gt.1.d4*fe) kpb=3
                               if(dsk.gt.1.d-5*fe.and.sl(1,k).gt.1.d0*fe
     &                                    .and.dsk.ge.5.d0*pslk) kpb=4
                               if(sourcec(kc,k).gt.1.d-5*fe.and.sl(1,k)
     &                                 .gt.100.d0*sourcec(kc,k)) kpb=5
                      else if(nita.eq.1.and.sl(1,k).lt.0.d0) then    ! v34n-36c
                         sl(1,k)=pslk/2.d0
                      else                             ! pas de gros pb
c
                         if(sl(1,k).lt.0.d0.and.sl(1,k).gt.-1.d-5*fe)
     &                    sl(1,k)=0.d0                       ! ajout v34m-36a
                 if(abs(pslk).gt.1.d-5*fe.and.abs(dsk/pslk).ge.dsmax)
     &           dsmax=abs(dsk/pslk)
         ss(k) = sl(1,k)
                      endif   ! pb                                          !pc
 1132    continue   ! k=strate
c
         if(dsmax.lt.dslim.and.nita.ge.3) kita=1
c
                  if(ifich.ge.5.and.ifich.le.8.and.kpb.gt.1)then !v34m-36a
                  write(29,*)'kc nita mpb pphij psl sl epsm1 epsb lstar'
     &             ,'/kc mpb emissiv kappa chie eps phij s'
                  write(29,775)kc,nita,mpb,pphij(mpb),ps(mpb),sl(1,mpb),
     &             epsm1(mpb),epsb(mpb),lstar(mpb)
c     &                              mpb1,phij(mpb1),ps(mpb1),sl(1,mpb1)!2345678
 775              format('PB:',2i3,i4,1p3e11.3,1p3e11.3)
              write(29,'(i2,2i4,1p7e11.3)')kpb,kc,mpb,kappa(mpb)*bb(mpb)
     &             ,kappa(mpb),chie(mpb),eps(mpb),phij(mpb),s(1,mpb)
                  if(kpb.ge.3) then
                     kita=1
                     do k = 1, nz
                        sl(1,k)=sourcec(kc,k) ! ajout v34n-36c
                     enddo
                     go to 1135                          ! retour a l'it1
                  else
                        f0s4pi=pf0s4pi
                        fHs4pi=pfHs4pi
                     do k = 1, nz
                        phij(k)=pphij(k)
                        sl(1,k)=ps(k)
                        dpradc(k)=pdpradc(k)
                     enddo
                     go to 1136                  ! AM retour a l'it precedente
                  endif
                         endif   ! PB
c
c                      write(2,'(1p7g10.3,1pg9.2)') (sl(1,k), k=1,nz)
                   if(ifich.ge.5.and.ifich.le.8.and.kimpf.eq.1.and.
     &               (jpb.eq.1000.or.kc.eq.1.or.kc.eq.3.or.kc.eq.221
     &               .or.kc.eq.122.or.kc.eq.138.or.kc.eq.126))
     &             write(29,776) kc,nita,dsmax,ss(1),ss(nz/2),ss(nz)
 776               format('kc=',i3,' its=',i3,' dS/Smax=',1pg10.3,
     &             ' S(1-mil-npt)',1p3e11.3)
c
c acceleration of convergence -- methode de Ng                              !pc
      if(iacc.ge.1.and.kita.eq.0.and.nita.lt.97) then
         do 511 k = 1, nz
 511        ss(k) = sl(1,k)
         accel=NG(1,ss,nz,p)
                   if(kimpf.eq.1.and.(jpb.eq.1000.or.kc.eq.1.or.kc.eq.3
     &            .or.kc.eq.221.or.kc.eq.122.or.kc.eq.138.or.kc.eq.126))
     &             then
                      if(ifich.ge.5.and.ifich.le.8.and.accel)
     &                write(29,777) ss(1),ss(nz/2),ss(nz)
 777               format(' acceleration',19x,'S(1-mil-npt)', 1p3e11.3)
                   endif   ! imp
         do 512 k = 1, nz
            sl(1,k) = ss(k)
            if(accel.and.ss(k).lt.0.d0) sl(1,k) = 0.d0
 512     continue
      endif   ! iacc>=1
c
         do 1134 k = 1, nz
            do 1134 ifrq = 1, ncfrq
 1134          s(ifrq,k) = sl(1,k)
c
 1135  continue   ! nita                                                    !pc
 1136  continue
          if(ifich.ge.5.and.ifich.le.8) then
       if(kc.eq.1) write(29,80) nitf,ss(1),ss(nz/2),ss(nz)
       if(kc.eq.3) write(29,81)nitf,kc,ss(1),ss(nz/2),ss(nz*3/4),ss(nz)
       if(kc.eq.20)write(29,81)nitf,kc,ss(1),ss(nz/2),ss(nz*3/4),ss(nz)
          endif
 80   format(' PALETOU-continu: nitf=',i4,' kc=1 S(1-mil-npt)',1p3e11.3)
 81   format('nitf=',i4,' kc=',i3,' S(1-mil-npt)',1p4e11.3)
c      if(kimpf.eq.1.and.(kc.eq.1.or.kc.eq.3.or.kc.eq.221)) then
c         write(29,*) 'fonction source tous les 10 strates pour kc=',kc
c         write(29,'(1p7g10.3,1pg9.2)') (sl(1,k), k=1,nz,10),sl(1,nz)
c      endif
c
c AM:                                      S= (emm+sigmath*J)/(capabs+sigmath)
       ciplush(kc)=fHs4pi*4.d0                             ! sortant/pi
       cimoins0(kc)=f0s4pi*4.d0                            ! reflechi/pi
c       if(ndir.ge.2.and.kimpf.eq.1) then
c          write(34,'(0pf12.5,i4,41(1pe11.3))') thnuc(kc),kc,exflux(kc),
c     &  (fra(idir)*4.d0*pi,idir=1,ndir),(fsa(idir)*4.d0*pi,idir=1,ndir)
c          if(ifich.ge.1.and.ifich.le.8)
c     &    write(33,'(0pf12.5,i4,41(1pe11.3))') thnuc(kc),kc,exflux(kc),
c     &  (fra(idir)*4.d0*pi,idir=1,ndir),(fsa(idir)*4.d0*pi,idir=1,ndir)
c       endif
       do idir=1,ndir
          fcra(idir,kc)=fra(idir)*4.d0*pi
          fcsa(idir,kc)=fsa(idir)*4.d0*pi
       enddo
                       do m=1,npt
       tjc(kc,m)=phij(m)
       sourcec(kc,m)=s(1,m)
             if(kc.le.102) then
                de=tdeb(kc)
             else if(kc.le.220) then
                de=tde(kc)
             else
                de=tde(kc-220)-tdeb(kc-220)
             endif
       pradc(m)=pradc(m) + dpradc(m)*de
                if(ifich.ge.5.and.ifich.le.8) then
                 txt='J<'
                if(phij(m).lt.-1.d-5*fe)write(29,'(a2,i3,i4,1p7e10.2)')
     &             txt,kc,m,kappa(m)*bb(m),kappa(m),chie(m),eps(m),
     &             epsm1(m),phij(m),s(1,m)
                if(kimpf.eq.1.and.(kpb.ge.1.or.kc.eq.1.or.kc.eq.3.or.
     &          kc.eq.221.or.kc.eq.122.or.kc.eq.138.or.kc.eq.126)) then
                if(m.eq.1) then
                write(29,*)'kc entrant/pi sort/pi refl/pi arriere/pi'
                write(29,'(i4,1p6e11.3)') kc,bc0(1,1),fHs4pi*4.d0,
     &                            f0s4pi*4.d0,bcd(1,1)
                write(29,*) ' kc   m emmissivite kappabs '//               !pc
     &            'sigthomson epsilon    J(m)      S(m)     lstar'
                endif   ! m=1
                if(m.eq.1.or.m.eq.npt.or.mod(m,50).eq.0)
     &          write(29,'(2i4,1p10e10.3)') kc,m,kappa(m)*bb(m),
     &             kappa(m),chie(m),eps(m),phij(m),s(1,m),lstar(m)
                if(m.eq.npt) write(29,*) '     lstar  kc   m  '//
     &            'lstar(k-1)  lstar(k)  lstar(k+1)  d-*d+ nitf'
                endif   ! kc
                endif   ! ifich
       if(tjc(kc,m).lt.0.d0) tjc(kc,m)=0.d0
                       enddo   ! m
c
                              enddo   ! kc
        do m=1,npt
           pradc(m)=pradc(m)*pi4*1.d8/hcse
        enddo
        return
        end                                                           !cPALETOU
c------------------------------------------------------------------------------
       subroutine cT1D(chil,chie,kappa,s,ns,bc0,bcd,reflct,nz,
     .  evaldiag,phij,evalsrc,coreint,jayx,evaljayx,kc)
c
c     the 1-d transfer problem de Paletou transforme pour le continu
c     if evaldiag = .false., computes the formal solution
c     if evaldiag = .true. the diagonal of the crd lambda operator is found
c        phij(id) = l(id,id) = lambda(s(jd)=delta function(id,jd))
c     input
c        tz(iz)    -spatial grid, tz(1) is smallest value
c        chil(iz) =0
c        chie(iz) -electron scattering opacity
c        kappa(iz)-backgroung continuum opacity
c        s(is,iz) -source function                                          !tc
c        ns       -number of independent source functions
c                  if ns=ncfrq, frequency dependent source functions used
c                  if ns ne ncfrq, freq independent s(ifrq,iz) = s(1,iz)
c        bc0(ifrq,idir) -intensity incident on z(1) boundary
c        bc1(ifrq,idir) -intensity incident on z(nz) boundary
c        nz       -number of z-points
c        evaldiag =.true. then evaluate the diagonal of the lambda operator
c                 =.false. then solve transfer equation for specified source
c        evalsrc  = .false. when calling rt1d (...ncfrq...) without
c                           calling srcofi
c        coreint = .false. integration Jbar sur ncfrq
c                  .true.  integration Jbar sur nfcore partial redistribution
c
c     in common /cquad/
c        csz(idir)   -cosine(angle between ray andz-axis)
c        wtdir(idir) -angular quadrature weights
c        ndir        -number of angles
c        phic(ifrq)   -line profile=1
c        wctnu(ifrq)  -frequency quadrature weights=1
c        ncfrq        -number of frequency points=1
c     output                                                                !tc
c        phij     = integral(phic(nu)*j(nu)*dnu) if evaldiag = .false.
c                 = diagonal of lambda operator if evaldiag = .true.
c     if ns=ncfrq, we assume a frequency dependent source function and
c        call 'src of i' (idir,in, int, k, 1)
c     passing the frequency dependent intensity for
c        direction   mu = in*csz(idir)
c        location    tz(k)
c     to permit evaluation of quantities requiring them
c     warning
c     this code assumes the matrices have been dimensioned to hold up to
c     nzs spatial grid points      ncfrqs=1 frequencies       3 angles
c
      implicit double precision (a-h,o-z)
      parameter (nzs=999,ncfrqs=1,ndirs=20)
      double precision kappa(nzs),mu,isp(ncfrqs),jayx(ncfrqs,nzs)
c      real*8 kappa(nzs),mu,isp(ncfrqs),jayx(ncfrqs,nzs)
      dimension chil(nzs),chie(nzs),s(ncfrqs,nzs),phij(nzs)
      dimension bc0(ncfrqs,ndirs),bcd(ncfrqs,ndirs)
      dimension chiu(ncfrqs),su(ncfrqs),chi0(ncfrqs),s0(ncfrqs)
     .          ,chid(ncfrqs),sd(ncfrqs)
      logical reflct,evaldiag,evalsrc,coreint,evaljayx
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
      common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
      common/cquad2/f0s4pi,fHs4pi,fe,fra(ndirs),fsa(ndirs),dpradc(nzs)
      common/cquad/phic(ncfrqs),wctnu(ncfrqs),wtnuc(ncfrqs),
     .             ncfrq,nfcore
      common/cosw/csz(ndirs),wtdir(ndirs)
         save /cquad/,/cquad2/,/cosw/
c ct1d
c
c     initialization
      f0s4pi=0.0d0
      fHs4pi=0.0d0
      do idir=1,ndir
         fra(idir)=0.0d0
         fsa(idir)=0.0d0
      enddo
      do 1000 k = 1, nz
         phij(k) = 0.0d0
         dpradc(k) = 0.d0
coriginal         if(evaljayx) then
c     	    do ifrq=1,ncfrq
c               jayx(ifrq,k)=0.0d0
c            enddo
c         end if
 1000 continue                                                              !tc
         ipb=0
         md=0
         mf=0
         ind=0
         inf=0
         hid=0.d0
         hif=0.d0
      do 1160 idir = 1, ndir
c        integrate first inward and then outward
         do 1150 in = -1, 1, 2
c           in = -1 for inward
            mu = in*csz(idir)
            if (in.lt.0) then
c              integrate inward
               k0 = 1
               k1 = nz
               kdel = 1
               do 1010 ifrq = 1, ncfrq
                     isp(ifrq) = bc0(ifrq,idir)
 1010          continue                                                     !tc
            else
c              integrate outward
               k0 = nz
               k1 = 1
               kdel = -1
               if (.not.reflct) then
c                 apply lower boundary condition
                  do 1020 ifrq = 1, ncfrq
                        isp(ifrq) = bcd(ifrq,idir)
 1020             continue
               end if
c              otherwise int already contains the correct quantity
c              end test on direction
            end if
            if (.not.evaldiag) then
c int defined at boundary
c one should perform computations involving those values here
c contribution of boundary to phij
               do 1030 ifrq = 1, ncfrq
                  phij(k0) = phij(k0) +wtdir(idir)*wctnu(ifrq)*
     .                       phic(ifrq)*isp(ifrq)
                  if(kita.eq.1) dpradc(k0) = dpradc(k0) +
     &                  wtdir(idir)*csz(idir)*csz(idir)*isp(ifrq)
 1030          continue                                                     !tc
            end if
c           transfer in direction idir,in
            do 1140 k = k0+kdel, k1, kdel
c              upwind side
               ku = k - kdel
               du = ((tz(ku)-tz(k)))/mu
c              opacity
               do 1040 ifrq = 1, ncfrq
                  chiu(ifrq) = chie(ku) + kappa(ku)
                  chi0(ifrq) = chie(k) + kappa(k)
c                  chiu(ifrq) = chil(ku)*phic(ifrq)+chie(ku)+kappa(ku) ! pour
c                  chi0(ifrq) = chil(k)*phic(ifrq)+chie(k)+kappa(k) ! des raies
 1040          continue
c              source function
               if (ns.eq.ncfrq) then
c                 frequency dependent source function
                  do 1050 ifrq = 1, ncfrq
                     su(ifrq) = s(ifrq,ku)
                     s0(ifrq) = s(ifrq,k)
 1050             continue                                                  !tc
               else
c                 frequency independent source function
                  do 1060 ifrq = 1, ncfrq
                     su(ifrq) = s(1,ku)
                     s0(ifrq) = s(1,k)
 1060             continue
               end if
c              downwind side
               if (k.ne.k1) then
                  kd = k + kdel
                  dd = (tz(k)-tz(kd))/mu
c                 values for 2nd order interpolation
                  if (ns.eq.ncfrq) then
                     do 1070 ifrq = 1, ncfrq
                        sd(ifrq) = s(ifrq,kd)
 1070                continue
                  else
                     do 1080 ifrq = 1, ncfrq
                        sd(ifrq) = s(1,kd)
 1080                continue
                  end if                                                    !tc
                  do 1090 ifrq = 1, ncfrq
                     chid(ifrq) = chie(kd) + kappa(kd)
c                     chid(ifrq) = chil(kd)*phic(ifrq)+chie(kd)+kappa(kd)!raies
 1090             continue
               else   ! k=k1
c                 at boundary so no upwind values,
c                           force linear interpolation
                  dd = du
                  do 1100 ifrq = 1, ncfrq
                     sd(ifrq) = 2.*s0(ifrq) - su(ifrq)
                     chid(ifrq) = chiu(ifrq)
                  if(in.lt.0.and.s0(ifrq).ge.0.d0.and.sd(ifrq).lt.0.d0)
     &                  sd(ifrq)=s0(ifrq)        ! est-ce bien?
 1100             continue
               end if
               if (evaldiag) then
c                 using s(i) = delta function (i,j) to get diagonal
                  do 1110 ifrq = 1, ncfrq
                     isp(ifrq) = 0.d0
                     su(ifrq) = 0.d0
                     s0(ifrq) = 1.d0
                     sd(ifrq) = 0.d0
 1110             continue
               end if                                                       !tc
c advance ray across cell to tz(k)
               do 1120 ifrq = 1, ncfrq
                  dtu = 0.5d0*(chiu(ifrq)+chi0(ifrq))*du
                  dtd = 0.5d0*(chid(ifrq)+chi0(ifrq))*dd
                             if(isf.ge.1) then
                  cs1 = (dtd*(su(ifrq)-s0(ifrq))/dtu+dtu*(s0(ifrq)-
     .              sd(ifrq))/dtd)/(dtd+dtu)
                  cs2= ((su(ifrq)-s0(ifrq))/dtu-(s0(ifrq)-sd(ifrq))/dtd)
     .                  /(dtu+dtd)
                             endif
c
                  exu = expa(-dtu)
                 if (dtu.le.0.01d0) then
           wt0=dtu*(1.-0.5d0*dtu+(1.d0/6.d0)*dtu*dtu-
     .         (1.d0/24.d0)*dtu*dtu*dtu+(1.d0/120.d0)*dtu*dtu*dtu*dtu)
           wt1=dtu*dtu*(0.5d0-dtu/3.d0+(1.d0/8.d0)*dtu*dtu-
     .         (1.d0/30.d0)*dtu*dtu*dtu+(1.d0/144.d0)*dtu*dtu*dtu*dtu)
           wt2=dtu*dtu*dtu*(1.d0/3.d0-dtu/4.d0+(1.d0/10.d0)*dtu*dtu-
     .         (1.d0/36.d0)*dtu*dtu*dtu+(1.d0/168.d0)*dtu*dtu*dtu*dtu)
                  else
                     wt0 = 1.d0 - exu
                     wt1 = wt0 - dtu*exu
                     wt2 = 2.d0*wt1 - dtu*dtu*exu
                  end if
c                                                                           !tc
                         if(isf.eq.0) then
           isp(ifrq)=isp(ifrq)*exu + s0(ifrq)*wt0 +
     &                             (su(ifrq) - s0(ifrq))*wt1/dtu
                         else
           isp(ifrq)=isp(ifrq)*exu + s0(ifrq)*wt0 + cs1*wt1 + cs2*wt2
                         endif
c
                                 if(isp(ifrq).lt.0.d0) then
                            if(nita.gt.1.and.isp(ifrq).lt.-1.d-3*fe)then
                                       ipb=ipb+1
                                       if(ipb.eq.1) then
                                          hid=isp(ifrq)
                                          md=k
                                          ind=-in
                                       endif
                                       hif=isp(ifrq)
                                       mf=k
                                       inf=-in
                                    endif
                                    isp(ifrq)=0.d0
                                 endif
 1120          continue   ! ifrq
c isp(ifrq) is intensity for mu,frq at tz(k) use it here contribution to phij
c
                  do 1130 ifrq = 1, ncfrq
                     phij(k) = phij(k) + 
     .                  wtdir(idir)*wctnu(ifrq)*phic(ifrq)*isp(ifrq)
c AM :                                                                      !tc
                     if(kita.eq.1) dpradc(k) = dpradc(k) + 
     &                  wtdir(idir)*csz(idir)*csz(idir)*isp(ifrq)
c
                     if(k.eq.1.and.in.gt.0) then
c cas d'un seul angle          (a condition que bc0(ifrq,idir) = exflux(kc)/pi)
        if(ndir.eq.1) f0s4pi=f0s4pi + isp(ifrq)/4.d0
c cas de plusieurs angles :
        if(ndir.ge.2) then
           f0s4pi=f0s4pi + wtdir(idir)*csz(idir)*isp(ifrq)
           fra(idir)=fra(idir) + wtdir(idir)*csz(idir)*isp(ifrq)
        endif
                     endif   ! k=1
                     if(k.eq.nz.and.in.lt.0) then
        if(ndir.eq.1) fHs4pi=fHs4pi + isp(ifrq)/4.d0
        if(ndir.ge.2) then
           fHs4pi=fHs4pi + wtdir(idir)*csz(idir)*isp(ifrq)
           fsa(idir)=fsa(idir) + wtdir(idir)*csz(idir)*isp(ifrq)
        endif
                     endif   ! k=nz
c
c                     if (evaljayx) then
c                        jayx(ifrq,k)=jayx(ifrq,k)+wtdir(idir)*isp(ifrq)
c                     end if
 1130             continue
 1140       continue   ! next k
 1150    continue   ! next in
 1160 continue   ! next idir
c      if(ipb.ge.1.and.(mod(nita,10).eq.0.or.kita.eq.1))
      if(ifich.ge.5.and.ifich.le.8.and.ipb.ge.1.and.kita.eq.1)
     &   write(29,70)ipb,kc,md,mf,ind,inf,hid,hif
 70   format(i3,' PB Ic<0 =>0 kc=',i3,' de m=',i3,' a ',i3,' (sens ',
     &       i2,' a ',i2,') I=',1pe10.3,' a ',1pe10.3)
c23 PB Ic<0 =>0 kc=123 de m=123 a 123 (sens +1 a -1) I=1234567890 a 1234567890
c
c      if (reflct.and.evaldiag) phij(nz) = 2.d0*phij(nz)
      return
      end                                                                 !cT1D
c------------------------------------------------------------------------------
       subroutine rPALETOU1(kr)
c
c kali=2
c TWO LEVEL ATOM IN A SEMI-INFINITE MEDIUM 1D!!!        F.Paletou le 1-11-1999
c     ITERATIVE SOLUTION OF  S = (1-EPS)*LAMBDA(S) + EPS*B
c     Simple ALI (Olson, Auer & Buchler) code (2-level, CRD)
c     envoye par F.Paletou le 12 nov 1999 (programme ali_simple)
c
c si les profils d'absorption et emission ne sont pas les memes
c (redistribution partielle) Sl=Sline varie le long du profil :
c il faut mettre une dimension nfrqs a sl:  dimension sl(nfrqs,nzs)
c
        implicit double precision (a-h,o-z)
       parameter (nzs=999,nfrqs=16,ndirs=20,nord=3,nrais=4200)
c       parameter (pi=3.1415926535898d0)
          logical ng,accel,a0
          character txt*11,tab*1
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/tabm/exflux(322),thnur(nrais)                                !pr1
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2) !2345678
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/fsource/sourcec(322,nzs+1),sourcer(16,nrais,nzs+1)
       common/cpal/temcac(322,nzs),tcapc(322,nzs),tcth(nzs),temc(322),
     &             pradc(nzs)
       common/mgcpal/cimoinsh(322),ciplush(322),cimoins0(322),
     &               fcra(ndirs,322),fcsa(ndirs,322)
       common/rpal/twnu(16,nrais,nzs),tphi(16,nrais,nzs),
     &        tcapr(nrais,nzs),dcapr(nrais,nzs),dsl(nrais,nzs),
     &        tas(nrais,nzs),tbs(nrais,nzs),tne(nzs+1),tdensf(nzs,26,10)
       common/rpal2/kali(nrais),npbali(nrais),lcompt(nzs,nrais)
       common/rpal5/tjrnu(nfrqs,nzs,nrais)
       common/sporpal/tjr(nrais,nzs),tcjn(nrais,nzs),pradr(nrais,nzs)
       common/mgrpal/tflincident,fsor(2,nrais),frer(2,nrais),
     &               fasor(ndirs,nrais),farer(ndirs,nrais)
       dimension psl(nzs),pphij(nzs),pjayx(nzs),pdpra(nzs),pjrnu(16,nzs)
       dimension dphij(nzs),djayx(nzs),ddpra(nzs),djrnu(16,nzs)
c
c      real*8 kappa(nzs),lstar(nzs),jayx(nfrqs,nzs)
      double precision kappa(nzs),lstar(nzs),jayx(nzs)                      !AM
      dimension chil(nzs),chie(nzs),s(nfrqs,nzs),bbc(nzs)
      dimension phij(nzs),bc0(nfrqs,ndirs),bcd(nfrqs,ndirs)
      dimension zerobc(nfrqs,ndirs),xlambda(nfrqs,nzs)
      dimension sl(1,nzs),ssl1(nzs),p(nzs*(nord+1))
      common/rquad2/hjrnu(nfrqs,nzs),hic(2,nzs),fra(ndirs),fsa(ndirs),
     &              dfra(ndirs),dfsa(ndirs),pfra(ndirs),pfsa(ndirs),
     &              fr0s4pi,frHs4pi,fr0s4pico,frHs4pico,fe,impr
      common/rquad3/hir(4,nzs),dpra(nzs)
      common/rquad/phi(nfrqs,nzs),wtnu(nfrqs,nzs),wtnuc(nfrqs,nzs),
     .             nfrq,nfcore
      common/cosw/csz(ndirs),wtdir(ndirs)
         save /cpal/,/rpal/,/sporpal/,/rpal2/,/rpal5/,
     &        /rquad/,/rquad2/,/cosw/
c rpaletou1
       nz=npt
       nfrq=nfrqs         !  nfrq=nf+1
       dslim=1.d-4
                          nfcore=nfrq-1
c       pi4=12.5663706144d0
c                         ou          ndir=1       ndir=3
c                         cosinus et poids pour le transfert Paletou si on a
c                         un nbre d'angles different pour continu et raies
c                         if(ndir.eq.1) then
c                            csz(1)=0.577350269189d0  ou plutot cszr
c                            csz(2)=0.d0
c                            csz(3)=0.d0
c                            wtdir(1)=0.5d0
c                            wtdir(2)=0.d0
c                            wtdir(3)=0.d0
c                         endif
c                         if(ndir.eq.3) then
c                            csz(1)=0.1127016654d0
c                            csz(2)=0.5d0
c                            csz(3)=0.8872983346d0
c                            wtdir(1)=0.13888888885d0 ! somme= 0.49999999990
c                            wtdir(2)=0.2222222222d0
c                            wtdir(3)=0.13888888885d0
c                         endif
c                                                                          !pr1
c------------------------
c do 10 kr=2,nraimax
                          if(kali(kr).ne.2) return
               if(kr.gt.nraissfek.and.kr.le.nraissfek+16) goto 10
       kc=kcr(kr)
       fe=exflux(1)
c          if(fe.eq.0.d0) fe=exflux(1)
c si emmt et capalt=0 quel que soit m tout est mis a zero
                         nul=0
                         do m=1,nz
                         if(abs(dsl(kr,m))+abs(dcapr(kr,m)).gt.0.d0)then
                            nul=1
                            goto 11
                         endif
                         enddo
                         if(nul.eq.0) then
                            do m=1,nz
                            tcjn(kr,m)=tjc(kc,m)*pi4*alo(kr)/1.98648d-8
                               tjr(kr,m)=0.d0
                               do lf = 1, nf+1
                                  sourcer(lf,kr,m) = sourcec(kc,m)
                               enddo ! lf
                            enddo
                            fsor(1,kr)=0.d0
                            frer(1,kr)=0.d0
                            fsor(2,kr)=0.d0
                            frer(2,kr)=0.d0
                            return
                         endif   ! nul=0
 11                      continue
c initialisation a zero                                                    !pr1
                         do m=1,nz
                            phij(m)=0.d0
                            dphij(m)=0.d0
                            pphij(m)=0.d0
                            sl(1,m)=0.d0
                            psl(m)=0.d0
                            jayx(m)=0.d0
                            djayx(m)=0.d0
                            pjayx(m)=0.d0
                            dpra(m)=0.d0
                            ddpra(m)=0.d0
                            pdpra(m)=0.d0
                         enddo
                            fr0s4pi=0.d0
                            dfr0s4pi=0.d0
                            pfr0s4pi=0.d0
                            fr0s4pico=0.d0
                            frHs4pi=0.d0
                            dfrHs4pi=0.d0
                            pfrHs4pi=0.d0
                            frHs4pico=0.d0
                            do idir=1,ndir
                               dfra(idir)=0.d0
                               dfsa(idir)=0.d0
                               pfra(idir)=0.d0
                               pfsa(idir)=0.d0
                            enddo
c definition des parametres du transfert
c profil et poids tels que kapat=kapa0*phi+kapac & som(wtnu(ifrq)*phi(ifrq))=1
       do 1030 m = 1, npt
          do lf=1,nf+1
             phi(lf,m)=tphi(lf,kr,m)         ! phi est normalise: som phi*dnu=1
             wtnu(lf,m)=twnu(lf,kr,m)
c                     wtnu=fint*u0 pour integrer sur nu une fonction quelconque
          enddo   ! lf
          bbc(m) = temcac(kc,m)                        ! emm/capabs du continu
          chil(m) = dcapr(kr,m)                    !abstot~N1B12hnu/4pi=capalt
          kappa(m) = tcapc(kc,m)                       ! capabs du continu
          chie(m) = tcth(m)                            ! ne*0.6652d-24
c sl=emmr/kappar=tAs*J+tBs  tAs(kr,m)=1-eps et tBs(kr,m)=eps*B passes en common
 1030  continue
c
c                         if(kimpf.eq.1.and.(kr.eq.2.or.kr.eq.46 
c     &                   .or.kr.eq.157.or.kr.eq.167.or.kr.eq.180
c     &                   .or.kr.eq.575.or.kr.eq.576.or.kr.eq.577          !pr1
c     &                   .or.kr.eq.159.or.kr.eq.184.or.kr.eq.217
c     &                   .or.kr.eq.548)) then
c                         do m=1,npt
c DIMENSIONNER som              som(m)=0.d0
c                            do lf=1,nf+1
c                               som(m)=som(m)+phi(lf,m)*wtnu(lf,m)
c                            enddo
c                         enddo
c                       write(1,700)'som wphi 1str./5 kr=',kr,(som(k),k=1,nz,5)
c                         write(1,*) 'm=10 phi wtnu'
c                         write(1,'(1pg9.3,1p7g10.3)')
c     &                     (phi(lf,10),wtnu(lf,10),lf=1,nfrq)
c                         write(1,*) 'm=100 phi wtnu'
c                         write(1,'(1pg9.3,1p7g10.3)')
c     &                     (phi(lf,100),wtnu(lf,100),lf=1,nfrq)
c                         write(1,*) 'm=200 phi wtnu'
c                         write(1,'(1pg9.3,1p7g10.3)')
c     &                     (phi(lf,200),wtnu(lf,200),lf=1,nfrq)
c                         endif   ! imp
c
       do 1040 idir = 1, ndir
          do 1041 ifrq = 1, nfrq
             if(incang.eq.0) then
                bc0(ifrq,idir)=exflux(kc)/pi
             else if(idir.eq.incang) then
             bc0(ifrq,idir)=exflux(kc)/(4.d0*pi*csz(idir)*wtdir(idir))
             else
                bc0(ifrq,idir)=0.d0
             endif
             bcd(ifrq,idir) = cimoinsh(kc)
             zerobc(ifrq,idir)=0.d0
 1041     continue
 1040  continue
c
c INITIALISATION de la fonction source                                     !pr1
       do 1031 m = 1, nz
          do 1031 ifrq = 1, nfrq
             s(ifrq,m)= sourcer(ifrq,kr,m)
 1031  continue
c
c EVALUATE LSTAR AND XLAMBDA (MONOCHROMATIC DIAGONAL OPERATOR)
c 1er appel :  phij(id) = l(id,id) = lambda(s(jd)=delta function(id,jd))
c              original: CALL RT1D(tz,chil,chie,kappa,s,1,zerobc,zerobc,.false.
c subroutine rt1d(chil,chie,kappa,s,ns,bc0,bcd,reflct,nz,evaldiag,
c     .           phij,evalsrc,coreint,jayx,evaljayx)
                         impr=0
       nita=0
       CALL RT1D(chil,chie,kappa,s,1,zerobc,zerobc,.false.,nz,.true.,
     .           lstar,.false.,.false.,xlambda,.true.,kr)
c
c                         if(kimpf.eq.1.and.(kr.eq.2.or.kr.eq.152
c     &                   .or.kr.eq.157.or.kr.eq.167.or.kr.eq.180
c     &                   .or.kr.eq.575.or.kr.eq.576.or.kr.eq.577
c     &                   .or.kr.eq.159.or.kr.eq.184.or.kr.eq.217
c     &                   .or.kr.eq.548)) write(1,700)
c     &                   'lstar 1str./5 pour kr=',kr,(lstar(k),k=1,nz,5)
c 700                     format(a22,i4/(1pg9.3,1p7g10.3))
         if(kimpf.eq.1.and.ifich.ge.5.and.ifich.le.8) then
            txt='bosse lstar'
            do m=10,nz-10
         if((lstar(m)-lstar(m-1))*(lstar(m+1)-lstar(m)).lt.-0.1d0)then
         write(29,'(a11,2i4,1p4e11.3,i4)')txt,kr,m,lstar(m-1),lstar(m),
     &      lstar(m+1),(lstar(m)-lstar(m-1))*(lstar(m+1)-lstar(m)),nitf
         if(chil(m)*phi(1,m).ge.kappa(m)*1.d-10) mbos(m)=1
         endif
            enddo
         endif   ! kimpf
c
       do 1131 m = 1, nz
          som=0.d0
          do 1132 ifrq=1,nfrq
             sl(1,m) = dsl(kr,m)                                           !pr1
             snum=chil(m)*phi(ifrq,m)*dsl(kr,m)
     &                   + (kappa(m)+chie(m))*sourcec(kc,m)
c     &                   + kappa(m)*sourcec(kc,m)+chie(m)*tjc(kc,m)
             if(nitf.gt.1) then
                if(ifrq.ne.nfrq.and.lcompt(m,kr).eq.0)
     &             snum=snum + chie(m)*tjrnu(ifrq,m,kr)
                if(ifrq.ne.nfrq.and.lcompt(m,kr).eq.1)
     &             som=som + chie(m)*tjrnu(ifrq,m,kr)*wtnu(ifrq,m)
             endif
             s(ifrq,m)= snum/(chil(m)*phi(ifrq,m)+kappa(m)+chie(m))

c             s(ifrq,k) = (chil(k)*phi(ifrq,k)*dsl(kr,k)
c     &                   + (kappa(k)+chie(k))*sourcec(kc,k))
c     &                 / (chil(k)*phi(ifrq,k) + kappa(k) + chie(k))
c2                               s(ifrq,k) = tas(kr,k)*sl(1,k)+tbs(kr,k)
c1                               s(ifrq,k) = sourcer(ifrq,kr,k)
 1132     continue
          if(lcompt(m,kr).eq.1) s(nfrq-1,m)=s(nfrq-1,m)
     &    + som/wtnu(nfrq-1,m)/(chil(m)*phi(nfrq-1,m)+kappa(m)+chie(m))
 1131  continue
c
                         dsmax=0.d0
c     &                   .or.kr.eq.215.or.kr.eq.237.or.kr.eq.251
c     &                   .or.kr.eq.276.or.kr.eq.287.or.kr.eq.323
c     &                   .or.kr.eq.761.or.kr.eq.772.or.kr.eq.809)       !234567
c     &                   .or.kr.eq.174.or.kr.eq.181.or.kr.eq.262
c     &                   .or.kr.eq.575.or.kr.eq.576.or.kr.eq.577
c     &                   .or.kr.eq.546)
                         if((kr.eq.63.or.kr.eq.828)
c                        .or.kr.eq.557.or.kr.eq.783.or.kr.eq.793
     &                   .and.ifich.ge.5.and.ifich.le.8
     &                   .and.(nitf.le.3.or.kimpf.eq.1)) then
                            write(29,776) 
     &                   kr,nita,dsmax,sl(1,1),sl(1,nz/2),sl(1,nz)
                         OPEN(UNIT=86,FILE='fort.86',STATUS='unknown')
                         if(kr.eq.63.or.kr.eq.828)then
                            impr=1
                         do m=1,nz
                         if(m.eq.1)write(86,*)'kr  m  nitf 1-emmr   '// !2345678
     &        '2-emmc   3-sig*Jrnu kappat(1)  tjr      s(1)      s(16)'
                         if(m.lt.3.or.m.gt.nz-2.or.mod(m,40).eq.0)
     &                   write(86,'(i3,i4,i3,1p7e10.3)') kr,m,nitf,
     &                   chil(m)*phi(1,m)*dsl(kr,m),
     &                   (kappa(m)+chie(m))*sourcec(kc,m),
     &                   chie(m)*tjrnu(1,m,kr),chil(m)*phi(1,m)+
     &                   kappa(m)+chie(m),tjr(kr,m),s(1,m),s(16,m)
                         enddo   ! m
                         endif   ! kr
                         endif   ! kr nitf
c
       a0=NG(0,ssl1,nz,p)                      ! pour preparer l'acceleration
       kita=0
c
       do 1145 nita = 1, 100
                            impr=0
c                        precedents resultats
                         do k = 1, nz
                            pphij(k)=phij(k)
                            psl(k)=sl(1,k)
                            pjayx(k)=jayx(k)                               !pr1
                            pdpra(k)=dpra(k)
                            do ifrq=1,nfrq
                               pjrnu(ifrq,k)=hjrnu(ifrq,k)
                            enddo
                         enddo
                         pfr0s4pi=fr0s4pi
                         pfrHs4pi=frHs4pi
                         do idir=1,ndir
                            pfra(idir)=fra(idir)
                            pfsa(idir)=fsa(idir)
                         enddo
c
c calcul de phij=integral(phi(nu)*lambda(ou I?)nu(s))dnu) & des flux sort.refl. 
c-1999 solution formelle avec caracteristiques courtes
coriginal          CALL RT1D(tz,chil,chie,kappa,s,1,bc0,bcd,.false.,nz,.false.,
c                           (z,chil,chie,kappa,s,ns,bc0,bcd,reflct,nz,evaldiag,
c     .                      phij,evalsrc,coreint,jayx,evaljayx)
          CALL RT1D(chil,chie,kappa,s,nfrq,bc0,bcd,.false.,nz,.false.,
     .       phij,.false.,.false.,jayx,.false.,kr)
c
       do m=1,npt
          do ifrq=1,nfrq
             tjrnu(ifrq,m,kr)=hjrnu(ifrq,m)
          enddo
       enddo   ! m
c                                           
          if(kita.eq.1) goto 1146
c                        resultats du debut (nita=1)
                         if(nita.eq.1) then
                            do k = 1, nz
                               if(phij(k).lt.0.d0) phij(k)=0.d0
                               dphij(k)=phij(k)
                               djayx(k)=jayx(k)
                               ddpra(k)=dpra(k)
                               do ifrq=1,nfrq
                                  djrnu(ifrq,k)=hjrnu(ifrq,k)
                               enddo
                            enddo
                            dfr0s4pi=fr0s4pi
                            dfrHs4pi=frHs4pi
                            do idir=1,ndir
                               dfra(idir)=fra(idir)
                               dfsa(idir)=fsa(idir)
                            enddo
                         endif   ! nita=1
c
                         kpb=0
                         jpb=0
                         mpb=0
                         mpb1=0
          dsmax=0.d0
          do 1133 k = 1, nz                                                !pr1
c         if(nitf.eq.1.and.nita.eq.1.and.k.eq.1)write(80,*)kr,kc,fe
             dsk = (tas(kr,k)*phij(k)+tbs(kr,k) - sl(1,k))
     .             /(1.d0-tas(kr,k)*lstar(k))
                         pslk = sl(1,k)
             sl(1,k) = sl(1,k) + dsk
c
c detection des problemes
                    if(nita.gt.1.and.nitf.gt.1.and.
     &                   (sl(1,k).lt.-1.d-5*fe
     &                   .or.phij(k).lt.-1.d-5*fe
     &                   .or.sl(1,k).gt.1000.d0*fe
     &                   .or.(dsk.gt.0.d0.and.sl(1,k).gt.10.d0*fe
     &                            .and.dsk.ge.5.d0*pslk)
     &                   .or.(dsl(kr,k).gt.1.d-5*fe.and.sl(1,k).gt.
     &                                     100.d0*dsl(kr,k)))) then
                            jpb=jpb+1
                            if(jpb.eq.1) then
                               mpb1=k
                               dskpb=dsk
                               if(phij(k).lt.-1.d-5*fe) kpb=1              !pr1
                               if(sl(1,k).lt.-1.d-5*fe) kpb=2
                               if(sl(1,k).gt.1.d4*fe) kpb=3
                               if(dsk.gt.0.d0.and.sl(1,k).gt.10.d0*fe
     &                                    .and.dsk.ge.5.d0*pslk) kpb=4
                               if(dsl(kr,k).gt.1.d-5*fe
     &                           .and.sl(1,k).gt.100.d0*dsl(kr,k)) kpb=5
                            endif   ! jpb=1
                            mpb=k
c if(tdensf(k,i,j)/nht.ge.1.d-20) sl(1,k)=0.d0 il n'y a pas ici de i et j
c petits problemes
                   else if(sl(1,k).lt.0.d0.and.sl(1,k).gt.-1.d-5*fe)then
                      sl(1,k)=0.d0
                   else if(nita.eq.1.and.sl(1,k).lt.-1.d-5*fe) then
                      sl(1,k)=pslk/2.d0
                   else if(k.ge.nz-2.and.sl(1,k).le.-1.d-5*fe) then
                      sl(1,k)=sl(1,k-1)
c ou if(nita.eq.1.and.sl(1,k).lt.0.d0) sl(1,k)=sl(1,k-1)
c
                            else   ! pas de pb
             if(pslk.gt.1.d-5*fe.and.abs(dsk/pslk).gt.dsmax)
     &       dsmax=abs(dsk/pslk)
                         endif   ! pb
 1133     continue  ! k=strate                                            !pr1
c
          if(dsmax.lt.dslim.and.nita.ge.3) kita=1
c
c en cas de probleme quelle solution ?
                   if(kpb.ge.1) then
                      if(ifich.ge.5.and.ifich.le.8) then
                         write(29,774) kpb
                         write(29,775)kr,nita,kpb,nitf,mpb1,phij(mpb1),
     &                     dsl(kr,mpb1),psl(mpb1),dskpb,sl(1,mpb1),mpb
 774                     format(i1,' kr nita kpb nitf mpb1 phij(mpb1) ',
     &                  'dsl(mpb1) psl(mpb1)  dsk1   sl(1,mpb1) mpb')   !2345678
 775                     format('PB',2i3,i2,2i4,1p5e11.3,i4)
                         if(kimpf.eq.1)write(4,*)'kr nita m kpb nitf '//
     &            ' phij    dsl       psl       sl       kap0sl    emmc'
                               do m=1,nz
                         if(kimpf.eq.1.and.(m.le.3.or.mod(m,10).eq.0.or.
     &                   m.ge.npt-2)) write(4,'(2i3,i4,i2,i3,1p6e10.3)')
     &                   kr,nita,m,kpb,nitf,phij(m),dsl(kr,m),psl(m),
     &                   sl(1,m),chil(m)*phi(1,m)*sl(1,m),
     &                   (kappa(m)+chie(m))*sourcec(kc,m)
                               enddo
                            endif   ! ifich
                         if(nita.le.3.or.kpb.ge.3                          !pr1
     &                      .or.abs(sl(1,k)).gt.1.d3*fe) then
                            do k = 1, nz
                               phij(k)=dphij(k)
                               sl(1,k)=dsl(kr,k)
                               jayx(k)=djayx(k)
                               dpra(k)=ddpra(k)
                               do ifrq=1,nfrq
                                  tjrnu(ifrq,k,kr)=djrnu(ifrq,k)
                               enddo
                            enddo
                            fr0s4pi=dfr0s4pi
                            frHs4pi=dfrHs4pi
                            do idir=1,ndir
                               fra(idir)=dfra(idir)
                               fsa(idir)=dfsa(idir)
                            enddo
                         else   ! nita>3 si kpb=2                          !pr1
                            do k = 1, nz
                               phij(k)=pphij(k)
                               sl(1,k)=psl(k)
                               jayx(k)=pjayx(k)
                               dpra(k)=pdpra(k)
                               do ifrq=1,nfrq
                                  tjrnu(ifrq,k,kr)=pjrnu(ifrq,k)
                               enddo
                            enddo
                            fr0s4pi=pfr0s4pi
                            frHs4pi=pfrHs4pi
                            do idir=1,ndir
                               fra(idir)=pfra(idir)
                               fsa(idir)=pfsa(idir)
                            enddo
                         endif  ! nita/2
                         if(nita.le.10) npbali(kr)=npbali(kr)+1
                         go to 1146 
                   endif   ! PB kpb
c
                        if((jpb.gt.1.or.kr.eq.63.or.kr.eq.828)
c     &                    .or.kr.eq.557.or.kr.eq.783.or.kr.eq.793
     &                   .and.ifich.ge.5.and.ifich.le.8
     &                   .and.(nitf.le.5.or.kimpf.eq.1))
     &                   write(29,776) kr,nita,dsmax,                      !pr1
     &                        sl(1,1),sl(1,nz/2),sl(1,nz)
 776                     format('kr=',i3,' it=',i3,' dS/Smax=',1pg10.3, !2345678
     &                   ' Sl(1-mil-npt)',1p3e11.3)
c
c     &                  .and.(kr.eq.142.or.kr.eq.737.or.kr.eq.758))
c     &                   .or.kr.eq.215.or.kr.eq.237.or.kr.eq.251
c     &                   .or.kr.eq.276.or.kr.eq.287.or.kr.eq.323
c     &                   .or.kr.eq.761.or.kr.eq.772.or.kr.eq.809))    !234567
c     &                   .or.kr.eq.174.or.kr.eq.181.or.kr.eq.262
c     &                   .or.kr.eq.575.or.kr.eq.576.or.kr.eq.577
c     &                   .or.kr.eq.546))
                        if(ifich.ge.5.and.ifich.le.8.and.kita.eq.1.and.
     &                     kimpf.eq.1.and.(kr.eq.63.or.kr.eq.828))then
c     &                    .or.kr.eq.68.or.kr.eq.557.or.kr.eq.783.or.kr.eq.793
                         write(4,*)'kr nita  m kpb nitf phij    '//
     &                   'dsl       psl       sl       kap0sl    emmc'
                               do m=1,nz
                           if(m.le.3.or.mod(m,10).eq.0.or.m.ge.npt-2)
     &                   write(4,'(2i3,i4,i2,i3,1p6e10.3)')kr,nita,m,
     &                   kpb,nitf,phij(m),dsl(kr,m),psl(m),sl(1,m),
     &                   chil(m)*phi(1,m)*sl(1,m),
     &                   (kappa(m)+chie(m))*sourcec(kc,m)
                               enddo
                           endif

c acceleration of convergence -- methode de Ng
          if(iacc.ge.2.and.kita.eq.0.and.nita.ge.10.and.nita.lt.97) then
                         jacc=1
             do 511 k = 1, nz
 511            ssl1(k) = sl(1,k)
             accel=NG(1,ssl1,nz,p)
c
c     &                    kr.eq.142.or.kr.eq.737.or.kr.eq.758
c     &                   .or.kr.eq.215.or.kr.eq.237.or.kr.eq.251
c     &                   .or.kr.eq.276.or.kr.eq.287.or.kr.eq.323
c     &                   .or.kr.eq.761.or.kr.eq.772.or.kr.eq.809      !234567
c     &                   .or.kr.eq.174.or.kr.eq.181.or.kr.eq.262
c     &                   .or.kr.eq.575.or.kr.eq.576.or.kr.eq.577
                         if(kimpf.eq.1.and.(jpb.gt.1.or.                   !pr1
     &                     kr.eq.63.or.kr.eq.828)) then
c     &                    .or.kr.eq.557.or.kr.eq.783.or.kr.eq.793
                         if(ifich.ge.5.and.ifich.le.8.and.accel) 
     &                   write(29,777) ssl1(1),ssl1(nz/2),ssl1(nz)
 777                     format(' acceleration',18x,'Sl(1-mil-npt)',
     &                          1p3e11.3)
                         endif   ! kimpf
c
             if(accel) then
                         do k=1,nz
                            if(ssl1(k).lt.0.d0) jacc=0
                         enddo
                         if(jacc.ne.0) then
                do 512 k = 1, nz
                   sl(1,k) = ssl1(k)
 512            continue
                         endif   ! jacc=1 pas de ssl1<0
             endif   ! accel=vrai cad mod(nita,4)=0
          endif   ! iacc>2

c calcul de la fonction source totale (er+ec+sigJ)/(capr+capc+sig)         !pr1
c ya sigmath*Jc et sigmath*Jraies
          do 1134 m = 1, nz
          som=0.d0
             do 1135 ifrq = 1, nfrq
                snum=chil(m)*phi(ifrq,m)*sl(1,m)
     &                   + (kappa(m)+chie(m))*sourcec(kc,m)
c     &               + kappa(m)*sourcec(kc,m) + chie(m)*tjc(kc,m)
                if(ifrq.ne.nfrq.and.lcompt(m,kr).eq.0)
     &               snum=snum+chie(m)*tjrnu(ifrq,m,kr)
                if(ifrq.ne.nfrq.and.lcompt(m,kr).eq.1)
     &               som=som + chie(m)*tjrnu(ifrq,m,kr)*wtnu(ifrq,m)
                s(ifrq,m)=snum/(chil(m)*phi(ifrq,m)+kappa(m)+chie(m))
 1135        continue
             if(lcompt(m,kr).eq.1) s(nfrq-1,m)=s(nfrq-1,m)
     &     + som/wtnu(nfrq-1,m)/(chil(m)*phi(nfrq-1,m)+kappa(m)+chie(m))
 1134     continue
c
                         if(ifich.ge.5.and.ifich.le.8) then
                         do m=1,npt
                            if((kr.eq.63.or.kr.eq.828)
     &                      .and.(m.lt.3.or.m.gt.nz-2.or.mod(m,40).eq.0)
     &                .and.(nita.lt.3.or.mod(nita,20).eq.0.or.kita.eq.1)
     &                      .and.(nitf.le.3.or.kimpf.eq.1)) then
                               impr=1
                         if(m.eq.1)write(86,*)' kr  m  nita  1-emmr '//
     &           '    2-emmc   3-sig*Jrnu  kappat(1)    s(1)      s(16)'
                         write(86,'(3i4,1p6e11.3)') kr,m,nita,
     &                   chil(m)*phi(1,m)*sl(1,m),
     &                   (kappa(m)+chie(m))*sourcec(kc,m),
     &                   chie(m)*tjrnu(1,m,kr),chil(m)*phi(1,m)
     &                   +kappa(m)+chie(m),s(1,m),s(16,m)
                         endif
                         do ifrq=1,nfrq
                            if(kita.eq.1.and.s(ifrq,m).lt.-1.d-20) then
                             kpb=6
                             if(kpb.eq.6.and.ifrq.eq.nfrq) write(1,80) 
     &                       kr,nita,nitf,m,sourcec(kc,m),sl(1,m),s(1,m)
                             endif
                          enddo
                          enddo   ! m
 80                      format('S<0 pour kr=',i3,' nita=',i2,' nitf=',
     &                   i3,' a m=',i3,' sc,sl,S=',1p3e10.3)
cS<0 pour kr=123 nita=12 nitf=123 a m=123 sc,sl,S= 1.546E-04 1.546E-04 1.546E-04
                         if(kita.eq.1.and.kpb.eq.6) then
                         write(1,*)'kr nita kpb nitf/ kr   m    phi  '//
     &              '    capth     capc      chil     sourcec    sl   s'
                         write(1,*) kr,nita,kpb,nitf
                               do m=1,nz
                           if(m.le.3.or.mod(m,10).eq.0.or.m.ge.npt-2)
     &                   write(1,'(2i4,1p7e10.3)')kr,m,phi(1,m),chie(m),!2345678
     &                   kappa(m),chil(m),sourcec(kc,m),sl(1,m),s(1,m)
                               enddo
                         endif
                         endif   ! ifich
 1145  continue   ! nita                                                   !pr1
 1146  continue
       if(kita.eq.1.and.npbali(kr).gt.0) npbali(kr)=0
c
c                         if(kr.eq.114.and.
c     &                   (nitf.le.5.or.mod(nitf,10).eq.0.or.kitf.eq.1)) then
c                         write(1,*)'fonction source tous les 10 strates ',
c     &                   'pour kr=',kr
c                         write(1,'(1p7g10.3,1pg9.2)') (sl(1,k), k=1,nz,10)
c       endif
c
c AM: calcul du flux total et des flux sortant et reflechi et cjn= 4pi*J/hnu
       frer(1,kr)=fr0s4pi*pi4
       fsor(1,kr)=frHs4pi*pi4
       frer(2,kr)=fr0s4pico*pi4
       fsor(2,kr)=frHs4pico*pi4
       if(ndir.ge.2.and.kimpf.eq.1) then
          do idir=1,ndir
             farer(idir,kr)=fra(idir)*pi4
             fasor(idir,kr)=fsa(idir)*pi4
          enddo
       endif
       do m=1,npt
                if(phij(m).lt.0.d0) phij(m)=0.d0
          tcjn(kr,m)=phij(m) *alo(kr)*6.325933d8 ! som phiJ *4pi/hnuergs-4pi/hc
          tjr(kr,m)=jayx(m)                               ! =som J*dnu
          pradr(kr,m)=dpra(m)*pi4/2.997925d10
          do lf = 1, nf+1
             sourcer(lf,kr,m) = s(lf,m)                                    !pr1
          enddo   ! lf
c
                     if(ifich.ge.5.and.ifich.le.8.and.kimpf2.eq.1
     &                  .and.(kr.eq.63.or.kr.eq.579))then           !CII,SiIX
c     &                 .or.kr.eq.557.or.kr.eq.783.or.kr.eq.793
                        if(m.eq.1)WRITE(21,*)'   kr   m  SlavantALI Sl',
     &                 'apresALI  Ir+/2    Ir-/2    I+phi/2    I-phi/2'
                        if(m.eq.1.or.m.eq.2.or.mod(m,nimpm).eq.0.or.
     &                     m.eq.npt-1.or.m.eq.npt) WRITE(21,489) kr,m,
     &                     dsl(kr,m),sl(1,m),(hir(ii,m),ii=1,4)
 489                    format('PR',2i4,10(1x,1pe17.10))
                     endif

                       if(ifich.ge.5.and.ifich.le.8.and.jayx(m).ge.
     &                    1.d17*fe) write(85,'(2i4,i5,1p2e11.3)')
     &                   nitf,m,kr,jayx(m),tcjn(kr,m)
c
c     &                   .or.kr.eq.215.or.kr.eq.237.or.kr.eq.251
c     &                   .or.kr.eq.276.or.kr.eq.287.or.kr.eq.323
c     &                   .or.kr.eq.159.or.kr.eq.162.or.kr.eq.168       !234567
c     &                   .or.kr.eq.174.or.kr.eq.181.or.kr.eq.262
c     &                   .or.kr.eq.761.or.kr.eq.772.or.kr.eq.809)
                        if((jpb.gt.1.or.kr.eq.63.or.kr.eq.828)
c     &                    .or.kr.eq.557.or.kr.eq.783.or.kr.eq.793
     &                   .and.ifich.ge.5.and.ifich.le.8
     &                   .and.(nitf.le.5.or.kimpf.eq.1).and.
     &                   (m.le.3.or.mod(m,30).eq.0.or.m.ge.npt-2))then
                         if(m.eq.1) write(2,778) kr,kc,bc0(1,1)*pi,
     &                              fr0s4pi*pi4,frHs4pi*pi4,nita,nitf
                         if(m.eq.1)write(2,*)' kr   m    tjr       phij'
     &        //'      Jc(16)    Jr(1)     sourcec  sraie(1)  source(1)'
                         write(2,'(2i4,1pe11.3,1p6e10.3)') kr,m,jayx(m),
     &                     phij(m),hic(1,m)+hic(2,m),hjrnu(1,m),
     &                     sourcec(kc,m),sl(1,m),s(1,m)
                         if(m.eq.1) write(3,*) kr,kc,nitf
                         if(m.eq.1) write(3,*)'kr  m   kappal   kappal0'
     &                //'    kappac     dsl       bbc       1-as     bs'
                         write(3,'(i3,i4,1p7e10.3)') kr,m,chil(m),
     &                     chil(m)*phi(1,m),kappa(m),dsl(kr,m),
     &                     bbc(m),1.d0-tas(kr,m),tbs(kr,m)
c                        if(m.eq.1)write(4,*) 'lf    Jr+  '//
c     &                    '      source m=1 2 3 /10 nitf kr',nitf,kr
c                        write(4,'(i3,1p3e11.3,i3,1p3e11.3)') 
c     &                  (ifrq,hjrnu(ifrq,m),
c     &                  s(ifrq,m),ifrq=1,nfrq,2),                   !1,3,..15
c     &                  nfrq,hic(1,m),hic(2,m),hic(1,m)+hic(2,m)
                         endif   ! imp
 778   format('kr kc cont-inc refl sort nita nitf',2i4,1p3e10.3,2i4)
c
       enddo   ! m
       if(abs(frer(1,kr)).ge.tflincident.or.
     &    abs(fsor(1,kr)).ge.tflincident) then
                        WRITE(6,779) frer(1,kr),fsor(1,kr),kr
                        frer(1,kr)=0.d0
                        fsor(1,kr)=0.d0
                        do m=1,npt
                           tcjn(kr,m)=0.d0
                           tjr(kr,m)=0.d0
                           do lf = 1, nf+1
                              sourcer(lf,kr,m)=0.d0                       !pr1
                           enddo   ! lf
                        enddo   ! m
                     endif
 779   format('Pb: flux',1p2e12.3,' mis a zero pour kr=',i4)
       return
c------------------------                                                 !pr1k
 10                      continue   ! kr
                         do m=1,nz
                            phij(m)=0.d0
                            dphij(m)=0.d0
                            pphij(m)=0.d0
                            sl(1,m)=0.d0
                            psl(m)=0.d0
                            jayx(m)=0.d0
                            djayx(m)=0.d0
                            pjayx(m)=0.d0
                            dpra(m)=0.d0
                            ddpra(m)=0.d0
                            pdpra(m)=0.d0
                         enddo
                            fr0s4pi=0.d0
                            dfr0s4pi=0.d0
                            pfr0s4pi=0.d0
                            fr0s4pico=0.d0
                            frHs4pi=0.d0
                            dfrHs4pi=0.d0
                            pfrHs4pi=0.d0
                            frHs4pico=0.d0
                            do idir=1,ndir
                               dfra(idir)=0.d0
                               dfsa(idir)=0.d0
                               pfra(idir)=0.d0
                               pfsa(idir)=0.d0
                            enddo
       kc=kcr(kr)
       do 1530 m = 1, npt
          do lf=1,nf+1
             phi(lf,m)=tphi(lf,kr,m)         ! phi est normalise: som phi*dnu=1
             wtnu(lf,m)=twnu(lf,kr,m)
c                     wtnu=fint*u0 pour integrer sur nu une fonction quelconque
          enddo   ! lf
          bbc(m) = temcac(kc,m)                        ! emm/capabs du continu
          chil(m) = dcapr(kr,m)                    !abstot~N1B12hnu/4pi=capalt
          kappa(m) = tcapc(kc,m)                       ! capabs du continu
          chie(m) = tcth(m)                            ! ne*0.6652d-24
 1530  continue
       do nita=1,3
          do 1531 m = 1, nz
             som=0.d0
             do 1532 ifrq=1,nfrq
                snum=phi(ifrq,m)*dsl(kr,m)
     &                   + (kappa(m)+chie(m))*sourcec(kc,m)
                if(nitf.gt.1.or.nita.gt.1) then
                   if(ifrq.ne.nfrq.and.lcompt(m,kr).eq.0)
     &                snum=snum+chie(m)*tjrnu(ifrq,m,kr)
                   if(ifrq.ne.nfrq.and.lcompt(m,kr).eq.1)
     &                som=som + chie(m)*tjrnu(ifrq,m,kr)*wtnu(ifrq,m)
                endif
                s(ifrq,m)= snum / (kappa(m) + chie(m))
 1532        continue
             if(lcompt(m,kr).eq.1) s(nfrq-1,m)=s(nfrq-1,m)
     &                + som/wtnu(nfrq-1,m)/(kappa(m)+chie(m))
 1531     continue
          CALL RT1D(chil,chie,kappa,s,nfrq,bc0,bcd,.false.,nz,.false.,
     .              phij,.false.,.false.,jayx,.false.,kr)
          do m=1,npt
             do ifrq=1,nfrq
                tjrnu(ifrq,m,kr)=hjrnu(ifrq,m)
             enddo   ! ifrq
          enddo   ! m
       enddo   ! nita
c AM:                                                                     !pr1k
       fsor(1,kr)=frHs4pi*pi4
       frer(1,kr)=fr0s4pi*pi4
       fsor(2,kr)=frHs4pico*pi4
       frer(2,kr)=fr0s4pico*pi4
       if(ndir.ge.2.and.kimpf.eq.1) then
          do idir=1,ndir
             farer(idir,kr)=fra(idir)*pi4
             fasor(idir,kr)=fsa(idir)*pi4
          enddo
       endif
       do m=1,npt
          tjr(kr,m)=jayx(m)                                ! =som J*dnu
          tcjn(kr,m)=0.d0
          pradr(kr,m)=dpra(m)*pi4/2.997925d10
          do ifrq=1,nfrq
             sourcer(ifrq,kr,m)=0.d0
          enddo   ! lf
                         if(kr.eq.nraissfek+15.and.ifich.ge.5.and.
     &                    ifich.le.8.and.(nitf.le.5.or.kimpf.eq.1).and.
     &                   (m.le.3.or.mod(m,10).eq.0.or.m.ge.npt-2)) then !2345678
                         if(m.eq.1) write(2,'(2i4,1p2e11.3)') 
     &                              kr,kc,fr0s4pi*pi4,frHs4pi*pi4
                         if(m.eq.1)write(2,*)'emr kapac sourcec tjc tjr'
                         write(2,'(i3,i4,1p7e11.3)') kr,m,dsl(kr,m),
     &                   kappa(m),sourcec(kc,m),tjc(kc,m),tjr(kr,m)
                         endif
       enddo   ! m
c fin des raies FeK<17
       return
       end                                                           !rPALETOU1
c------------------------------------------------------------------------------
       subroutine rPALETOU2(j,i)
c
c  Methode MALI pour l'atome a plusieurs niveaux        F.Paletou le 19-4-2000
c     Transfert 1D et en supposant la CRD (complete redistribution)
c     Methode de Rybicki & Hummer (1991)
c     envoye par F.Paletou le 19 avril 2000   (programme h3crd)
c
c si les profils d'absorption et emission ne sont pas les memes
c (redistribution partielle) Sl=Sline varie le long du profil :
c il faut mettre une dimension nfrqs a sl:  dimension sl(nfrqs,nzs)
c
        implicit double precision (a-h,o-z)
       parameter (nzs=999,nfrqs=16,ndirs=20,nord=3,nrais=4200)
       parameter (nivtots=330,nivmax=15)
          character txt*11,tab*1
          double precision ne,nht
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/temp/T,VT,tk,tk1,templog
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/raies2/g1rt(nrais),g2rt(nrais),n1rt(nrais),n2rt(nrais)
       common/raies3/A21(nrais),A21hnu(nrais),A21hnus4pi(nrais),
     &               B12(nrais),B12hnus4pi(nrais),B12hnu2s4pi(nrais)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/tabm/exflux(322),thnur(nrais)                                !pr2
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2) !2345678
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/fsource/sourcec(322,nzs+1),sourcer(16,nrais,nzs+1)
       common/pop/po1(nivmax+1,26),sompo1(26),f(27,10),
     &             ph0ne,phpne,phe0ne,phepne,sech1,sech2,seche1,seche2
       common/dastpo/alfint(nivtots),cioe(nivtots),
     &          texcoll(nrais,nzs+1),teer(nrais,nzs)
       common/cpal/temcac(322,nzs),tcapc(322,nzs),tcth(nzs),temc(322),
     &             pradc(nzs)
       common/mgcpal/cimoinsh(322),ciplush(322),cimoins0(322),
     &               fcra(ndirs,322),fcsa(ndirs,322)
       common/rpal/twnu(16,nrais,nzs),tphi(16,nrais,nzs),
     &        tcapr(nrais,nzs),dcapr(nrais,nzs),dsl(nrais,nzs),
     &        tas(nrais,nzs),tbs(nrais,nzs),tne(nzs+1),tdensf(nzs,26,10)
       common/rpal2/kali(nrais),npbali(nrais),lcompt(nzs,nrais)
       common/rpal3/tlstar(nrais,nzs+1),tjeff(nrais,nzs+1)
       common/rpal4/tsl(nrais,nzs+1),ppop(nivmax,nzs+1)
       common/rpal5/tjrnu(nfrqs,nzs,nrais)
       common/sporpal/tjr(nrais,nzs),tcjn(nrais,nzs),pradr(nrais,nzs)
       common/mgrpal/tflincident,fsor(2,nrais),frer(2,nrais),
     &               fasor(ndirs,nrais),farer(ndirs,nrais)
c       dimension psl(nzs),pphij(nzs),pjayx(nzs),pdpra(nzs),som(nzs)
c       dimension dphij(nzs),djayx(nzs),ddpra(nzs)
c
      double precision kappa(nzs),lstar(nzs),jayx(nzs)                      !AM
      dimension chil(nzs),chie(nzs),s(nfrqs,nzs),bbc(nzs)
      dimension phij(nzs),bc0(nfrqs,ndirs),bcd(nfrqs,ndirs)
      dimension zerobc(nfrqs,ndirs),xlambda(nfrqs,nzs)
      dimension sl(1,nzs)
      common/rquad2/hjrnu(nfrqs,nzs),hic(2,nzs),fra(ndirs),fsa(ndirs),
     &              dfra(ndirs),dfsa(ndirs),pfra(ndirs),pfsa(ndirs),
     &              fr0s4pi,frHs4pi,fr0s4pico,frHs4pico,fe,impr
      common/rquad3/hir(4,nzs),dpra(nzs)
      common/rquad/phi(nfrqs,nzs),wtnu(nfrqs,nzs),wtnuc(nfrqs,nzs),
     .             nfrq,nfcore
      common/cosw/csz(ndirs),wtdir(ndirs)
         save /cpal/,/pop/,/rpal/,/rpal2/,/rpal3/,/rpal4/,
     &        /rpal5/,/sporpal/,/gnv/,/rquad/,/rquad2/,/cosw/
c rpaletou2
       kri=nrcum(i-1,j)+1
       krf=nrcum(i,j)
       do kr=kri,krf
          kali(kr)=3
       enddo
                        if(j.ge.3) then
                           do m=1,npt
                              if(tdensf(m,i,j)/nht.ge.1.d-20) goto 10
                           enddo ! m
                           do kr=kri,krf
                              kali(kr)=0
                           enddo
                           return
                        endif
 10    continue
                           do kr=kri,krf
                              kali(kr)=3
                           enddo
       nz=npt
       nfrq=nfrqs         !  nfrq=nf+1
       dnlim=0.001d0
c       dslim=1.d-4 (paletou1)
                          nfcore=nfrq-1
       niv=nivion(i,j)
       nij1=nivcum(i-1,j)+1
       nijf=nivcum(i,j)
                         if(ifich.ge.5.and.ifich.le.8) then
                           if(j.eq.1) then
                           write(29,180) j,i,nitf
                           write(2,180)  j,i,nitf
                           write(3,180)  j,i,nitf
                           write(4,180)  j,i,nitf
                           write(81,180) j,i,nitf
                           write(82,180) j,i,nitf
                           write(84,180) j,i,nitf
                           else
                           write(29,181) j,i,nitf
                           if(kimpf.eq.1) write(2,181)  j,i,nitf
                           if(kimpf.eq.1) write(3,181)  j,i,nitf
c                           write(4,181)  j,i,nitf
                           write(81,181) j,i,nitf
                           write(82,181) j,i,nitf
                           if(kimpf.eq.1) write(84,181) j,i,nitf
                           endif
                           endif   ! ifich
 180   format(' PALETOU2-raies-MALI: ion j=',i3,' i=',i3,' nitf=',i4)
 181   format(' raies-MALI: ion j=',i3,' i=',i3,' nitf=',i4)
c------------------------
c debut : CALCUL DES POPULATIONS de l'ion comme dans sSTRATE               !pr2
       do m=1,npt
             kimpm2=0
c             if(m.le.2.or.mod(m,30).eq.0.or.m.ge.npt-1) kimpm2=1
       if(m.eq.1.or.m.eq.2.or.m.eq.(npt-1).or.m.eq.npt) kimpm2=1
       if(m.eq.35.or.m.eq.50.or.m.eq.65.or.m.eq.80.or.m.eq.95.or.m.eq.  !2345678
     &    110.or.m.eq.125.or.m.eq.170.or.m.eq.230.or.m.eq.300) kimpm2=1
          T=temp(m)
          VT=sqrt(T)
          nht=tnht(m)
          ne=tne(m)
       if(ifich.ge.5.and.ifich.le.8.and.m.eq.1.and.kimpf.eq.1
     & .and.(j.le.2.or.j.eq.5.or.j.eq.9)) 
     & write(82,*)'nitf j i nita m popn/pop1                    dnsnmax'
          nita=0
          kita=0
          kimpm=kimpm2
          if((j.eq.2.and.i.eq.1).or.tdensf(m,i,j)/nht.ge.1.d-30) then
             CALL sPOPmali(j,i)
          else
             po1(1,i)=1.d0
             do n=2,niv+1
                po1(n,i)=0.d0
             enddo
          endif
       if(ifich.ge.5.and.ifich.le.8.and.kimpm2.eq.1.and.kimpf.eq.1
     &    .and.(j.le.2.or.j.eq.5.or.j.eq.9))
     &write(82,'(4i3,i4,1p16e10.3)')nitf,j,i,nita,m,(po1(n,i),n=2,niv+1)
       enddo   ! m
c-----------
c iterations pour le TRANSFERT
       kita=0
       kita1=0
       do 2135 nita = 1, 100
          if(nita.eq.100) kita=1
c 1ere partie : TRANSFERT DES RAIES de l'ion
                            jpb=0
               do 100 kr=kri,krf
                         if(kali(kr).eq.0) then
                            do m=1,nz
                               tcjn(kr,m)=0.d0
                            enddo
                         endif                                             !pr2
                         if(kali(kr).le.2) then
                            do m=1,nz
                               tjeff(kr,m)=0.d0
                            enddo
                            goto 100
                         endif
c initialisation a zero
                         do m=1,nz
                            phij(m)=0.d0
c                            dphij(m)=0.d0
c                            pphij(m)=0.d0
                            sl(1,m)=0.d0
c                            psl(m)=0.d0
                            jayx(m)=0.d0
c                            djayx(m)=0.d0
c                            pjayx(m)=0.d0
                            dpra(m)=0.d0
                         enddo
                            fr0s4pi=0.d0
                            fr0s4pico=0.d0
c                            dfr0s4pi=0.d0
c                            pfr0s4pi=0.d0
                            frHs4pi=0.d0
                            frHs4pico=0.d0
c                            dfrHs4pi=0.d0
c                            pfrHs4pi=0.d0                                 !pr2
c definition des parametres du transfert
       kc=kcr(kr)                                      ! kali=3
       fe=exflux(1)
c profil et poids tels que kapat=kapa0*phi+kapac & som(wtnu(ifrq)*phi(ifrq))=1
       do 2030 m = 1, npt
          do lf=1,nf+1
             phi(lf,m)=tphi(lf,kr,m)         ! phi est normalise: som phi*dnu=1
             wtnu(lf,m)=twnu(lf,kr,m)        ! phi(1)~1/dnudopplerVpi
c                     wtnu=fint*u0 pour integrer sur nu une fonction quelconque
          enddo   ! lf
          bbc(m) = temcac(kc,m)                        ! emm/capabs du continu
          kappa(m) = tcapc(kc,m)                       ! capabs du continu
          chie(m) = tcth(m)                            ! ne*0.6652d-24
 2030  continue
c
       do 2120 idir = 1, ndir
          do 2130 ifrq = 1, nfrq
             if(incang.eq.0) then
                bc0(ifrq,idir)=exflux(kc)/pi
             else if(idir.eq.incang) then
             bc0(ifrq,idir)=exflux(kc)/(4.d0*pi*csz(idir)*wtdir(idir))
             else
                bc0(ifrq,idir)=0.d0
             endif
             bcd(ifrq,idir) = cimoinsh(kc)
             zerobc(ifrq,idir)=0.d0
 2130     continue
 2120  continue
c
c                         if(ifich.ge.5.and.ifich.le.8.and.
c     &                      kimpf.eq.1.and.nita.eq.1.and.
c     &                   (kr.eq.2.or.kr.eq.46 .or.kr.eq.237
c     &                   .or.kr.eq.575.or.kr.eq.576.or.kr.eq.577          !pr2
c     &                   .or.kr.eq.276.or.kr.eq.287.or.kr.eq.323
c     &                   .or.kr.eq.761.or.kr.eq.772.or.kr.eq.809)) then
c                         do m=1,npt
c                            som(m)=0.d0
c                            do lf=1,nf+1
c                               som(m)=som(m)+phi(lf,m)*wtnu(lf,m)
c                            enddo
c                         enddo
c                    write(1,700)'som wphi 1str./5 kr=',kr,' nitf=',nitf,!234567
c     &                   (som(k),k=1,nz,5)
c                         write(1,*) 'm=1 phi wtnu'
c                         write(1,'(1pg9.3,1p7g10.3)')
c     &                     (phi(lf,1),wtnu(lf,1),lf=1,nfrq)
c                         write(1,*) 'm=50 phi wtnu'
c                         write(1,'(1pg9.3,1p7g10.3)')
c     &                     (phi(lf,50),wtnu(lf,50),lf=1,nfrq)
c                         write(1,*) 'm=95 phi wtnu'
c                         write(1,'(1pg9.3,1p7g10.3)')
c     &                     (phi(lf,95),wtnu(lf,95),lf=1,nfrq)
c                         write(1,*) 'm=200 phi wtnu'
c                         write(1,'(1pg9.3,1p7g10.3)')
c     &                     (phi(lf,200),wtnu(lf,200),lf=1,nfrq)
c 700                    format(a22,i3,a6,i3/(1pg9.3,1p7g10.3))
c                        endif      ! imp
c
c INITIALISATION de la fonction source                                     !pr2
       if(nita.eq.1) then
          do 2031 m=1,npt
             do 2031 ifrq=1,nfrq
                s(ifrq,m)=sourcer(ifrq,kr,m)        ! ? pour les kali
 2031     continue
       endif
       do 2032 m=1,npt
          if(nita.eq.1) then
             chil(m)=dcapr(kr,m)      ! kappa-r-tot=(N1B12-N2B21)hnu/4pi=capalt
             sl(1,m)=dsl(kr,m)               ! dsl(kr,m)=demt/capalt de sSTRATE
          else
             chil(m)=tcapr(kr,m)
             sl(1,m)=tsl(kr,m)
          endif
 2032  continue
c
c EVALUATE LSTAR AND XLAMBDA (MONOCHROMATIC DIAGONAL OPERATOR)
c 1er appel :  phij(id) = l(id,id) = lambda(s(jd)=delta function(id,jd))
c              original: CALL RT1D(tz,chil,chie,kappa,s,1,zerobc,zerobc,.false.
c subroutine rt1d(chil,chie,kappa,s,ns,bc0,bcd,reflct,nz,evaldiag,
c     .           phij,evalsrc,coreint,jayx,evaljayx)
                         impr=0
       CALL RT1D(chil,chie,kappa,s,1,zerobc,zerobc,.false.,nz,.true.,
     &           lstar,.false.,.false.,xlambda,.true.,kr)
          do m=1,npt
             tlstar(kr,m)=lstar(m)
          enddo
c                                                                          !pr2
c calcul de la fonction source totale (er+ec+sigJ)/(capr+capc+sig)
       do 2131 m=1,npt
          som=0.d0
          do 2132 ifrq=1,nfrq
             snum=chil(m)*phi(ifrq,m)*sl(1,m)
     &                   + (kappa(m)+chie(m))*sourcec(kc,m)
c     &            + kappa(m)*sourcec(kc,m) + chie(m)*tjc(kc,m)
             if(nitf.gt.1.or.nita.gt.1) then
                if(ifrq.ne.nfrq.and.lcompt(m,kr).eq.0)
     &             snum=snum + chie(m)*tjrnu(ifrq,m,kr)
                if(ifrq.ne.nfrq.and.lcompt(m,kr).eq.1) 
     &             som=som + chie(m)*tjrnu(ifrq,m,kr)*wtnu(ifrq,m)
             endif
             s(ifrq,m)= snum/(chil(m)*phi(ifrq,m) + kappa(m) + chie(m))
 2132     continue
          if(lcompt(m,kr).eq.1) s(nfrq-1,m)=s(nfrq-1,m)
     &    + som/wtnu(nfrq-1,m)/(chil(m)*phi(nfrq-1,m)+kappa(m)+chie(m))

                        if(ifich.ge.5.and.ifich.le.8.and.kimpf.eq.1
     &                  .and.(nita.le.3.or.kita.eq.1).and.(kr.eq.2.or.
     &                  kr.eq.10.or.kr.eq.46.or.kr.eq.68.or.kr.eq.330  !2345678
     &                  .or.kr.eq.379.or.kr.eq.848.or.kr.eq.898).and.
     &                  (m.eq.1.or.m.eq.2.or.m.eq.(npt-1).or.m.eq.npt
     &                  .or.m.eq.35.or.m.eq.50.or.m.eq.65.or.m.eq.80
     &                  .or.m.eq.95.or.m.eq.110.or.m.eq.125.or.m.eq.170
     &                  .or.m.eq.230.or.m.eq.300)) 
     &                  write(84,'(2i3,i4,1pe9.2,1p6e10.3)')
     &                  kr,nita,m,phi(1,m),tjrnu(1,m,kr),
     &                  chil(m)*phi(1,m)*sl(1,m),
     &               (kappa(m)+chie(m))*sourcec(kc,m),chie(m)*hjrnu(1,m)

 2131  continue   ! m
c
c calcul de phij=integral(phi(nu)*lambda(ou I?)nu(s))dnu) & des flux sort.refl. 
c-1999 solution formelle avec caracteristiques courtes
coriginal          CALL RT1D(tz,chil,chie,kappa,s,1,bc0,bcd,.false.,nz,.false.,
c                           (z,chil,chie,kappa,s,ns,bc0,bcd,reflct,nz,evaldiag,
c     .                      phij,evalsrc,coreint,jayx,evaljayx)
c
       CALL RT1D(chil,chie,kappa,s,nfrq,bc0,bcd,.false.,nz,.false.,
     &           phij,.false.,.false.,jayx,.false.,kr)
c                                                                          !pr2
       do m=1,npt
          tjeff(kr,m)=phij(m) - lstar(m)*sl(1,m)
          do ifrq=1,nfrq
             tjrnu(ifrq,m,kr)=hjrnu(ifrq,m)
          enddo
       enddo   ! m
c
c detection des problemes
                           do m=1,npt
c         if(nitf.eq.1.and.nita.eq.1.and.m.eq.1)write(80,*)kr,kc,fe
                              if(tjeff(kr,m).lt.0.d0) then
                                 if(tjeff(kr,m).lt.-1.d-5*fe) then
                                    jpb=jpb+1
                                    if((kita.eq.1.or.mod(nita,10).eq.0)
     &                                 .and.ifich.ge.5.and.ifich.le.8)
     &                              write(29,182) tjeff(kr,m),kr,m,nita
                                    tjeff(kr,m)=0.d0
                                 else
                                    tjeff(kr,m)=0.d0
                                 endif
                              endif
                           enddo
 182   format(' PB jeff<0 :',1pe10.3,' pour kr=',i4,' a m=',i3,
     &        ' a l''it.mali=',i3)
c                                                            
c AM: calcul du flux total et des flux sortant et reflechi et cjn= 4pi*J/hnu
       if(kita.eq.1) then
          fsor(1,kr)=frHs4pi*pi4
          frer(1,kr)=fr0s4pi*pi4                                           !pr2
          fsor(2,kr)=frHs4pico*pi4
          frer(2,kr)=fr0s4pico*pi4
       if(ndir.ge.2.and.kimpf.eq.1) then
          do idir=1,ndir
             farer(idir,kr)=fra(idir)*pi4
             fasor(idir,kr)=fsa(idir)*pi4
          enddo
       endif
          do m=1,npt
                if(phij(m).lt.0.d0) phij(m)=0.d0
          tcjn(kr,m)=phij(m)*alo(kr)*6.325933d8   !som phiJ *4pi/hnuergs-4pi/hc
             tjr(kr,m)=jayx(m)     ! =som J*dnu
             pradr(kr,m)=dpra(m)*pi4/2.997925d10
             do lf = 1, nf+1
                sourcer(lf,kr,m) = s(lf,m) 
             enddo   ! lf
          enddo   ! m
c       if(kimpf.eq.1) write(29,183) nita,kr,fsor(1,kr),frer(1,kr)
 183   format(' nita=',i3,' kr=',i4,' flux sortant reflechi=',1p2e11.4)
c
c
                     if(ifich.ge.4.and.ifich.le.8.and.kimpf2.eq.1
     &                  .and.(kr.eq.379.or.kr.eq.828)) then           !O8,Fe23
                        WRITE(21,*)'   kr   m  N(nij1)   N(nij2)ali '//
     &                       'SlavantALI  SlapresALI  Jphi  Jr  '
     &                ,'Ir+/2  Ir-/2  I+phi/2  I-phi/2  Ic+/2  Ic-/2'
                        do m=1,npt
                           kimpm=0
                           if(m.eq.1.or.m.eq.2.or.m.eq.(npt-1).or.
     &                              m.eq.npt.or.mod(m,nimpm).eq.0) 
     &                     WRITE(21,489) kr,m,tdensf(m,i,j),po1(2,i)*
     &                 tdensf(m,i,j),dsl(kr,m),tsl(kr,m),phij(m),jayx(m)
     &                ,(hir(ii,m),ii=1,4),hic(1,m),hic(2,m)
                        enddo
 489                    format('PR',2i4,13(1x,1pe17.10))
                     endif
                     do m=1,npt
                        kimpm2=0
       if(m.eq.1.or.m.eq.2.or.m.eq.(npt-1).or.m.eq.npt) kimpm2=1
       if(m.eq.35.or.m.eq.50.or.m.eq.65.or.m.eq.80.or.m.eq.95.or.m
     & .eq.110.or.m.eq.125.or.m.eq.170.or.m.eq.230.or.m.eq.300) kimpm2=1
                        if(ifich.ge.5.and.ifich.le.8.and.(kr.eq.2
     &                     .or.kr.eq.10.or.kr.eq.46                 !HI,HeII
     &                     .or.kr.eq.68.or.kr.eq.330                !CIII,OVII
c     &                   .or.kr.eq.215.or.kr.eq.237.or.kr.eq.251
c     &                   .or.kr.eq.159.or.kr.eq.162.or.kr.eq.168        !234567
c     &                   .or.kr.eq.174.or.kr.eq.181.or.kr.eq.262
     &                   .or.kr.eq.379.or.kr.eq.848.or.kr.eq.898).and.
     &                   (nitf.le.5.or.kimpf.eq.1).and.kimpm2.eq.1)then
                         if(m.eq.1) write(2,778) kr,kc,bc0(1,1)*pi,
     &                              fr0s4pi*pi4,frHs4pi*pi4,nita,nitf
                         if(m.eq.1)write(2,*)' kr   m    tjr       phij'
     &        //'      Jc(16)    Jr(1)     sourcec  sraie(1)  source(1)'
                         write(2,'(2i4,1pe11.3,1p6e10.3)') kr,m,jayx(m),
     &                     phij(m),hic(1,m)+hic(2,m),hjrnu(1,m),
     &                     sourcec(kc,m),sl(1,m),s(1,m)
                         if(m.eq.1) write(3,*) kr,kc,nitf,nita
                         if(m.eq.1) write(3,*)'kr  m   densfond  dkappa'
     &             //'lt  kappalt   kappal0    kappac     dsl       bbc'
                         write(3,'(i3,i4,1p7e10.3)') kr,m,tdensf(m,i,j),
     &                    dcapr(kr,m),chil(m),chil(m)*phi(1,m),kappa(m),
     &                    dsl(kr,m),bbc(m)
c                        if(m.eq.1)write(4,*) 'lf   Ir+/2      Ir-/2 '//
c     &                    '      source m=1 2 3 /10 nitf kr',nitf,kr
c                        write(4,'(i3,1p3e11.3,i3,1p3e11.3)') 
c     &                  (ifrq,hjrnu(ifrq,m),
c     &                  s(ifrq,m),ifrq=1,nfrq,2),                    !1,3,..15
c     &                  nfrq,hic(1,m),hic(2,m),hic(1,m)+hic(2,m)
                         endif   ! imp
                         txt='tjr>1e14'
                  if(ifich.ge.5.and.ifich.le.8.and.jayx(m).ge.1.d17*fe)
     &                   write(85,'(a8,2i4,i5,1p2e11.3)')
     &                   txt,nitf,m,kr,jayx(m),tcjn(kr,m)
                     enddo   ! m
 778   format('kr kc cont-inc refl sort nita nitf',2i4,1p3e10.3,2i4)
       endif   ! kita=1
c
                        if(ifich.ge.5.and.ifich.le.8.and.kimpf.eq.1
     &                     .and.(nita.le.3.or.kita.eq.1).and.(kr.eq.2
     &                     .or.kr.eq.10.or.kr.eq.46.or.kr.eq.15  
     &                     .or.kr.eq.68.or.kr.eq.330.or.kr.eq.379
c                        .or.kr.eq.276.or.kr.eq.280 .or.kr.eq.798
     &                     .or.kr.eq.848.or.kr.eq.898)) then
                         do m=1,npt
c                            dphij(m)=tcjn(kr,m)/alo(kr)/6.325933d8
                            if(kimpf.eq.1) then                            !pr2
                               kimpm2=0
c                      if(m.le.2.or.mod(m,30).eq.0.or.m.ge.npt-1) kimpm2=1
       if(m.eq.1.or.m.eq.2.or.m.eq.(npt-1).or.m.eq.npt) kimpm2=1
       if(m.eq.35.or.m.eq.50.or.m.eq.65.or.m.eq.80.or.m.eq.95.or.m.eq.  !2345678
     &    110.or.m.eq.125.or.m.eq.170.or.m.eq.230.or.m.eq.300) kimpm2=1
                         if(m.eq.1)write(81,*)'kr nita m  capalt    '//
     &          'lstar     sl        s      hjrnu(1,m)   phij     tjeff'
                   if(kimpm2.eq.1)write(81,'(i4,i3,i4,1pe9.2,1p6e10.3)')!2345678
     &                   kr,nita,m,chil(m),lstar(m),
     &                   sl(1,m),s(1,m),hjrnu(1,m),phij(m),tjeff(kr,m)
c                     if(kimpm2.eq.1)write(84,'(2i3,i4,1pe9.2,1p6e10.3)') 
c     &               kr,nita,m,phi(1,m),hjrnu(1,m),hjrnu(16,m),
c     &               chil(m)*phi(ifrq,m)*sl(1,m),
c     &               (kappa(m)+chie(m))*sourcec(kc,m),chie(m)*hjrnu(1,m)

                         endif   ! kimpf
                         enddo   ! m
                         endif   ! imp
       if(abs(frer(1,kr)).ge.tflincident.or.
     &    abs(fsor(1,kr)).ge.tflincident) then
                        WRITE(6,779) frer(1,kr),fsor(1,kr),kr
                        frer(1,kr)=0.d0
                        fsor(1,kr)=0.d0
                        do m=1,npt
                           tcjn(kr,m)=0.d0
                           tjr(kr,m)=0.d0
                           do lf = 1, nf+1
                              sourcer(lf,kr,m)=0.d0                       !pr2
                           enddo   ! lf
                        enddo   ! m
                     endif
 779   format('Pb: flux',1p2e12.3,' mis a zero pour kr=',i4)
c
 100    continue  ! enddo kr
       if(kita.eq.1) goto 2136
       if(nita.gt.10.and.jpb.gt.50)  then
          if(ifich.ge.5.and.ifich.le.8) write(29,187) nita,jpb
 187      format(' a l''it.mali=',i3,i4,
     &        ' jeff<0 : on retourne aux Jr et Jphi initiaux')
          goto 2136                ! on ne change pas tjr et tcjn
       endif   ! jpb>10
c
c 2e partie : reCALCUL DES POPULATIONS de l'ion avec des coeff modifies   !pr2p
c calcul de dn/n
       dnsnmax=0.d0
       jpb2=0
       do m=1,npt
          kimpm2=0
c             if(m.le.2.or.mod(m,30).eq.0.or.m.ge.npt-1) kimpm2=1
          if(m.eq.1.or.m.eq.2.or.m.eq.(npt-1).or.m.eq.npt) kimpm2=1
         if(m.eq.35.or.m.eq.50.or.m.eq.65.or.m.eq.80.or.m.eq.95.or.m.eq.!2345678
     &    110.or.m.eq.125.or.m.eq.170.or.m.eq.230.or.m.eq.300) kimpm2=1
          T=temp(m)
          VT=sqrt(T)
          nht=tnht(m)
          ne=tne(m)
          if(ifich.ge.5.and.ifich.le.8.and.m.eq.1.and.kimpf.eq.1
     &      .and.(j.le.2.or.j.eq.5.or.j.eq.9).and.
     &      (mod(nita,20).eq.1.or.nita.eq.2.or.kita1.eq.1)) write(82,*)
     &    'nitf j i nita m popn/pop1                        dnsnmax'
c         if(kimpm2.eq.1.and.kimpf.eq.1) write(82,'(4i3,i4,1p12e10.3)')
c     &    nitf,j,i,nita,m,(ppop(n,m),n=2,niv+1)
          kimpm=kimpm2
          if(tdensf(m,i,j)/nht.lt.1.d-30) then          ! maximum=abondance(j)
             do kr=kri,krf
                tsl(kr,m)=1.d0
                tcapr(kr,m)=0.d0
             enddo
          else
             CALL sPOPmali(j,i)                                          !pr2p
             do kr=kri,krf
                n1=n1rt(kr)
                n2=n2rt(kr)
                g1=g1rt(kr)
                g2=g2rt(kr)
                        if(tdensf(m,i,j)*po1(n1,i)/nht.le.1.d-30) then
                             tsl(kr,m)=1.d0
                             tcapr(kr,m)=0.d0
                        else
                gngn=po1(n2,i)*g1/(po1(n1,i)*g2)
                if(gngn.ge.1.d0) then            ! en general les niveaux les
                   jpb2=jpb2+1                 ! plus eleves (pas de irecsup=1)
c                   if(kita1.eq.1.and.jpb2.eq.1)
c     &                write(6,184) teer(kr,m),gngn,kr,nita,m
                   gngn=teer(kr,m)
                endif
 184   format('PBmali N2g1/N1g2=',1pe9.3,' remplace',1pe10.3,
     &         '>1 pour kr=',i3,' nita=',i2,' apres m=',i3)
cBmali N2g1/N1g2=1.345e+89 remplace 2.456e+90>1 pour kr= 23 nita= 2 apres m= 23
c
                tsl(kr,m)=po1(n2,i)*A21hnus4pi(kr)/
     &                    (po1(n1,i)*(1.d0-gngn)*B12hnus4pi(kr))
                tcapr(kr,m)=tdensf(m,i,j)*po1(n1,i)*
     &                      (1.d0-gngn)*B12hnus4pi(kr)
                        endif   ! po1 tres petit
c
c                if((kr.eq.2.or.kr.eq.323).and.kimpf.eq.1.and.kimpm2.eq.1)
c     &                        write(84,'(2i3,i4,2i2,0p2f3.0,1p5e10.3)')
c     &                        nitf,nita,m,n1,n2,g1,g2,tdensf(m,i,j),
c     &                        po1(n1,i),po1(n2,i),tcapr(kr,m),tsl(kr,m)
             enddo   ! kr                                                 !pr2p
          endif
             do n=1,niv
                if(nita.gt.1.and.ppop(n,m).gt.1.d-20) then
                   dp=abs(po1(n,i)-ppop(n,m))/ppop(n,m)
                   if(dp.gt.dnsnmax) dnsnmax=dp
                endif
                ppop(n,m)=po1(n,i)
             enddo
       if(ifich.ge.5.and.ifich.le.8.and.kimpm2.eq.1.and.kimpf.eq.1
     &    .and.(j.le.2.or.j.eq.5.or.j.eq.9)
     &    .and.(mod(nita,20).eq.1.or.nita.eq.2.or.kita1.eq.1))
     &    write(82,'(4i3,i4,1p16e10.3)') nitf,j,i,nita,m,
     &    (po1(n,i),n=2,niv+1),dnsnmax*100.d0
       enddo   ! m
       if(kita1.eq.1) kita=1
       if((nita.gt.2.and.dnsnmax.lt.dnlim).or.nita.eq.99) kita1=1     ! 0.1%
       if(ifich.ge.5.and.ifich.le.8) then
       if((nita.gt.1.and.nita.le.10).or.mod(nita,5).eq.0.or.kita.eq.1
     &     .or.nita.eq.99) write(29,185) nita,dnsnmax*100.d0
 185   format(' it.mali=',i3,':variation rel. maximum des populations',
     &       '(tous n & m)=',f11.4,'%')
       write(82,*) j,i,nita,'  dnsnmax%=',dnsnmax*100.d0
       if(kita.eq.1) write(29,186) nita,dnsnmax*100.d0
       endif   ! ifich
 186   format(' nbre iterations mali nita=',i3,' dnsnmaxfin=',f10.6,'%')
       if((nita.gt.10.and.dnsnmax.gt.10.d0*pdnsnmax).or.
     &    (nita.gt.10.and.dnsnmax.gt.1.d3).or.
     &    (nita.eq.99.and.dnsnmax.gt.1.d0)) then
          do kr=kri,krf
             npbali(kr)=npbali(kr)+1
          enddo
          if(ifich.ge.5.and.ifich.le.8)write(29,188) nita,dnsnmax
 188      format(' a l''it.mali=',i3,' PB dn/n max=',1pe10.3,
     &        ' : on RETOURne aux Jr et Jphi INITIAUX')
          goto 2136  ! sans changer tjr et tcjn
       endif   ! pb
       pdnsnmax=dnsnmax
c
 2135  continue   ! nita
 2136  continue
       do kr=kri,krf
          if(kita.eq.1.and.npbali(kr).gt.0) npbali(kr)=0
       enddo
c
       return
       end                                                           !rPALETOU2
c------------------------------------------------------------------------------
       subroutine RT1D(chil,chie,kappa,s,ns,bc0,bcd,reflct,nz,evaldiag,
     .         phij,evalsrc,coreint,jayx,evaljayx,kr)
c
coriginal  subroutine RT1D(z,chil,chie,kappa,s,ns,bc0,bcd,reflct,nz,
cor     .  evaldiag,phij,evalsrc,coreint,jayx,evaljayx)
c     warning
c     this code assumes the matrices have been dimensioned to hold up to
c     nzs spatial grid points      nfrq=15+1 frequencies       5 angles
c
      implicit double precision (a-h,o-z)
      parameter (nzs=999,nfrqs=16,ndirs=20,nrais=4200)
      double precision kappa(nzs),mu,isp(nfrqs),jayx(nzs)               !AM
c      real*8 kappa(nzs),mu,isp(nfrqs),jayx(nfrqs,nzs)
      dimension chil(nzs),chie(nzs),s(nfrqs,nzs),phij(nzs)
      dimension bc0(nfrqs,ndirs),bcd(nfrqs,ndirs)
      dimension chiu(nfrqs),su(nfrqs),chi0(nfrqs),s0(nfrqs)
     .          ,chid(nfrqs),sd(nfrqs)
c
      logical reflct,evaldiag,evalsrc,coreint,evaljayx
c
      common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
      common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
      common/opt2/iacc,isf,ndir,incang
      common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
      common/rpal2/kali(nrais),npbali(nrais),lcompt(nzs,nrais)
      common/rquad/phi(nfrqs,nzs),wtnu(nfrqs,nzs),wtnuc(nfrqs,nzs),
     .             nfrq,nfcore
      common/rquad2/hjrnu(nfrqs,nzs),hic(2,nzs),fra(ndirs),fsa(ndirs),
     &              dfra(ndirs),dfsa(ndirs),pfra(ndirs),pfsa(ndirs),
     &              fr0s4pi,frHs4pi,fr0s4pico,frHs4pico,fe,impr
      common/rquad3/hir(4,nzs),dpra(nzs)
      common/cosw/csz(ndirs),wtdir(ndirs)
         save /rquad/,/rquad2/,/cosw/
c rt1d
c
c     initialization
      fr0s4pi= 0.0d0
      frHs4pi= 0.0d0
      fr0s4pico= 0.0d0
      frHs4pico= 0.0d0
      do idir=1,ndir
         fra(idir)=0.0d0
         fsa(idir)=0.0d0
      enddo
      do 1000 k = 1, nz
         phij(k) = 0.0d0
         jayx(k) = 0.0d0                                                 !AM
         dpra(k) = 0.0d0
         hir(1,k)=0.0d0
         hir(2,k)=0.0d0
         hir(3,k)=0.0d0
         hir(4,k)=0.0d0
            hic(1,k)=0.d0
            hic(2,k)=0.d0
         do 1001 ifrq=1,nfrq
            hjrnu(ifrq,k)=0.d0
 1001    continue
c
 1000 continue
         ipb=0
         md=0
         mf=0
         ind=0
         inf=0
         hid=0.d0
         hif=0.d0
c boucles 1160 et 1150 jusqu'a la fin de la subroutine
      do 1160 idir = 1, ndir
c integrate first inward (in=-1 for inward: I+AM) and then outward (in=+1: I-)
         do 1150 in = -1, 1, 2
            mu = in*csz(idir)
c initialisation de l'intensite
            if (in.lt.0) then             ! integrate inward (in<0 => I+AM)
               k0 = 1
               k1 = nz
               kdel = 1
               hic(1,1)=hic(1,1) + wtdir(idir)*bc0(nfrq,idir)
               do 1010 ifrq = 1, nfrq
                     isp(ifrq) = bc0(ifrq,idir)            ! flux incident
c                     hjrnu(ifrq,1)=0.d0                    ! (I+AM-Ic+)/2
 1010          continue                                                     !tr
            else                          ! integrate outward (in>0 => I-)
               k0 = nz
               k1 = 1
               kdel = -1
co               if (.not.reflct) then
c                 apply lower boundary condition
                  hic(2,nz)=hic(2,nz) + wtdir(idir)*bcd(nfrq,idir)
                  do 1020 ifrq = 1, nfrq
                     isp(ifrq) = bcd(ifrq,idir)         ! flux arriere
c                     hjrnu(ifrq,nz)=hjrnu(ifrq,nz) + 0.d0         !I-AM/2-Ic-
 1020             continue
co              end if   ! reflct
co              otherwise int already contains the correct quantity
            end if   ! end test on direction in
            if (.not.evaldiag) then
c int defined at boundary
c one should perform computations involving those values here
c contribution of boundary to phij
               do 1030 ifrq = 1, nfrq
                  phij(k0) = phij(k0) +wtdir(idir)*wtnu(ifrq,k0)*
     .                       phi(ifrq,k0)*isp(ifrq)
 1030          continue                                                     !tr
c
ca               if (evalsrc) then
ca               if (ns.eq.nfrq) call SRCOFI(idir,in,int,k0,1)
ca               end if
            end if   ! evaldiag
c
c transfer in direction idir,in - calcul de l'opacite
c
       if(impr.eq.1.and.ifich.ge.5.and.ifich.le.8) then
c         write(6,*) 'nita=',nita
c         write(6,'(5(i3,1pe11.3))') (ifrq,isp(ifrq),ifrq=1,nfrq,3)
         write(86,'(i4,1p6e11.3)') k0,isp(1),isp(4),isp(nfrq)
         if(in.lt.0)write(86,*)' m   isp(1)     s0(1)      isp(4) '//
     &             '    s0(4)    isp(nfrq)  s0(nfrq)  int1-int16'
       endif   ! impr
c
            do 1140 k = k0+kdel, k1, kdel
c              upwind side
               ku = k - kdel
               du = ((tz(ku)-tz(k)))/mu
c              opacity
               do 1040 ifrq = 1, nfrq
                  chiu(ifrq) = chil(ku)*phi(ifrq,ku)+chie(ku)+kappa(ku)
                  chi0(ifrq) = chil(k)*phi(ifrq,k)+chie(k)+kappa(k)
                     if(chi0(ifrq).lt.0.d0) write(6,'(a,i4,1p5e12.4)')
     &        'chi<0',kr,chi0(ifrq),chil(k),phi(ifrq,k),chie(k),kappa(k)
 1040          continue
c              source function
ca               if (ns.eq.nfrq) then
c frequency dependent source function - il s'agit de la fonction source totale
                  do 1050 ifrq = 1, nfrq
                     su(ifrq) = s(ifrq,ku)
                     s0(ifrq) = s(ifrq,k)
c if(in.lt.0.and.k.eq.k1.and.s0(ifrq).lt.su(ifrq)/10.) s0(ifrq)=su(ifrq) !NON
 1050             continue                                                 !tr
ca               else
ca frequency independent source function
ca                  do 1060 ifrq = 1, nfrq
ca                     su(ifrq) = s(1,ku)
ca                     s0(ifrq) = s(1,k)
ca 1060             continue
ca               end if
c              downwind side
               if (k.ne.k1) then
                  kd = k + kdel
                  dd = (tz(k)-tz(kd))/mu
c                 values for 2nd order interpolation
ca                  if (ns.eq.nfrq) then
                     do 1070 ifrq = 1, nfrq
                        sd(ifrq) = s(ifrq,kd)
 1070                continue
ca                  else
ca                     do 1080 ifrq = 1, nfrq
ca                        sd(ifrq) = s(1,kd)
ca 1080                continue
ca                  end if                                                  !tr
                  do 1090 ifrq = 1, nfrq
                     chid(ifrq)=chil(kd)*phi(ifrq,kd)+chie(kd)+kappa(kd)
 1090             continue
               else   ! k=k1
c at boundary so no upwind values, force linear interpolation
                  dd = du
                  do 1100 ifrq = 1, nfrq
                     sd(ifrq) = 2.d0*s0(ifrq) - su(ifrq)   !expression Paletou
                     chid(ifrq) = chiu(ifrq)               !expression Paletou
c                              chid(ifrq)=2.d0*chi0(ifrq)-chiu(ifrq) ! oublier!
                  if(in.lt.0.and.s0(ifrq).ge.0.d0.and.sd(ifrq).lt.0.d0)
     &                  sd(ifrq)=s0(ifrq)        ! est-ce bien?
                     if(in.lt.0.and.chi0(ifrq).lt.0.d0) 
     &                  chi0(ifrq) = chiu(ifrq)
 1100             continue
               end if   ! k/k1
               if (evaldiag) then
c using s(i) = delta function (i,j) to get diagonal - calcul de Lambda*
                  do 1110 ifrq = 1, nfrq
                     isp(ifrq) = 0.0
                     su(ifrq) = 0.0
                     s0(ifrq) = chil(k)*phi(ifrq,k)/chi0(ifrq)
                     sd(ifrq) = 0.0
 1110             continue
               end if   ! evaldiag                                          !tr
c advance ray across cell to tz(k),calcul de l'intensite specifique I(nu,dir,z)
               do 1120 ifrq = 1, nfrq
                  dtu = 0.5d0*(chiu(ifrq)+chi0(ifrq))*du
                  dtd = 0.5d0*(chid(ifrq)+chi0(ifrq))*dd
                             if(isf.ge.2) then
                  cs1 = (dtd*(su(ifrq)-s0(ifrq))/dtu+dtu*(s0(ifrq)-
     .                   sd(ifrq))/dtd)/(dtd+dtu)
                  cs2= ((su(ifrq)-s0(ifrq))/dtu-(s0(ifrq)-sd(ifrq))/dtd)
     .                  /(dtu+dtd)
                             endif
c
                  exu = expa(-dtu)
                 if (dtu.le.0.01d0) then
           wt0=dtu*(1.-0.5d0*dtu+(1.d0/6.d0)*dtu*dtu-
     .         (1.d0/24.d0)*dtu*dtu*dtu+(1.d0/120.d0)*dtu*dtu*dtu*dtu)
           wt1=dtu*dtu*(0.5d0-dtu/3.d0+(1.d0/8.d0)*dtu*dtu-
     .         (1.d0/30.d0)*dtu*dtu*dtu+(1.d0/144.d0)*dtu*dtu*dtu*dtu)
           wt2=dtu*dtu*dtu*(1.d0/3.d0-dtu/4.d0+(1.d0/10.d0)*dtu*dtu-
     .         (1.d0/36.d0)*dtu*dtu*dtu+(1.d0/168.d0)*dtu*dtu*dtu*dtu)
                  else
                     wt0 = 1.d0 - exu
                     wt1 = wt0 - dtu*exu
                     wt2 = 2.d0*wt1 - dtu*dtu*exu
                  end if
c                                                                           !tr
                         if(isf.le.1) then
           isp(ifrq)=isp(ifrq)*exu + s0(ifrq)*wt0 + 
     &                             (su(ifrq) - s0(ifrq))*wt1/dtu
                         else if(isf.eq.2) then
           isp(ifrq)=isp(ifrq)*exu + s0(ifrq)*wt0 + cs1*wt1 + cs2*wt2
                         else if(isf.eq.3) then
                            if(cs2.eq.0.d0) then
                               isp(ifrq)=isp(ifrq)*exu + s0(ifrq)*wt0 + 
     &                             (su(ifrq) - s0(ifrq))/dtu*wt1
                            else if(cs1/cs2.gt.0.d0.and.
     &                              cs1/cs2.lt.dtu*2.d0) then
                               isp(ifrq)=isp(ifrq)*exu + s0(ifrq)*wt0 + 
     &                             (su(ifrq) - s0(ifrq))/dtu*wt1
                            else
                               isp(ifrq)=isp(ifrq)*exu + s0(ifrq)*wt0 +
     &                             cs1*wt1 + cs2*wt2
                            endif
                         else if(isf.eq.4) then
                            psu=abs((su(ifrq)-s0(ifrq))/dtu)
                            psd=abs((s0(ifrq)-sd(ifrq))/dtd)
                            if(psu.gt.psd*2.d0.or.psd.gt.psu*2.d0) then
                               isp(ifrq)=isp(ifrq)*exu + s0(ifrq)*wt0 + 
     &                             (su(ifrq) - s0(ifrq))/dtu*wt1
                            else
                               isp(ifrq)=isp(ifrq)*exu + s0(ifrq)*wt0 +
     &                             cs1*wt1 + cs2*wt2
                            endif
                         endif
c
                                 if(isp(ifrq).lt.0.d0) then
                                    if(isp(ifrq).lt.-1.d-3*fe)then
                                       ipb=ipb+1
                                       if(ipb.eq.1) then
                                          hid=isp(ifrq)
                                          md=k
                                          ind=-in
                                       endif
                                       hif=isp(ifrq)
                                       mf=k
                                       inf=-in
                                    endif
                                    isp(ifrq)=0.d0
                                 endif
 1120          continue   ! ifrq
c
      if(ifich.ge.5.and.ifich.le.8.and.impr.eq.1.and.
     &        (k.lt.3.or.k.gt.nz-2.or.mod(k,40).eq.0)) then
c         write(6,'(2i3,1p6e11.3)') k,in,du,dd,su(1),s0(1),sd(1)
         write(86,'(i3,1p7e11.3)') k,isp(1),s0(1),isp(4),s0(4),
     &                             isp(nfrq),s0(nfrq),isp(1)-isp(nfrq)
      endif   ! impr                                                       !tr
c
c isp(ifrq) is intensity for mu,frq at z(k) use it here contribution to phij
co               if (coreint) then      ! cas de la redistribution partielle
co                  do 1125 ifrq = 1, nfcore
co                     phij(k) = phij(k) + wtdir(idir)*wtnuc(ifrq,k)*
co     .                         phi(ifrq,k)*isp(ifrq)
co 1125             continue
co               else
         if(in.lt.0) hic(1,k)=hic(1,k) + wtdir(idir)*isp(nfrq)          !Ic+
         if(in.gt.0) hic(2,k)=hic(2,k) + wtdir(idir)*isp(nfrq)          !Ic-
c
                  do 1130 ifrq = 1, nfrq-1
       phij(k)=phij(k) + wtdir(idir)*wtnu(ifrq,k)*phi(ifrq,k)*isp(ifrq)
c
c AM :
c somme sur les frequences, les angles et le sens
       jayx(k)=jayx(k) + wtdir(idir)*wtnu(ifrq,k)*(isp(ifrq)-isp(nfrq))  ! AM
c somme sur les angles et le sens
       hjrnu(ifrq,k)=hjrnu(ifrq,k) + wtdir(idir)*(isp(ifrq)-isp(nfrq))
c pression de radiation
       if(kita.eq.1) dpra(k)=dpra(k) + wtdir(idir)*csz(idir)*csz(idir)
     &                            *wtnu(ifrq,k)*(isp(ifrq)-isp(nfrq))
c
       if(in.lt.0) then
      hir(1,k)=hir(1,k)+wtdir(idir)*wtnu(ifrq,k)*(isp(ifrq)-isp(nfrq)) !Ir+/2
      hir(3,k)=hir(3,k)+wtdir(idir)*wtnu(ifrq,k)*phi(ifrq,k)*isp(ifrq) !I+phi/2
       else if(in.gt.0) then
      hir(2,k)=hir(2,k)+wtdir(idir)*wtnu(ifrq,k)*(isp(ifrq)-isp(nfrq)) !Ir-/2
      hir(4,k)=hir(4,k)+wtdir(idir)*wtnu(ifrq,k)*phi(ifrq,k)*isp(ifrq) !I-phi/2
       endif
c
                     if(k.eq.1.and.in.gt.0) then      ! fin du retour
       if(ndir.eq.1) fr0s4pi=fr0s4pi +
     &        wtnu(ifrq,k)*(isp(ifrq)-isp(nfrq))/4.d0
       if(ndir.ge.2) then
          fr0s4pi=fr0s4pi +
     &        wtdir(idir)*csz(idir)*wtnu(ifrq,k)*(isp(ifrq)-isp(nfrq))
          fra(idir)=fra(idir) + wtdir(idir)*
     &        csz(idir)*wtnu(ifrq,k)*(isp(ifrq)-isp(nfrq))
       endif
                     endif   ! k=1 z=0
                     if(k.eq.nz.and.in.lt.0) then     ! fin de l'aller
       if(ndir.eq.1) frHs4pi=frHs4pi +
     &        wtnu(ifrq,k)*(isp(ifrq)-isp(nfrq))/4.d0
       if(ndir.ge.2) then
          frHs4pi=frHs4pi +
     &        wtdir(idir)*csz(idir)*wtnu(ifrq,k)*(isp(ifrq)-isp(nfrq))
          fsa(idir)=fsa(idir) + wtdir(idir)*
     &        csz(idir)*wtnu(ifrq,k)*(isp(ifrq)-isp(nfrq))
       endif
                     endif   ! k=nz z=H                                     !tr
coriginal
c                     if (evaljayx) then           
c                        jayx(ifrq,k)=jayx(ifrq,k)+wtdir(idir)*isp(ifrq)
c                     endif                
 1130             continue   ! ifrq
             if(lcompt(k,kr).eq.1) then
                     if(k.eq.1.and.in.gt.0) then
       if(ndir.eq.1) fr0s4pico=fr0s4pico
     &    +wtnu(nfrq-1,k)*(isp(nfrq-1)-isp(nfrq))/4.d0
       if(ndir.ge.2) fr0s4pico=fr0s4pico
     &    +wtdir(idir)*csz(idir)*wtnu(nfrq-1,k)*(isp(nfrq-1)-isp(nfrq))
                     endif   ! k=1 z=0
                     if(k.eq.nz.and.in.lt.0) then
       if(ndir.eq.1) frHs4pico=frHs4pico
     &    +wtnu(nfrq-1,k)*(isp(nfrq-1)-isp(nfrq))/4.d0
       if(ndir.ge.2) frHs4pico=frHs4pico
     &    +wtdir(idir)*csz(idir)*wtnu(nfrq-1,k)*(isp(nfrq-1)-isp(nfrq))
                     endif   ! k=nz z=H
             endif   ! lcompt=1
c
co               end if   ! coreint
c                                                                           !tr
ca               if (evalsrc) then
ca               if (.not.evaldiag.and.ns.eq.nfrq) call SRCOFI(idir,in,int,k,1)
ca               end if
c
 1140       continue         ! next k    z
 1150    continue       ! next in        sens
 1160 continue      ! next idir          direction
co      if (reflct.and.evaldiag) phij(nz) = 2.d0*phij(nz)
      if(ifich.ge.5.and.ifich.le.8.and.ipb.ge.1.and.(mod(nita,10).eq.0
     &  .or.kita.eq.1)) write(29,70)ipb,kr,md,mf,ind,inf,hid,hif
 70   format(i3,' PB I<0 =>0 kr=',i4,' de m=',i3,' a ',i3,' (sens ',
     &       i2,' a ',i2,') I=',1pe10.3,' a ',1pe10.3)
      return
      end                                                                 !RT1D
c------------------------------------------------------------------------------
      logical function NG(init,x,n,xx)
      implicit double precision (a-h,o-z)

c original       dimension x(n),xx(*)
       parameter (nzs=999,nord=3)
       dimension x(nzs),xx(nzs*(nord+1))                                    !AM
       data k,knt/2*0/

       if(init.eq.0) then
c initialize for new acceleration sequence
          knt = 0
          k = 0
          ng = knt.gt.3
          return
       endif

 1000 do 100 i = 1, n
          k = k + 1
          xx(k) = x(i)
 100  continue
      knt = knt + 1
      ng = knt.gt.3
      if (.not.ng) return
      a1 = 0.d0
      b1 = 0.d0
      b2 = 0.d0
      c1 = 0.d0
      c2 = 0.d0
      k0 = k + 1
      k1 = k0 - n
      k2 = k1 - n
      k3 = k2 - n
      do 200 i = 1, n
         tmp2 = xx(k0-i) - 2.d0*xx(k1-i) + xx(k2-i)
         a1 = a1 + tmp2**2
         tmp3 = xx(k3-i) - xx(k2-i) - xx(k1-i) + xx(k0-i)
         b1 = b1 + tmp2*tmp3
         b2 = b2 + tmp3**2
         c1 = c1 - (xx(k1-i) - xx(k0-i))*tmp2
         c2 = c2 - (xx(k1-i) - xx(k0-i))*tmp3
 200  continue
      det = b2*a1 - b1**2
c original      if (det.eq.0.d0) return
      if (det.eq.0.d0) then                                            !AM
         knt = 0                                                       !AM
         k = 0                                                         !AM
         return                                                        !AM
      endif   ! det=0                                                  !AM
      a = (c1*b2 - c2*b1)/det
      b = (c2*a1 - c1*b1)/det
      if (det/(b2*a1 +b1**2) .gt. 1.d-4) then
         k0 = k - n
         k1 = k0 - n
         k2 = k1 - n
         do 300 i = 1, n
            x(i) = (1.d0-a-b)*xx(k0+i) + a*xx(k1+i) + b*xx(k2+i)
 300     continue
         ng = .true.
      end if
      knt = 0
      k = 0
      if (.not.ng) goto 1000
      return
c     initialize for new acceleration sequence
c      entry ng0
c      knt = 0
c      k = 0
c      return
      end                                                                   !ng
c==============================================================================
        subroutine TC2dir

c transfert du continu a 2 directions comme dans les premiers Titans
c acceleration si iacc.ne.0
c faire un TA2dir appele ds le main juste avant "210 continue" pour l'acceler.?

       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15)
         character tab*1,txt*5
         dimension ttoc1(322),ttoc2(322),ttoc3(322),rr(16,nzs+2),
     & trim1(16,nrais,nzs+2),trim2(16,nrais,nzs+2),trim3(16,nrais,nzs+2)
         dimension tcim1(322,nzs),tcim2(322,nzs),tcim3(322,nzs)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/tabm/exflux(322),thnur(nrais)
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/im/fii,itali
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/fsource/sourcec(322,nzs+1),sourcer(16,nrais,nzs+1)
       common/degmt/hautsom,dtaucmax,hnumaxto,
     &             p_grait,p_gfree,p_gbound,g_pcompt,ptot,
     &             restflux(322),rtaucef(322),ttaucef(322),rtaucat(322)
       common/degmt2/exdtauc(322,0:nzs+1),exdtaur(16,nrais,0:nzs+1)
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2) !234567
       common/transf2/cimoins(322,0:nzs+2),varim,ecarim
       common/rpal2/kali(nrais),npbali(nrais),lcompt(nzs,nrais)
c
c TRANSFERT AM ancien pour le continu
        varim=0.d0
        do mm=1,npt             !m
           m=npt-mm+1
           do kc=1,322
              if(m.eq.1) cim=cimoins(kc,m)
              if(m.eq.1.and.nitf.eq.1) cim=exflux(kc)/pi2v3*fii
              ddt=(1.d0-exdtauc(kc,m)*exdtauc(kc,m-1))/2.d0
              cimoins(kc,m)=(cimoins(kc,m+1)*exdtauc(kc,m-1)
     &             + sourcec(kc,m)*ddt)
              if(m.eq.1.and.cimoins(kc,1).gt.1.d-20)
     &             varim=varim+((cimoins(kc,1)-cim)/cimoins(kc,1))**2
           enddo                ! kc
        enddo                   ! mm
        ecarim=sqrt(varim/322)
c
       if(iacc.eq.0.or.mod(nitf,10).gt.4.or.nitf.le.50) goto 210
c ACCELERATION DE CONVERGENCE   avec les iterations 1,2,3,4
c       if(nitf.eq.14) write(6,*) 'accLy: nitf    dt     d2t/dt   '//
c     & 'ttoc3     ttoc2     ttoc1    ttoc0  ttaucef-res'
               if(mod(nitf,10).ge.1.and.mod(nitf,10).le.3) then
          do m=1,npt
             do kc=1,322
                if(mod(nitf,10).eq.1) tcim3(kc,m)=cimoins(kc,m)
                if(mod(nitf,10).eq.2) tcim2(kc,m)=cimoins(kc,m)
                if(mod(nitf,10).eq.3) tcim1(kc,m)=cimoins(kc,m)
             enddo             ! kc
          enddo                ! m
 220                 continue
          do kc=1,322
                if(mod(nitf,10).eq.1) ttoc3(kc)=ttaucef(kc)
                if(mod(nitf,10).eq.2) ttoc2(kc)=ttaucef(kc)
                if(mod(nitf,10).eq.3) ttoc1(kc)=ttaucef(kc)
          enddo            ! kc
          do m=1,npt
             do kr=2,nraimax
                do lf=1,nf+1
                   if(mod(nitf,10).eq.1) trim3(lf,kr,m)=rimoins(lf,kr,m)!234567
                   if(mod(nitf,10).eq.2) trim2(lf,kr,m)=rimoins(lf,kr,m)
                   if(mod(nitf,10).eq.3) trim1(lf,kr,m)=rimoins(lf,kr,m)
c                   if(mod(nitf,10).eq.4) trim0(lf,kr,m)=rimoins(lf,kr,m)
                enddo          ! lf
             enddo             ! kr
          enddo                ! m
              goto 210
       endif                   ! nitf=*1,2,3
c
       if(mod(nitf,10).eq.4) then

c       if(nitf.ge.ndebali) goto 221
          do kc=1,322
             a1=0.d0
             b1=0.d0
             b2=0.d0
             c1=0.d0
             c2=0.d0
             wt=1.d0
             do m=1,npt
                d0=cimoins(kc,m) - tcim1(kc,m)
                d1=cimoins(kc,m) - tcim1(kc,m)*2.d0 + tcim2(kc,m)
                d2=cimoins(kc,m)-tcim1(kc,m)-tcim2(kc,m)+tcim3(kc,m)
                a1=a1 + d1*d1   !*wt
                b1=b1 + d1*d2   !*wt
                b2=b2 + d2*d2   !*wt
                c1=c1 + d0*d1   !*wt
                c2=c2 + d0*d2   !*wt
             enddo              ! m
             a=(b2*c1 - b1*c2)/(b2*a1 - b1*b1)
             b=(a1*c2 - b1*c1)/(b2*a1 - b1*b1)
             do m=1,npt
                dcc=cimoins(kc,m)*(-a-b)+tcim1(kc,m)*a+tcim2(kc,m)*b
       if((kc.eq.1.or.kc.eq.220).and.(m.eq.40.or.m.eq.150))
     & write(6,'(i2,2i4,2(1pe9.2),5(1pe11.4))')
     & iacc,kc,m,dcc/cimoins(kc,m),dcc,tcim3(kc,m),tcim2(kc,m),
     & tcim1(kc,m),cimoins(kc,m),cimoins(kc,m)+dcc
                    if(abs(dcc).gt.0.5d0*cimoins(kc,m))
     &          dcc=(cimoins(kc,m)-tcim1(kc,m))*3.d0
                cimoins(kc,m)=cimoins(kc,m) + dcc
             enddo              ! m
          enddo                 ! kc
 221         continue
c
          do 211 kc=1,322
             wt=1.d0
c  if(ttaucef(kc).gt.0.d0) wt=1.d0/abs(ttaucef(kc))
             d0=ttaucef(kc) - ttoc1(kc)
                if(d0.eq.0.d0) goto 211
             d1=ttaucef(kc) - ttoc1(kc)*2.d0 + ttoc2(kc)
             d2=ttaucef(kc) - ttoc1(kc) - ttoc2(kc) + ttoc3(kc)
             a1=d1*d1   !*wt
             b1=d1*d2   !*wt
             b2=d2*d2   !*wt
             c1=d0*d1   !*wt
             c2=d0*d2   !*wt
             den=b2*a1 - b1*b1
             a=(b2*c1 - b1*c2)/den
             b=(a1*c2 - b1*c1)/den
             ttaucef(kc)=ttaucef(kc)*(1.d0-a-b)+ttoc1(kc)*a+ttoc2(kc)*b
                dtf=d0*3.d0
             if(dtf.gt.-ttaucef(kc)) ttaucef(kc)=ttaucef(kc) + dtf
c        write(6,'(i2,i4,2(1pe9.2),5(1pe11.4))') iacc,kc,d1/d0,dtf/      !23456
c     &  rtaucef(kc),d0*3.d0,ttoc2(kc),ttoc1(kc),rtaucef(kc),ttaucef(kc)
 211                   continue             ! kc
              txt='accLy'
       write(6,'(a5,i2,i4,2f9.4,5f10.5)') txt,iacc,nitf,d0,d1/d0,
     &        ttoc3(1),ttoc2(1),ttoc1(1),rtaucef(1),ttaucef(1)
c
c acceleration des raies
          do 212 kr=2,nraimax
             if(kali(kr).ge.2) goto 212
             kineg=0
             do lf=1,nf+1
                a1=0.d0
                b1=0.d0
                b2=0.d0
                c1=0.d0
                c2=0.d0
                wt=1.d0
                do m=1,npt
                  d0=rimoins(lf,kr,m) - trim1(lf,kr,m)
                  d1=rimoins(lf,kr,m)-trim1(lf,kr,m)*2.d0+trim2(lf,kr,m)!234567
                  d2=rimoins(lf,kr,m) - trim1(lf,kr,m) - trim2(lf,kr,m)
     &                                + trim3(lf,kr,m)
                   a1=a1 + d1*d1   !*wt
                   b1=b1 + d1*d2   !*wt
                   b2=b2 + d2*d2   !*wt
                   c1=c1 + d0*d1   !*wt
                   c2=c2 + d0*d2   !*wt
                enddo           ! m
                den=b2*a1 - b1*b1
                if(den.ne.0.d0) then
                   a=(b2*c1 - b1*c2)/den
                   b=(a1*c2 - b1*c1)/den
                else
                   a=0.d0
                   b=0.d0
                endif
                do m=1,npt
                   rr(lf,m)=rimoins(lf,kr,m)*(1.d0-a-b)
     &                        + trim1(lf,kr,m)*a + trim2(lf,kr,m)*b
                   if(rr(lf,m).lt.0.d0) kineg=1
                enddo           ! m
             enddo              ! lf
             do lf=1,nf+1
                do m=1,npt
                   if(kineg.eq.0) then
                      rimoins(lf,kr,m)=rr(lf,m)
                   else if(rimoins(lf,kr,m).lt.0.d0) then
                      rimoins(lf,kr,m)=0.d0
                   endif
                enddo           ! m
             enddo              ! lf
 212               continue              ! kr

       endif                    ! nitf=*4
 210      continue
c
        return
        end                                                            !tC2dir
c------------------------------------------------------------------------------
        subroutine TR2dirA(capalt,demt,am,u0,cx,kc,kr,lcomp)

c calcul du transfert a 2 directions comme dans les premiers Titans
c calcul de tcjn(kr,m)=cjn(kr) & tjr(kr,m)
c rajouter  somkbt  comme parametre si besoin

       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15)
         double precision ne,nht
         character tab*1
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/im/fii,itali
       common/temp/T,VT,tk,tk1,templog
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2)
       common/fsource/sourcec(322,nzs+1),sourcer(16,nrais,nzs+1)
       common/degmt/hautsom,dtaucmax,hnumaxto,                            !tr2
     &          p_grait,p_gfree,p_gbound,g_pcompt,ptot,
     &          restflux(322),rtaucef(322),ttaucef(322),rtaucat(322)
       common/degmt2/exdtauc(322,0:nzs+1),exdtaur(16,nrais,0:nzs+1)
       common/dastpo/alfint(nivtots),cioe(nivtots),
     &          texcoll(nrais,nzs+1),teer(nrais,nzs)
       common/tabm/exflux(322),thnur(nrais)
       common/mstpo/rtaur0(nrais),rtaurl(nrais),
     &          twr(nrais),tws(nrais),uhz(nrais),cjn(nrais)
       common/stpo/tphot(nivtots),tphotk(26,10),dens(nivtots),
     &          alfinduit(nivtots),zfree,cthomson
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/comptonrai/dnuco(nrais),sourdnuco(2,nrais,nzs+1),
     &   ripdnuco(2,nrais),pripdnuco(2,nrais),rimdnuco(2,nrais,0:nzs+2)
       common/cpal/temcac(322,nzs),tcapc(322,nzs),tcth(nzs),temc(322),
     &             pradc(nzs)
       common/sporpal/tjr(nrais,nzs),tcjn(nrais,nzs),pradr(nrais,nzs)
       common/tr2ab/emcdnu,emmco,hjco,hjcoc
          save /stpo/,/dastpo/,/cpal/,/sporpal/,/tr2ab/
c
c tr2dirA
c        tk1=1.d0/(T*ckev)
        hnuev=thnur(kr)
        capabs=tcapc(kc,m)
        emmc=temc(kc)
c
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
        bt=hnuev*hnuev
        ee=teer(kr,m)                                         ! exp(-hnuev*tk1)
        if((1.d0-ee).gt.1.d-6) bt=bt*ee *(hnuev*tk1/(1.d0-ee))**2
           endif   ! kitt
        somkbt=0.d0
c        som2jdnu=0.d0
c        somfdnu=0.d0
c        somkfdnu=0.d0
c
c calcul du J et du dir=4piSomme(Jrdnu)/hnu en vue du nbre de photoionisations
        somjr=0.d0
        somjn=0.d0
        somw=0.d0
        do lf=1,nf
           prof=FPROF(lf,am,x,cx)
           somw=somw + prof *fint(lf)
        enddo
        whz=somw*u0
        capal0=capalt/whz
        dem0=demt/whz
        som=0.d0                                                            !tr2
        fi=fii
        if(nitf.eq.1.and.hnuev.ge.1.d3) fi=0.d0
                        do 310 lf=nf+1,1,-1
                if(lf.eq.nf+1) then
                prof=0.d0
                else
        prof=FPROF(lf,am,x,cx)
                endif
        ai=priplus(lf,kr)                                                  !tr2
        bi=rimoins(lf,kr,m+1)
        if(nitf.eq.1.and.isuite.le.2) bi=(ai + debim(kc)) *fi
c                            if(prof.lt.1.d-20.and.lf.ne.nf+1) then
c                            write(6,*) m,nitt,nitd,kr,lf,' prof=0'
c                            dto=dhaut*V3*(capabs + cthomson)
c                            exdto=expa(-dto)
c                            pexdto=exdtauc(kc,m-1)
c                            hj=FJ(exdto,pexdto,capabs,emmc,cthomson,ai,bi,fii)
c                            somjr=somjr + (hj - hjc)*fint(lf)
c                            if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
c                            exdtaur(lf,kr,m)=1.d0
c                            sourcer(lf,kr,m)=sourcec(kc,m)
c                            ddt=(1.d0-exdto*pexdto)/2.d0
c                            riplus(lf,kr)=(priplus(lf,kr)*exdto
c     &                                  + sourcec(kc,m)*ddt)
c                            endif
c                                goto 310
c                            endif
        capat=capabs + capal0*prof
        dto=dhaut*V3*(capat + cthomson)                             ! cont+raie
        exdto=expa(-dto)
        pexdto=exdtaur(lf,kr,m-1) *exdtauc(kc,m-1)
        emm=emmc + dem0*prof
           if(lcomp.eq.1) then
              em2=emm + cthomson*tjc(kc,m)
           hj=(ai+bi)/2.d0+em2/(capat+cthomson)*(2.d0-pexdto-exdto)/4.d0
           else
        hj=FJ(exdto,pexdto,capat,emm,cthomson,ai,bi,fii)
           endif
        if(hj.lt.0.d0) hj=0.d0
                if(lf.eq.nf+1) then
                hjc=hj                                                     !tr2
                else
        som=som + prof *fint(lf)
        somjn=somjn + (hj - hjc)*prof *fint(lf)
        somjr=somjr + (hj - hjc)*fint(lf)
                endif
c
                if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
c
c transfert (kitt=1)
c               si HALO spherique rajouter des dilus
                exdtaur(lf,kr,m)=expa(-dhaut*V3*capal0*prof)       ! raie seule
                sourcer(lf,kr,m)=(emm + cthomson *hj)/(capat+cthomson)
                if(lcomp.eq.1) sourcer(lf,kr,m)=
     &             (emm + cthomson*tjc(kc,m))/(capat+cthomson)
                ddt=(1.d0-exdto*pexdto)/2.d0
                riplus(lf,kr)=(priplus(lf,kr)*exdto
     &                                  + sourcer(lf,kr,m)*ddt)
c               if(lf.eq.nf) rtaurl(kr)=rtaurl(kr)+dhaut*V3*capal0*prof !aile r
c
        if(lf.eq.1) hj0=hj
       hirp=priplus(lf,kr)+sourcer(lf,kr,m)*(1.d0-pexdto)/2.d0
       hirm=bi+sourcer(lf,kr,m)*(1.d0-exdto)/2.d0
       if(lf.eq.nf+1) hicp=hirp
       if(lf.eq.nf+1) hicm=hirm                                             !tr2
          if(lf.lt.nf+1) then
       somkbt=somkbt +
     &    bt*(1.d0/(capat+cthomson)-1.d0/(capabs+cthomson)) *fint(lf)
          endif   ! lf
c
        if(imp.ge.1.and.kimpm.eq.1.and.kimpf2.eq.1.and.
     &     ifich.ge.4.and.ifich.le.8.and.(kr.eq.2.or.kr.eq.10)) then
c        if(imp.ge.1.and.kimpm.eq.1.and.ifich.ne.9        utiliser le .tra
c     &     .and.(kr.eq.2.or.kr.eq.10.or.kr.eq.258.or.kr.eq.319
c     &     .or.kr.eq.379.or.kr.eq.771.or.kr.eq.778.or.kr.eq.786)) then
        if(lf.eq.nf+1) WRITE(21,*) ' lf  ;kr;dnu/dnudop  ;prof   ',
     &  '  ;J      ;dsomjr   ;dsomjn   ;Ir+-Ic+  ;Ir--Ic-'
        if(lf.ne.nf+1) WRITE(21,483) lf,kr,x,prof,hj,
     &  (hj-hjc)*fint(lf),(hj-hjc)*prof*fint(lf),hirp-hicp,hirm-hicm
        endif  ! imp krc
                endif   ! kitt                                              !tr2
 483                format(i3,';',i4,7(';',1pe9.2))
c
 310                                continue   ! lf
           emmco=0.d0
           djcodnu=0.d0
c les tableaux ...dnuco contiennent les photons sortis de la raie par
c diffusion Compton    (il faut soustraire des hjc coherents)
           if(m.eq.1.and.nitd.eq.1) dnuco(kr)=hnuev*
     &        6.060952d14 *sqrt(T*1.6863d-10 + hnuev**2*7.659162d-13)
           if(m.eq.2.and.nitd.eq.1.and.nitt.eq.1) dnuco(kr)=hnuev*
     &        6.060952d14*sqrt(temp(1)*1.6863d-10+hnuev**2*7.659162d-13)
c                  (Hz)   V2pi*e/h; kT(1)/mc2; 1/(5*511kev**2)    cf Ross 1978
           emcdnu=emmc*dnuco(kr)
       if(lcomp.eq.1) then
          emmco=emcdnu + somjr*u0*cthomson
             exdto=exdtauc(kc,m)
             pexdto=exdtauc(kc,m-1)
             ai=pripdnuco(1,kr)
             bi=rimdnuco(1,kr,m+1)
             if(nitf.eq.1.and.isuite.le.2) bi=(ai + debim(kc)) *fi
          hjco=FJ(exdto,pexdto,capabs,emmco,cthomson,ai,bi,fi)
             ai=pripdnuco(2,kr)
             bi=rimdnuco(2,kr,m+1)
             if(nitf.eq.1.and.isuite.le.2) bi=(ai + debim(kc)) *fi
          hjcoc=FJ(exdto,pexdto,capabs,emcdnu,cthomson,ai,bi,fi)
          djcodnu=hjco - hjcoc
       endif   ! lcomp=1
c
        somphi=som*u0/whz
        cjn(kr)=(hjc + somjn/som) *pi4 /hnuev/chel
        if(cjn(kr).lt.0.d0) cjn(kr)=0.d0
        tcjn(kr,m)=cjn(kr)
        tjr(kr,m)=somjr*u0 + djcodnu
c
        return
        end                                                            !tr2dirA
c capalt,demt,am,u0,kc,kr,lcomp
c tk1 am nf fint(lf) fii cthomson priplus(lf,kr) riplus(lf,kr) rimoins(lf,kr,m)
c dnuco(kr) pripdnuco(1,kr) rimdnuco(1,kr,m+1) debim(kc)
c thnur(kr) tcapc(kc,m) temc(kc) teer(kr,m) exdtauc(kc,m) exdtaur(lf,kr,m)
c tjc(kc,m) sourcer(lf,kr,m) rtaurl(kr) cjn(kr) tcjn(kr,m) tjr(kr,m)
c------------------------------------------------------------------------------
        subroutine TR2dirB(kc,kr,lcomp)

c preparation du transfert a 2 directions comme dans les premiers Titans
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15)
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/comptonrai/dnuco(nrais),sourdnuco(2,nrais,nzs+1),
     &   ripdnuco(2,nrais),pripdnuco(2,nrais),rimdnuco(2,nrais,0:nzs+2)
       common/stpo/tphot(nivtots),tphotk(26,10),dens(nivtots),
     &          alfinduit(nivtots),zfree,cthomson
       common/degmt2/exdtauc(322,0:nzs+1),exdtaur(16,nrais,0:nzs+1)
       common/cpal/temcac(322,nzs),tcapc(322,nzs),tcth(nzs),temc(322),
     &             pradc(nzs)
       common/tr2ab/emcdnu,emmco,hjco,hjcoc
          save /stpo/,/comptonrai/,/cpal/,/tr2ab/
c tr2dirB
        capabs=tcapc(kc,m)
        emmc=temc(kc)
       sourdnuco(2,kr,m)=(emcdnu + cthomson*hjcoc)/(capabs+cthomson)
          ddt=(1.d0-exdtauc(kc,m)*exdtauc(kc,m-1))/2.d0
       ripdnuco(2,kr)=(pripdnuco(2,kr)*exdtauc(kc,m)
     &                                  + sourdnuco(2,kr,m)*ddt)
       if(lcomp.eq.0) then
            ripdnuco(1,kr)=ripdnuco(2,kr)
            sourdnuco(1,kr,m)=sourdnuco(2,kr,m)
       else if(lcomp.eq.1) then
c            if(ncomp(kr).eq.0) mdebcomp(kr)=m
c          ncomp(kr)=ncomp(kr) + 1
          sourdnuco(1,kr,m)=(emmco + cthomson*hjco)/(capabs+cthomson)
          ripdnuco(1,kr)=(pripdnuco(1,kr)*exdtauc(kc,m)
     &                                  + sourdnuco(1,kr,m)*ddt)
       endif   ! lcomp=1
c
        return
        end                                                            !tr2dirB
c kc,kr,lcomp
c cthomson hjcoc emcdnu emmcohjco
c tcapc(kc,m) temc(kc) pripdnuco(1,kr) ripdnuco(2,kr) sourdnuco(2,kr,m)
c exdtauc(kc,m) 
c------------------------------------------------------------------------------
        function FJ(exdto,pexdto,capabs,emm,cthomson,ai,bi,fi)
c
       implicit double precision (a-h,o-z)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/im/fii,itali
c fj
              if(nitf.eq.1.and.isuite.le.1.and.irent.eq.0) then  ! PREMIER TOUR
        aux=(1.d0+fi)*(1.d0-pexdto) /(capabs+cthomson)/4.d0
            cim=bi                               ! si I-(z)=I+(z)*fii
c
                        else                                   ! TOURS SUIVANTS
        aux=(2.d0-pexdto-exdto) /(capabs+cthomson)/4.d0
        cim=bi
                        endif   ! nitf
        FJ=((ai + cim)/2.d0+ emm*aux) /(1.d0-cthomson*aux)
        return
        end                                                                 !fj
c exdto,pexdto,capabs,emm,cthomson,ai,bi,fi
c nitf isuite irent 
c--============================================================================
        subroutine sECRITUREf
c
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nrais=4200)
         double precision nht,ne
         character titre*78,tab*1,txt*46,fichatomic*51
         integer ijour(3)
       common/abond/ab(10)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/flux7/yloghnu(10),ylogfnu(10),a(9),b(9),cpl,npy
       common/im/fii,itali
       common/tabm/exflux(322),thnur(nrais)
       common/mec1/titre,fichatomic
       common/mec2/rayminrg,raymaxrg,hLbol,tauth,ndirlu
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/flx/flxdurincid,acompt,bcompt,ccompt,dcompt,ecompt,fcompt,
     &            scomptx1,scomptx2,ftauth
       common/drugi/ flin2,hnumin2,hnumax2,fxdofo,fluxex2          !Agata
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/bb/Tbb,wbb
c secriture-francais                                        ! formats libres 99
        external idate
        call idate(ijour)
c        tab=char(9)                                         ! tabulation
        tab=';'
c        tab=' '
c
        WRITE(6,80)  titre,ijour
        WRITE(14,80) titre,ijour   ! .res
        WRITE(30,80) titre,ijour   ! .spe
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,80) titre,ijour   ! .txt
        WRITE(17,80) titre,ijour   ! .tem
        WRITE(19,80) titre,ijour   ! .flu
        WRITE(23,80) titre,ijour   ! .bil
           endif
               if(igrid.eq.11.or.igrid.eq.12) goto 10
        txt='Coltot'
        if(igrid.eq.2) txt='Htot  '
                if(idens.eq.0) then
        WRITE(6,81)  densinit,txt,coltot
        WRITE(14,81) densinit,txt,coltot
        WRITE(30,81) densinit,txt,coltot
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,81) densinit,txt,coltot
        WRITE(17,81) densinit,txt,coltot
        WRITE(19,81) densinit,txt,coltot
        WRITE(23,81) densinit,txt,coltot
           endif
                endif
                if(idens.eq.1) then
        WRITE(6,82)  densinit,raymin,dendex
        WRITE(14,82) densinit,raymin,dendex
        WRITE(30,82) densinit,raymin,dendex
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,82) densinit,raymin,dendex
        WRITE(17,82) densinit,raymin,dendex
        WRITE(19,82) densinit,raymin,dendex
        WRITE(23,82) densinit,raymin,dendex
           endif
                endif
 10            continue
 80     format(a78/1x,i2,' -',i2,' -',i4,' TITAN106a (',
     &                     'transfert ALIcont+4000raies-UTA-public)')
c12 -12 -1234 TITAN106c (transfert ALIcont+4000raies-UTA-public)
 81     format(' NUAGE: densite constante Nh=',1pe10.3,
     &             ' - epaisseur totale ',a6,'=',1pe10.3)
 82     format(' NUAGE: densite en loi de puissance Nh=',1pe10.3,
     &             '*(',1pe9.2,'/R)**',0pf5.2)
 83     format('   de Rmin=',1pe10.3,' (',1pe10.3,'Rg) a Rmax=',
     &                                    1pe10.3,' (',1pe10.3,'Rg)')
c
        if(incid.eq.0) csit=0.d0
                if(chaufsup.gt.0.d0) then
        WRITE(14,180) chaufsup
        WRITE(30,180) chaufsup
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,180) chaufsup
        WRITE(17,180) chaufsup
        WRITE(19,180) chaufsup
        WRITE(23,180) chaufsup
           endif
                endif
                if(itemp.eq.1) then
        WRITE(14,181) itemp
        WRITE(30,181) itemp
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,181) itemp
        WRITE(17,181) itemp
        WRITE(19,181) itemp
        WRITE(23,181) itemp
           endif
                endif
                if(idens.eq.2.or.idens.eq.5) then
        WRITE(14,182) densinit,txt,coltot,idens                            !ef
        WRITE(30,182) densinit,txt,coltot,idens
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,182) densinit,txt,coltot,idens
        WRITE(17,182) densinit,txt,coltot,idens
        WRITE(19,182) densinit,txt,coltot,idens
        WRITE(23,182) densinit,txt,coltot,idens
           endif
                endif
                if(idens.eq.3.or.idens.eq.4.or.idens.ge.6) then
        WRITE(14,183) densinit,txt,coltot,idens
        WRITE(30,183) densinit,txt,coltot,idens
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,183) densinit,txt,coltot,idens
        WRITE(17,183) densinit,txt,coltot,idens
        WRITE(19,183) densinit,txt,coltot,idens
        WRITE(23,183) densinit,txt,coltot,idens
           endif
                endif
                if(idens.eq.10.or.idens.eq.20) then
        WRITE(14,*)'  recherche de T a n*T constant, solution froide'
        WRITE(30,*)'  recherche de T a n*T constant, solution froide'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*)'  recherche de T a n*T constant, solution froide'
        WRITE(17,*)'  recherche de T a n*T constant, solution froide'
        WRITE(19,*)'  recherche de T a n*T constant, solution froide'
        WRITE(23,*)'  recherche de T a n*T constant, solution froide'
           endif
                endif
                if(idens.eq.11.or.idens.eq.21) then
        WRITE(14,*)'  recherche de T a n*T constant, solution chaude'
        WRITE(30,*)'  recherche de T a n*T constant, solution chaude'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*)'  recherche de T a n*T constant, solution chaude'
        WRITE(17,*)'  recherche de T a n*T constant, solution chaude'
        WRITE(19,*)'  recherche de T a n*T constant, solution chaude'
        WRITE(23,*)'  recherche de T a n*T constant, solution chaude'
           endif
                endif
                if(idens.eq.12) then
        WRITE(14,*)'  recherche de T a n*T constant, solution instable'
        WRITE(30,*)'  recherche de T a n*T constant, solution instable'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*)'  recherche de T a n*T constant, solution instable'
        WRITE(17,*)'  recherche de T a n*T constant, solution instable'
        WRITE(19,*)'  recherche de T a n*T constant, solution instable'
        WRITE(23,*)'  recherche de T a n*T constant, solution instable'
           endif
                endif
                if(idens.eq.8) then
        WRITE(14,*)'  la recherche de T se fait a densite constante'
        WRITE(30,*)'  la recherche de T se fait a densite constante'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*)'  la recherche de T se fait a densite constante'
        WRITE(17,*)'  la recherche de T se fait a densite constante'
        WRITE(19,*)'  la recherche de T se fait a densite constante'
        WRITE(23,*)'  la recherche de T se fait a densite constante'
           endif
                endif                                                       !ef
                if(itemp.eq.10) then
        WRITE(14,184) Tinit
        WRITE(30,184) Tinit
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,184) Tinit
        WRITE(17,184) Tinit
        WRITE(19,184) Tinit
        WRITE(23,184) Tinit
           endif
                endif
                if(igrid.ge.13) then
        WRITE(14,185)
        WRITE(30,185)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,185)
        WRITE(17,185)
        WRITE(19,185)
        WRITE(23,185)
           endif
                endif
               if(incid.eq.0) then
        WRITE(14,186)
        WRITE(30,186)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,186)
        WRITE(17,186)
        WRITE(19,186)
        WRITE(23,186)
           endif
               else if(incid.eq.1.or.incid.eq.17.or.incid.eq.18) then  !Agata
        fff=exflux(1)
        if(incid.eq.1) fff=fluxex0
        WRITE(14,188) csit,hnumin,hnumax,fff
        WRITE(30,188) csit,hnumin,hnumax,fff
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,188) csit,hnumin,hnumax,fff
        WRITE(17,188) csit,hnumin,hnumax,fff
        WRITE(19,188) csit,hnumin,hnumax,fff
        WRITE(23,188) csit,hnumin,hnumax,fff
           endif
               else
        fff=exflux(1)
        WRITE(14,187) fff,csit                                              !ef
        WRITE(30,187) fff,csit
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,187) fff,csit
        WRITE(17,187) fff,csit
        WRITE(19,187) fff,csit
        WRITE(23,187) fff,csit
           endif
               endif
               if(incid.eq.1.or.incid.eq.17.or.incid.eq.18) then     !Agata
        WRITE(14,189) flindex,incid
        WRITE(30,189) flindex,incid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,189) flindex,incid
        WRITE(17,189) flindex,incid
        WRITE(19,189) flindex,incid
        WRITE(23,189) flindex,incid
           endif
               endif
               if(incid.eq.2) then
        WRITE(14,190) Tbb,wbb,incid
        WRITE(30,190) Tbb,wbb,incid
           if(ifich.ge.1.and.ifich.le.8) then                              !ef
        WRITE(15,190) Tbb,wbb,incid
        WRITE(17,190) Tbb,wbb,incid
        WRITE(19,190) Tbb,wbb,incid
        WRITE(23,190) Tbb,wbb,incid
           endif
               endif
               if(incid.ge.3.and.incid.le.9) then
        WRITE(14,191) incid
        WRITE(30,191) incid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,191) incid
        WRITE(17,191) incid
        WRITE(19,191) incid
        WRITE(23,191) incid
           endif
               endif
               if(incid.eq.10) then
        WRITE(14,293) npy,incid
        WRITE(14,294) (yloghnu(i),ylogfnu(i),i=1,npy)
        WRITE(30,293) npy,incid
        WRITE(30,294) (yloghnu(i),ylogfnu(i),i=1,npy)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,293) npy,incid
        WRITE(15,294) (yloghnu(i),ylogfnu(i),i=1,npy)
        WRITE(17,293) npy,incid
        WRITE(17,294) (yloghnu(i),ylogfnu(i),i=1,npy)
        WRITE(19,293) npy,incid
        WRITE(19,294) (yloghnu(i),ylogfnu(i),i=1,npy)
        WRITE(23,293) npy,incid
        WRITE(23,294) (yloghnu(i),ylogfnu(i),i=1,npy)
           endif
               endif
               if(incid.eq.11.or.incid.eq.12) then                         !ef
        WRITE(14,193) incid,fluxex0,csit
        WRITE(30,193) incid,fluxex0,csit
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,193) incid,fluxex0,csit
        WRITE(17,193) incid,fluxex0,csit
        WRITE(19,193) incid,fluxex0,csit
        WRITE(23,193) incid,fluxex0,csit
           endif
               endif
               if(incid.eq.13.or.incid.eq.14) then
        WRITE(14,192) csit,incid                                            !ef
        WRITE(30,192) csit,incid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,192) csit,incid
        WRITE(17,192) csit,incid
        WRITE(19,192) csit,incid
        WRITE(23,192) csit,incid
           endif
               endif
               if(incid.eq.15.or.incid.eq.16) then
        WRITE(14,198) incid
        WRITE(30,198) incid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,198) incid
        WRITE(17,198) incid
        WRITE(19,198) incid
        WRITE(23,198) incid
           endif
               endif
               if(incid.eq.18) then
        WRITE(14,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
        WRITE(30,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
        WRITE(17,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
        WRITE(19,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
        WRITE(23,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
           endif
               endif
               if(irent.eq.0) then                                         !ef
        WRITE(14,194)
        WRITE(30,194)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,194)
        WRITE(17,194)
        WRITE(19,194)
        WRITE(23,194)
           endif
               endif
        if(irent.eq.1.or.irent.eq.2.or.irent.eq.3.or.irent.eq.4) then
        WRITE(6,195)  Tbb,wbb,irent
        WRITE(14,195) Tbb,wbb,irent
        WRITE(30,195) Tbb,wbb,irent
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,195) Tbb,wbb,irent
        WRITE(17,195) Tbb,wbb,irent
        WRITE(19,195) Tbb,wbb,irent
        WRITE(23,195) Tbb,wbb,irent
           endif
               endif
               if(irent.eq.5) then                                         !ef
        WRITE(14,196) Tbb,wbb,irent
        WRITE(30,196) Tbb,wbb,irent
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,196) Tbb,wbb,irent
        WRITE(17,196) Tbb,wbb,irent
        WRITE(19,196) Tbb,wbb,irent
        WRITE(23,196) Tbb,wbb,irent
           endif
               endif
               if(irent.eq.6) then
        WRITE(14,197) wbb,irent
        WRITE(30,197) wbb,irent
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,197) wbb,irent
        WRITE(17,197) wbb,irent
        WRITE(19,197) wbb,irent
        WRITE(23,197) wbb,irent
           endif
               endif
        WRITE(14,86) fichatomic
        WRITE(30,86) fichatomic
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,86) fichatomic
        WRITE(17,86) fichatomic
        WRITE(19,86) fichatomic
        WRITE(23,86) fichatomic
           endif
 86     format(' DONNEES ATOMIQUES: fichier ',a51)                          !ef
        WRITE(14,87) (ab(j),j=2,10)
        WRITE(30,87) (ab(j),j=2,10)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,87) (ab(j),j=2,10)
        WRITE(17,87) (ab(j),j=2,10)
        WRITE(19,87) (ab(j),j=2,10)
        WRITE(23,87) (ab(j),j=2,10)
           endif
 87     format(' ABONDANCES/H:      He:',1pe10.3,'  C :',1pe10.3,
     &  '  N :',1pe10.3,'  O :',1pe10.3/5x,'Ne:',1pe10.3,'  Mg:',
     &  1pe10.3,'  Si:',1pe10.3,'  S :',1pe10.3,'  Fe:',1pe10.3)
               if(vturbkm.gt.0.d0) then
        WRITE(14,287) vturbkm
        WRITE(30,287) vturbkm
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,287) vturbkm
        WRITE(17,287) vturbkm
        WRITE(19,287) vturbkm
        WRITE(23,287) vturbkm
           endif
               endif
               if(igrid.eq.0) then
        WRITE(14,281) npt,igrid
        WRITE(30,281) npt,igrid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,281) npt,igrid
        WRITE(17,281) npt,igrid
        WRITE(19,281) npt,igrid
        WRITE(23,281) npt,igrid
           endif
               else if(igrid.lt.10) then
        dcmax=coltot/float(ndcm)
        WRITE(14,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
        WRITE(30,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
        WRITE(17,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
        WRITE(19,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
        WRITE(23,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
           endif
               else
        WRITE(14,282) npt,igrid
        WRITE(30,282) npt,igrid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,282) npt,igrid
        WRITE(17,282) npt,igrid
        WRITE(19,282) npt,igrid
        WRITE(23,282) npt,igrid
           endif
               endif
               txt='  '
               if(iacc.eq.1) txt=' acceleration-c'
               if(iacc.eq.2) txt=' acceleration-cr'
               if(iacc.eq.3) txt=' accelerat-crt'
        WRITE(14,284) fii,Tinit,txt(1:16)                                   !ef
        WRITE(30,284) fii,Tinit,txt(1:16)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,284) fii,Tinit,txt(1:16)
        WRITE(17,284) fii,Tinit,txt(1:16)
        WRITE(19,284) fii,Tinit,txt(1:16)
        WRITE(23,284) fii,Tinit,txt(1:16)
           endif
c ietl=0 Verner - ietl=1  suppression des recombinaisons dielectroniques
c et calcul des recombinaisons par integration de sigmav
c  nuage plan   pres de l ETL  (ietl=1: diel=0 & recomb<-sigma)
               if(ietl.eq.0) txt='loin de l ETL  (ietl=0)'
       if(ietl.eq.1)txt='pres de l ETL (ietl=1: diel=0 & recomb<-sigma)'
        WRITE(14,*) '   nuage plan - ',txt
        WRITE(30,*) '   nuage plan - ',txt
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*) '   nuage plan - ',txt
        WRITE(17,*) '   nuage plan - ',txt
        WRITE(19,*) '   nuage plan - ',txt
        WRITE(23,*) '   nuage plan - ',txt
           endif
               if(ndirlu.ge.2) then
                  if(incang.eq.0) then
        WRITE(14,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident semi-isotrope'
        WRITE(30,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident semi-isotrope'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident semi-isotrope'
        WRITE(17,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident semi-isotrope'
        WRITE(19,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident semi-isotrope'
        WRITE(23,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident semi-isotrope'
           endif
                  else if(incang.eq.1) then
        WRITE(14,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident normal'
        WRITE(30,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident normal'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident normal'
        WRITE(17,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident normal'
        WRITE(19,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident normal'
        WRITE(23,*) '   calcul avec',ndir,' directions'//
     &              ' - rayonnement incident normal'
           endif
                  else
        WRITE(14,*) '   calcul avec',ndir,' directions'//
     &              ' - incident dans la direction',incang
        WRITE(30,*) '   calcul avec',ndir,' directions'//
     &              ' - incident dans la direction',incang
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*) '   calcul avec',ndir,' directions'//
     &              ' - incident dans la direction',incang
        WRITE(17,*) '   calcul avec',ndir,' directions'//
     &              ' - incident dans la direction',incang
        WRITE(19,*) '   calcul avec',ndir,' directions'//
     &              ' - incident dans la direction',incang
        WRITE(23,*) '   calcul avec',ndir,' directions'//
     &              ' - incident dans la direction',incang
           endif
                  endif   ! incang
               endif   ! ndirlu
               if(isf.le.1) then
      WRITE(14,'(a,a,i1,a)')'    solution formelle avec approximation',
     &                  ' de S lineaire                  (isf=',isf,')'
      WRITE(30,'(a,a,i1,a)')'    solution formelle avec approximation',
     &                  ' de S lineaire                  (isf=',isf,')'
           if(ifich.ge.1.and.ifich.le.8) then
      WRITE(15,'(a,a,i1,a)')'    solution formelle avec approximation',
     &                  ' de S lineaire                  (isf=',isf,')'
      WRITE(17,'(a,a,i1,a)')'    solution formelle avec approximation',
     &                  ' de S lineaire                  (isf=',isf,')'
      WRITE(19,'(a,a,i1,a)')'    solution formelle avec approximation',
     &                  ' de S lineaire                  (isf=',isf,')'
      WRITE(23,'(a,a,i1,a)')'    solution formelle avec approximation',
     &                  ' de S lineaire                  (isf=',isf,')'
           endif
               else if(isf.eq.2) then
        WRITE(14,*) '   solution formelle avec approximation de S',
     &              ' parabolique               (isf=2)'
        WRITE(30,*) '   solution formelle avec approximation de S',
     &              ' parabolique               (isf=2)'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*) '   solution formelle avec approximation de S',
     &              ' parabolique               (isf=2)'
        WRITE(17,*) '   solution formelle avec approximation de S',
     &              ' parabolique               (isf=2)'
        WRITE(19,*) '   solution formelle avec approximation de S',
     &              ' parabolique               (isf=2)'
        WRITE(23,*) '   solution formelle avec approximation de S',
     &              ' parabolique               (isf=2)'
           endif
               else if(isf.ge.3) then
        WRITE(14,'(a,a,i1,a)')'   solution formelle avec approximation',
     &              ' de S lineaire en cas de pb      (isf=',isf,')'
        WRITE(30,'(a,a,i1,a)')'   solution formelle avec approximation',
     &              ' de S lineaire en cas de pb      (isf=',isf,')'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,'(a,a,i1,a)')'   solution formelle avec approximation',
     &              ' de S lineaire en cas de pb      (isf=',isf,')'
        WRITE(17,'(a,a,i1,a)')'   solution formelle avec approximation',
     &              ' de S lineaire en cas de pb      (isf=',isf,')'
        WRITE(19,'(a,a,i1,a)')'   solution formelle avec approximation',
     &              ' de S lineaire en cas de pb      (isf=',isf,')'
        WRITE(23,'(a,a,i1,a)')'   solution formelle avec approximation',
     &              ' de S lineaire en cas de pb      (isf=',isf,')'
           endif
               endif
c
               if(irecsup.eq.0.or.irecsup.ge.3) then
        WRITE(14,*)
     &  '   recomb.supplementaires negligees               (irecsup=0)'
        WRITE(30,*)
     &  '   recomb.supplementaires negligees               (irecsup=0)'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*)
     &  '   recomb.supplementaires negligees               (irecsup=0)'
        WRITE(17,*)
     &  '   recomb.supplementaires negligees               (irecsup=0)'
        WRITE(19,*)
     &  '   recomb.supplementaires negligees               (irecsup=0)'
        WRITE(23,*)
     &  '   recomb.supplementaires negligees               (irecsup=0)'
           endif
               endif
               if(irecsup.eq.1) then                                        !ef
        WRITE(14,*)
     &  '   recomb.supplementaires sur le dernier niv      (irecsup=1)'
        WRITE(30,*)
     &  '   recomb.supplementaires sur le dernier niv      (irecsup=1)'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*)
     &  '   recomb.supplementaires sur le dernier niv      (irecsup=1)'
        WRITE(17,*)
     &  '   recomb.supplementaires sur le dernier niv      (irecsup=1)'
        WRITE(19,*)
     &  '   recomb.supplementaires sur le dernier niv      (irecsup=1)'
        WRITE(23,*)
     &  '   recomb.supplementaires sur le dernier niv      (irecsup=1)'
           endif
               endif
               if(irecsup.eq.2) then
        WRITE(14,*)
     &'   recomb.supplementaires reparties sur tous les niv (irecsup=2)'
        WRITE(30,*)
     &'   recomb.supplementaires reparties sur tous les niv (irecsup=2)'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*)
     &'   recomb.supplementaires reparties sur tous les niv (irecsup=2)'
        WRITE(17,*)
     &'   recomb.supplementaires reparties sur tous les niv (irecsup=2)'
        WRITE(19,*)
     &'   recomb.supplementaires reparties sur tous les niv (irecsup=2)'
        WRITE(23,*)
     &'   recomb.supplementaires reparties sur tous les niv (irecsup=2)'
           endif
               endif
           if(iprof.le.1) then
        WRITE(14,285) nraimax,nf,aint,VTref*VTref                              
        WRITE(30,285) nraimax,nf,aint,VTref*VTref
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,285) nraimax,nf,aint,VTref*VTref
        WRITE(17,285) nraimax,nf,aint,VTref*VTref
        WRITE(19,285) nraimax,nf,aint,VTref*VTref
        WRITE(23,285) nraimax,nf,aint,VTref*VTref
           endif
           else if(iprof.eq.2) then
        WRITE(14,286) nraimax,nf,aint,VTref*VTref                          !ef
        WRITE(30,286) nraimax,nf,aint,VTref*VTref
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,286) nraimax,nf,aint,VTref*VTref
        WRITE(17,286) nraimax,nf,aint,VTref*VTref
        WRITE(19,286) nraimax,nf,aint,VTref*VTref
        WRITE(23,286) nraimax,nf,aint,VTref*VTref
           endif
           endif   ! iprof
           if(iprof.eq.1) then
        WRITE(14,*)
     &     '       profil Doppler pour Lya des H- & He-like +C4 +O6'
        WRITE(30,*) 
     &     '       profil Doppler pour Lya des H- & He-like +C4 +O6'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*) 
     &     '       profil Doppler pour Lya des H- & He-like +C4 +O6'
        WRITE(17,*) 
     &     '       profil Doppler pour Lya des H- & He-like +C4 +O6'
        WRITE(19,*) 
     &     '       profil Doppler pour Lya des H- & He-like +C4 +O6'
        WRITE(23,*) 
     &     '       profil Doppler pour Lya des H- & He-like +C4 +O6'
           endif
           endif   ! iprof
               if(icompt.eq.0) then
        WRITE(14,*) '       pas de comptonisation des raies'
        WRITE(30,*) '       pas de comptonisation des raies'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*) '       pas de comptonisation des raies'
        WRITE(17,*) '       pas de comptonisation des raies'
        WRITE(19,*) '       pas de comptonisation des raies'
        WRITE(23,*) '       pas de comptonisation des raies'
           endif
               else
        WRITE(14,288) icompt
        WRITE(30,288) icompt
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,288) icompt
        WRITE(17,288) icompt
        WRITE(19,288) icompt
        WRITE(23,288) icompt
           endif
               endif   ! icompt
               if(acompt.eq.0.d0.and.bcompt.eq.0.d0) then
        WRITE(14,*)'gain Compton: formule approximative de Tarter'
        WRITE(30,*)'gain Compton: formule approximative de Tarter'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,*)'gain Compton: formule approximative de Tarter'
        WRITE(17,*)'gain Compton: formule approximative de Tarter'
        WRITE(19,*)'gain Compton: formule approximative de Tarter'
        WRITE(23,*)'gain Compton: formule approximative de Tarter'
           endif
               else
        WRITE(14,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
        WRITE(30,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt             !ef
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
        WRITE(17,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
        WRITE(19,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
        WRITE(23,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
           endif
               endif   ! acompt
               if(isuite.ge.2) then
        WRITE(14,291) isuite
        WRITE(30,291) isuite
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,291) isuite
        WRITE(17,291) isuite
        WRITE(19,291) isuite
        WRITE(23,291) isuite
           endif
               endif
c        WRITE(15,292) tauth
        WRITE(14,*) 
        WRITE(30,*)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(17,*)
        WRITE(19,*)
        WRITE(23,*)
           endif
 180  format(' chauffage supplementaire constant=',1pe10.3,
     &                                  'erg/cm3/(NhNe)')
 181  format(' au dela de z=0.98*H on impose une temperature',
     &                                        ' constante: itemp=',i2)
 182  format(' NUAGE: densite initiale Nh=',1pe10.3,' - epaisseur ',
     &        'totale ',a6,'=',1pe10.3/'   calcul avec PRESSION ',
     &        'GAZEUSE constante',28x,'(idens=',i2,')')
 183  format(' NUAGE: densite initiale Nh=',1pe10.3,' - epaisseur ',
     &        'totale ',a6,'=',1pe10.3/'   calcul avec PRESSION ',
     &        'TOTALE constante',29x,'(idens=',i2,')')
 184  format(' PAS DE RECHERCHE D''EQUILIBRE THERMIQUE : temperature',
     &                                          ' constante=',1pe10.3)
 185  format(' PAS DE RECHERCHE D''EQUILIBRE ',
     &              'THERMIQUE : temperature imposee lue   (itemp=11)')
 186  format(' Nuage NON ECLAIRE a temperature constante')
 187  format(' ECLAIREMENT INCIDENT:  - flux(13.6ev)=',1pg10.3,
     &                                  ' csit=',1pg12.5)
 188  format(' ECLAIREMENT INCIDENT: parametre d''ionisation csit=',
     &       1pg12.5/'   hnu de',0pf7.3,' a',1pg10.3,
     &       'ev - flux(13.6ev)=',1pg10.3)                                  !ef
c     &  1pg10.3,' -  Lbol=',1pe10.3)
 189  format('   loi de puissance d''index',0pf5.2,37x,'(incid=',i2,')')!234567
 190  format('   corps noir de temperature Tbb=',1pe10.3,
     &             ' multiplie par',1pg10.3,' (incid=',i2,')')
 191  format('   spectre donne analytiquement dans une subroutine ',
     &                                   'FFLUX  (incid=',i2,')')
 192  format('   flux entre ds le fichier FLi. (format.res) csit=',
     &              1pg12.5,' (incid=',i2,')')
 293  format('   spectre donne par',i3,' points: loghnuev logflux     ',
     &           ' (incid=',i2,')')
 294  format(5x,1pg12.5,1pg12.5,';',1pg12.5,1pg12.5,';',
     &            1pg12.5,1pg12.5,';')
 193  format('   spectre entre dans le fichier FLi. (format libre',
     &              ',kc croissants,incid=',i2,')'/20x,'(multiplie par',
     &              1pg12.5, ') csit=',1pg12.5)
 198  format('   spectre entre par le fichier FLi',34x,'(incid=',i2,')')
 199  format('   2e loi de puissance (avec cut-off exp): hnumin2=',
     &       0pf7.3,' hnumax2',1pg10.3/'    index2=',0pf5.2,' fxdofo=',
     &       0pf10.5,' A1=',1pg10.3,' A2/A1=',1pg10.3)
 194  format(' pas d''eclairement par l''arriere',37x,'(irent= 0)')
 195  format(' ECLAIREMENT ARRIERE: corps noir de temperature',
     &                         1pe10.3,' *',1pg10.3,'(irent=',i2,')')
 196  format(' ECLAIREMENT ARRIERE: loi de Wien de temperature',
     &                         1pe10.3,'*',1pg10.3,'(irent=',i2,')')
 197  format(' ECLAIREMENT ARRIERE egal a l''incident multiplie par ',
     &                         1pg10.3,'       (irent=',i2,')')
 287  format(' MICROTURBULENCE: vitesse =',f8.0,'km/s')
 280  format(/' DECOUPAGE: a partir de NH=',1pe9.2,':',i3,' strates/',  !2345678
     &   'decade'/'    puis: strates egaux avec ',
     &   'dcmax=',1pe9.2,'  (ndcm=',i4,')'/'    redivises en',0pf6.1,
     &   ' entre',1pe9.2,' et',1pe9.2/'    fin:',i3,' strates/decade',
     &   ' jusqu''a dcol=',1pe9.2,20x,'(igrid=',i2,')')
 281  format(/' DECOUPAGE en ',i3,' strates egaux       (igrid=',i2,')')
 282  format(/' DECOUPAGE &/ou densites donnes dans le fichier MZN. en',
     &    i4,' strates  (igrid=',i2,')')
 284  format(' OPTIONS DE CALCUL: I-/I+init=',0pf8.5,
     &                                  ' Tinit=',1pe10.3,a16)
 285  format(' raies: nombre total :',i5/
     &       '        profil Voigt,transfert:',i2,' pts nu-nu0=',
     &                0pf5.1,'*(1/ygau^3-1)*dnudop(',1pg7.1,')')
 286  format(' raies: nombre total :',i5/
     &       '        prof.Doppler,transfert:',i2,' pts nu-nu0=',
     &                0pf5.1,'*(1/ygau^3-1)*dnudop(',1pg7.1,')')
 288  format('        comptonisation des raies si dEcompmoy >',i2,
     &    '*FWHM')
 290  format(' gain Compton=(',f8.5,'*exp(-',f8.5,'*tauth du bord)+',
     &    f8.5,'*exp(-',f8.5,'*t)'/'    (X>26kev)       +',f8.5,
     &    '*exp(-',f8.3,'*t))  *Netot*sigth*flxdurincid/NeNh')
 291  format(' SUITE D UN PRECEDENT PASSAGE',39x,'(isuite=',i2,')')
 292  format(/' tau thomson environ',1pg10.3)
c
 91     format('strate  ',a,'z         ',a,'T   ',a,'conv ',a,'nht',
     &  7x,a,'dh     ',a,'dtaucmax  ',a,'hnumax   ',a,'coldens'/
     &                1pe7.0,8(a,1pe8.1))
 92     format('     hnuev  ',a,'kc ',a,'Lentrant  ',a,'Lsortant ',a,
     &  'Lreflechi',a,'Ltransmisap ',a,'tauceff',a/1pe7.0,8(a,1pe8.1))
   90   format(/)
c
        return
        end                                                         !secrituref
c------------------------------------------------------------------------------
        subroutine sECRITUREe
c
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nrais=4200)
         double precision nht,ne
         character titre*78,tab*1,txt*46,fichatomic*51
         integer ijour(3)
       common/abond/ab(10)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/drugi/ flin2,hnumin2,hnumax2,fxdofo,fluxex2          !Agata
       common/flux7/yloghnu(10),ylogfnu(10),a(9),b(9),cpl,npy
       common/im/fii,itali
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/tabm/exflux(322),thnur(nrais)
       common/mec1/titre,fichatomic
       common/mec2/rayminrg,raymaxrg,hLbol,tauth,ndirlu
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/flx/flxdurincid,acompt,bcompt,ccompt,dcompt,ecompt,fcompt,
     &            scomptx1,scomptx2,ftauth
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/bb/Tbb,wbb
c secriture-english                                         ! formats libres 99
        external idate
        call idate(ijour)
c        tab=char(9)                                         ! tabulation
        tab=' '
c
        WRITE(6,80)  titre,ijour
        WRITE(14,80) titre,ijour   ! .res
        WRITE(30,80) titre,ijour   ! .spe
           if(ifich.ge.1.and.ifich.le.8) then
        if(iexpert.eq.1) WRITE(15,80) titre,ijour   ! .txt
        WRITE(17,80) titre,ijour   ! .tem
        WRITE(19,80) titre,ijour   ! .flu
        WRITE(23,80) titre,ijour   ! .bil
           endif
               if(igrid.eq.11.or.igrid.eq.12) goto 10
        txt='Coltot'
        if(igrid.eq.2) txt='Htot  '
                if(idens.eq.0) then
        WRITE(6,81)  densinit,txt,coltot
        WRITE(14,81) densinit,txt,coltot
        WRITE(30,81) densinit,txt,coltot
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,81) densinit,txt,coltot
        WRITE(17,81) densinit,txt,coltot
        WRITE(19,81) densinit,txt,coltot
        WRITE(23,81) densinit,txt,coltot
           endif
                endif
                if(idens.eq.1) then
        WRITE(6,82)  densinit,raymin,dendex
        WRITE(30,82) densinit,raymin,dendex
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(14,82) densinit,raymin,dendex
        WRITE(15,82) densinit,raymin,dendex
        WRITE(17,82) densinit,raymin,dendex
        WRITE(19,82) densinit,raymin,dendex
        WRITE(23,82) densinit,raymin,dendex
           endif
                endif                                                     !ee
 10            continue
 80    format('#',a78/'#',1x,i2,' -',i2,' -',i4,' TITAN106a'
     &             ,'(transfALIcont+line,UTA)')
 81    format('# CLOUD: constant density Nh=',1pe10.3,
     &             ' - total thickness ',a6,'=',1pe10.3)
 82    format('# CLOUD: power law density Nh=',1pe10.3,
     &             '*(',1pe9.2,'/R)**',0pf5.2)
c01 -12 -2002 TITAN106a(transfALIcont+line,UTA)
c
        if(incid.eq.0) csit=0.d0
                if(chaufsup.gt.0.d0) then
        WRITE(14,180) chaufsup
        WRITE(30,180) chaufsup
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,180) chaufsup
        WRITE(17,180) chaufsup
        WRITE(19,180) chaufsup
        WRITE(23,180) chaufsup
           endif
                endif
        txt='Coltot'
        if(igrid.eq.2) txt='Htot  '
                if(itemp.eq.1) then
        WRITE(14,181) itemp
        WRITE(30,181) itemp
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,181) itemp
        WRITE(17,181) itemp
        WRITE(19,181) itemp
        WRITE(23,181) itemp
           endif
                endif                                                       !ee
                if(idens.eq.2.or.idens.eq.5) then
        WRITE(14,182) densinit,txt,coltot,idens
        WRITE(30,182) densinit,txt,coltot,idens
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,182) densinit,txt,coltot,idens
        WRITE(17,182) densinit,txt,coltot,idens
        WRITE(19,182) densinit,txt,coltot,idens
        WRITE(23,182) densinit,txt,coltot,idens
           endif
                endif
          if(idens.eq.3.or.idens.eq.4.or.idens.ge.6) then
        WRITE(14,183) densinit,txt,coltot,idens
        WRITE(30,183) densinit,txt,coltot,idens
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,183) densinit,txt,coltot,idens
        WRITE(17,183) densinit,txt,coltot,idens
        WRITE(19,183) densinit,txt,coltot,idens
        WRITE(23,183) densinit,txt,coltot,idens
           endif
                endif
                if(idens.eq.10.or.idens.eq.20) then
        WRITE(14,'(a)')
     &        '#  searching T with constant pressure,cold solution'
        WRITE(30,'(a)')
     &        '#  searching T with constant pressure,cold solution'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,'(a)')
     &        '#  searching T with constant pressure,cold solution'
        WRITE(17,'(a)')
     &        '#  searching T with constant pressure,cold solution'
        WRITE(19,'(a)')
     &        '#  searching T with constant pressure,cold solution'
        WRITE(23,'(a)')
     &        '#  searching T with constant pressure,cold solution'
           endif
                endif
                if(idens.eq.11.or.idens.eq.21) then
        WRITE(14,'(a)')
     &        '#  searching T with constant pressure, hot solution'
        WRITE(30,'(a)')
     &        '#  searching T with constant pressure, hot solution'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,'(a)')
     &        '#  searching T with constant pressure, hot solution'
        WRITE(17,'(a)')
     &        '#  searching T with constant pressure, hot solution'
        WRITE(19,'(a)')
     &        '#  searching T with constant pressure, hot solution'
        WRITE(23,'(a)')
     &        '#  searching T with constant pressure, hot solution'
           endif
                endif
                if(idens.eq.12) then
        WRITE(14,'(a)')
     &       '#  searching T with constant pressure, instable solution'
        WRITE(30,'(a)')
     &       '#  searching T with constant pressure, instable solution'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,'(a)')
     &       '#  searching T with constant pressure, instable solution'
        WRITE(17,'(a)')
     &       '#  searching T with constant pressure, instable solution'
        WRITE(19,'(a)')
     &       '#  searching T with constant pressure, instable solution'
        WRITE(23,'(a)')
     &       '#  searching T with constant pressure, instable solution'
           endif
                endif
                if(idens.eq.8) then
        WRITE(14,'(a)')'#  searching T with constant density'
        WRITE(30,'(a)')'#  searching T with constant density'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,'(a)')'#  searching T with constant density'
        WRITE(17,'(a)')'#  searching T with constant density'
        WRITE(19,'(a)')'#  searching T with constant density'
        WRITE(23,'(a)')'#  searching T with constant density'
           endif
                endif                                                       !ee
                if(itemp.eq.10) then
        WRITE(14,184) Tinit
        WRITE(30,184) Tinit
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,184) Tinit
        WRITE(17,184) Tinit
        WRITE(19,184) Tinit
        WRITE(23,184) Tinit
           endif
                endif
                if(igrid.ge.13) then
        WRITE(14,185)
        WRITE(30,185)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,185)
        WRITE(17,185)
        WRITE(19,185)
        WRITE(23,185)
           endif
                endif                                                       !ee
               if(incid.eq.0) then
        WRITE(14,186)
        WRITE(30,186)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,186)
        WRITE(17,186)
        WRITE(19,186)
        WRITE(23,186)
           endif
               else if(incid.eq.1.or.incid.eq.17.or.incid.eq.18) then !Agata
        fff=exflux(1)
        if(incid.eq.1) fff=fluxex0
        WRITE(14,188) csit,hnumin,hnumax,fff
        WRITE(30,188) csit,hnumin,hnumax,fff
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,188) csit,hnumin,hnumax,fff
        WRITE(17,188) csit,hnumin,hnumax,fff
        WRITE(19,188) csit,hnumin,hnumax,fff
        WRITE(23,188) csit,hnumin,hnumax,fff
           endif
               else
        fff=exflux(1)
        WRITE(14,187) fff,csit
        WRITE(30,187) fff,csit
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,187) fff,csit
        WRITE(17,187) fff,csit
        WRITE(19,187) fff,csit
        WRITE(23,187) fff,csit
           endif
               endif                                                        !ee
               if(incid.eq.1.or.incid.eq.17.or.incid.eq.18) then   !Agata
        WRITE(14,189) flindex,incid
        WRITE(30,189) flindex,incid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,189) flindex,incid
        WRITE(17,189) flindex,incid
        WRITE(19,189) flindex,incid
        WRITE(23,189) flindex,incid
           endif
               endif
               if(incid.eq.2) then
        WRITE(14,190) Tbb,wbb,incid
        WRITE(30,190) Tbb,wbb,incid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,190) Tbb,wbb,incid
        WRITE(17,190) Tbb,wbb,incid
        WRITE(19,190) Tbb,wbb,incid
        WRITE(23,190) Tbb,wbb,incid
           endif
               endif
               if(incid.ge.3.and.incid.le.9) then                         !ee
        WRITE(14,191) incid
        WRITE(30,191) incid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,191) incid
        WRITE(17,191) incid
        WRITE(19,191) incid
        WRITE(23,191) incid
           endif
               endif
               if(incid.eq.10) then
        WRITE(14,293) npy,incid
        WRITE(14,294) (yloghnu(i),ylogfnu(i),i=1,npy)
        WRITE(30,293) npy,incid
        WRITE(30,294) (yloghnu(i),ylogfnu(i),i=1,npy)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,293) npy,incid
        WRITE(15,294) (yloghnu(i),ylogfnu(i),i=1,npy)
        WRITE(17,293) npy,incid
        WRITE(17,294) (yloghnu(i),ylogfnu(i),i=1,npy)
        WRITE(19,293) npy,incid
        WRITE(19,294) (yloghnu(i),ylogfnu(i),i=1,npy)
        WRITE(23,293) npy,incid
        WRITE(23,294) (yloghnu(i),ylogfnu(i),i=1,npy)
           endif
               endif
               if(incid.eq.11.or.incid.eq.12) then
        WRITE(14,193) incid,fluxex0,csit
        WRITE(30,193) incid,fluxex0,csit
           if(ifich.ge.1.and.ifich.le.8) then                             !ee
        WRITE(15,193) incid,fluxex0,csit
        WRITE(17,193) incid,fluxex0,csit
        WRITE(19,193) incid,fluxex0,csit
        WRITE(23,193) incid,fluxex0,csit
           endif
               endif
               if(incid.eq.13.or.incid.eq.14) then
        WRITE(14,192) csit,incid
        WRITE(30,192) csit,incid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,192) csit,incid
        WRITE(17,192) csit,incid
        WRITE(19,192) csit,incid
        WRITE(23,192) csit,incid
           endif
               endif                                                        !ee
               if(incid.eq.15.or.incid.eq.16) then
        WRITE(14,198) incid
        WRITE(30,198) incid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,198) incid
        WRITE(17,198) incid
        WRITE(19,198) incid
        WRITE(23,198) incid
           endif
               endif
               if(incid.eq.18) then
        WRITE(14,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
        WRITE(30,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
        WRITE(17,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
        WRITE(19,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
        WRITE(23,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
           endif
               endif
               if(irent.eq.0) then
        WRITE(14,194)
        WRITE(30,194)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,194)
        WRITE(17,194)
        WRITE(19,194)
        WRITE(23,194)
           endif
               endif
        if(irent.eq.1.or.irent.eq.2.or.irent.eq.3.or.irent.eq.4) then
        WRITE(6,195)  Tbb,wbb,irent
        WRITE(14,195) Tbb,wbb,irent
        WRITE(30,195) Tbb,wbb,irent
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,195) Tbb,wbb,irent
        WRITE(17,195) Tbb,wbb,irent
        WRITE(19,195) Tbb,wbb,irent
        WRITE(23,195) Tbb,wbb,irent
           endif
               endif
               if(irent.eq.5) then                                         !ee
        WRITE(14,196) Tbb,wbb,irent
        WRITE(30,196) Tbb,wbb,irent
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,196) Tbb,wbb,irent
        WRITE(17,196) Tbb,wbb,irent
        WRITE(19,196) Tbb,wbb,irent
        WRITE(23,196) Tbb,wbb,irent
           endif
               endif
               if(irent.eq.6) then
        WRITE(14,197) wbb,irent
        WRITE(30,197) wbb,irent
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,197) wbb,irent
        WRITE(17,197) wbb,irent
        WRITE(19,197) wbb,irent
        WRITE(23,197) wbb,irent
           endif
               endif                                                        !ee
        WRITE(14,86) fichatomic
        WRITE(30,86) fichatomic
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,86) fichatomic
        WRITE(17,86) fichatomic
        WRITE(19,86) fichatomic
        WRITE(23,86) fichatomic
           endif
 86     format('# ATOMIC DATA : file ',a51)
        WRITE(14,87) (ab(j),j=2,10)
        WRITE(30,87) (ab(j),j=2,10)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,87) (ab(j),j=2,10)
        WRITE(17,87) (ab(j),j=2,10)
        WRITE(19,87) (ab(j),j=2,10)
        WRITE(23,87) (ab(j),j=2,10)
           endif
 87     format('# ABUNDANCES/H:     He:',1pe10.3,'  C :',1pe10.3,
     &  '  N :',1pe10.3,'  O :',1pe10.3/'#',4x,'Ne:',1pe10.3,'  Mg:',
     &  1pe10.3,'  Si:',1pe10.3,'  S :',1pe10.3,'  Fe:',1pe10.3)
               if(vturbkm.gt.0.d0) then
        WRITE(14,287) vturbkm
        WRITE(30,287) vturbkm
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,287) vturbkm
        WRITE(17,287) vturbkm
        WRITE(19,287) vturbkm
        WRITE(23,287) vturbkm
           endif
               endif
               if(igrid.eq.0) then
        WRITE(14,281) npt,igrid
        WRITE(30,281) npt,igrid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,281) npt,igrid
        WRITE(17,281) npt,igrid
        WRITE(19,281) npt,igrid
        WRITE(23,281) npt,igrid
           endif
               else if(igrid.lt.10) then
        dcmax=coltot/float(ndcm)
        WRITE(14,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
        WRITE(30,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
        WRITE(17,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
        WRITE(19,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
        WRITE(23,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
     &                finchute,npdf,dcolf,igrid
           endif
               else
        WRITE(14,282) npt,igrid
        WRITE(30,282) npt,igrid
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,282) npt,igrid
        WRITE(17,282) npt,igrid
        WRITE(19,282) npt,igrid
        WRITE(23,282) npt,igrid
           endif
               endif                                                        !ee
               txt='  '
               if(iacc.eq.1) txt=' accelerat.-c'
               if(iacc.eq.2) txt=' accelerat.-cr'
               if(iacc.eq.3) txt=' accelerat.-crt'
        WRITE(14,284) fii,Tinit,txt(1:16)
        WRITE(30,284) fii,Tinit,txt(1:16)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,284) fii,Tinit,txt(1:16)
        WRITE(17,284) fii,Tinit,txt(1:16)
        WRITE(19,284) fii,Tinit,txt(1:16)
        WRITE(23,284) fii,Tinit,txt(1:16)
           endif
c ietl=0 Verner - ietl=1   dielectronic recombinations suppressed
c and computation of recombinations by integral of sigma
c  plane parallel cloud   near ETL  (ietl=1: diel=0 & recomb<-sigma)
               if(ietl.eq.0) then
        txt='far from LTE  (ietl=0)'
       WRITE(14,'(a)')'#   plane parallel cloud - '//txt(1:41)
       WRITE(30,'(a)')'#   plane parallel cloud - '//txt(1:41)
           if(ifich.ge.1.and.ifich.le.8) then
       WRITE(15,'(a)')'#   plane parallel cloud - '//txt(1:41)
       WRITE(17,'(a)')'#   plane parallel cloud - '//txt(1:41)
       WRITE(19,'(a)')'#   plane parallel cloud - '//txt(1:41)
       WRITE(23,'(a)')'#   plane parallel cloud - '//txt(1:41)
          endif
               else if(ietl.eq.1) then
        txt='near LTE (ietl=1: diel=0 & recomb<-sigma)'
       WRITE(14,'(a)')'#   plane parallel cloud - '//txt(1:41)
       WRITE(30,'(a)')'#   plane parallel cloud - '//txt(1:41)
           if(ifich.ge.1.and.ifich.le.8) then
       WRITE(15,'(a)')'#   plane parallel cloud - '//txt(1:41)
       WRITE(17,'(a)')'#   plane parallel cloud - '//txt(1:41)
       WRITE(19,'(a)')'#   plane parallel cloud - '//txt(1:41)
       WRITE(23,'(a)')'#   plane parallel cloud - '//txt(1:41)
          endif
               endif
               if(ndirlu.ge.2) then
                  if(incang.eq.0) then
        WRITE(14,393) ndirlu
        WRITE(30,393) ndirlu
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,393) ndirlu
        WRITE(17,393) ndirlu
        WRITE(19,393) ndirlu
        WRITE(23,393) ndirlu
           endif
 393    format('#   calculation for',i4,' directions',
     &                   ' - semi-isotropic incident radiation')
                  else if(incang.eq.1) then
        WRITE(14,394) ndir
        WRITE(30,394) ndir
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,394) ndir
        WRITE(17,394) ndir
        WRITE(19,394) ndir
        WRITE(23,394) ndir
           endif
 394    format('#   calculation for',i4,' directions',
     &                   ' - normal incident radiation')
                  else
        WRITE(14,395) ndirlu,incang
        WRITE(30,395) ndirlu,incang
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,395) ndirlu,incang
        WRITE(17,395) ndirlu,incang
        WRITE(19,395) ndirlu,incang
        WRITE(23,395) ndirlu,incang
           endif
 395    format('#   calculation for',i4,' directions',
     &                   ' - incident radiation in direction',i3)
                  endif   ! incang
               endif   ! ndirlu
               if(isf.le.1) then
       WRITE(14,'(a)')
     &'#   formal solution with linear approximation for S     (isf=1)'
       WRITE(30,'(a)')
     &'#   formal solution with linear approximation for S     (isf=1)'
           if(ifich.ge.1.and.ifich.le.8) then
       WRITE(15,'(a)')
     &'#   formal solution with linear approximation for S     (isf=1)'
       WRITE(17,'(a)')
     &'#   formal solution with linear approximation for S     (isf=1)'
       WRITE(19,'(a)')
     &'#   formal solution with linear approximation for S     (isf=1)'
       WRITE(23,'(a)')
     &'#   formal solution with linear approximation for S     (isf=1)'
          endif
               else if(isf.ge.3) then
       WRITE(14,'(a)')
     &  '#   formal solution with linear approximation for S if problem'
       WRITE(30,'(a)')
     &  '#   formal solution with linear approximation for S if problem'
           if(ifich.ge.1.and.ifich.le.8) then
       WRITE(15,'(a)')
     &  '#   formal solution with linear approximation for S if problem'
       WRITE(17,'(a)')
     &  '#   formal solution with linear approximation for S if problem'
       WRITE(19,'(a)')
     &  '#   formal solution with linear approximation for S if problem'
       WRITE(23,'(a)')
     &  '#   formal solution with linear approximation for S if problem'
          endif
               else if(isf.eq.2) then
       WRITE(14,'(a)')
     &'#   formal solution with parabolic approximation for S   (isf=2)'
       WRITE(30,'(a)')
     &'#   formal solution with parabolic approximation for S   (isf=2)'
           if(ifich.ge.1.and.ifich.le.8) then
       WRITE(15,'(a)')
     &'#   formal solution with parabolic approximation for S   (isf=2)'
       WRITE(17,'(a)')
     &'#   formal solution with parabolic approximation for S   (isf=2)'
       WRITE(19,'(a)')
     &'#   formal solution with parabolic approximation for S   (isf=2)'
       WRITE(23,'(a)')
     &'#   formal solution with parabolic approximation for S   (isf=2)'
          endif
               endif
c
               if(irecsup.eq.0.or.irecsup.ge.3) then
        WRITE(14,'(a)')
     &  '#   additional recombinations neglected       (irecsup=0)'
        WRITE(30,'(a)')
     &  '#   additional recombinations neglected       (irecsup=0)'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,'(a)')
     &  '#   additional recombinations neglected       (irecsup=0)'
        WRITE(17,'(a)')
     &  '#   additional recombinations neglected       (irecsup=0)'
        WRITE(19,'(a)')
     &  '#   additional recombinations neglected       (irecsup=0)'
        WRITE(23,'(a)')
     &  '#   additional recombinations neglected       (irecsup=0)'
           endif
               endif
               if(irecsup.eq.1) then
        WRITE(14,'(a)')
     &  '#   additional recombinations on the last level    (irecsup=1)'
        WRITE(30,'(a)')
     &  '#   additional recombinations on the last level    (irecsup=1)'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,'(a)')
     &  '#   additional recombinations on the last level    (irecsup=1)'
        WRITE(17,'(a)')
     &  '#   additional recombinations on the last level    (irecsup=1)'
        WRITE(19,'(a)')
     &  '#   additional recombinations on the last level    (irecsup=1)'
        WRITE(23,'(a)')
     &  '#   additional recombinations on the last level    (irecsup=1)'
           endif
               endif
               if(irecsup.eq.2) then
        WRITE(14,'(a)')
     &'#   additional recomb. shared among all levels      (irecsup=2)'
        WRITE(30,'(a)')
     &'#   additional recomb. shared among all levels      (irecsup=2)'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,'(a)')
     &'#   additional recomb. shared among all levels      (irecsup=2)'
        WRITE(17,'(a)')
     &'#   additional recomb. shared among all levels      (irecsup=2)'
        WRITE(19,'(a)')
     &'#   additional recomb. shared among all levels      (irecsup=2)'
        WRITE(23,'(a)')
     &'#   additional recomb. shared among all levels      (irecsup=2)'
           endif
               endif
           if(iprof.le.1) then
        WRITE(14,285) nraimax,nf,aint,VTref*VTref
        WRITE(30,285) nraimax,nf,aint,VTref*VTref
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,285) nraimax,nf,aint,VTref*VTref
        WRITE(17,285) nraimax,nf,aint,VTref*VTref
        WRITE(19,285) nraimax,nf,aint,VTref*VTref
        WRITE(23,285) nraimax,nf,aint,VTref*VTref
           endif
           else if(iprof.eq.2) then
        WRITE(14,286) nraimax,nf,aint,VTref*VTref                          !ee
        WRITE(30,286) nraimax,nf,aint,VTref*VTref
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,286) nraimax,nf,aint,VTref*VTref
        WRITE(17,286) nraimax,nf,aint,VTref*VTref
        WRITE(19,286) nraimax,nf,aint,VTref*VTref
        WRITE(23,286) nraimax,nf,aint,VTref*VTref
           endif
           endif
           if(iprof.eq.1) then
        WRITE(14,'(a)')
     &     '#       Doppler profile for Lya of H- & He-like +C4 +O6'
        WRITE(30,'(a)')
     &     '#       Doppler profile for Lya of H- & He-like +C4 +O6'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,'(a)')
     &     '#       Doppler profile for Lya of H- & He-like +C4 +O6'
        WRITE(17,'(a)')
     &     '#       Doppler profile for Lya of H- & He-like +C4 +O6'
        WRITE(19,'(a)')
     &     '#       Doppler profile for Lya of H- & He-like +C4 +O6'
        WRITE(23,'(a)')
     &     '#       Doppler profile for Lya of H- & He-like +C4 +O6'
           endif
           endif   ! iprof
               if(icompt.eq.0) then
        WRITE(14,'(a)') '#      no line comptonization'
        WRITE(30,'(a)') '#      no line comptonization'
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,'(a)') '#      no line comptonization'
        WRITE(17,'(a)') '#      no line comptonization'
        WRITE(19,'(a)') '#      no line comptonization'
        WRITE(23,'(a)') '#      no line comptonization'
           endif
               else
        WRITE(14,288) icompt
        WRITE(30,288) icompt
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,288) icompt
        WRITE(17,288) icompt
        WRITE(19,288) icompt
        WRITE(23,288) icompt
           endif
               endif   ! icompt
               if(acompt.eq.0.d0.and.bcompt.eq.0.d0) then
        WRITE(14,289)
        WRITE(30,289)
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,289)
        WRITE(17,289)
        WRITE(19,289)
        WRITE(23,289)
           endif
               else
        WRITE(14,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt             !ee
        WRITE(30,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
        WRITE(17,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
        WRITE(19,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
        WRITE(23,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
           endif
               endif   ! acompt
               if(isuite.ge.2) then
        WRITE(14,291) isuite
        WRITE(30,291) isuite
           if(ifich.ge.1.and.ifich.le.8) then
        WRITE(15,291) isuite
        WRITE(17,291) isuite
        WRITE(19,291) isuite
        WRITE(23,291) isuite
           endif
               endif
c        WRITE(15,292) tauth
        WRITE(14,*)
        WRITE(30,*)
           if(ifich.ge.1.and.ifich.le.8) then
        if(ila.eq.0) WRITE(17,*)
        if(ila.eq.1) WRITE(17,'(a)') '#'
        WRITE(19,*)
        WRITE(23,*)
           endif
 180    format('# constant additional heating=',1pe10.3,
     &                                  'erg/cm3/(NhNe)')
 181    format('# beyond z=0.98*H temperature is assigned',
     &       ' constant: itemp=',i2)
 182    format('# CLOUD: surface density Nh=',1pe10.3,' - total thick',
     &       'ness ',a6,'=',1pe10.3/'#  calculation with constant ',
     &       'GAZEOUS PRESSURE',23x,'(idens=',i2,')')
 183    format('# CLOUD: surface density Nh=',1pe10.3,' - total thick',
     &       'ness ',a6,'=',1pe10.3/'#  calculation with constant ',
     &       'TOTAL PRESSURE',24x,'(idens=',i2,')')
 184    format('# NO SEARCH OF THERMAL EQUILIBRIUM : constant ',
     &       'temperature=',1pe10.3)
 185    format('# NO SEARCH OF THERMAL EQUILIBRIUM',
     &       ' : assigned temperature read   (itemp=11)')
 186    format('# NON ILLUMINATED Cloud with constant temperature')
 187    format('# INCIDENT RADIATION:  - flux(13.6ev)=',1pg10.3,
     &       ' csit=',1pg12.5)
 188    format('# INCIDENT RADIATION: ionisation parameter csit=',
     &       1pg12.5/'#   hnu from',0pf9.5,' to',1pg10.3,
     &       'ev - flux(13.6ev)=',1pg10.3)                                 !ee
c     &  'ev - flux(13.6ev)=',1pg10.3,' - Lbol=',1pe10.3)
 189    format('#   power law with index',0pf5.2,38x,'(incid=',i2,')')
 190    format('#   black body with temperature Tbb=',1pe10.3,
     &       ' multiplied by',1pg10.3,' (incid=',i2,')')
 191    format('#   spectrum given by internal subroutine ',
     &       'FFLUX  (incid=',i2,')')
 192    format('#  flux given by file FLi. (format .res) csit=',
     &       1pg12.5,' (incid=',i2,')')
 293    format('#   broken power law given by',i3,' points: loghnuev ',
     &           'logflux    (incid=',i2,')')
 294    format('#    ',1pg12.5,1pg12.5,';',1pg12.5,1pg12.5,';',
     &            1pg12.5,1pg12.5,';')
 193    format('#  spectrum given by file FLi. (free format',
     &       ',increasing kc,incid=',i2,')'/'#',19x,'(multiplied by',
     &       1pg12.5, ') csit=',1pg12.5)
 198    format('#  spectrum given by file FLi.',37x,'(incid=',i2,')')
 199    format('#   2nd power-law (with exp cut-off): hnumin2=',0pf7.3,
     &       ' hnumax2',1pg10.3/'#   index2=',0pf5.2,' fxdofo=',0pf10.5,
     &       ' A1=',1pg10.3,' A2/A1=',1pg10.3)
 194    format('# no backside illumination',41x,'(irent= 0)')
 195    format('# BACKSIDE ILLUMINATION: black body  temperature',
     &       1pe10.3,' *',1pg10.3,'(irent=',i2,')')
 196    format('# BACKSIDE ILLUMINATION: Wien law  temperature',
     &       1pe10.3,'*',1pg10.3,'(irent=',i2,')')
 197    format('# BACKSIDE ILLUMINATION equal to incident multiplied',
     &       ' by',1pg11.3,'       (irent=',i2,')')
 287    format('# MICROTURBULENCE: speed =',f8.0,'km/s')
 280    format('#'/'# COLUMN GRID: from NH=',1pe9.2,':',i3,' layers/',  !2345678
     &   'decade'/'#    then: equal layers with ',
     &   'dcmax=',1pe9.2,'  (ndcm=',i4,')'/'#    divided by',0pf5.1,1x,
     &   'between',1pe9.2,' and',1pe9.2/'#    end:',i3,' layers/decade',
     &   ' till dcol=',1pe9.2,21x,'(igrid=',i2,')')
 281    format('#'/'# COLUMN GRID: ',i3,' equal layers     (igrid=',i2,
     &   ')')
 282    format('#'/'# COL GRID &/or densities given in file MZN. with',
     &    i4,' layers  (igrid=',i2,')')
 284    format('# COMPUTATION OPTIONS: I-/I+init=',0pf8.5,
     &       ' Tinit=',1pe10.3,a16)
 285    format('# lines:total number :',i5/
     &         '#       Voigt profile,transfer:',i2,' pts nu-nu0=',
     &       0pf5.1,'*(1/ygau^3-1)*dnudop(',1pg7.1,')')
 286    format('# lines:total number :',i5/
     &         '#       Doppler prof.,transfer:',i2,' pts nu-nu0=',
     &       0pf5.1,'*(1/ygau^3-1)*dnudop(',1pg7.1,')')
 288    format('#       lines comptonization if dEcompmoy >',i2,
     &       '*FWHM')
 289    format('# Compt heating: approximate formula of Tarter')
 290    format('# Compt heating=(',f8.5,'*exp(-',f8.5,'*tauthfromedge)',
     &       '+',f8.5,'*exp(-',f8.5,'*t)'/'#   (X>26kev)       +',f8.5,
     &       '*exp(-',f8.3,'*t))  *Netot*sigth*flxdurincid/NeNh')
 291    format('# CONTINUATION OF A PREVIOUS RUN',39x,'(isuite=',i2,')')
 292    format(/'# tau thomson about',1pg10.3)
c
        return
        end                                                         !secrituree
c------------------------------------------------------------------------------
        subroutine sECRITUREp
c
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nrais=4200)
         double precision nht,ne
         character titre*78,tab*1,txt*46,fichatomic*51
         integer ijour(3)
       common/abond/ab(10)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/drugi/ flin2,hnumin2,hnumax2,fxdofo,fluxex2          !Agata
       common/flux7/yloghnu(10),ylogfnu(10),a(9),b(9),cpl,npy
       common/im/fii,itali
       common/mnua/dcol1,diviseur,debchute,finchute,dcolf,
     &             Tinit,ttauo8,dtaufixe,ecart,chaufsup,
     &             tcol(nzs+1),temp(nzs),tnht(nzs),trij(nzs,120)
       common/tabm/exflux(322),thnur(nrais)
       common/mec1/titre,fichatomic
       common/mec2/rayminrg,raymaxrg,hLbol,tauth,ndirlu
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/flx/flxdurincid,acompt,bcompt,ccompt,dcompt,ecompt,fcompt,
     &            scomptx1,scomptx2,ftauth
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/bb/Tbb,wbb
c secriture-public
        external idate
        call idate(ijour)
c        tab=char(9)                                         ! tabulation
        tab=' '
c
        WRITE(6,80)  titre,ijour
        WRITE(14,80) titre,ijour   ! .res
        WRITE(30,80) titre,ijour   ! .spe
               if(igrid.eq.11.or.igrid.eq.12) goto 10
c               if(itemp.le.1.or.itemp.eq.10.or.itemp.eq.11) then
        txt='Coltot'
        if(igrid.eq.2) txt='Htot  '
                if(idens.eq.0) then
        WRITE(6,81)  densinit,txt,coltot
        WRITE(14,81) densinit,txt,coltot
        WRITE(30,81) densinit,txt,coltot
                endif
                if(idens.eq.1) then
        WRITE(6,82)  densinit,raymin,dendex
        WRITE(14,82) densinit,raymin,dendex
        WRITE(30,82) densinit,raymin,dendex
                endif                                                     !ep
 10             continue
 80    format('#',a78/'#',1x,i2,' -',i2,' -',i4,' TITAN106a')
c     &             ,'(transfALIcont+line-UTA)')
 81    format('# CLOUD: constant density Nh=',1pe10.3,
     &             ' - total thickness ',a6,'=',1pe10.3)
 82    format('# CLOUD: power law density Nh=',1pe10.3,
     &             '*(',1pe9.2,'/R)**',0pf5.2)
c
        if(incid.eq.0) csit=0.d0
        txt='Coltot'
        if(igrid.eq.2) txt='Htot  '
c                if(idens.eq.2.or.idens.eq.5) then
c        WRITE(14,182) densinit,txt,coltot,idens
c        WRITE(30,182) densinit,txt,coltot,idens
c                endif
c          if(idens.eq.3.or.idens.eq.4.or.idens.ge.6) then
c        WRITE(14,183) densinit,txt,coltot,idens
c        WRITE(30,183) densinit,txt,coltot,idens
c                endif
c                if(idens.eq.10.or.idens.eq.20) then
c        WRITE(14,'(a)')
c     &        '#  searching T with constant pressure,cold solution'
c        WRITE(30,'(a)')
c     &        '#  searching T with constant pressure,cold solution'
c                endif
c                if(idens.eq.11.or.idens.eq.21) then
c        WRITE(14,'(a)')
c     &        '#  searching T with constant pressure, hot solution'
c        WRITE(30,'(a)')
c     &        '#  searching T with constant pressure, hot solution'
c                endif
c                if(idens.eq.8) then
c        WRITE(14,'(a)')'#  searching T with constant density'
c        WRITE(30,'(a)')'#  searching T with constant density'
c                endif                                                     !ep
                if(itemp.eq.10) then
        WRITE(6,184)  Tinit
        WRITE(14,184) Tinit
        WRITE(30,184) Tinit
                endif
                if(igrid.ge.13) then
        WRITE(14,185)
        WRITE(30,185)
                endif                                                      !ep
               if(incid.eq.0) then
        WRITE(14,186)
        WRITE(30,186)
               else if(incid.eq.1.or.incid.eq.17.or.incid.eq.18) then !Agata
        fff=exflux(1)
        if(incid.eq.1) fff=fluxex0
        WRITE(6,188)  csit,hnumin,hnumax,fff
        WRITE(14,188) csit,hnumin,hnumax,fff
        WRITE(30,188) csit,hnumin,hnumax,fff
               else
        fff=exflux(1)
        WRITE(6,187)  fff,csit
        WRITE(14,187) fff,csit
        WRITE(30,187) fff,csit
               endif                                                       !ep
               if(incid.eq.1) then
        WRITE(6,189)  flindex
        WRITE(14,189) flindex
        WRITE(30,189) flindex
               else if(incid.eq.17.or.incid.eq.18) then   !Agata
        WRITE(6,295)  flindex
        WRITE(14,295) flindex
        WRITE(30,295) flindex
               else if(incid.eq.2) then
        WRITE(6,190)  Tbb,wbb
        WRITE(14,190) Tbb,wbb
        WRITE(30,190) Tbb,wbb
c               if(incid.ge.3.and.incid.le.9) then                         !ep
c        WRITE(14,191) 
c        WRITE(30,191) 
c               endif
               else if(incid.eq.3) then
        WRITE(6,'(a)')  '#   incident from Laor (1997)'
        WRITE(14,'(a)') '#   incident from Laor (1997)'
        WRITE(30,'(a)') '#   incident from Laor (1997)'
               else if(incid.eq.4) then
        WRITE(6,'(a)')  '#   incident for AGN from Cloudy (1998)'
        WRITE(14,'(a)') '#   incident for AGN from Cloudy (1998)'
        WRITE(30,'(a)') '#   incident for AGN from Cloudy (1998)'
               else if(incid.eq.5) then
        WRITE(6,'(a)')  '#   incident from Netzer (1996)'
        WRITE(14,'(a)') '#   incident from Netzer (1996)'
        WRITE(30,'(a)') '#   incident from Netzer (1996)'
               else if(incid.eq.6) then
        WRITE(6,'(a)')  '#   incident from Krolik (1995)'
        WRITE(14,'(a)') '#   incident from Krolik (1995)'
        WRITE(30,'(a)') '#   incident from Krolik (1995)'
               else if(incid.eq.7) then
        WRITE(6,'(a)')  '#   incident for NGC5548 (Collin et al. 1997)'
        WRITE(14,'(a)') '#   incident for NGC5548 (Collin et al. 1997)'
        WRITE(30,'(a)') '#   incident for NGC5548 (Collin et al. 1997)'
               else if(incid.eq.8) then
        WRITE(6,'(a)')  '#   incident for NGC3783 (Kaspi et al. 2001)'
        WRITE(14,'(a)') '#   incident for NGC3783 (Kaspi et al. 2001)'
        WRITE(30,'(a)') '#   incident for NGC3783 (Kaspi et al. 2001)'
               else if(incid.eq.9) then
        WRITE(6,'(a)')  '#   incident for optically thin X-ray models'//
     &              ' of meeting at Lexington 2000'
        WRITE(14,'(a)') '#   incident for optically thin X-ray models'//
     &              ' of meeting at Lexington 2000'
        WRITE(30,'(a)') '#   incident for optically thin X-ray models'//
     &              ' of meeting at Lexington 2000'
               else if(incid.eq.10) then
        WRITE(6,293)  npy
        WRITE(14,293) npy
        WRITE(14,294) (yloghnu(i),ylogfnu(i),i=1,npy)
        WRITE(30,293) npy
        WRITE(30,294) (yloghnu(i),ylogfnu(i),i=1,npy)
c               else if(incid.eq.11.or.incid.eq.12) then
c        WRITE(6,193)  incid,fluxex0,csit
c        WRITE(14,193) incid,fluxex0,csit
c        WRITE(30,193) incid,fluxex0,csit
c               else if(incid.eq.13.or.incid.eq.14) then
c        WRITE(14,192) csit,incid
c        WRITE(30,192) csit,incid
               else if(incid.eq.15.or.incid.eq.16) then
        WRITE(6,198)
        WRITE(14,198)
        WRITE(30,198)
               else if(incid.eq.18) then
        WRITE(14,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
        WRITE(30,199) hnumin2,hnumax2,flin2,fxdofo,fluxex0,fluxex2
               endif   ! incid
               if(irent.eq.0) then
        WRITE(14,194)
        WRITE(30,194)
               endif
        if(irent.eq.1.or.irent.eq.2.or.irent.eq.3.or.irent.eq.4) then
        WRITE(6,195)  Tbb,wbb
        WRITE(14,195) Tbb,wbb
        WRITE(30,195) Tbb,wbb
               endif
c               if(irent.eq.5) then
c        WRITE(14,196) Tbb,wbb,irent
c        WRITE(30,196) Tbb,wbb,irent
c               endif
c               if(irent.eq.6) then
c        WRITE(14,197) wbb,irent
c        WRITE(30,197) wbb,irent
c               endif                                                     !ep
        WRITE(14,86) fichatomic
        WRITE(30,86) fichatomic
 86     format('# ATOMIC DATA : file ',a51)
        WRITE(14,87) (ab(j),j=2,10)
        WRITE(30,87) (ab(j),j=2,10)
 87     format('# ABUNDANCES/H:     He:',1pe10.3,'  C :',1pe10.3,
     &  '  N :',1pe10.3,'  O :',1pe10.3/'#',4x,'Ne:',1pe10.3,'  Mg:',
     &  1pe10.3,'  Si:',1pe10.3,'  S :',1pe10.3,'  Fe:',1pe10.3)
c ABONDANCES/H:     He: 2.456E+90  C : 2.456E+90  N : 2.456E+90  O : 2.456E+90
c    Ne: 2.456E+90  Mg: 2.456E+90  Si: 2.456E+90  S : 2.456E+90  Fe: 2.456E+90
               if(vturbkm.gt.0.d0) then
        WRITE(14,287) vturbkm
        WRITE(30,287) vturbkm
               endif
               if(igrid.eq.0) then
        WRITE(14,281) npt
        WRITE(30,281) npt
c               else if(igrid.lt.10) then
c        dcmax=coltot/float(ndcm)
c        WRITE(14,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
c     &                finchute,npdf,dcolf,igrid
c        WRITE(30,280) dcol1,npd1,dcmax,ndcm,diviseur,debchute,
c     &                finchute,npdf,dcolf,igrid
               else if(igrid.eq.10.or.igrid.eq.11) then
        WRITE(14,282) npt
        WRITE(30,282) npt
               endif                                                       !ep
c               txt='  '
c               if(iacc.eq.1) txt=' accelerat.-c'
c               if(iacc.eq.2) txt=' accelerat.-cr'
c               if(iacc.eq.3) txt=' accelerat.-crt'
c        WRITE(14,284) fii,Tinit,txt(1:16)
c        WRITE(30,284) fii,Tinit,txt(1:16)
c ietl=0 Verner - ietl=1   dielectronic recombinations suppressed
c and computation of recombinations by integral of sigma
c  plane parallel cloud   near ETL  (ietl=1: diel=0 & recomb<-sigma)
c               if(ietl.eq.0) then
c        txt='far from LTE  (ietl=0)'
c       WRITE(14,'(a)')'#  plane parallel cloud - '//txt(1:41)
c       WRITE(30,'(a)')'#  plane parallel cloud - '//txt(1:41)
c               else if(ietl.eq.1) then
c        txt='near LTE (ietl=1: diel=0 & recomb<-sigma)'
c       WRITE(14,'(a)')'#  plane parallel cloud - '//txt(1:41)
c       WRITE(30,'(a)')'#  plane parallel cloud - '//txt(1:41)
c               endif
               if(ndirlu.ge.2) then
                  if(incang.eq.0) then
        WRITE(14,393) ndirlu
        WRITE(30,393) ndirlu
 393    format('#  calculation for',i4,' directions',
     &                   ' - semi-isotropic incident radiation')
                  else if(incang.eq.1) then
        WRITE(14,394) ndir
        WRITE(30,394) ndir
 394    format('#  calculation for',i4,' directions',
     &                   ' - normal incident radiation')
                  else
        WRITE(14,395) ndirlu,incang
        WRITE(30,395) ndirlu,incang
 395    format('#  calculation for',i4,' directions',
     &                   ' - incident radiation in direction',i3)
                  endif   ! incang
               endif   ! ndirlu
c               if(isf.le.1) then
c       WRITE(14,'(a)')
c     &   '#  formal solution with linear approximation for S'
c       WRITE(30,'(a)')
c     &   '#  formal solution with linear approximation for S'
c               else if(isf.ge.3) then
c       WRITE(14,'(a)')
c     &   '#  formal solution with linear approximation for S if problem'
c       WRITE(30,'(a)')
c     &   '#  formal solution with linear approximation for S if problem'
c               else if(isf.eq.2) then
c       WRITE(14,'(a)')
c     &   '#  formal solution with parabolic approximation for S'
c       WRITE(30,'(a)')
c     &   '#  formal solution with parabolic approximation for S'
c               endif
c               if(irecsup.eq.0.or.irecsup.ge.3) then
c        WRITE(14,'(a)')
c     &  '#  additional recombinations neglected       (irecsup=0)'
c        WRITE(30,'(a)')
c     &  '#  additional recombinations neglected       (irecsup=0)'
c               endif
c               if(irecsup.eq.1) then
c        WRITE(14,'(a)')
c     &  '#  additional recombinations on the last level    (irecsup=1)'
c        WRITE(30,'(a)')
c     &  '#  additional recombinations on the last level    (irecsup=1)'
c               endif
c               if(irecsup.eq.2) then
c        WRITE(14,'(a)')
c     &'#  additional recomb. shared among all levels      (irecsup=2)'
c        WRITE(30,'(a)')
c     &'#  additional recomb. shared among all levels      (irecsup=2)'
c               endif
           if(iprof.le.1) then
        WRITE(14,285) nraimax,nf
c        WRITE(14,285) nraimax,nf,aint,VTref*VTref
        WRITE(30,285) nraimax,nf
c           else if(iprof.eq.2) then
c        WRITE(14,286) nraimax,nf
cc        WRITE(14,286) nraimax,nf,aint,VTref*VTref                         !ep
c        WRITE(30,286) nraimax,nf
           endif
           if(iprof.eq.1) then
        WRITE(14,'(a)')
     &     '#       Doppler profile for Lya of H- & He-like +C4 +O6'
        WRITE(30,'(a)')
     &     '#       Doppler profile for Lya of H- & He-like +C4 +O6'
           endif   ! iprof
c               if(icompt.eq.0) then
c        WRITE(14,'(a)') '#      no line comptonization'
c        WRITE(30,'(a)') '#      no line comptonization'
c               else
c        WRITE(14,288) icompt
c        WRITE(30,288) icompt
c               endif   ! icompt
               if(acompt.eq.0.d0.and.bcompt.eq.0.d0) then
        WRITE(14,289)
        WRITE(30,289)
c               else
c        WRITE(14,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt           !ep
c        WRITE(30,290) acompt,bcompt,ccompt,dcompt,ecompt,fcompt
               endif   ! acompt
        WRITE(6,*)
c        WRITE(14,*)
c        WRITE(30,*)
 182    format('# CLOUD: surface density Nh=',1pe10.3,' - total thick',
     &       'ness ',a6,'=',1pe10.3/'#  calculation with constant ',
     &       'GAZEOUS PRESSURE',23x,'(idens=',i2,')')
 183    format('# CLOUD: surface density Nh=',1pe10.3,' - total thick',
     &       'ness ',a6,'=',1pe10.3/'#  calculation with constant ',
     &       'TOTAL PRESSURE',24x,'(idens=',i2,')')
 184    format('# NO SEARCH OF THERMAL EQUILIBRIUM : constant ',
     &       'temperature=',1pe10.3)
 185    format('# NO SEARCH OF THERMAL EQUILIBRIUM',
     &       ' : assigned temperature read')
 186    format('# NON ILLUMINATED Cloud with constant temperature')
 187    format('# INCIDENT RADIATION:  - flux(13.6ev)=',1pg10.3,
     &       ' csit=',1pg12.5)
 188    format('# INCIDENT RADIATION: ionisation parameter csit=',
     &       1pg12.5/'#   hnu from',0pf9.5,' to',1pg10.3,
     &       'ev - flux(13.6ev)=',1pg10.3)                                 !ep
c     &  'ev - flux(13.6ev)=',1pg10.3,' - Lbol=',1pe10.3)
 189    format('#   power law with index',0pf5.2)
c 189    format('#   power law with index',0pf5.2,37x,'(incid=',i2,')')
 295    format('#   power law with exponential cut-off: index=',0pf5.2)
 190    format('#   black body with temperature Tbb=',1pe10.3,
     &       ' multiplied by',1pg10.3)
c     &       ' multiplied by',1pg10.3,' (incid=',i2,')')
 191    format('#   spectrum given by internal subroutine ')
c     &        ,'FFLUX  (incid=',i2,')')
 192    format('#  flux given by file FLi. (format .res) csit=',
     &       1pg12.5,' (incid=',i2,')')
 293    format('#   broken power law given by',i3,' points: loghnuev ',
     &           'logflux')
 294    format('#    ',1pg12.5,1pg12.5,';',1pg12.5,1pg12.5,';',
     &            1pg12.5,1pg12.5,';')
 193    format('#  spectrum given by file FLi. (free format',
     &       ',increasing kc,incid=',i2,')'/'#',19x,'(multiplied by',
     &       1pg12.5, ') csit=',1pg12.5)
 198    format('#  spectrum given by file')
 199    format('#   2nd power-law (with exp cut-off): hnumin2=',0pf7.3,
     &       ' hnumax2',1pg10.3/'#   index2=',0pf5.2,' fxdofo=',0pf10.5,
     &       ' A1=',1pg10.3,' A2/A1=',1pg10.3)
 194    format('# no backside illumination')
 195    format('# BACKSIDE ILLUMINATION: black body  temperature',
     &       1pe10.3,' *',1pg10.3)
c     &       1pe10.3,' *',1pg10.3,'(irent=',i2,')')
 196    format('# BACKSIDE ILLUMINATION: Wien law  temperature',
     &       1pe10.3,'*',1pg10.3,'(irent=',i2,')')
 197    format('# BACKSIDE ILLUMINATION equal to incident multiplied',
     &       ' by',1pg11.3,'       (irent=',i2,')')
 287    format('# MICROTURBULENCE: speed =',f8.0,'km/s')
 280    format('#'/'# COLUMN GRID: from NH=',1pe9.2,':',i3,' layers/',  !234567
     &   'decade'/'#    then: equal layers with ',
     &   'dcmax=',1pe9.2,'  (ndcm=',i4,')'/'#    divided by',0pf5.1,1x,
     &   'between',1pe9.2,' and',1pe9.2/'#    end:',i3,' layers/decade',
     &   ' till dcol=',1pe9.2,21x,'(igrid=',i2,')')
 281    format('#'/'# COLUMN GRID: ',i3,' equal layers')
 282    format('#'/'# COL GRID &/or densities given in a file with',
     &             i4,' layers')
 284    format('# COMPUTATION OPTIONS: I-/I+init=',0pf8.5,
     &       ' Tinit=',1pe10.3,a16)
 285    format('# LINES:total number :',i5/
     &         '#       Voigt profile, transfer with ',i2,' pts')
c     &         '#       Voigt profile,transfer:',i2,' pts nu-nu0=',
c     &       0pf5.1,'*(1/ygau^3-1)*dnudop(',1pg7.1,')')
 286    format('# LINES:total number :',i5/
     &         '#       Doppler profile, transfer with ',i2,' pts')
c     &         '#       Doppler prof.,transfer:',i2,' pts nu-nu0=',
c     &       0pf5.1,'*(1/ygau^3-1)*dnudop(',1pg7.1,')')
 288    format('#       lines comptonization if dEcompmoy >',i2,
     &       '*FWHRM')
 289    format('# Compt heating: approximate formula of Tarter')
 290    format('# Compt heating=(',f8.5,'*exp(-',f8.5,'*tauthfromedge)',
     &       '+',f8.5,'*exp(-',f8.5,'*t)'/'#   (X>26kev)       +',f8.5,
     &       '*exp(-',f8.3,'*t))  *Netot*sigth*flxdurincid/NeNh')
c 291    format('# CONTINUATION OF A PREVIOUS RUN',39x,'(isuite=',i2,')')
c 292    format(/'# tau thomson about',1pg10.3)
c
        return
        end                                                         !secriturep
c--============================================================================
       subroutine LECTASTROGRID
c ASTROGRID est mort - inutilise
c lecture du fichier des donnees d'entree dat.web, rempli sur internet
c avec Astrogrid, s'il n'y a pas de ti9.dat
! reading input file when running from internet with Astrogrid

       implicit double precision (a-h,o-z)
       parameter (nzs=999)
         character titre*78,fich*30,fifli*30,fimzn*30,fimzt*30,filog*30
         character fichatomic*51
         double precision ne,nht
       common/mec1/titre,fichatomic
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/flux7/yloghnu(10),ylogfnu(10),a(9),b(9),cpl,npy
       common/lecentr/flym,resolution,fifli,fimzn
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/bb/Tbb,wbb
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/abond/ab(10)
       common/opt2/iacc,isf,ndir,incang
       common/mec2/rayminrg,raymaxrg,hLbol,tauth,ndirlu
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &             kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/mweb/Tcst,fich,fimzt
c lectastrogrid
c
       READ(9,'(a)') titre        ! lit 78 caracteres
       READ(9,*) fich             ! lit du premier non-blanc au premier blanc
c pour mesioq il faut mettre :
c READ(9,'(a)')fich   READ(9,'(a)')fifli   READ(9,'(a)')fimzn
           nc=index(fich,' ') - 1
           filog=fich(:nc)//'.log'
           OPEN(UNIT=6,FILE=filog,STATUS='new')
           WRITE(6,*) ' """"Input data""""'  ! on n'obtient pas les memes
           WRITE(6,'(a,a)'),'  " ',titre     ! blancs avec * ou (a)
       READ(9,*) incid,irent
       READ(9,*) fifli
       READ(9,*) flindex,hnumin,hnumax
       READ(9,*) Tbb,wbb
          if(incid.eq.10) then
             OPEN(UNIT=12,FILE=fifli,STATUS='OLD',ERR=10)
             READ(12,*)
             READ(12,*) npy
             READ(12,*) (yloghnu(i),ylogfnu(i),i=1,npy)
             goto 11
 10          continue
          WRITE(6,*)'the file ',fifli,' is not correctly open - STOP'
             STOP
 11          continue
          endif
          if(incid.eq.1) then
             WRITE(6,'(a,3(1pg12.4))') '  " incident power law: '//
     &             'index,Emin,Emax:',flindex,hnumin,hnumax
          else if(incid.eq.2) then
             WRITE(6,'(a,2(1pg12.5))') '  " incident blackbody: '//
     &            'T, geometrical dilution factor: ',Tbb,wbb
          else if(incid.eq.3) then
             WRITE(6,*) ' " incident from Laor (1997)'
          else if(incid.eq.4) then
             WRITE(6,*) ' " incident for AGN from Cloudy (1998)'
          else if(incid.eq.5) then
             WRITE(6,*) ' " incident from Netzer (1996)'
          else if(incid.eq.6) then
             WRITE(6,*) ' " incident from Krolik (1995)'
          else if(incid.eq.7) then
             WRITE(6,*) ' " incident for NGC5548 (Collin et al. 1997)'
          else if(incid.eq.8) then
             WRITE(6,*) ' " incident for NGC3783 (Kaspi et al. 2001)'
          else if(incid.eq.9) then
             WRITE(6,*) ' " incident for optically thin X-ray models'//
     &              ' of meeting at Lexington 2000'
          else if(incid.eq.10) then
             WRITE(6,*) ' " incident broken power law, file ',fifli
             WRITE(6,'(6f11.6)') (yloghnu(i),ylogfnu(i),i=1,npy)
          else if(incid.eq.15) then
            if(incid.eq.15)WRITE(6,*)' " incident file in flux ',fifli
          else if(incid.eq.16) then
           if(incid.eq.16)WRITE(6,*)' " incident file in nu*flux ',fifli
          else if(incid.eq.17) then
             WRITE(6,*)
     &       ' " incident power law with exponential cut-off:'
             WRITE(6,'(a,3(1pg12.4))')
     &       '  "    index,Emin,Emax:',flindex,hnumin,hnumax
          endif
          if(irent.eq.1) then
             WRITE(6,'(a,2(1pg12.5))') '  " backward blackbody: '//
     &            'T, geometrical dilution factor: ',Tbb,wbb
          endif
       READ(9,*) csit,flym
          if(csit.gt.0.d0) WRITE(6,'(a,1pg13.6)')'  " xi_total= ',csit
          if(flym.gt.0.d0) WRITE(6,'(a,1pg13.6)') 
     &                            '  " incident flux at 13.6ev= ',flym
       READ(9,*) coltot
          WRITE(6,'(a,1pe12.4)')'  " column-density= ',coltot
       READ(9,*) idens,densinit,dendex,raymin
          if(idens.eq.0) WRITE(6,'(a,1pg12.4)') 
     &                                '  " constant density= ',densinit
          if(idens.eq.1) then
             WRITE(6,*)' " density in power law: n=n0(R/(R+z))**index'
             WRITE(6,'(a,1pe11.4,1pg12.5,1pe11.4)')
     &       '  "    n0,index,R: ',densinit,dendex,raymin
          endif
          if(idens.eq.100) igrid=11               ! pas utile
       READ(9,*) vturbkm
         WRITE(6,'(a,f10.2)')'  " turbulence velocity in km/s= ',vturbkm
       READ(9,*) inr
          if(inr.eq.1) WRITE(6,*)
     &    ' " computation with the short line list (about 1000 lines)'
       READ(9,*) (ab(j),j=2,10)
          ab(1)=1.d0
          WRITE(6,*) ' " abundances'
          WRITE(6,'(1p5e14.4)') (ab(j),j=1,10)
       READ(9,*) incang,ndir ! attention l'ordre est invers� dans le form.web
          ndirlu=ndir
          if(ndir.eq.4) ndirlu=103
          if(ndir.eq.6) ndirlu=105
          if(ndir.eq.4.or.ndir.eq.6) then
             if(incang.eq.0) incang=1
             WRITE(6,*)
     &   ' " incident flux normal; computation with',ndir,' directions'
          else
             WRITE(6,*)
     &' " incident flux isotropic; computation with',ndir,' directions'
          endif
       READ(9,*) igrid,nstr
       READ(9,*) fimzn
          if(igrid.eq.0) then
             WRITE(6,*) ' "',nstr,' layers of equal thickness'
             ndcm=nstr
             npt=nstr
          else if(igrid.eq.1) then
             WRITE(6,*) ' " automatic column-grid'
          else
             WRITE(6,*)
     &      ' " file ',fimzn,' gives column-grid with ',nstr,' layers'
          endif
          if(igrid.eq.12) then
             igrid=14
             fimzt=fimzn
             itemp=11
          endif
       READ(9,*) Tcst
          if(Tcst.le.1.d0) then
             itemp=0
          else
             itemp=10
             WRITE(6,'(a,1pe12.4)')'  " constant temperature= ',Tcst
          endif
       READ(9,*) nfmax
          WRITE(6,*) ' "',nfmax,' iterations maximum'
       READ(9,*) resolution
          WRITE(6,*)' " resolution of the lines in the output spectra=',
     &          resolution
       READ(9,*) ifich
       WRITE(6,*)' """"""""""""""""""""""""""""""""""""""""""""""""""""'
       WRITE(6,*)
c
       return
       end                                                       !lectastrogrid
c------------------------------------------------------------------------------
       subroutine LECTENTREETEXTE

c lecture du fichier des donnees d'entree s'il n'y a pas de ti9.dat - public
! reading input file when running with : ./executable < input

       implicit double precision (a-h,o-z)
       parameter (nzs=999)
         character ligne*80,titre*78,ch*1,txt1*4,txt2*4,txt3*4
         character fifli*30,fimzn*30,fich*30,fimzt*30,fichatomic*51
         double precision ne,nht
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/opt2/iacc,isf,ndir,incang
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/mec1/titre,fichatomic
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/bb/Tbb,wbb
       common/flux7/yloghnu(10),ylogfnu(10),a(9),b(9),cpl,npy
       common/lecentr/flym,resol,fifli,fimzn
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/abond/ab(10)
       common/mec2/rayminrg,raymaxrg,hLbol,tauth,ndirlu
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/nbnua/igrid,npd1,npdf,ndcm,nfin,nimpm,nstr,imppgt
       common/mweb/Tcst,fich,fimzt
c
c lectentreetexte
       kti=0
       kin=0
       kx=0
       kba=0
       kde=0
       kdef=0
       kco=0
       kab=0
       kli=0
       kdi=0
       ktu=0
       kte=0
       kre=0
       kgr=0
       kiter=0
       csit=0.d0
       flym=0.d0
       WRITE(6,*)
       WRITE(6,*) '"""Input data"""'
 1     READ(5,'(a80)',err=3,end=2) ligne
       ch=ligne(1:1)
       if(ch.eq.' '.or.ch.eq.'!'.or.ch.eq.'#') goto 1
       txt1=ligne(1:4)
c       print*,'&&txt1  ',txt1
c TITRE
       if(txt1.eq.'titl') then
          titre=ligne(6:80)
          kti=1
          WRITE(6,'(a,a)') ' " ',titre
c SPECTRE INCIDENT
       else if(txt1.eq.'inci') then
          kin=1
          n1=index(ligne,' ')
 10       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 10
          endif
          txt2=ligne(n1+1:n1+4)
c          print*,'txt2  ',txt2
          if(txt2.eq.'laor') then
             incid=3
             WRITE(6,*) '" incident from Laor (1997)'
          else if(txt2.eq.'agnc') then 
             incid=4
             WRITE(6,*) '" incident for AGN from Cloudy (1998)'
          else if(txt2.eq.'netz') then
             incid=5
             WRITE(6,*) '" incident from Netzer (1996)'
          else if(txt2.eq.'krol') then
             incid=6
             WRITE(6,*) '" incident from Krolik (1995)'
          else if(txt2.eq.'5548') then
             incid=7
             WRITE(6,*) '" incident for NGC5548 (Collin et al. 1997)'
          else if(txt2.eq.'3783') then
             incid=8
             WRITE(6,*) '" incident for NGC3783 (Kaspi et al. 2001)'
          else if(txt2.eq.'lexi') then
             incid=9
             WRITE(6,*) '" incident for optically thin X-ray models'//
     &              ' of meeting at Lexington 2000'
c incident=broken power law
          else if(txt2.eq.'brok') then
             incid=10
             n2=index(ligne(n1+1:),' ') + n1
 11          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 11
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) npy
             READ(5,*,err=3) (yloghnu(i),ylogfnu(i),i=1,npy)
             WRITE(6,*) '" incident broken power law'
             WRITE(6,'(2(1pg12.4),3x,2(1pg12.4),3x,2(1pg12.4))')
     &            (yloghnu(i),ylogfnu(i),i=1,npy)
c incident=file
          else if(txt2.eq.'file') then
             n2=index(ligne(n1+1:),' ') + n1
 12          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 12
             endif
             txt3=ligne(n2+1:n2+4)
             if(txt3.eq.'flux') then
             incid=15
             else if(txt3.eq.'nufl') then
             incid=16
             else
                goto 3
             endif
             n3=index(ligne(n2+1:),' ') + n2
 13          if(ligne(n3+1:n3+1).eq.' ') then
                n3=n3+1
                goto 13
             endif
             n4=index(ligne(n3+1:),' ') + n3
             fifli=ligne(n3+1:n4)
            if(incid.eq.15)WRITE(6,*)'" incident file in flux ',fifli
            if(incid.eq.16)WRITE(6,*)'" incident file in nu*flux ',fifli
c incident=loi de puissance = power law
          else if(txt2.eq.'powe') then
             incid=1
             n2=index(ligne(n1+1:),' ') + n1
 21          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 21
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) flindex
 22          if(ligne(n3+1:n3+1).eq.' ') then
                n3=n3+1
                goto 22
             endif
             n4=index(ligne(n3+1:),' ') + n3
             READ(ligne(n3:n4),*,err=3) hnumin
 23          if(ligne(n4+1:n4+1).eq.' ') then
                n4=n4+1
                goto 23
             endif
             n5=index(ligne(n4+1:),' ') + n4
             READ(ligne(n4:n5),*,err=3) hnumax
             WRITE(6,'(a,3(1pg12.4))')' " incident power law: '//
     &             'index,Emin,Emax:',flindex,hnumin,hnumax
c incident=loi de puissance avec cut-off= power law with exponential cut-off
          else if(txt2.eq.'peco') then
             incid=17
             n2=index(ligne(n1+1:),' ') + n1
 24          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 24
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) flindex
 25          if(ligne(n3+1:n3+1).eq.' ') then
                n3=n3+1
                goto 25
             endif
             n4=index(ligne(n3+1:),' ') + n3
             READ(ligne(n3:n4),*,err=3) hnumin
 26          if(ligne(n4+1:n4+1).eq.' ') then
                n4=n4+1
                goto 26
             endif
             n5=index(ligne(n4+1:),' ') + n4
             READ(ligne(n4:n5),*,err=3) hnumax
             WRITE(6,*)
     &       '" incident power law with exponential cut-off:'
             WRITE(6,'(a,3(1pg12.4))')
     &       ' "    index,Emin,Emax:',flindex,hnumin,hnumax
c corps noir - black body
          else if(txt2.eq.'blac') then
             incid=2
             n2=index(ligne(n1+1:),' ') + n1
 27          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 27
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) Tbb
 28          if(ligne(n3+1:n3+1).eq.' ') then
                n3=n3+1
                goto 28
             endif
             n4=index(ligne(n3+1:),' ') + n3
             READ(ligne(n3:n4),*,err=3) wbb         !  corps noir
c             print*,'Tbb,wbb',Tbb,wbb
             WRITE(6,'(a,2(1pg12.5))') ' " incident blackbody: '//
     &            'T, geometrical dilution factor: ',Tbb,wbb
          else
             kin=0
             goto 3
          endif
c IONISATION PARAMETER or FLUX at 13.6 ev 
       else if(txt1.eq.'xito') then
          kx=kx+1
          n1=index(ligne,' ')
 31       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 31
          endif
          n2=index(ligne(n1+1:),' ') + n1
          READ(ligne(n1:n2),*,err=3) csit
          WRITE(6,'(a,1pg13.6)') ' " xi_total= ',csit
       else if(txt1.eq.'flym') then
          kx=kx+1
          n1=index(ligne,' ')
 32       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 32
          endif
          n2=index(ligne(n1+1:),' ') + n1
          READ(ligne(n1:n2),*,err=3) flym
          WRITE(6,'(a,1pe13.6)')' " incident flux at 13.6ev= ',flym
c BACKWARD BLACK-BODY
       else if(txt1.eq.'back') then                         ! facultatif
          kba=1
          irent=1
          if(incid.eq.2) then
             WRITE(6,*)'STOP: Sorry, these options are not compatible'
             stop
          endif
          n1=index(ligne,' ')
 33       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 33
          endif
          n2=index(ligne(n1+1:),' ') + n1
          READ(ligne(n1:n2),*,err=3) Tbb
 34       if(ligne(n2+1:n2+1).eq.' ') then
             n2=n2+1
             goto 34
          endif
          n3=index(ligne(n2+1:),' ') + n2
          READ(ligne(n2:n3),*,err=3) wbb
          WRITE(6,'(a,2(1pg12.5))') ' " backward blackbody: '//
     &         'T, geometrical dilution factor: ',Tbb,wbb
c DENSITY             densinit
       else if(txt1.eq.'dens') then
          kde=1
          n1=index(ligne,' ')
 40       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 40
          endif
          txt2=ligne(n1+1:n1+4)
          if(txt2.eq.'cons') then
             idens=0
             n2=index(ligne(n1+1:),' ') + n1
 41          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 41
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) densinit
             WRITE(6,'(a,1pg12.4)') ' " constant density= ',densinit
c
          else if(txt2.eq.'powe') then
             idens=1
             n2=index(ligne(n1+1:),' ') + n1
 42          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 42
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) densinit
 43          if(ligne(n3+1:n3+1).eq.' ') then
                n3=n3+1
                goto 43
             endif
             n4=index(ligne(n3+1:),' ') + n3
             READ(ligne(n3:n4),*,err=3) dendex
 44          if(ligne(n4+1:n4+1).eq.' ') then
                n4=n4+1
                goto 44
             endif
             n5=index(ligne(n4+1:),' ') + n4
             READ(ligne(n4:n5),*,err=3) raymin
             WRITE(6,*)'" density in power law: n=n0(R/(R+z))**index'
             WRITE(6,'(a,1pe11.4,1pg12.5,1pe11.4)')
     &       '"    n0,index,R: ',densinit,dendex,raymin
c
          else if(txt2.eq.'file') then
             kdef=1
             idens=0
             igrid=11
             n2=index(ligne(n1+1:),' ') + n1
 45          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 45
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) fimzn
 46          if(ligne(n3+1:n3+1).eq.' ') then
                n3=n3+1
                goto 46
             endif
             n4=index(ligne(n3+1:),' ') + n3
             READ(ligne(n3:n4),*,err=3) nstr
             WRITE(6,'(a,1pg12.4)')' "', nstr,
     &       ' column-densities and densities read in the file ',fimzn
          else
             kde=0
             goto 3
          endif
c COLUMN-DENSITY      coltot
       else if(txt1.eq.'colu') then
          kco=1
          n1=index(ligne,' ')
 51       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 51
          endif
          n2=index(ligne(n1+1:),' ') + n1
          READ(ligne(n1:n2),*,err=3) coltot
          WRITE(6,'(a,1pe12.4)') ' " column-density= ',coltot
c ABUNDANCES    ab
       else if(txt1.eq.'abun') then
          kab=1
          ab(1)=1.d0
          READ(5,*,err=3) (ab(j),j=2,10)
          WRITE(6,*) '" abundances'
          WRITE(6,'(1p5e14.4)') (ab(j),j=1,10)
c DIRECTION     ndir,ndirlu,incang
       else if(txt1.eq.'dire') then                         ! facultatif
          kdi=1
          n1=index(ligne,' ')
 52       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 52
          endif
          txt2=ligne(n1+1:n1+4)
          if(txt2.eq.'norm') then
             n2=index(ligne(n1+1:),' ') + n1
 53          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 53
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) ndir
             if(ndir.ne.4.and.ndir.ne.6) then
                WRITE(6,*)'STOP: number of directions not available:'//
     &          ' write 4 or 6 (including the normal direction)'
                stop
             endif
             if(ndir.eq.4) ndirlu=103
             if(ndir.eq.6) ndirlu=105
             incang=1
             WRITE(6,*)
     &    '" incident flux normal; computation with',ndir,' directions'
c
          else if(txt2.eq.'isot') then
             incang=0
             n2=index(ligne(n1+1:),' ') + n1
 54          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 54
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) ndir
            if(ndir.ne.1.and.ndir.ne.3.and.ndir.ne.5.and.ndir.ne.20)then
               WRITE(6,*)'STOP: number of directions not available: '//
     &          'write 1,3,5 or 20'
                stop
             endif
             ndirlu=ndir
             WRITE(6,*)
     & '" incident flux isotropic; computation with',ndir,' directions'
          endif
c LINES LIST
       else if(txt1.eq.'line') then                         ! facultatif
          n1=index(ligne,' ')
 55       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 55
          endif
          txt2=ligne(n1+1:n1+4)
c          print*,'txt2  ',txt2
          if(txt2.eq.'shor') then
             kli=1
             inr=1
             WRITE(6,*)
     &    '" computation with the short line list (about 1000 lines)'
          else
             kli=0
          endif
c TURBULENCE    vturbkm
       else if(txt1.eq.'turb') then                         ! facultatif
          ktu=1
          n1=index(ligne,' ')
 56       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 56
          endif
          n2=index(ligne(n1+1:),' ') + n1
          READ(ligne(n1:n2),*,err=3)
          WRITE(6,'(a,f10.2)')' " turbulence velocity in km/s= ',vturbkm
c TEMPERATURE CONSTANTE       Tcst
       else if(txt1.eq.'temp') then                         ! facultatif
          kte=1
          itemp=10
          n1=index(ligne,' ')
 57       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 57
          endif
          n2=index(ligne(n1+1:),' ') + n1
          READ(ligne(n1:n2),*,err=3) Tcst
          WRITE(6,'(a,1pe12.4)') ' " constant temperature= ',Tcst
c RESOLUTION of the spectra output    resol
       else if(txt1.eq.'reso') then                         ! facultatif
          kre=1
          n1=index(ligne,' ')
 58       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 58
          endif
          n2=index(ligne(n1+1:),' ') + n1
          READ(ligne(n1:n2),*,err=3) resol
          WRITE(6,'(a,f8.2)')' " resolution of the lines in the '//
     &         'output spectra= ',resol
c COLUMN-GRID
       else if(txt1.eq.'grid') then                         ! facultatif
          kgr=1
          n1=index(ligne,' ')
 60      if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 60
          endif
          txt2=ligne(n1+1:n1+4)
          if(txt2.eq.'equa') then
             igrid=0
             n2=index(ligne(n1+1:),' ') + n1
 61          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 61
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) nstr
             ndcm=nstr
             WRITE(6,*) '"',nstr,' layers of equal thickness'
c
c          else if(txt2.eq.'auto') then
c             igrid=1
c             n2=index(ligne(n1+1:),' ') + n1
c 62          if(ligne(n2+1:n2+1).eq.' ') then
c                n2=n2+1
c                goto 62
c             endif
c             n3=index(ligne(n2+1:),' ') + n2
c             READ(ligne(n2:n3),*,err=3) ndcm
c             ndcm=nstr
c            print*,'" automatic column-grid with about ',nstr,' layers'
c
          else if(txt2.eq.'file') then
             igrid=11
             kde=1
             n2=index(ligne(n1+1:),' ') + n1
 63          if(ligne(n2+1:n2+1).eq.' ') then
                n2=n2+1
                goto 63
             endif
             n3=index(ligne(n2+1:),' ') + n2
             READ(ligne(n2:n3),*,err=3) fimzn
 64          if(ligne(n3+1:n3+1).eq.' ') then
                n3=n3+1
                goto 64
             endif
             n4=index(ligne(n3+1:),' ') + n3
             READ(ligne(n3:n4),*,err=3) nstr
             ndcm=nstr
             WRITE(6,*)
     &       '" file ',fimzn,' gives column-grid with ',nstr,' layers'
          else
             goto 3
          endif
c ITERATION NUMBER
       else if(txt1.eq.'iter') then                         ! facultatif
          kiter=1
          n1=index(ligne,' ')
 65       if(ligne(n1+1:n1+1).eq.' ') then
             n1=n1+1
             goto 65
          endif
          n2=index(ligne(n1+1:),' ') + n1
          READ(ligne(n1:n2),*,err=3) nfmax
          WRITE(6,*) '"',nfmax,' iterations maximum'
c
       else if(txt1.eq.'endf') then                         ! facultatif
          goto 2
       else
          goto 3
       endif     
       goto 1
 3                   WRITE(6,*) 'STOP: this line of '//
     &      'the input file is not correct, please read the user guide'
                     WRITE(6,*) ligne
                     stop
 2     continue
       if(kin.eq.0.or.kx.eq.0.or.kde.eq.0.or.kco.eq.0) then 
         if(kin.eq.0)WRITE(6,*)'STOP: put or check the line "incident"'
          if(kx.eq.0.and.incid.ne.2)  WRITE(6,*)
     &               'STOP: put or check the line "csit" or "flyman"'
          if(kde.eq.0) WRITE(6,*) 'STOP: put or check the lines '//
     &                        '"density" or "grid file"'
          if(kco.eq.0) WRITE(6,*)
     &               'STOP: put or check the line "column-density"'
          stop
       endif
       if(csit.eq.0.d0.and.flym.eq.0.d0.and.incid.ne.2) then
          WRITE(6,*) 'STOP: put or check the line "csit" or "flyman"'
          stop
       endif
       if(kx.gt.1) then
          WRITE(6,*) 'STOP: put ONE line "csit" OR "flyman"'
          stop
       endif
       if(kdef.eq.1.and.kgr.eq.1) then
          WRITE(6,*) 'STOP: the lines "density file" and "grid"'//
     &                ' are not compatible'
          stop
       endif
       if(kx.eq.1.and.csit.eq.0.d0.and.incid.eq.9) then
          WRITE(6,*)
     &    'STOP: sorry, please put a line "csit", not a line "flyman"'
          stop
       endif
       if(kti.eq.0) WRITE(6,*) ' " title: TITAN given density model'
       if(kab.eq.0) WRITE(6,*) ' " default abundances'
       if(kba.eq.0) irent=0
       if(kli.eq.0) then
          inr=2
          WRITE(6,*) '" computation with about 4000 lines'
       endif
       if(kdi.eq.0) then
          ndirlu=103
          ndir=4
          incang=1
          WRITE(6,*)
     &    '" incident flux normal; computation with',ndir,' directions'
       endif
       if(ktu.eq.0) vturbkm=0.d0
       if(kte.eq.0) itemp=0
       if(kre.eq.0) then
          resol=100.
          WRITE(6,*)
     &    '" resolution of the lines in the output spectra= 100'
       endif
       if(kgr.eq.0) igrid=1
c       if(kgr.eq.0) ndcm=0
       if(kiter.eq.0) then
          nfmax=50
          WRITE(6,*) '" 50 iterations maximum'
       endif
       WRITE(6,*)'"""""""""""""""""""""""""""""""""""""""""""""""""""""'
       WRITE(6,*)
       return
       end                                                     !lectentreetexte
c------------------------------------------------------------------------------
       subroutine LECTATOM
! lit la version 20 des atomic.dat - read the version 20 of atomic.dat

       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200)
       parameter (nups=10)
! LC variables in common
       integer dr_itype,rr_itype,iverbose
       logical ldebug
!
       character ch*1,ligne*79,nom*7,nomion*7,nomrai*7
c       dimension t1(10),t2(10),t3(10),t4(10),it(10)
       dimension omega(150),ae(150),dv(150),gg(150)
       dimension g3n(7,3),ae3(7,6,3),om3(7,6,3),dv3(7,6,3),nbrele(7)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/abond/ab(10)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/nom/nomion(322),nomrai(0:nrais)
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/raies2/g1rt(nrais),g2rt(nrais),n1rt(nrais),n2rt(nrais)
c     &                                      ,jrt(nrais),irt(nrais)
       common/raies4/gups(nups,nrais)
       common/raiesuta/probuta(100),dnuta2(100),utanion(26,10),
     &                 juta(100),iuta(100)
       common/raiesadd/is(3200),js(3200)                                    !le
       common/lecnu/sab,cmas
       common/lecdeg/eps1,eps2,sel
       common/lecdat/adi(26,10),tdi0(26,10),bdi(26,10),tdi1(26,10),
     &      athe(26,10),pthe(26,10),tthe(26,10),
     &      aech(26,10),bech(26,10),aeche(26,10),beche(26,10),
     &      arrn(nivtots),brrn(nivtots),crrn(nivtots),drrn(nivtots),
     &      nbq(nivtots)
       common/lexc/aexc(nrais),bexc(nrais),kex(nrais)
       common/lecpho/sigs(nivtots),s(nivtots),av(nivtots),
     &        hki2(nivtots),sigs2(nivtots),s2(nivtots),a2(nivtots),
     &        pol1(5),pol2(5),pol3(5),pol4(5),pol5(5),
     &        pkiel(26,10),sigsl(26,10),sl(26,10),al(26,10),
     &        sigsk(26,10),sk(26,10),ak(26,10),ksig(nivtots),j2b(5)
       common/leclos/omega,ae,dv,gg,g3n,ae3,om3,dv3,nbrele
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/rpal2/kali(nrais),npbali(nrais),lcompt(nzs,nrais)
! LC COMMON
! /lcf/ is for various double precision float numbers or arrays
! /lci/ is for various integer numbers or arrays
! WARN: if you mix integer and float, put all integer first.
! /debug/ is for debugging flags used everywhere
! /lecdr/ is for dielectronic recombination (DR) rates read form atomic 
!         data file (new format)
! /lecrr/ is for total radiative recombination (RR) rates read form atomic
!         data file (new block <total-radrec>)
! 
! coltdrop is the column-density for which we compure p11 models below 
!         and p10 models above.
! it permits to force some intermediates models between p11 and p10.
! nitpg is the iteration number where extra Heating-Cooling curves are 
!         printed every iteration  to .pgt when ifich=7 or 8.
! COMMON debug includes all flags relative to debug modes (verbose printing,etc)
! ldebug: print some debugging messages
! iverbose: level of verbosity of debugging messages, 1: minimum, 2: details, 
!           3: all
! dr_itype(integer): type of fitting algorithm for DR rates 
!           (1: usual Pequignot, 2: new Badnell)
! dr_coeff(float): DR coeffs, whatever the algorithm from dr_type. 
!           dr_coeff(1,i)=a_i, dr_coeff(2,i)=t_i
! rr_itype(integer): type of fitting algorithm for total RR rates
!           (0: use builtin RRFIT routine, 1: usual Pequignot equivalent
!           to 0 but in atomic.dat, 2: new Badnell)
! rr_coeff(float): total RR coeffs, whatever the algorithm from rr_type
! (equal to -1. when not defined).
       common/lcf/coltdrop
       common/lci/nitpg
       common/debug/ldebug,iverbose
       common/lecdr/dr_coeff(2,9,26,10),dr_itype(26,10)
       common/lecrr/rr_coeff(6,26,10),rr_itype(26,10)
       save /atom/,/lecnu/,/lecdeg/,/lecdat/,/lexc/,/lecpho/,/leclos/,
     &      /raies2/,/raies4/,/gnv/,/rpal2/,/lecdr/,/lecrr/
! lectatom
c       imp=10
       hcse=12398.54d0                                 ! h*c/e'*1e8
          nelem=10          ! lus ici plutot que dans le atomic.dat
          jmax=10
       eps1=0.0003          ! test de la convergence des degres d'ionisation
       eps2=0.0003
       nalire=8

 1000  READ(11,'(a1,a79)') ch,ligne
       if(ch.eq.'!'.or.ch.eq.'#'.or.ch.eq.'{'.or.ch.eq.'}') then
          goto 1000
!!
       else if(ch.eq.'@'.and.ligne(1:8).eq.'elements') then
 101      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 101
! donnees pour les ELEMENTS : numero_atomique_Z, abondance, masse_atomique
c          READ(11,'(2i5)') nelem,jmax
          sa=0.d0
          do j=2,jmax
             sa=sa+ab(j)
          enddo
          do j=1,jmax
             READ(11,'(5x,i5,2f10.3)') iz(j),abdef,pmat(j)
             if(sa.le.0.d0) ab(j)=abdef
          enddo
          sab=1.d0
          cmas=1.d0
          do j=2,nelem
             sab=sab+ab(j)
             cmas=cmas+ab(j)*pmat(j)
          enddo
          cmas=cmas*1.67352d-14/2.d0        ! mH *1e5**2/2
          kc=0
          do j=1,nelem
             iz0=iz(j)
             do i=1,iz0
                kc=kc+1
                jct(kc)=j
                ict(kc)=i
             enddo
          enddo
          READ(11,'(a1)') ch
          if(ch.ne.'}') then
       if(ila.eq.0)write(6,*)'Pb de lecture du atomic.dat ds <elements>'
       if(ila.eq.1)write(6,*)'Pb in atomic.dat reading in <elements>'
             stop
          endif
!          print*,'element',iz(jmax)
          goto 1000
!!
       else if(ch.eq.'@'.and.ligne(1:13).eq.'nblevli-sigkl') then
 102      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 102
! donnees atomiques pour les IONS - photoionisations K et L
          kc=0
          do j=1,jmax
             iz0=iz(j)
             do ii=1,iz0
                kc=kc+1
                READ(11,'(a7,3i3,2e9.2,3f6.3,e9.2,3f6.3)') nom,
     &          i,niv,nraie,pkiv,pkik,sigk,ssk,aak,pkil,sigl,ssl,aal
                if(ii.ne.i) then
          if(ila.eq.0) WRITE(6,*) ' PB lecture des ions a',ii,j,i,pkiv
          if(ila.eq.1) WRITE(6,*) ' PB in ion reading at',ii,j,i,pkiv
                   stop
                endif
                nomion(kc)=nom
                nivion(i,j)=niv
                nrai(i,j)=nraie
                pkiev(i,j)=pkiv
                pkiek(i,j)=pkik
                sigsk(i,j)=sigk*1.d-20
                sk(i,j)=ssk
                ak(i,j)=aak                                                 !le
                pkiel(i,j)=pkil
                sigsl(i,j)=sigl*1.d-20
                sl(i,j)=ssl
                al(i,j)=aal
                thnuc(kc)=pkiv
             enddo   ! i
           enddo   ! j
!
           do kc=103,220
              nomion(kc)='       '
           enddo
           do kc=221,322
              nomion(kc)=nomion(kc-220)
           enddo
           sel=0.d0
           do j=1,jmax
              iz0=iz(j)
              do i=0,iz0+1
                 nivcum(i,j)=0
                 nrcum(i,j) =0
              enddo
           enddo
           nni=0
           nnr=0
           do j=1,jmax
              if(j.eq.1) then                                              !le
                 nivcum(0,j)=0
                 nrcum(0,j)=0
              else
                 iz1=iz(j-1)
                 nivcum(0,j)=nivcum(iz1+1,j-1)
                 nrcum(0,j)=nrcum(iz1+1,j-1)
              endif   ! j/1
              iz0=iz(j)
              sel=sel + ab(j)*dfloat(iz0)
!                 i=0
!                 write(6,'(4i6)') j,i,nivcum(i,j),nrcum(i,j)
              do i=1,iz0
                 nni=nni + nivion(i,j)
                 nnr=nnr + nrai(i,j)
                 nivcum(i,j)=nni
                 nrcum(i,j) =nnr
!                 write(6,'(4i6)') j,i,nivcum(i,j),nrcum(i,j)
              enddo   !i
              nni=nni+1
              nivcum(iz0+1,j)=nivcum(iz0,j)+1
              nrcum(iz0+1,j)=nrcum(iz0,j)
!              write(6,'(4i6)') j,iz0+1,nivcum(iz0+1,j),nrcum(iz0+1,j)
           enddo   ! j
           nraissfek=nrcum(26,10)
           nraimax=nrcum(26,10)+24
!          WRITE(10,*) 'sel=',sel
           nivt=nivcum(27,10)                                              !le
          READ(11,'(a1)') ch
          if(ch.ne.'}') then
      if(ila.eq.0)write(6,*)'Pb de lecture du atomic.dat ds <ion-sigkl>'
      if(ila.eq.1)write(6,*)'Pb in atomic.dat reading in <ion-sigkl>'
             stop
          endif
c          print*,'ion-sigkl',nraimax
!!
       else
          if(ila.eq.0) write(6,*) 'Pb de lecture du atomic.dat'//
     &         ' - series obligatoires au debut: element & ion-sigkl'
          if(ila.eq.1) write(6,*) 'Pb in atomic.dat reading'//
     &         ' - necessary sets at beginning: element & ion-sigkl'
          stop
       endif
!!
       nsetlu=0
 2000  READ(11,'(a1,a79)') ch,ligne
       if(ch.eq.'!'.or.ch.eq.'#'.or.ch.eq.'{'.or.ch.eq.'}') then
          goto 2000                                                        !le
!!
       else if(ch.eq.'@'.and.ligne(1:14).eq.'colion-dielrec') then
 201      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 201
! DONNEES ATOMIQUES POUR LES IONS - collisions & dielectroniques
!            coef. for ionization by collision, and dielectronic recombination
! ionisations par collisions:Peq.idem + Shull-VanSteenberg 82 pour ions eleves
! recombinaisons dielectroniques:donnees de Bryans-Badnell...2006 ApJs,167,343
!                 (anciennement: Pequignot 86 (de son programme) & web Verner)
          nsetlu=nsetlu+1
          do j=1,jmax
             iz0=iz(j)
             gfond(iz0+1,j)=1.d0
             do i=1,iz0
!LC new atomic17.dat format (update DR rates with Badnell data 2006)
                READ(11,1001)
     &               ii,gfond(i,j),athe(i,j),pthe(i,j),tthe(i,j),
     &               dr_itype(i,j),((dr_coeff(m,k,i,j),m=1,2),k=1,9)
 1001  format(7x,1x,i3,1x,f4.1,1x,e8.2,1x,f4.2,1x,f9.4,1x,i1,
     &        18(1x,e10.3))
!
!LC dr_itype=1: use adi,bdi,tdi0,tdi1  Pequignot 86 & web Verner
                if (dr_itype(i,j).eq.1) then
                   adi(i,j) = dr_coeff(1,1,i,j)
                   bdi(i,j) = dr_coeff(2,1,i,j)
                   tdi0(i,j) = dr_coeff(1,2,i,j)
                   tdi1(i,j) = dr_coeff(2,2,i,j)
                endif
! LC DEBUG              
                if (ldebug) write(*,*) 'DR coeff(i,j,io,gfond,athe,'/
     &  /'pthe,tthe,adi,bdi,tdi0,tdi1,dr_type,dr_coeff)=',
     &  i,j,ii,gfond(i,j),athe(i,j),pthe(i,j),tthe(i,j),adi(i,j),
     &  bdi(i,j),tdi0(i,j),tdi1(i,j),dr_itype(i,j),
     &  ((dr_coeff(m,k,i,j),m=1,2),k=1,9)
!
                if(ii.ne.i) then
                   if(ila.eq.0) WRITE(6,*)
     &         'PB lecture des donnees dielectroniques a',j,i,gfond(i,j)
                   if(ila.eq.1) WRITE(6,*)
     &         'PB in reading dielectronic data for',j,i,gfond(i,j)
                   stop
                endif
             enddo   ! i
          enddo   ! j
!
          READ(11,'(a1)') ch                                            !le
          if(ch.ne.'}') then
             if(ila.eq.0) write(6,*)
     &       'Pb de lecture du atomic.dat dans <colion/recdiel>'
             if(ila.eq.1) write(6,*)
     &       'Pb in atomic.dat reading in <colion/recdiel>'
             stop
          endif
          if(nsetlu.eq.nalire) then
c             print*,'nsetlu=',nsetlu
             goto 3001
          endif
          goto 2000
!!
       else if(ch.eq.'@'.and.ligne(1:12).eq.'total-radrec') then
 209      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 209
! DONNEES ATOMIQUES POUR LES IONS - coefficients de recombinaison radiative
! totale (sur tous les niveaux)
! donnees de Badnell 2006 ApJs,167,343
! les recombinaisons radiatives totales peuvent etre calculees dans Titan
! par RRFIT, provenant des donnees web Verner
          nsetlu=nsetlu+1
          do j=1,jmax
             iz0=iz(j)
             do i=1,iz0
                READ(11,'(7x,1x,i3,1x,i1,6(1x,e10.3))')
     &               ii,rr_itype(i,j),(rr_coeff(k,i,j),k=1,6)
! LC DEBUG
                if (ldebug)
     &  write(*,*) 'RR coeff(i,j,io,rr_type,rr_coeff)=',
     &  i,j,ii,rr_itype(i,j),(rr_coeff(k,i,j),k=1,9)
!
                if(ii.ne.i) then
                   if(ila.eq.0) WRITE(6,*)
     &            'PB de lecture des recomb. radiatives totales a',j,i
                   if(ila.eq.1) WRITE(6,*)
     &            'PB in reading total radiative recombinations at',j,i
                   stop
                endif
             enddo
          enddo
          READ(11,'(a1)') ch                                               !le
          if(ch.ne.'}') then
             if(ila.eq.0) write(6,*)
     &       'Pb de lecture du atomic.dat dans <total-radrec>'
             if(ila.eq.1) write(6,*)
     &       'Pb in atomic.dat reading in <total-radrec>'
             stop
          endif
          if(nsetlu.eq.nalire) then
c             print*,'nsetlu=',nsetlu
             goto 3001
          endif
            goto 2000
!!
       else if(ch.eq.'@'.and.ligne(1:14).eq.'level-photoion') then
 202      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 202
! DONNEES ATOMIQUES POUR LES NIVEAUX  photoionisations
!                                          data for  levels : photoionizations
! photoionisations <== fits de TOPBASE - formule du cas general :
! (x=hkin(nij)/hnuev)  sigma=sigs(nij)*x**s(nij) *(1 + av(nij) - av(nij)*x)
! ==>i,n,gps(nij),hkin(nij),ksig(nij),sigs(nij),s(nij),av(nij),
!    sigs2(nij),s2(nij),a2(nij)
! gps=poids statistique du niveau / hkin=energie d'ionisation
! ksig: option de calcul des sigma, sections efficaces de photoionisation
! les sigs(au seuil) lus sont *1e20
          nsetlu=nsetlu+1
          do nij=1,nivt
             READ(11,'(2x,2i3,i2,f5.0,i2,f9.2,3f6.3,f9.2,3f6.3)') i,n,
     &       nbq(nij),gps(nij),ksig(nij),hkin(nij),sig,s(nij),av(nij),
     &       hki2(nij),sig2,s2(nij),a2(nij)
             sigs(nij)=sig*1.d-20
             sigs2(nij)=sig2*1.d-20
          enddo
          READ(11,'(a1)') ch                                               !le
          if(ch.ne.'}') then
             if(ila.eq.0) write(6,*) 'Pb de lecture du atomic.dat '
     &         //'dans <photoionisation> - nombre de niveaux faux'
             if(ila.eq.1) write(6,*) 'Pb in atomic.dat reading '
     &         //'in <photoionisation> - wrong level number'
             stop
           endif
          if(nsetlu.eq.nalire) then
c             print*,'nsetlu=',nsetlu
             goto 3001
          endif
          goto 2000
!!
       else if(ch.eq.'@'.and.ligne(1:16).eq.'neutral-photoion') then
 203      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 203
! DONNEES ATOMIQUES POUR LES NIVEAUX FONDAMENTAUX DES NEUTRES
!                     data for ground levels of neutral atom: photoionizations
! photoionisations <== polynomes
          nsetlu=nsetlu+1
          do j6=1,5
             READ(11,'(10x,i5,5x,5f10.4)')
     &            j2b(j6),pol1(j6),pol2(j6),pol3(j6),pol4(j6),pol5(j6)
          enddo
          READ(11,'(a1)') ch                                               !le
          if(ch.ne.'}') then
             if(ila.eq.0) write(6,*)
     &          'Pb de lecture du atomic.dat dans <neutral-photo>'
             if(ila.eq.1) write(6,*)
     &          'Pb in atomic.dat reading in <neutral-photo>'
             stop
          endif
          if(nsetlu.eq.nalire) then
c             print*,'nsetlu=',nsetlu
             goto 3001
          endif
          goto 2000
!!
       else if(ch.eq.'@'.and.ligne(1:12).eq.'level-radrec') then
 204            READ(11,'(a1)') ch
          if(ch.ne.'{') goto 204
! DONNEES ATOMIQUES POUR LES NIVEAUX  recombinaisons radiatives
! formule Pequignot 1991 A&A251,680 :              tt4=T*1.d-4/float(i)**2
!    alfint(nij)=arrn(nij)*tt4**(-brrn(nij))/(1.d0 + crrn(nij)*tt4**drrn(nij))
          nsetlu=nsetlu+1
          do nij=1,nivt
             READ(11,'(2x,2i3,f8.4,3f7.4)')
     &            i,n,a1,brrn(nij),crrn(nij),drrn(nij)
             arrn(nij)=a1*1.d-13*float(i)
          enddo
          READ(11,'(a1)') ch                                               !le
          if(ch.ne.'}') then
             if(ila.eq.0) write(6,*)'Pb de lecture du atomic.dat dans '
     &         //'<rad-recomb> - nombre de niveaux faux'
             if(ila.eq.1) write(6,*)'Pb in atomic.dat reading in '
     &         //'<rad-recomb> - wrong level number'
             stop
          endif
          if(nsetlu.eq.nalire) then
c             print*,'nsetlu=',nsetlu
             goto 3001
          endif
          goto 2000
!!
       else if(ch.eq.'@'.and.ligne(1:12).eq.'fluorescence') then
 205      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 205
! DONNEES ATOMIQUES POUR LA FLUORESCENCE K DU FER
!                                         Fe K lines : line energy and yield
! apres ionisation de la couche K,  proba de la fluorescence / effet Auger
! et energie en eV de la raie defluorescence
          nsetlu=nsetlu+1
          izfer=iz(10)-2
          READ(11,'(7x,f7.3,f7.1)') (pfluo(i),efluo(i),i=1,izfer)
c          efluo(25)=pkiev(25,10)  idiot
c          efluo(26)=pkiev(26,10)
          READ(11,'(a1)') ch                                               !le
          if(ch.ne.'}') then
             if(ila.eq.0) write(6,*)
     &          'Pb de lecture du atomic.dat dans <fluorescence>'
             if(ila.eq.1) write(6,*)
     &          'Pb in atomic.dat reading in <fluorescence>'
             stop
          endif
          if(nsetlu.eq.nalire) then
c             print*,'nsetlu=',nsetlu
             goto 3001
          endif
          goto 2000
!!
       else if(ch.eq.'@'.and.ligne(1:12).eq.'continuum-fr') then
 206      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 206
! donnees pour les FREQUENCES du continu - "trous" entre 8ev et 10kev
! valeurs imposees des dhnu au dessus de 7300ev
          nsetlu=nsetlu+1
          READ(11,'(11f7.2)') (thnuc(kc),kc=103,156)
          READ(11,*)
          READ(11,'(3(i6,f7.2,f6.3,f7.3))')
     &         (kc,thnuc(kc),tde(kc),tdeb(kc),k=1,18)
          READ(11,'(a1)') ch                                               !le
          if(ch.ne.'}') then
             if(ila.eq.0) write(6,*)
     &          'Pb de lecture du atomic.dat dans <continuum-freq>'
             if(ila.eq.1) write(6,*)
     &          'Pb in atomic.dat reading in <continuum-freq>'
             stop
          endif
          if(nsetlu.eq.nalire) then
c             print*,'nsetlu=',nsetlu
             goto 3001
          endif
          goto 2000
!!
       else if(ch.eq.'@'.and.ligne(1:12).eq.'charge-excha') then
 207      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 207
! DONNEES ATOMIQUES POUR LES ECHANGES DE CHARGES
! 3 papiers: Butler et al ApJ 1980 et 1979 + Pequignot 1990 pour O
          nsetlu=nsetlu+1
          do j=3,jmax
             do ii=1,4
                READ(11,'(2x,i2,1x,4f10.4)')
     &               i,aech(i,j),bech(i,j),aeche(i,j),beche(i,j)
                if(ii.ne.i) then
                   if(ila.eq.0) WRITE(6,*)
     &             'PB lecture des echanges de charges a',j,i,aech(i,j)
                   if(ila.eq.1) WRITE(6,*)
     &             'PB in charge exchanges reading at',j,i,aech(i,j)
                   stop
                endif
             enddo
          enddo
          READ(11,'(a1)') ch                                               !le
          if(ch.ne.'}') then
             if(ila.eq.0) write(6,*)
     &          'Pb de lecture du atomic.dat dans <charge-exch>'
             if(ila.eq.1) write(6,*)
     &          'Pb in atomic.dat reading in <charge-exch>'
             stop
          endif
          if(nsetlu.eq.nalire) then
c             print*,'nsetlu=',nsetlu
             goto 3001
          endif
          goto 2000
!!
       else if(ch.eq.'@') then
          if(ila.eq.0) write(6,*) 'Pb de lecture du atomic.dat ?'
          if(ila.eq.1) write(6,*) 'Pb in atomic.dat reading ?'
 208      READ(11,'(a1)') ch
          if(ch.ne.'}') goto 208
          goto 2000
!!
       else
          if(ila.eq.0) write(6,*)
     &    'Pb de lecture du atomic.dat -1er caractere non autorise'
          if(ila.eq.1) write(6,*)                                          !le
     &    'Pb in atomic.dat reading - non permitted 1st character'
          goto 2000
       endif
!!
 3001  nsetlu=0
 3000  READ(11,'(a1,a79)') ch,ligne
       if(ch.eq.'!'.or.ch.eq.'#'.or.ch.eq.'{'.or.ch.eq.'}') then
          goto 3000
!!
       else if(ch.eq.'@'.and.ligne(1:6).eq.'lines1') then
 301      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 301
! DONNEES ATOMIQUES POUR LES RAIES 1ere serie   1=bas 2=haut
!                                       data for lines (1=lower, 2=upper level)
! Nist Topbase
! kam: option de calcul des profils de raies (1 sauf qques raies de resonance)
! kex: option de calcul des excitations-deexc. par collisions(coef.Aexc & Bexc)
          nsetlu=nsetlu+1
          nraimax2=nraimax
          nbrh=nrai(1,1)
          nomrai(0)='       '
          do j=1,jmax
             iz0=iz(j)
             do i=1,iz0
                kri=nrcum(i-1,j)+1
                krf=nrcum(i,j)
                do kr=kri,krf                                              !le
                   READ(11,'(a7,3i4,i3,2f5.0,e10.2,e9.3,2i2,2e9.3)')
     &            nomrai(kr),izZ,ir,n1rt(kr),n2rt(kr),g1rt(kr),g2rt(kr),
     &            alo(kr),fdo(kr),kam(kr),kex(kr),aexc(kr),bexc(kr)
                   if(ir.ne.i.or.izZ.ne.iz0) then
                      if(ila.eq.0) then
          WRITE(6,*) ' PB lecture des raies a',kr,ir,alo(kr),j,i,kri,krf
          if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &    WRITE(15,*)' PB lecture des raies a',j,i,alo(kr)
                      else
          WRITE(6,*) ' PB line reading at',kr,ir,alo(kr),j,i,kri,krf
          if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &    WRITE(15,*)' PB line reading at',j,i,alo(kr)
                      endif
                      stop
                   endif
                   if(fdo(kr).lt.fdolim) nraimax2=nraimax2-1
                   if(kex(kr).eq.6.or.kex(kr).eq.7) then
                      READ(11,'(e7.3,9e8.3)') (gups(it,kr),it=1,nups)
                      do it=1,nups
                         gups(it,kr)=gups(it,kr)/g1rt(kr) *8.63d-6
                      enddo     ! it
                   endif        ! kex=6
                enddo           ! kr
             enddo   ! ir
          enddo   ! j                                                      !le
          do i=1,izfer
             kr=nraissfek+i
             alo(kr)=hcse/efluo(i)
             fdo(kr)=0.4d0
             kex(kr)=3
             aexc(kr)=0.d0
             bexc(kr)=0.d0
             kk=nrcum(i-1,10)+1
             nomrai(kr)=nomrai(kk)
          enddo
          READ(11,*) nuta,ns
          READ(11,'(a1)') ch
          if(ch.ne.'}') then
      if(ila.eq.0)write(6,*)'Pb de lecture du atomic.dat dans <lines1>'
      if(ila.eq.1)write(6,*)'Pb in atomic.dat reading in <lines1>'
             stop
          endif
          if(nsetlu.eq.4) then
c             print*,'nsetlu=',nsetlu
             goto 4000
          endif
          goto 3000
!!
       else if(ch.eq.'@'.and.ligne(1:6).eq.'lines2') then
          nsetlu=nsetlu+1
 302      READ(11,'(a1)') ch
          if(inr.eq.2) then
             if(ch.ne.'{') goto 302
          else   ! inr=1 on ne lit pas les 3000 raies supplementaires
             if(ch.ne.'}') goto 302
             goto 3000
          endif
! raies de resonance tirees de Cloudy (2007), fichier vlines.dat
! nr2 est lu a la fin de la serie 'lines1'
          do ks=1,ns
             kr=nraissfek+24+nuta + ks
             READ(11,'(a7,3i4,3x,2f5.0,f10.3,f9.6,i2,i2)') 
     &       nomrai(kr),izZ,is(ks),js(ks),g1rt(kr),g2rt(kr),alo(kr),
     &       fdo(kr),kam(kr),kex(kr)
             n1rt(kr)=1
             j=js(ks)
             aexc(kr)=0.d0
             bexc(kr)=0.d0
                  if(izZ.ne.iz(j)) then
                      if(ila.eq.0) then
          WRITE(6,*) ' PB lecture des raies a',izZ,is(ks),kr,alo(kr)
          if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &    WRITE(15,*)' PB lecture des raies a',izZ,is(ks),kr,alo(kr)
                      else
          WRITE(6,*) ' PB line reading at',izZ,is(ks),kr,alo(kr)
          if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &    WRITE(15,*)' PB line reading at',izZ,is(ks),kr,alo(kr)
                      endif
                  endif
          enddo
!
          READ(11,'(a1)') ch                                               !le
          if(ch.ne.'}') then
      if(ila.eq.0)write(6,*)'Pb de lecture du atomic.dat dans <lines2>'
      if(ila.eq.1)write(6,*)'Pb in atomic.dat reading in <lines2>'
             stop
          endif
          if(nsetlu.eq.4) then
c             print*,'nsetlu=',nsetlu
             goto 4000
          endif
          goto 3000
!!
       else if(ch.eq.'@'.and.ligne(1:9).eq.'uta-lines') then
 303      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 303
! raies de la couche interne (UTA) de Behar, Netzer, 2002,ApJ 570,165 
! & raies UTA du fer regroupees comme dans Behar,Sako,Kahn,2001 ApJ 563,497
! (elles ont une largeur en Angstrom dlambda / width=dlambda )
! probabilite de fluorescence probuta(k)=A21/(A21+Aa)     Aa:autoionisation
          nsetlu=nsetlu+1
c          READ(11,*) nuta - nuta est lu a la fin de la serie 'lines1'
          do ku=1,nuta
             kr=nraissfek+24 + ku
             READ(11,'(a7,3i4,3x,2f5.0,e10.3,e9.3,2i2,2e9.4)') 
     &       nomrai(kr),izZ,iuta(ku),juta(ku),g1rt(kr),g2rt(kr),
     &       alo(kr),fdo(kr),kam(kr),kex(kr),probuta(ku),dlambda
             j=juta(ku)
             aexc(kr)=0.d0
             bexc(kr)=0.d0
             dnuta2(ku)=(dlambda*2.99792458d18/alo(kr)**2)**2 *2.d0
          enddo
!
          READ(11,'(a1)') ch                                               !le
          if(ch.ne.'}'.or.izZ.ne.iz(j)) then
             if(ila.eq.0) write(6,*)
     &          'Pb de lecture du atomic.dat dans <UTA-lines>'
             if(ila.eq.1) write(6,*)
     &          'Pb in atomic.dat reading in <UTA-lines>'
             stop
          endif
!
           nraimax=nraimax + nuta
           nraimax2=nraimax2 + nuta
              if(inr.eq.2) then
           nraimax=nraimax + ns
           nraimax2=nraimax2 + ns
              endif
           if(ila.eq.0) WRITE(10,75) nivt,nraimax,nivtots,nrais
           if(ila*iexpert.eq.1) WRITE(10,975) nivt,nraimax,nivtots,nrais
 75     format(' nombre total de niveaux=',i4,' nombre total de raies=',
     &         i5/' dimension des tableaux=',i4,i5/)
 975    format(' total number of levels=',i4,' total number of lines=',
     &         i5/' array bounds=',i4,i5/)
               if(ila.eq.0) then
c          WRITE(6,70)  nivcum(27,10),nraimax,nraimax2,nivtots,nrais
          if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &    WRITE(15,70) nivcum(27,10),nraimax,nraimax2,nivtots,nrais
               else
c          WRITE(6,970) nivcum(27,10),nraimax,nraimax2,nivtots,nrais
          if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &    WRITE(15,970)nivcum(27,10),nraimax,nraimax2,nivtots,nrais
               endif
 70    format(' nombre total de niveaux=',i4,' nombre total de raies=',
     &        i5,' transferees=',i5/' dimension des tableaux=',i4,i5/)
 970   format(' total number of levels=',i4,' total number of lines=',
     &         i5,' transfered lines=',i5/' array bounds=',i4,i5/)
          if(nivcum(27,10).gt.nivtots.or.nraimax.gt.nrais) then
             WRITE(6,*) 'TABLEAUX TROP PETITS : revoir les dimensions'
             if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &       WRITE(15,*) 'TABLEAUX TROP PETITS : revoir les dimensions'
             stop
          endif                                                             !le
!
c          print*,'raies-UTA',nsetlu
          if(nsetlu.eq.4) then
c             print*,'nsetlu=',nsetlu
             goto 4000
          endif
          goto 3000
!!
       else if(ch.eq.'@'.and.ligne(1:12).eq.'forbid-lines') then
 304      READ(11,'(a1)') ch
          if(ch.ne.'{') goto 304
! donnees pour quelques raies interdites
! ae,omega,gg,dv(150=>76),puis g3n(7,3),puis nbrele(7),puis ae3,om3,dv3(7,6,3)
! formats (8e9.3) (26f3.0) (26i3) (9e8.3)
          nsetlu=nsetlu+1
          mmay=76
          READ(11,'(8e9.3)') (ae(m),omega(m),gg(m),dv(m),m=1,mmay)
          READ(11,'(26f3.0)') ((g3n(i,l),l=1,3),i=1,7)
          READ(11,'(26i3)') (nbrele(i),i=1,7)
          READ(11,'(9e8.3)') (((ae3(i,j,k),om3(i,j,k),dv3(i,j,k),k=1,3)
     &         ,j=1,6),i=1,7)
          READ(11,'(a1,a79)') ch,ligne                                     !le
          if(ch.ne.'}'.and.ligne(1:1).eq.'.') then
             if(ila.eq.0) write(6,*)
     &          'Pb de lecture du atomic.dat dans <forbid-lines>'
             if(ila.eq.1) write(6,*)
     &          'Pb in atomic.dat reading in <forbid-lines>'
             stop
          endif
c          print*,'r-interdites',nsetlu
          if(nsetlu.eq.4) then
c             print*,'nsetlu=',nsetlu
             goto 4000
          endif
          goto 3000
!!
       else if(ch.eq.'@') then
          if(ila.eq.0) write(6,*) 'Pb de lecture du atomic.dat ?'
          if(ila.eq.1) write(6,*) 'Pb in atomic.dat reading ?'
 309      READ(11,'(a1)') ch
          if(ch.ne.'}') goto 309
          goto 2000
!!
       else
          if(ila.eq.0) write(6,*)
     &    'Pb de lecture du atomic.dat-1er caractere non autorise'
          if(ila.eq.1) write(6,*)
     &    'Pb in atomic.dat reading - non permitted 1st character'
          goto 3000
       endif
!!
!                  impression des niveaux dans le fichier .atom
 4000  if(iexpert.le.0) goto 4001
       if(ila.eq.0) WRITE(10,*) 'NIVEAUX :'
       if(ila.eq.1) WRITE(10,*) 'LEVELS :'
       if(ila.eq.0) WRITE(10,*) 'ion   niv/ion n  nij ki_niv  g '//
     &             'nivcum rai/ion kri krf j  i  n  nij  ki_niv  g'
       if(ila.eq.1) WRITE(10,*) 'ion   lev/ion n  nij ki_lev'//
     &             ' g levcum line/ion kri krf j  i  n  nij  ki_lev  g'
        kc=0                                                                !le
        do j=1,10
           iz0=iz(j)
           do i=1,iz0
              kc=kc+1
              nij=nivcum(i-1,j) + 1
              niv=nivion(i,j)
              n=1
           if(niv.eq.1) WRITE(10,71) nomion(kc),niv,n,nij,hkin(nij),
     &        gps(nij),nivcum(i,j),nrai(i,j),nrcum(i-1,j)+1,nrcum(i,j)
           if(niv.ge.2) WRITE(10,72) nomion(kc),niv,n,nij,hkin(nij),
     &        gps(nij),nivcum(i,j),nrai(i,j),nrcum(i-1,j)+1,nrcum(i,j),
     &        (j,i,n,nij-1+n,hkin(nij-1+n),gps(nij-1+n),n=2,2)
           if(niv.gt.2) WRITE(10,73) (j,i,n,nij-1+n,hkin(nij-1+n),
     &        gps(nij-1+n),n=3,niv)
           enddo
           nij=nivcum(iz0+1,j)
           WRITE(10,74) nij,gps(nij)
        enddo
 71     format(a7,i4,i5,i4,f8.2,f4.0,i4,i7,i5,i4)
 72     format(a7,i4,i5,i4,f8.2,f4.0,i4,i7,i5,i4,i4,2i3,i4,f9.3,f4.0)
 73     format(53x,3i3,i4,f9.3,f4.0)
 74     format('stripped',i12,f12.0/)
cion   niv/ion n  nij ki-niv  g nivcum rai/ion kri krf j  i  n  nij  ki_niv  g
ceXXIII   6    1 142 9277.12  2.  14     10  153 162   1  1  2   2    3.400  2.
ctripped         153          1.
c
        if(ila.eq.0) WRITE(10,70)
     &      nivcum(27,10),nraimax,nraimax2,nivtots,nrais                   !le
c
c prescrition KALI :
c kali=0:raie non transteree/kali=1:transfert 2dir/kali=2:ali simple/kali=3:mali
c ne pas mettre en kali=2 les raies tres interdites
c kali=0:non transfered line/kali=1:transfer 2dir/kali=2:simple ali/kali=3:mali
c do not put kali=2 for very forbidden lines
 4001   do kr=1,nraimax
           npbali(kr)=0
           kali(kr)=2
        enddo   !kr
        do j=1,10
           iz0=iz(j)
           do i=1,iz0
              kri=nrcum(i-1,j)+1
              krf=nrcum(i,j)
              if(j.ge.3.and.i.eq.1) then                        ! neutres
                 do kr=kri,krf
                    kali(kr)=1
                 enddo
              else if(j.eq.10.and.i.eq.2) then                     ! FeII
                 do kr=kri,krf
                    kali(kr)=1
                 enddo
              else if(nivion(i,j).gt.1) then         ! ions multi-niveaux
                 do kr=kri,krf
                    kali(kr)=3
                 enddo
              endif
           enddo
        enddo
        do kr=nraissfek,nraissfek+16
           kali(kr)=1
        enddo   !kr
        do kr=1,nraissfek
           if(fdo(kr).lt.fdolim) kali(kr)=0
        enddo   !kr

c                     do kr=1,nraimax
c                        npbali(kr)=0
c                        kali(kr)=2
c        if(fdo(kr).lt.fdolim) then
c           kali(kr)=0
c        else
c           if(kr.le.15) kali(kr)=3                                        ! H
c           if(kr.ge.16.and.kr.le.45) kali(kr)=3                           ! He0
c           if(kr.ge.46.and.kr.le.55) kali(kr)=3                           ! He+
c              do j=3,10
c                 iz0=iz(j)
c       if(kr.ge.nrcum(0,j)+1.and.kr.le.nrcum(1,j)) kali(kr)=1         !neutre
cc       if(kr.ge.nrcum(1,j)+1.and.kr.le.nrcum(2,j)) kali(kr)=1             !+
cc       if(kr.ge.nrcum(2,j)+1.and.kr.le.nrcum(3,j)) kali(kr)=1             !++
c       if(kr.ge.nrcum(iz0-3,j)+1.and.kr.le.nrcum(iz0-2,j)) kali(kr)=3     !Li
c       if(kr.ge.nrcum(iz0-2,j)+2.and.kr.le.nrcum(iz0-1,j)) kali(kr)=3     !Hel
c       if(kr.ge.nrcum(iz0-1,j)+1.and.kr.le.nrcum(iz0,j)) kali(kr)=3       !Hyd
c              enddo
c       if(kr.ge.nrcum(3,5)+1.and.kr.le.nrcum(4,5)) kali(kr)=3             !O4
c       if(kr.ge.nrcum(4,5)+1.and.kr.le.nrcum(5,5)) kali(kr)=3             !O5
c       if(kr.gt.nraissfek.and.kr.le.nraissfek+16) kali(kr)=1          !1ers FeK
c       if(kr.ge.nrcum(1,10)+1.and.kr.le.nrcum(2,10)) kali(kr)=1           !Fe2
cc       if(kr.ge.nrcum(1,4)+1.and.kr.le.nrcum(2,4)) kali(kr)=1             !N2
cc       if(kr.ge.nrcum(2,4)+1.and.kr.le.nrcum(3,4)) kali(kr)=1             !N3
cc       if(kr.ge.nrcum(2,8)+1.and.kr.le.nrcum(3,8)) kali(kr)=1             !Si3
cc       if(kr.ge.nrcum(2,10)+1.and.kr.le.nrcum(3,10)) kali(kr)=1           !Fe3
cc       if(kr.ge.nrcum(3,10)+1.and.kr.le.nrcum(4,10)) kali(kr)=1           !Fe4
cc       if(kr.ge.nrcum(2,5)+1.and.kr.le.nrcum(3,5)) kali(kr)=2             !O3
cc       if(kr.ge.nrcum(2,6)+1.and.kr.le.nrcum(3,6)) kali(kr)=2             !Ne3
cc       if(kr.ge.nrcum(2,7)+1.and.kr.le.nrcum(3,7)) kali(kr)=2             !Mg3
c        endif
c                     enddo   ! kr
c                     kali(6)=1
                     kali(7)=0
                     kali(8)=0
                     kali(9)=0
                     kali(15)=1

              kra=0
           do j=1,nelem
              iz0=iz(j)
              do i=1,iz0
                 kri=kra+1
                 kra=kra+nrai(i,j)
                 do kr=kri,kra
                    if(ab(j).le.0.d0) kali(kr)=0
                    if(nivion(i,j).eq.1.and.kali(kr).eq.3) kali(kr)=2
                 enddo
              enddo
           enddo
           if(ab(10).le.0.d0) then
              do kr=nraissfek+1,nraissfek+24
                 kali(kr)=0
              enddo
           endif
           if(inr.eq.2) then
              do ks=1,ns
                 kr=nraissfek+24+nuta + ks
                 j=js(ks)
                 if(ab(j).le.0.d0) kali(kr)=0
              enddo
           endif
           do ku=1,nuta
              kr=nraissfek+24 + ku
              j=juta(ku)
              if(ab(j).le.0.d0) kali(kr)=0
           enddo
c
        if(imp.lt.10) goto 7700
c
c        write(10,'(a78)') titre
              write(10,7100) nelem,jmax
 7100   format('  nombre d elements : nelem=',i5,'  jmax=',i5)
        write(10,7101)eps1,eps2
 7101   format('  limites d iteration :',2f10.5//
     1'  nombre Z de protons,abondance,poids atomique')
        do 7001 j=1,jmax  
           write(10,7102) iz(j),ab(j),pmat(j)
 7001   continue
 7102   format(5x,i5,1pg10.3,0pf10.3)
c
        write(10,270)
  270   format(/' poids statistique du niveau fondamental,',
     1  'coef.pour l ionisation thermique (collisions),',
     2  'coef. pour les recombinaisons radiatives,'/
     3  'coef. pour les recomb. dielectroniques: gfond(i,j),athe(i,j),',
     4  'pthe(i,j),tthe(i,j),'/
     5  'adi(i,j),bdi(i,j),tdi0(i,j),tdi1(i,j)')
        do 7011 j=1,jmax                                                    !le
        iz0=iz(j)
        do 7011 i=1,iz0
        write(10,271) iz0,j,gfond(i,j),athe(i,j),pthe(i,j),tthe(i,j),
     1   adi(i,j),bdi(i,j),tdi0(i,j),tdi1(i,j)
 7011    continue
  271   format(' z=',i2,' i=',i2,3(1pg12.4),1pg14.6,6(1pg12.4))
        write(10,272)
  272   format(/' coef. pour les photoionisations et nbre de raies'/
     1   ' hkin(nij),sigs(nij),s(nij),av(nij),pkiek(i,j),pkiel(i,j),',
     2   'sigsk(i,j),tde(kc),nrai(i,j)')
        kc=0
        do 7012 j=1,jmax
           iz0=iz(j)
           do 7013 i=1,iz0
              nmax=nivion(i,j)
           do 7013 n=1,nmax
              nij=nivcum(i-1,j) + n
              if(n.eq.1) kc=kc+1
        write(10,273) iz0,i,pkiev(i,j),sigs(nij),s(nij),av(nij),
     1   pkiek(i,j),pkiel(i,j),sigsk(i,j),tde(kc),nrai(i,j)
 7013        continue
  273   format(' z=',i2,' i=',i2,1pg14.6,4(1pg12.4),1pg14.6,
     1          2(1pg12.4),i10)
        j6=j-6
        if(j6.ge.1) write(10,274) iz0,
     1         pol1(j6),pol2(j6),pol3(j6),pol4(j6),pol5(j6),j2b(j6)
        if(j6.eq.4) write(10,274) iz0,
     1         pol1(5),pol2(5),pol3(5),pol4(5),pol5(5),j2b(5)
 7012        continue
  274 format(' coef.pol.neutre z=',i2,5(1pg12.4),' jmax 2 branches=',i3)
        write(10,275)
  275   format(/' coefficients des echanges de charges avec H et He')
        do 7014 j=1,jmax
        iz0=iz(j)
        do 7014 i=1,iz0
        write(10,271) iz0,i,aech(i,j),bech(i,j),aeche(i,j),beche(i,j)
 7014   continue                                                            !le
        write(10,276)
  276   format(/' frequences supplementaires pour les integrations ',
     1          'et leurs dhnu, en ev')
        write(10,'(8f9.3)') (thnuc(k),tde(k),k=103,174)
        write(10,277)
  277   format(/' probabilites et energies des raies de fluorescence',
     1          ' du fer')
        izfer=iz(10)-2
        write(10,*) (pfluo(i),efluo(i),i=1,izfer)
c
      write(10,374)
  374 format(/)
      write(10,375) nraissfek
  375 format(5x,'nraissfek=nbre de raies permises=',i10)
      write(10,376)                                                         !le
  376 format(5x,'longueurs d ondes et forces d oscillateur des raies ',
     1        'permises')
        kra=0
      do 7004 j=1,jmax
      iz0=iz(j)
      do 7004 i=1,iz0
        kri=kra+1
        kra=kra+nrai(i,j)
      write(10,377) j,i,(alo(kr),fdo(kr),kr=kri,kra)
 7004 continue
  377 format (i2,i3,5(f8.1,f7.4)/5x,5(f8.1,f7.4))
      write(10,378)
  378 format(5x,'kex')
      write(10,379)(kex(kr),kr=1,nraissfek)
  379 format(5x,70i1)
      mmay=76                                                               !le
      write(10,380) mmay
  380 format(5x,'mmay=nbre de raies interdites (sauf atomes neutres)='
     1,i10)
      write(10,381)
  381 format(5x,'ae=pot. d exc., omega=f. de coll./poids stat., gg=g1/',
     1'g2, dv=prob. de trans. radiative, (raies int. sf at. neutres)')
      write(10,382) (ae(m),omega(m),gg(m),dv(m),m=1,mmay)
  382 format(1p8e13.5)
      write(10,383)
  383 format(9x,'ions a 3 niveaux',/,5x,'pds stat. et nbre elect.')
      write(10,384) ((g3n(i,l),l=1,3),i=1,7)
  384 format(26f4.0)
      write(10,*) (nbrele(i),i=1,7)
      write(10,385)
  385 format(5x,'energie,omega,inv. duree de vie')                          !le
      write(10,386)(((ae3(i,j,k),om3(i,j,k),dv3(i,j,k),k=1,3),j=1,6),
     1i=1,7)
  386 format(1p9e11.3)
      write(10,387)
 387  format(/,5x,'rec. dielect., adi, tdi0, bdi, tdi1')
      do 7006 j=1,jmax
      iz0=iz(j)
      write(10,388)(adi(i,j),tdi0(i,j),bdi(i,j),tdi1(i,j),i=1,iz0)
 7006 continue
 388  format(1pe9.3,1p7e10.3)
c
 7700        continue
      return
      end                                                             !lectatom
c------------------------------------------------------------------------------
       subroutine LECTINCID

c lecture du fichier fli.x si incid= 15 ou 16

       implicit double precision (a-h,o-z)
       parameter (nivtots=330,nrais=4200)
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/tabm/exflux(322),thnur(nrais)
       common/tablecinc/kctrie(322)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/talfdeg/sigmac(220,nivtots),sigmacv(220,nivtots),
     &           sigmack(220,nivtots),sigmark(nrais,nivtots)
       common/talfdeg2/exphot(nivtots),exphotk(nivtots)
       common/flx/flxdurincid,acompt,bcompt,ccompt,dcompt,ecompt,fcompt,
     &            scomptx1,scomptx2,ftauth
c lectincid
c  if(incid.ge.11) OPEN(UNIT=12,FILE=fifli,STATUS='OLD') - fait ds le main
c
       pi4=12.5663706144d0
       do kc=1,322
          exflux(kc)=0.d0
       enddo
       write(10,*)
       write(10,*) 'SPECTRE entre dans fli.'
       READ(12,*)               ! titre
       READ(12,*) nbf
       write(6,*) ' INCIDENT FILE : number of frequencies=',nbf
          if(ifich.ge.1.and.ifich.le.8)
     & write(15,*)' INCIDENT FILE : number of frequencies=',nbf
       e=-100.d0
       f=-100.d0
       n=0
 10    ep=e
       fp=f
       n=n+1
       READ(12,*) en,fl
          write(6,*) '                 first line ',en,fl
       if(incid.eq.16) fl=fl/en
       e=log10(en)
       if(fl.ge.1.d-100) f=log10(fl)
c       print'(i4,f11.4,3(1pe12.3))',n,en,fl,e,f
       if(en.lt.thnuc(220)) goto 10
          if(ifich.ge.1.and.ifich.le.8)
     &    write(15,*)'                 first line ',en,fl
          write(6,*)
          if(ifich.ge.1.and.ifich.le.8) write(15,*)
       eti=log10(thnuc(220))
c interpolation normale, lineaire en log-log
       exflux(220)=10.d0**(fp + (f-fp)*(eti-ep)/(e-ep))
       do 20 ik=2,322
          kc=kctrie(ik)                                                     !li
          eti=log10(thnuc(kc))
          if(en.gt.thnuc(kc)) then
             exflux(kc)=10.d0**(fp + (f-fp)*(eti-ep)/(e-ep))
          else if(en.eq.thnuc(kc)) then
             exflux(kc)=fl
          else  ! en<thnuc(kc)
 11                    ep=e
             fp=f
             n=n+1
             if(n.le.nbf) then
                READ(12,*) en,fl
                if(incid.eq.16) fl=fl/en
                e=log10(en)
                f=log10(fl)
                if(en.lt.thnuc(kc)) go to 11
                exflux(kc)=10.d0**(fp + (f-fp)*(eti-ep)/(e-ep))
             else   ! n=nbf+1
                e=log10(thnuc(kc))+0.05d0
                f=-100.d0
                exflux(kc)=10.d0**(fp + (f-fp)*(eti-ep)/(e-ep))
                goto 12
             endif
          endif
c        print'(a,i4,f14.4,1pe14.3)','kc hnu flu',kc,thnuc(kc),exflux(kc)
c       print'(2i4,3f11.4,3(1pe12.3))',
c     &        kc,n,10.d0**ep,thnuc(kc),en,10.d0**fp,exflux(kc),fl
 20        continue                    ! ik
c
c calcul de exphot(nij)                                                     !li
 12    continue
        do 41 jn=1,nelem
           inz0=iz(jn)
        do 41 in=1,inz0
           niv=nivion(in,jn)
        do 41 nn=1,niv
           nij=nivcum(in-1,jn) + nn
           som=0.d0
           somk=0.d0
           akiev=hkin(nij)
           do 42 kc=1,184
              hnuev=thnuc(kc)
              de=tde(kc)
              deb=tdeb(kc)
              dea=de-deb
              de1=de
                 if(kc.le.102) then
                    ic=ict(kc)
                    jc=jct(kc)
                    if(nn.eq.1.and.jc.eq.jn.and.ic.eq.in) de1=deb
                 endif
              if(hnuev+deb.gt.akiev) then
                 if(hnuev-dea.le.akiev) hnuev=akiev
                 som=som + sigmac(kc,nij)*de1/hnuev *exflux(kc)
                 somk=somk + sigmack(kc,nij)*de/hnuev *exflux(kc)
              endif
 42        continue    ! kc                                                 !li
           exphot(nij)=som/6.6262d-27 *2.d0 !*2 si refl~inc
           exphotk(nij)=somk/6.6262d-27 *2.d0
 41     continue
c        print*,'exphot',exphot(1),exphotk(1)
c
c calcul de flxdurincid
c        print*,'n,nbf',n,nbf
       if(n.eq.nbf) goto 100
          cind=hcse*hcse/chel/2.d+16
       flxdurincid=0.d0
       scomptx1=0.d0
       scomptx2=0.d0                ! mise a 0 manquante dans titan105v03d
       q=1.1d0
       dq=2.d0*(q-1.d0)/(q+1.d0)
       hnuev=thnuc(184)
       do 30 kx=1,32                               ! >26 kev -> 500kev
          hnuev=hnuev*q                            ! q=1.1
          dhnu=hnuev*dq
          eti=log10(hnuev)
c          print'(i4,3f11.4)',kx,b1,b2
          if(en.ge.hnuev) then
             exf=10.d0**(fp + (f-fp)*(eti-ep)/(e-ep))
          else if(en.eq.thnuc(kc)) then
             exf=fl
          else   ! en<hnuev
 31                    ep=e
             fp=f
             n=n+1
             if(n.le.nbf) then
                READ(12,*) en,fl
                if(incid.eq.16) fl=fl/en
                e=log10(en)
                f=log10(fl)
                if(en.lt.hnuev) go to 31
                exf=10.d0**(fp + (f-fp)*(eti-ep)/(e-ep))
             else   ! n=nbf+1
                e=log10(thnuc(kc))+0.05d0
                f=-100.d0
                exf=10.d0**(fp + (f-fp)*(eti-ep)/(e-ep))
             endif
c             print'(2i4,1pe10.3)',kx,m,exf
          endif
c        print'(a,i4,f14.4,1pe14.3)','kx hnu flu',kx,hnuev,exf
c       print'(2i4,3f11.4,3(1pe12.3))',
c     &        kx,n,10.d0**ep,hnuev,en,10.d0**fp,exf,fl
          flxdurincid=flxdurincid + exf*dhnu
c en vue du calcul du chauffage Compton par formule de Tarter
             altar=1.d0/(1.d0+ hnuev*8.6675d-6 + hnuev*hnuev*3.827d-12)
c                 scomptx1=scomptx1 + exf*dhnu *altar*hnuev  ! sans les induits
             scomptx1=scomptx1 + exf*dhnu *altar*hnuev
     &             *(1.d0+exf/pi4/(hnuev**3)*cind)        ! avec les induits
c on a ici exp(-hnu/kT) a peu pres egal a 1
             scomptx2=scomptx2 + exf*dhnu *altar*
     1          (1.d0- altar*hnuev*(8.6675d-6 + hnuev*7.655d-12)/4.d0)
c          endif
          if(n.gt.nbf) goto 32
 30           continue
c
c ftauth=1.d0 + tauth/(1.d0+tauth)pour tenir compte du diffuse
 32    flxdurincid=flxdurincid *2.41797d14
       scomptx1=scomptx1/pi4 *ftauth
       scomptx2=scomptx2/pi4 *ftauth
c
 100    CLOSE(12)
       return
       end                                                           !lectincid
c--============================================================================
        subroutine TABSIG

c mise en tableau des energies des photons et des pas en energie, ainsi que
c des sections de photoionisation pour les energies utilisees

       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200)
         dimension iw(nrais)
         character tab*1,nomion*7,nomrai*7,txt*3
c       common/photab/sigma,sigmal,sigmak,hnuev,in,jn,nn,nij
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/nom/nomion(322),nomrai(0:nrais)
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/raies2/g1rt(nrais),g2rt(nrais),n1rt(nrais),n2rt(nrais)
       common/raies3/A21(nrais),A21hnu(nrais),A21hnus4pi(nrais),
     &               B12(nrais),B12hnus4pi(nrais),B12hnu2s4pi(nrais)
       common/raiesuta/probuta(100),dnuta2(100),utanion(26,10),
     &                 juta(100),iuta(100)
       common/rpal2/kali(nrais),noali(nrais),lcompt(nzs,nrais)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/flx/flxdurincid,acompt,bcompt,ccompt,dcompt,ecompt,fcompt,
     &            scomptx1,scomptx2,ftauth
       common/talfdeg/sigmac(220,nivtots),sigmacv(220,nivtots),
     &           sigmack(220,nivtots),sigmark(nrais,nivtots)
       common/talfdeg2/exphot(nivtots),exphotk(nivtots)
       common/tabstpo/sigmar(nrais,nivtots)
       common/tabm/exflux(322),thnur(nrais)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/lexc/aexc(nrais),bexc(nrais),kex(nrais) !supprimer si pas d'impr.
       common/tablecinc/kctrie(322)
       save /atom/,/raies2/,/raies3/,/talfdeg/,/tabstpo/,/gnv/,/lexc/,
     & /tablecinc/
c tabsig
        hca=1.986485d-8
c
c TABLEAUX des hnu et des dhnu DES PHOTONS DU CONTINU et des flux incidents :
c energies hnu(kc) egales aux ki(ic,jc) + valeurs pour les "trous"(de Lectatom)
c puis en progression geometrique au dessus de 10 keV et en dessous de 6.5 ev
c on conserve les de-deb au-dessus de 7kev car ils avaient ete soigneusement
c etudies en janv 1999
        if(iexpert.eq.1) WRITE(10,*) 
        if(ila.eq.0) WRITE(10,*) 'CONTINU trie :'
        if(ila*iexpert.eq.1) WRITE(10,*) 'CONTINUUM sorted :'
        if(iexpert.eq.1) WRITE(10,*)' no         kc    hnuev     dhnu'//
     &                 '     dhnua    dhnub        interval      diff'
c progressions geometriques
            q=1.1d0
            dq=2.d0*(q-1.d0)/(q+1.d0)
            hnuev=10525.35d0/q
                           do kc=175,184                        ! 10->26 kev
            hnuev=hnuev*q
        thnuc(kc)=hnuev
        tde(kc)=hnuev*dq
        tdeb(kc)=hnuev*dq/2.d0
                           enddo
            q1=1.2d0
            dq1=2.d0*(q1-1.d0)/(q1+1.d0)
            hnuev=6.5d0 *q1
                           do kc=185,220                      ! 0.01->7.64 ev
            hnuev=hnuev/q1
        thnuc(kc)=hnuev
            dhnu=hnuev*dq1
            dhnub=dhnu/2.d0
              if(kc.eq.185) then
              dhnu=1.314d0
              dhnub=0.7231d0
              endif   ! kc=185
        tde(kc)=dhnu
        tdeb(kc)=dhnub
                           enddo
c tri du continu brut                                                       !ta
        n=220
        call INDEXTRIr(n,thnuc,iw)
c calcul des de et deb et ecriture
        kc=iw(37)       ! kc=35 Mg1 7.64ev
           km=iw(36)
           kp=iw(38)
        tdeb(kc)=(thnuc(kp)-thnuc(kc))/2.d0
        tde(kc)=tdeb(kc) + thnuc(kc)-thnuc(km)-tdeb(km)
        it=0
                           do i=1,37
        kc=iw(i)
                     if(kc.le.102) then
                        it=it+1
                        kctrie(it)=kc+220
                        it=it+1
                        kctrie(it)=kc
                     else
                        it=it+1
                        kctrie(it)=kc
                     endif
           dea=tde(kc)-tdeb(kc)
           if(i.gt.1) then
              km=iw(i-1)
              dd=thnuc(kc)-tde(kc)+tdeb(kc)-thnuc(km)-tdeb(km)
           endif
        if(iexpert.eq.1) 
     &  WRITE(10,'(i4,i12,f9.6,f10.7,2f9.6,1x,2f9.6,f7.4)')
     &        i,kc,thnuc(kc),tde(kc),
     &        dea,tdeb(kc),thnuc(kc)-dea,thnuc(kc)+tdeb(kc),dd
                           enddo
        if(iexpert.eq.1) WRITE(10,*)' no  ion    kc    hnuev     dhnu'//
     &                   '     dhnua    dhnub        interval      diff'
                          do i=38,190
        kc=iw(i)
                     if(kc.le.102) then
                        it=it+1
                        kctrie(it)=kc+220
                        it=it+1
                        kctrie(it)=kc
                     else
                        it=it+1
                        kctrie(it)=kc
                     endif
           km=iw(i-1)
           kp=iw(i+1)
        tdeb(kc)=(thnuc(kp)-thnuc(kc))/2.d0
        tde(kc)=(thnuc(kp)-thnuc(km))/2.d0
           dea=tde(kc)-tdeb(kc)
           dd=thnuc(kc)-tde(kc)+tdeb(kc)-thnuc(km)-tdeb(km)
           if(kc.le.102) then
              iz0=iz(jct(kc))
        if(iexpert.eq.1)
     &  WRITE(10,'(i4,1x,a7,i4,f9.2,3f9.3,2x,2f9.3,f7.4)') 
     &        i,nomion(kc),kc,thnuc(kc),tde(kc),dea,tdeb(kc),
     &        thnuc(kc)-dea,thnuc(kc)+tdeb(kc),dd
           endif
           if(kc.gt.102.and.iexpert.eq.1)
     &  WRITE(10,'(i4,i12,f9.2,f9.3,2f9.3,2x,2f9.3,f7.4)') 
     &        i,kc,thnuc(kc),tde(kc),dea,tdeb(kc),
     &        thnuc(kc)-dea,thnuc(kc)+tdeb(kc),dd
                           enddo
        kc=iw(201)       ! kc=101 Fe25 8828.1ev                             !ta
           km=iw(200)
           kp=iw(202)
        tdeb(kc)=thnuc(kp)-tde(kp)+tdeb(kp) -thnuc(kc)
        tde(kc)=tdeb(kc) + thnuc(kc)-thnuc(km)-tdeb(km)
        kc=iw(206)       ! kc=102 Fe26 9278ev
           km=iw(205)
           kp=iw(207)
        tdeb(kc)=thnuc(kp)-tde(kp)+tdeb(kp) -thnuc(kc)
        tde(kc)=tdeb(kc) + thnuc(kc)-thnuc(km)-tdeb(km)
        kc=iw(210)       ! kc=174 9830ev
           km=iw(209)
           kp=iw(211)
           dea=tde(kc)-tdeb(kc)
        tdeb(kc)=thnuc(kp)-thnuc(kc) -tde(kp)+tdeb(kp)
        tde(kc)=tdeb(kc) + dea
        if(iexpert.eq.1) WRITE(10,*)' no  ion    kc    hnuev     dhnu'//
     &                   '     dhnua    dhnub        interval      diff'
                           do i=191,220
        kc=iw(i)
                     if(kc.le.102) then
                        it=it+1
                        kctrie(it)=kc+220
                        it=it+1
                        kctrie(it)=kc
                     else
                        it=it+1
                        kctrie(it)=kc
                     endif
           km=iw(i-1)
           dea=tde(kc)-tdeb(kc)
           dd=thnuc(kc)-tde(kc)+tdeb(kc)-thnuc(km)-tdeb(km)
           if(kc.le.102) then
              iz0=iz(jct(kc))
              if(iexpert.eq.1)
     &  WRITE(10,'(i4,1x,a7,i4,f9.2,f10.3,2f9.3,1x,2f9.2,f7.4)') 
     &        i,nomion(kc),kc,thnuc(kc),tde(kc),dea,tdeb(kc),
     &        thnuc(kc)-dea,thnuc(kc)+tdeb(kc),dd
           endif
           if(kc.gt.102.and.iexpert.eq.1)
     &  WRITE(10,'(i4,i12,f9.2,f10.3,2f9.3,1x,2f9.2,f7.4)') 
     &        i,kc,thnuc(kc),tde(kc),dea,tdeb(kc),thnuc(kc)-dea,
     &        thnuc(kc)+tdeb(kc),dd
                           enddo
                           do kc=221,322
        thnuc(kc) = thnuc(kc-220)
                           enddo                                            !ta
c
c calcul du flux incident
        if(iexpert.eq.1) WRITE(10,*) 
        if(ila.eq.0) WRITE(10,*) 'CONTINU :'
        if(ila*iexpert.eq.1) WRITE(10,*) 'CONTINUUM :'
        if(iexpert.eq.1) WRITE(10,*) '  ion     kc  hnuev       dhnu'//
     &                '        dhnub                interval'
        do kc=1,184
           hnuev=thnuc(kc)
           if(incid.eq.1) exflux(kc)=FFLUXLP(hnuev)
           if(incid.eq.2) exflux(kc)=FFLUXBB(hnuev)*pi
           if(incid.eq.3) exflux(kc)=FFLUX(hnuev)
           if(incid.eq.4) exflux(kc)=FFLUX1(hnuev)
           if(incid.eq.5) exflux(kc)=FFLUX2(hnuev)
           if(incid.eq.6) exflux(kc)=FFLUX3(hnuev)
           if(incid.eq.7) exflux(kc)=FFLUX4(hnuev)
           if(incid.eq.8) exflux(kc)=FFLUX5(hnuev)
           if(incid.eq.9) exflux(kc)=FFLUX6(hnuev)
           if(incid.eq.10)exflux(kc)=FFLUX7(hnuev)
           if(incid.eq.17)exflux(kc)=FFLUXECO1LP(hnuev)       !Agata
           if(incid.eq.18)exflux(kc)=FFLUXECO2LP(hnuev)       !Agata
        enddo
        if(iexpert.eq.1) then
                             do kc=1,102
!            iz0=iz(jct(kc)) ! used ?
        hnuev=thnuc(kc)
        WRITE(10,'(2x,a7,i4,3(1pg12.5),4x,2(1pg13.6))') nomion(kc),kc,
     &  thnuc(kc),tde(kc),tdeb(kc),thnuc(kc),thnuc(kc)+tdeb(kc)
                             enddo
        WRITE(10,*) '          kc  hnuev       dhnu'//
     &                '        dhnub                interval'
                             do kc=103,174
        hnuev=thnuc(kc)
        WRITE(10,'(9x,i4,3(1pg12.5),4x,2(1pg13.6))') kc,thnuc(kc),
     &  tde(kc),tdeb(kc),thnuc(kc)-tde(kc)+tdeb(kc),thnuc(kc)+tdeb(kc)
                             enddo
                             do kc=175,184                        ! ->26 kev
        hnuev=thnuc(kc)
        WRITE(10,'(9x,i4,3(1pg12.5),4x,2(1pg13.6))') kc,thnuc(kc),
     &  tde(kc),tdeb(kc),thnuc(kc)-tde(kc)+tdeb(kc),thnuc(kc)+tdeb(kc)
                             enddo
        endif
            cind=hcse*hcse/chel/2.d+16
            flxdurincid=0.d0
c            scomptx1ssi=0.d0
            scomptx1=0.d0
            scomptx2=0.d0
                             do kx=1,32                   ! >26 kev -> 500kev
            hnuev=hnuev*q                                 ! q=1.1
            dhnu=hnuev*dq
            dhnub=dhnu/2.d0
c            if(incid.eq.1.or.incid.eq.3.or.incid.ge.11.or.incid.eq.17.
c     &         or.incid.eq.18) then                             !Agata
            if(incid.eq.1.or.incid.eq.3.or.incid.ge.11) then
               if(hnuev-dhnub.gt.hnumax) goto 100
               if(hnuev+dhnub.gt.hnumax) then
                  dhnu=hnumax - hnuev+dhnub
                  hnuev=(hnumax + hnuev-dhnub)/2.d0
               endif   ! hnumax
            endif   ! incid=1,3,11,12,13,14
        if(incid.eq.1) exf=FFLUXLP(hnuev)
        if(incid.eq.2) exf=FFLUXBB(hnuev)*pi
        if(incid.eq.3) exf=FFLUX(hnuev)
        if(incid.eq.4) exf=FFLUX1(hnuev)
        if(incid.eq.5) exf=FFLUX2(hnuev)
        if(incid.eq.6) exf=FFLUX3(hnuev)
        if(incid.eq.7) exf=FFLUX4(hnuev)
        if(incid.eq.8) exf=FFLUX5(hnuev)
        if(incid.eq.9) exf=FFLUX6(hnuev)
        if(incid.eq.10)exf=FFLUX7(hnuev)
        if(incid.ge.11)exf=exflux(184)*(thnuc(184)/hnuev)**flindex !a 14 compris
        if(incid.eq.17) exf=FFLUXECO1LP(hnuev)              !Agata
        if(incid.eq.18) exf=FFLUXECO2LP(hnuev)              !Agata
        flxdurincid=flxdurincid + exf*dhnu
c en vue du calcul du chauffage Compton
           altar=1.d0/(1.d0+ hnuev*8.6675d-6 + hnuev*hnuev*3.827d-12)
           scomptx1=scomptx1 + exf*dhnu *altar*hnuev
     &             *(1.d0+exf/pi4/(hnuev*hnuev*hnuev) *cind) ! avec les induits
c on a ici exp(-hnu/kT) a peu pres egal a 1
           scomptx2=scomptx2 + exf*dhnu *altar*
     1          (1.d0- altar*hnuev*(8.6675d-6 + hnuev*7.655d-12)/4.d0)
        if(iexpert.eq.1)WRITE(10,'(9x,i4,3(1pg12.5),4x,2(1pg13.6))') 
     &  kx,hnuev,dhnu,dhnub,hnuev-dhnub,hnuev+dhnub
                             enddo   ! kx
 100    flxdurincid=flxdurincid *2.41797d14
           scomptx1=scomptx1/pi4 *ftauth
           scomptx2=scomptx2/pi4 *ftauth
c
        if(iexpert.eq.1)WRITE(10,*) '          kc  hnuev       dhnu'//
     &                '        dhnub                interval'
                             do kc=185,220                      ! 0.01->7.64 ev
           hnuev=thnuc(kc)
           hnuev1=hnuev
           dhnub=tdeb(kc)
           dhnua=tde(kc)-dhnub
c           if(hnuev+dhnub.gt.hnumin.and.hnuev-dhnua.lt.hnumin.and.
c     &     (incid.eq.1.or.incid.eq.3.or.incid.eq.17.or.incid.eq.18))   !Agata
           if(hnuev+dhnub.gt.hnumin.and.hnuev-dhnua.lt.hnumin.and.
     &     (incid.eq.1.or.incid.eq.3))
     &      hnuev1=(hnumin+hnuev+dhnub)/2.d0
        if(incid.eq.1) exflux(kc)=FFLUXLP(hnuev1)
        if(incid.eq.2) exflux(kc)=FFLUXBB(hnuev1)*pi
        if(incid.eq.3) exflux(kc)=FFLUX(hnuev1)
        if(incid.eq.4) exflux(kc)=FFLUX1(hnuev1)
        if(incid.eq.5) exflux(kc)=FFLUX2(hnuev1)
        if(incid.eq.6) exflux(kc)=FFLUX3(hnuev1)
        if(incid.eq.7) exflux(kc)=FFLUX4(hnuev1)
        if(incid.eq.8) exflux(kc)=FFLUX5(hnuev1)
        if(incid.eq.9) exflux(kc)=FFLUX6(hnuev1)
        if(incid.eq.10) exflux(kc)=FFLUX7(hnuev1)
        if(incid.eq.17) exflux(kc)=FFLUXECO1LP(hnuev)       !Agata
        if(incid.eq.18) exflux(kc)=FFLUXECO2LP(hnuev)       !Agata
        if(iexpert.eq.1)
     &  WRITE(10,'(9x,i4,3(1pg12.5),4x,2(1pg13.6))') kc,thnuc(kc),
     &  tde(kc),tdeb(kc),thnuc(kc)-tde(kc)+tdeb(kc),thnuc(kc)+tdeb(kc)
                enddo
        if(iexpert.eq.1)WRITE(10,*) '          kc  hnuev       dhnu'//
     &                '        dhnub                interval'
                             do kc=221,322
           if(incid.le.10.or.incid.eq.17.or.incid.eq.18)
     &  exflux(kc)=exflux(kc-220)
        k=kc-220
        if(iexpert.eq.1)
     &  WRITE(10,'(9x,i4,3(1pg12.5),4x,2(1pg13.6))') kc,thnuc(kc), 
     &        tde(k),tdeb(k),thnuc(k)-tde(k)+tdeb(k),thnuc(k)
                             enddo                                         !ta
c
c calcul des probabilites de transition, du numero du continu sous-jacent
c et des sections de photoionisation pour les RAIES : sigmar(kr,nij)
       do k=1,nrais
          thnur(k)=0.d0
       enddo
       if(iexpert.eq.1) WRITE(10,*) 
       if(ila.eq.0) WRITE(10,*) 'RAIES :'
       if(ila*iexpert.eq.1) WRITE(10,*) 'LINES :'
       hcse=12398.54d0                                 ! h*c/e'*1e8
          kra=0
          jsup=jmax
          if(inr.eq.2) jsup=jmax+1
       do 10 jr=1,jsup
          if(jr.le.jmax) then
             irz0=iz(jr)
          else if(jr.eq.jmax+1) then
             irz0=1
          endif
       do 10 ir=1,irz0
          if(jr.le.jmax) then
             kri=kra+1
             kra=kra+nrai(ir,jr)
          else if(jr.eq.jmax+1) then
             kri=nraissfek+24+nuta + 1
             kra=nraissfek+24+nuta + ns
          endif
       do 11 kr=kri,kra
          if(mod(kr,100).eq.1.and.iexpert.eq.1) 
     &    WRITE(10,*)'ion     kr  lambda     n1'//
     &        ' n2  hnuev       f12        A21   kcr hnucont kex kali'
          alambda=alo(kr)
                                if(alambda.le.1.01d0) then
                                thnur(kr)=0.d0
                                kcr(kr)=220
                                goto 11
                                endif
          hnuev=hcse/alambda
                thnur(kr) = hnuev
                                if(hnuev.le.0.01078d0) then
                                kcr(kr)=220
          B12hnus4pi(kr)=fdo(kr)*0.02654d0
          B12hnu2s4pi(kr)=B12hnus4pi(kr)*hca/alambda
          A21(kr)=fdo(kr)*0.02654d0*pi*8.d16/(alambda**2)
                 if(jr.le.jmax.and.nivion(ir,jr).gt.1)
     &           A21(kr)=A21(kr)*g1rt(kr)/g2rt(kr)
          B12(kr)=B12hnus4pi(kr)*alambda*pi4/hca
          A21hnu(kr)=A21(kr)*hca/alambda
          A21hnus4pi(kr)=A21hnu(kr)/pi4
          if(iexpert.eq.1)then
        if(fdo(kr).ge.1.d-2) then            ! lambda > 1e6
            WRITE(10,72) nomrai(kr),kr,alo(kr),n1rt(kr),n2rt(kr),hnuev,
     &              fdo(kr),A21(kr),kcr(kr),thnuc(220),kex(kr),kali(kr)
        else
            WRITE(10,74) nomrai(kr),kr,alo(kr),n1rt(kr),n2rt(kr),hnuev,
     &              fdo(kr),A21(kr),kcr(kr),thnuc(220),kex(kr),kali(kr)
        endif
           endif
                                goto 11
                                endif
c       recherche du kc du continu correspondant a chaque raie
                        do kc=1,322
                        if(kc.le.102) then
                        hnuc=thnuc(kc)
                        d1=0.d0
                        d2=tdeb(kc)
                        else if(kc.le.220) then
                        hnuc=thnuc(kc)
                        d1=tde(kc)-tdeb(kc)
                        d2=tdeb(kc)
                        else 
                        hnuc=thnuc(kc-220)
                        d1=tde(kc-220)-tdeb(kc-220)
                        d2=0.d0
                        endif
        if(hnuev.ge.(hnuc-d1).and.hnuev.lt.(hnuc+d2)) then
        kcr(kr)=kc
                        goto 13
                        endif
                        enddo
                        write(10,*) 'pas de kc pour kr=',kr
   13                        continue
          B12hnus4pi(kr)=fdo(kr)*0.02654d0
          B12hnu2s4pi(kr)=B12hnus4pi(kr)*hca/alambda
          A21(kr)=fdo(kr)*0.02654d0*pi*8.d16/(alambda**2)
             if(jr.le.jmax) then
             if(nivion(ir,jr).gt.1.and.kr.le.nraissfek)
     &    A21(kr)=A21(kr)*g1rt(kr)/g2rt(kr)
             endif
c pour les raies niv=1 on ne connait pas g1 & g2, et on n'en a pas besoin
c A21 est alors egal a A21g2/g1
          B12(kr)=B12hnus4pi(kr)*alambda*pi4/hca
          A21hnu(kr)=A21(kr)*hca/alambda
          A21hnus4pi(kr)=A21hnu(kr)/pi4
          if(iexpert.eq.1) then
        if(fdo(kr).ge.1.d-2) then
           if(alambda.lt.1.d5) then
            WRITE(10,71) nomrai(kr),kr,alo(kr),n1rt(kr),n2rt(kr),hnuev,
     &              fdo(kr),A21(kr),kcr(kr),hnuc,kex(kr),kali(kr)
           else
            WRITE(10,72) nomrai(kr),kr,alo(kr),n1rt(kr),n2rt(kr),hnuev,
     &              fdo(kr),A21(kr),kcr(kr),hnuc,kex(kr),kali(kr)
           endif
        else   ! fdo < 1.d-2
           if(alambda.lt.1.d5) then
            WRITE(10,73) nomrai(kr),kr,alo(kr),n1rt(kr),n2rt(kr),hnuev,
     &              fdo(kr),A21(kr),kcr(kr),hnuc,kex(kr),kali(kr)
           else
            WRITE(10,74) nomrai(kr),kr,alo(kr),n1rt(kr),n2rt(kr),hnuev,
     &              fdo(kr),A21(kr),kcr(kr),hnuc,kex(kr),kali(kr)
           endif
        endif
          endif
c         nom   kr  alo    n1 n2  hnu     fdo     A21   kcr  hnu  kex kali
 71   format(a7,i5,1pg12.5,i2,i3,1pg11.4,0pf9.5,1pe12.3,i4,1pg10.3,2i2)!234567
 72   format(a7,i5,1pe10.3,i4,i3,1pg11.4,0pf9.5,1pe12.3,i4,1pg10.3,2i2)
 73   format(a7,i5,1pg12.5,i2,i3,1pg11.4,1pe11.4,1pe10.3,i4,1pg10.3,2i2)
 74   format(a7,i5,1pe10.3,i4,i3,1pg11.4,1pe11.4,1pe10.3,i4,1pg10.3,2i2)
 75   format(a7,i5,1pg12.5,5x,1pg11.4,0pf9.5,1pe12.3,i4,1pg10.3,2i2)
          do 12 jn=1,nelem                                                  !ta
            inz0=iz(jn)
          do 12 in=1,inz0
           niv=nivion(in,jn)
          do 12 nn=1,niv
           nij=nivcum(in-1,jn) + nn
                sigmar(kr,nij)=0.d0
                sigmark(kr,nij)=0.d0
                sigma=0.d0
                sigmal=0.d0
                sigmak=0.d0
            akiev=hkin(nij)
            if(hnuev.ge.akiev) then
        CALL sPHOTOION(hnuev,in,jn,nn,nij,sigma,sigmal,sigmak)
            if(jn.lt.3.or.in.gt.(inz0-2).or.hnuev.lt.pkiek(in,jn))
     &      sigmak=0.d0
        sigmar(kr,nij)=sigma+sigmal+sigmak
        sigmark(kr,nij)=sigmak
                endif   ! akiev
   12          continue                                      ! in,jn
   11        continue                                        ! kr
   10        continue                                        ! ir,jr
c        write(15,'(20i4)') (kr,kcr(kr),k=1,421)
c calcul pour les raies de fluorescence du Fer FeK                          !ta
        if(iexpert.eq.1) WRITE(10,*)'ion     kr  lambda   (FeK)'//
     &        '   hnuev     probafluo     A21   kcr hnucont kex kali'
        jfe=jmax
          ifz2=iz(jfe)-2
        do 21 ife=1,ifz2
          hnuev=efluo(ife)
          kr=nrcum(26,10)+ife
          thnur(kr)=efluo(ife)
          kcr(kr)=156
          alambda=hcse/hnuev                  ! calcule dans lectatom
        B12hnus4pi(kr)=0.d0
        if(ife.ge.17) B12hnus4pi(kr)=fdo(kr)*0.02654d0
        B12hnu2s4pi(kr)=B12hnus4pi(kr)*hca/alambda
        B12(kr)=B12hnus4pi(kr)*alambda*pi4/hca
        A21(kr)=fdo(kr)*0.02654d0*pi*8.d16/(alambda**2)
        A21hnu(kr)=A21(kr)*hca/alambda
        A21hnus4pi(kr)=A21hnu(kr)/pi4
        if(iexpert.eq.1)
     &  WRITE(10,75) nomrai(kr),kr,alo(kr),hnuev,pfluo(ife),
     &              A21(kr),kcr(kr),thnuc(156),kex(kr),kali(kr)
          do 22 jn=1,nelem
            inz0=iz(jn)
          do 22 in=1,inz0
           niv=nivion(in,jn)
          do 22 nn=1,niv
           nij=nivcum(in-1,jn) + nn
                sigmar(kr,nij)=0.d0
                sigmark(kr,nij)=0.d0
                sigma=0.d0
                sigmal=0.d0
                sigmak=0.d0
            akiev=hkin(nij)
            if(hnuev.ge.akiev) then
        CALL sPHOTOION(hnuev,in,jn,nn,nij,sigma,sigmal,sigmak)
            if(jn.lt.3.or.in.gt.(inz0-2).or.hnuev.lt.pkiek(in,jn))
     &      sigmak=0.d0
        sigmar(kr,nij)=sigma+sigmal+sigmak
        sigmark(kr,nij)=sigmak
                endif   ! akiev
   22          continue                                        ! in,jn
   21        continue                                        ! ife
c calcul pour les raies UTA (saut d'un electron de la couche interne)       !ta
        if(iexpert.eq.1) WRITE(10,*)'ion     kr  lambda   (UTA)'//
     &        '   hnuev       f12     probafluo kcr hnucont kex kali'
        do 31 ku=1,nuta
          kr=nraissfek+24 + ku
          alambda=alo(kr)
          hnuev=hcse/alambda
                thnur(kr) = hnuev
c       recherche du kc du continu correspondant a chaque raie
                        do kc=1,322
                        if(kc.le.102) then
                        hnuc=thnuc(kc)
                        d1=0.d0
                        d2=tdeb(kc)
                        else if(kc.le.220) then
                        hnuc=thnuc(kc)
                        d1=tde(kc)-tdeb(kc)
                        d2=tdeb(kc)
                        else
                        hnuc=thnuc(kc-220)
                        d1=tde(kc-220)-tdeb(kc-220)
                        d2=0.d0
                        endif
        if(hnuev.ge.(hnuc-d1).and.hnuev.lt.(hnuc+d2)) then
        kcr(kr)=kc
                        goto 33
                        endif
                        enddo
                        write(10,*) 'pas de kc pour kr=',kr
 33                                             continue
        B12hnus4pi(kr)=fdo(kr)*0.02654d0                                    !ta
        B12hnu2s4pi(kr)=B12hnus4pi(kr)*hca/alambda
        B12(kr)=B12hnus4pi(kr)*alambda*pi4/hca
        A21(kr)=fdo(kr)*0.02654d0*pi*8.d16/(alambda**2)
        A21hnu(kr)=A21(kr)*hca/alambda
        A21hnus4pi(kr)=A21hnu(kr)/pi4
        if(iexpert.eq.1) WRITE(10,75) nomrai(kr),kr,alo(kr),hnuev,
     &            fdo(kr),probuta(ku),kcr(kr),hnuc,kex(kr),kali(kr)
          do 32 jn=1,nelem
            inz0=iz(jn)
          do 32 in=1,inz0
           niv=nivion(in,jn)
          do 32 nn=1,niv
           nij=nivcum(in-1,jn) + nn
                sigmar(kr,nij)=0.d0
                sigmark(kr,nij)=0.d0
                sigma=0.d0
                sigmal=0.d0
                sigmak=0.d0
            akiev=hkin(nij)
            if(hnuev.ge.akiev) then
        CALL sPHOTOION(hnuev,in,jn,nn,nij,sigma,sigmal,sigmak)
            if(jn.lt.3.or.in.gt.(inz0-2).or.hnuev.lt.pkiek(in,jn))
     &      sigmak=0.d0
        sigmar(kr,nij)=sigma+sigmal+sigmak
        sigmark(kr,nij)=sigmak
                endif   ! akiev
 32       continue                                        ! in,jn
 31     continue
c                                                                           !ta
c tri des raies
        if(iexpert.eq.1) then
        n=nrais
        call INDEXTRIr(n,thnur,iw)
        WRITE(10,*)
        if(ila.eq.0) WRITE(10,*) 'RAIES triees :'
        if(ila.eq.1) WRITE(10,*) 'LINES sorted :'
        WRITE(10,*)
     & '    hnuev      ion      lambda      n1 n2     f12         kr'
                          do 50 i=1,n
        kr=iw(i)
        if(thnur(kr).le.0.d0) goto 50
        if(kr.le.nraissfek) then
           WRITE(10,'(1pg13.5,2x,a7,1pg12.5,i5,i3,1pg13.4,i7)')
     &        thnur(kr),nomrai(kr),alo(kr),n1rt(kr),n2rt(kr),fdo(kr),kr
        else if(kr.le.nraissfek+24) then
           txt='FeK'
           WRITE(10,'(1pg13.5,2x,a7,1pg12.5,4x,a3,1x,1pg13.4,i7)')
     &        thnur(kr),nomrai(kr),alo(kr),txt,fdo(kr),kr
        else if(kr.le.nraissfek+24+nuta) then
           txt='UTA'
           WRITE(10,'(1pg13.5,2x,a7,1pg12.5,4x,a3,1x,1pg13.4,i7)')
     &        thnur(kr),nomrai(kr),alo(kr),txt,fdo(kr),kr
        else if(inr.ge.2) then
           txt='sup'
           WRITE(10,'(1pg13.5,2x,a7,1pg12.5,4x,a3,1x,1pg13.4,i7)')
     &        thnur(kr),nomrai(kr),alo(kr),txt,fdo(kr),kr
        endif
 50                      continue
        WRITE(10,*)
                       if(incid.lt.15) then
        WRITE(10,*)'SPECTRE INCIDENT UTILISE - USED INCIDENT SPECTRUM'
        WRITE(10,*) 'if incid=10 or 12 this is spectrum shape'
        WRITE(10,*) '   kc   hnuev  incident flux'
      WRITE(10,'(i5,0pf10.2,1pe13.4)')(kc,thnuc(kc),exflux(kc),kc=1,220)
      WRITE(10,'(i5,0pf10.2,1pe13.4)')(kc,thnuc(kc)*0.999d0,exflux(kc),
     &        kc=221,322)
                       endif
        endif   ! iexpert
c                                                                           !ta
c calcul des sections de photoionisation pour le CONTINU sigmac(kc,nij) et 
c des taux de photoionisation a m=1 par le flux incident exterieur exphot(nij)
c    frequences nu(kc) egales aux ki(ic,jc) + qques valeurs pour les "trous"
c    puis en progression geometrique        au dessus de 3700 eV
        do 41 jn=1,nelem
          inz0=iz(jn)
        do 41 in=1,inz0
           niv=nivion(in,jn)
        do 41 nn=1,niv
           nij=nivcum(in-1,jn) + nn
          som=0.d0
          somk=0.d0
           akiev=hkin(nij)
           uu=gps(nij)/gfond(in+1,jn)
                        do 42 kc=1,184
          hnuev=thnuc(kc)
          de=tde(kc)
          deb=tdeb(kc)
          dea=de-deb
          de1=de
                        if(kc.le.102) then
          ic=ict(kc)
          jc=jct(kc)
          if(nn.eq.1.and.jc.eq.jn.and.ic.eq.in) de1=deb
                        endif
                sigmac(kc,nij)=0.d0
                sigmacv(kc,nij)=0.d0
                sigmack(kc,nij)=0.d0
                sigma=0.d0
                sigmal=0.d0
                sigmak=0.d0
                    if(hnuev+deb.gt.akiev) then                             !ta
            if(hnuev-dea.le.akiev) hnuev=akiev
        CALL sPHOTOION(hnuev,in,jn,nn,nij,sigma,sigmal,sigmak)
            if(jn.lt.3.or.in.gt.(inz0-2).or.(hnuev+deb).le.pkiek(in,jn))
     &      sigmak=0.d0
        sigmac(kc,nij)=sigma+sigmal+sigmak
        sigmacv(kc,nij)=sigma
        som=som+(sigma+sigmal+sigmak)*de1/hnuev *exflux(kc)
c        if(kc.eq.3.and.in.eq.inz0) print*,jn,in,akiev,sigma,sigmal,sigmak
        sigmack(kc,nij)=sigmak
        somk=somk+sigmak*de/hnuev *exflux(kc)
                    endif   ! akiev
c        if(kc.eq.24.and.in.ge.inz0-2) 
c     &  write(71,'(4i3,i4,0pf4.0,0p2f9.2,1p4e10.3)') kc,jn,in,nn,
c     &  nij,uu,hnuev,akiev,sigmac(kc,nij),sigma,sigmal,sigmak
  42                    continue    ! kc
        exphot(nij)=som/6.6262d-27*sqrt(3.d0)*2.d0           !*2 si refl~inc
        exphotk(nij)=somk/6.6262d-27 *sqrt(3.d0)*2.d0
        if(jn.gt.3) goto 41
                       do kc=185,220                            ! 0.01->7.64 ev
          hnuev=thnuc(kc) 
          de=tde(kc) 
          deb=tdeb(kc) 
          dea=de-deb
                sigmac(kc,nij)=0.d0
                sigmacv(kc,nij)=0.d0
                sigmack(kc,nij)=0.d0
                sigma=0.d0
                sigmal=0.d0
                sigmak=0.d0                                                 !ta
          if(hnuev+deb.ge.akiev) then
                if(hnuev-dea.le.akiev) hnuev=akiev
        CALL sPHOTOION(hnuev,in,jn,nn,nij,sigma,sigmal,sigmak)
        sigmac(kc,nij)=sigma+sigmal+sigmak
        sigmacv(kc,nij)=sigma
          endif   ! akiev
                       enddo   ! kc=185 a 220
   41        continue                ! in,jn

c        do kc=103,175
c  hnuev kc sigmacHe1-4 sigmacN5-5 sigmacN6-2 sigmacN7-1 sigSi12-5 sigSi13-6
c        write(10,'(f8.2,i4,1p6e10.3)') thnuc(kc),kc,sigmac(kc,11),sigmac!234567
c     & (kc,62),sigmac(kc,64),sigmac(kc,78),sigmac(kc,208),sigmac(kc,214)
c        enddo

        return
        end                                                             !tabsig
c-------------------------------------------------------------------------------
        subroutine INDEXTRIr(n,arrin,indx)
c
c tri par ordre croissant des energies de photons
c       Subroutine INDEXX de Numerical Recipes p233
c
        implicit double precision (a-h,o-z)
        dimension arrin(n),indx(n)
c
        do 11 j=1,n
           indx(j)=j
 11                        continue
        l=n/2+1
        ir=n
 10               continue
        if(l.gt.1) then
           l=l-1
           indxt=indx(l)
           q=arrin(indxt)
        else
           indxt=indx(ir)
           q=arrin(indxt)
           indx(ir)=indx(1)                                                 !gi
           ir=ir-1
           if(ir.eq.1) then
              indx(1)=indxt
              return
           endif
        endif
        i=l
        j=l+l
 20               if(j.le.ir) then
           if(j.lt.ir) then
              if(arrin(indx(j)).lt.arrin(indx(j+1))) j=j+1
           endif
           if(q.lt.arrin(indx(j))) then
              indx(i)=indx(j)
              i=j
              j=j+j
           else
              j=ir+1
           endif
        goto 20
        endif
        indx(i)=indxt
        goto 10
        end                                                          !indextrir
c-------------------------------------------------------------------------
        subroutine sPHOTOION(hnuev,in,jn,nn,nij,sigma,sigmal,sigmak)

c calcul des sections de photoionisation sigma(nu,nij)
c versions 20 des atomic.dat

c Attention aux sigmak pour hnu < pkiek

       implicit double precision (a-h,o-z)
       parameter (nivtots=330)
c       common/photab/sigma,sigmal,sigmak,hnuev,in,jn,nn,nij
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/lecpho/sigs(nivtots),s(nivtots),av(nivtots),
     &        hki2(nivtots),sigs2(nivtots),s2(nivtots),a2(nivtots),
     &        pol1(5),pol2(5),pol3(5),pol4(5),pol5(5),
     &        pkiel(26,10),sigsl(26,10),sl(26,10),al(26,10),
     &        sigsk(26,10),sk(26,10),ak(26,10),ksig(nivtots),j2b(5)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/cst3/ee4,collg,hlog2
       common/pho/nerrsig
       save /atom/,/lecpho/,/gnv/,/pho/
c sphotoion
        x=hkin(nij)/hnuev
c                                        
        sigma=0.d0
        sigmal=0.d0
        sigmak=0.d0
        if(nn.eq.1.and.x.gt.1.d0) return
        if(nn.gt.1) then
           if(hnuev.le.7.2d0.and.x.gt.1.1d0) return
           if(hnuev.gt.7.2d0.and.x.gt.1.055d0) return
        endif
        inz0=iz(jn)
        if(nij.eq.1.and.hnuev.gt.10.2d0*0.999d0.and.
     &                       hnuev.lt.10.2d0*1.001d0) nerrsig=0
 80     format('ksig=',i2,' bon ??? pour nij=',5i5)

c si ksig=0 : cas courant
        if(ksig(nij).eq.0) then
           sigma=sigs(nij)*x**s(nij)*(1.d0 + av(nij) - av(nij)*x)
c
c si ksig=1 : cas des Hydrogenoides (tous niveaux)
        else if(ksig(nij).eq.1) then
              if(nerrsig.le.100.and.in.ne.inz0) then
                 write(6,80) ksig(nij),nij,jn,in,nn
                 nerrsig=nerrsig+1
              endif
           if(x.ne.1.d0)then
              y=sqrt(x/(1.d0-x))
              sigma=sigs(nij) *x*x*x*x *expa(-4.d0
     &             *y*atan(1.d0/y))*ee4/(1.d0-expa(-6.283185d0*y))
           else
              sigma=sigs(nij)
           endif
c
c si ksig=2 : cas particulier des neutres de C N O Ne
        else if(ksig(nij).eq.2) then
              if(nerrsig.le.100.and.
     &           (jn.lt.3.or.jn.gt.6.or.in.gt.1.or.nn.gt.1)) then
                 write(6,80) ksig(nij),nij,jn,in,nn
                 nerrsig=nerrsig+1
              endif
           sigm1=sigs(nij)*x**s(nij)*(1.d0 + av(nij) - av(nij)*x)
           sigm2=sigs2(nij)*(1000.d0/hnuev)**2.3d0      ! C0,N0,O0,Ne0
           sigma=min(sigm1,sigm2)
c
c si ksig=3 : cas particulier des neutres de Mg Si S Fe et Fe+
        else if(ksig(nij).eq.3) then
              if(nerrsig.le.100.and.(jn.lt.7.or.in.gt.2.or.nn.gt.1))then
                 write(6,80) ksig(nij),nij,jn,in,nn
                 nerrsig=nerrsig+1
              endif
           if(jn.ge.7.and.in.eq.1.and.nn.eq.1) j6=jn-6
           if(jn.eq.10.and.in.eq.2.and.nn.eq.1) j6=5
           sigma=1.d-18*(pol1(j6)+pol2(j6)*x+pol3(j6)*x*x   !Mg0,Si0,S0,Fe0,Fe+
     &                  +pol4(j6)*x*x*x+pol5(j6)*x*x*x*x)
c donne sig=0 vers 10ev pour Fe0,Reilman-Manson ont la des sig tres petits
c
c si ksig=4 :
c les sigmas tires de Topbase presentent 2 "seuils" - le 2e denote des
c ionisations correspondant a des dielectroniques, on les appelle sigmal
c elles sont additionnees aux ionisations sigma
c et on ne les compte pas dans le calcul des recombinaisons radiatives
c
        else if(ksig(nij).eq.4) then
c              if(nerrsig.le.100.and.nn.eq.1) then
c                 write(6,80) ksig(nij),nij,jn,in,nn
c                 nerrsig=nerrsig+1
c              endif
           sigma=sigs(nij)*x**s(nij) *(1.d0 + av(nij) - av(nij)*x)
              if(hnuev.ge.hki2(nij)) then
           sigmal=sigs2(nij)*(hki2(nij)/hnuev)**s2(nij)
     &               *(1.d0 + a2(nij) - a2(nij)*hki2(nij)/hnuev)
              endif   ! hki2
c
c si ksig=5 : cas particulier des niveaux excites du He1 & Fer 25 (heliumoide)
c commode aussi pour O4
c les sigmas tires de Topbase presentent 2 "seuils" - le 2e denote des
c ionisations correspondant a des dielectroniques
c ne pas additionner les 2 composantes (a cause des niveaux 2s1S & 2s3S) :
c          la commande suivante ne marche pas :
c if(hnuev.ge.hki2(nij)) sigma=sigma+sigs2(nij)*(hki2(nij)/hnuev)**3
c
        else if(ksig(nij).eq.5) then
              if(nerrsig.le.100.and.(nn.eq.1.or.in.ne.inz0-1)) then
                 write(6,80) ksig(nij),nij,jn,in,nn
                 nerrsig=nerrsig+1
              endif
           if(hnuev.lt.hki2(nij)) then
              sigma=sigs(nij)*x**s(nij) *(1.d0 + av(nij) - av(nij)*x)
              sigmal=0.d0
           else
              sigma=sigs2(nij)*(hki2(nij)/hnuev)**s2(nij)
     &               *(1.d0 + a2(nij) - a2(nij)*hki2(nij)/hnuev)
              sigmal=0.d0
           endif   ! hki2
c
        else
           write(6,*)' ATTENTION : mauvaise donnee pour ksig a nij=',nij
        endif   ! ksig
c
        if(sigma.lt.0.d0) sigma=0.d0

c calcul de sigmaL et sigmaK
        nij1=nivcum(0,jn)+1
        if(jn.ge.7.and.in.le.j2b(jn-6).and.hnuev.ge.pkiel(in,jn))     !couche L
     &     sigmal=sigmal+ sigsl(in,jn)*(pkiel(in,jn)/hnuev)**sl(in,jn) ! Mg,Si,
     &            *(1.d0 + al(in,jn) - al(in,jn)*pkiel(in,jn)/hnuev)   ! S,Fe
        if(in.le.(inz0-2).and.hnuev*1.05d0.ge.pkiek(in,jn)) then
                hh=hnuev
                if(hnuev*0.95d0.lt.pkiek(in,jn)) hh=pkiek(in,jn)
                sigmak=sigsk(in,jn)*(pkiek(in,jn)/hh)**sk(in,jn)
     &                 *(1.d0 + ak(in,jn) - ak(in,jn)*pkiek(in,jn)/hh)
                endif
c
 100    return
        end                                                         !sphotoion
c--============================================================================
        subroutine DATOM

c calcul des donnees atomiques dependant de la temperature

       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200)
! LC variables in common
       integer dr_itype,rr_itype,iverbose
       logical ldebug
!
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/temp/T,VT,tk,tk1,templog
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/datdeg/alpha(26,10),alfdi(26,10),edi(26,10),cio(nivtots),
     &           ech(26,10),eche(26,10),alfrad(26,10),
     &           teec(220),tgaufrec(220)
       common/stdalf/tekc(220,nivtots)
       common/lecdat/adi(26,10),tdi0(26,10),bdi(26,10),tdi1(26,10),
     &      athe(26,10),pthe(26,10),tthe(26,10),
     &      aech(26,10),bech(26,10),aeche(26,10),beche(26,10),
     &      arrn(nivtots),brrn(nivtots),crrn(nivtots),drrn(nivtots),
     &      nbq(nivtots)
       common/dastpo/alfint(nivtots),cioe(nivtots),
     &          texcoll(nrais,nzs+1),teer(nrais,nzs)
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
! LC COMMON
! /debug/ is for debugging flags used everywhere
! /lecdr/ is for dielectronic recombination (DR) rates read form atomic data
! file (new format) 
! /lecrr/ is for total radiative recombination (RR) rates read form atomic
!         data file (new block @rad-recomb-total)
! COMMON debug includes all flags relative to debug modes (verbose printing,
! etc.)
! ldebug: print some debugging messages
! iverbose: level of verbosity of debugging messages, 1: minimum, 2: details,
! 3: all
! dr_itype(integer): type of fitting algorithm for DR rates (1: usual 
! Pequignot, 2: new Badnell)
! dr_coeff(float): DR coeffs, whatever the algorithm from dr_type.
! dr_coeff(1,i)=a_i, dr_coeff(2,i)=t_i
! rr_coeff(float): total RR coeffs, whatever the algorithm from
! rr_type (equal to -1. when not defined).
       common/debug/ldebug,iverbose
       common/lecdr/dr_coeff(2,9,26,10),dr_itype(26,10)
       common/lecrr/rr_coeff(6,26,10),rr_itype(26,10)
!
       save /datdeg/,/stdalf/,/lecdat/,/dastpo/,/atom/,/gnv/
c datom
        VT=sqrt(T)
        tk=ckev*T                                    ! kT ev   8.6171d-5=k/e'
        tk1=1.d0/tk
        templog=log10(T)
c IONISATION
        te5=T/1.d5
        te4=T/1.d4
        if (te4.gt.100.d0) te4=100.d0                        ! ATTENTION a Te4
c
c hydrogenoides : ionisations par collisions et collisions triples
c approximation hydrogenoide pour les niveaux excites des ions multi-niveaux
        do j=1,nelem
           iz0=iz(j)
        do i=1,iz0
           niv=nivion(i,j)
           if(niv.eq.1) goto 300
           nij0=nivcum(i-1,j)
        if(i.eq.iz0) cioe(nij0+1)=athe(iz0,j)*sqrt(te5)
     &                   /(1.d0+0.1d0/tk1/hkin(nij0+1))*1.d-10
           c0=athe(i,j)*1.d-10
           if(j.eq.1) c0=1.8d-8                                            !da
        do n=2,niv
           nij=nij0+n
           hnbq=dfloat(nbq(nij))          ! nombre quantique principal
           cioe(nij)=c0 *hnbq*hnbq*hnbq*hnbq*sqrt(te5)
     &                     /(1.d0+ 0.1d0/tk1/hkin(nij))
        enddo   ! n                                 ! kT/khin=kTn2/e'/(13.6*z2)
 300    continue
        enddo   ! i
        enddo   ! j
c                                                                           !da
        do 101 j=1,nelem
        iz0=iz(j)
        do 102 i=1,iz0
           niv=nivion(i,j)
           nij0=nivcum(i-1,j)
           salfn=0.d0
        do 103 n=1,niv
           nij=nij0 + n
c    calcul des recombinaisons alpha(i,j)
c recomb.radiatives totales et dielectroniques = formules et donnees Badnell
c pour chaque niveau il est beaucoup plus rapide d'utiliser quand c'est possible
c des coefs (arrn,brrn,crrn,drrn) lus ds atomic.dat, calcules par recombint.f
c par integration des sigmas et fittes par MINUIT ou a l'oeil par Kaleidagraf
c pres de l'ETL:pas de recomb.dielectroniques & radiat.=integration des sigmacv
           if(arrn(nij).lt.1.d-100) then
              alfint(nij)=FALFINT(nij,n,i,j)
           else                       ! si les coef.sont lus dans le atomic.dat
c formule Pequignot 1991 A&A251,680
                tt4=T*1.d-4/float(i)**2
             alfint(nij)=arrn(nij)*tt4**(-brrn(nij))
     &                     /(1.d0 + crrn(nij)*tt4**drrn(nij))
c
              akiev=hkin(nij)                                              !da
              do kc=1,220
                 hnuev=thnuc(kc)
                 deb=tdeb(kc)
              if(hnuev+deb.le.akiev.or.(hnuev-akiev)*tk1.gt.700.d0)then
                    tekc(kc,nij)=0.d0
                 else
                    tekc(kc,nij)=expa((akiev-hnuev)*tk1)
                    dea=tde(kc)-deb
                    if(hnuev-dea.le.akiev) tekc(kc,nij)=1.d0
                 endif   ! hnuev
              enddo   ! kc
           endif   ! calcul de alfint et tekc
           salfn=salfn + alfint(nij)
c
 103    continue   ! n
c
! First algorithm rr_itype = 0 (use RRFIT routine, data are builtin)
        if (rr_itype(i,j).eq.0) then
           CALL RRFIT(iz0,iz0-i+1,T,alfrverner)
           alfrad(i,j)=alfrverner
!/LC
! Second algorithm rr_itype = 1 (use 2-parameter data, equivalent 
! to the RRFIT routine)
        else if (rr_itype(i,j).eq.1) then
           ar = rr_coeff(1,i,j)
           br = rr_coeff(2,i,j)
           alfrad(i,j) = ar*(T/1.d4)**br                                   !da
! Third algorithm rr_itype = 2 (use 6-parameter data, from Badnell (2006))
        else if (rr_itype(i,j).eq.2) then
           A = rr_coeff(1,i,j)
           T0 = rr_coeff(2,i,j)
           B = rr_coeff(3,i,j)
           T1 = rr_coeff(4,i,j)
           C = rr_coeff(5,i,j)
           T2 = rr_coeff(6,i,j)
           if ((C.gt.0.d0).and.(T2.gt.0.d0)) B = B + C*expa(-T2/T)
           alfrad(i,j) = A/sqrt(T/T0)/(1.d0+sqrt(T/T0))**(1.d0-B)/
     &                     (1.d0+sqrt(T/T1))**(1.d0+B)
        endif
!        print*,'alfrad(i,j)=',i,j,alfrad(i,j)
!/LC
! compute dielectronic recombination at low temperature
        drbt=0.d0
        CALL DIEBT(iz0,iz0-i+1,T,drbt)
! compute dielectronic recombination at high temperature (usual)
        alfdi(i,j)=0.d0
        edi(i,j)=0.d0
! Pequignot fitting formulae (2 exponentials = 4 coefficients a,ta,b,tb)
        if (i.ne.iz0) then                                                 !da
           if (dr_itype(i,j).eq.1.and.abs(tdi0(i,j)/t).le.700.d0)then
              alfdi(i,j)=adi(i,j)/t**1.5d0
     &        *expa(-tdi0(i,j)/t)*(1.d0+bdi(i,j)*expa(-tdi1(i,j)/t))
              edi(i,j)=adi(i,j)/t**1.5d0*expa(-tdi0(i,j)/t)*(tdi0(i,j)+
     &      (tdi0(i,j)+tdi1(i,j))*bdi(i,j)*expa(-tdi1(i,j)/t))*8.6171d-5
           endif
! Badnell fitting formulae (9 exponentials = 18 coefficients a1,t1,a2,t2...)
! This is extension of Pequignot, with a1=a,t1=ta,a2=a*b,t2=ta+tb
           if (dr_itype(i,j).eq.2) then
              alfdiel=0.d0
              ediel=0.d0
             do k=1,9
                 xak = dr_coeff(1,k,i,j) 
                 xtk = dr_coeff(2,k,i,j) 
                 if (xak.gt.0.d0) then
                    alfdiel = alfdiel + xak * expa(-xtk/t)
                    ediel = ediel + xak*expa(-xtk/t) *xtk*8.6171d-5
                 endif
              enddo
              alfdi(i,j)=alfdiel / t**1.5d0
              edi(i,j)=ediel / t**1.5d0
           endif
        endif
        alpha(i,j)= alfrad(i,j) + alfdi(i,j) + drbt                        !da
        if(j.eq.1) goto 104
c  calcul des taux d ionisation thermique par electron et par ion cio(nij)
c  pour les autres elements
c  formule de Shull et Van Steenberg 1982 avec donnees du prog. Pequignot 1986
        if(i.ne.iz0) then
        cioe(nij0+1)=athe(i,j)*te5**pthe(i,j) *1.d-10
     1           /(1.d0+ 0.1d0*(te5/tthe(i,j))**(pthe(i,j)+0.5d0))
        cio(nij0+1)=cioe(nij0+1) * expa(-tthe(i,j)/te5)
        endif
c cio(nij)=athe(i,j)*te5**pthe(i,j)*exp(-tthe(i,j)/te5) *1.d-10 ! Pequignot
c  calcul des taux d'echanges de charges avec H et He
        if(j.le.2) goto 104
        if(i.le.4) then
                ech(i,j)=aech(i,j)*te4**bech(i,j)*1.d-10
                eche(i,j)=aeche(i,j)*te4**beche(i,j)*1.d-10
        else                                     ! extrapolation voir Pequignot
                hi=dfloat(i)/4.d0
                ech(i,j)=ech(4,j)*hi
                eche(i,j)=eche(4,j)*hi                                      !da
        endif
 104    continue
         if(salfn.gt.alfrad(i,j).and.salfn.gt.0.d0) then
            rap=alfrad(i,j)/salfn
            do n=1,niv
               nij=nij0 + n
               alfint(nij)=alfint(nij)*rap
            enddo 
         endif
 102  continue   ! i
 101  continue   ! j
c
c mise en tableau des exponentielles et facteurs de gaunt pour le free-free
        do kc=1,220
        hnuev=thnuc(kc)
           if(hnuev*tk1.le.700.d0) then
        teec(kc)=expa(-hnuev*tk1)
           else
           teec(kc)=0.d0
           endif
        tgaufrec(kc)=FREEGAUNT(hnuev)
        enddo                                                               !da
c
c EXCITATION
c
c calcul de la gaussienne du profil des raies : tableaux fx et fgauss(lf)
        aaprof=FPROF(0,1.d-5,x,cx)
        do j=1,nelem
           vth(j)=VT/sqrt(pmat(j)) *1.2845d12               !10**8*V(2kT/Mion)
        enddo
c
c mise en tableau des exponentielles
c calcul de 1/sqrt(T)*facteur de Gaunt integre (sur les vitesses des electrons)
c pour obtenir le coefficient d'excitation des niveaux atomiques par collisions
c
        hcse=12398.54d0                                     ! h*c/e'*1e8
        hcsb=1.43884d8                                      ! h*c/k en angstrom
        do 200 kr=1,nraimax
        alambda=alo(kr)
        texcoll(kr,m)=0.d0                                                  !da
        x=hcsb/alambda/T                                ! x=E/kT 
        teer(kr,m)=expa(-x)
        texcoll(kr,m)=FEXCOLL(x,kr)
 200    continue
c
        return
        end                                                              !datom
c-----------------------------------------------------------------------------
      subroutine DIEBT(iz,nel,T,drbt)

c     faite par Severine Coupe' le 17 juillet 2001
c     calcule les recombinaisons dielectroniques DR totales basse temperature
c     donnees recuperees sur le site de Verner.
c     References : Nussbaumer et Storey 83,86 et 87
c     (iz=iz0=Z,nel=iz0-i+1,drbt=resultat)
c
       implicit double precision (a-h,o-z)
       common/ddr/dr(5,14,14),dr2(10,14,14)
c      real*8 drbt,T,drbt1
c
      drbt=0.d0
      tp=T/10000.d0
      tp1=6.d0

         IF(iz.lt.6.or.iz.eq.9.or.iz.eq.11.or.iz.gt.14) then        ! H He S Fe
            drbt=0.d0
            return
         endif
       
         IF(iz.ne.8.or.(iz.eq.8.and.(nel.ne.5.or.nel.ne.8))) THEN   ! C N Mg Si
                        IF (tp.le.tp1) THEN                         ! O4 & 1
         drbt=1.d-12*(dr(1,iz,nel)/tp+dr(2,iz,nel)+dr(3,iz,nel)*tp
     &       +dr(4,iz,nel)*tp*tp)*tp**(-1.5d0)*expa(-dr(5,iz,nel)/tp)
                        ELSE
         drbt=1.d-12*(dr(1,iz,nel)/tp1+dr(2,iz,nel)+dr(3,iz,nel)*tp1
     &   +dr(4,iz,nel)*tp1*tp1)*tp1**(-1.5d0)*expa(-dr(5,iz,nel)/tp1)
     &   *(tp1/tp)**1.5d0
                        ENDIF   ! tp, pas OI ni OIV
         ENDIF         ! pas OI ni OIV
         
        IF(iz.eq.8.and.(nel.eq.5.or.nel.eq.8)) THEN
                        IF(tp.lt.2.d0) 
     &  drbt=1.d-12*(dr2(1,iz,nel)/tp+dr2(2,iz,nel)+dr2(3,iz,nel)*tp+
     &       dr2(4,iz,nel)*tp*tp)*tp**(-1.5d0)*expa(-dr2(5,iz,nel)/tp)
                        IF(tp.ge.2.d0.and.tp.le.tp1) 
     &  drbt=1.d-12*(dr2(6,iz,nel)/tp+dr2(7,iz,nel)+dr2(8,iz,nel)*tp+
     &       dr2(9,iz,nel)*tp*tp)*tp**(-1.5d0)*expa(-dr2(10,iz,nel)/tp) 
                        IF(tp.gt.tp1) 
     &  drbt=1.d-12*(dr2(6,iz,nel)/tp1+dr2(7,iz,nel)+dr2(8,iz,nel)
     &  *tp1+ dr2(9,iz,nel)*tp1*tp1)*tp1**(-1.5d0)
     &  *expa(-dr2(10,iz,nel)/tp1)*(tp1/tp)**1.5d0

       endif
      return
      END                                                              ! diebt
c-------------------------------------------------------------------------
       subroutine RRFIT(iz,in,t,r)

c recombinaisons radiatives totales
c implante le 25 avril 2001 drivee par Severine

*** Version 4. June 29, 1999.
*** Written by D. A. Verner, verner@pa.uky.edu
!*********************************************
*** This subroutine calculates rates of radiative recombination for all ions
*** of all elements from H through Zn by use of the following fits:
*** H-like, He-like, Li-like, Na-like - Verner & Ferland, 1996, ApJS, 103, 467
*** Other ions of C, N, O, Ne - Pequignot et al. 1991, A&A, 251, 680,
***    refitted by Verner & Ferland formula to ensure correct asymptotes
*** Fe XVII-XXIII - Arnaud & Raymond, 1992, ApJ, 398, 394
*** Fe I-XV - refitted by Verner & Ferland formula to ensure correct asymptotes
*** Other ions of Mg, Si, S, Ar, Ca, Fe, Ni -
***                      - Shull & Van Steenberg, 1982, ApJS, 48, 95
*** Other ions of Na, Al - Landini & Monsignori Fossi, 1990, A&AS, 82, 229
*** Other ions of F, P, Cl, K, Ti, Cr, Mn, Co (excluding Ti I-II, Cr I-IV,
*** Mn I-V, Co I)        - Landini & Monsignori Fossi, 1991, A&AS, 91, 183
*** All other species    - interpolations of the power-law fits
*** Input parameters:  iz - atomic number
***                    in - number of electrons from 1 to iz
***                    t  - temperature, K
*** Output parameter:  r  - rate coefficient, cm^3 s^(-1)
**********************************************
       implicit double precision (a-h,o-z)
       common/drrec/rrec(2,30,30),rnew(4,30,30),fe(3,13)

       r=0.d0
c      if(iz.lt.1.or.iz.gt.30)then
c      write(6,'('' rrfit called with insane atomic number, ='',i4)')iz
c         stop
c      endif
c      if(in.lt.1.or.in.gt.iz)then
c         write(6,'('' rrfit called with insane number elec ='',i4)')in
c        stop
c      endif
      if(in.le.3.or.in.eq.11.or.(iz.gt.5.and.iz.lt.9).or.iz.eq.10.or.
     &(iz.eq.26.and.in.gt.11))then
         tt=sqrt(t/rnew(3,iz,in))
         r=rnew(1,iz,in)/(tt*(tt+1.d0)**(1.d0-rnew(2,iz,in))*
     &        (1.d0+sqrt(t/rnew(4,iz,in)))**(1.d0+rnew(2,iz,in)))
      else
         tt=t*1.0d-04
         if(iz.eq.26.and.in.le.13)then
            r=fe(1,in)/tt**(fe(2,in)+fe(3,in)*log10(tt))
         else
            r=rrec(1,iz,in)/tt**rrec(2,iz,in)
         endif
      endif
      return
      end                                                                !rrfit
c-------------------------------------------------------------------------------
       function FALFINT(nij,nn,in,jn)

c   pour le cas ou arrn(nij) est nul
c   calcul des recombinaisons a partir des coefficients d'ionisation sigma-val.
c       alpha=(X/X+*Ne)saha*somme(sigma(nu)*B(nu)*4pidnu/hnu)
c   frequences nu egales aux ki(i,j)

       implicit double precision (a-h,o-z)
       parameter (nivtots=330,nrais=4200)
       common/temp/T,VT,tk,tk1,templog
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/talfdeg/sigmac(220,nivtots),sigmacv(220,nivtots),
     &         sigmack(220,nivtots),sigmark(nrais,nivtots)
       common/talfdeg2/exphot(nivtots),exphotk(nivtots)
       common/stdalf/tekc(220,nivtots)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       save /atom/,/talfdeg/,/stdalf/,/gnv/
c falfint
c        tk=T*8.6171d-5                                        ! kT en ev
c        tk1=1.d0/tk
c        VT=sqrt(T)
c        zz=dfloat(inz0)
        inz0=iz(jn)
        akiev=hkin(nij)
c
        do 5 kc=1,220
           hnuev=thnuc(kc)
           deb=tdeb(kc)
        if(hnuev+deb.le.akiev.or.(hnuev-akiev)*tk1.gt.700.d0) then
           tekc(kc,nij)=0.d0
        else
        tekc(kc,nij)=expa((akiev-hnuev)*tk1)
           dea=tde(kc)-deb
           if(hnuev-dea.le.akiev) tekc(kc,nij)=1.d0
        endif   ! hnuev/akiev
   5        continue                                                        !fa
c
        uu=gps(nij)/gfond(in+1,jn)
        q=1.3d0
           dq=2.d0*(q-1.d0)/(q+1.d0)
c on pose x=(hnuev-akiev)/kT et x varie en progression geometrique
        dx=0.3d0*(1.d0-dq/2.d0)
        hnuev=akiev + tk*dx/2.d0
            CALL sPHOTOION(hnuev,in,jn,nn,nij,sigma,sigmal,sigmak)
            salf=sigma*hnuev*hnuev*expa(-dx/2.d0)*dx
        x=0.3d0/q
        do k=1,17                                           ! va jusque x=20
           x=x*q
           dx=x*dq
           hnuev=akiev + tk*x
           CALL sPHOTOION(hnuev,in,jn,nn,nij,sigma,sigmal,sigmak)
            salf=salf+sigma*hnuev*hnuev*expa(-x)*dx
        enddo   ! k
        falfint=salf*uu/VT *7.054146d3              ! 4pie'2/c2/(2pime)3/2/Vk
c si on integre en hnuev et pas x:     /T et *8.18625d7=4pie'3/c2/(2pimek)3/2

        return
        end                                                            !falfint
c-------------------------------------------------------------------------
        function FEXCOLL(x,kr)

c calcul des coefficients d'excitation-deexcitation des raies par collisions

c le coefficient de deexcitation des niveaux atomiques par collisions est
c C21=g1/g2 *fexcoll
c et fexcoll=gaunt/VT *17e-4/hcse *fdo*alambda = gaunt/VT *1.3711e-7*fdo*alambda
c          (gaunt= facteur de Gaunt integre sur les vitesses des electrons)
c ou fexcoll=upsilon/VT *8.63e-6/g1           upsilon=omega integres
c
c si kex=1 ou 2 : quand les donnees publiees (Daresbury report par ex) sont les
c     forces de collision effectives upsilon, elles ont ete entrees dans le
c     fichier de donnees transformees par:
c            gaunt=62.806 * upsilon/(g1*fdo*lambda)
c si kex=6 : on utilise les donnees de Zhang & Sampson 1987,ApJ Sup.Ser 63,487
c     fait le 8 octobre 2001 par sev.        Tups = T/Z**3
c
       implicit double precision (a-h,o-z)
       parameter (nrais=4200)
         parameter (nups=10)
         dimension Tups(nups),tupslog(nups)
       common/temp/T,VT,tk,tk1,templog
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/lexc/aexc(nrais),bexc(nrais),kex(nrais)
       common/raies4/gups(nups,nrais)
       common/cst3/ee4,collg,hlog2
          save /lexc/,/raies4/
       data Tups/0.1d0,4.d2,6.d2,9.d2,1.35d3,2.d3,3.d3,4.5d3,6.7d3,1.d4/
c
c        VT=sqrt(T)
c        templog=log10(T)
c        collg=1.3711d-7                       ! 17.e-4/hcse(=12398.54)
        fexcoll=0.d0
        goto(10,20,30,40,50,60,70,90) kex(kr)                               !fe
c
c kex=1 : les donnees proviennent des "recommended data"" de Daresbury 1985
c   ou d'interpolations le long des sequences isoelectroniques
c   ajustees par AM sous la forme aexc + bexc*T
 10     gaunt=(aexc(kr) + bexc(kr)*T/1.d6)
        if(gaunt.lt.0.d0) gaunt=0.d0
        fexcoll=gaunt/VT *collg *fdo(kr)*alo(kr)
        goto 100
c
c kex=2 : les donnees proviennent des "recommended data"" de Daresbury 1985
c   ou d'interpolations le long des sequences isoelectroniques
c   ou encore des formules de Mewe (A&A20,215,1972) pour les sequences He,Li,Ne
c   ajustees par AM sous la forme aexc + bexc*(Ln((x+1)/x)-0.4/(x+1)^2)
c   avec x=E/kt=1.43884e8/lambda/T
 20     gaunt=(aexc(kr) + bexc(kr)*(log((x+1.d0)/x)-0.4d0/(x+1.d0)
     1            /(x+1.d0)))
        fexcoll=gaunt/VT *collg *fdo(kr)*alo(kr)
        goto 100
c
c kex=3 : pour les raies d'IONS pour lesquelles n'existent pas d'autres donnees
c   formule generale de Mewe (1972) avec x=E/kt=1.43884e8/lambda/T
c   mettre en general  aexc=0 et bexc=0  sauf si d(nbquantique)=0 : aexc=0.45
 30     gaunt=(aexc(kr)+0.15d0 + (0.28d0+x*bexc(kr))
     1            *(log((x+1.d0)/x)-0.4d0/(x+1.d0)/(x+1.d0)))
        fexcoll=gaunt/VT *collg *fdo(kr)*alo(kr)
        goto 100
c
c kex=4 : pour les raies d'intercombinaison (changement de spin) pour 
c   lesquelles n'existent pas d'autres donnees. bexc est ici le rapport de la 
c   force d'oscillateur de la raie permise la plus proche a celle de la raie
c   aexc=0.1 d'apres Kato 1976
c   formule generale de Mewe (1972) avec x=E/kt=1.43884e8/lambda/T
 40     gaunt=bexc(kr)*(x*aexc(kr) - x*x*aexc(kr)
     1            *(log((x+1.d0)/x)-0.4d0/(x+1.d0)/(x+1.d0)))
        fexcoll=gaunt/VT *collg *fdo(kr)*alo(kr)
        goto 100
c
c kex=5 : pour les raies de NEUTRES pour lesquelles n'existent pas de donnees:
c approximation tabulee par Allen page 43 (1964) avec x=E/kt=1.43884e8/lambda/T
 50     continue
c        if(i.gt.1) go to 10
        if(x.lt.0.1d0) then                                                 !fe
                gaunt=0.29d0*e1(x)
        else if(x.lt.10.d0) then
                gaunt=0.103d0/x**0.7d0
        else
                gaunt=0.066d0/x**0.5d0
        endif
        fexcoll=gaunt/VT *collg *fdo(kr)*alo(kr)
        goto 100
c
c kex=6 coeff. de coll effectives upsilon en fontion de T pour les He-like
c   pour les temperatures T/Z**3=0.1,400,600,900,1350,2000,3000,4500,6700,1e4
c   article Zhang & sampson 1987, ApJ Sup. Ser 63,487     Tups est en T/Z**3
c   ups depend de 4 parametres : nb. de pts de T(ii), iz0, niv1, niv2
c   fait le 8 octobre 2001 par Severine Coupe
c   dans LECTATOM on a deja calcule :
c        gups(ii,kr)=ups/g1rt(kr) *8.63d-6     ii=1,10

 60         continue
                         do j=1,jmax
                            iz0=iz(j)
                            do i=1,iz0
                               if(kr.lt.nrcum(i,j)) then
                                  iz3=iz0*iz0*iz0
                                  goto 61
                               endif
                            enddo
                         enddo
 61    continue
c                      templog = log10(T)
       do ii = 1,nups
         tupslog(ii) = log10(Tups(ii)*iz3)
       enddo

       do ii = 1,nups-1
          if(templog.ge.tupslog(ii).and.templog.lt.tupslog(ii+1)) then
             fexcoll = ((gups(ii+1,kr)-gups(ii,kr))
     &          *(templog-tupslog(ii))/(tupslog(ii+1)-tupslog(ii))
     &          + gups(ii,kr)) /sqrt(T)
             goto 100
          endif
       enddo   ! ii

            if(templog.lt.tupslog(1)) then            ! extrapolation T < T1
               fexcoll = ((gups(3,kr)-gups(1,kr))
     &              *(templog-tupslog(1))/(tupslog(3)-tupslog(1))
     &              + gups(1,kr)) /sqrt(T)
            else if(templog.gt.tupslog(nups)) then    ! T > T10
               fexcoll =  gups(nups,kr) /sqrt(T)
            endif   ! T <T(1) ou >T(10)

       if(fexcoll.lt.0.d0) fexcoll=0.d0                      ! cela arrive !
        goto 100
c
c kex=7 coeff. de coll effectives upsilon en fontion de T, tires de Tipbase
c   pour les temperatures T=3e3,1e4,3e4,1e5,3e5,1e6,3e6,1e7,3e7,1e8
c   http://cdsweb.u-strasbg.fr/tipbase/home.html  (extrapolation si besoin)
c   dans LECTATOM on a deja calcule :                            sept 2007
c        gups(it,kr)=ups/g1rt(kr) *8.63d-6     it=1,10
c
 70         continue
c                          templog = log10(T)
          tupslog(1)=3.5d0
       do it=1,nups-1
          tupslog(it+1)=tupslog(it)+0.5d0
          if(templog.ge.tupslog(it).and.templog.lt.tupslog(it+1)) then
             fexcoll = ((gups(it+1,kr)-gups(it,kr))
     &          *(templog-tupslog(it))/(tupslog(it+1)-tupslog(it))
     &          + gups(it,kr)) /sqrt(T)
             goto 100
          endif
       enddo   ! it
c
            if(templog.lt.tupslog(1)) then            ! extrapolation T < T1
               fexcoll = ((gups(3,kr)-gups(1,kr))
     &              *(templog-tupslog(1))/(tupslog(3)-tupslog(1))
     &              + gups(1,kr)) /sqrt(T)
            else if(templog.gt.tupslog(nups)) then    ! T > T10
               fexcoll =  gups(nups,kr) /sqrt(T)
            endif   ! T <T(1) ou >T(10)
c
       if(fexcoll.lt.0.d0) fexcoll=0.d0                      ! cela arrive !
        goto 100
c
c pb de kexc
 90     write(6,91) kex(kr),kr,T
 91     format('PB ds les excitations par collisions kex=',i2,
     &  ' pour kr=',i4,' a T=',1pe10.3)
c
 100        continue
        return
        end                                                          !fexcoll
c-------------------------------------------------------------------------
        function FREEGAUNT(hnuev)

c calcul des coefficients de Gaunt pour le free-free DIVISES PAR sqrt(T)

c au lieu de       function GFFREE(gam2, T, u)   car ON NE CONSIDERE QUE Z=1
c Free free gaunt factors venant de Jean-Louis Masnou 27-JUN-1991
c FERLAND'S FABULOUS FUNCTIONAL FITS to Karzas & Latter Ap. J. 1966
*     gam2=13.6*Z*Z/kT=1.58e5*Z*Z/T  - Z is charge on ion
*     T    Temperature ( K)
*     u=h*nu/(k*T)  (Planck*freq/(boltzman*temperature))
c
        implicit double precision (a-h,o-z)
        dimension coeff(28), a(7)
        common/temp/T,VT,tk,tk1,templog
c                                                                           !fr
       DATA coeff
     + /1.102d0       ,-0.1085d0     ,0.09775d0     ,-0.01125d0     ,
     +  1.2d0         ,-0.24016667d0 ,0.07675d0     ,-0.01658333d0  ,
     +  1.26d0        ,-0.313166667d0,0.15075d0     ,0.00241667d0   ,
     +  1.29d0        ,-0.4518333d0  ,0.12925d0     ,0.00258333d0   ,
     +  1.27d0        ,-0.579d0      ,0.092d0       ,-0.003d0       ,
     +  1.16d0        ,-0.707333d0   ,0.112d0       ,0.0053333333d0 ,
     +  0.883d0       ,-0.76885d0    ,0.190175d0    ,0.022675d0     /
       DATA a
     + /100.d0, 10.d0, 3.d0, 1.d0, 0.3d0, 0.1d0, 0.001d0/
c
c        tk=T*8.6171d-5                                        ! kT en ev
        u=hnuev/tk
        ulog = log10(u)                                               
c        gam2=13.6d0*Z*Z/tk
        gam2=13.6d0/tk                                        ! Z=1
c
       if (gam2.gt.a(7)) go to 10
         i = 7
         j = 7
         k = 7
         frac = 0.5d0
         go to 60
   10  continue
       if (gam2.lt.a(1)) go to 20
         i = 1
         j = 1
         k = 1
         frac = 0.5d0
         go to 60                                                           !fr
   20  continue
       do 30 i = 2, 7
           if (gam2.gt.a(i)) go to 40
   30  continue
   40  continue
       k = i - 1
   50  continue
       b = log10(a(k))
       c = log10(a(i))
       gam2 = log10(gam2)
       frac = abs ((gam2-b) / (b-c))
   60  continue
       k = (k-1)*4
       sum1 = coeff(k+1)
       d = 1.0d0
       do 70 m = 2, 4                                                       !fr
           d = d*ulog
           sum1 = sum1 + coeff(k+m)*d
   70  continue
       sum1 = sum1 * (1.0d0 - frac)
       i = (i-1)*4
       sum2 = coeff(i+1)
       d = 1.0d0
       do 80 m = 2, 4
           d = d*ulog
           sum2 = sum2 + coeff(i+m)*d
   80  continue
       sum2 = sum2 * frac
c
        FREEGAUNT = (sum1 + sum2)/sqrt(T)
        return
        end                                                                 !fr
c--============================================================================
        subroutine sPoP0(i,j)
c
c calcul des degres d ionisation des ions a 1 niveau + continu, niv=1
 
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15)
         double precision ne,nht
         character txt*10,tab*1
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/temp/T,VT,tk,tk1,templog
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)    !po0
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/raies3/A21(nrais),A21hnu(nrais),A21hnus4pi(nrais),
     &               B12(nrais),B12hnus4pi(nrais),B12hnu2s4pi(nrais)
       common/raiesuta/probuta(100),dnuta2(100),utanion(26,10),
     &                 juta(100),iuta(100)
       common/datdeg/alpha(26,10),alfdi(26,10),edi(26,10),cio(nivtots),
     &          ech(26,10),eche(26,10),alfrad(26,10),
     &          teec(220),tgaufrec(220)
       common/dastpo/alfint(nivtots),cioe(nivtots),
     &          texcoll(nrais,nzs+1),teer(nrais,nzs)
       common/stpo/tphot(nivtots),tphotk(26,10),dens(nivtots),
     &          alfinduit(nivtots),zfree,cthomson
       common/deglosm/pnenht,praieint,r(27,10)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/mstpo/rtaur0(nrais),rtaurl(nrais),
     &           twr(nrais),tws(nrais),uhz(nrais),cjn(nrais)
       common/pop/po1(nivmax+1,26),sompo1(26),f(27,10),
     &             ph0ne,phpne,phe0ne,phepne,sech1,sech2,seche1,seche2
          save /atom/,/raies3/,/datdeg/,/deglosm/,/dastpo/,/stpo/,
     &         /mstpo/,/gnv/,/pop/
c spop0

c        pi4=12.5663706144d0
c        hcse=12398.54d0                                     ! h*c/e'*1e8
c        hcsb=1.43884d8                                      ! h*c/k en angstrom
c        nraissfek=nrcum(26,10)
        saha=2.07079d-16/T/VT              ! saha=1/constante de la loi de saha
c        tk1=1.d0/8.6171d-5/T                                ! k/e'

                ecr=0.d0
                eci=0.d0
                doublion=0.d0                                              !po0
                f(i+1,j)=0.d0
        iz0=iz(j)
        nij1=nivcum(i-1,j)+1               ! numero du fondamental de l'ion i,j
        uu=gfond(i,j)/gfond(i+1,j)
        alfa=alpha(i,j)
        if(ietl.eq.1) alfa=alfint(nij1)
        if(j.eq.2) then
           eci=seche1
           ecr=seche2
        endif   ! j=2
        if(j.ge.3) then
           ecr=ech(i,j)*ph0ne + eche(i,j)*phe0ne
                yy=tk1*(pkiev(i,j)-13.6d0)
                if(j.eq.5.and.i.eq.1.and.yy.lt.20.d0)      ! O1
     &     eci=ech(i,j)*2.d0*expa(-yy)/uu*phpne
                if(j.eq.8.and.i.eq.2.and.yy.lt.20.d0)      ! Si2
     &     eci=ech(i,j)*2.d0*expa(-yy)/uu*phpne
                if(j.eq.8.and.i.eq.3.and.yy.lt.20.d0)      ! Si3
     &     eci=eche(i,j)/2.d0*expa(tk1*(24.58d0-pkiev(i,j)))/uu*phepne
c l'effet Auger Fe24+photonK ne donne pas un Fe26 - Band1990 - (question de DE)
        if(i.ne.1.and.i.le.(iz0-2)) then
           doublion=f(i-1,j)*tphotk(i-1,j)/ne
            if(j.eq.10) then
            doublion=(1.d0-pfluo(i-1))*doublion                        ! Fe2-24
            if(i.ge.18) doublion=doublion + f(i,j)*cjn(nraissfek+i-1)  ! 18-24
     &             *B12hnus4pi(nraissfek+i-1)*(1.d0-pfluo(i-1))/ne
            if(doublion.lt.0.d0.and.nitd.eq.1) doublion=0.d0
            endif   ! j=10
        endif   ! i
        endif   ! j>3
c autoionisation apres absorption d'un photon UTA
        eci=eci + utanion(i,j)/ne
c
c                                       ! si j>2  f(i,j)=N(i,j)/N(1,j) *1d-150
        f(i+1,j)=(f(i,j)*(cio(nij1)+tphot(nij1)/ne+eci) + doublion)
     &      /(alfa + alfinduit(nij1) + cioe(nij1)*uu*saha*ne + ecr)
c
        if(f(i,j).gt.0.d0.and.f(i+1,j).lt.0.d0.and.kitt.eq.1
     &                 .and.kitn.eq.1.and.kitd.eq.1) then
           txt='f & termes'
        write(6,'(i4,3i3,1p2e11.3,a10)') m,nitd,j,i,f(i,j),f(i+1,j),txt
        write(6,'(8(1pe9.2))') cio(nij1),tphot(nij1)/ne,eci,
     &  doublion/f(i,j),alfa,alfinduit(nij1),cioe(nij1)*uu*saha*ne,ecr
        endif
c        if(m.eq.1.and.nitt.eq.1.and.j.eq.3.and.i.eq.1)write(75,*)m,nitd !234567
c       if(m.eq.10.and.kitf.eq.1.and.kitt.eq.1.and.kitd.eq.1.and.j.eq.10)
c     &  write(75,'(i2,i3,11(1pe9.2))') j,i,f(i,j),f(i+1,j),cio(nij1),
c     &  tphot(nij1)/ne,eci,utanion(i,j)/ne,
c     &  doublion/f(i,j),alfa,alfinduit(nij1),cioe(nij1)*uu*saha*ne,ecr

        po1(1,i)=1.d0
        niv=nivion(i,j)
           po1(niv+1,i)=0.d0
        if(f(i,j).ne.0.d0) po1(niv+1,i)=f(i+1,j)/f(i,j)
        sompo1(i)=1.d0
        return
        end                                                             ! spop0
c------------------------------------------------------------------------------
        subroutine sPoP1(i,j)
c PAS UTILISE
c
c calcul des degres d ionisation des ions a niv niveaux + ion, niv>1
c nivmax=15 niveaux maximum 
c inconnues : xn=Nn/Nion, n=1:fond.
c resolution de A*X=B ou :        a(1,1)*x1 + a(1,2)*x2 + a(1,3)*x3 +...= b1
c                                a(2,1)*x1 + a(2,2)*x2 + a(2,3)*x3 +...= b2 ...
c apres les RESEQ les resultats sont dans B
c
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15)
         double precision ne,nht
         dimension a(nivmax,nivmax),b(nivmax),indx(nivmax),
     &             ai(nivmax,nivmax),bi(nivmax)
         dimension gas(nrais),gbs(nrais)
         character tab*1
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/abond/ab(10)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/temp/T,VT,tk,tk1,templog
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/raies2/g1rt(nrais),g2rt(nrais),n1rt(nrais),n2rt(nrais)
       common/raies3/A21(nrais),A21hnu(nrais),A21hnus4pi(nrais),
     &               B12(nrais),B12hnus4pi(nrais),B12hnu2s4pi(nrais)
       common/deglosm/pnenht,praieint,r(27,10)
       common/tabstpo/sigmar(nrais,nivtots)
       common/datdeg/alpha(26,10),alfdi(26,10),edi(26,10),cio(nivtots),
     &          ech(26,10),eche(26,10),alfrad(26,10),
     &          teec(220),tgaufrec(220)
       common/dastpo/alfint(nivtots),cioe(nivtots),
     &          texcoll(nrais,nzs+1),teer(nrais,nzs)
       common/mstpo/rtaur0(nrais),rtaurl(nrais),
     &           twr(nrais),tws(nrais),uhz(nrais),cjn(nrais)
       common/stpo/tphot(nivtots),tphotk(26,10),dens(nivtots),
     &          alfinduit(nivtots),zfree,cthomson
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/pop/po1(nivmax+1,26),sompo1(26),f(27,10),
     &             ph0ne,phpne,phe0ne,phepne,sech1,sech2,seche1,seche2
       common/rpal/twnu(16,nrais,nzs),tphi(16,nrais,nzs),
     &        tcapr(nrais,nzs),dcapr(nrais,nzs),dsl(nrais,nzs),
     &        tas(nrais,nzs),tbs(nrais,nzs),tne(nzs+1),tdensf(nzs,26,10)
       common/rpal2/kali(nrais),npbali(nrais),lcompt(nzs,nrais)
       common/sporpal/tjr(nrais,nzs),tcjn(nrais,nzs),pradr(nrais,nzs)
       common/pop1m/tapo(nivtots,nzs),tbpo(nivtots,nzs)
       save /atom/,/deglosm/,/tabstpo/,/datdeg/,/dastpo/,/mstpo/,/stpo/,
     &    /raies2/,/raies3/,/gnv/,/pop/,/pop1m/,/rpal/,/rpal2/,/sporpal/
c spop1
c
c        hcse=12398.54d0                                     ! h*c/e'*1e8
c        hcsb=1.43884d8                                      ! h*c/k en angstrom
           k4=nrcum(1,2)+1       ! He2-1
           k9=nrcum(7,5)+1       ! O8-1
        niv=nivion(i,j)
        if(niv.eq.1) then
           write(6,*) 'PB:niv=1 dans sPoP1 pour j,i=',j,i
           return
        endif
        ndim=nivmax
        saha=2.07079d-16/T/VT              ! saha=1/constante de la loi de saha
c        tk1=1.d0/8.6171d-5/T                                ! k/e'
c        write(10,*) ' dans sPoP1 j,i=',j,i
c
        iz0=iz(j)
        nij0=nivcum(i-1,j)
        kri=nrcum(i-1,j)+1
        krf=nrcum(i,j)
        do n1=1,niv
           b(n1)=0.d0
           bi(n1)=0.d0
           do n2=1,niv
              a(n1,n2)=0.d0
              ai(n1,n2)=0.d0
           enddo
        enddo
           ecr=0.d0
           eci=0.d0
           doublion=0.d0
c elements non diagonaux
        do 10 kr=kri,krf
           n1=n1rt(kr)
           n2=n2rt(kr)
           gn1=gps(nij0+n1)
           gn2=gps(nij0+n2)
           alambda=alo(kr)
           hnuev=hcse/alambda
c cjn est 4piJ/hnu
                ee=teer(kr,m)                  ! (0.667d16)
                aa=A21(kr) /ne
                bb=B12hnus4pi(kr)*cjn(kr) /ne                        ! B12J/Ne
                cc=texcoll(kr,m)                                     ! C21g2/g1
        a(n2,n1)=bb + cc*ee                          ! En1n2=B12J/Ne+C12
        a(n1,n2)=aa + gn1/gn2*(bb + cc)              ! Dn2n1=A21/Ne+B21J/Ne+C21
        ai(n2,n1)=a(n2,n1)
        ai(n1,n2)=a(n1,n2)
           gas(kr)=aa/(aa + bb + cc)
c
      if(imp.ge.6.and.imp.le.7.and.kitt.eq.1.and.kitn.eq.1.and.kitd.eq.1
     &   .and.kimpm.eq.1.and.kimpf2.eq.1.and.ifich.ge.4.and.ifich.le.8
     &   .and.(kr.eq.k4.or.kr.eq.k9)) then
        WRITE(21,*) ' kr     aa         bb',
     &        '          cc          ee       am'
c        WRITE(21,*) ' kr   gngn         aa          bb',
c     &        '          cc          ee       am'
        am=gn1/gn2*fdo(kr)/alambda*413.23d0/VT                             !po1
c        gngn=0.d0
c        if(po1(n1,i).gt.0.d0) gngn=gn1*po1(n2,i)/gn2/po1(n1,i)
c        WRITE(21,'(i4,6(1pe12.5))') kr,gngn,aa,bb,cc,ee,am
        WRITE(21,'(i4,6(1pe12.5))') kr,aa,bb,cc,ee,am
        endif   ! imp
c
   10                continue
c
c elements diagonaux et second membre
                   salfn=0.d0
                do n=1,niv
                nij=nij0+n
                gn=gps(nij)
                   som=0.d0
                   do ii=1,niv
                      if(ii.ne.n) som=som + a(ii,n)
                   enddo   ! ii
        a(n,n)= -(cioe(nij)*expa(-hkin(nij)*tk1) + tphot(nij)/ne + som)  ! In
        b(n)=-(alfint(nij)+alfinduit(nij) + ne*cioe(nij)*gn*saha)        ! Rn
        ai(n,n)=a(n,n)
        bi(n)=b(n)
                if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1)
     &  tapo(nij,m)=cioe(nij)*expa(-hkin(nij)*tk1) + tphot(nij)/ne
                salfn=salfn + alfint(nij)
c      if(n.eq.2)write(10,777)a(n,n),cioe(nij0+2),cioe(nij0+2)*expa
c     &        (-pkiev(nij)*tk1),tphot(nij)/ne,som
                enddo   ! n
c
c irecsup=1 : les recombinaisons (y compris dielectroniques) sur les niveaux
c excites superieurs sont attribuees a n=niv (et niv1)
        if(irecsup.eq.1.and.ietl.eq.0) then
           alf=alpha(i,j)-salfn
              if(alf.lt.0.d0) alf=0.d0
              nivsup=niv1(i,j)
           if(nivsup.eq.0) then
              b(niv)=b(niv) - alf
              bi(niv)=b(niv)
           else                        ! 2 systemes de niveaux
              if(i.eq.iz0-1) fr=1.d0/4.d0                ! heliumoides
              if(i.eq.iz0-3) fr=1.d0/4.d0                ! Be
              if(i.eq.iz0-4) fr=1.d0/3.d0                ! B
              b(nivsup)=b(nivsup) - alf*fr
              bi(nivsup)=b(nivsup)
              b(niv)=b(niv) - alf*(1.d0-fr)
              bi(niv)=b(niv)
           endif
        endif
c irecsup=2 : les recombinaisons (y compris dielectroniques) sur les niveaux
c excites superieurs sont distribuees sur tous les niveaux
        if(irecsup.eq.2.and.ietl.eq.0) then
           rap=alpha(i,j)/salfn -1.d0
           if(rap.lt.0.d0) rap=0.d0
                do n=1,niv
                nij=nij0+n
           b(n)=b(n) - alfint(nij)*rap
           bi(n)=b(n)
                enddo   ! n
        endif
c effet Auger
c     l'effet Auger Fe24+photonK ne donne pas un Fe26 mais 25 - Band1990 -
c     (question de DeltaE)
               doublion=0.d0                                               !po1
        if(ietl.eq.0.and.j.ge.3.and.i.ne.1.and.i.le.(iz0-2)) then
           doublion=f(i-1,j)/f(i,j)*tphotk(i-1,j)/ne
           if(j.eq.10) then
              doublion=(1.d0-pfluo(i-1))*doublion                     ! Fe2-24
              if(i.ge.18) doublion=doublion + f(i,j)*cjn(nraissfek+i-1)  !18-24
     &             *B12hnus4pi(nraissfek+i-1)*(1.d0-pfluo(i-1))/ne
            if(doublion.lt.0.d0.and.nitd.eq.1) doublion=0.d0
           endif   ! j=10
           a(1,1)=a(1,1) - doublion
           ai(1,1)=a(1,1)
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
              nij=nij0+1
              tapo(nij,m)=tapo(nij,m)+doublion
           endif
        endif   ! ietl & i
c echanges de charges - tres peu se recombinent sur le fondamental
c le niveau n de recombinaison a un khi(nij) a peu pres de 13.6
c cad un E/fond=khifond-13.6              alors on n'a plus besoin de pech
c dans le cas de j>3 on choisit le dernier niveau
        if(j.ge.3) then
                yy=tk1*(pkiev(i,j)-13.6d0)
                if(j.eq.5.and.i.eq.1.and.yy.lt.20.d0)      ! O1
     &     eci=ech(i,j)*expa(-yy)*8.d0/9.d0*phpne
                if(j.eq.8.and.i.eq.2.and.yy.lt.20.d0)      ! Si2
     &     eci=ech(i,j)*expa(-yy)/3.d0*phpne
                if(j.eq.8.and.i.eq.3.and.yy.lt.20.d0)      ! Si3
     &     eci=eche(i,j)*expa(tk1*(24.58d0-pkiev(i,j)))*phepne
           a(1,1)=a(1,1) - eci
           ai(1,1)=a(1,1)
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
              nij=nij0+1
              tapo(nij,m)=tapo(nij,m)+eci
           endif
           ecr=ech(i,j)*ph0ne + eche(i,j)*phe0ne
           b(niv)=b(niv) - ecr
           bi(niv)=b(niv)
        endif   ! j>3 
        if(j.eq.1.and.i.eq.1) then                                         !po1
           a(1,1)=a(1,1) - sech1
           b(1)=b(1) - sech2
           ai(1,1)=a(1,1)
           bi(1)=b(1)
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
              nij=nij0+1
              tapo(nij,m)=tapo(nij,m)+sech1
           endif
        endif   ! j=1 i=1
        if(j.eq.2.and.i.eq.1) then
           a(1,1)=a(1,1) - seche1
           b(1)=b(1) - seche2
           ai(1,1)=a(1,1)
           bi(1)=b(1)
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
              nij=nij0+1
              tapo(nij,m)=tapo(nij,m)+seche1
           endif
        endif   ! j=2 i=1
c
        if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
                do n=1,niv
                nij=nij0+n
           tbpo(nij,m)=-b(n)
                enddo   ! n
        endif
c
        call RESEQ1(a,niv,ndim,indx,d,myerr)
                if(myerr.eq.1) then
c              write(15,*) '  j   m nitt nitd'//
c     &        '  T   dz: MATRICE SINGULIERE-SINGULAR MATRIX sPOP1'
              write(6,*) '  j   m nitt nitd'//
     &        '  T   dz: MATRICE SINGULIERE-SINGULAR MATRIX sPOP1'
                write(6,'(4i4,6(1pe10.3))') j,m,nitt,nitd,T,dhaut
                do ii=1,niv
                write(6,'(8(1pe10.3))') (ai(ii,jj),jj=1,niv),bi(ii)
                enddo
                stop
                endif   ! myerr
        call RESEQ2(a,niv,ndim,indx,b)
c                                                        ! a et b sont modifies
                po1(1,i)=1.d0
                sompo1(i)=1.d0
                   kpb=0
                   if(b(1).lt.0.d0) kpb=1
                do n=2,niv
                   po1(n,i)=b(n)/b(1)
                   sompo1(i)=sompo1(i) + b(n)/b(1)
                   if(b(n).lt.0.d0) kpb=1
                enddo                                                      !po1
        po1(niv+1,i)=1.d0/b(1)
        f(i+1,j)=f(i,j)*po1(niv+1,i)
c calcul des epsilon pour le transfert ALI-simple
        if(kitd.eq.0) goto 21
        do 20 kr=kri,krf
           if(kali(kr).ne.2) goto 20
           n1=n1rt(kr)
           n2=n2rt(kr)
           alambda=alo(kr)
           gngn=0.d0                                              ! N2g1/N1g2
           if(po1(n1,i).gt.0.d0) then
              gngn=po1(n2,i)/po1(n1,i)*g1rt(kr)/g2rt(kr)
              gbs(kr)=(gngn/(alambda**2) *2.d16                   ! Sl-(1-eps)J
     &              -gas(kr)*cjn(kr)/pi4)/alambda *1.986485d-8       !hc*1e8
              if(gngn.ge.1.d0) then
c non :                                             tbs(kr,m)=0.d0
                 if(kitt.eq.1.and.kitn.eq.1.and.kitd.eq.1) 
     &           write(6,180) teer(kr,m),gngn,m,kr,nitd,nitt
                 gngn=teer(kr,m)
              endif
              tas(kr,m)=gas(kr)/(1.d0-gngn)
              tbs(kr,m)=gbs(kr)/(1.d0-gngn)
 180    format(' PB N2g1/N1g2=',1pe10.3,' remplace',1pe10.3,'>1 a m=',
     &         i3,' kr=',i3,' nitd=',i2,' nitt=',i2)
cPB N2g1/N1g2= 2.456e+90 remplace 2.456e+90>1 a m= 23 kr= 23 nitd= 2 nitt= 2 
           else   ! po1< ou=0
              tas(kr,m)=0.d0
              tbs(kr,m)=0.d0
           endif
c
           if(ifich.ge.5.and.ifich.le.8.and.kitt.eq.1.and.kitn.eq.1
     &       .and.kitd.eq.1.and.kimpf.eq.1
     &       .and.(m.le.3.or.mod(m,40).eq.0.or.m.ge.npt-2)
     &       .and.(kr.eq.2.or.kr.eq.46.or.kr.eq.68
     &       .or.kr.eq.330.or.kr.eq.379.or.kr.eq.898)) then
c            .or.kr.eq.215.or.kr.eq.237
c     &       .or.kr.eq.251.or.kr.eq.276.or.kr.eq.287.or.kr.eq.323
c     &       .or.kr.eq.761.or.kr.eq.848.or.kr.eq.809)) then
                aa=A21(kr) /ne
                bb=B12hnus4pi(kr)*cjn(kr) /ne                        ! B12J/Ne
                cc=texcoll(kr,m)                                     ! C21g2/g1
             if(kr.eq.2) write(86,*)' kr  m nitd    aa     bb    cc '//
     &          '  r(n1)    gngn    1-gngn   teer    cjn    as    bs'
             if(ab(j).gt.0.d0) write(86,'(i3,i4,i2,1p12e11.3)')
     &       kr,m,nitd,aa,bb,cc,dens(nij0+n1)/nht/ab(j),gngn,
     &       1.d0-gngn,teer(kr,m),cjn(kr),tas(kr,m),tbs(kr,m)
           endif
 20     continue   ! kr                                                    !po1

                   if(kpb.eq.1.and.kitt.eq.1.and.kitn.eq.1.and.kitd.eq.1
     &                      .and.r(i,j).gt.1.d-20) then       ! b<0
c                   if(kitt.eq.1.and.kitn.eq.1.and.kitd.eq.1.and.((kimpf
c     &             .eq.1.and.kimpm.eq.1.and.j.eq.5.and.(i.eq.6.or.i.eq.
c     &             8)).or.(kpb.eq.1.and.r(i,j).gt.1.d-20))) then
                   if(ifich.ge.1.and.ifich.le.8.and.iexpert.eq.1)
     &             write(15,297) m,j,i,nitd,nitt,ne,T,kitt
                   if(kpb.eq.1)write(6,297) m,j,i,nitd,nitt,ne,T,kitt
                   if(kpb.eq.0)write(6,298) m,j,i,nitd,nitt,ne,T,kitt
                   write(6,*) ' resultat Nn/Nion=b et Nn/N1=po1'
                   write(6,'(7(1pe11.3))') (b(jj),jj=1,niv)
                   write(6,'(7(1pe11.3))') (po1(n,i),n=1,niv+1)
                   write(6,*) ' matrice & recombinaisons'
                   do ii=1,niv
                   write(6,'(7(1pe11.3))') (ai(ii,jj),jj=1,niv),bi(ii)
                   enddo   ! ii
                   write(6,*)'phot/ne ioncol alfint rectot    rap=',rap
                   write(6,'(7(1pe11.3))') (tphot(nij0+n)/ne,n=1,niv)
                   write(6,'(7(1pe11.3))') 
     &                 (cioe(nij0+n)*expa(-hkin(nij0+n)*tk1),n=1,niv)
                   write(6,'(7(1pe11.3))') (alfint(nij0+n),n=1,niv)
                   write(6,'(7(1pe11.3))') (bi(ii),ii=1,niv)
                   write(6,*)'doublion(ion niv1) ecr(rec nivhaut) eci'
     &                       //' alftot'
                   write(6,'(7(1pe11.3))') doublion,ecr,eci,alpha(i,j)
                   return
                   endif   ! kpb=1
 297    format(' PB b<0 a m=',i3,' j=',i2,' i=',i2,' nitd=',i2,' nitt=',
     &         i2,' ne=',1pe10.3,' T=',1pe10.3,' kitt=',i1,'PoP1')
 298    format(' sPoP1 a m=',i3,' j=',i2,' i=',i2,' nitd=',i2,
     &         ' nitt=',i2,' ne=',1pe10.3,' T=',1pe10.3,' kitt=',i1)
c
 21     continue
        return
        end                                                               !spop1
c------------------------------------------------------------------------------
        subroutine sPoP2(i,j)
c
c calcul des degres d ionisation des ions a niv niveaux + ion, niv>1
c autre set d'equations, n=1(fondamental) est traite a part
c nivmax=15 niveaux maximum 
c inconnues : xn=Nn/Nion, sans n=1:fondamental
c resolution de A*X=B ou :        a(1,1)*x1 + a(1,2)*x2 + a(1,3)*x3 +...= b1
c                                a(2,1)*x1 + a(2,2)*x2 + a(2,3)*x3 +...= b2 ...
c avec les subroutines de Numerical Recipes
c apres les RESEQ les resultats sont dans B
c
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15)
         double precision ne,nht
         dimension a(nivmax,nivmax),b(nivmax),indx(nivmax),
     &     a2(nivmax,nivmax),b2(nivmax),ciot(nivmax),
     &     ai(nivmax,nivmax),bi(nivmax),a2i(nivmax,nivmax),b2i(nivmax)
         dimension gas(nrais),gbs(nrais)
         character tab*1
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/abond/ab(10)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/temp/T,VT,tk,tk1,templog
       common/atom/pkiev(26,10),pkiek(26,10),gfond(27,10),hkin(nivtots)
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/raies2/g1rt(nrais),g2rt(nrais),n1rt(nrais),n2rt(nrais)
       common/raies3/A21(nrais),A21hnu(nrais),A21hnus4pi(nrais),
     &               B12(nrais),B12hnus4pi(nrais),B12hnu2s4pi(nrais)
       common/raiesuta/probuta(100),dnuta2(100),utanion(26,10),
     &                 juta(100),iuta(100)
       common/deglosm/pnenht,praieint,r(27,10)
       common/tabstpo/sigmar(nrais,nivtots)
       common/datdeg/alpha(26,10),alfdi(26,10),edi(26,10),cio(nivtots),
     &          ech(26,10),eche(26,10),alfrad(26,10),
     &          teec(220),tgaufrec(220)
       common/dastpo/alfint(nivtots),cioe(nivtots),
     &          texcoll(nrais,nzs+1),teer(nrais,nzs)
       common/mstpo/rtaur0(nrais),rtaurl(nrais),
     &           twr(nrais),tws(nrais),uhz(nrais),cjn(nrais)
       common/stpo/tphot(nivtots),tphotk(26,10),dens(nivtots),
     &          alfinduit(nivtots),zfree,cthomson
       common/nuamd/tz(nzs+1),raymin,raymax,densinit,mbos(nzs)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/pop/po1(nivmax+1,26),sompo1(26),f(27,10),
     &             ph0ne,phpne,phe0ne,phepne,sech1,sech2,seche1,seche2
       common/rpal/twnu(16,nrais,nzs),tphi(16,nrais,nzs),
     &        tcapr(nrais,nzs),dcapr(nrais,nzs),dsl(nrais,nzs),
     &        tas(nrais,nzs),tbs(nrais,nzs),tne(nzs+1),tdensf(nzs,26,10)
       common/rpal2/kali(nrais),npbali(nrais),lcompt(nzs,nrais)
       common/sporpal/tjr(nrais,nzs),tcjn(nrais,nzs),pradr(nrais,nzs)
       common/pop1m/tapo(nivtots,nzs),tbpo(nivtots,nzs)
       save /atom/,/deglosm/,/tabstpo/,/datdeg/,/dastpo/,/mstpo/,/stpo/,
     &    /raies2/,/raies3/,/gnv/,/pop/,/pop1m/,/rpal/,/rpal2/,/sporpal/
c spop2
c
c        hcse=12398.54d0                                     ! h*c/e'
c        hcsb=1.43884d8                                      ! h*c/k en angstrom
           k4=nrcum(1,2)+1       ! He2-1
           k9=nrcum(7,5)+1       ! O8-1
        niv=nivion(i,j)
        if(niv.eq.1) then
           write(6,*) 'PB:niv=1 dans sPoP2 pour j,i=',j,i
           return
        endif
c        ndim=niv            ne marche pas
        ndim=nivmax
        saha=2.07079d-16/T/VT              ! saha=1/constante de la loi de saha
c        tk1=1.d0/8.6171d-5/T                                ! k/e'
c        write(10,*) ' dans sPoP2 j,i=',j,i
c
        iz0=iz(j)
        nij0=nivcum(i-1,j)
        kri=nrcum(i-1,j)+1
        krf=nrcum(i,j)
        do n1=1,niv
           b(n1)=0.d0
           bi(n1)=0.d0
           do n2=1,niv
              a(n1,n2)=0.d0
              ai(n1,n2)=0.d0
           enddo
        enddo
           ecr=0.d0
           eci=0.d0
           doublion=0.d0
c elements non diagonaux
        do 10 kr=kri,krf
           n1=n1rt(kr)
           n2=n2rt(kr)
           gn1=gps(nij0+n1)
           gn2=gps(nij0+n2)
           alambda=alo(kr)
           hnuev=hcse/alambda
c cjn est 4piJ/hnu
                ee=teer(kr,m)                  ! (0.667d16)
                aa=A21(kr) /ne
                bb=B12hnus4pi(kr)*cjn(kr) /ne                        ! B12J/Ne
                cc=texcoll(kr,m)                                     ! C21g2/g1
        a(n2,n1)=bb + cc*ee                         ! En1n2=B12J/Ne+C12
        a(n1,n2)=aa + gn1/gn2*(bb+cc)               ! Dn2n1=A21/Ne+B21J/Ne+C21
        ai(n2,n1)=a(n2,n1)
        ai(n1,n2)=a(n1,n2)
           gas(kr)=aa/(aa + bb + cc)
c
       if(ifich.ge.4.and.ifich.le.8.and.imp.ge.6.and.imp.le.7.and.
     &    kitt.eq.1.and.kitn.eq.1.and.kitd.eq.1.and.kimpm.eq.1.and.
     &    kimpf2.eq.1.and.ifich.ne.9.and.(kr.eq.k4.or.kr.eq.k9)) then
        WRITE(21,*) ' kr     aa         bb',
     &        '          cc          ee       am'
        am=gn1/gn2*fdo(kr)/alambda*413.23d0/VT                             !po2
        WRITE(21,'(i4,6(1pe12.5))') kr,aa,bb,cc,ee,am
        endif   ! imp
c
   10                continue
c
c elements diagonaux et second membre
                   salfn=0.d0
                do n=1,niv
                nij=nij0+n
                gn=gps(nij)
                   som=0.d0
                   do ii=1,niv
                      if(ii.ne.n) som=som + a(ii,n)
                   enddo   ! ii
        ciot(n)=cioe(nij)*expa(-hkin(nij)*tk1) + tphot(nij)/ne
        a(n,n)= -(cioe(nij)*expa(-hkin(nij)*tk1) + tphot(nij)/ne + som)  ! In
        b(n)=-(alfint(nij)+alfinduit(nij) + ne*cioe(nij)*gn*saha)        ! Rn
        ai(n,n)=a(n,n)
        bi(n)=b(n)
                if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1)
     &  tapo(nij,m)=cioe(nij)*expa(-hkin(nij)*tk1) + tphot(nij)/ne
                salfn=salfn + alfint(nij)
c      if(n.eq.2)write(10,777)a(n,n),cioe(nij0+2),cioe(nij0+2)*expa
c     &        (-pkiev(nij)*tk1),tphot(nij)/ne,som
                enddo   ! n
c
c irecsup=1 : les recombinaisons (y compris dielectroniques) sur les niveaux
c excites superieurs sont attribuees a n=niv (et niv1)
        if(irecsup.eq.1.and.ietl.eq.0) then
           alf=alpha(i,j)-salfn
              if(alf.lt.0.d0) alf=0.d0
              nivsup=niv1(i,j)
           if(nivsup.eq.0) then
              b(niv)=b(niv) - alf
              bi(niv)=b(niv)
           else                        ! 2 systemes de niveaux
              if(i.eq.iz0-1) fr=1.d0/4.d0                ! heliumoides
              if(i.eq.iz0-3) fr=1.d0/4.d0                ! Be
              if(i.eq.iz0-4) fr=1.d0/3.d0                ! B
              b(nivsup)=b(nivsup) - alf*fr
              bi(nivsup)=b(nivsup)
              b(niv)=b(niv) - alf*(1.d0-fr)
              bi(niv)=b(niv)
           endif
        endif
c irecsup=2 : les recombinaisons (y compris dielectroniques) sur les niveaux
c excites superieurs sont distribuees sur tous les niveaux
        if(irecsup.eq.2.and.ietl.eq.0) then
           rap=alpha(i,j)/salfn -1.d0
           if(rap.lt.0.d0) rap=0.d0
                do n=1,niv
                nij=nij0+n
           b(n)=b(n) - alfint(nij)*rap
           bi(n)=b(n)
                enddo   ! n
        endif
c effet Auger
c     l'effet Auger Fe24+photonK ne donne pas un Fe26 mais 25 - Band1990 -
c     (question de DeltaE)
               doublion=0.d0                                               !po2
        if(ietl.eq.0.and.j.ge.3.and.i.ne.1.and.i.le.(iz0-2)) then
           doublion=f(i-1,j)/f(i,j)*tphotk(i-1,j)/ne
           if(j.eq.10) then
              doublion=(1.d0-pfluo(i-1))*doublion                     ! Fe2-24
              if(i.ge.18) doublion=doublion + f(i,j)*cjn(nraissfek+i-1)  !18-24
     &             *B12hnus4pi(nraissfek+i-1)*(1.d0-pfluo(i-1))/ne
            if(doublion.lt.0.d0.and.nitd.eq.1) doublion=0.d0
           endif   ! j=10
           a(1,1)=a(1,1) - doublion
           ciot(1)=ciot(1) + doublion
           ai(1,1)=a(1,1)
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
              nij=nij0+1
              tapo(nij,m)=tapo(nij,m)+doublion
           endif
        endif   ! ietl & i
c echanges de charges - tres peu se recombinent sur le fondamental
c le niveau n de recombinaison a un khi(nij) a peu pres de 13.6
c cad un E/fond=khifond-13.6              alors on n'a plus besoin de pech
c dans le cas de j>3 on choisit le dernier niveau
        if(j.ge.3) then
                yy=tk1*(pkiev(i,j)-13.6d0)
                if(j.eq.5.and.i.eq.1.and.yy.lt.20.d0)      ! O1
     &     eci=ech(i,j)*expa(-yy)*8.d0/9.d0*phpne
                if(j.eq.8.and.i.eq.2.and.yy.lt.20.d0)      ! Si2
     &     eci=ech(i,j)*expa(-yy)/3.d0*phpne
                if(j.eq.8.and.i.eq.3.and.yy.lt.20.d0)      ! Si3
     &     eci=eche(i,j)*expa(tk1*(24.58d0-pkiev(i,j)))*phepne
           a(1,1)=a(1,1) - eci
           ciot(1)=ciot(1) + eci
           ai(1,1)=a(1,1)
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
              nij=nij0+1
              tapo(nij,m)=tapo(nij,m)+eci
           endif
           ecr=ech(i,j)*ph0ne + eche(i,j)*phe0ne
           b(niv)=b(niv) - ecr
           bi(niv)=b(niv)
        endif   ! j>3 
        if(j.eq.1.and.i.eq.1) then                                         !po2
           a(1,1)=a(1,1) - sech1
           ciot(1)=ciot(1) + sech1
           b(1)=b(1) - sech2
           ai(1,1)=a(1,1)
           bi(1)=b(1)
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
              nij=nij0+1
              tapo(nij,m)=tapo(nij,m)+sech1
           endif
        endif   ! j=1 i=1
        if(j.eq.2.and.i.eq.1) then
           a(1,1)=a(1,1) - seche1
           ciot(1)=ciot(1) + seche1
           b(1)=b(1) - seche2
           ai(1,1)=a(1,1)
           bi(1)=b(1)
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
              nij=nij0+1
              tapo(nij,m)=tapo(nij,m)+seche1
           endif
        endif   ! j=2 i=1
c autoionisation apres absorption d'un photon UTA
        a(1,1)=a(1,1) - utanion(i,j)/ne
        ciot(1)=ciot(1) + utanion(i,j)/ne
        ai(1,1)=a(1,1)
           if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
              nij=nij0+1
              tapo(nij,m)=tapo(nij,m) + utanion(i,j)/ne
           endif
c
        if(kitd.eq.1.and.kitt.eq.1.and.kitn.eq.1) then
                do n=1,niv
                nij=nij0+n
           tbpo(nij,m)=-b(n)
                enddo   ! n
        endif
c
c particularite de sPOP2
           somr=0.d0
        do n=1,niv
           somr=somr - b(n)
        enddo   ! n
        apn1ni=somr/ciot(1)   ! N1/Nion si on neglige les ionis. des niv.super.
        do n=2,niv
           b2(n-1)=b(n) - a(n,1)*apn1ni
           b2i(n-1)=b2(n-1)
c           a2(n-1,n-1)=a(n,n) - a(n,1)*ciot(n)/ciot(1)
           do n2=2,niv
              a2(n-1,n2-1)=a(n,n2) - a(n,1)*ciot(n2)/ciot(1)
              a2i(n-1,n2-1)=a2(n-1,n2-1)
           enddo   ! n2
        enddo   ! n
c
        call RESEQ1(a2,niv-1,ndim,indx,d,myerr)
                if(myerr.eq.1) then
c              write(15,*) '  j   m nitt nitd'//
c     &        '  T   dz: MATRICE SINGULIERE-SINGULAR MATRIX sPOP2'
              write(6,*) '  j   m nitt nitd'//
     &        '  T   dz: MATRICE SINGULIERE-SINGULAR MATRIX sPOP2'
                write(6,'(4i4,6(1pe10.3))') j,m,nitt,nitd,T,dhaut
                do ii=1,niv-1
                write(6,'(8(1pe10.3))')(a2i(ii,jj),jj=1,niv-1),b2i(ii)
                enddo
                stop
                endif   ! myerr
        call RESEQ2(a2,niv-1,ndim,indx,b2)
c                              ! a2 et b2 sont modifies b2=N2/Ni,N3/Ni,N4/Ni...
c 
           kpb=0
           somi=0.d0
           do n=2,niv
              somi=somi + ciot(n)*b2(n-1)
           enddo                                                         !po2
           po1(niv+1,i)=0.d0
           if((apn1ni-somi/ciot(1)).ne.0.d0)
     &  po1(niv+1,i)=1.d0/(apn1ni-somi/ciot(1))
           if(po1(niv+1,i).le.0.d0) kpb=1
        po1(1,i)=1.d0
        sompo1(i)=1.d0
        do n=2,niv
           po1(n,i)=b2(n-1)*po1(niv+1,i)
           sompo1(i)=sompo1(i) + po1(n,i)
           if(b2(n-1).lt.0.d0) kpb=1
        enddo
        f(i+1,j)=f(i,j)*po1(niv+1,i)
c
           if(kpb.eq.1) then
                      imp=0
                      if(kitt.eq.1.and.kitn.eq.1.and.kitd.eq.1.and.
     &                      abs(po1(niv+1,i)).gt.1.d-20) then       ! b<0
c                      write(15,297) m,j,i,nitd,nitt,ne,T,kitt
                      imp=1
                      write(6,297) m,j,i,nitd,nitt,ne,T,kitt
c                      write(6,*)' apn1ni somrec somion / coef-ionis-tot'
                      write(6,*)' apn1ni somrec somion'
                      write(6,'(7(1pe11.3))') apn1ni,somr,somi
c                      write(6,'(7(1pe11.3))') (ciot(jj),jj=1,niv)
                      write(6,*) ' resultat Nn/Nion=b2,Nn/N1=po1 et fij'
                      write(6,'(7(1pe11.3))') (b2(jj),jj=1,niv-1)
                      write(6,'(7(1pe11.3))') (po1(n,i),n=1,niv+1)
                      write(6,'(7(1pe11.3))') (f(i,j),i=1,iz0+1)
                      write(6,*) ' matrice & recombinaisons'
                      do ii=1,niv-1
                  write(6,'(7(1pe11.3))')(a2i(ii,jj),jj=1,niv-1),b2i(ii)!2345678
                      enddo   ! ii
                      write(6,*)'phot/ne ioncol alfint rectot  rap=',rap
                      write(6,'(7(1pe11.3))') (tphot(nij0+n)/ne,n=1,niv)
                      write(6,'(7(1pe11.3))') 
     &                 (cioe(nij0+n)*expa(-hkin(nij0+n)*tk1),n=1,niv)
                      write(6,'(7(1pe11.3))') (alfint(nij0+n),n=1,niv)
                      write(6,'(7(1pe11.3))') (bi(ii),ii=1,niv)
                      write(6,*)'doublion(ion n=1) ecr(rec nivhaut) eci'
     &                          //' alftot'
                      write(6,'(7(1pe11.3))')doublion,ecr,eci,alpha(i,j)
                      endif   ! impressions
              sompo1(i)=1.d0
           do n=2,niv+1
              if(po1(n,i).lt.0.d0) po1(n,i)=0.d0
              sompo1(i)=sompo1(i) + po1(n,i)
           enddo
           f(i+1,j)=f(i,j)*po1(niv+1,i)
                      if(imp.eq.1) then
                      write(6,*) ' nouveaux Nn/N1=po1'
                      write(6,'(7(1pe11.3))') (po1(n,i),n=1,niv+1)
                      endif
           endif   ! kpb=1
 297    format(' PB b<0 a m=',i3,' j=',i2,' i=',i2,' nitd=',i2,' nitt=',
     &         i2,' ne=',1pe10.3,' T=',1pe10.3,' kitt=',i1,'PoP2')
cPB b<0 a m=123 j=12 i=12 nitd=12 nitt=12 ne= 2.456E+90 T= 2.456E+90 kitt=1PoP2
c
c calcul des epsilon pour le transfert ALI-simple (cas d'ions multi-niveaux)
        if(kitd.eq.0) goto 21
        do 20 kr=kri,krf
           if(kali(kr).ne.2) goto 20
           n1=n1rt(kr)
           n2=n2rt(kr)
           alambda=alo(kr)
           gngn=0.d0                                              ! N2g1/N1g2
           if(po1(n1,i).gt.0.d0) then
              gngn=po1(n2,i)/po1(n1,i)*g1rt(kr)/g2rt(kr)
              gbs(kr)=(gngn/(alambda**2) *2.d16                   ! Sl-(1-eps)J
     &              -gas(kr)*cjn(kr)/pi4)/alambda *1.986485d-8       !hc*1e8
              if(gngn.ge.1.d0) then
c non :                                             tbs(kr,m)=0.d0
                 if(kitt.eq.1.and.kitn.eq.1.and.kitd.eq.1) 
     &           write(6,180) teer(kr,m),gngn,m,kr,nitd,nitt
                 gngn=teer(kr,m)
              endif
              tas(kr,m)=gas(kr)/(1.d0-gngn)
              tbs(kr,m)=gbs(kr)/(1.d0-gngn)
 180    format(' PB N2g1/N1g2=',1pe10.3,' remplace',1pe10.3,'>1 a m=',
     &         i3,' kr=',i3,' nitd=',i2,' nitt=',i2)
           else   ! po1< ou=0
              tas(kr,m)=0.d0
              tbs(kr,m)=0.d0
          endif
c
           if(ifich.ge.5.and.ifich.le.8
     &       .and.kitt.eq.1.and.kitn.eq.1.and.kitd.eq.1.and.kimpf.eq.1
     &       .and.(m.le.3.or.mod(m,40).eq.0.or.m.ge.npt-2)
     &       .and.(kr.eq.2.or.kr.eq.46.or.kr.eq.215.or.kr.eq.237
     &       .or.kr.eq.251.or.kr.eq.276.or.kr.eq.287.or.kr.eq.323
     &       .or.kr.eq.761.or.kr.eq.772.or.kr.eq.898)) then
                aa=A21(kr) /ne
                bb=B12hnus4pi(kr)*cjn(kr) /ne                        ! B12J/Ne
                cc=texcoll(kr,m)                                     ! C21g2/g1
             if(kr.eq.2) write(86,*)' kr  m nitd    aa     bb    cc '//
     &          '  r(n1)    gngn    1-gngn   teer    cjn    as    bs'
             if(ab(j).gt.0.d0) write(86,'(i3,i4,i2,1p12e11.3)') 
     &       kr,m,nitd,aa,bb,cc,dens(nij0+n1)/nht/ab(j),gngn,
     &       1.d0-gngn,teer(kr,m),cjn(kr),tas(kr,m),tbs(kr,m)
           endif
 20     continue   ! kr                                                    !po2
 21     continue
        return
        end                                                               !spop2
c------------------------------------------------------------------------------
        subroutine sPoPmali(j,i)
c
c calcul des degres d ionisation des ions a niv niveaux + ion, niv>1
c dans le cas de l'iteration avec le calcul de J avec ALI (a T et deg.ion.csts)
c nivmax=15 niveaux maximum 
c inconnues : xn=Nn/Nion, n=1:fond.
c resolution de A*X=B ou :        a(1,1)*x1 + a(1,2)*x2 + a(1,3)*x3 +...= b1
c                                a(2,1)*x1 + a(2,2)*x2 + a(2,3)*x3 +...= b2 ...
c apres les RESEQ les resultats sont dans B
c
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nivtots=330,nrais=4200,nivmax=15)
         double precision ne,nht
         dimension a(nivmax,nivmax),b(nivmax),indx(nivmax),
     &             ai(nivmax,nivmax),bi(nivmax)
         character tab*1
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/cst2/chel,esh,hca,hcse,hcsb,ckev,bhca
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/temp/T,VT,tk,tk1,templog
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/raies2/g1rt(nrais),g2rt(nrais),n1rt(nrais),n2rt(nrais)
       common/raies3/A21(nrais),A21hnu(nrais),A21hnus4pi(nrais),
     &               B12(nrais),B12hnus4pi(nrais),B12hnu2s4pi(nrais)
       common/gnv/gps(nivtots),nivion(26,10),niv1(26,10),nivcum(0:27,10)
       common/dastpo/alfint(nivtots),cioe(nivtots),
     &          texcoll(nrais,nzs+1),teer(nrais,nzs)
       common/rpal2/kali(nrais),npbali(nrais),lcompt(nzs,nrais)
       common/rpal3/tlstar(nrais,nzs+1),tjeff(nrais,nzs+1)
       common/sporpal/tjr(nrais,nzs),tcjn(nrais,nzs),pradr(nrais,nzs)
       common/pop/po1(nivmax+1,26),sompo1(26),f(27,10),
     &             ph0ne,phpne,phe0ne,phepne,sech1,sech2,seche1,seche2
       common/pop1m/tapo(nivtots,nzs),tbpo(nivtots,nzs)
       save /gnv/,/rpal2/,/rpal3/,/sporpal/,/pop/,/pop1m/,/dastpo/
c spopmali
c
c        hcse=12398.54d0                                     ! h*c/e'*1e8
c        hcsb=1.43884d8                                      ! h*c/k en angstrom
           k4=nrcum(1,2)+1       ! He2-1
           k9=nrcum(7,5)+1       ! O8-1
        niv=nivion(i,j)
        if(niv.eq.1) then
           write(6,*) 'PB:niv=1 dans sPoPmali pour j,i=',j,i
           return
        endif
c        ndim=niv            ne marche pas
        ndim=nivmax
        saha=2.07079d-16/T/VT              ! saha=1/constante de la loi de saha
c        tk1=1.d0/8.6171d-5/T                                ! k/e'
c        write(10,*) ' dans sPoPmali j,i=',j,i
c
        iz0=iz(j)
        nij0=nivcum(i-1,j)
        kri=nrcum(i-1,j)+1
        krf=nrcum(i,j)
        do n1=1,niv
           b(n1)=0.d0
           bi(n1)=0.d0
           do n2=1,niv
              a(n1,n2)=0.d0                                               !pom
              ai(n1,n2)=0.d0
           enddo
        enddo
c elements non diagonaux
        do 10 kr=kri,krf
           n1=n1rt(kr)
           n2=n2rt(kr)
           gn1=gps(nij0+n1)
           gn2=gps(nij0+n2)
           alambda=alo(kr)
           hnuev=hcse/alambda
c cjn est 4piJ/hnu
                ee=teer(kr,m)                  ! (0.667d16)
                aa=A21(kr) /ne
        if(kali(kr).eq.3.and.nita.ge.1) aa=aa*(1.d0-tlstar(kr,m))
        if(kali(kr).le.2.or.nita.eq.0) bb=B12hnus4pi(kr)*tcjn(kr,m) /ne
        if(kali(kr).eq.3.and.nita.ge.1) bb=B12(kr)*tjeff(kr,m) /ne
                if(kali(kr).eq.0) bb=0.d0                            ! B12J/Ne
                cc=texcoll(kr,m)                                     ! C21g2/g1
        a(n2,n1)=bb + cc*ee                          ! En1n2=B12J/Ne+C12
        a(n1,n2)=aa + gn1/gn2*(bb + cc)              ! Dn2n1=A21/Ne+B21J/Ne+C21
        ai(n2,n1)=a(n2,n1)
        ai(n1,n2)=a(n1,n2)
c                                                                         !pom
        if(ifich.ge.5.and.ifich.le.8.and.kitt.eq.1.and.kitd.eq.1
     &     .and.kitn.eq.1.and.kimpm.eq.1.and.kimpf.eq.1
     &     .and.(j.le.2.or.j.eq.5).and.(nita.le.2.or.kita.eq.1))then
c     &  (j.le.2.or.j.eq.5.or.j.eq.9).and.(nita.le.2.or.kita.eq.1))then !2345678
           if(nita.eq.0.and.m.eq.1.and.j.eq.1.and.kr.eq.kri) then
              OPEN(UNIT=83,FILE='fort.83',STATUS='unknown')
              write(83,*)'nitf=',nitf
           endif
           if(kr.eq.kri) then
              write(83,*) 'nita m nitf j i T nht ne : popmali'
              write(83,'(i3,2i4,2i3,1p5e10.3)')nita,m,nitf,j,i,T,nht,ne
              write(83,*) 'kr kali B12      tlstar     tcjn'//
     &                    '      tjeff     aa        bb        cc'
           endif
           write(83,'(i4,i2,1p9e10.3)')kr,kali(kr),B12(kr),
     &           tlstar(kr,m),tcjn(kr,m),tjeff(kr,m),aa,bb,cc
        endif
c
      if(ifich.ge.4.and.ifich.le.8.and.imp.ge.6.and.imp.le.7
     &   .and.kitt.eq.1.and.kitn.eq.1.and.kitd.eq.1.and.kimpm.eq.1
     &   .and.kimpf2.eq.1.and.(kr.eq.k4.or.kr.eq.k9)) then
        WRITE(21,*) ' kr     aa         bb',
     &        '          cc          ee       am'
c        WRITE(21,*) ' kr   gngn         aa          bb',
c     &        '          cc          ee       am'
        am=gn1/gn2*fdo(kr)/alambda*413.23d0/VT                            !pom
c        gngn=0.d0
c        if(po1(n1,i).gt.0.d0) gngn=gn1*po1(n2,i)/gn2/po1(n1,i)
c        WRITE(21,'(i4,6(1pe12.5))') kr,gngn,aa,bb,cc,ee,am
        WRITE(21,'(i4,6(1pe12.5))') kr,aa,bb,cc,ee,am
        endif   ! imp
c
   10                continue
c
c elements diagonaux et second membre
                do n=1,niv
                nij=nij0+n
                   som=0.d0
                   do ii=1,niv
                      if(ii.ne.n) som=som + a(ii,n)
                   enddo   ! ii
        a(n,n)= -(tapo(nij,m) + som)         ! In
        b(n)=-tbpo(nij,m)                    ! Rn
        ai(n,n)=a(n,n)
        bi(n)=b(n)
                enddo   ! n
c
        call RESEQ1(a,niv,ndim,indx,d,myerr)                              !pom
                if(myerr.eq.1) then
c              write(15,*)'  j   i   m nitt nitd'//
c     &        '  T   dz: MATRICE SINGULIERE-SINGULAR MATRIX sPOPmali'
              write(6,*) '  j   i   m nitt nitd'//
     &        '  T   dz: MATRICE SINGULIERE-SINGULAR MATRIX sPOPmali'
                write(6,'(5i4,6(1pe10.3))') j,i,m,nitt,nitd,T,dhaut
                do ii=1,niv
                write(6,'(8(1pe10.3))') (ai(ii,jj),jj=1,niv),bi(ii)
                enddo
                stop
                endif   ! myerr
        call RESEQ2(a,niv,ndim,indx,b)
c                                                        ! a et b sont modifies
                po1(1,i)=1.d0
                sompo1(i)=1.d0
                   kpb=0
                   if(b(1).lt.-1.d-30.or.b(1).eq.0.d0) kpb=1
                do n=2,niv
                      if(b(1).ne.0.d0) then
                   po1(n,i)=b(n)/b(1)
                   sompo1(i)=sompo1(i) + b(n)/b(1)
                   if(b(n).lt.-1.d-30) kpb=1
                      endif
                   if(po1(n,i).lt.0.d0.or.b(1).eq.0.d0) po1(n,i)=0.d0  !essai
                enddo                                                     !pom
                   po1(niv+1,i)=0.d0
         if(b(1).ne.0.d0) po1(niv+1,i)=1.d0/b(1)
c
c        if(m.eq.1.and.nitf.eq.1.and.nitt.eq.1) then
c           nij1=nij0+1
c           ccc=cioe(nij1)*expa(-hkin(nij1)*tk1)
c        write(75,'(i2,i3,8(1pe9.2))') 
c     &  j,i,ccc,tphot(nij1)/ne,eci,doublion,alfint(nij1),
c     &  alfinduit(nij1),cioe(nij1)*gps(nij1)*saha*ne,ecr
c        write(75,'(8(1pe9.2))') (po1(n,i),n=1,niv+1),sompo1(i)
c        write(75,'(8(1pe9.2))') f(i,j),f(i+1,j)
c        endif

c                   if(kpb.eq.1.and.nita.gt.1) then     ! b<0
                   if(kpb.eq.1.and.kita.eq.1) then     ! b<0
c                   write(15,297) m,j,i,nita,ne,T,kita
                   write(6,297) m,j,i,nita,ne,T,kita
                   write(6,*) ' resultat Nn/Nion=b et Nn/N1=po1'
                   write(6,'(7(1pe11.3))') (b(jj),jj=1,niv)
                   write(6,'(7(1pe11.3))') (po1(n,i),n=1,niv+1)
                   write(6,*) ' matrice & recombinaisons'
                   do ii=1,niv
                   write(6,'(7(1pe11.3))') (ai(ii,jj),jj=1,niv),bi(ii)
                   enddo   ! ii
                   write(6,*) 'tapo tbpo'                                  !pom
                   write(6,'(7(1pe11.3))') (tapo(nij0+n,m),n=1,niv)
                   write(6,'(7(1pe11.3))') (tbpo(nij0+n,m),n=1,niv)
                   return
                   endif   ! kpb=1
 297    format(' PBmali b<0 a m=',i3,' j=',i2,' i=',i2,' nita=',i2,
     &         ' ne=',1pe9.3,' T=',1pe9.3,' kita=',i1)
cPBmali b<0 a m=123 j=12 i=12 nita=12 ne=1.345E+89 T=1.345E+89 kita=1

        return
        end                                                           !spopmali
c------------------------------------------------------------------------------
      subroutine RESEQ1(a,n,np,indx,d,myerr)

c resolution de n equations lineaires a n inconnues

c                subroutine LUDCMP(a,n,np,indx,d) de NUMERICAL RECIPES 1986
c resolution de A*X=B ou :        a(1,1)*x1 + a(1,2)*x2 + a(1,3)*x3 +...= b1
c                                a(2,1)*x1 + a(2,2)*x2 + a(2,3)*x3 +...= b2 ...
c matrice A en entree(n*n,dimensionnee np), modifiee en sortie - indx : sortie
c
        implicit double precision (a-h,o-z)
        parameter (nmax=100,tiny=1.0d-20)
        dimension a(np,np),indx(n),vv(nmax)
        myerr=0                                        ! flag perso
      d=1.d0
      do 12 i=1,n
        aamax=0.d0
        do 11 j=1,n                                                          !r
          if (abs(a(i,j)).gt.aamax) aamax=abs(a(i,j))
11      continue
c        if (aamax.eq.0.d0) pause 'SINGULAR MATRIX.'                ! original
                if (aamax.eq.0.d0) then
                myerr=1
                return
                endif
caa
        vv(i)=1.d0/aamax
12    continue
      do 19 j=1,n
        if (j.gt.1) then
          do 14 i=1,j-1
            sum=a(i,j)
            if (i.gt.1)then
              do 13 k=1,i-1
                sum=sum-a(i,k)*a(k,j)
13            continue
              a(i,j)=sum
            endif
14        continue                                                           !r
        endif
        aamax=0.d0
        do 16 i=j,n
          sum=a(i,j)
          if (j.gt.1)then
            do 15 k=1,j-1
              sum=sum-a(i,k)*a(k,j)
15          continue
            a(i,j)=sum
          endif
          dum=vv(i)*abs(sum)
          if (dum.ge.aamax) then
            imax=i
            aamax=dum
          endif
16      continue                                                             !r
        if (j.ne.imax)then
          do 17 k=1,n
            dum=a(imax,k)
            a(imax,k)=a(j,k)
            a(j,k)=dum
17        continue
          d=-d
          vv(imax)=vv(j)
        endif
        indx(j)=imax
        if(j.ne.n)then
          if(a(j,j).eq.0.d0) a(j,j)=tiny
          dum=1.d0/a(j,j)
          do 18 i=j+1,n
            a(i,j)=a(i,j)*dum
18        continue                                                           !r
        endif
19    continue
      if(a(n,n).eq.0.d0) a(n,n)=tiny
      return
      end                                                               !reseq1
c------------------------------------------------------------------------------
      subroutine RESEQ2(a,n,np,indx,b)

c resolution de n equations lineaires a n inconnues

c                subroutine LUBKSB(a,n,np,indx,b) de NUMERICAL RECIPES 1986
c resolution de A*X=B
c B en entree, egal en sortie a la solution X - a et indx = sorties de RESEQ1
c
        implicit double precision (a-h,o-z)
        dimension a(np,np),indx(n),b(n)
      ii=0
      do 12 i=1,n
        ll=indx(i)
        sum=b(ll)
        b(ll)=b(i)
        if (ii.ne.0)then
          do 11 j=ii,i-1
            sum=sum-a(i,j)*b(j)
11        continue                                                           !r
        else if (sum.ne.0.d0) then
          ii=i
        endif
        b(i)=sum
12    continue
      do 14 i=n,1,-1
        sum=b(i)
        if(i.lt.n)then
          do 13 j=i+1,n
            sum=sum-a(i,j)*b(j)
13        continue
        endif
        b(i)=sum/a(i,i)
14    continue
      return
      end                                                                    !r
c--============================================================================
      function GFI(enel,ph0ne)
c
c calcul du nombre de photoionisations de l'hydrogene dues a un electron de
c grande energie Enel, produit par photoionisation d'un ion par un photon X
c
c coefficients calcules par Bergeron-Souffrin, cf 1973,A&A,25,1
c les e(j) sont en progression geometrique d'un facteur 2
c ATTENTION les fits sont mauvais si phone>300 (H0/Htot>.997) ET hnuev>5kev
c
        implicit double precision (a-h,o-z)
           dimension e(9),c(7,9),y(9)
           common/cst3/ee4,collg,hlog2
        data e/42.5d0,85.d0,170.d0,340.d0,680.d0,1360.d0,2720.d0,
     &       5440.d0,10880.d0/,
     &      ((c(i,j),i=1,7),j=1,9)/
     &-1.886 , 0.9752,-0.03297,-0.01257,-0.009543,-0.004214, 0.001700, 
     &-1.126 , 0.9645,-0.1442 ,0.001164, 0.04181 ,-0.03473 , 0.006687,
     &-0.6101, 0.9420,-0.09900,-0.05199, 0.01119 ,-0.002813, 0.0007540,
     &-0.1613, 0.9219,-0.1120 ,-0.06893, 0.008425, 0.005507,-0.001018,
     & 0.2424, 0.8962,-0.1245 ,-0.06674, 0.005095, 0.008338,-0.001546,
     & 0.6286, 0.8650,-0.1479 ,-0.05627, 0.009119, 0.005586,-0.001159,
     & 0.9882, 0.8476,-0.1639 ,-0.06168, 0.01951 , 0.002715,-0.0009673,
     & 1.345 , 0.8317,-0.1955 ,-0.05924, 0.03762 ,-0.006028, 0.0002257,
     & 1.691 , 0.8064,-0.2017 ,-0.05371, 0.03406 ,-0.003759, 0.0002221/
c
c        hlog2=log10(2.d0)
c 
c        write(6,*) e(4),(c(i,4),i=1,7)
        x=log10(ph0ne)
        if(ph0ne.le.0.1) x=-1.d0
        if(ph0ne.gt.1.d3) x=3.
        p=1.d0/ph0ne
           do j=1,9
           y(j)=0.d0
           enddo ! j
                              if(enel.lt.e(1)) then
           do i=1,7
              y(1)=y(1) + c(i,1)* x**(i-1)
              enddo ! i
        GFI=10.d0**y(1) *(enel-13.6d0)/28.9d0
c        write(6,*) '1',y(1),gfi
        goto 100
                               else if(enel.lt.e(2)) then
           do i=1,7
              y(1)=y(1) + c(i,1)* x**(i-1)
              y(2)=y(2) + c(i,2)* x**(i-1)
              enddo ! i
        GFI=10.d0**(y(1) - (y(1)-y(2)) *log10(enel/e(1)) /hlog2)
c        write(6,*) '2',y(1),y(2),gfi
        goto 100
                               else if(enel.ge.e(9)) then
           do i=1,7
              y(9)=y(9) + c(i,9)* x**(i-1)
              enddo ! i
        GFI=10.d0**y(9) *enel/e(9)
c        write(6,*) '10',y(9),gfi
        goto 100
                               endif
c
                     do j=3,9
                               if(enel.lt.e(j)) then
           do i=1,7
              y(j)=y(j) + c(i,j)* x**(i-1)
              y(j-1)=y(j-1) + c(i,j-1)* x**(i-1)
              y(j-2)=y(j-2) + c(i,j-2)* x**(i-1)
              enddo ! i
              efi=y(j) + (y(j)-y(j-1)) *log10(enel/e(j))/hlog2
     &                 + (y(j)-2.d0*y(j-1)+y(j-2))*log10(enel/e(j))
     &                 *log10(enel/e(j-1))/2.d0/hlog2/hlog2
        GFI=10.d0**efi
c        write(6,*) j,y(j),y(j-1),y(j-2),efi,gfi
        goto 100
                               endif
                     enddo ! j
c
 100    if(ph0ne.le.0.1) GFI=gfi*ph0ne/0.1d0
        return
        end
c------------------------------------------------------------------------------
      subroutine LOSSA

c calcul des pertes par les raies interdites
 
       implicit double precision (a-h,o-z)
         dimension omega(150),ae(150),dv(150),gg(150)
         dimension p(26,10)
         dimension iobs(30),jobs(30),pobs(30)
         dimension g3n(7,3),ae3(7,6,3),om3(7,6,3),dv3(7,6,3),nbrele(7)  !234567
         dimension fr(3),gr(3),qr(3),popn(3),pert3(7)
         dimension wlobs(30)
         dimension iint(50),jint(50),rin32(50),rin31(50),rin21(50),
     &             wl32(50),wl31(50),wl21(50)
       double precision ne,nht
       common/abond/ab(10)
       common/strat/nht,dendex,dhaut,ne,dilu,dilutot,coltot,htot
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/nbr2/m,npt,nitd,nitt,nitn,nitf,nita,nfmax,
     &               kitd,kitt,kitn,kitf,kita,kimpm,kimpf,kimpf2
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/temp/T,VT,tk,tk1,templog
       common/leclos/omega,ae,dv,gg,g3n,ae3,om3,dv3,nbrele
       common/deglosm/pnenht,praieint,r(27,10)
       save /leclos/,/deglosm/
c lossa
c      az(x)=sqrt(x)/(1.d0+0.105*x+0.015*x*x)
c      bz(x)=sqrt(x/(x*x+13.4))*(x+1.d0)**2.5d0
      hcsb=1.43884d8
c      b=8.6171d-5      tk=b*T
      ne=pnenht*nht
      kobs=0
      kper=0
      kint=0
 575  km=0
        mimp=0
      do 76 j=1,nelem
      iz0=iz(j)                                                        
      do 76 i=1,iz0
      p(i,j)=0.d0
      km=km+1
   79 q12sex=8.6d-6*omega(km)/VT
        pert=0.d0
        if(gg(km).gt.0.d0) 
     1 pert=ab(j)*r(i,j)*1.6d-12*q12sex*expa(-ae(km)/tk)*dv(km)*
     2    ae(km)/(dv(km)+ne*q12sex*(expa(-ae(km)/tk)+gg(km)))
 1081 if(ae(km)/tk.gt.90.d0)  pert=0.d0
      if((ae(km).gt.13.6d0).or.(ae(km).lt.1.54d0.and.ae(km).gt.0.1d0)
     1    .or.(ae(km).lt.0.41d-2)) go to 81
      kobs=kobs+1
      iobs(kobs)=i
      jobs(kobs)=j
      pobs(kobs)=pert
      wlobs(kobs)=1.24d4/ae(km)                                             !lo
   90 continue
c pertes par recombinaisons dielectroniques
   81 p(i,j)=p(i,j)+pert
   76 continue
        if(imp.lt.5.or.imp.ge.8) goto 351
c        write(10,173)
  173 format(/40x,'pertes raies interdites/(Ne*H*Nh) , sf at. neutres') 
c      do 83 j=1,nelem
c      iz0=iz(j)                                                       
c   83 write(10,112) iz0,(p(i,j),i=1,iz0)
  112 format (1x,i2,16(1pe8.1))
  351 continue
      pt=0.d0
      do 84 j=1,nelem
      iz0=iz(j)
      do 84 i=1,iz0
      pt=pt+p(i,j)
   84 p(i,j)=p(i,j)*nht*ne
      ptn=pt*nht*ne
c        if(imp.ge.2) write(10,7172) pt,ptn
 7172 format(10x,'total=',1pe12.3,5x,'total en erg/cm3-sec =',1pe12.3)
      pinti=pt
      pintin=ptn
c
c     boucle raies interdites    3 niveaux
c     i  sequence   =1 ci, =2 ni, =3 oi, =4  si  ,=5 s  (donnees czyzak
c     et al.) , =6 ci, =7 oi (donnees saraph et al.)
c     j  element (=1 n, =2 o, =3 ne  etc.)
c k  transition (=1 2-3, =2  1-3, =3  1-2)
c        if(imp.eq.3) write(10,190)                                     
  190 format(/40x,'raies int. ions a 3 niveaux'/2('seq.',3x,'n',8x,
     1'o',7x,'ne',7x,'mg',7x,'si',8x,'s',6x,'total  '))
  356 continue
c ATTENTION cette boucle 1 PAS MODIFIEE quand i=etat d'ionisation et j=element
      tpert3=0.d0
      j1=nelem-3
      do 1 i=1,7
      do 9 j=1,7
    9 pert3(j)=0.d0
      do 8 j=1,j1
c      supprimer a partir du mg   j=4
      if(j.lt.4) go to 1111
      pert3(j)=0.d0
      pert3(7)=0.d0
      go to 8
 1111 continue                                                              !lo
      numion=iz(j+3)-nbrele(i)
      if(ae3(i,j,1).le.0.d0) goto 8
    2 continue
      do 3 k=1,3
         if(k.eq.1) then
            k1=3
            k2=2
         else if(k.eq.2) then
            k1=3
            k2=1
         else if(k.eq.3) then
            k1=2
            k2=1
         endif
    7 continue
      qr(k)=8.63d-6/VT*om3(i,j,k)/g3n(i,k1)*ne                         
      fr(k)=8.63d-6/VT*om3(i,j,k)/g3n(i,k2)*expa(-ae3(i,j,k)/tk)*ne
      if(ae3(i,j,k)/tk.gt.90.d0)  fr(k)=0.d0
      gr(k)=qr(k)+dv3(i,j,k)
    3 continue
      denom=(gr(3)+fr(1))*(gr(2)+fr(2))+fr(3)*(gr(1)+gr(2)+fr(1))+gr(1)
     1*(gr(3)+fr(2))
      popn(2)=(fr(3)*(gr(1)+gr(2))+fr(2)*gr(1))/denom
      popn(3)=(fr(3)*fr(1)+fr(2)*(gr(3)+fr(1)))/denom
      tr32=r(j+3,numion)*ab(j+3)/ne*1.6d-12*popn(3)*
     1dv3(i,j,1)*ae3(i,j,1)
      tr31=r(j+3,numion)*ab(j+3)/ne*1.6d-12*popn(3)*
     1dv3(i,j,2)*ae3(i,j,2)
      tr21=r(j+3,numion)*ab(j+3)/ne*1.6d-12*popn(2)*
     1dv3(i,j,3)*ae3(i,j,3)
      pert3(j)=tr32+tr31+tr21
      pert3(7)=pert3(7)+pert3(j)                                            !lo
      kint=kint+1
      iint(kint)=j+3
      jint(kint)=numion
      wl32(kint)=1.24d4/ae3(i,j,1)
      wl31(kint)=1.24d4/ae3(i,j,2)
      wl21(kint)=1.24d4/ae3(i,j,3)
      rin32(kint)=tr32
      rin31(kint)=tr31
      rin21(kint)=tr21
    8 continue
      tpert3=tpert3+pert3(7)
c        if(imp.ge.2.and.mod(i,2).eq.1) write(10,191) i,(pert3(j),j=1,7)
c        if(imp.ge.2.and.mod(i,2).eq.0) write(10,192) i,(pert3(j),j=1,7)
  191 format(i3,7(1pe9.2))
  192 format('+',65x,i3,7(1pe9.2))
    1 continue                                                         
c
      pert3n=tpert3*nht*ne
c        if(imp.eq.3) write(10,7172)tpert3,pert3n
        pdie=0.d0
c
c  sommation des pertes 
      praieint = pinti + tpert3                 ! (pertes par unite de volume)
      if(r(1,1).gt.0.d0) go to 3618
      WRITE(10,3619) r(1,1),m,nitd,nitt,kitt,nitf
 3619 format(' dans loss r(1,1)=',1pe15.3,' pour m=',i3,' nitd=',i3,
     &                           ' nitt=',i3,' kitt=',i2,' nitf=',i3)
 3618 continue
        if(imp.lt.5.or.imp.gt.7) go to 354
c      write(10,183)
  183 format(/,5x,'raies interdites 2 niveaux'
     1/4('  el. ion lambda  int./NeHNh'))                                   !lo
c      write(10,182)(iobs(k),jobs(k),wlobs(k),pobs(k),k=1,kobs)
c      write(10,185)
  182 format(4(2i4,0pf10.0,1pe10.2))
  185 format(/5x,'raies interdites 3 niveaux'/2('  el. ion   w132',  
     1 5x,'rin32',5x,'wl31',5x,'rin31',5x,'wl21',5x,'rin21  '))
c        do 5185 k=1,kint
c        if(mod(k,2).eq.1) write(10,186) iint(k),jint(k),wl32(k),
c    1rin32(k),wl31(k),rin31(k),wl21(k),rin21(k)
c        if(mod(k,2).eq.0) write(10,187) iint(k),jint(k),wl32(k),
c    1rin32(k),wl31(k),rin31(k),wl21(k),rin21(k)
c 5185        continue 
  186 format(2i4,3(1pe10.2,1pe9.2))
  187 format('+',65x,2i4,3(1pe10.2,1pe9.2))
  354 continue
      if(kitt.ge.1.and.imp.ge.5.and.imp.le.7.and.ifich.ge.3.and.ifich
     &   .le.8.and.kimpm.eq.1.and.kimpf2.eq.1) WRITE(16,176) praieint
  176 format (' pertes raies interdites=',1pe10.3)
      return
  111 format(10(1pe12.3))
      end                                                                   !lo
c-------------------------------------------------------------------------
       function e1(x)
c
       implicit double precision (a-h,o-z)
       if(x.eq.0.d0) then
          e1=1.d+75
       else if(x.lt.-4.d0) then
          e1=0.d0
       WRITE(6,*)'dans le calcul du gaunt de neutre avec E1: E/kT<-4.'
       else if(x.gt.4.d0) then
          a=4.d0/x
          e1=expa(-x)*((((((((.00094427614d0*a-.0049362007d0)*a
     1  +.011723273d0)*a-.017555779d0)*a+.020412099d0)*a-.022951979d0)*a
     2  +.031208561d0)*a-.062498588d0)*a+.24999999d0)*a
       else   ! -4<x<4
          e1=-log(abs(x))-(((((((((((((.10317602d-11*x-.15798675d-10)*x
     1  +.16826592d-9)*x-.21915699d-8)*x+.27635830d-7)*x-.30726221d-6)*x
     2  +.30996040d-5)*x-.28337590d-4)*x+.23148392d-3)*x-.16666906d-2)*x
     3  +.010416662d0)*x-.055555520d0)*x+.25d0)*x-1.0d0)*x-.57721566d0
       endif
       return
       end                                                                  !e1
c--============================================================================
c...........FPROF............... profil de raie : approximation de Voigt 
        function FPROF(lf,am,x,cx)
c
c x=(nu-nu0)/dnut  dnut=sqrt(dnudoppler**2+dnuturb**2)
c ce profil de raie est normalise a 1 au centre de la raie si am<1
c   cad qu'il faut diviser par Vpidnut pour que l'integrale du profil = 1
c l'integration en nu se fait par la methode de Gauss-Legendre en 15 points yl
c   determines (voir Numerical recipes p122) avec un changement de variable
c   nul-nu0=u0*aint*(1/yl**3-1) 
c avec u0=uhz(kr)=largeur doppler de chaque raie a T=Tref(Tinit/10 maintenant)
c   ou u0=sqrt(dnu-dopplerthermique(Tref)**2 + dnu-turbulence**2) si turbulence
c
       implicit double precision (a-h,o-z)

c             QUADRATURE DE GAUSS a  2*15 points
         dimension y(15),p(15)
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/profr2/fgauss(15)
       common/temp/T,VT,tk,tk1,templog
       data y/ 0.9968934841,0.9836681233,0.9600218650,0.9262000474,
     & 0.8825605358,0.8295657624,0.7677774321,0.6978504948,0.6205261830,
     & 0.5366241481,0.4470337695,0.3527047255,0.2546369262,0.1538699136,
     & 0.0514718426/
       data p/ 0.0079681925,0.0184664683,0.0287847079,
     & 0.0387991926,0.0484026728,0.0574931562,0.0659742299,0.0737559747,
     & 0.0807558952,0.0868997872,0.0921225222,0.0963687372,0.0995934206,
     & 0.1017623897,0.102852652/
c       data nf/15/      mis dans le main pour l'idris
c
                        if(lf.eq.1000) then     ! appele dans le main une fois
        do l=1,nf
           fint(l)=p(l)/y(l)**4 *aint*6.d0
        enddo
                        else if(lf.eq.0) then   ! appele dans datom a chaque T
        tt=1.d0
        if(vturbkm.lt.1.d-3) tt=VTref/VT                             ! u0/dnut
        do l=1,nf
           x=aint *(1.d0/y(l)**3 - 1.d0) *tt      ! x=(nu-nu0)/dnut si vturb=0
           fx(l)=x
           fgauss(l)=expa(-x*x)
        enddo
c
                        else  ! lf=1 a nf         appele dans sstrate
        x=fx(lf)*cx
                if(iprof.ge.2) then           ! turbulence ou Doppler thermique
        FPROF=fgauss(lf)
        return
        endif
                if(am.lt.1.d0) then                       ! approximation Voigt
        FPROF=fgauss(lf)
        if(x.gt.1.d0) FPROF=fprof + am/(am*am+x*x) /1.77245385091d0       ! Vpi
                else        ! am>1                          ! amortissement pur
        FPROF=1.d0/(1.d0+(x/am)**2)
c             FPROF=am/(am*am+x*x) /1.77245385091d0    ! Vpi - profil normalise
                endif        ! am/1
                        endif        ! lf
        return
        end                                                             !fprof
c------------------------------------------------------------------------------
c...........FPROF............... profil de raie : approximation de Voigt 
        function FPROF0(lf,am,x)
c
c ce profil de raie est normalise a 1 au centre de la raie si am<1
c   cad qu'il faut diviser par Vpidnudoppler pour que l'integrale du profil = 1
c l'integration en nu se fait par la methode de Gauss-Legendre en 15 points yi
c   determines (voir Numerical recipes p122) avec un changement de variable
c   nu-nu0=u0*aint*(1/y-1) 
c avec u0=uhz(kr)=largeur doppler de chaque raie a T=Tref(Tinit/10 maintenant)
c
       implicit double precision (a-h,o-z)

c              QUADRATURE DE GAUSS a  2*15 points
         dimension y(15),p(15)
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/profr2/fgauss(15)
       common/temp/T,VT,tk,tk1,templog
       data y/ 0.9968934841,0.9836681233,0.9600218650,0.9262000474,
     & 0.8825605358,0.8295657624,0.7677774321,0.6978504948,0.6205261830,
     & 0.5366241481,0.4470337695,0.3527047255,0.2546369262,0.1538699136,
     & 0.0514718426/,          p/0.0079681925,0.0184664683,0.0287847079,
     & 0.0387991926,0.0484026728,0.0574931562,0.0659742299,0.0737559747,
     & 0.0807558952,0.0868997872,0.0921225222,0.0963687372,0.0995934206,
     & 0.1017623897,0.102852652/
c
                        if(lf.eq.1000) then
        do l=1,nf
        fint(l)=p(l)/y(l)/y(l) *aint*2.d0
        enddo
                        else if(lf.eq.0) then
           if(iprof.le.2) then
        tt=VTref/VT
           else
           tt=0.5d0
           endif
        do l=1,nf
        x=aint*(1.d0/y(l)-1.d0) *tt
        fx(l)=x
        fgauss(l)=expa(-x*x)
        enddo
                        else  ! lf=1 a nf
c
        x=fx(lf)
                if(iprof.ge.2) then           ! turbulence ou Doppler thermique
        FPROF0=fgauss(lf)
        return
        endif
                if(am.lt.1.) then                         ! approximation Voigt
        FPROF0=fgauss(lf)
        if(x.gt.1.) FPROF0=fprof0 + am/(am*am+x*x) /1.77245385091d0       ! Vpi
                else        ! am>1                          ! amortissement pur
        FPROF0=am/(am*am+x*x) /1.77245385091d0                            ! Vpi
                endif        ! am/1
                        endif        ! lf
        return
        end

c AUTRES NOMBRES DE POINTS DE LA QUADRATURE DE GAUSS
c       QUADRATURE DE GAUSS a  2*5 points
c        dimension y(5),p(5)
c        data y/0.9739065285,0.8650633666,0.6794095682,0.4333953941,
c     &           0.1488743389/, p/0.0666713443,0.1494513491,0.2190863625,
c     &           0.2692667193,0.2955242247/
c        data nf/5/
c       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
c       common/profr2/fgauss(15)
c
c       QUADRATURE DE GAUSS a 2*10 points
c        dimension y(10),p(10)
c        data y/0.9931285992,0.9639719273,0.9122344283,0.8391169718,
c     & 0.7463319065,0.6360536807,0.5108670020,0.3737060887,0.2277858511,
c     & 0.0765265211/
c        data p/0.0176140071,0.0406014298,0.0626720483,0.0832767416,
c     & 0.1019301198,0.1181945320,0.1316886384,0.1420961093,0.1491729865,
c     & 0.1527533871/
c  changement de variable aint=   25.
c 0.172974 0.934365 2.405236 4.793224 8.497161 14.304859 23.936416 41.897492
c 84.752207   301.684130
c log -0.762 -0.0295 0.3812 0.6806 0.9293 1.1555 1.37906 1.6222 1.92815 2.47955
c        data nf/10/
c       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
c       common/profr2/fgauss(15)
c
c       QUADRATURE DE GAUSS a  2*15 points
c        dimension y(15),p(15)
c        data y/ 0.9968934841,0.9836681233,0.9600218650,0.9262000474,
c     & 0.8825605358,0.8295657624,0.7677774321,0.6978504948,0.6205261830,
c     & 0.5366241481,0.4470337695,0.3527047255,0.2546369262,0.1538699136,
c     & 0.0514718426/,          p/0.0079681925,0.0184664683,0.0287847079,
c     & 0.0387991926,0.0484026728,0.0574931562,0.0659742299,0.0737559747,
c     & 0.0807558952,0.0868997872,0.0921225222,0.0963687372,0.0995934206,
c     & 0.1017623897,0.102852652/
c        data nf/15/
c       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
c       common/profr2/fgauss(15)
c  changement de variable aint=   15.
c 0.046743 0.249046 0.624644 1.195205 1.996001 3.081749 4.536912 6.494575
c 9.173033 12.952525 18.554512 27.528492 43.907403 82.484945 276.421470
c
c       QUADRATURE DE GAUSS a  2*25 points
c        dimension y(25),p(25)
c        data y/0.9988664044,0.9940319694,0.9853540840,0.9728643851,
c     & 0.9566109552,0.9366566189,0.9130785567,0.8859679795,0.8554297694,
c     & 0.8215820709,0.7845558329,0.7444943022,0.7015524687,0.6558964657,
c     & 0.6077029272,0.5571583045,0.5044581449,0.4498063350,0.3934143119,
c     & 0.3355002454,0.2762881938,0.2160072369,0.1548905900,0.0931747016,
c     & 0.0310983383/
c        data p/0.0029086226,0.0067597992,0.0105905484,0.0143808228,
c     & 0.0181155607,0.0217802432,0.0253606736,0.0288429936,0.0322137282,
c     & 0.0354598356,0.0385687566,0.0415284631,0.0443275043,0.0469550513,
c     & 0.0494009384,0.0516557031,0.0537106219,0.0555577448,0.0571899256,
c     & 0.0586008498,0.0597850587,0.0607379708,0.0614558996,0.0619360674,
c     & 0.0621766167/
c  changement de variable aint=   15.
c 0.017023 0.090058 0.222954 0.418387 0.680356 1.014407 1.427940 1.930634 
c 2.535046 3.257458 4.119098 5.147904 6.381152 7.869463 9.683113 11.922330
c 14.734875 18.347685 23.127744 29.709356 39.291136 54.442118 81.842552
c 145.987905 467.340884
c        data nf/25/
c       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
c       common/profr2/fgauss(15)
c
c------------------------------------------------------------------------------
c....FPROF1 pour la raie kr=20 de HeII profil de raie : approximation de Voigt
c PAS UTILISE
        function FPROF1(lf,am,x)
c
       implicit double precision (a-h,o-z)

c       QUADRATURE DE GAUSS a  2*15 points
c        dimension y(15),p(15)
       dimension y(15)
       common/profr/VTref,aint,vturbkm,fint(15),fx(15),vth(10),nf,iprof
       common/profr2/fgauss(15)
       common/temp/T,VT,tk,tk1,templog
c            data y/ 0.9968934841,0.9836681233,0.9600218650,0.9262000474,
c     & 0.8825605358,0.8295657624,0.7677774321,0.6978504948,0.6205261830,
c     & 0.5366241481,0.4470337695,0.3527047255,0.2546369262,0.1538699136,
c     & 0.0514718426/,          p/0.0079681925,0.0184664683,0.0287847079,
c     & 0.0387991926,0.0484026728,0.0574931562,0.0659742299,0.0737559747,
c     & 0.0807558952,0.0868997872,0.0921225222,0.0963687372,0.0995934206,
c     & 0.1017623897,0.102852652/
            data y/ 0.9968934841,0.9836681233,0.9600218650,0.9262000474,
     & 0.8825605358,0.8295657624,0.7677774321,0.6978504948,0.6205261830,
     & 0.5366241481,0.4470337695,0.3527047255,0.2546369262,0.1538699136,
     & 0.0514718426/
c
        tt=VTref/VT
        x=aint*(1.d0/y(lf)-1.d0) *tt *5.d0
                if(am.lt.1.) then                       ! approximation Voigt
        FPROF1=expa(-x*x)
        if(x.gt.1.) FPROF1=fprof1 + am/(am*am+x*x) /1.77245385091d0       ! Vpi
                else    ! am>1                          ! amortissement pur
        FPROF1=am/(am*am+x*x) /1.77245385091d0                            ! Vpi
                endif   ! am/1
        return
        end
c--============================================================================
        subroutine GRAF(resol,Tmoy)

c       pour presenter mieux les graphes, continu et raies ensemble :
c - on rajoute quelques points pour exp(-dhnu/kT) si kT petit
c       seulement si le flux est plus grand apres le bord d'ionisation qu'avant
c       ainsi que 2 pts de chaque cote des raies
c - en dessous de R=400, on calcule par extrapolation tous les
c       points correspondant a une resolution 5 fois superieure a celle 
c       demandee, puis on convole par une gaussienne 
c - au dessus, on trie et somme les points de meme frequence seulement
c       => nuF(nu)=f(hnuev), hnuev ordonnes pour graphique
c                 R<=100 ->230pts/decade soit ~1500pts de 0.1 a 1e5ev
c                 R =200 ou 400 -> 6882pts de 0.01ev a 25kev
c                 R >400 -> nbre de pts variable
c
       implicit double precision (a-h,o-z)
       parameter (nzs=999,nrais=4200,ndirs=20)
         character tab*1,sep*1,nomion*7,nomrai*7
         dimension hc(500),ec(500),sc(500),rc(500),tc(500),p(4001),
     &         s(nrais*3),r(nrais*3),eo(38000),so(38000),ro(38000),
     &         to(38000),soc(38000),roc(38000),toc(38000)
         dimension iw(38000),lnuc(500),nk(500),lnur(nrais*3),
     &         lo(38000),kco(38000),kro(38000),kas(38000)
       common/tabm/exflux(322),thnur(nrais)
       common/transf/him(322),debim(322),tjc(322,nzs),
     &        ciplus(322),pciplus(322),
     &     riplus(16,nrais),priplus(16,nrais),rimoins(16,nrais,0:nzs+2) !234567
       common/degmt/hautsom,dtaucmax,hnumaxto,
     &           p_grait,p_gfree,p_gbound,g_pcompt,ptot,
     &           restflux(322),rtaucef(322),ttaucef(322),rtaucat(322)
       common/photon/thnuc(322),tde(220),tdeb(220),ict(102),jct(102)
       common/mgrpal/tflincident,fsor(2,nrais),frer(2,nrais),
     &               fasor(ndirs,nrais),farer(ndirs,nrais)
       common/raies/pmat(10),nrai(27,10),alo(nrais),fdo(nrais),fdolim,
     &        pfluo(27),efluo(27),kcr(nrais),nrcum(0:27,10),kam(nrais)
       common/nom/nomion(322),nomrai(0:nrais)
       common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
       common/nbr1/nelem,jmax,iz(10),nraimax,nraimax2,nraissfek,nuta,ns
       common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
       common/cst1/pi,pi4,pi2v3,Vpi,V3,tab
       common/mgcpal/cimoinsh(322),ciplush(322),cimoins0(322),
     &               fcra(ndirs,322),fcsa(ndirs,322)
c graf
c        tab=';'
c        sep=';'
        if(ila.eq.0) then
           sep=';'
        else
           sep=' '
        endif
c Tmoy est utile pour les edges des fe19a21 & si13-14 (1450-2750ev)
        tk=8.6171d-5*Tmoy       ! Tmoy=temp(npt/2)             k/e'
        ntab=nrais              ! ATTENTION au nbre total de raies
        do k=1,500
           lnuc(k)=0
        enddo
        do k=1,ntab*3
           lnur(k)=0
           s(k)=0.d0
           r(k)=0.d0
           enddo
        do k=1,38000
           lo(k)=0
        enddo
        if(resol.gt.400.d0) goto 1000 
c
c                    CAS D'UNE FAIBLE RESOLUTION DEMANDEE
c-----------------------  profil des raies gaussien                         !g1
c TRAITEMENT DU CONTINU
c
c        res=0.0005d0   ! avant dec 20009  ! 4606 pts par decade => 29550 pts
           res=0.2d0/resol           ! =dnutravail/nu dnutravail=dnudemande/5
        WRITE(30,80) resol
 80    format('#  resolution=',0pf10.1)
                        do 15 kc=1,322
        hnuev=thnuc(kc)
        ec(kc)=exflux(kc)*hnuev*2.41797d14
        sc(kc)=ciplush(kc)*pi*hnuev*2.41797d14
        rc(kc)=cimoins0(kc)*pi*hnuev*2.41797d14
        tc(kc)=restflux(kc)*hnuev*2.41797d14
        nk(kc)=kc
        if(kc.le.220) then
           lnuc(kc)=nint((log10(hnuev)+2.d0)*2.302585d0/res)
           else
           lnuc(kc)=nint((log10(hnuev)+2.d0)*2.302585d0/res)-1
           endif
 15                     continue   ! kc
        lomin=nint((log10(hnumin)+2.d0)*2.302585d0/res)
 81     format(0pf10.5,a,i4,5(a,1pe10.3))
c-----------------------
c tri du continu brut
        n=322
        call INDEXTRI(n,lnuc,iw)
c        write(50,*) '1) continu juste apres tri   i,kc,hnu,lnu,rc'   !
c        do i=1,n                                                     !
c        kc=iw(i)                                                     !
c        write(50,*) i,kc,thnuc(kc),lnuc(kc),rc(kc)                   !
c        enddo                                                        !
c-----------------------
c rajout de points dans le continu - remplissage des trous
        j=0                                                                 !g1
                        do 100 i=1,322
        kc=iw(i)
          if(lnuc(kc).le.0) goto 100
        if(i.lt.322) then
           k1=iw(i+1)
           n=lnuc(k1)-lnuc(kc)
           endif   ! i<322
        if(kc.eq.184) then
           k1=iw(i-1)
           n=(lnuc(kc)-lnuc(k1))/2-1
           endif   ! fin
        dhnu=thnuc(kc)*res
        nkt=nint(tk*2.d0/dhnu)
        if(n.eq.0) goto 100                             ! bords proches
c
        if(kc.eq.190) then                              !discontinuite Balmer
        j=j+1                                 ! interpolation entre 190 et 189
        ll=lnuc(kc)                           ! avec la pente 191-190
        ee=ec(kc)
        ss=sc(kc)
        rr=rc(kc)
        tt=tc(kc)                                                          !g1
        lo(j)=ll
        eo(j)=ee
        so(j)=ss
        ro(j)=rr
        to(j)=tt
        kco(j)=kc
        kas(j)=40
        if(incid.eq.1.and.ll.lt.lomin) eo(j)=0.d0
        if(incid.eq.1.and.ll.lt.lomin) ee=0.d0
c        if(incid.eq.17.and.ll.lt.lomin) eo(j)=0.d0         !Agata
c        if(incid.eq.17.and.ll.lt.lomin) ee=0.d0
c        if(incid.eq.18.and.ll.lt.lomin) eo(j)=0.d0
c        if(incid.eq.18.and.ll.lt.lomin) ee=0.d0
          if(n.le.1) goto 100
        de=(ee-ec(189))/dfloat(lnuc(190)-lnuc(189))
        ds=(ss-sc(191))/dfloat(lnuc(190)-lnuc(191))
        dr=(rr-rc(191))/dfloat(lnuc(190)-lnuc(191))
        dt=(tt-tc(191))/dfloat(lnuc(190)-lnuc(191))                         !g1
c        de=(ee-ec(191))/dfloat(n)
c        ds=(ss-sc(191))/dfloat(n)
c        dr=(rr-rc(191))/dfloat(n)
c        dt=(tt-tc(191))/dfloat(n)
          do m=1,n-1
          j=j+1
          ll=ll+1
          ee=ee+de
          ss=ss+ds
          rr=rr+dr
          tt=tt+dt
          lo(j)=ll
          eo(j)=ee
          so(j)=ss
          ro(j)=rr
          to(j)=tt
          kco(j)=0
          kas(j)=41
          if(incid.eq.1.and.ll.lt.lomin) eo(j)=0.d0
c          if(incid.eq.17.and.ll.lt.lomin) eo(j)=0.d0      !Agata
c          if(incid.eq.18.and.ll.lt.lomin) eo(j)=0.d0
          enddo
        goto 100
        endif   ! kc=190
        if(kc.eq.189) then                              !discontinuite Balmer
        j=j+1                                 ! interpolation entre 189 et 188
        lo(j)=lnuc(kc)                                ! avec la pente 188-187
        so(j)=sc(kc)
        ro(j)=rc(kc)
        to(j)=tc(kc)
        eo(j)=ec(kc)
        kco(j)=kc
        kas(j)=42                                                          !g1
          if(n.le.1) goto 100
        de=(ec(189)-ec(188))/dfloat(lnuc(189)-lnuc(188))
        ds=(sc(187)-sc(188))/dfloat(lnuc(187)-lnuc(188))
        dr=(rc(187)-rc(188))/dfloat(lnuc(187)-lnuc(188))
        dt=(tc(187)-tc(188))/dfloat(lnuc(187)-lnuc(188))
        ll=lnuc(189)
        ee=ec(189)
        ss=sc(188)-ds*dfloat(n)
        rr=rc(188)-dr*dfloat(n)
        tt=tc(188)-dt*dfloat(n)
          do m=1,n-1
          j=j+1
          ll=ll+1
          ss=ss+ds
          rr=rr+dr
          tt=tt+dt
          ee=ee+de
          lo(j)=ll
          so(j)=ss
          ro(j)=rr
          to(j)=tt
          eo(j)=ee
          kco(j)=0
          kas(j)=43
          enddo                                                             !g1
        goto 100
        endif   ! kc=189
c
             if(kc.le.102.and.nkt.le.1) then                ! kT tres petit
             j=j+1                                ! reduction du pic par kT/dnu
             lo(j)=lnuc(kc)
                if((sc(kc)-sc(kc+220)).gt.0.d0) then      ! bord en emission
             ss=sc(kc+220)
             so(j)=ss + (sc(kc)-ss)*tk/dhnu
                else                                      ! bord en absorption
                ss=sc(kc)
                so(j)=ss
                endif
                if((rc(kc)-rc(kc+220)).gt.0.d0) then      ! bord en emission
             rr=rc(kc+220)
             ro(j)=rr + (rc(kc)-rr)*tk/dhnu
                else                                      ! bord en absorption
                rr=rc(kc)
                ro(j)=rr
                endif
             ee=ec(kc)
             tt=tc(kc)
             eo(j)=ee
             to(j)=tt
             kco(j)=kc
             kas(j)=10                                                      !g1
             if(n.le.1) goto 100
             j=j+1
             ll=lnuc(kc)+1
             lo(j)=ll
             eo(j)=ee
             so(j)=ss
             ro(j)=rr
             to(j)=tt
             kco(j)=0
             kas(j)=11
             if(n.le.2) goto 100
             de=(ec(k1)-ee)/dfloat(n-1)
             ds=(sc(k1)-ss)/dfloat(n-1)
             dr=(rc(k1)-rr)/dfloat(n-1)
             dt=(tc(k1)-tt)/dfloat(n-1)
               do m=1,n-2
               j=j+1
               ll=ll+1
               ee=ee+de
               ss=ss+ds
               rr=rr+dr
               tt=tt+dt
               lo(j)=ll
               eo(j)=ee
               so(j)=ss
               ro(j)=rr
               to(j)=tt
               kco(j)=0
               kas(j)=12
               enddo                                                        !g1
             goto 100
             endif
c
c il faut rajouter ee=0 si hnuev<hnumin, ici ca interpole
        if((kc.gt.102).or.n.lt.nkt) then       ! kc > 102 ou nusuivant-nu < 2kT
        j=j+1                                                   ! cas normal
        ll=lnuc(kc)                  ! interpolation entre les 2 nu consecutifs
        ee=ec(kc)
        ss=sc(kc)
        rr=rc(kc)
        tt=tc(kc)
        lo(j)=ll
        eo(j)=ee
        so(j)=ss
        ro(j)=rr
        to(j)=tt
        kco(j)=kc
        kas(j)=20
          if(i.eq.322) jcmax=j
          if(i.eq.322) goto 100
          if(n.le.1) goto 100
        de=(ec(k1)-ee)/dfloat(n)
        ds=(sc(k1)-ss)/dfloat(n)
        dr=(rc(k1)-rr)/dfloat(n)
        dt=(tc(k1)-tt)/dfloat(n)                                            !g1
          do m=1,n-1
          j=j+1
          ll=ll+1
          ee=ee+de
          ss=ss+ds
          rr=rr+dr
          tt=tt+dt
          lo(j)=ll
          eo(j)=ee
          so(j)=ss
          ro(j)=rr
          to(j)=tt
          kco(j)=0
          kas(j)=21
          if(incid.eq.1.and.ll.lt.lomin) eo(j)=0.d0
c          if(incid.eq.17.and.ll.lt.lomin) eo(j)=0.d0       !Agata
c          if(incid.eq.18.and.ll.lt.lomin) eo(j)=0.d0
          enddo   ! m
          if(kc.eq.184) jcmax=j
       goto 100
       endif   ! interpolation
c est utile pour les edges des fe19a21 & si13-14 (1450-2750ev)
       if((kc.le.102).and.n.ge.nkt) then                 ! nusuivant-nu > 2kT
                  j=j+1                      ! rajout de points en exp(-dnu/kt)
                  ll=lnuc(kc)
                  sfond=sc(kc+220)
                  rfond=rc(kc+220)
                  kas(j)=30                                                 !g1
                  k2=min(iw(i-1),iw(i-2))
       if(k2.le.102.and.nkt.gt.(lnuc(kc)-lnuc(k2)).and.
     &        sc(k2).gt.sc(k2+220).and.sc(kc+220).gt.sc(k2+220))
     &        sfond=sc(k2+220) +
     & (sc(k1)-sc(k2+220))*(thnuc(kc)-thnuc(k2))/(thnuc(k1)-thnuc(k2))
       if(k2.le.102.and.nkt.gt.(lnuc(kc)-lnuc(k2)).and.
     &        rc(k2).gt.rc(k2+220).and.rc(kc+220).gt.rc(k2+220))
     &        rfond=rc(k2+220) +
     & (rc(k1)-rc(k2+220))*(thnuc(kc)-thnuc(k2))/(thnuc(k1)-thnuc(k2))
       if(k2.le.102.and.nkt.gt.(lnuc(kc)-lnuc(k2))) kas(j)=31
                  dse=sc(kc)-sfond
                  dre=rc(kc)-rfond
                  lo(j)=ll
                  eo(j)=ec(kc)
                  so(j)=sc(kc)
                  ro(j)=rc(kc)
                  to(j)=tc(kc)
                  kco(j)=kc
                    if(n.le.1) goto 100
                  nn=min(n-1,nkt*2)
                  de=(ec(k1)-ec(kc))/dfloat(n)
                  ds=(sc(k1)-sfond)/dfloat(n)
                  dr=(rc(k1)-rfond)/dfloat(n)
                  dt=(tc(k1)-tc(kc))/dfloat(n)
                  ee=ec(kc)                                                 !g1
                  tt=tc(kc)
                        if(dse.gt.0.d0) then
                  exs=expa(-dhnu/tk)
                  ss=sfond
                        else
                        ss=sc(kc)
                        dse=0.d0
                        exs=1.d0
                        endif
                        if(dre.gt.0.d0) then   ! bord en emission
                  exr=expa(-dhnu/tk)
                  rr=rfond
                        else                   ! bord en absorption
                        rr=rc(kc)
                        dre=0.d0
                        exr=1.d0
                        endif
                    do m=1,nn
                    j=j+1
                    ll=ll+1
                    lo(j)=ll
                    dse=dse*exs
                    dre=dre*exr
                    ee=ee+de
                    ss=ss+ds
                    rr=rr+dr
                    tt=tt+dt
                    eo(j)=ee
                    so(j)=ss+dse
                    ro(j)=rr+dre
                    to(j)=tt
                    kco(j)=kc
                    kas(j)=32
                    enddo
                  if(n.le.nn+1) goto 100
                    do m=nn+1,n-1                                           !g1
                    j=j+1
                    ll=ll+1
                    ee=ee+de
                    ss=ss+ds
                    rr=rr+dr
                    tt=tt+dt
                    lo(j)=ll
                    eo(j)=ee
                    so(j)=ss
                    ro(j)=rr
                    to(j)=tt
                    kco(j)=0
                    kas(j)=33
                    enddo
                  goto 100
                  endif
 100                     continue
c        WRITE(6,*) ' nombre de points (continu) en frequence=',jcmax
        if(jcmax.gt.30000) then                   ! 29259 pour R=400
        write(6,*) ' trop de pts continus rajoutes - STOP'
        write(6,*) ' REDIMENSIONNER LES TABLEAUX'
        endif
c-----------------------
c        write(50,*) '2) continu tous les pts   cas,j,kc,lo,ro,hnu'      !
c        do j=1,jcmax                                                    !
c        hnuev=10.d0**(res*dfloat(lo(j))/2.302585d0 - 2.d0)              !
c        if(kco(j).ne.0) write(50,*) kas(j),j,kco(j),lo(j),ro(j),hnuev,  !
c     &                             thnuc(kco(j))                        !
c        if(kco(j).eq.0) write(50,*) kas(j),j,kco(j),lo(j),ro(j),hnuev   !
c        enddo                                                           !
c-----------------------
c
c TRAITEMENT DES RAIES                                                    !g1
c
        if(ila.eq.0) then
           WRITE(14,*) 'RAIES: continu sous-jacent (nuFcont)'//
     &     ' & largeurs equivalentes (eV & Angstrom)'
           WRITE(14,91)
 91        format('     hnuraie  ;ion    ;kr ;nuFincid;nuFcont-sor;',
     &     'nuFcont-ref  ;Wev-sort ;Wev-refl  ;WA-sort   ;WA-refl;')
        else
           WRITE(14,*) ' LINES: underlying continuum (nuFcont)'//
     &     ' and equivalent widths (eV & Angstrom)'
           WRITE(14,*)'    line-hnu  ion      kr  nuFincid  nuFcont-'//
     &     'out nuFcont-ref Wev-out    Wev-ref    WA-out     WA-ref'
        endif
c        write(50,*) '3) rajout de pts raies   hnu,kr,lnu,r,lnu,r,lnu,r'  !
                        do 25 kr=2,nraimax
        alambda=alo(kr)
                 if(fdo(kr).lt.fdolim) goto 25
        hnuev=thnur(kr)
        sort=fsor(1,kr)
        refl=frer(1,kr)
c
c calcul et ecriture des largeurs equivalentes
             if(kr.ge.6.and.kr.le.12) then
             if(kr.eq.6) goto 25
             if(kr.eq.7) then
             has=sort
             har=refl
             goto 25
             else if(kr.eq.8) then
             hbs=sort
             hbr=refl
             goto 25
             else if(kr.eq.9) then
             hgs=sort
             hgr=refl
             goto 25
             else if(kr.eq.10) then
             sort=has+sort
             refl=har+refl
             else if(kr.eq.11) then
             sort=hbs+sort
             refl=hbr+refl                                                  !g1
             else if(kr.eq.12) then
             sort=hgs+sort
             refl=hgr+refl
             endif
             endif   ! kr=6 a 12 (hydrogene)
                kf=nrcum(26,10)
                if(kr.gt.kf.and.kr.le.(kf+11)) then
                if(kr.eq.(kf+1)) then
                soms=sort
                somr=refl
                goto 25
                else if(kr.gt.(kf+1).and.kr.lt.(kf+11)) then
                soms=soms+sort
                somr=somr+refl
                goto 25
                else   ! kr=kf+11
                sort=soms+sort
                refl=somr+refl
                endif
                endif   ! raies de fluorescence K du fer
        kc=kcr(kr) 
        ss=sc(kc)
        rr=rc(kc)
        wevs=0.d0
        wevr=0.d0
        was=0.d0
        war=0.d0
        if(ss.gt.0.d0) wevs=hnuev*sort/ss
        if(rr.gt.0.d0) wevr=hnuev*refl/rr
        if(ss.gt.0.d0) was=alambda*sort/ss
        if(rr.gt.0.d0) war=alambda*refl/rr
c        if(abs(ss).le.1.d-36) ss=1.111d-36
c        if(abs(rr).le.1.d-36) rr=1.111d-36
c        if(abs(sort).le.1.d-36) sort=1.111d-36
c        if(abs(refl).le.1.d-36) refl=1.111d-36
        WRITE(14,86) hnuev,tab,nomrai(kr),tab,kr,tab,ec(kc),tab,ss,
     &        tab,rr,tab,wevs,tab,wevr,tab,was,tab,war
 86     format(0pf12.6,a,a7,a,i4,8(a,1pe10.3))
c                                                                           !g1
c mise en table et rajout de points de chaque cote de la raie
            if(sort.eq.0.d0.and.refl.eq.0.d0) goto 25
        lnur(kr)=nint((log10(hnuev)+2.d0)*2.302585d0/res)
        s(kr)=sort/res                           ! ici res=dnu/nu = 1/2000
        r(kr)=refl/res
        lnur(kr+ntab)=lnur(kr) - 1
        s(kr+ntab)=0.d0
        r(kr+ntab)=0.d0
        lnur(kr+ntab*2)=lnur(kr) + 1
        s(kr+ntab*2)=0.d0
        r(kr+ntab*2)=0.d0
c        write(50,*) hnuev,kr,lnur(kr),r(kr),lnur(kr+ntab),r(kr+ntab),  !
c     &             lnur(kr+ntab*2),r(kr+ntab*2)                        !
 25                     continue   ! do k
 26     continue   ! sortie de boucle
        WRITE(14,*)
c8670.86   ;354; 2.45E+90; 2.45E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90
c-----------------------
c tri des raies et sommation des points de meme frequence
c puis on les range a la suite du continu
        n=ntab*3
        call INDEXTRI(n,lnur,iw)
c        write(50,*) '--) raies juste apres tri   i,kr,lnu,r'           !
c        do i=1,n                                                       !
c        kr=iw(i)                                                       !
c        write(50,*) i,kr,lnur(kr),r(kr)                                !
c        enddo                                                          !
c
c        write(50,*)                                                    !
c     &  '4) toutes les raies triees avec sommation i,kr,ko,lnu,r'      !
        ii=jcmax                                                            !g1
                        do 210 i=1,n
        kr=iw(i)
        if(lnur(kr).le.0) goto 210
        if(lnur(kr).eq.lo(ii)) then                  ! passages suivants
           if(abs(r(kr)).gt.abs(ro(ii))) kro(ii)=kr
           so(ii)=so(ii)+s(kr)
           ro(ii)=ro(ii)+r(kr)
           else
        ii=ii+1                                      ! 1er passage a ce lnu
        lo(ii)=lnur(kr)
        so(ii)=s(kr)
        ro(ii)=r(kr)
        kco(ii)=0
        kro(ii)=kr                                  ! kro va de 1 a ntab*3
        endif
        imax=ii
c        write(50,*) ii,kr,kro(ii),lo(ii),ro(ii)
 210                    continue
c        WRITE(6,*) ' nombre de points du tableau=',imax  ! 34461 pour R=400
        if(imax.gt.38000) then
           write(6,*) ' trop de pts dans graf1 - STOP - imax=',imax
           write(6,*) ' REDIMENSIONNER LES TABLEAUX'
        endif
c           
c        write(50,*)                                                      !
c     & '4) continu ordonne puis raies ordonnees et sommees  i,kco,lo,ro' !
c        do i=1,imax+2                                                    !
c        write(50,*) i,kco(i),lo(i),ro(i)                                 !
c        enddo                                                            !
c---------------------
c
c MELANGE des raies et du continu
c                                                                           !g1
        n=imax
        call INDEXTRI(n,lo,iw)
c        write(50,*) '5) entrelacement    i,j,kco,kro,lo,ro'              !
c        do i=1,n                                                         !
c        j=iw(i)                                                          !
c        if(kco(j).gt.0)                                                  !
c     &   write(50,*) i,j,kco(j),kro(j),lo(j),ro(j),thnuc(kco(j))         !
c        if(kco(j).eq.0) write(50,*) i,j,kco(j),kro(j),lo(j),ro(j)        !
c        enddo                                                            !
c
c sommation du continu et des raies
c
c        write(50,*) '6) melange    i,jc,jr,kco,kro,lo,ro'             !
                       do 300 i=1,n-1
        j=iw(i)
        if(lo(j).le.0) goto 300
        if(i.gt.1.and.lo(j).eq.lo(iw(i-1))) goto 300         ! deja traite
        j1=iw(i+1)
        if(lo(j).eq.lo(j1)) then            ! forcement 1 raie et 1 continu
                  jc=min(j,j1)
                  jr=max(j,j1)
                  soc(jc)=so(jc)
                  roc(jc)=ro(jc)
                  toc(jc)=to(jc)
                  so(jc)=so(j) + so(j1)
                  ro(jc)=ro(j) + ro(j1)
                  kro(jc)=kro(jr)
                  else
                  soc(j)=so(j)
                  roc(j)=ro(j)
                  toc(j)=to(j)
                  endif
 300                   continue
c---------------------
c                                                                           !g1
c MOYENNE MOBILE en forme de gaussienne pour obtenir la resolution demandee
c
           if(ila.eq.0) then
        WRITE(30,*) ' CONTINU ET RAIES : nu*F_nu (erg cm-2 s-1)'
        WRITE(30,92) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep
           else
        WRITE(30,'(a)')'# CONTINUUM AND LINES: nu*F_nu (erg cm-2 s-1)'
        WRITE(30,93) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep
           endif   ! ila
 92     format('  hnuev   ',a,'nuFentrant',a,
     &      'nuFreflechi',a,'nuFsortant',a,'nuFtransapp',a,
     &      'nuFcontrefl',a,'nuFcontsort',a,'kc ',a,'kr ',a'ion',a)
 93     format('# hnuev   ',a,'nuFincident',a,
     &      'nuFreflected',a,'nuFoutward',a,'nuFtransapp',a,
     &      'nuFcontref',a,'nuFcontout',a,'kc ',a,'kr ',a'ion',a)
c
        dnur=2.5d0
        j0=6          ! 5 pts de chaque cote pour definir la gaussienne
        mm=11
        p(j0)=1.d0
        som=1.d0
                        do 410 j=j0+1,mm
c  p(j)=exp(- (hnu-hnu0)**2/(dnu**2/(4*Ln2)) ) si dnu=fwhm
        p(j)=expa(-(dfloat(j-j0)*0.8325546d0/dnur)**2)              ! sqrt(Ln2)
        p(2*j0-j)=p(j)
        som=som+2.d0*p(j)
 410                    continue
        fm=som
        i1=0
c        write(6,*) (p(j),j=1,mm),som                                      !g1
                        do 400 i=1,jcmax-mm+1
        hnuev=10.d0**(res*dfloat(lo(i+j0-1))/2.302585d0 - 2.d0)
                  if(hnuev.lt.0.9d0) then
                     if(kco(i).eq.0.and.kro(i).eq.0) goto 400
                     kr=kro(i)
                     hnuev=10.d0**(res*float(lo(i))/2.302585d0-2.d0)
                     if(kr.eq.0) then                   ! continu
                        if(i.le.i1) goto 400
                        ss=soc(i)
                        rr=roc(i)
                        tt=toc(i)
                        ee=eo(i)
                        ssc=soc(i)
                        rrc=roc(i)
                        i1=i
                     else                               ! cas de la raie
                        if(kr.le.ntab) then             ! centre raie
                        dss=(so(i)-soc(i))*res*resol
                        drr=(ro(i)-roc(i))*res*resol
       if(i.le.i1.and.dss.lt.soc(i)/1.d3.and.drr.lt.roc(i)/1.d3)goto 400
        if(i.le.i1) hnuev=10.d0**(res*float(lo(i1+1))/2.302585d0-2.d0)
                           ss=soc(i) + dss
                           rr=roc(i) + drr
                           tt=toc(i) + (to(i)-toc(i))*res*resol
                           ee=eo(i)
                           ssc=soc(i)
                           rrc=roc(i)                                        !g1
                        else if(kr.le.ntab*2) then
                           if(i-j0.le.i1) goto 400
                           kr=0
                     hnuev=10.d0**(res*float(lo(i-j0))/2.302585d0-2.d0)
                           ss=soc(i-j0)
                           rr=roc(i-j0)
                           tt=toc(i-j0)
                           ee=eo(i-j0)
                           ssc=soc(i-j0)
                           rrc=roc(i-j0)
                        else if(kr.gt.ntab*2) then
                           kr=0
                           i1=i+j0
                     hnuev=10.d0**(res*float(lo(i1))/2.302585d0-2.d0)
                           ss=soc(i+j0)
                           rr=roc(i+j0)
                           tt=toc(i+j0)
                           ee=eo(i+j0)
                           ssc=soc(i+j0)
                           rrc=roc(i+j0)
                        endif   ! cas de la raie
                     endif   ! kr
                     if(ee.le.1.d-36) ee=1.111d-36
                     if(ss.le.1.d-36) ss=1.111d-36
                     if(rr.le.1.d-36) rr=1.111d-36
                     if(tt.le.1.d-36) tt=1.111d-36
                     if(ssc.le.1.d-36) ssc=1.111d-36
                     if(rrc.le.1.d-36) rrc=1.111d-36
                     WRITE(30,88)hnuev,sep,ee,sep,rr,sep,ss,sep,tt,sep,
     &                     rrc,sep,ssc,sep,kco(i),sep,kr,sep,nomrai(kr) !TITAN
                     goto 400
                  else   ! hnuev>0.1
c        if(mod(i,jpas).ne.0) goto 400
                  endif  ! hnuev/0.1
        ee=0.d0
        ss=0.d0
        rr=0.d0
        ssc=0.d0
        rrc=0.d0
        tt=0.d0
        kc=0
        kr=0
        kk=0
           do 401 j=1,mm                                                    !g1
           ee=ee+eo(i+j-1)*p(j)
           ss=ss+so(i+j-1)*p(j)
           rr=rr+ro(i+j-1)*p(j)
           ssc=ssc+soc(i+j-1)*p(j)
           rrc=rrc+roc(i+j-1)*p(j)
           tt=tt+to(i+j-1)*p(j)
           if(kco(i+j-1).gt.0) then
              kk=kco(i+j-1)
              if(kc.eq.0) kc=kk                                             !g1
              kc=min(kc,kk)
              endif
           if(kro(i+j-1).gt.0.and.kro(i+j-1).le.500) then
              kkr=kro(i+j-1)
              if(kr.eq.0) kr=kkr
              kr=min(kr,kkr)
              endif
 401       continue
        ee=ee/fm
        ss=ss/fm
        rr=rr/fm
        ssc=ssc/fm
        rrc=rrc/fm
        tt=tt/fm
        if(ee.le.1.d-36) ee=1.111d-36
        if(ss.le.1.d-36) ss=1.111d-36
        if(rr.le.1.d-36) rr=1.111d-36
        if(ssc.le.1.d-36) ssc=1.111d-36
        if(rrc.le.1.d-36) rrc=1.111d-36
        if(tt.le.1.d-36) tt=1.111d-36
        WRITE(30,88) hnuev,sep,ee,sep,rr,sep,ss,sep,tt,sep,
     &               rrc,sep,ssc,sep,kc,sep,kr,sep,nomrai(kr)
 400                    continue
 88     format(0pf13.5,6(a,1pe10.3),a,i3,a,i4,a,a7)
        WRITE(30,*)
c
        stop                                                                !g1
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
c resolution>400
c                    CAS D'UNE FORTE RESOLUTION DEMANDEE
 1000   continue                                                            !g2
c-----------------------  profil des raies en triangle
c TRAITEMENT DU CONTINU
        res=1.d0/resol
        WRITE(30,80) resol
                        do 1015 kc=1,322
        hnuev=thnuc(kc)
        hc(kc)=hnuev
        ec(kc)=exflux(kc)*hnuev*2.41797d14
        sc(kc)=ciplush(kc)*pi*hnuev*2.41797d14
        rc(kc)=cimoins0(kc)*pi*hnuev*2.41797d14
        tc(kc)=restflux(kc)*hnuev*2.41797d14
        nk(kc)=kc
        if(kc.le.220) then
           lnuc(kc)=nint((log10(thnuc(kc))+2.d0)*2.302585d0/res)
           else
           lnuc(kc)=nint((log10(thnuc(kc-220))+2.d0)*2.302585d0/res)-1
           endif
 1015                   continue
c-----------------------
c tri du continu brut
        n=322
        call INDEXTRI(n,lnuc,iw)
c        write(50,*) '--) continu juste apres tri   i,kc,hnu,lnu,rc'   !
c        do i=1,n                                                      !
c        kc=iw(i)                                                      !
c        write(50,*) i,kc,thnuc(kc),lnuc(kc),rc(kc)                    !
c        enddo                                                         !
c-----------------------
c rajout de points dans le continu
        ii=0
c        write(50,*) '1) rajout de pts continu - kT en ev=',tk,
c     &             ' i,kc,hnu,lnu,rc'
                        do 1100 i=1,322
        kc=iw(i)
        hnuev=thnuc(kc)
        dhnu=hnuev*res
        if(kc.eq.189) then                             !discontinuite Balmer
           ii=ii+1                                                          !g2
           lnuc(322+ii)=lnuc(189)-1
           hc(322+ii)=thnuc(189)-dhnu
           dd=dfloat(lnuc(322+ii)-lnuc(190))/dfloat(lnuc(190)-lnuc(191))
c           ec(322+ii)=ec(189)
c           sc(322+ii)=sc(190)
c           rc(322+ii)=rc(190)
c           tc(322+ii)=tc(190)
           ec(322+ii)=ec(190)+(ec(189)-ec(190))
     &       *dfloat(lnuc(322+ii)-lnuc(190))/dfloat(lnuc(189)-lnuc(190))
           sc(322+ii)=sc(190)+(sc(190)-sc(191))*dd
           rc(322+ii)=rc(190)+(rc(190)-rc(191))*dd
           tc(322+ii)=tc(190)+(tc(190)-tc(191))*dd
           nk(322+ii)=189
           ii=ii+1                             ! interpolation entre 188 et 189
           lnuc(322+ii)=lnuc(189)+1            ! avec la pente 187-188
           hc(322+ii)=thnuc(189)+dhnu
           dd=dfloat(lnuc(322+ii)-lnuc(188))/dfloat(lnuc(188)-lnuc(187))
           ec(322+ii)=ec(189)+(ec(188)-ec(189))
     &       *dfloat(lnuc(322+ii)-lnuc(188))/dfloat(lnuc(189)-lnuc(188))
           sc(322+ii)=sc(188)-(sc(187)-sc(188))*dd
           rc(322+ii)=rc(188)-(rc(187)-rc(188))*dd
           tc(322+ii)=tc(188)-(tc(187)-tc(188))*dd
           nk(322+ii)=188
c           write(50,*) ii,kc,hc(322+ii),lnuc(322+ii),rc(322+ii)
           endif   ! kc=189
c
        if(kc.gt.102) goto 1100
        k1=iw(i+1)
        if(lnuc(k1).eq.lnuc(kc)) k1=iw(i+2)
        if(lnuc(k1).eq.lnuc(kc)) k1=iw(i+3)
        if(lnuc(k1).eq.lnuc(kc)) k1=iw(i+4)
        dnusuiv=thnuc(k1)-hnuev
        if(tk*2.d0.gt.dnusuiv) goto 1100
c
        if(tk*2.d0.le.dhnu) then   ! kT tres petit: reduction du pic par kT/dnu
           ii=ii+1
           lnuc(322+ii)=lnuc(kc)+1                                          !g2
           hc(322+ii)=hnuev+dhnu
                if((sc(kc)-sc(kc+220)).gt.0.d0) then      ! bord en emission
           sc(kc)=sc(kc+220) + (sc(kc)-sc(kc+220))*tk/dhnu
           ss=sc(kc+220)
                else                                      ! bord en absorption
           ss=sc(kc)
                endif
           sc(322+ii)=ss+(sc(k1)-ss)*dhnu/dnusuiv
                if((rc(kc)-rc(kc+220)).gt.0.d0) then      ! bord en emission
           rc(kc)=rc(kc+220) + (rc(kc)-rc(kc+220))*tk/dhnu
           rr=rc(kc+220)
                else                                      ! bord en absorption
           rr=rc(kc)
                endif
           rc(322+ii)=rr+(rc(k1)-rr)*dhnu/dnusuiv
           ec(322+ii)=ec(kc)+(ec(k1)-ec(kc))*dhnu/dnusuiv
           tc(322+ii)=tc(kc)+(tc(k1)-tc(kc))*dhnu/dnusuiv
           nk(322+ii)=kc
c           write(50,*) '0',kc,hc(kc),lnuc(kc),rc(kc)
c           write(50,*) ii,kc,hc(322+ii),lnuc(322+ii),rc(322+ii)

              else   ! 2kt > dhnures
        if((sc(kc)-sc(kc+220)).le.0.d0.and.(rc(kc)-rc(kc+220)).le.0.d0)
     &                          goto 1100
              nn=nint(tk*0.5d0/dhnu)
              if(nn.eq.0) nn=1
              ll=lnuc(kc)
                  if((sc(kc)-sc(kc+220)).gt.0.d0) then      ! bord en emission
              sfond=sc(kc+220)
c                                          if(kc.eq.46) sfond=sc(319)
              k2=min(iw(i-1),iw(i-2))
        if(k2.le.102.and.k2.ge.1.and.tk*2.d0.gt.(hnuev-thnuc(k2)).and.
     &        sc(k2).gt.sc(k2+220).and.sc(kc+220).gt.sc(k2+220))
     &        sfond=sc(k2+220) +
     &       (sc(k1)-sc(k2+220))*(hnuev-thnuc(k2))/(thnuc(k1)-thnuc(k2))!234567
              dse=sc(kc)-sfond
                  else                                    ! bord en absorption
                  sfond=sc(kc)
                  dse=0.d0                                                  !g2
                  endif
                  if((rc(kc)-rc(kc+220)).gt.0.d0) then      ! bord en emission
              rfond=rc(kc+220)
c                                          if(kc.eq.46) rfond=rc(319)
        if(k2.le.102.and.k2.ge.1.and.tk*2.d0.gt.(hnuev-thnuc(k2)).and.
     &        rc(k2).gt.rc(k2+220).and.rc(kc+220).gt.rc(k2+220))
     &        rfond=rc(k2+220) +
     &       (rc(k1)-rc(k2+220))*(hnuev-thnuc(k2))/(thnuc(k1)-thnuc(k2))
              dre=rc(kc)-rfond
                  else                                    ! bord en absorption
                  rfond=rc(kc)
                  dre=0.d0
                  endif
              do 1101 ik=1,6
                 ll=ll+nn
                 if(ll.ge.lnuc(k1)) goto 1100
                 if(ik.eq.3.and.(ll+nn).lt.lnuc(k1)) goto 1101
                 if(ik.eq.5.and.(ll+nn).lt.lnuc(k1)) goto 1101
                 ii=ii+1
                 dd=dhnu*dfloat(nn*ik)
                 lnuc(322+ii)=ll
                 hc(322+ii)=hnuev+dd
                 ec(322+ii)=ec(kc) + (ec(k1)-ec(kc))*dd/dnusuiv
                 sc(322+ii)=sfond + (sc(k1)-sfond)*dd/dnusuiv
     &                            + dse*expa(-dd/tk)
                 rc(322+ii)=rfond + (rc(k1)-rfond)*dd/dnusuiv
     &                            + dre*expa(-dd/tk)
                 tc(322+ii)=tc(kc) + (tc(k1)-tc(kc))*dd/dnusuiv
                 nk(322+ii)=kc
c        write(50,*) ii,kc,hc(322+ii),lnuc(322+ii),rc(322+ii)
 1101            continue
        endif
 1100                     continue                       ! 500-322=178
c        WRITE(6,*) ' nombre de points (continu) rajoutes=',ii
        if(ii.gt.178) WRITE(6,*) ' nombre de points rajoutes=',ii
        if(ii.gt.178) write(6,*) ' trop de pts continus rajoutes - STOP'
        if(ii.gt.178) write(6,*) ' REDIMENSIONNER LES TABLEAUX'
c-----------------------
c tri du continu                                                            !g2
        n=500
        call INDEXTRI(n,lnuc,iw)
c        write(50,*) '--) continu juste apres tri   i,ke,hnu,lnu,rc'    !
c        do i=1,500                                                     !
c        ke=iw(i)                                                       !
c        write(50,*) i,ke,hc(ke),lnuc(ke),rc(ke)                        !
c        enddo                                                          !
c-----------------------
c suppression des points de meme frequence
c        write(50,*) '2) CONTINU nuF(nu) et log(hnuev*100)*2.302585/res'
        ii=0
                        do 1110 i=1,500
        ke=iw(i)
        if(lnuc(ke).le.0) goto 1110
        if(ii.ge.1.and.lnuc(ke).eq.lo(ii)) then
           if(s(ke).gt.so(ii)) kco(ii)=ke
           eo(ii)=max(eo(ii),ec(ke))
           so(ii)=max(so(ii),sc(ke))
           ro(ii)=max(ro(ii),rc(ke))
           to(ii)=max(to(ii),tc(ke))
           goto 1110
           endif
        ii=ii+1
        kco(ii)=nk(ke)
        lo(ii)=lnuc(ke)
        eo(ii)=ec(ke)
        so(ii)=sc(ke)
        ro(ii)=rc(ke)
        to(ii)=tc(ke)
        icmax=ii
c        write(50,83) hc(ke),tab,nk(ke),tab,ec(ke),tab,sc(ke),
c     &               tab,rc(ke),tab,tc(ke),tab,lnuc(ke)
 1110   continue
 83     format(0pf12.5,a,i4,4(a,1pe10.3),a,i10)
c        WRITE(6,*)' nombre de points du continu differents=',icmax
c-----------------------
c                                                                           !g2
c TRAITEMENT DES RAIES
c
        WRITE(14,*)
     &  ' RAIES: continu sous-jacent et largeurs equivalentes'
        WRITE(14,91)
c        write(50,*) '3) rajout de pts raies   hnu,kr,lnu,r,lnu,r,lnu,r'  !
                        do 1025 kr=2,nraimax
        alambda=alo(kr)
                 if(fdo(kr).lt.fdolim) goto 1025
        hnuev=thnur(kr)
        sort=fsor(1,kr)
        refl=frer(1,kr)
c
c calcul et ecriture des largeurs equivalentes
             if(kr.ge.6.and.kr.le.12) then
             if(kr.eq.6) goto 1025
             if(kr.eq.7) then
             has=sort
             har=refl
             goto 1025
             else if(kr.eq.8) then
             hbs=sort
             hbr=refl
             goto 1025
             else if(kr.eq.9) then
             hgs=sort
             hgr=refl
             goto 1025
             else if(kr.eq.10) then
             sort=has+sort
             refl=har+refl
             else if(kr.eq.11) then
             sort=hbs+sort
             refl=hbr+refl
             else if(kr.eq.12) then
             sort=hgs+sort
             refl=hgr+refl
             endif
             endif   ! kr=6 a 12 (hydrogene)
                kf=nrcum(26,10)
                if(kr.gt.kf.and.kr.le.(kf+11)) then
                if(kr.eq.(kf+1)) then
                soms=sort                                                   !g2
                somr=refl
                goto 1025
                else if(kr.gt.(kf+1).and.kr.lt.(kf+11)) then
                soms=soms+sort
                somr=somr+refl
                goto 1025
                else   ! kr=kf+11
                sort=soms+sort
                refl=somr+refl
                endif
                endif   ! raies de fluorescence K du fer
        kc=kcr(kr)
        ss=sc(kc)
        rr=rc(kc)
        wevs=0.d0
        wevr=0.d0
        was=0.d0
        war=0.d0
        if(ss.gt.0.d0) wevs=hnuev*sort/ss
        if(rr.gt.0.d0) wevr=hnuev*refl/rr
        if(ss.gt.0.d0) was=alambda*sort/ss
        if(rr.gt.0.d0) war=alambda*refl/rr
c        if(abs(ss).le.1.d-36) ss=1.111d-36
c        if(abs(rr).le.1.d-36) rr=1.111d-36
c        if(abs(sort).le.1.d-36) sort=1.111d-36
c        if(abs(refl).le.1.d-36) refl=1.111d-36
        WRITE(14,86) hnuev,tab,nomrai(kr),tab,kr,tab,ec(kc),tab,ss,
     &        tab,rr,tab,wevs,tab,wevr,tab,was,tab,war
c
c mise en table et rajout de points de chaque cote de la raie
            if(sort.eq.0.d0.and.refl.eq.0.d0) goto 1025
        lnur(kr)=nint((log10(hnuev)+2.d0)*2.302585d0/res)
        s(kr)=sort/res
        r(kr)=refl/res
        lnur(kr+ntab)=lnur(kr) - 1
        s(kr+ntab)=0.d0
        r(kr+ntab)=0.d0                                                     !g2
        lnur(kr+ntab*2)=lnur(kr) + 1
        s(kr+ntab*2)=0.d0
        r(kr+ntab*2)=0.d0
c        write(50,*) hnuev,kr,lnur(kr),r(kr),lnur(kr+ntab),r(kr+ntab),    !
c     &             lnur(kr+ntab*2),r(kr+ntab*2)                          !
 1025                     continue   ! do k
 1026   continue   ! sortie de boucle
        WRITE(14,*)
c8670.86   ;354; 2.45E+90; 2.45E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90
c-----------------------
c tri des raies et sommation des points de meme frequence
        n=ntab*3
        call INDEXTRI(n,lnur,iw)
c        write(50,*) '--) raies juste apres tri   i,kr,lnu,r'             !
c        do i=1,n                                                         !
c        kr=iw(i)                                                         !
c        write(50,*) i,kr,lnur(kr),r(kr)                                  !
c        enddo                                                            !
c        write(50,*)                                                       !
c     &  '4) toutes les raies triees avec sommation i,ii,kr,kco,lnu,r'     !
        ii=icmax
                        do 1210 i=1,n
        kr=iw(i)
        if(lnur(kr).le.0) goto 1210
        if(lnur(kr).eq.lo(ii)) then                  ! passages suivants
           if(abs(r(kr)).gt.abs(ro(ii))) kco(ii)=kr+1000
           so(ii)=so(ii)+s(kr)
           ro(ii)=ro(ii)+r(kr)
           else
        ii=ii+1                                      ! 1er passage a ce lnu
        lo(ii)=lnur(kr)
        so(ii)=s(kr)
        ro(ii)=r(kr)
        kco(ii)=kr + 1000            ! ko-raie va de 1000 a 1000+ntab*3
        endif
c        if(kco(ii).le.500) then
c           kco(ii)=1000 + kco(ii)
c           else if(kco(ii).le.1000) then
c           kco(ii)=500 + kco(ii)
c           endif
        imax=ii                                                             !g2
c        write(50,*) i,ii,kr,kco(ii),lo(ii),ro(ii)
 1210                    continue
c
c        WRITE(6,*) ' nombre de points du tableau=',imax
        if(imax.gt.38000) then
           write(6,*) ' trop de pts dans graf2 - STOP - imax=',imax
           write(6,*) ' REDIMENSIONNER LES TABLEAUX'
        endif
c
c        write(50,*)                                                      !
c     & '4b) continu ordonne puis raies ordonnees et sommees i,kco,lo,ro' !
c        do i=1,imax+2                                                    !
c        write(50,*) i,kco(i),lo(i),ro(i)                                 !
c        enddo                                                            !
c---------------------
c
c MELANGE des raies et du continu
        n=imax
        call INDEXTRI(n,lo,iw)
c        write(50,*)'5) melange juste apres tri:i,j,kco,lo,ro,hnucontinu' !
c        do i=1,n                                                         !
c        j=iw(i)                                                          ! 
c        if(kco(j).le.1000)write(50,*)i,j,kco(j),lo(j),ro(j),hc(kco(j))   !
c        if(kco(j).gt.1000) write(50,*) i,j,kco(j),lo(j),ro(j)            !
c        enddo                                                            !
c
c sommation des points de meme frequence et resultat
           if(ila.eq.0) then
        WRITE(30,*) ' CONTINU ET RAIES : nu*F_nu (erg cm-2 s-1)'
        WRITE(30,1092) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep
           else
        WRITE(30,'(a)')'# CONTINUUM AND LINES : nu*F_nu (erg cm-2 s-1)'
        WRITE(30,1093) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep
           endif   ! ila
 1092   format('  hnuev   ',a,'nuFentrant',a,
     &      'nuFreflechi',a,'nuFsortant',a,'nuFtransapp',a,
     &      'nuFcontrefl',a,'nuFcontsort',a,'kc ',a,'kr ',a,'ion',a)
 1093   format('  hnuev   ',a,'nuFincident',a,
     &      'nuFreflected',a,'nuFoutward',a,'nuFtransapp',a,
     &      'nuFcontref',a,'nuFcontout',a,'kc ',a,'kr ',a,'ion',a)
c
        ii=0
                       do 1300 i=1,n
        j=iw(i)
           if(lo(j).le.0) goto 1300
           if(i.gt.1.and.lo(j).eq.lo(iw(i-1))) goto 1300         ! deja traite
        hnuev=10.d0**(res*dfloat(lo(j))/2.302585d0 - 2.d0)
           if(hnuev.lt.0.0118d0) goto 1300
        j1=iw(i+1)
        ii=ii+1
        if(lo(j).eq.lo(j1)) then            ! forcement 1 raie et 1 continu
                  ss=so(j) + so(j1)
                  rr=ro(j) + ro(j1)                                         !g2
                  if(ss.le.1.d-36) ss=1.111d-36
                  if(rr.le.1.d-36) rr=1.111d-36
                  kr=0
                  if(kco(j).le.1000) then
                     jc=j
                     kc=kco(j)
                     if(kco(j1).le.1000+ntab) kr=kco(j1)-1000
                     ee=ec(kc)
                     tt=tc(kc)
                     ssc=so(j)
                     rrc=ro(j)
                     if(ee.le.1.d-36) ee=1.111d-36
                     if(tt.le.1.d-36) tt=1.111d-36
                     if(ssc.le.1.d-36) ssc=1.111d-36
                     if(rrc.le.1.d-36) rrc=1.111d-36
                     else   ! kco(j)>1000
                     jc=j1
                     kc=kco(j1)
                     if(kco(j).le.1000+ntab) kr=kco(j)-1000
                     ee=ec(kc)
                     tt=tc(kc)
                     ssc=so(j1)
                     rrc=ro(j1)
                     if(ee.le.1.d-36) ee=1.111d-36
                     if(tt.le.1.d-36) tt=1.111d-36
                     if(ssc.le.1.d-36) ssc=1.111d-36
                     if(rrc.le.1.d-36) rrc=1.111d-36
                     endif
                  WRITE(30,1087) hnuev,sep,ee,sep,rr,sep,ss,sep,tt,sep,
     &                  rrc,sep,ssc,sep,kc,sep,kr,sep,nomrai(kr)
                  goto 1300
                  endif
        if(kco(j).lt.1000) then                                  ! continu
             kc=kco(j)
             kr=0
             jc=j
             ee=ec(kc)
             ss=so(j)
             rr=ro(j)
             tt=tc(kc)
             if(ee.le.1.d-36) ee=1.111d-36
             if(ss.le.1.d-36) ss=1.111d-36
             if(rr.le.1.d-36) rr=1.111d-36                                  !g2
             if(tt.le.1.d-36) tt=1.111d-36
             WRITE(30,1087) hnuev,sep,ee,sep,rr,sep,ss,sep,tt,sep,
     &                      rr,sep,ss,sep,kc,sep,kr,sep,nomrai(kr)
c
             else                                            ! raies
        ff=dfloat(lo(j)-lo(jc))/dfloat(lo(jc+1)-lo(jc))
        ssc=so(jc) + (so(jc+1)-so(jc))*ff
        rrc=ro(jc) + (ro(jc+1)-ro(jc))*ff
        ss=so(j) + ssc
        rr=ro(j) + rrc
        kr=kco(j) - 1000
        if(kco(j).gt.1000+ntab) kr=0
        if(ss.le.1.d-36) ss=1.111d-36
        if(rr.le.1.d-36) rr=1.111d-36
        if(ssc.le.1.d-36) ssc=1.111d-36
        if(rrc.le.1.d-36) rrc=1.111d-36
        ee=ec(kco(jc))+(ec(kco(jc+1))-ec(kco(jc)))*ff
        tt=tc(kco(jc))+(tc(kco(jc+1))-tc(kco(jc)))*ff
        if(ee.le.1.d-36) ee=1.111d-36
        if(tt.le.1.d-36) tt=1.111d-36
c        write(50,*) kc,lo(j),j,jc,lo(jc),lo(jc+1),ro(j),ro(jc),ro(jc+1)
        WRITE(30,1087) hnuev,sep,ee,sep,rr,sep,ss,sep,tt,sep,
     &                 rrc,sep,ssc,sep,kc,sep,kr,sep,nomrai(kr)
        endif
 1087   format(0pf13.5,6(a,1pe10.3),a,i3,a,i4,a,a7)
 1300                   continue
c        WRITE(6,*)' nombre total de points du spectre=',ii                 !g2
c
        return
        end                                                               !graf
c------------------------------------------------------------------------------
        subroutine INDEXTRI(n,larrin,indx)
c
c tri par ordre croissant des energies de photons
c       Subroutine INDEXX de Numerical Recipes p233
c       (arrin et q modifies en larrin et lq)
c
        implicit double precision (a-h,o-z)
        dimension larrin(n),indx(n)
c
        do 11 j=1,n
           indx(j)=j
 11        continue
        l=n/2+1
        ir=n
 10     continue
        if(l.gt.1) then
           l=l-1
           indxt=indx(l)
           lq=larrin(indxt)
        else
           indxt=indx(ir)
           lq=larrin(indxt)
           indx(ir)=indx(1)                                                 !gi
           ir=ir-1
           if(ir.eq.1) then
              indx(1)=indxt
              return
           endif
        endif
        i=l
        j=l+l
 20     if(j.le.ir) then
           if(j.lt.ir) then
              if(larrin(indx(j)).lt.larrin(indx(j+1))) j=j+1
           endif
           if(lq.lt.larrin(indx(j))) then
              indx(i)=indx(j)
              i=j
              j=j+j
           else
              j=ir+1
           endif
        goto 20
        endif
        indx(i)=indxt
        goto 10
        end                                                      !graf indextri
c--============================================================================
c...................................................incid=1 loi de puissance
        function FFLUXLP(hnuev)
c
        implicit double precision (a-h,o-z)
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
c
                        if(hnuev.le.1.d-100) goto 10
c
        if(hnuev.ge.hnumin.and.hnuev.le.hnumax) then
        FFLUXLP=fluxex0*(13.6d0/hnuev)**flindex
                else
                FFLUXLP=0.d0
                endif
        return
   10                        continue
c
c coef.compton approx. si LP1-26a100kev (15-4-98) : 1.574  0.55  1.4  0.630724
c
c calcul de Fluxex0, riontot (pour gksi)
c riontot=fluxionisant/fluxtotal
c Fionisant de 1 a 1000 ryd (KMT)        Ftotal de 10**-3 ryd a 100 kev
c r1=hnumin/100kev; r2=0.5ev/100kev; r3=13.6ev/13.6kev; r4=hnumin/1ryd
                if(flindex.gt.0.9999.and.flindex.lt.1.0001) then   ! nuL=13.6ev
        fluxex0=Ftotinit/3.2893d15/log(hnumax/hnumin)           !1/nuL/Ln(1/r1)
        riontot=log(1.d3)/log(hnumax/hnumin)                    ! Ln(r3)/Ln(r1)
                else
        rr1=(hnumin/hnumax)**(flindex-1.d0)
        rr2=5.d-6**(flindex-1.d0) 
        rr3=1.d-3**(flindex-1.d0)
        rr4=(hnumin/13.6d0)**(flindex-1.d0)
        fluxex0=Ftotinit/3.2893d15 *(flindex-1.d0)*rr4/(1.d0-rr1)       ! 1/nuL
        riontot=rr4*(1.d0-rr3)/(1.d0-rr1)
                endif
        FFLUXLP=fluxex0
        return
        end                                                           !fflux-LP
c------------------------------------------------------------------------------
c.........................................incid=2 ou irent=1-2-3-4 (corps noir)
        function FFLUXBB(hnuev)
c
c ici ffluxbb= B(nu)=2hnu**3/c**2 1/(exp(hnu/kT)-1) *wbb =I- ou I+
c ce n'est pas un flux
c
        implicit double precision (a-h,o-z)
        common/bb/Tbb,wbb
c
        if(hnuev.le.1.d-100) then
          FFLUXBB = wbb
!          FFLUXBB = wbb * 5.67d-5 * Tbb**4
        else
          FFLUXBB=0.d0
          if (hnuev/tbb.lt.0.06)
     &       FFLUXBB=2.0845056d-4*wbb*hnuev**3/
     &                (expa(+hnuev/Tbb*1.16048d4)-1.d0)
        endif
c
        return
        end                                                           !fflux-bb
c------------------------------------------------------------------------------
c........................................................irent=5 loi de Wien
        function FFLUXWI(hnuev)
c
c ici ffluxwi=2hnu**3/c**2 *exp(-hnu/kT)  =I- 
c
        implicit double precision (a-h,o-z)
        common/bb/Tbb,wbb
c
                    if(hnuev/tbb.lt.0.06) then
                                                              ! e/k - 2e3/h2c2
        FFLUXWI=hnuev*hnuev*hnuev*expa(-hnuev/Tbb*1.16048d4)
     &                                          *2.0845056d-4*wbb
                    else
        FFLUXWI=0.d0
                    endif
c
        return
        end
c------------------------------------------------------------------------------
c................................................corps noir
        function FFLUXBBT(hnuev,T)
c
c ici ffluxbbt= B(nu)=2hnu**3/c**2 1/(exp(hnu/kT)-1) =I- ou I+
c
        implicit double precision (a-h,o-z)
c
                    if(hnuev/T.lt.0.06) then
                                                      ! B(nu) - e/k - 2e3/h2c2
        FFLUXBBT=hnuev*hnuev*hnuev/(expa(+hnuev/T*1.16048d4)-1.d0)
     &                                          *2.0845056d-4
                    else
        FFLUXBBT=0.d0
                    endif
c
        return
        end                                                        !fflux-bb(T)
c------------------------------------------------------------------------------
c...FFLUXLaor...spectre moyen des radio-quiets Laor&al 1997ApJ477,93...incid=3
        function FFLUX(hnuev)
c
        implicit double precision (a-h,o-z)
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
c
                        if(hnuev.le.1.d-100) goto 10
                if(hnuev.lt.0.1d0) then
        fflux=0.d0
                else if(hnuev.lt.4.13285d0) then
        fflux=fluxex0* 3.1674135d0*(4.13285d0/hnuev)**0.35d0
                else if(hnuev.lt.11.808d0) then
        fflux=fluxex0* 1.2841125d0*(11.808d0/hnuev)**0.86d0
                else if(hnuev.lt.35.4244d0) then
        fflux=fluxex0* (13.6d0/hnuev)**1.77d0
                else if(hnuev.lt.200.d0) then
        fflux=fluxex0* 0.1836955d0*(35.4244d0/hnuev)**1.4d0
                else if(hnuev.lt.2000.d0) then
        fflux=fluxex0* 1.628096d-2*(200.d0/hnuev)**1.69d0
                else if(hnuev.lt.100000.d0) then
        fflux=fluxex0* 3.324145d-4*(2000.d0/hnuev)
                else
        fflux=0.d0
                endif
        return
c
 10                             continue
c
c coef.compton approx.(15-4-98 commeLP1-26a100kev) : 1.574  0.55  1.4  0.630724
c
c calcul de Fluxex0, riontot (pour gksi)
c riontot=fluxionisant/fluxtotal
c Fionisant de 1 a 1000 ryd (KMT)       Ftotal de 10**-2 ev a 500 kev
        fluxex0=Ftotinit/2.41797d14/58.87576
        hnumin=0.1d0
        hnumax=1.d5
        flindex=1.d0
        riontot=0.37992
        fflux=fluxex0
        return
        end                                                         !fflux-Laor
c------------------------------------------------------------------------------
c.......FFLUX AGN de Cloudy...spectre moyen de Ferland&Matthews 1987...incid=4
        function FFLUX1(hnuev)
c
        implicit double precision (a-h,o-z)
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
c
                        if(hnuev.le.1.d-100) goto 10
                if(hnuev.lt.0.124d0) then
        fflux1=fluxex0* 9.19120752d3*hnuev**2.5d0
                else if(hnuev.lt.2.8d0) then
        fflux1=fluxex0* 6.1708994d0/hnuev
                else if(hnuev.lt.23.7d0) then
        fflux1=fluxex0* sqrt(13.6d0/hnuev)
                else if(hnuev.lt.56.17d0) then
        fflux1=fluxex0* 17.9532727d0/hnuev
                else if(hnuev.lt.365.d0) then
        fflux1=fluxex0* 5.66438123d4/hnuev**3.d0
                else if(hnuev.lt.100000.d0) then
        fflux1=fluxex0* 0.0724234d0/hnuev**0.7d0
                else
        fflux1=fluxex0* 5.1271838d3/hnuev**1.67d0
                endif
        return
c
 10                             continue
c
c coef.compton approx.(13-5-98) ->tauth=30 : 0.532  0.43  0.35  0.656647
c
c calcul de Fluxex0, riontot (pour gksi)
c riontot=fluxionisant/fluxtotal
c Fionisant de 1 a 1000 ryd (KMT)       Ftotal de 10**-2 ev a 500 kev
        fluxex0=Ftotinit/2.41797d14/78.454298d0
        riontot=0.4555673d0
        fflux1=fluxex0
        return
        end                                                   !fflux1-AGNCloudy
c------------------------------------------------------------------------------
c...........FFLUXnetzer.....................Netzer 1996ApJ473,781.....incid=5
        function FFLUX2(hnuev)
c
        implicit double precision (a-h,o-z)
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
c
                        if(hnuev.le.1.d-100) goto 10
                if(hnuev.lt.0.408d0) then
        fflux2=fluxex0* 1.43294974d2*hnuev**2.5d0              ! somme=1.776125
c                                 (13.6/1.632)**0.5*(1.632/.408)**1.2/.408**2.5
                else if(hnuev.lt.1.632d0) then
        fflux2=fluxex0* 5.19605288d0/hnuev**1.2d0              ! somme=7.526294
c                                   (13.6/1.632)**0.5 *1.632**1.2
                else if(hnuev.lt.40.8d0) then
        fflux2=fluxex0* 3.687818d0/sqrt(hnuev)     ! V(13.6/hnu somme=37.689426
                else if(hnuev.lt.200.d0) then
        fflux2=fluxex0* 5.16212688d5/hnuev**3.695d0            ! somme=8.620088
c                                   3.695=ln(F(200)/F(40.8))/ln(40.8/200)
                else if(hnuev.lt.5.d4) then
        fflux2=fluxex0* 0.19124144d0/hnuev**0.9d0              ! somme=2.394081
c                                   (13.6/4.96)**0.5 *(4.96/2e3)**1.5 *2e3**0.9
                else
        fflux2=fluxex0* 1.41064913d9/hnuev**3.d0               ! somme=0.282130
c                                    0.19124144d0/5e4**0.9 *5e4**3
                endif
        return
c
 10                             continue
c
c coef.compton approx.(15-4-98 bon->tauth=35) : 0.677  0.5  0.5  0.69054
c
c calcul de Fluxex0 (flux a 13.6), riontot (pour gksi)
c riontot=fluxionisant/fluxtotal
c Fionisant de 1 a 1000 ryd (KMT)       Ftotal de 10**-2 ev a 500 kev
c csit(AM) = 4pi/Nh *somme(Fdnu) = 4pi/Nh *2.41797d14*58.288143
c Ux(Netzer) = 1/(Nh*c*h) *somme(Fdnu/nu)=1/(Nh*c*h) *0.0070032431
c csit/Ux = 58.288/0.007 *4pie'c = 5023.73
        fluxex0=Ftotinit/2.41797d14/58.288143d0
        riontot=0.518753d0
        fflux2=fluxex0
        return
        end                                                      !fflux2-netzer
c------------------------------------------------------------------------------
c...........FFLUXkrolik...............Krolik-Kriss 1995ApJ447,512.....incid=6
        function FFLUX3(hnuev)
c
        implicit double precision (a-h,o-z)
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
c
                        if(hnuev.le.1.d-100) goto 10
                if(hnuev.lt.0.136d0) then
        fflux3=fluxex0* 9.02287d3*hnuev**2.4d0
                else if(hnuev.lt.2.8d0) then
        fflux3=fluxex0* 10.21826d0/hnuev
                else if(hnuev.lt.4.96d0) then
        fflux3=fluxex0* 6.10658d0/hnuev**0.5d0
                else if(hnuev.lt.78.91d0) then
        fflux3=fluxex0* 13.6d0/hnuev
                else if(hnuev.lt.500.d0) then
        fflux3=fluxex0* 1.073176d3/hnuev**2
                else if(hnuev.lt.1.d5) then
        fflux3=fluxex0* 0.453897d0/hnuev**0.75d0
                else
        fflux3=fluxex0* 8.071578d7/hnuev**2.4d0
                endif
        return
c
   10                        continue
c
c coef.compton approx.(2-12-98 bon->tauth=20) : 1.379/0.5/1.2/0.58033
c
c calcul de Fluxex0 (flux a 13.6), riontot (pour gksi)
c riontot=fluxionisant/fluxtotal                  ! nuF(nu)=F0*13.6*2.41797d14
c Fionisant de 1 a 1000 ryd (KMT)        Ftotal de 10**-2 ev a 500 kev
        fluxex0=Ftotinit/3.2893d15/9.25236d0
        riontot=0.36864d0
        fflux3=fluxex0
        return
        end                                                      !fflux3-Krolik
c------------------------------------------------------------------------------
c...........FFLUX5548...............spectre simplifie de 5548.......incid=7
        function FFLUX4(hnuev)
c
        implicit double precision (a-h,o-z)
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
c
                        if(hnuev.le.1.d-100) goto 10
                if(hnuev.lt.0.1d0) then
        fflux4=fluxex0* 136.d0*(hnuev/0.1d0)**2.5d0               ! 13.6/0.1
                else if(hnuev.lt.200.d0) then
        fflux4=fluxex0* 13.6d0/hnuev               ! nuF(nu)=F0*13.6*2.41797d14
                else if(hnuev.lt.1000.d0) then
        fflux4=fluxex0* 6.57688d-3*(1000.d0/hnuev)**1.45d0
                else if(hnuev.lt.10000.d0) then
        fflux4=fluxex0* 6.57688d-3*(1000.d0/hnuev)**0.9d0
                else if(hnuev.lt.55000.d0) then
        fflux4=fluxex0* 8.48328d-4*(10000.d0/hnuev)**0.5d0
                else
        fflux4=fluxex0* 3.61728d-4*(55000.d0/hnuev)**2
                endif
        return
c
   10                        continue
c
c calcul de Fluxex0, riontot (pour gksi)
c riontot=fluxionisant/fluxtotal
c Fionisant de 1 a 1000 ryd (KMT)        Ftotal de 10**-2 ev a 500 kev
        fluxex0=Ftotinit/3.2893d15/11.7533d0
        riontot=0.41261d0
        fflux4=fluxex0
        return
        end                                                     !fflux4-NGC5548
c------------------------------------------------------------------------------
c...........FFLUX3783...............spectre simplifie de 3783.......incid=8
        function FFLUX5(hnuev)
c
c Kaspi et al. ApJ 2001,554,226
        implicit double precision (a-h,o-z)
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
c
                        if(hnuev.le.1.d-100) goto 10
                if(hnuev.lt.0.2d0) then
        fflux5=0.d0
                else if(hnuev.lt.2.d0) then
        fflux5=fluxex0* 5.215362d0/hnuev         ! sqrt(13.6/2)*2
                else if(hnuev.lt.40.d0) then
        fflux5=fluxex0* sqrt(13.6d0/hnuev)
                else if(hnuev.lt.100.d0) then
        fflux5=fluxex0* 0.5830952d0*(40.d0/hnuev)**4.77d0        ! sqrt(13.6/40)
                 else if(hnuev.lt.50000.d0) then
        fflux5=fluxex0* 7.3716654d-3*(100.d0/hnuev)**0.77d0
                else
        fflux5=fluxex0* 6.156852d-5*(50000.d0/hnuev)**3
                endif
        return
c
   10                        continue
c
c calcul de Fluxex0, riontot (pour gksi)
c riontot=fluxionisant/fluxtotal                  ! nuF(nu)=F0*13.6*2.41797d14
c Fionisant de 1 a 1000 ryd (KMT)        Ftotal de 10**-2 ev a 500 kev
        fluxex0=Ftotinit/2.41797d14/65.9316113d0
        riontot=0.4876964d0
        fflux5=fluxex0
        return
        end                                                     !fflux5-NGC3783
c------------------------------------------------------------------------------
c........... Spectrum from Lexington 2000 X-ray thin model.......incid=9
        function FFLUX6(hnuev)
c
! We use a generic way of defining a broken power-law
        implicit double precision (a-h,o-z)
        parameter(np=5)
!       parameter(np=6)
        dimension xloghnu(np),xlogfnu(np),a(np-1),b(np-1)
        double precision log10
        logical ldebug
        integer iverbose
        data xloghnu/-4.8663d0,-0.8663d0,1.6108d0,2.d0,4.3979d0/
        data xlogfnu/1.d0,14.5d0,12.7d0,10.6d0,8.2021d0/
!       data xloghnu/-4.8663d0,-0.8663d0,1.6108d0,2.d0,5.d0,7.1337d0/
!       data xlogfnu/1.d0,14.5d0,12.7d0,10.6d0,7.6d0,1.d0/
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
        common/debug/ldebug,iverbose
c
! compute the coefficients of the piecewise power-law F(nu)=a*hnu**b
! and the total unnormalizd flux xint
        xint = 0.d0
        xion = 0.d0
        xhnuion = 13.6d0
        xloghnuion = log10(xhnuion)
        xln10 = 2.302585d0
        do i=1,np-1
           xhnu1 = 10.d0**xloghnu(i)
           xhnu2 = 10.d0**xloghnu(i+1)
           xb = (xlogfnu(i+1)-xlogfnu(i))/(xloghnu(i+1)-xloghnu(i))
           xa = 10.d0**(xlogfnu(i) - xb * xloghnu(i))
           if (xb.eq.-1.d0) then
              xint1 = xa*xln10*(xloghnu(i+1)-xloghnu(i))    
           else
              xint1 = xa/(xb+1.d0)*(xhnu2**(xb+1.d0) - xhnu1**(xb+1.d0))
           endif   
           if (xloghnuion.le.xloghnu(i)) xion = xion + xint1
           if((xloghnuion.gt.xloghnu(i)).and.
     &          (xloghnuion.lt.xloghnu(i+1)))then
             if (xb.eq.-1.d0) then
                xion = xion + xa*xln10*(xloghnu(i+1)-xloghnuion)    
             else
                xion = xion + xa/(xb+1.d0)*(xhnu2**(xb+1.d0) -
     &             xhnuion**(xb+1.d0))
             endif
           endif
           xint = xint + xint1
           a(i) = xa
           b(i) = xb
!
           if (ldebug.and.iverbose.ge.2) 
     &       write(*,*) 'FFLUX6(i,xa,xb,xint1,xint,xion)=',
     &                             i,xa,xb,xint1,xint,xion
!
           enddo
! compute some values
!        if(hnuev.le.1.d-100) then    
!           riontot = xion/xint ! ionizing flux / total flux (not used)
!        fluxex0 = 0.d0 ! flux at 13.6 eV (call this routine to get the result)
!           fflux6 = fluxex0
!        else
           xf = 0.d0
           xloghnuev = log10(hnuev)
           do i = 1,np-1
              if (xloghnuev.eq.xloghnu(i)) then
                  xf = 10.d0**xlogfnu(i)
              else if ((xloghnuev.gt.xloghnu(i)).and.
     &                  (xloghnuev.lt.xloghnu(i+1))) then
                  xf = a(i)*hnuev**b(i)
              endif    
           enddo
           if (xloghnuev.eq.xloghnu(np)) xf = xlogfnu(np)
! normalisation Ftot=Ftotinit, facteur 2.41797e14 is to express integration
! on frequency s-1 instead of energy in eV
           fflux6 = xf * Ftotinit / xint / 2.41797d14
!
           if (ldebug.and.iverbose.ge.1) 
     &       write(*,*) 'FFLUX6(hnuev,xf,fflux6)=',hnuev,xf,fflux6
!        endif   
        return
        end                                              !fflux6-lexington2000
c------------------------------------------------------------------------------
c........ Spectre entr� par points...broken power law spectrum.......incid=10
        function FFLUX7(hnuev)
c
! We use a generic way of defining a broken power-law
        implicit double precision (a-h,o-z)
        double precision log10
        logical ldebug
        integer iverbose
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
        common/flux7/yloghnu(10),ylogfnu(10),apl(9),bpl(9),cpl,npy
        common/debug/ldebug,iverbose
c
! compute the coefficients of the piecewise power-law F(nu)=a*hnu**b
! and the total unnormalizd flux yint
c les points yloghnu,ylogfnu d�finissent la loi de puissance a*hnu**b
c qu'il faut multiplier par cpl pour obtenir le bon Ftotinit=csi*n/4pi
                        if(hnuev.le.1.d-100) then
        yint = 0.d0
        yion = 0.d0
        yhnuion = 13.6d0
        yloghnuion = log10(yhnuion)
        yln10 = 2.302585d0
        do i=1,npy-1
           yhnu1 = 10.d0**yloghnu(i)
           yhnu2 = 10.d0**yloghnu(i+1)
           b = (ylogfnu(i+1)-ylogfnu(i))/(yloghnu(i+1)-yloghnu(i))
           a = 10.d0**(ylogfnu(i) - b * yloghnu(i))
           if (b.eq.-1.d0) then
              yint1=a*yln10*(yloghnu(i+1)-yloghnu(i))    
           else
              yint1=a/(b+1.d0)*(yhnu2**(b+1.d0) - yhnu1**(b+1.d0))
           endif   
           if (yloghnuion.le.yloghnu(i)) yion = yion + yint1
           if((yloghnuion.gt.yloghnu(i)).and.
     &          (yloghnuion.lt.yloghnu(i+1)))then
              yf136=a *13.6d0**b
             if (b.eq.-1.d0) then
                yion = yion + a*yln10*(yloghnu(i+1)-yloghnuion)    
             else
                yion = yion + a/(b+1.d0)*(yhnu2**(b+1.d0) -
     &             yhnuion**(b+1.d0))
             endif
           endif
           yint = yint + yint1
           apl(i) = a
           bpl(i) = b
!
!           riontot = yion/yint ! ionizing flux / total flux (not used)
              if(ldebug.and.iverbose.ge.2) write(*,*)
     &        'FFLUX7(i,a,b,yint1,yint,yion)=',i,a,b,yint1,yint,yion
!
        enddo
        cpl=Ftotinit /yint /2.41797d14
        fflux7=yf136 *cpl
        fluxex0=yf136 *cpl
c
c calcul pour hnu - compute values
                        else
           yf = 0.d0
           yloghnuev = log10(hnuev)
           do i = 1,npy-1
              if (yloghnuev.eq.yloghnu(i)) then
                  yf = 10.d0**ylogfnu(i)
              else if ((yloghnuev.gt.yloghnu(i)).and.
     &                  (yloghnuev.lt.yloghnu(i+1))) then
                  yf = apl(i)*hnuev**bpl(i)
              endif    
           enddo
           if (yloghnuev.eq.yloghnu(npy)) yf = ylogfnu(npy)
! normalisation Ftot=Ftotinit, facteur 2.41797e14 is to express integration
! on frequency s-1 instead of energy in eV
c           fflux7 = yf * Ftotinit / yint / 2.41797d14
           fflux7 = yf *cpl
!
           if (ldebug.and.iverbose.ge.1) 
     &       write(*,*) 'FFLUX7(hnuev,yf,fflux7)=',hnuev,yf,fflux7
                        endif
        return
        end                                             !fflux7-brokenpowerlaw
c------------------------------------------------------------------------------
c................................incid=17 power-law  with exp -cut-off Agata
        function FFLUXECO1LP(hnuev)                   
c
        implicit double precision (a-h,o-z)
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
        common/opt1/incid,irent,idens,itemp,ietl,irecsup,icompt,inr,
     &                isuite,ila,imp,ifich,iexpert
        dimension ev(10001), evi(10001) 
c
                        if(hnuev.le.1.d-100) goto 10
         
        FFLUXECO1LP=fluxex0*(13.6d0/hnuev)**flindex
     &        *expa(-(hnuev/hnumax+hnumin/hnuev))
          if (FFLUXECO1LP.lt.1.d-70) FFLUXECO1LP=0.d0
       
        return
   10                        continue
c
c calcul de Fluxex0, riontot (pour gksi)
c riontot=fluxionisant/fluxtotal
c Fionisant de 1 a 1000 ryd (KMT)        Ftotal de 10**-3 ryd a 100 kev
c r1=hnumin/100kev; r2=0.5ev/100kev; r3=13.6ev/13.6kev; r4=hnumin/1ryd
c       normalization for power-law with exponential cutt-off  !!!!!!!!!!
        N=10000
        dlwid=(log10(hnumax*100.d0)-log10(hnumin/100.d0))/N
        dlion=log10(1.d7)/N
        ev(1)=(hnumin/100.d0)/13.6d0
        evi(1)=(13.6d0/100.d0)/13.6d0
        flcalk=0.d0
        flion=0.d0
        do 30 i=2,N+1 
          ev(i)=10.d0**(log10(hnumin/100.d0)+dlwid*(i-1))/13.6d0
          dev=ev(i)-ev(i-1)
          evi(i)=10.d0**(log10(13.6d0/100.d0)+dlion*(i-1))/13.6d0
          devi=evi(i)-evi(i-1)
          flcalk=flcalk + (ev(i))**(-flindex)*dev
     &      *dexp(-ev(i)*13.6d0/hnumax)*dexp(-hnumin/ev(i)/13.6d0)
          flion=flion + (evi(i))**(-flindex)*devi
     &      *dexp(-evi(i)/1.d3) *dexp(-1.d0/evi(i))
 30     continue 
        
        fluxex0=Ftotinit/3.2893d15/flcalk      
        riontot=flion/flcalk                               
        FFLUXECO1LP=fluxex0 
        if(iexpert.eq.1) write(15,*)'incid=17: fluxex0=',fluxex0
        if(iexpert.eq.1) write(15,*)
        return
        end                                                       !fflux-eco1LP
c------------------------------------------------------------------------------
c..........................incid=18 double power law  with exp cut-off by Agata
        function FFLUXECO2LP(hnuev)
c
        implicit double precision (a-h,o-z)
        common/flux/flindex,hnumin,hnumax,Ftotinit,fluxex0,csit,riontot
        common/drugi/flin2,hnumin2,hnumax2,fxdofo,fluxex2
        dimension ev(10001), ev2(10001), evi(10001)
c
                        if(hnuev.le.1.d-100) goto 10

        FFLUXECO2LP=fluxex0*(
     *    (13.6d0/hnuev)**flindex*expa(-(hnuev/hnumax+hnumin/hnuev)) +
     *    fluxex2*(13.6d0/hnuev)**flin2*expa(-(hnuev/hnumax2+
     &                                         hnumin2/hnuev)))

        if (FFLUXECO2LP.lt.1.d-70) FFLUXECO2LP=0.d0

        return
  10                        continue
c
c coef.compton approx. si LP1-26a100kev (15-4-98) : 1.574  0.55  1.4  0.630724
c
c calcul de Fluxex0, riontot (pour gksi)
c riontot=fluxionisant/fluxtotal
c Fionisant de 1 a 1000 ryd (KMT)        Ftotal de 10**-3 ryd a 100 kev
c r1=hnumin/100kev; r2=0.5ev/100kev; r3=13.6ev/13.6kev; r4=hnumin/1ryd
        N=10000
        dlwid=(log10(hnumax*100.d0)-log10(hnumin/100.d0))/N
        dlwid2=(log10(hnumax2*100.d0)-log10(hnumin2/100.d0))/N
        dlion=log10(1.d7)/N
        ev(1)=(hnumin/100.d0)/13.6d0
        ev2(1)=(hnumin2/100.d0)/13.6d0
        evi(1)=(13.6d0/100.d0)/13.6d0
        flcalk=0.d0
        flcalk2=0.d0
        flion=0.d0
        do 30 i=2,N+1 
          ev(i)=10.d0**(log10(hnumin/100.d0)+dlwid*(i-1))/13.6d0
          dev=ev(i)-ev(i-1)
          ev2(i)=10.d0**(log10(hnumin2/100.d0)+dlwid2*(i-1))/13.6d0
          dev2=ev2(i)-ev2(i-1)
          evi(i)=10.d0**(log10(13.6d0/100.d0)+dlion*(i-1))/13.6d0
          devi=evi(i)-evi(i-1)
          flcalk=flcalk + (ev(i))**(-flindex)*dev
     &      *dexp(-ev(i)*13.6d0/hnumax)*dexp(-hnumin/ev(i)/13.6d0)
          flcalk2=flcalk2 + (ev2(i))**(-flin2)*dev2
     &      *dexp(-ev2(i)*13.6d0/hnumax2)*dexp(-hnumin2/ev2(i)/13.6d0)
          flion=flion + (evi(i))**(-flindex)*devi
     &      *dexp(-evi(i)/1.d3) *dexp(-1.d0/evi(i))
 30     continue 
        
        fluxex0=Ftotinit/3.2893d15/flcalk/(1.d0+fxdofo)
        fluxex2=Ftotinit*fxdofo/3.2893d15/flcalk2/(1.d0+fxdofo)                
        riontot=flion/flcalk         
        fluxex2=fluxex2/fluxex0
        FFLUXECO2LP=fluxex0

        return
        end                                                       !fflux-eco2LP
c--============================================================================
        function EXPA(x)
c calcul des exponentielles
c pour un compilateur qui ne met pas exp(-x)=0 si x est trop grand
c remplacer tous les exp par expa
        implicit double precision (a-h,o-z)
        if(x.le.-500.d0) then
              expa=0.d0
           else if(x.gt.700.d0) then
              write(6,*) 'expa x > 700 : x=',x
              expa=0.d0                  ! ???
           else if(abs(x).le.0.003d0) then               ! paletou 0.01
              expa=1.d0+x+x**2/2.d0+x**3/6.d0+x**4/24.d0+x**5/120.d0
           else
              expa=exp(x)
           endif
        return
        end                                                               !expa
c-----------------------------------------------------------------------------
       block data coeffDR

c coef recomb dielectr basse temperature - utilise dans DIEBT

       implicit double precision (a-h,o-z)
       common/ddr/dr(5,14,14),dr2(10,14,14)

       data(dr(i,6, 6),i=1,5)/0.0108,-0.1075,0.2810,-0.0193,-0.1127/
       data(dr(i,6,5),i=1,5)/1.8267,4.1012,4.8443,0.2261,0.5960/
       data(dr(i,6,4),i=1,5)/2.3196,10.7328,6.8830,-0.1824,0.4101/
       data(dr(i,7,6),i=1,5)/0.0320,-0.6624,4.3191,0.0003,0.5946/
       data(dr(i,7,5),i=1,5)/-0.8806,11.2406,30.7066,-1.1721,0.6127/
       data(dr(i,7,4),i=1,5)/0.4134,-4.6319,25.9172,-2.2290,0.2360/
       data(dr(i,8,7),i=1,5)/-0.0036,0.7519,1.5252,-0.0838,0.2769/
       data(dr(i,8,6),i=1,5)/0.0000,21.8790,16.2730,-0.7020,1.1899/
       data(dr(i,8,4),i=1,5)/-2.8425,0.2283,40.4072,-3.4956,1.7558/
       data(dr(i,10,9),i=1,5)/0.0129,-0.1779,0.9353,-0.0682,0.4516/
       data(dr(i,10,8),i=1,5)/3.6781,14.1481,17.1175,-0.5017,0.2313/
       data(dr(i,10,7),i=1,5)/-0.0254,5.5365,17.0727,-0.7225,0.1702/
       data(dr(i,10,6),i=1,5)/-0.0141,33.8479,43.1608,-1.6072,0.1942/
       data(dr(i,10,5),i=1,5)/19.9280,235.0536,152.5096,9.1413,0.1282/
       data(dr(i,10,4),i=1,5)/5.4751,203.9751,86.9016,-7.4568,2.5145/
       data(dr(i,12,12),i=1,5)/1.2044,-4.6836,7.6620,-0.5930,1.6260/
       data(dr(i,13,13),i=1,5)/0.0219,-0.4528,2.5427,-0.1678,0.2276/
       data(dr(i,13,12),i=1,5)/0.7086,-3.1083,7.0422,0.5998,0.4194/
       data(dr(i,14,14),i=1,5)/-0.0219,0.4364,0.0684,-0.0032,0.1342/
       data(dr(i,14,13),i=1,5)/3.2163,-12.0571,16.2118,-0.5886,0.5613/
       data(dr(i,14,12),i=1,5)/0.1203,-2.6900,19.1943,-0.1479,0.1118/ 
       data(dr2(i,8,5),i=1,10)/-0.3648,7.2698,17.2187,9.8335,-0.0166,
     &          -2.5053,3.4903,67.4128,-3.4450,0.8501/
       data(dr2(i,8,8),i=1,10)/-0.0001,0.0001,0.0956,0.0193,0.4106,
     &           0.3715,-0.0239,-0.0597,0.0678,0.7993/
       end                                                       ! coefDR-diebt
c*********************************************
       block data radrec

c coef recomb radiatives totales (somme sur ts les niveaux) - utilise ds RRFIT

       implicit double precision (a-h,o-z)
       common/drrec/rrec(2,30,30),rnew(4,30,30),fe(3,13)

      data(rrec(i, 4, 4),i=1,2)/4.500d-13,0.6480/
      data(rrec(i, 5, 4),i=1,2)/2.680d-12,0.7250/
      data(rrec(i, 6, 4),i=1,2)/4.900d-12,0.8030/
      data(rrec(i, 7, 4),i=1,2)/9.400d-12,0.7650/
      data(rrec(i, 8, 4),i=1,2)/1.590d-11,0.7590/
      data(rrec(i, 9, 4),i=1,2)/1.919d-11,0.7849/
      data(rrec(i,10, 4),i=1,2)/2.800d-11,0.7710/
      data(rrec(i,11, 4),i=1,2)/4.030d-11,0.7873/
      data(rrec(i,12, 4),i=1,2)/6.830d-11,0.7650/
      data(rrec(i,13, 4),i=1,2)/8.343d-11,0.8291/
      data(rrec(i,14, 4),i=1,2)/1.430d-10,0.8230/
      data(rrec(i,15, 4),i=1,2)/1.460d-10,0.8391/
      data(rrec(i,16, 4),i=1,2)/2.000d-10,0.8060/
      data(rrec(i,17, 4),i=1,2)/2.091d-10,0.8691/
      data(rrec(i,18, 4),i=1,2)/3.070d-10,0.8190/
      data(rrec(i,19, 4),i=1,2)/3.210d-10,0.8934/
      data(rrec(i,20, 4),i=1,2)/4.610d-10,0.8330/
      data(rrec(i,21, 4),i=1,2)/4.870d-10,0.8060/
      data(rrec(i,22, 4),i=1,2)/5.139d-10,0.7781/
      data(rrec(i,23, 4),i=1,2)/5.850d-10,0.7570/
      data(rrec(i,24, 4),i=1,2)/6.556d-10,0.7359/
      data(rrec(i,25, 4),i=1,2)/7.238d-10,0.7242/
      data(rrec(i,27, 4),i=1,2)/8.404d-10,0.7167/
      data(rrec(i,28, 4),i=1,2)/1.360d-09,0.8420/
      data(rrec(i,29, 4),i=1,2)/1.880d-09,0.8420/
      data(rrec(i,30, 4),i=1,2)/2.400d-09,0.8420/
      data(rrec(i, 5, 5),i=1,2)/4.600d-13,0.6360/
      data(rrec(i, 6, 5),i=1,2)/2.300d-12,0.6450/
      data(rrec(i, 7, 5),i=1,2)/5.000d-12,0.6760/
      data(rrec(i, 8, 5),i=1,2)/9.600d-12,0.6700/
      data(rrec(i, 9, 5),i=1,2)/1.558d-11,0.6816/
      data(rrec(i,10, 5),i=1,2)/2.300d-11,0.7040/
      data(rrec(i,11, 5),i=1,2)/3.253d-11,0.7075/
      data(rrec(i,12, 5),i=1,2)/4.600d-11,0.7110/
      data(rrec(i,13, 5),i=1,2)/5.951d-11,0.7125/
      data(rrec(i,14, 5),i=1,2)/7.700d-11,0.7140/
      data(rrec(i,15, 5),i=1,2)/1.042d-10,0.7330/
      data(rrec(i,16, 5),i=1,2)/1.400d-10,0.7550/
      data(rrec(i,17, 5),i=1,2)/1.760d-10,0.7682/
      data(rrec(i,18, 5),i=1,2)/2.140d-10,0.7740/
      data(rrec(i,19, 5),i=1,2)/2.629d-10,0.7772/
      data(rrec(i,20, 5),i=1,2)/3.240d-10,0.7800/
      data(rrec(i,21, 5),i=1,2)/3.970d-10,0.7840/
      data(rrec(i,22, 5),i=1,2)/4.700d-10,0.7873/
      data(rrec(i,23, 5),i=1,2)/5.500d-10,0.7910/
      data(rrec(i,24, 5),i=1,2)/6.301d-10,0.7952/
      data(rrec(i,25, 5),i=1,2)/7.058d-10,0.7982/
      data(rrec(i,27, 5),i=1,2)/8.252d-10,0.8004/
      data(rrec(i,28, 5),i=1,2)/8.710d-10,0.8000/
      data(rrec(i,29, 5),i=1,2)/9.170d-10,0.8000/
      data(rrec(i,30, 5),i=1,2)/9.630d-10,0.7990/
      data(rrec(i, 6, 6),i=1,2)/4.700d-13,0.6240/
      data(rrec(i, 7, 6),i=1,2)/2.200d-12,0.6390/
      data(rrec(i, 8, 6),i=1,2)/5.100d-12,0.6600/
      data(rrec(i, 9, 6),i=1,2)/9.171d-12,0.6757/
      data(rrec(i,10, 6),i=1,2)/1.500d-11,0.6840/
      data(rrec(i,11, 6),i=1,2)/2.191d-11,0.6875/
      data(rrec(i,12, 6),i=1,2)/3.200d-11,0.6910/
      data(rrec(i,13, 6),i=1,2)/4.308d-11,0.6970/
      data(rrec(i,14, 6),i=1,2)/5.800d-11,0.7030/
      data(rrec(i,15, 6),i=1,2)/7.316d-11,0.7027/
      data(rrec(i,16, 6),i=1,2)/9.200d-11,0.7140/
      data(rrec(i,17, 6),i=1,2)/1.198d-10,0.7508/
      data(rrec(i,18, 6),i=1,2)/1.580d-10,0.7900/
      data(rrec(i,19, 6),i=1,2)/2.048d-10,0.8032/
      data(rrec(i,20, 6),i=1,2)/2.600d-10,0.8000/
      data(rrec(i,21, 6),i=1,2)/3.280d-10,0.7990/
      data(rrec(i,22, 6),i=1,2)/3.966d-10,0.7973/
      data(rrec(i,23, 6),i=1,2)/4.760d-10,0.8000/
      data(rrec(i,24, 6),i=1,2)/5.547d-10,0.8027/
      data(rrec(i,25, 6),i=1,2)/6.313d-10,0.8058/
      data(rrec(i,27, 6),i=1,2)/7.503d-10,0.8085/
      data(rrec(i,28, 6),i=1,2)/7.940d-10,0.8080/
      data(rrec(i,29, 6),i=1,2)/8.380d-10,0.8080/
      data(rrec(i,30, 6),i=1,2)/8.810d-10,0.8070/
      data(rrec(i, 7, 7),i=1,2)/4.100d-13,0.6080/
      data(rrec(i, 8, 7),i=1,2)/2.000d-12,0.6460/
      data(rrec(i, 9, 7),i=1,2)/5.231d-12,0.6615/
      data(rrec(i,10, 7),i=1,2)/9.100d-12,0.6680/
      data(rrec(i,11, 7),i=1,2)/1.447d-11,0.6814/
      data(rrec(i,12, 7),i=1,2)/2.300d-11,0.6950/
      data(rrec(i,13, 7),i=1,2)/3.145d-11,0.6915/
      data(rrec(i,14, 7),i=1,2)/4.300d-11,0.6880/
      data(rrec(i,15, 7),i=1,2)/5.659d-11,0.7023/
      data(rrec(i,16, 7),i=1,2)/7.400d-11,0.7160/
      data(rrec(i,17, 7),i=1,2)/9.561d-11,0.7102/
      data(rrec(i,18, 7),i=1,2)/1.230d-10,0.7020/
      data(rrec(i,19, 7),i=1,2)/1.587d-10,0.7105/
      data(rrec(i,20, 7),i=1,2)/2.040d-10,0.7300/
      data(rrec(i,21, 7),i=1,2)/2.630d-10,0.7490/
      data(rrec(i,22, 7),i=1,2)/3.220d-10,0.7683/
      data(rrec(i,23, 7),i=1,2)/3.950d-10,0.7830/
      data(rrec(i,24, 7),i=1,2)/4.671d-10,0.7967/
      data(rrec(i,25, 7),i=1,2)/5.407d-10,0.8058/
      data(rrec(i,27, 7),i=1,2)/6.611d-10,0.8121/
      data(rrec(i,28, 7),i=1,2)/7.080d-10,0.8110/
      data(rrec(i,29, 7),i=1,2)/7.550d-10,0.8100/
      data(rrec(i,30, 7),i=1,2)/8.020d-10,0.8090/
      data(rrec(i, 8, 8),i=1,2)/3.100d-13,0.6780/
      data(rrec(i, 9, 8),i=1,2)/1.344d-12,0.6708/
      data(rrec(i,10, 8),i=1,2)/4.400d-12,0.6750/
      data(rrec(i,11, 8),i=1,2)/7.849d-12,0.6952/
      data(rrec(i,12, 8),i=1,2)/1.400d-11,0.7160/
      data(rrec(i,13, 8),i=1,2)/2.049d-11,0.7090/
      data(rrec(i,14, 8),i=1,2)/3.000d-11,0.7020/
      data(rrec(i,15, 8),i=1,2)/4.125d-11,0.6965/
      data(rrec(i,16, 8),i=1,2)/5.500d-11,0.7110/
      data(rrec(i,17, 8),i=1,2)/7.280d-11,0.7518/
      data(rrec(i,18, 8),i=1,2)/9.550d-11,0.7930/
      data(rrec(i,19, 8),i=1,2)/1.235d-10,0.8052/
      data(rrec(i,20, 8),i=1,2)/1.580d-10,0.8000/
      data(rrec(i,21, 8),i=1,2)/2.060d-10,0.7990/
      data(rrec(i,22, 8),i=1,2)/2.537d-10,0.7977/
      data(rrec(i,23, 8),i=1,2)/3.190d-10,0.8020/
      data(rrec(i,24, 8),i=1,2)/3.844d-10,0.8071/
      data(rrec(i,25, 8),i=1,2)/4.564d-10,0.8124/
      data(rrec(i,27, 8),i=1,2)/5.842d-10,0.8168/
      data(rrec(i,28, 8),i=1,2)/6.380d-10,0.8160/
      data(rrec(i,29, 8),i=1,2)/6.920d-10,0.8150/
      data(rrec(i,30, 8),i=1,2)/7.460d-10,0.8140/
      data(rrec(i, 9, 9),i=1,2)/6.273d-13,0.6798/
      data(rrec(i,10, 9),i=1,2)/1.500d-12,0.6930/
      data(rrec(i,11, 9),i=1,2)/3.399d-12,0.7054/
      data(rrec(i,12, 9),i=1,2)/7.700d-12,0.7180/
      data(rrec(i,13, 9),i=1,2)/1.275d-11,0.7170/
      data(rrec(i,14, 9),i=1,2)/2.110d-11,0.7160/
      data(rrec(i,15, 9),i=1,2)/2.975d-11,0.6945/
      data(rrec(i,16, 9),i=1,2)/4.000d-11,0.6960/
      data(rrec(i,17, 9),i=1,2)/5.281d-11,0.7491/
      data(rrec(i,18, 9),i=1,2)/6.920d-11,0.8110/
      data(rrec(i,19, 9),i=1,2)/9.044d-11,0.8251/
      data(rrec(i,20, 9),i=1,2)/1.180d-10,0.8100/
      data(rrec(i,21, 9),i=1,2)/1.580d-10,0.8040/
      data(rrec(i,22, 9),i=1,2)/1.983d-10,0.7980/
      data(rrec(i,23, 9),i=1,2)/2.570d-10,0.8040/
      data(rrec(i,24, 9),i=1,2)/3.154d-10,0.8101/
      data(rrec(i,25, 9),i=1,2)/3.837d-10,0.8183/
      data(rrec(i,27, 9),i=1,2)/5.147d-10,0.8253/
      data(rrec(i,28, 9),i=1,2)/5.750d-10,0.8240/
      data(rrec(i,29, 9),i=1,2)/6.350d-10,0.8230/
      data(rrec(i,30, 9),i=1,2)/6.960d-10,0.8210/
      data(rrec(i,10,10),i=1,2)/2.200d-13,0.7590/
      data(rrec(i,11,10),i=1,2)/8.775d-13,0.7467/
      data(rrec(i,12,10),i=1,2)/3.500d-12,0.7340/
      data(rrec(i,13,10),i=1,2)/6.481d-12,0.7345/
      data(rrec(i,14,10),i=1,2)/1.200d-11,0.7350/
      data(rrec(i,15,10),i=1,2)/1.834d-11,0.7285/
      data(rrec(i,16,10),i=1,2)/2.700d-11,0.7330/
      data(rrec(i,17,10),i=1,2)/3.711d-11,0.7641/
      data(rrec(i,18,10),i=1,2)/4.900d-11,0.8010/
      data(rrec(i,19,10),i=1,2)/6.444d-11,0.8175/
      data(rrec(i,20,10),i=1,2)/8.510d-11,0.8200/
      data(rrec(i,21,10),i=1,2)/1.170d-10,0.8220/
      data(rrec(i,22,10),i=1,2)/1.494d-10,0.8242/
      data(rrec(i,23,10),i=1,2)/2.010d-10,0.8280/
      data(rrec(i,24,10),i=1,2)/2.525d-10,0.8311/
      data(rrec(i,25,10),i=1,2)/3.177d-10,0.8341/
      data(rrec(i,27,10),i=1,2)/4.552d-10,0.8364/
      data(rrec(i,28,10),i=1,2)/5.250d-10,0.8360/
      data(rrec(i,29,10),i=1,2)/5.950d-10,0.8360/
      data(rrec(i,30,10),i=1,2)/6.650d-10,0.8350/
      data(rrec(i,12,12),i=1,2)/1.400d-13,0.8550/
      data(rrec(i,13,12),i=1,2)/7.197d-13,0.7697/
      data(rrec(i,14,12),i=1,2)/3.700d-12,0.6930/
      data(rrec(i,15,12),i=1,2)/7.980d-12,0.6829/
      data(rrec(i,16,12),i=1,2)/1.200d-11,0.7010/
      data(rrec(i,17,12),i=1,2)/1.800d-11,0.7232/
      data(rrec(i,18,12),i=1,2)/2.690d-11,0.7440/
      data(rrec(i,19,12),i=1,2)/3.748d-11,0.7628/
      data(rrec(i,20,12),i=1,2)/5.040d-11,0.7800/
      data(rrec(i,21,12),i=1,2)/7.240d-11,0.7950/
      data(rrec(i,22,12),i=1,2)/9.440d-11,0.8107/
      data(rrec(i,23,12),i=1,2)/1.350d-10,0.8220/
      data(rrec(i,24,12),i=1,2)/1.751d-10,0.8340/
      data(rrec(i,25,12),i=1,2)/2.298d-10,0.8417/
      data(rrec(i,27,12),i=1,2)/3.461d-10,0.8469/
      data(rrec(i,28,12),i=1,2)/4.030d-10,0.8460/
      data(rrec(i,29,12),i=1,2)/4.600d-10,0.8450/
      data(rrec(i,30,12),i=1,2)/5.170d-10,0.8440/
      data(rrec(i,13,13),i=1,2)/3.980d-13,0.8019/
      data(rrec(i,14,13),i=1,2)/1.000d-12,0.7860/
      data(rrec(i,15,13),i=1,2)/2.558d-12,0.7629/
      data(rrec(i,16,13),i=1,2)/5.700d-12,0.7550/
      data(rrec(i,17,13),i=1,2)/1.011d-11,0.7703/
      data(rrec(i,18,13),i=1,2)/1.580d-11,0.7930/
      data(rrec(i,19,13),i=1,2)/2.448d-11,0.8052/
      data(rrec(i,20,13),i=1,2)/3.760d-11,0.8100/
      data(rrec(i,21,13),i=1,2)/5.870d-11,0.8150/
      data(rrec(i,22,13),i=1,2)/7.972d-11,0.8206/
      data(rrec(i,23,13),i=1,2)/1.130d-10,0.8270/
      data(rrec(i,24,13),i=1,2)/1.470d-10,0.8325/
      data(rrec(i,25,13),i=1,2)/1.908d-10,0.8372/
      data(rrec(i,27,13),i=1,2)/2.976d-10,0.8406/
      data(rrec(i,28,13),i=1,2)/3.630d-10,0.8400/
      data(rrec(i,29,13),i=1,2)/4.280d-10,0.8390/
      data(rrec(i,30,13),i=1,2)/4.940d-10,0.8390/
      data(rrec(i,14,14),i=1,2)/5.900d-13,0.6010/
      data(rrec(i,15,14),i=1,2)/1.294d-12,0.6766/
      data(rrec(i,16,14),i=1,2)/2.700d-12,0.7450/
      data(rrec(i,17,14),i=1,2)/5.165d-12,0.7893/
      data(rrec(i,18,14),i=1,2)/9.120d-12,0.8110/
      data(rrec(i,19,14),i=1,2)/1.513d-11,0.8186/
      data(rrec(i,20,14),i=1,2)/2.400d-11,0.8200/
      data(rrec(i,21,14),i=1,2)/3.960d-11,0.8220/
      data(rrec(i,22,14),i=1,2)/5.518d-11,0.8245/
      data(rrec(i,23,14),i=1,2)/8.370d-11,0.8280/
      data(rrec(i,24,14),i=1,2)/1.123d-10,0.8313/
      data(rrec(i,25,14),i=1,2)/1.525d-10,0.8342/
      data(rrec(i,26,14),i=1,2)/2.000d-10,0.8360/
      data(rrec(i,27,14),i=1,2)/2.537d-10,0.8364/
      data(rrec(i,28,14),i=1,2)/3.160d-10,0.8360/
      data(rrec(i,29,14),i=1,2)/3.780d-10,0.8360/
      data(rrec(i,30,14),i=1,2)/4.410d-10,0.8350/
      data(rrec(i,15,15),i=1,2)/9.761d-13,0.6209/
      data(rrec(i,16,15),i=1,2)/1.800d-12,0.6860/
      data(rrec(i,17,15),i=1,2)/3.320d-12,0.7579/
      data(rrec(i,18,15),i=1,2)/6.030d-12,0.8120/
      data(rrec(i,19,15),i=1,2)/1.063d-11,0.8269/
      data(rrec(i,20,15),i=1,2)/1.800d-11,0.8200/
      data(rrec(i,21,15),i=1,2)/3.130d-11,0.8180/
      data(rrec(i,22,15),i=1,2)/4.451d-11,0.8153/
      data(rrec(i,23,15),i=1,2)/6.830d-11,0.8200/
      data(rrec(i,24,15),i=1,2)/9.206d-11,0.8246/
      data(rrec(i,25,15),i=1,2)/1.250d-10,0.8302/
      data(rrec(i,26,15),i=1,2)/1.640d-10,0.8340/
      data(rrec(i,27,15),i=1,2)/2.093d-10,0.8349/
      data(rrec(i,28,15),i=1,2)/2.630d-10,0.8340/
      data(rrec(i,29,15),i=1,2)/3.170d-10,0.8330/
      data(rrec(i,30,15),i=1,2)/3.700d-10,0.8320/
      data(rrec(i,16,16),i=1,2)/4.100d-13,0.6300/
      data(rrec(i,17,16),i=1,2)/1.248d-12,0.7663/
      data(rrec(i,18,16),i=1,2)/3.230d-12,0.8690/
      data(rrec(i,19,16),i=1,2)/6.384d-12,0.8790/
      data(rrec(i,20,16),i=1,2)/1.070d-11,0.8400/
      data(rrec(i,21,16),i=1,2)/1.920d-11,0.8210/
      data(rrec(i,22,16),i=1,2)/2.765d-11,0.8012/
      data(rrec(i,23,16),i=1,2)/4.650d-11,0.8060/
      data(rrec(i,24,16),i=1,2)/6.539d-11,0.8099/
      data(rrec(i,25,16),i=1,2)/9.539d-11,0.8202/
      data(rrec(i,26,16),i=1,2)/1.330d-10,0.8280/
      data(rrec(i,27,16),i=1,2)/1.769d-10,0.8299/
      data(rrec(i,28,16),i=1,2)/2.290d-10,0.8280/
      data(rrec(i,29,16),i=1,2)/2.810d-10,0.8260/
      data(rrec(i,30,16),i=1,2)/3.330d-10,0.8240/
      data(rrec(i,17,17),i=1,2)/1.010d-12,0.7380/
      data(rrec(i,18,17),i=1,2)/1.950d-12,0.7520/
      data(rrec(i,19,17),i=1,2)/3.766d-12,0.7662/
      data(rrec(i,20,17),i=1,2)/7.080d-12,0.7800/
      data(rrec(i,21,17),i=1,2)/1.430d-11,0.7920/
      data(rrec(i,22,17),i=1,2)/2.152d-11,0.8038/
      data(rrec(i,23,17),i=1,2)/3.740d-11,0.8120/
      data(rrec(i,24,17),i=1,2)/5.335d-11,0.8207/
      data(rrec(i,25,17),i=1,2)/7.807d-11,0.8260/
      data(rrec(i,26,17),i=1,2)/1.090d-10,0.8290/
      data(rrec(i,27,17),i=1,2)/1.459d-10,0.8296/
      data(rrec(i,28,17),i=1,2)/1.910d-10,0.8290/
      data(rrec(i,29,17),i=1,2)/2.360d-10,0.8280/
      data(rrec(i,30,17),i=1,2)/2.810d-10,0.8280/
      data(rrec(i,18,18),i=1,2)/3.770d-13,0.6510/
      data(rrec(i,19,18),i=1,2)/1.304d-12,0.6753/
      data(rrec(i,20,18),i=1,2)/3.960d-12,0.7000/
      data(rrec(i,21,18),i=1,2)/1.130d-11,0.7240/
      data(rrec(i,22,18),i=1,2)/1.857d-11,0.7484/
      data(rrec(i,23,18),i=1,2)/3.170d-11,0.7680/
      data(rrec(i,24,18),i=1,2)/4.479d-11,0.7883/
      data(rrec(i,25,18),i=1,2)/6.106d-11,0.8020/
      data(rrec(i,26,18),i=1,2)/8.130d-11,0.8100/
      data(rrec(i,27,18),i=1,2)/1.098d-10,0.8118/
      data(rrec(i,28,18),i=1,2)/1.500d-10,0.8100/
      data(rrec(i,29,18),i=1,2)/1.900d-10,0.8080/
      data(rrec(i,30,18),i=1,2)/2.300d-10,0.8060/
      data(rrec(i,19,19),i=1,2)/2.762d-13,0.8023/
      data(rrec(i,20,19),i=1,2)/6.780d-13,0.8000/
      data(rrec(i,21,19),i=1,2)/2.330d-12,0.7980/
      data(rrec(i,22,19),i=1,2)/3.983d-12,0.7955/
      data(rrec(i,23,19),i=1,2)/1.150d-11,0.7940/
      data(rrec(i,24,19),i=1,2)/1.906d-11,0.7919/
      data(rrec(i,25,19),i=1,2)/3.620d-11,0.7907/
      data(rrec(i,26,19),i=1,2)/6.050d-11,0.7900/
      data(rrec(i,27,19),i=1,2)/8.818d-11,0.7898/
      data(rrec(i,28,19),i=1,2)/1.190d-10,0.7900/
      data(rrec(i,29,19),i=1,2)/1.500d-10,0.7900/
      data(rrec(i,30,19),i=1,2)/1.810d-10,0.7900/
      data(rrec(i,20,20),i=1,2)/1.120d-13,0.9000/
      data(rrec(i,21,20),i=1,2)/6.540d-13,0.8670/
      data(rrec(i,22,20),i=1,2)/1.196d-12,0.8344/
      data(rrec(i,23,20),i=1,2)/5.330d-12,0.8090/
      data(rrec(i,24,20),i=1,2)/9.471d-12,0.7846/
      data(rrec(i,25,20),i=1,2)/2.169d-11,0.7683/
      data(rrec(i,26,20),i=1,2)/4.120d-11,0.7590/
      data(rrec(i,27,20),i=1,2)/6.409d-11,0.7570/
      data(rrec(i,28,20),i=1,2)/8.910d-11,0.7590/
      data(rrec(i,29,20),i=1,2)/1.140d-10,0.7610/
      data(rrec(i,30,20),i=1,2)/1.390d-10,0.7630/
      data(rrec(i,21,21),i=1,2)/1.170d-13,0.8980/
      data(rrec(i,22,21),i=1,2)/5.330d-12,0.8640/
      data(rrec(i,23,21),i=1,2)/1.060d-11,0.8300/
      data(rrec(i,24,21),i=1,2)/1.580d-11,0.7960/
      data(rrec(i,25,21),i=1,2)/2.100d-11,0.7620/
      data(rrec(i,26,21),i=1,2)/2.620d-11,0.7280/
      data(rrec(i,27,21),i=1,2)/2.822d-11,0.7280/
      data(rrec(i,28,21),i=1,2)/3.040d-11,0.7280/
      data(rrec(i,29,21),i=1,2)/3.260d-11,0.7280/
      data(rrec(i,30,21),i=1,2)/3.480d-11,0.7280/
      data(rrec(i,22,22),i=1,2)/1.220d-13,0.8970/
      data(rrec(i,23,22),i=1,2)/3.870d-12,0.8480/
      data(rrec(i,24,22),i=1,2)/7.610d-12,0.7980/
      data(rrec(i,25,22),i=1,2)/1.140d-11,0.7480/
      data(rrec(i,26,22),i=1,2)/1.510d-11,0.6990/
      data(rrec(i,27,22),i=1,2)/1.626d-11,0.6990/
      data(rrec(i,28,22),i=1,2)/1.750d-11,0.6990/
      data(rrec(i,29,22),i=1,2)/1.870d-11,0.6990/
      data(rrec(i,30,22),i=1,2)/2.000d-11,0.6990/
      data(rrec(i,23,23),i=1,2)/1.270d-13,0.8950/
      data(rrec(i,24,23),i=1,2)/2.680d-12,0.8240/
      data(rrec(i,25,23),i=1,2)/5.240d-12,0.7530/
      data(rrec(i,26,23),i=1,2)/7.800d-12,0.6820/
      data(rrec(i,27,23),i=1,2)/8.402d-12,0.6820/
      data(rrec(i,28,23),i=1,2)/9.050d-12,0.6820/
      data(rrec(i,29,23),i=1,2)/9.700d-12,0.6820/
      data(rrec(i,30,23),i=1,2)/1.030d-11,0.6820/
      data(rrec(i,24,24),i=1,2)/1.320d-13,0.8940/
      data(rrec(i,25,24),i=1,2)/1.730d-12,0.8200/
      data(rrec(i,26,24),i=1,2)/3.320d-12,0.7460/
      data(rrec(i,27,24),i=1,2)/3.575d-12,0.7460/
      data(rrec(i,28,24),i=1,2)/3.580d-12,0.7460/
      data(rrec(i,29,24),i=1,2)/3.580d-12,0.7460/
      data(rrec(i,30,24),i=1,2)/3.590d-12,0.7460/
      data(rrec(i,25,25),i=1,2)/1.370d-13,0.8920/
      data(rrec(i,26,25),i=1,2)/1.020d-12,0.8430/
      data(rrec(i,27,25),i=1,2)/1.278d-12,0.7682/
      data(rrec(i,28,25),i=1,2)/1.600d-12,0.7000/
      data(rrec(i,29,25),i=1,2)/1.920d-12,0.7000/
      data(rrec(i,30,25),i=1,2)/2.240d-12,0.7000/
      data(rrec(i,26,26),i=1,2)/1.420d-13,0.8910/
      data(rrec(i,27,26),i=1,2)/4.459d-13,0.7897/
      data(rrec(i,28,26),i=1,2)/1.400d-12,0.7000/
      data(rrec(i,29,26),i=1,2)/1.500d-12,0.7000/
      data(rrec(i,30,26),i=1,2)/1.600d-12,0.7000/
      data(rrec(i,27,27),i=1,2)/2.510d-13,0.7950/
      data(rrec(i,28,27),i=1,2)/1.000d-12,0.7000/
      data(rrec(i,29,27),i=1,2)/1.000d-12,0.7000/
      data(rrec(i,30,27),i=1,2)/1.100d-12,0.7000/
      data(rrec(i,28,28),i=1,2)/3.600d-13,0.7000/
      data(rrec(i,29,28),i=1,2)/3.600d-13,0.7000/
      data(rrec(i,30,28),i=1,2)/3.600d-13,0.7000/
      data(rrec(i,29,29),i=1,2)/3.600d-13,0.7000/
      data(rrec(i,30,29),i=1,2)/3.600d-13,0.7000/
      data(rrec(i,30,30),i=1,2)/3.600d-13,0.7000/
      data(rnew(i, 1, 1),i=1,4)/7.982d-11,0.7480,3.148d+00,7.036d+05/
      data(rnew(i, 2, 1),i=1,4)/1.891d-10,0.7524,9.370d+00,2.774d+06/
      data(rnew(i, 3, 1),i=1,4)/3.039d-10,0.7539,1.871d+01,6.209d+06/
      data(rnew(i, 4, 1),i=1,4)/4.290d-10,0.7557,3.000d+01,1.093d+07/
      data(rnew(i, 5, 1),i=1,4)/5.437d-10,0.7560,4.576d+01,1.706d+07/
      data(rnew(i, 6, 1),i=1,4)/6.556d-10,0.7567,6.523d+01,2.446d+07/
      data(rnew(i, 7, 1),i=1,4)/7.586d-10,0.7563,9.015d+01,3.338d+07/
      data(rnew(i, 8, 1),i=1,4)/8.616d-10,0.7563,1.191d+02,4.352d+07/
      data(rnew(i, 9, 1),i=1,4)/9.712d-10,0.7566,1.499d+02,5.498d+07/
      data(rnew(i,10, 1),i=1,4)/1.085d-09,0.7570,1.834d+02,6.776d+07/
      data(rnew(i,11, 1),i=1,4)/1.163d-09,0.7558,2.328d+02,8.262d+07/
      data(rnew(i,12, 1),i=1,4)/1.317d-09,0.7574,2.585d+02,9.769d+07/
      data(rnew(i,13, 1),i=1,4)/1.419d-09,0.7578,3.057d+02,1.143d+08/
      data(rnew(i,14, 1),i=1,4)/1.517d-09,0.7574,3.601d+02,1.329d+08/
      data(rnew(i,15, 1),i=1,4)/1.586d-09,0.7560,4.327d+02,1.534d+08/
      data(rnew(i,16, 1),i=1,4)/1.729d-09,0.7568,4.725d+02,1.746d+08/
      data(rnew(i,17, 1),i=1,4)/1.791d-09,0.7565,5.591d+02,1.972d+08/
      data(rnew(i,18, 1),i=1,4)/1.913d-09,0.7567,6.175d+02,2.212d+08/
      data(rnew(i,19, 1),i=1,4)/2.033d-09,0.7569,6.797d+02,2.463d+08/
      data(rnew(i,20, 1),i=1,4)/2.129d-09,0.7570,7.591d+02,2.739d+08/
      data(rnew(i,21, 1),i=1,4)/2.262d-09,0.7578,8.186d+02,3.000d+08/
      data(rnew(i,22, 1),i=1,4)/2.370d-09,0.7574,9.002d+02,3.307d+08/
      data(rnew(i,23, 1),i=1,4)/2.415d-09,0.7565,1.032d+03,3.635d+08/
      data(rnew(i,24, 1),i=1,4)/2.537d-09,0.7571,1.108d+03,3.954d+08/
      data(rnew(i,25, 1),i=1,4)/2.618d-09,0.7565,1.225d+03,4.307d+08/
      data(rnew(i,26, 1),i=1,4)/2.735d-09,0.7568,1.314d+03,4.659d+08/
      data(rnew(i,27, 1),i=1,4)/2.809d-09,0.7565,1.444d+03,5.042d+08/
      data(rnew(i,28, 1),i=1,4)/3.002d-09,0.7581,1.467d+03,5.409d+08/
      data(rnew(i,29, 1),i=1,4)/3.022d-09,0.7564,1.666d+03,5.855d+08/
      data(rnew(i,30, 1),i=1,4)/3.127d-09,0.7567,1.779d+03,6.246d+08/
      data(rnew(i, 2, 2),i=1,4)/9.356d-10,0.7892,4.266d-02,4.677d+06/
      data(rnew(i, 3, 2),i=1,4)/1.112d-10,0.6926,2.437d+01,8.323d+06/
      data(rnew(i, 4, 2),i=1,4)/1.317d-10,0.6691,8.473d+01,1.412d+07/
      data(rnew(i, 5, 2),i=1,4)/1.922d-10,0.6717,1.272d+02,1.975d+07/
      data(rnew(i, 6, 2),i=1,4)/2.765d-10,0.6858,1.535d+02,2.556d+07/
      data(rnew(i, 7, 2),i=1,4)/3.910d-10,0.6988,1.611d+02,3.271d+07/
      data(rnew(i, 8, 2),i=1,4)/4.897d-10,0.7048,1.906d+02,4.093d+07/
      data(rnew(i, 9, 2),i=1,4)/5.602d-10,0.7052,2.476d+02,5.077d+07/
      data(rnew(i,10, 2),i=1,4)/6.161d-10,0.7029,3.274d+02,6.243d+07/
      data(rnew(i,11, 2),i=1,4)/6.833d-10,0.7018,4.060d+02,7.491d+07/
      data(rnew(i,12, 2),i=1,4)/7.510d-10,0.7020,4.921d+02,8.643d+07/
      data(rnew(i,13, 2),i=1,4)/8.182d-10,0.7008,5.875d+02,1.007d+08/
      data(rnew(i,14, 2),i=1,4)/8.722d-10,0.6996,7.098d+02,1.155d+08/
      data(rnew(i,15, 2),i=1,4)/9.142d-10,0.6961,8.682d+02,1.335d+08/
      data(rnew(i,16, 2),i=1,4)/9.692d-10,0.6945,1.017d+03,1.517d+08/
      data(rnew(i,17, 2),i=1,4)/1.021d-09,0.6932,1.184d+03,1.695d+08/
      data(rnew(i,18, 2),i=1,4)/1.087d-09,0.6936,1.329d+03,1.880d+08/
      data(rnew(i,19, 2),i=1,4)/1.145d-09,0.6921,1.503d+03,2.098d+08/
      data(rnew(i,20, 2),i=1,4)/1.179d-09,0.6893,1.757d+03,2.344d+08/
      data(rnew(i,21, 2),i=1,4)/1.265d-09,0.6902,1.877d+03,2.555d+08/
      data(rnew(i,22, 2),i=1,4)/1.322d-09,0.6885,2.092d+03,2.829d+08/
      data(rnew(i,23, 2),i=1,4)/1.375d-09,0.6885,2.321d+03,3.056d+08/
      data(rnew(i,24, 2),i=1,4)/1.422d-09,0.6874,2.589d+03,3.336d+08/
      data(rnew(i,25, 2),i=1,4)/1.488d-09,0.6867,2.802d+03,3.623d+08/
      data(rnew(i,26, 2),i=1,4)/1.542d-09,0.6859,3.073d+03,3.926d+08/
      data(rnew(i,27, 2),i=1,4)/1.589d-09,0.6846,3.373d+03,4.267d+08/
      data(rnew(i,28, 2),i=1,4)/1.676d-09,0.6861,3.530d+03,4.538d+08/
      data(rnew(i,29, 2),i=1,4)/1.686d-09,0.6824,4.031d+03,4.948d+08/
      data(rnew(i,30, 2),i=1,4)/1.758d-09,0.6834,4.254d+03,5.258d+08/
      data(rnew(i, 3, 3),i=1,4)/1.036d-11,0.3880,1.077d+02,1.177d+07/
      data(rnew(i, 4, 3),i=1,4)/2.338d-11,0.4211,3.647d+02,1.215d+07/
      data(rnew(i, 5, 3),i=1,4)/4.487d-11,0.4644,5.371d+02,1.465d+07/
      data(rnew(i, 6, 3),i=1,4)/8.540d-11,0.5247,5.014d+02,1.479d+07/
      data(rnew(i, 7, 3),i=1,4)/1.169d-10,0.5470,6.793d+02,1.650d+07/
      data(rnew(i, 8, 3),i=1,4)/2.053d-10,0.6019,4.772d+02,1.711d+07/
      data(rnew(i, 9, 3),i=1,4)/2.739d-10,0.6188,5.033d+02,2.064d+07/
      data(rnew(i,10, 3),i=1,4)/3.200d-10,0.6198,6.329d+02,2.616d+07/
      data(rnew(i,11, 3),i=1,4)/3.873d-10,0.6295,7.000d+02,2.989d+07/
      data(rnew(i,12, 3),i=1,4)/4.284d-10,0.6287,8.748d+02,3.586d+07/
      data(rnew(i,13, 3),i=1,4)/4.881d-10,0.6326,9.941d+02,4.085d+07/
      data(rnew(i,14, 3),i=1,4)/5.373d-10,0.6337,1.164d+03,4.677d+07/
      data(rnew(i,15, 3),i=1,4)/5.876d-10,0.6354,1.341d+03,5.292d+07/
      data(rnew(i,16, 3),i=1,4)/6.571d-10,0.6400,1.452d+03,5.796d+07/
      data(rnew(i,17, 3),i=1,4)/7.076d-10,0.6397,1.653d+03,6.555d+07/
      data(rnew(i,18, 3),i=1,4)/7.538d-10,0.6388,1.889d+03,7.306d+07/
      data(rnew(i,19, 3),i=1,4)/8.182d-10,0.6411,2.044d+03,8.057d+07/
      data(rnew(i,20, 3),i=1,4)/8.577d-10,0.6403,2.334d+03,8.850d+07/
      data(rnew(i,21, 3),i=1,4)/9.162d-10,0.6413,2.543d+03,9.690d+07/
      data(rnew(i,22, 3),i=1,4)/9.844d-10,0.6440,2.708d+03,1.044d+08/
      data(rnew(i,23, 3),i=1,4)/1.020d-09,0.6427,3.057d+03,1.140d+08/
      data(rnew(i,24, 3),i=1,4)/1.091d-09,0.6445,3.225d+03,1.229d+08/
      data(rnew(i,25, 3),i=1,4)/1.151d-09,0.6451,3.461d+03,1.334d+08/
      data(rnew(i,26, 3),i=1,4)/1.198d-09,0.6443,3.789d+03,1.437d+08/
      data(rnew(i,27, 3),i=1,4)/1.211d-09,0.6406,4.357d+03,1.572d+08/
      data(rnew(i,28, 3),i=1,4)/1.288d-09,0.6440,4.506d+03,1.651d+08/
      data(rnew(i,29, 3),i=1,4)/1.372d-09,0.6472,4.627d+03,1.740d+08/
      data(rnew(i,30, 3),i=1,4)/1.412d-09,0.6454,5.053d+03,1.891d+08/
      data(rnew(i,11,11),i=1,4)/5.641d-12,0.1749,3.077d+02,2.617d+06/
      data(rnew(i,12,11),i=1,4)/1.920d-11,0.3028,4.849d+02,5.890d+06/
      data(rnew(i,13,11),i=1,4)/3.753d-11,0.3585,6.848d+02,9.035d+06/
      data(rnew(i,14,11),i=1,4)/5.942d-11,0.3930,8.962d+02,1.213d+07/
      data(rnew(i,15,11),i=1,4)/1.721d-10,0.5429,2.848d+02,3.975d+07/
      data(rnew(i,16,11),i=1,4)/3.502d-10,0.6266,1.532d+02,1.755d+07/
      data(rnew(i,17,11),i=1,4)/2.502d-10,0.5580,5.303d+02,4.558d+07/
      data(rnew(i,18,11),i=1,4)/2.862d-10,0.5621,7.002d+02,4.885d+07/
      data(rnew(i,19,11),i=1,4)/2.757d-10,0.5364,1.204d+03,7.013d+07/
      data(rnew(i,20,11),i=1,4)/5.273d-10,0.6281,5.329d+02,3.188d+07/
      data(rnew(i,21,11),i=1,4)/3.890d-10,0.5645,1.391d+03,6.295d+07/
      data(rnew(i,22,11),i=1,4)/4.207d-10,0.5646,1.688d+03,6.872d+07/
      data(rnew(i,23,11),i=1,4)/4.605d-10,0.5659,1.949d+03,7.419d+07/
      data(rnew(i,24,11),i=1,4)/4.975d-10,0.5655,2.257d+03,8.072d+07/
      data(rnew(i,25,11),i=1,4)/5.349d-10,0.5658,2.577d+03,8.710d+07/
      data(rnew(i,26,11),i=1,4)/7.688d-10,0.6173,1.653d+03,6.161d+07/
      data(rnew(i,27,11),i=1,4)/5.850d-10,0.5598,3.538d+03,1.052d+08/
      data(rnew(i,28,11),i=1,4)/6.347d-10,0.5631,3.780d+03,1.116d+08/
      data(rnew(i,29,11),i=1,4)/6.619d-10,0.5602,4.322d+03,1.210d+08/
      data(rnew(i,30,11),i=1,4)/7.002d-10,0.5612,4.726d+03,1.287d+08/
      data(rnew(i, 6, 4),i=1,4)/2.020D-09,0.7798,6.690D-01,2.425D+06/
      data(rnew(i, 7, 4),i=1,4)/1.595D-11,0.3529,9.870D+03,2.584D+07/
      data(rnew(i, 8, 4),i=1,4)/2.008D-11,0.3567,1.520D+04,3.843D+07/
      data(rnew(i,10, 4),i=1,4)/2.793D-11,0.3533,3.017D+04,7.872D+07/
      data(rnew(i, 6, 5),i=1,4)/8.577D-10,0.7837,7.286D-01,1.140D+07/
      data(rnew(i, 7, 5),i=1,4)/7.039D-10,0.8607,2.203D+00,3.029D+06/
      data(rnew(i, 8, 5),i=1,4)/1.542D-10,0.6712,1.775D+02,1.535D+08/
      data(rnew(i,10, 5),i=1,4)/2.515D-10,0.7011,3.028D+02,8.903D+06/
      data(rnew(i, 6, 6),i=1,4)/7.651D-09,0.8027,1.193D-03,9.334D+12/
      data(rnew(i, 7, 6),i=1,4)/5.989D-10,0.7560,1.377D+00,1.517D+09/
      data(rnew(i, 8, 6),i=1,4)/3.672D-09,0.7676,2.900D-01,8.521D+07/
      data(rnew(i,10, 6),i=1,4)/9.353D-11,0.6270,9.031D+02,2.387D+07/
      data(rnew(i, 7, 7),i=1,4)/1.243D-08,0.7022,1.136D-03,1.015D+13/
      data(rnew(i, 8, 7),i=1,4)/1.816D-08,0.7170,6.717D-03,3.286D+12/
      data(rnew(i,10, 7),i=1,4)/4.227D-11,0.5395,1.687D+03,1.491D+17/
      data(rnew(i, 8, 8),i=1,4)/1.341D-10,0.6159,1.673D+00,6.366D+16/
      data(rnew(i,10, 8),i=1,4)/9.563D-12,0.3067,9.768D+03,4.851D+17/
      data(rnew(i,10, 9),i=1,4)/5.417D-08,0.6930,1.179D-03,1.060D+07/
      data(rnew(i,10,10),i=1,4)/5.023D-12,0.2420,3.181D+02,1.450D+18/
      data(rnew(i,26,12),i=1,4)/8.110D-10,0.5442,1.755D+03,6.799D+07/ 
      data(rnew(i,26,13),i=1,4)/6.967D-10,0.5602,1.727D+03,5.618D+07/ 
      data(rnew(i,26,14),i=1,4)/3.236D-08,0.3247,2.338D+01,8.337D+12/
      data(rnew(i,26,15),i=1,4)/2.664D-08,0.3285,2.297D+01,6.672D+12/
      data(rnew(i,26,16),i=1,4)/2.165D-08,0.3403,2.195D+01,6.383D+12/
      data(rnew(i,26,17),i=1,4)/2.460D-08,0.3387,1.487D+01,5.228D+12/
      data(rnew(i,26,18),i=1,4)/1.907D-08,0.3768,1.216D+01,5.431D+12/
      data(rnew(i,26,19),i=1,4)/1.439D-08,0.4170,1.006D+01,4.898D+12/
      data(rnew(i,26,20),i=1,4)/1.184D-08,0.4798,5.883D+00,2.582D+12/
      data(rnew(i,26,21),i=1,4)/1.036D-08,0.5428,2.743D+00,1.014D+12/
      data(rnew(i,26,22),i=1,4)/8.288D-09,0.6012,1.216D+00,1.182D+12/
      data(rnew(i,26,23),i=1,4)/6.330D-09,0.6355,5.457D-01,8.545D+11/
      data(rnew(i,26,24),i=1,4)/5.422D-09,0.5067,4.998D-01,8.079D+11/
      data(rnew(i,26,25),i=1,4)/6.076D-09,0.3112,3.401D-01,1.960D+12/
      data(rnew(i,26,26),i=1,4)/8.945D-09,0.2156,4.184D-02,5.353D+13/
      data(fe(1,i),i=4,13)/4.33d-10,3.91d-10,3.49d-10,3.16d-10,
     &2.96d-10,2.59d-10,2.24d-10,1.91d-10,1.68d-10,1.46d-10/
      data(fe(2,i),i=4,13)/0.531d0,0.523d0,0.521d0,0.534d0,
     &0.557d0,0.567d0,0.579d0,0.601d0,0.602d0,0.597d0/
      data(fe(3,i),i=4,13)/5.77d-02,6.15d-02,6.22d-02,6.02d-02,
     &5.79d-02,5.65d-02,5.49d-02,5.10d-02,5.07d-02,5.22d-02/
      end                                                        ! radrec-rrfit
c--============================================================================
c
