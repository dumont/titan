      program GRAFTIT                                 ! jan 2019
c v105.03

! to get a spectrum, continuum and lines together ready for a graph :
! - 2 pts for each side of lines are added, then hnu are sorted
! - if "gaussian" all points corresponding to a resolution 2000 are
!       extrapolated, then convoluated by a gaussian function
! - if "triangle" hnu are sorted and points of same frequency are summed
! - if iraie=2, lines are gaussian, output spectrum is versus lambda(A)
!       with a constant width in lambda, range is 100ev to 25kev
! - if iraie=3, there can be turbulence, bin_lambda=constant
!
! input : file name.res or name.an2 or name.ang
! output : file name.speg with equivalent widths and nuF(nu)=f(hnuev)
c
c pour presenter mieux les graphes, continu et raies ensemble :
c - on rajoute 2 pts de chaque cote des raies et on trie les hnu
c - si "raies gaussiennes" (iraie=0), on calcule par extrapolation tous les
c   points correspondant a une resolution 5 fois superieure a celle demandee,
c   puis on convole par une gaussienne : on obtient 5 points par element
c   resolu, d'ou de gros fichiers pour de grandes resolutions
c - si "raies en triangle" (iraie=1), on trie et somme les points de meme 
c   frequence seulement, on a les points du continu + 3 points par raie
c   les fichiers sont moins gros, mais les raies sont moins jolies.
c les spectres contiennent alors nuF(nu)=fonction de hnu(ev) avec 
c   une largeur a mi-hauteur de raie dnu=nu/R
c - si iraie=2, les raies sont gaussiennes, le spectre va de 100ev a 25kev,
c   le spectre contient alors nuF(nu)=fonction de lambda(A) avec 
c   une largeur a mi-hauteur de raie dlambda=constante
c - si iraie=3, on peut traiter un elargissement des raies par turbulence,
c   le pas est constant en lambda
c
c entree : fichier xxx.res  ordinaire ou xxx.an2 ou xxx.ang
c sortie : fichier xxx.speg contenant largeurs equivalentes (si demande') et 
c          nuF(nu)=f(hnuev) pour graphique
cc                 si "raies gaussiennes"
cc                 R <=100 ->230pts/decade soit 1258pts de 0.01 a 1e5ev
cc                 R =200 -> 4124pts de 0.01 a 1e5ev
cc                 R =400 -> 6855pts de 0.01 a 1e5ev
cc                 R >400 -> nbre de pts variable          ???

        implicit double precision (a-h,o-z)
        parameter (nrais=4200)
        character ecrit1*20,ecrit2*59,sep*1,ligne*80,nomrai*7
        character fich*51,fiche*55,fichs*55
        dimension rdir(20),sdir(20)
        common/grkcr/nraimax,ntab,kf,kcr(nrais)
        common/gr/s(nrais*3),r(nrais*3),t(nrais*3),e(nrais*3),
     &            fic(500),sc(500),rc(500),tc(500),ec(500)
        common/grc/thnuc(500),alo(nrais)
        common/grl/lnur(nrais*3),lnuc(500),nk(500),ieqw,ila
        common/gtab/thnur(nrais)
        common/nom/nomrai(0:nrais)
c main
c        tab=';'
        ntab=nrais
        WRITE(6,*)'  english (enter 1) - separator in the file is blank'
        WRITE(6,*)'  ou francais (enter 0) - le separateur est ";"  ?'
        READ(5,*) ila
        if(ila.ne.0) ila=1
           if(ila.eq.0) then
        WRITE(6,*)'  le fichier doit comporter une ligne avec <hnuev>'//1235678
     &            'et une autre avec <hnuraie>'
cle fichier doit comporter une ligne avec <hnuev> et une autre avec <hnuraie>
        WRITE(6,*)
     &  '  NOM DU FICHIER (sans ".res" ni ".an2" ni ".ang")'
           else
        WRITE(6,*) '  file must have one line with <hnuev>'//
     &             ' and one line with <line-hnu>'
cfile must have one line with <hnuev> and one line with <line-hnu>
        WRITE(6,*)
     &  '  FILE NAME (without ".res" nor ".an2" nor ".ang") ?'
           endif
        READ(5,'(a)') fich
        nc=index(fich,' ') - 1
c                                                                           !m
           if(ila.eq.0) then
        WRITE(6,*)
     &  '  une seule (.res) ou plusieurs directions (.an2 ou ang) ? '
        WRITE(6,*) '  entrer le NOMBRE TOTAL de DIRECTIONS'//
     &             '                        (0 0 pour res)'
        WRITE(6,*) '  et le NUMERO de la direction pour laquelle on '//
     &             'veut le spectre (ex: 6 1)'
           else
        WRITE(6,*) '  one (.res) or several directions (.an2 or ang) ? '
        WRITE(6,*) '  enter the TOTAL NUMBER OF DIRECTIONS'//
     &             ' of the run              (0 0 if .res)'
        WRITE(6,*) '  and the NUMBER of the direction for which '//
     &             'spectrum is wanted (example: 6 1)'
c  entrer le NOMBRE TOTAL de DIRECTIONS                        (0 0 pour res)
c  et le NUMERO de la direction pour laquelle on veut le spectre (ex: 6 1)
c  enter the TOTAL NUMBER OF DIRECTIONS of the run              (0 0 if .res)
c  and the NUMBER of the direction for which spectrum is wanted (example: 6 1)
           endif
        READ(5,*) nbang,idir

        if(nbang.eq.0) then
           fiche=fich(:nc)//'.res'
        else
           fiche=fich(:nc)//'.an2'
        endif
        fichs=fich(:nc)//'.speg'
        if(nbang.eq.0) then
           OPEN (UNIT=13,FILE=fiche,STATUS='old')
           goto 8
        else
           OPEN (UNIT=13,FILE=fiche,STATUS='old',ERR=7)
           goto 8                                                           !m
        endif
 7      fiche=fich(:nc)//'.ang'
           OPEN (UNIT=13,FILE=fiche,STATUS='old')
 8      continue
       if(ila.eq.0) WRITE(6,*)
     & '     nom du fichier en entree: ',fiche(:nc+4)
       if(ila.eq.1) WRITE(6,*)'     input file name: ',fiche(:nc+4)
       if(ila.eq.0) WRITE(6,*)
     & '     nom du fichier en sortie: ',fichs(:nc+5)
       if(ila.eq.1) WRITE(6,*)'     output file name: ',fichs(:nc+5)
        OPEN (UNIT=14,FILE=fichs,STATUS='new')
           if(ila.eq.0) WRITE(6,*)
     &  '  largeurs equivalentes dans le fichier ? 0=non 1=oui'
           if(ila.eq.1) WRITE(6,*)
     &  '  do you want equivalent widths in the file ? 0=no 1=yes'
        READ(5,*) ieqw
        if(ila.eq.0) sep=';'
        if(ila.eq.1) sep=' '
c
           if(ila.eq.0) then                                                !m
           WRITE(6,*) 'si le modele a ete calcule avec une vitesse '//
     &     'de TURBULENCE v_turb'
           WRITE(6,*) 'utiliser une resolution nu/dnu <'//
     &     ' ou = 0.6*c/v_turb (pour l''option 0 ou 1)'
           else
           WRITE(6,*)'if the model was computed with microTURBULENCE,'
           WRITE(6,*)
     &'use a resolution nu/dnu < or = 0.6*c/v_turb (for option 0 or 1)'
           endif
        WRITE(6,*)
c
        if(ila.eq.0) then
           WRITE(6,*) '  4 options :'
           WRITE(6,*) 'nu/dnu=constant, raies gaussiennes (0) ou'//
     &                ' raies en triangle (1)'
           WRITE(6,*) 'dlambda=constante, pas=dlambda/5, de 100ev a '//
     &                '25kev, raies gaussiennes (2)'
           WRITE(6,*) 'pas constant en lambda, plus de choix (3)'
cdlambda=constante, pas=dlambda/5, de 100ev a 26kev, raies gaussiennes (2)
           WRITE(6,*)
     &     '  entrer : RESOLUTION(nu/dnu) et option de raie (0 ou 1)'
           WRITE(6,*) '         ou DLAMBDA(Angstrom) et 2'
           WRITE(6,*) '         ou PAS(Angstrom) et 3'
        else
           WRITE(6,*) '  4 options :'
           WRITE(6,*) 'nu/dnu=constant, lines with gaussian shape (0)'//     !m
     &                ' or triangle shape (1)'
           WRITE(6,*) 'dlambda=constant,bin=dlambda/5,range '//
     &                '100ev-25kev,lines with gaussian shape (2)'
           WRITE(6,*) 'bin constant in lambda, more choices (3)'
cdlambda=constant,bin=dlambda/5,range 100ev-26kev,lines with gaussian shape (2)
           WRITE(6,*)
     &     '  enter : RESOLUTION(nu/dnu) and shape option (0 or 1)'
           WRITE(6,*) '       or DLAMBDA(Angstrom) and 2'
           WRITE(6,*) '       or BIN(Angstrom) and 3'
        endif
        READ(5,*) resol,iraie
        if(ila.eq.0) then
           WRITE(14,*)'FICHIER: ',fiche
           if(nbang.gt.0) then
              WRITE(14,*)'NOMBRE TOTAL de DIRECTIONS=',nbang
      WRITE(14,*)'direction pour laquelle est calcule le spectre=',idir
           endif
        else
           WRITE(14,'(a,a)')'# FILE: ',fiche
           if(nbang.gt.0) then
              WRITE(14,'(a,i3)')'# TOTAL NUMBER OF DIRECTIONS=',nbang
              WRITE(14,'(a,i3)')
     &        '# direction for which the spectrum is computed=',idir
           endif
        endif
        if(iraie.eq.0) WRITE(14,'(a,a)')
     &                 '# nu/dnu=constant - gaussian line shape'
        if(iraie.eq.1) WRITE(14,'(a,a)')
     &                 '# nu/dnu=constant - triangle line shape'
        if(iraie.ge.2) WRITE(14,'(a,a)')
     &                 '# dlambda=constant - gaussian line shape'
        WRITE(14,*)                                                         !m
c
        Tmoy=3.d5 !est utile pour les edges des fe19a21 & si13-14 (1450-2750ev)
        tk=8.6171d-5*Tmoy                               ! ev  k/e' =25.85ev
        do k=1,500
           lnuc(k)=0
        enddo
        do k=1,nrais*3
           lnur(k)=0
        enddo
c
c                           ACQUISITION DES DONNEES
c
c lecture du CONTINU -  CONTINUUM reading
c attention res est defini en plusieurs endroits
c dans Titan on a:      if(resol.le.400.d0) res=0.0005d0 
c                       if(resol.gt.400.d0) res=1.d0/resol et triangles
        if(iraie.eq.0) then                  ! gaussienne Rinterne=2000 ou 5000
c           if(resol.le.800.d0) then
c              res=0.0005d0                   ! = dnutravail/nu
c           else 
c              res=0.0002d0
c           endif
           res=0.2d0/resol           ! =dnutravail/nu dnutravail=dnudemande/5
        else if(iraie.eq.1) then             ! triangle
           res=1.d0/resol                                                   !m
        else if(iraie.eq.2) then             ! dlambda=cst et gaussienne
           dlambda=resol
           pas=dlambda/5.d0
        else if(iraie.eq.3) then             ! pas_lambda=cst et gaussienne
           pas=resol
        endif
                         if(nbang.eq.0) then   ! fichier .res
 10     READ(13,70) ecrit1,ecrit2
 70     format(a20,a59)
        nd=index(ecrit1,'c')
c        type*,ecrit1,nd,ecrit1(nd:nd+6),'-'
        if(nd.eq.0.or.ecrit1(nd:nd+6).ne.'coldens') then
           if(ila.eq.0) then
              WRITE(14,70) ecrit1,ecrit2
           else
              WRITE(14,71) ecrit1,ecrit2
 71           format('#',a20,a59)
           endif
           goto 10
        endif
c        if(ieqw.eq.1) WRITE(14,80) resol
 80     format(/' resolution=',0pf10.1,'             (graftit105v03)')!12345678
  11    READ(13,70) ecrit1
        nd=index(ecrit1,'h')
c        type*,ecrit1,nd,ecrit1(nd:nd+4),'-'
        if(nd.eq.0) goto 11                                                 !m
        if(nd.gt.8.or.ecrit1(nd:nd+4).ne.'hnuev') goto 11

c  hnuev   ;Fincident ;Freflechi ;Fsortant ;Ftransmi-ap ;tauceff  ;kc;bord-ion;
c   13.600; 1.751E-03; 8.475E-03; 1.060E-03; 5.528E-14; 5.286E+01;   1 ;HI
c npv     1          2          3          4          5          6
c   13.600  1.516E-02  4.782E-03  1.080E-02  1.450E-02  1.674E-01    1  HI     
c23456789012345678901123456789011234567890112345678901123456789011234567890
                         else                 ! nbang>0 - fichier .ang
 50     READ(13,70) ecrit1,ecrit2
        nd=index(ecrit1,'h')
c        type*,ecrit1,nd,ecrit1(nd:nd+6),'-'
        if(nd.eq.0.or.ecrit1(nd:nd+4).ne.'hnuev') then
           if(ila.eq.0) then
              WRITE(14,70) ecrit1,ecrit2
           else
              WRITE(14,71) ecrit1,ecrit2
           endif
           goto 50
        endif
chnuev k(nu) refl1 refl2 refl3 refl4 refl5 out1 out2 out3 out4 out5
c   13.60000   1  9.692E-04  6.098E-03  1.676E-02  3.084E-02  4.232E-02  ...
                        endif

                        do 15 k=1,322
                        if(nbang.eq.0) then   ! fichier .res               !m
          if(ila.eq.0) then
       READ(13,'(a80)',err=16) ligne
       npv1=index(ligne,';')
       if(npv1.eq.0)  write(6,*) ' PB lecture des flux continus a kc=',k
       READ(ligne(:npv1-1),*) hnuev
       npv2=index(ligne(npv1+1:),';') + npv1
       READ(ligne(npv1+1:npv2-1),*) entrant
       npv3=index(ligne(npv2+1:),';') + npv2
       READ(ligne(npv2+1:npv3-1),*) reflechi
       npv4=index(ligne(npv3+1:),';') + npv3
       READ(ligne(npv3+1:npv4-1),*) sortant
       npv5=index(ligne(npv4+1:),';') + npv4
       READ(ligne(npv4+1:npv5-1),*) transmis
       npv6=index(ligne(npv5+1:),';') + npv5
       npv7=index(ligne(npv6+1:),';') + npv6
       READ(ligne(npv6+1:npv7-1),*) kc
          else
       READ(13,'(f10.3,5e11.3,i5)',err=16) hnuev,entrant,reflechi,
     &                                     sortant,transmis,rien,kc
          endif   ! ila
                        else                  ! nbang>0 - fichier .an2
       READ(13,'(f12.5,i5,41e11.3)') hnuev,kc,entrant,
     &     (rdir(i),i=1,nbang),(sdir(i),i=1,nbang)
       reflechi=rdir(idir)
       sortant=sdir(idir)                                                   !m
       transmis=sdir(1)
c      if(kc.le.10)write(6,'(f10.3,i4,2e11.3)'),hnuev,kc,entrant,reflechi
                        endif
c
       emis=0.d0
c        if(k.ne.kc) write(6,*)' PB lecture des flux continus a kc=',kc
        fic(kc)=entrant
        sc(kc)=sortant
        rc(kc)=reflechi
        tc(kc)=transmis
        ec(kc)=emis
        nk(kc)=kc
        thnuc(kc)=hnuev
 15                     continue
                        do kc=1,322
        if(iraie.le.1) then
           if(kc.le.220) then                                     ! ln(10)
              lnuc(kc)=nint((log10(thnuc(kc))+2.d0)*2.302585d0/res)
           else
              thnuc(kc)=thnuc(kc-220)
           lnuc(kc)=nint((log10(thnuc(kc-220))+2.d0)*2.302585d0/res)-1
           endif
        endif
        fic(kc)=fic(kc)*thnuc(kc)*2.41797d14          ! nu*F_nu
        sc(kc)=sc(kc)*thnuc(kc)*2.41797d14
        rc(kc)=rc(kc)*thnuc(kc)*2.41797d14
        tc(kc)=tc(kc)*thnuc(kc)*2.41797d14
        ec(kc)=ec(kc)*thnuc(kc)*2.41797d14                                 !m
                        enddo
 16      continue
c lnuc va de 191 a 29449
c-----------------------
c        write(9,*)'lecture du continu: kc hnuev lnuc inc'
c        write(9,'(i4,0pf12.3,i8,1pe13.3)') (nk(kc),thnuc(kc),lnuc(kc),
c     &        fic(kc),kc=1,500)
c ou        write(9,'(i4,0pf12.3,1p2e13.3)') (nk(kc),thnuc(kc),fic(kc),
c     &        rc(kc),kc=1,322,30),nk(322),thnuc(322),fic(322),rc(322)
c-----------------------
c lecture des RAIES
 20     READ(13,70) ecrit1
        nd=index(ecrit1,'h')
        if(nd.eq.0) goto 20
        if(nbang.eq.0.and.ecrit1(nd:nd+6).ne.'hnuraie'.and.
     &     ecrit1(nd-5:nd+2).ne.'line-hnu') goto 20
        if(nbang.ge.1.and.ecrit1(nd:nd+4).ne.'hnuev') goto 20
        if(ieqw.eq.1) then
              WRITE(14,*)
           if(ila.eq.0) then
              WRITE(14,'(a)') '1) RAIES: intensite (Fdnurai), '//
     &        'continu sous-jacent (nuFcont)'
              WRITE(14,*)'   et largeurs equivalentes (eV & Angstrom)'
              WRITE(14,91)                                                  !m
 91           format(' hnuraie    ;ion     ;kr  ;Fdnuraisor;Fdnurairef',
     &        ';nuFincid;nuFcont-sor;nuFcont-ref;Wev-sort  ;Wev-refl  ',
     &        ';WA-sort   ;WA-refl')
           else
              WRITE(14,'(a)') '# 1) LINES: intensity (Fdnuline),'//
     &        'underlying continuum (nuFcont)'
             WRITE(14,'(a)')'#    and equivalent widths (eV & Angstrom)'
              WRITE(14,991)
 991          format('# line-hnu   ion      kr  Fdnulinout Fdnulinref ',
     &        'nuFincid nuFcont-out nuFcont-ref  Wev-out    ',
     &        'Wev-ref    WA-out     WA-ref')
           endif   ! ila
        endif   ! ieqw=1
c        write(9,*) '3) rajout de pts raies   hnu,kr,lnu,r,lnu,r,lnu,r'  !
c
c  hnuraie   ;lambda   ;reflechi    ;sortant  ;taur0raie ;kr;kali;pb;ion;
c   10.199;  1215.670; -1.609E+10;  7.402E+09; 9.978E+03;   2; 3; 0 ;HI
c npv     1          2           3           4          5    6
c line-hnu    lambda    reflected    outward  line-taur0  kr kali pb ion
c   10.199   1215.670   6.480E+10   9.520E+08  1.333E+02    2  3  0  HI
c234567890c2345678901c23456789012c23456789012c2345678901c2345
c
        has=0.d0
        har=0.d0
        hbs=0.d0
        hbr=0.d0
        hgs=0.d0
        hgr=0.d0                                                           !m
                        do 25 k=2,ntab
                        if(nbang.eq.0) then   ! fichier .res
       if(ila.eq.0) then
          READ(13,'(a80)') ligne
          npv1=index(ligne,';')
c                    write(9,*) npv1,ligne(1:60)
          if(npv1.eq.0) then
             npvs=index(ligne,'S')
             if(npvs.eq.0) then
                goto 26
             else
                goto 25
             endif
          endif
          READ(ligne(:npv1-1),*) hnuev
c                    write(9,*) hnuev
          if(hnuev.le.0.) goto 26
          npv2=index(ligne(npv1+1:),';') + npv1
          READ(ligne(npv1+1:npv2-1),*) alambda
          npv3=index(ligne(npv2+1:),';') + npv2
          READ(ligne(npv2+1:npv3-1),*) refl
          npv4=index(ligne(npv3+1:),';') + npv3
          READ(ligne(npv3+1:npv4-1),*) sort
          npv5=index(ligne(npv4+1:),';') + npv4
          npv6=index(ligne(npv5+1:),';') + npv5
          READ(ligne(npv5+1:npv6-1),*) kr
          nraimax=kr                                                        !m
c
       else   ! english
          READ(13,'(f10.3,e11.3,2e12.3,e11.3,i5)',err=25,end=26)
     &        hnuev,alambda,refl,sort,rien,kr
          if(kr.ne.0) nraimax=kr
       endif   ! ila
                        else                  ! nbang>0 - fichier .ang
       READ(13,'(f12.5,i5,41e11.3)',err=25,end=26) 
     &     hnuev,kr,(rdir(i),i=1,nbang),(sdir(i),i=1,nbang)
       refl=rdir(idir)
       sort=sdir(idir)
       alambda=12398.54d0/hnuev
        if(kr.ne.0) nraimax=kr
                        endif
c
c        write(6,*) k,kr,hnuev,refl
           emis=0.d0
           abso=0.d0
        if(kr.ne.0) thnur(kr)=hnuev
        if(kr.ne.0) alo(kr)=alambda
c mise en table
             if(hnuev.lt.0.01d0) goto 25
        if(iraie.le.1) then
           lnur(kr)=nint((log10(hnuev)+2.d0)*2.302585d0/res)
        else
           res=pas/alo(kr)                                                  !m
        endif
        s(kr)=sort/res              ! ici res=dnutravail/nu = 1/(5R)
        r(kr)=refl/res
        t(kr)=abso/res
        e(kr)=emis/res
 25                     continue   ! k
 26     continue   ! sortie de lecture
c---------
c        write(9,*)'lecture des raies: kr hnuev lnur inc'
c        write(9,'(i4,0pf12.3,i8,1pe13.3)') (kr,thnur(kr),lnur(kr),
c     &        s(kr),kr=1,ntab)
c---------
        kf=nraimax-24
        if(ila.eq.0) WRITE(6,*)'nraimax,nbr.lus & dimension tableaux=',
     &                          nraimax,k-1,nrais
        if(nraimax.gt.nrais.and.ila.eq.0) 
     &  WRITE(6,*)'ATTENTION a la DIMENSION des TABLEAUX'

c recherche du continu correspondant aux raies
        CALL TABSIGext
c-----------------------
             if(ieqw.eq.0) goto 29
c calcul et ecriture des LARGEURS EQUIVALENTES
                        do 30 kr=2,nraimax
        hnuev=thnur(kr)                                                     !m
             if(hnuev.le.0.01d0) goto 30
        sort=s(kr)*res
        refl=r(kr)*res
        emis=0.d0
        abso=0.d0
             if(kr.ge.6.and.kr.le.12) then
             if(kr.eq.6) goto 30
             if(kr.eq.7) then
             has=sort
             har=refl
             goto 30
             else if(kr.eq.8) then
             hbs=sort
             hbr=refl
             goto 30
             else if(kr.eq.9) then
             hgs=sort
             hgr=refl
             goto 30
             else if(kr.eq.10) then
             sort=has+sort
             refl=har+refl                                                  !m
             else if(kr.eq.11) then
             sort=hbs+sort
             refl=hbr+refl
             else if(kr.eq.12) then
             sort=hgs+sort
             refl=hgr+refl
             endif
             endif
                if(kr.ge.(kf+1).and.kr.le.(kf+11)) then
                if(kr.eq.(kf+1)) then
                soms=sort
                somr=refl
                goto 30
                else if(kr.gt.(kf+1).and.kr.lt.(kf+11)) then
                soms=soms+sort
                somr=somr+refl
                goto 30
                else   ! kr=kf+11
                sort=soms+sort
                refl=somr+refl
                endif
                endif
        kc=kcr(kr)
        ss=sc(kc)
        rr=rc(kc)
        wevs=0.
        wevr=0.
        was=0.                                                             !m
        war=0.
        if(ss.gt.0.) wevs=hnuev*sort/ss
        if(rr.gt.0.) wevr=hnuev*refl/rr
        if(ss.gt.0.) was=alo(kr)*sort/ss
        if(rr.gt.0.) war=alo(kr)*refl/rr
c        if(abs(ss).le.1.d-36) ss=1.111d-36
c        if(abs(rr).le.1.d-36) rr=1.111d-36
c        if(abs(sort).le.1.d-36) sort=1.111d-36
c        if(abs(refl).le.1.d-36) refl=1.111d-36
        WRITE(14,86) hnuev,sep,nomrai(kr),sep,kr,sep,sort,sep,refl,sep,
     &        fic(kc),sep,ss,sep,rr,sep,wevs,sep,wevr,sep,was,sep,war
 86     format(0pf12.6,a,a7,a,i4,10(a,1pe10.3))
 30     continue
 29     continue
c
chnuraie    ;ion     ;kr  ;Fdnuraisor;Fdnurairef;nuFincid;nuFcont-sor;nuFcont-ref;Wev-sort  ;Wev-refl  ;WA-sort    ;WA-refl
c8670.86   ;1234; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90;
c
c        if(resol.le.400.d0) CALL graf0(resol,tk) 
c        if(resol.gt.400.d0) CALL graf1(resol,tk) 
        if(iraie.eq.0) CALL graf0(resol,tk)   ! nu/dnu=constant - gaussienne
        if(iraie.eq.1) CALL graf1(resol,tk)   ! nu/dnu=constant - triangle
        if(iraie.eq.2) CALL graf2(resol,tk)   ! dlambda=constant - gaussienne
        if(iraie.eq.3) CALL graf3(resol,tk)   ! pas_lambda=constant - gaussienne
        stop
        end                                                              !main
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine GRAF0(resol,tk)
c
c                    CAS DE RAIES GAUSSIENNES DEMANDEES
c
        implicit double precision (a-h,o-z)
        parameter (nrais=4200)
        character sep*1,nomrai*7
        dimension fo(90000),so(90000),ro(90000),to(90000),eo(90000),
     &      soc(90000),roc(90000),toc(90000),p(4001)
        dimension iw(90000),
     &      lo(90000),kco(90000),kro(90000),kas(90000)
        common/grkcr/nraimax,ntab,kf,kcr(nrais)
        common/gr/s(nrais*3),r(nrais*3),t(nrais*3),e(nrais*3),
     &            fic(500),sc(500),rc(500),tc(500),ec(500)
        common/grc/thnuc(500),alo(nrais)
        common/grl/lnur(nrais*3),lnuc(500),nk(500),ieqw,ila
        common/nom/nomrai(0:nrais)
c graf0
c        tab=';'
        if(ila.eq.0) sep=';'
        if(ila.eq.1) sep=' '
c        if(resol.le.800.d0) then
c           res=0.0005d0
c        else
c           res=0.0002d0
c        endif
        res=0.2d0/resol           ! =dnutravail/nu dnutravail=dnudemande/5
        do k=1,90000
           lo(k)=0
        enddo
c
c TRAITEMENT DU CONTINU
c tri du continu brut
        n=322
        call INDEXTRI(n,lnuc,iw)
c
c        write(9,*) '1) continu juste apres tri   i,kc,hnu,lnu,rc'       !
c        do i=1,n                                                        !
c        kc=iw(i)                                                        !
c       write(9,'(2i5,0pf12.5,i8,1pe12.3)')i,kc,thnuc(kc),lnuc(kc),rc(kc)!234567
c        enddo                                                           !
c-----------------------
c rajout de points dans le continu - remplissage des trous
        j=0                                                                !g0
                        do 100 i=1,322
        kc=iw(i)
          if(lnuc(kc).le.0) goto 100
          if(i.lt.322) then
             k1=iw(i+1)
             n=lnuc(k1)-lnuc(kc)
          endif   ! i<322
          if(kc.eq.184) then
             k1=iw(i-1)
             n=(lnuc(kc)-lnuc(k1))/2-1
          endif   ! fin
        dhnu=thnuc(kc)*res
        nkt=nint(tk*2.d0/dhnu)
c             if(j.ge.1.and.lnuc(kc).eq.lo(j))then           ! bords proches
c             km1=iw(i-1)                    dans graftit105v02e.f mais faux
c               if(kc.lt.km1) then           fait des trous dans les lo
c               lo(j)=lnuc(kc)               surtout avec resol petit (ex:30)
c               fo(j)=fic(kc)
c               so(j)=sc(kc)
c               ro(j)=rc(kc)
c               to(j)=tc(kc)
c               eo(j)=ec(kc)
c               kco(j)=kc
c               kas(j)=50
c               endif
c             goto 100                                                      !g0
c             endif
        if(n.eq.0) then                                      ! bords proches
c           write(9,'(2i5,a5)')i,kc,' saut'
           goto 100
        endif
c                         if(ino.ge.5) goto 110
        if(kc.eq.190) then                              !discontinuite Balmer
        j=j+1                                 ! interpolation entre 190 et 189
        ll=lnuc(kc)                           ! avec la pente 191-190
        ff=fic(kc)
        ss=sc(kc)
        rr=rc(kc)
        tt=tc(kc)
        ee=ec(kc)
        lo(j)=ll
        fo(j)=ff
        so(j)=ss
        ro(j)=rr
        to(j)=tt
        eo(j)=ee
        kco(j)=kc
        kas(j)=40                                                             !
          if(n.le.1) goto 100
        df=(ff-fic(189))/dfloat(lnuc(190)-lnuc(189))
        ds=(ss-sc(191))/dfloat(lnuc(190)-lnuc(191))
        dr=(rr-rc(191))/dfloat(lnuc(190)-lnuc(191))
        dt=(tt-tc(191))/dfloat(lnuc(190)-lnuc(191))
        de=(ee-ec(191))/dfloat(lnuc(190)-lnuc(191))
          do m=1,n-1
          j=j+1                                                            !g0
          ll=ll+1
          ff=ff+df
          ss=ss+ds
          rr=rr+dr
          tt=tt+dt
          ee=ee+de
          lo(j)=ll
          fo(j)=ff
          so(j)=ss
          ro(j)=rr
          to(j)=tt
          eo(j)=ee
          kco(j)=0
          kas(j)=41                                                           !
          enddo
        goto100
        endif  ! kc=190
        if(kc.eq.189) then                              !discontinuite Balmer
        j=j+1                                 ! interpolation entre 189 et 188
        lo(j)=lnuc(kc)                                ! avec la pente 188-187
        fo(j)=fic(kc)
        so(j)=sc(kc)
        ro(j)=rc(kc)
        to(j)=tc(kc)
        eo(j)=ec(kc)
        kco(j)=kc
        kas(j)=42                                                           !g0
          if(n.le.1) goto 100
        df=(fic(189)-fic(188))/dfloat(lnuc(189)-lnuc(188))
        ds=(sc(187)-sc(188))/dfloat(lnuc(187)-lnuc(188))
        dr=(rc(187)-rc(188))/dfloat(lnuc(187)-lnuc(188))
        dt=(tc(187)-tc(188))/dfloat(lnuc(187)-lnuc(188))
        de=(ec(187)-ec(188))/dfloat(lnuc(187)-lnuc(188))
        ll=lnuc(189)
        ff=fic(189)
        ss=sc(188)-ds*dfloat(n)
        rr=rc(188)-dr*dfloat(n)
        tt=tc(188)-dt*dfloat(n)
        ee=ec(188)-de*dfloat(n)
          do m=1,n-1
          j=j+1
          ll=ll+1
          ff=ff+df
          ss=ss+ds
          rr=rr+dr
          tt=tt+dt
          ee=ee+de
          lo(j)=ll
          fo(j)=ff
          so(j)=ss                                                         !g0
          ro(j)=rr
          to(j)=tt
          eo(j)=ee
          kco(j)=0
          kas(j)=43
          enddo
        goto 100
        endif   ! kc=189
c obsolete grace a un changement des frequences-continu supplementaires
c                         if(ino.ge.4) goto 110
c        if(kc.eq.139) then                              !absorption du FeK
c        j=j+1                                 ! interpolation entre 139 et 140
c        ll=lnuc(kc)                           ! avec la pente 138-139
c        ff=fic(kc)
c        ss=sc(kc)
c        rr=rc(kc)
c        tt=tc(kc)
c        ee=ec(kc)
c        lo(j)=ll
c        fo(j)=ff
c        so(j)=ss
c        ro(j)=rr
c        to(j)=tt
c        eo(j)=ee
c        kco(j)=kc
c        kas(j)=44                                                         !g0
c          if(n.le.1) goto 100
c        df=(ff-fic(140))/dfloat(lnuc(139)-lnuc(140))
c        ds=(ss-sc(138))/dfloat(lnuc(139)-lnuc(138))
c        dr=(rr-rc(138))/dfloat(lnuc(139)-lnuc(138))
c        dt=(tt-tc(138))/dfloat(lnuc(139)-lnuc(138))
c        de=(ee-ec(138))/dfloat(lnuc(139)-lnuc(138))
c          do m=1,n-1
c          j=j+1
c          ll=ll+1
c          ff=ff+df
c          ss=ss+ds
c          rr=rr+dr
c          tt=tt+dt
c          ee=ee+de
c          lo(j)=ll
c          fo(j)=ff
c          so(j)=ss
c          ro(j)=rr
c          to(j)=tt
c          eo(j)=ee
c          kco(j)=0
c          kas(j)=45
c          enddo                                                           !g0
c        goto100
c        endif   ! kc=139
 110    continue
             if(kc.le.102.and.nkt.le.1) then                ! kT tres petit
             j=j+1                                ! reduction du pic par kT/dnu
             lo(j)=lnuc(kc)
                if((sc(kc)-sc(kc+220)).gt.0.d0) then      ! bord en emission
             ss=sc(kc+220)
             so(j)=ss + (sc(kc)-ss)*tk/dhnu
                else                                      ! bord en absorption
                ss=sc(kc)
                so(j)=ss
                endif
                if((rc(kc)-rc(kc+220)).gt.0.d0) then      ! bord en emission
             rr=rc(kc+220)
             ro(j)=rr + (rc(kc)-rr)*tk/dhnu
                else                                      ! bord en absorption
                rr=rc(kc)
                ro(j)=rr
                endif
                if((ec(kc)-ec(kc+220)).gt.0.d0) then      ! bord en emission
             ee=ec(kc+220)
             eo(j)=ee + (ec(kc)-ee)*tk/dhnu
                else                                      ! bord en absorption
                ee=ec(kc)
                eo(j)=ee
                endif
             ff=fic(kc)                                                    !g0
             tt=tc(kc)
             fo(j)=ff
             to(j)=tt
             kco(j)=kc
             kas(j)=10
             if(n.le.1) goto 100
             j=j+1
             ll=lnuc(kc)+1
             lo(j)=ll
             fo(j)=ff
             so(j)=ss
             ro(j)=rr
             to(j)=tt
             eo(j)=ee
             kco(j)=0
             kas(j)=11                                                     !g0
             if(n.le.2) goto 100
             df=(fic(k1)-ff)/dfloat(n-1)
             ds=(sc(k1)-ss)/dfloat(n-1)
             dr=(rc(k1)-rr)/dfloat(n-1)
             dt=(tc(k1)-tt)/dfloat(n-1)
             de=(ec(k1)-ee)/dfloat(n-1)
               do m=1,n-2
               j=j+1
               ll=ll+1
               ff=ff+df
               ss=ss+ds
               rr=rr+dr
               tt=tt+dt
               ee=ee+de
               lo(j)=ll
               fo(j)=ff
               so(j)=ss
               ro(j)=rr
               to(j)=tt
               eo(j)=ee
               kco(j)=0
               kas(j)=12                                                   !g0
               enddo
             goto 100
             endif
c
        if((kc.gt.102).or.n.lt.nkt) then       ! kc > 102 ou nusuivant-nu < 2kT
        j=j+1                                                   ! cas normal
        ll=lnuc(kc)                  ! interpolation entre les 2 nu consecutifs
        ff=fic(kc)
        ss=sc(kc)
        rr=rc(kc)
        tt=tc(kc)
        ee=ec(kc)
        lo(j)=ll
        fo(j)=ff
        so(j)=ss
        ro(j)=rr
        to(j)=tt
        eo(j)=ee
        kco(j)=kc
        kas(j)=20
          if(i.eq.322) jcmax=j
          if(i.eq.322) goto 100        
          if(n.le.1) goto 100
        df=(fic(k1)-ff)/dfloat(n)
        ds=(sc(k1)-ss)/dfloat(n)
        dr=(rc(k1)-rr)/dfloat(n)
        dt=(tc(k1)-tt)/dfloat(n)                                           !g0
        de=(ec(k1)-ee)/dfloat(n)
          do m=1,n-1
          j=j+1
          ll=ll+1
          ff=ff+df
          ss=ss+ds
          rr=rr+dr
          tt=tt+dt
          ee=ee+de
          lo(j)=ll
          fo(j)=ff
          so(j)=ss
          ro(j)=rr
          to(j)=tt
          eo(j)=ee
          kco(j)=0
          kas(j)=21                                                        !g0
          enddo
       goto 100
       endif   ! interpolation

       if((kc.le.102).and.n.ge.nkt) then                 ! nusuivant-nu > 2kT
c est utile pour les edges des fe19a21 & si13-14 (1450-2750ev)
                  j=j+1                      ! rajout de points en exp(-dnu/kt)
                  ll=lnuc(kc)
c sfond=tc(kc+220) !ds 2009 version105.02e et suppression de la definition
c suivante de sfond, mais il n'y a pas de transmis dans le .ang !
                  sfond=sc(kc+220)
                  rfond=rc(kc+220)
                  efond=ec(kc+220)
                  kas(j)=30
c cas de 2 edges tres proches, le 1er en emission-n'arrive peut etre jamais?
                  k2=min(iw(i-1),iw(i-2))
       if(k2.le.102.and.nkt.gt.(lnuc(kc)-lnuc(k2))) then
          if(sc(k2).gt.sc(k2+220).and.sc(kc+220).gt.sc(k2+220))
     &        sfond=sc(k2+220) +
     & (sc(k1)-sc(k2+220))*(thnuc(kc)-thnuc(k2))/(thnuc(k1)-thnuc(k2))
          if(rc(k2).gt.rc(k2+220).and.rc(kc+220).gt.rc(k2+220))
     &        rfond=rc(k2+220) +
     & (rc(k1)-rc(k2+220))*(thnuc(kc)-thnuc(k2))/(thnuc(k1)-thnuc(k2))
          if(ec(k2).gt.ec(k2+220).and.ec(kc+220).gt.ec(k2+220))
     &        efond=ec(k2+220) +
     & (ec(k1)-ec(k2+220))*(thnuc(kc)-thnuc(k2))/(thnuc(k1)-thnuc(k2))
          kas(j)=31                                                       !g0
       endif
c                        if(n.ge.nkt*2) then
c                        vsig=(thnuc(k1)/thnuc(kc))**3
c                        sfond=(sc(k1)/fic(k1))**vsig *fic(kc)
c                        rfond=(rc(k1)/fic(k1))**vsig *fic(kc)
c                        efond=(ec(k1)/fic(k1))**vsig *fic(kc)
c                        endif
                  lo(j)=ll
                  fo(j)=fic(kc)
                  so(j)=sc(kc)
                  ro(j)=rc(kc)
                  to(j)=tc(kc)
                  eo(j)=ec(kc)
                  kco(j)=kc
                    if(n.le.1) goto 100
                  nn=min(n-1,nkt*2)
                  dse=sc(kc)-sfond
                  dre=rc(kc)-rfond 
                  dee=ec(kc)-efond 
                  df=(fic(k1)-fic(kc))/dfloat(n)
                  ds=(sc(k1)-sfond)/dfloat(n)
                  dr=(rc(k1)-rfond)/dfloat(n)
                  dt=(tc(k1)-tc(kc))/dfloat(n)
                  de=(ec(k1)-efond)/dfloat(n)
                  exs=exp(-dhnu/tk)                                        !g0
                  ss=sfond
                  ff=fic(kc)
                  tt=tc(kc)
                        if(dre.gt.0.d0) then   ! bord en emission
                  exr=exp(-dhnu/tk)
                  rr=rfond
                        else                   ! bord en absorption
                        rr=rc(kc)
                        dre=0.d0
                        exr=1.d0
                        endif
                        if(dee.gt.0.d0) then   ! bord en emission
                  exe=exp(-dhnu/tk)
                  ee=efond
                        else                   ! bord en absorption
                        ee=ec(kc)
                        dee=0.d0
                        exe=1.d0                                          !g0
                        endif
                    do m=1,nn
                    j=j+1
                    ll=ll+1
                    lo(j)=ll
                    dse=dse*exs
                    dre=dre*exr
                    dee=dee*exe
                    ff=ff+df
                    ss=ss+ds
                    rr=rr+dr
                    tt=tt+dt
                    ee=ee+de
                    fo(j)=ff
                    so(j)=ss+dse
                    ro(j)=rr+dre
                    to(j)=tt
                    eo(j)=ee+dee
                    kco(j)=kc
                    kas(j)=32                                              !g0
                    enddo
                  if(n.le.nn+1) goto 100
                    do m=nn+1,n-1              ! au dela de 2kT
                    j=j+1
                    ll=ll+1
                    ff=ff+df
                    ss=ss+ds
                    rr=rr+dr
                    tt=tt+dt
                    ee=ee+de
                    lo(j)=ll
                    fo(j)=ff
                    so(j)=ss
                    ro(j)=rr
                    to(j)=tt
                    eo(j)=ee
                    kco(j)=0
                    kas(j)=33                                               !g0
                    enddo
                  goto 100
                  endif
 100                     continue
c        WRITE(6,*) ' nombre de points en frequence=',jcmax
        if(jcmax.gt.80000) then
	write(6,*)' trop de pts continus rajoutes - STOP - jcmax=',jcmax
        write(6,*) ' REDIMENSIONNER LES TABLEAUX'
	endif
c-----------------------
c        write(9,*) '2) continu tous les pts   cas,j,kc,lo,ro,hnu'           !
c        do j=1,jcmax                                                        !
c        hnuev=10.d0**(res*float(lo(j))/2.302585d0 - 2.d0)                   !
c        if(kco(j).ne.0) write(9,'(i4,i7,i5,i7,1pe12.3,1p2e15.6)')           !
c     &                  kas(j),j,kco(j),lo(j),ro(j),hnuev,thnuc(kco(j))     !
c        if(kco(j).eq.0) write(9,'(i4,i7,i5,i7,1pe12.3,1pe15.6)')            !
c     &                  kas(j),j,kco(j),lo(j),ro(j),hnuev                   !
c        enddo                                                               !
c        write(9,*) ' continu tous les pts   cas,j,kc,lo,ro,hnu'             !
c-----------------------
c
c TRAITEMENT DES RAIES
c
c mise en table et rajout de points de chaque cote de la raie              !g0
c        write(9,*) ' raies :  kr  lnur  r  s  et avant  et apres'           !
                             do 25 kr=2,nraimax
            if(s(kr).eq.0.d0.and.r(kr).eq.0.d0) goto 25
        lnur(kr+ntab)=lnur(kr) - 1
        s(kr+ntab)=0.d0
        r(kr+ntab)=0.d0
        t(kr+ntab)=0.d0
        e(kr+ntab)=0.d0
        lnur(kr+ntab*2)=lnur(kr) + 1
        s(kr+ntab*2)=0.d0
        r(kr+ntab*2)=0.d0
        t(kr+ntab*2)=0.d0
        e(kr+ntab*2)=0.d0
c---
c        write(9,'(i4,i7,1p2e12.4,2(i10,1pe10.2))') kr,lnur(kr),r(kr),   !234567
c     &       s(kr),lnur(kr+ntab),r(kr+ntab),lnur(kr+ntab*2),r(kr+ntab*2)   !
c--- 
 25                         continue   ! do kr
c-----------------------
c tri des raies et sommation des points de meme frequence
c puis on les range a la suite du continu                                  !g0
        n=ntab*3
        call INDEXTRI(n,lnur,iw)
c
c        write(9,*) '--) raies juste apres tri   i,kr,lnu,r'                !
c        do i=1,n                                                           !
c        kr=iw(i)                                                           !
c        write(9,'(2i7,i8,1p2e13.4)') i,kr,lnur(kr),r(kr),s(kr)             !
c        enddo                                                              !
c        write(9,*)                                                         !
c     &  '4) toutes les raies triees avec sommation i,kr,kro,lnu,r'         !
        ii=jcmax
                        do 210 i=1,n
        kr=iw(i)
        if(lnur(kr).le.0) goto 210
        if(lnur(kr).eq.lo(ii)) then                  ! passages suivants
           if(abs(r(kr)).gt.abs(ro(ii))) kro(ii)=kr
           so(ii)=so(ii)+s(kr)
           ro(ii)=ro(ii)+r(kr)
           to(ii)=to(ii)+t(kr)
           eo(ii)=eo(ii)+e(kr)
           else
        ii=ii+1                                      ! 1er passage a ce lnu
        lo(ii)=lnur(kr)
        so(ii)=s(kr)
        ro(ii)=r(kr)
        to(ii)=t(kr)
        eo(ii)=e(kr)                                                       !g0
        kco(ii)=0
        kro(ii)=kr                                  ! kro va de 1 a ntab*3
        endif
        imax=ii
c        write(9,'(2i7,2i8,1pe13.4)') ii,kr,kro(ii),lo(ii),ro(ii)          !
 210                    continue
c        WRITE(6,*) ' nombre de points du tableau=',imax
        if(imax.gt.90000) then
	write(6,*) ' trop de pts - STOP - imax=',imax
        write(6,*) ' REDIMENSIONNER LES TABLEAUX'
	endif
c
c        write(9,*)                                                        !
c     & '4) continu ordonne puis raies ordonnees et sommees  i,kco,lo,ro'  !
c        do i=1,imax+2                                                     !
c        write(9,'(2i7,i8,1pe12.4)') i,kco(i),lo(i),ro(i)                  !
c        enddo                                                             !
c---------------------
c
c MELANGE des raies et du continu
c                                                                         !g0
        n=imax
        call INDEXTRI(n,lo,iw)
c        write(9,*) '5) entrelacement    i,j,kco,kro,lo,ro'           !
c        do i=1,n                                                     !
c        j=iw(i)                                                      !
c        if(kco(j).gt.0) write(9,'(2i7,3i8,1pe12.4,0pf13.5)')         !
c     &     i,j,kco(j),kro(j),lo(j),ro(j),thnuc(kco(j))               !
c        if(kco(j).eq.0) write(9,'(2i7,3i8,1pe12.4)')                 !
c     &      i,j,kco(j),kro(j),lo(j),ro(j)
c        enddo                                                        !
c
c sommation du continu et des raies
c
c        write(9,*) '6) melange    i,jc,jr,kco,kro,lo,ro'             !
                       do 300 i=1,n-1
        j=iw(i)
        if(lo(j).eq.0) goto 300
        if(i.gt.1.and.lo(j).eq.lo(iw(i-1))) goto 300         ! deja traite
        j1=iw(i+1)
        if(lo(j).eq.lo(j1)) then            ! forcement 1 raie et 1 continu
                  jc=min(j,j1)
                  jr=max(j,j1)
                  soc(jc)=so(jc)
                  roc(jc)=ro(jc)
                  toc(jc)=to(jc)
                  so(jc)=so(j) + so(j1)
                  ro(jc)=ro(j) + ro(j1)
                  to(jc)=to(j) + to(j1)
                  eo(jc)=eo(j) + eo(j1)                                   !g0
                  kro(jc)=kro(jr)
c ne surtout pas mettre: if(so(jc)<0) so(jc)=0.  if(ro(jc)<0) ro(jc)=0.
c        write(9,'(3i7,3i8,1pe12.4,0pf13.5)') i,jc,jr,kco(jc),kro(jc),
c     &                                       lo(j),ro(jc)
c                  goto 300      inutile
                  else
                  soc(j)=so(j)
                  roc(j)=ro(j)
                  toc(j)=to(j)
                  endif
 300                   continue
c---------------------
c
c MOYENNE MOBILE en forme de gaussienne pour obtenir la resolution demandee
c
        Tmoy=tk/8.6171d-5
           if(ila.eq.0) then
        WRITE(14,80) resol
        if(ieqw.eq.1) WRITE(14,*) '2) CONTINU ET RAIES : nuF(nu)'
        if(ieqw.eq.0) WRITE(14,*) ' CONTINU ET RAIES : nuF(nu)'
        WRITE(14,92) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep
           else                                                            !g0
        WRITE(14,81) resol
        if(ieqw.eq.1)WRITE(14,'(a)')'#2) CONTINUUM AND LINES : nuF(nu)'
        if(ieqw.eq.0)WRITE(14,'(a)') '# CONTINUUM AND LINES : nuF(nu)'
        WRITE(14,93) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep
           endif   ! ila
 80    format(/' resolution=',0pf10.1,'             (graftit105v03)') !12345678
 81    format(/'# resolution=',0pf10.1)
 92     format('  hnuev   ',a,'nuFentrant',a,
     &     'nuFreflechi',a,'nuFsortant',a,'nuFtransapp',a,
     &     'nuFcontrefl',a,'nuFcontsort',a,'kc ',a,'kr ',a,'ion',a)
 93     format('# hnuev   ',a,'nuFincident',a,
     &     'nuFreflected',a,'nuFoutward',a,'nuFtransapp',a,
     &     'nuFcontref',a,'nuFcontout',a,'kc ',a,'kr ',a,'ion',a)
c
c avant dec2009:       dnur=0.5d0/res/resol
c        mm=nint(dnur)*4+1
c        dnur=1000.d0/resol
c        j0=nint(dnur)*2+1
c        jpas=nint(dnur/2.d0) !pour eviter un fichier trop gros en sortie
cc        if(resol.gt.70) jpas=jpas+1
c                            encore avant : jpas=20    if(resol.gt.100) jpas=5
c pour resol= 30  70  100  200  400  1000
c jpas=       17   7    5    3    1    1
c d'ou         4   4                      fois plus de points que la resolution
c jpas=       17   7    6    4    2    2        if(resol.gt.70) jpas=jpas+1
c d'ou         4   4  3.3  2.5  2.5   2.5 fois plus de points que la resolution
        dnur=2.5d0
        j0=6          ! 5 pts de chaque cote pour definir la gaussienne
        mm=11
        p(j0)=1.d0
        som=1.d0                                                            !g0
                        do 410 j=j0+1,mm
c  p(j)=exp(- (hnu-hnu0)**2/(dnu**2/(4*Ln2)) ) si dnu=fwhm
        p(j)=exp(-(float(j-j0)*0.8325546d0/dnur)**2)              ! sqrt(Ln2)
        p(2*j0-j)=p(j)
        som=som+2.d0*p(j)
 410                    continue
c        type*,(p(j),j=1,mm),som
        fm=som
        i1=0
                        do 400 i=1,jcmax-mm+1
        hnuev=10.d0**(res*float(lo(i+j0-1))/2.302585d0 - 2.d0)
                  if(hnuev.lt.0.9d0) then
                     if(kco(i).eq.0.and.kro(i).eq.0) goto 400
                     kr=kro(i)
                     hnuev=10.d0**(res*float(lo(i))/2.302585d0-2.d0)
c                           i1=i
                     if(kr.eq.0) then                   ! continu
                        if(i.le.i1) goto 400
                        ff=fo(i)
                        ss=soc(i)
                        rr=roc(i)
                        tt=toc(i)
                        ee=eo(i)
                        ssc=soc(i)
                        rrc=roc(i)                                         !g0
                        i1=i
                     else                               ! cas de la raie
                        if(kr.le.ntab) then             ! centre raie
                        dss=(so(i)-soc(i))*res*resol
                        drr=(ro(i)-roc(i))*res*resol
        if(i.le.i1.and.dss.lt.soc(i)/1.d3.and.drr.lt.roc(i)/1.d3)goto400
        if(i.le.i1) hnuev=10.d0**(res*float(lo(i1+1))/2.302585d0-2.d0)
                           ff=fo(i)
                           ss=soc(i) + dss
                           rr=roc(i) + drr
                           tt=toc(i) + (to(i)-toc(i))*res*resol
                           ee=eo(i)
                           ssc=soc(i)
                           rrc=roc(i)
                        else if(kr.le.ntab*2) then
                           if(i-j0.le.i1) goto 400
                           kr=0
                     hnuev=10.d0**(res*float(lo(i-j0))/2.302585d0-2.d0)
                           ff=fo(i-j0)
                           ss=soc(i-j0)
                           rr=roc(i-j0)
                           tt=toc(i-j0)
                           ee=eo(i-j0)
                           ssc=soc(i-j0)
                           rrc=roc(i-j0)
                        else if(kr.gt.ntab*2) then
                           kr=0                                           !g0
                           i1=i+j0
                     hnuev=10.d0**(res*float(lo(i1))/2.302585d0-2.d0)
                           ff=fo(i+j0)
                           ss=soc(i+j0)
                           rr=roc(i+j0)
                           tt=toc(i+j0)
                           ee=eo(i+j0)
                           ssc=soc(i+j0)
                           rrc=roc(i+j0)
                        endif   ! cas de la raie
                     endif
                     if(ff.le.1.d-36) ff=1.111d-36
                     if(ss.le.1.d-36) ss=1.111d-36
                     if(rr.le.1.d-36) rr=1.111d-36
                     if(tt.le.1.d-36) tt=1.111d-36
                     if(ee.le.1.d-36) ee=1.111d-36
                     if(ssc.le.1.d-36) ssc=1.111d-36
                     if(rrc.le.1.d-36) rrc=1.111d-36
                     WRITE(14,88)hnuev,sep,ff,sep,rr,sep,ss,sep,tt,sep,
     &                     rrc,sep,ssc,sep,kco(i),sep,kr,sep,nomrai(kr)
                     goto 400
                  else   ! hnuev>0.1
c        if(mod(i,jpas).ne.0) goto 400                                      !g0
                  endif  ! hnuev/0.1
        ff=0.d0
        ss=0.d0
        rr=0.d0
        ssc=0.d0
        rrc=0.d0
        tt=0.d0
        ee=0.d0
        kc=0
        kr=0
        kk=0
           do 401 j=1,mm
           ff=ff+fo(i+j-1)*p(j)
           ss=ss+so(i+j-1)*p(j)
           rr=rr+ro(i+j-1)*p(j)
           ssc=ssc+soc(i+j-1)*p(j)
           rrc=rrc+roc(i+j-1)*p(j)
           tt=tt+to(i+j-1)*p(j)
           ee=ee+eo(i+j-1)*p(j)
           if(kco(i+j-1).gt.0) then
              kk=kco(i+j-1)
              if(kc.eq.0) kc=kk
              kc=min(kc,kk)
              endif
           if(kro(i+j-1).gt.0.and.kro(i+j-1).le.ntab) then
              kkr=kro(i+j-1)                                              !g0
              if(kr.eq.0) kr=kkr
              kr=min(kr,kkr)
              endif
 401       continue
        ff=ff/fm
        ss=ss/fm
        rr=rr/fm
        ssc=ssc/fm
        rrc=rrc/fm
        tt=tt/fm
        ee=ee/fm
        if(ff.le.1.d-36) ff=1.111d-36
        if(ss.le.1.d-36) ss=1.111d-36
        if(rr.le.1.d-36) rr=1.111d-36
        if(ssc.le.1.d-36) ssc=1.111d-36
        if(rrc.le.1.d-36) rrc=1.111d-36
        if(tt.le.1.d-36) tt=1.111d-36
        if(ee.le.1.d-36) ee=1.111d-36
        WRITE(14,88) hnuev,sep,ff,sep,rr,sep,ss,sep,tt,sep,
     &               rrc,sep,ssc,sep,kc,sep,kr,sep,nomrai(kr)
 400                    continue
 88     format(0pf13.5,6(a,1pe10.3),a,i3,a,i4,a,a7)
c hnuev   ;nuFentrant;nuFreflechi;nuFsortant;nuFtransapp;nuFcontrefl;nuFcontsort;kc ;kr ;ion
c 25000.12345; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90;123;1234;1234567
c        WRITE(6,*)' nombre total de points du spectre=',jcmax
c
        return
         end                                                             !graf0
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine GRAF1(resol,tk)
c
c                    CAS DE RAIES EN TRIANGLE DEMANDEES
c
        implicit double precision (a-h,o-z)
        parameter (nrais=4200)
        character sep*1,nomrai*7
        dimension hc(500)
        dimension fo(50000),so(50000),ro(50000),to(50000),eo(50000)
        dimension iw(50000),lo(50000),kco(50000)
        common/grkcr/nraimax,ntab,kf,kcr(nrais)
        common/gr/s(nrais*3),r(nrais*3),t(nrais*3),e(nrais*3),
     &            fic(500),sc(500),rc(500),tc(500),ec(500)
        common/grc/thnuc(500),alo(nrais)
        common/grl/lnur(nrais*3),lnuc(500),nk(500),ieqw,ila
        common/gtab/thnur(nrais)
        common/nom/nomrai(0:nrais)
c graf1
c        tab=';'
        if(ila.eq.0) sep=';'
        if(ila.eq.1) sep=' '
        res=1.d0/resol
        do k=1,50000
           lo(k)=0
        enddo
c
c TRAITEMENT DU CONTINU
                        do kc=1,322
        hc(kc)=thnuc(kc)
        enddo
c tri du continu brut
        n=322
        call INDEXTRI(n,lnuc,iw)
c        write(9,*) '--) continu juste apres tri   i,kc,hnu,lnu,rc'       !
c        do i=1,n                                                         !
c        kc=iw(i)                                                         !
c       write(9,'(2i5,0pf12.5,i8,1pe12.3)')i,kc,thnuc(kc),lnuc(kc),rc(kc) !
c        enddo                                                            !g1
c-----------------------
c rajout de points dans le continu
        ii=0
c        write(9,*) '1) rajout de pts continu - kT en ev=',tk,
c     &             ' i,kc,hnu,lnu,rc'
                        do 1100 i=1,322
        kc=iw(i)
        hnuev=thnuc(kc)
        dhnu=hnuev*res
        if(kc.eq.189) then                             !discontinuite Balmer
           ii=ii+1                             ! interpolation entre 190 et 189
           lnuc(322+ii)=lnuc(189)-1            ! avec la pente 191-190
           hc(322+ii)=thnuc(189)-dhnu
           dd=dfloat(lnuc(322+ii)-lnuc(190))/dfloat(lnuc(190)-lnuc(191))
           fic(322+ii)=fic(190)+(fic(189)-fic(190))
     &       *dfloat(lnuc(322+ii)-lnuc(190))/dfloat(lnuc(189)-lnuc(190))
           sc(322+ii)=sc(190)+(sc(190)-sc(191))*dd
           rc(322+ii)=rc(190)+(rc(190)-rc(191))*dd
           tc(322+ii)=tc(190)+(tc(190)-tc(191))*dd
           ec(322+ii)=ec(190)+(ec(190)-ec(191))*dd                        !g1
           nk(322+ii)=189
           ii=ii+1                             ! interpolation entre 188 et 189
           lnuc(322+ii)=lnuc(189)+1            ! avec la pente 187-188
           hc(322+ii)=thnuc(189)+dhnu
           dd=dfloat(lnuc(322+ii)-lnuc(188))/dfloat(lnuc(188)-lnuc(187))
           fic(322+ii)=fic(189)+(fic(188)-fic(189))
     &       *dfloat(lnuc(322+ii)-lnuc(188))/dfloat(lnuc(189)-lnuc(188))
           sc(322+ii)=sc(188)-(sc(187)-sc(188))*dd
           rc(322+ii)=rc(188)-(rc(187)-rc(188))*dd
           tc(322+ii)=tc(188)-(tc(187)-tc(188))*dd
           ec(322+ii)=ec(188)-(ec(187)-ec(188))*dd
           nk(322+ii)=188
c           write(9,*) ii,kc,hc(322+ii),lnuc(322+ii),rc(322+ii)
           endif   ! kc=189
        if(kc.eq.140) then                             ! absorption du FeK
           ii=ii+1                             ! interpolation entre 139 et 140
           lnuc(322+ii)=lnuc(140)-1            ! avec la pente 138-139
           hc(322+ii)=thnuc(140)-dhnu
           dd=dfloat(lnuc(322+ii)-lnuc(139))/dfloat(lnuc(139)-lnuc(138))
           fic(322+ii)=fic(139)+(fic(140)-fic(139))
     &       *dfloat(lnuc(322+ii)-lnuc(139))/dfloat(lnuc(140)-lnuc(139))
           sc(322+ii)=sc(139)+(sc(139)-sc(138))*dd
           rc(322+ii)=rc(139)+(rc(139)-rc(138))*dd
           tc(322+ii)=tc(139)+(tc(139)-tc(138))*dd
           ec(322+ii)=ec(139)+(ec(139)-ec(138))*dd                       !g1
           nk(322+ii)=140
c           write(9,*) ii,kc,hc(322+ii),lnuc(322+ii),rc(322+ii)
           endif   ! kc=140
c
        if(kc.gt.102) goto 1100
        k1=iw(i+1)
        if(lnuc(k1).eq.lnuc(kc)) k1=iw(i+2)
        if(lnuc(k1).eq.lnuc(kc)) k1=iw(i+3)
        if(lnuc(k1).eq.lnuc(kc)) k1=iw(i+4)
        dnusuiv=thnuc(k1)-hnuev
        if(tk*2.d0.gt.dnusuiv) goto1100
c
        if(tk*2.d0.le.dhnu) then   ! kT tres petit: reduction du pic par kT/dnu
           ii=ii+1
           lnuc(322+ii)=lnuc(kc)+1
           hc(322+ii)=hnuev+dhnu
                if((sc(kc)-sc(kc+220)).gt.0.d0) then      ! bord en emission
           sc(kc)=sc(kc+220) + (sc(kc)-sc(kc+220))*tk/dhnu
           ss=sc(kc+220)
                else                                      ! bord en absorption
           ss=sc(kc)
                endif                                                      !g1
           sc(322+ii)=ss+(sc(k1)-ss)*dhnu/dnusuiv
                if((rc(kc)-rc(kc+220)).gt.0.d0) then      ! bord en emission
           rc(kc)=rc(kc+220) + (rc(kc)-rc(kc+220))*tk/dhnu
           rr=rc(kc+220)
                else                                      ! bord en absorption
           rr=rc(kc)
                endif
           rc(322+ii)=rr+(rc(k1)-rr)*dhnu/dnusuiv
                if((ec(kc)-ec(kc+220)).gt.0.d0) then      ! bord en emission
           ec(kc)=ec(kc+220) + (ec(kc)-ec(kc+220))*tk/dhnu
           ee=ec(kc+220)
                else                                      ! bord en absorption
           ee=ec(kc)
                endif
           ec(322+ii)=ee+(ec(k1)-ee)*dhnu/dnusuiv
           fic(322+ii)=fic(kc)+(fic(k1)-fic(kc))*dhnu/dnusuiv
           tc(322+ii)=tc(kc)+(tc(k1)-tc(kc))*dhnu/dnusuiv
           nk(322+ii)=kc
c           write(9,*) '0',kc,hc(kc),lnuc(kc),rc(kc)
c           write(9,*) ii,kc,hc(322+ii),lnuc(322+ii),rc(322+ii)
              else                                             ! 2kt > dhnures
        if((sc(kc)-sc(kc+220)).le.0.d0.and.(rc(kc)-rc(kc+220)).le.0.d0)
     &                          goto 1100

              nn=nint(tk*0.5d0/dhnu)
              if(nn.eq.0) nn=1                                             !g1
              ll=lnuc(kc)
              k2=min(iw(i-1),iw(i-2))
c
c sfond=tc(kc+220) !ds 2009 version105.02e et suppression de la definition
c suivante de sfond, mais il n'y a pas de transmis dans le .ang !
              sfond=sc(kc+220)                                             !g1
              rfond=rc(kc+220)
              efond=ec(kc+220)
              if(k2.le.102.and.tk*2.d0.gt.(hnuev-thnuc(k2))) then
                 if(sc(k2).gt.sc(k2+220).and.sc(kc+220).gt.sc(k2+220))
     &           sfond=sc(k2+220) + (sc(k1)-sc(k2+220))
     &           *(hnuev-thnuc(k2))/(thnuc(k1)-thnuc(k2))
                 if(rc(k2).gt.rc(k2+220).and.rc(kc+220).gt.rc(k2+220))
     &           rfond=rc(k2+220) + (rc(k1)-rc(k2+220))
     &           *(hnuev-thnuc(k2))/(thnuc(k1)-thnuc(k2))
                 if(ec(k2).gt.ec(k2+220).and.ec(kc+220).gt.ec(k2+220))
     &           efond=ec(k2+220) + (ec(k1)-ec(k2+220))
     &           *(hnuev-thnuc(k2))/(thnuc(k1)-thnuc(k2))
              endif
              dse=sc(kc)-sfond
              dre=rc(kc)-rfond
              dee=ec(kc)-efond
c        if((rc(kc)-rc(kc+220)).lt.0.d0)print*,'abs.ds reflechi a kc=',kc!23456
c        if((ec(kc)-ec(kc+220)).lt.0.d0)print*,'abs.ds emis a kc=',kc
c
              do 1101 ik=1,6
                 ll=ll+nn                                                  !g1
                 if(ll.ge.lnuc(k1)) goto 1100
                 if(ik.eq.3.and.(ll+nn).lt.lnuc(k1)) goto 1101
                 if(ik.eq.5.and.(ll+nn).lt.lnuc(k1)) goto 1101
                 ii=ii+1
                 dd=dhnu*float(nn*ik)
                 lnuc(322+ii)=ll
                 hc(322+ii)=hnuev+dd
                 fic(322+ii)=fic(kc) + (fic(k1)-fic(kc))*dd/dnusuiv
                 sc(322+ii)=sfond + (sc(k1)-sfond)*dd/dnusuiv
     &                            + dse*exp(-dd/tk)
                 rc(322+ii)=rfond + (rc(k1)-rfond)*dd/dnusuiv
     &                            + dre*exp(-dd/tk)
                 tc(322+ii)=tc(kc) + (tc(k1)-tc(kc))*dd/dnusuiv
                 ec(322+ii)=efond + (ec(k1)-efond)*dd/dnusuiv
     &                            + dee*exp(-dd/tk)
                 nk(322+ii)=kc
c        if(kc.eq.9)print*,kc,k1,k2,nn,tk,dnusuiv,dhnu,sc(k1)-sfond,dse,
c     &  dd,sfond,(sc(k1)-sfond)*dd/dnusuiv,dse*exp(-dd/tk)
c        write(9,*) ii,kc,hc(322+ii),lnuc(322+ii),rc(322+ii)
 1101            continue
        endif
 1100                     continue
        if(ii.gt.178) WRITE(6,*) ' nombre de points rajoutes=',ii          !g1
        if(ii.gt.178) write(6,*) ' trop de pts continus rajoutes - STOP'
        if(ii.gt.178) write(6,*) ' REDIMENSIONNER LES TABLEAUX'
c-----------------------
c tri du continu
        n=500
        call INDEXTRI(n,lnuc,iw)
c        write(9,*) '--) continu juste apres tri   i,ke,hnu,lnu,rc' !
c        do i=1,500                                             !
c        ke=iw(i)                                               !
c        write(9,'(2i5,0pf12.5,i8,1pe12.3)') i,ke,hc(ke),lnuc(ke),rc(ke)    !
c        enddo                                                  !
c-----------------------
c suppression des points de meme frequence
c        write(9,*) '2) CONTINU nuF(nu) et log(hnuev*100)*2.302585/res'
        ii=0
                        do 1110 i=1,500
        ke=iw(i)
        if(lnuc(ke).le.0) goto 1110
        if(ii.ge.1.and.lnuc(ke).eq.lo(ii)) then
           if(s(ke).gt.so(ii)) kco(ii)=ke
           fo(ii)=max(fo(ii),fic(ke))
           so(ii)=max(so(ii),sc(ke))
           ro(ii)=max(ro(ii),rc(ke))
           to(ii)=max(to(ii),tc(ke))
           eo(ii)=max(eo(ii),ec(ke))                                      !g1
           goto 1110
           endif
        ii=ii+1
        kco(ii)=nk(ke)
        lo(ii)=lnuc(ke)
        fo(ii)=fic(ke)
        so(ii)=sc(ke)
        ro(ii)=rc(ke)
        to(ii)=tc(ke)
        eo(ii)=ec(ke)
        icmax=ii
c        write(9,83) hc(ke),sep,nk(ke),sep,fic(ke),sep,sc(ke),
c     &               sep,rc(ke),sep,tc(ke),sep,lnuc(ke)
 1110   continue
 83     format(0pf12.5,a,i4,4(a,1pe10.3),a,i10)
c        WRITE(6,*)' nombre de points du continu differents=',icmax
c-----------------------
c
c TRAITEMENT DES RAIES
c
c rajout de points de chaque cote de la raie
c        write(9,*) ' raies :  kr  lnur  r  s  et avant  et apres'           !
                             do 1025 kr=2,nraimax
            if(s(kr).eq.0.d0.and.r(kr).eq.0.d0) goto 1025
        lnur(kr+ntab)=lnur(kr) - 1                                         !g1
        s(kr+ntab)=0.d0
        r(kr+ntab)=0.d0
        t(kr+ntab)=0.d0
        e(kr+ntab)=0.d0
        lnur(kr+ntab*2)=lnur(kr) + 1
        s(kr+ntab*2)=0.d0
        r(kr+ntab*2)=0.d0
        t(kr+ntab*2)=0.d0
        e(kr+ntab*2)=0.d0
c            if(thnur(kr).ge.500.d0.and.thnur(kr).le.800.d0)
c     &
c        write(9,'(i4,i7,1p2e12.4,2(i10,1pe10.2))') kr,lnur(kr),r(kr), !234567
c     &       s(kr),lnur(kr+ntab),r(kr+ntab),lnur(kr+ntab*2),r(kr+ntab*2)   !
 1025                     continue   ! do kr
c-----------------------
c tri des raies et sommation des points de meme frequence
        n=ntab*3
        call INDEXTRI(n,lnur,iw)
c        write(9,*) '--) raies juste apres tri   i,kr,lnu,r'              !g1
c        do i=1,n                                                          !
c        kr=iw(i)                                                          !
c        write(9,'(2i7,i8,1p2e13.4)') i,kr,lnur(kr),r(kr),s(kr)            !
c        enddo                                                             !
c
c        write(9,*)                                                        !
c     &  '4) toutes les raies triees avec sommation i,kr,kco,lnu,r'        !
        ii=icmax
                        do 1210 i=1,n
        kr=iw(i)
        if(lnur(kr).le.0) goto 1210
        if(lnur(kr).eq.lo(ii)) then                  ! passages suivants
           if(abs(r(kr)).gt.abs(ro(ii))) kco(ii)=kr+ntab*2
           so(ii)=so(ii)+s(kr)
           ro(ii)=ro(ii)+r(kr)
           to(ii)=to(ii)+t(kr)
           eo(ii)=eo(ii)+e(kr)
           else
        ii=ii+1                                      ! 1er passage a ce lnu
        lo(ii)=lnur(kr)
        so(ii)=s(kr)
        ro(ii)=r(kr)
        to(ii)=t(kr)
        eo(ii)=e(kr)
        kco(ii)=kr + 1000            ! ko-raie va de 1000 a 1000+ntab*3
        endif
c        if(kco(ii).le.500) then
c           kco(ii)=1000 + kco(ii)
c           else if(kco(ii).le.1000) then
c           kco(ii)=500 + kco(ii)
c           endif
        imax=ii
c        write(9,'(2i7,2i8,1pe13.4)') ii,kr,kco(ii),lo(ii),ro(ii)         !g1
 1210                    continue
c
c        write(9,*)                                                       !
c     & '4b) continu ordonne puis raies ordonnees et sommees i,kco,lo,ro' !
c        do i=1,imax+2                                                    !
c        write(9,'(2i7,i8,1pe12.4)') i,kco(i),lo(i),ro(i)                 !
c        enddo                                                            !
c---------------------
c
c MELANGE des raies et du continu
        n=imax
        call INDEXTRI(n,lo,iw)
c        write(9,*)'5) melange juste apres tri   i,j,kco,lo,ro,hnucontinu'
c        do i=1,n                                               
c        j=iw(i)                                                 
c        if(kco(j).le.1000) write(9,'(2i7,2i8,1pe12.4,0pf13.5)')
c     &                    i,j,kco(j),lo(j),ro(j),hc(kco(j))
c        if(kco(j).gt.1000) write(9,'(2i7,2i8,1pe12.4)')
c     &                     i,j,kco(j),lo(j),ro(j)
c        enddo                                    
c
c sommation des points de meme frequence et resultat                       !g1
        Tmoy=tk/8.6171d-5
           if(ila.eq.0) then
        WRITE(14,80) resol
        if(ieqw.eq.1) WRITE(14,*) '2) CONTINU ET RAIES : nuF(nu)'
        if(ieqw.eq.0) WRITE(14,*) ' CONTINU ET RAIES : nuF(nu)'
        WRITE(14,1092) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep
           else
        WRITE(14,81) resol
        if(ieqw.eq.1)WRITE(14,'(a)')'#2) CONTINUUM AND LINES : nuF(nu)'
        if(ieqw.eq.0)WRITE(14,'(a)')'# CONTINUUM AND LINES : nuF(nu)'
        WRITE(14,1093) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep
           endif   ! ila
 80    format(/' resolution=',0pf10.1,'             (graftit105v03)')  !12345678
 81    format(/'# resolution=',0pf10.1)
 1092   format('  hnuev   ',a,'nuFentrant',a,
     &      'nuFreflechi',a,'nuFsortant',a,'nuFtransapp',a,
     &      'nuFcontrefl',a,'nuFcontsort',a,'kc ',a,'kr ',a,'ion',a)
 1093   format('# hnuev   ',a,'nuFincident',a,
     &      'nuFreflected',a,'nuFoutward',a,'nuFtransapp',a,
     &      'nuFcontref',a,'nuFcontout',a,'kc ',a,'kr ',a,'ion',a)
c
        ii=0                                                               !g1
                       do 1300 i=1,n
        j=iw(i)
           if(lo(j).le.0) goto 1300
           if(i.gt.1.and.lo(j).eq.lo(iw(i-1))) goto 1300         ! deja traite
        hnuev=10.d0**(res*float(lo(j))/2.302585d0 - 2.d0)
           if(hnuev.lt.0.0118d0) goto 1300
        j1=iw(i+1)
        ii=ii+1
        if(lo(j).eq.lo(j1)) then            ! forcement 1 raie et 1 continu
                  ss=so(j) + so(j1)
                  rr=ro(j) + ro(j1)
                  tt=to(j) + to(j1)
                  ee=eo(j) + eo(j1)
                  if(ss.le.1.d-36) ss=1.111d-36
                  if(rr.le.1.d-36) rr=1.111d-36
                  if(tt.le.1.d-36) tt=1.111d-36
                  if(ee.le.1.d-36) ee=1.111d-36
                  kr=0
                  if(kco(j).le.1000) then
                     jc=j
                     kc=kco(j)
                     if(kco(j1).le.1000+ntab) kr=kco(j1)-1000
                     ff=fo(j)
                     ssc=so(j)
                     rrc=ro(j)
                     ttc=to(j)
                     eec=eo(j)                                             !g1
                     else   ! kco(j)>1000
                     jc=j1
                     kc=kco(j1)
                     if(kco(j).le.1000+ntab) kr=kco(j)-1000
                     ff=fo(j1)
                     ssc=so(j1)
                     rrc=ro(j1)
                     ttc=to(j1)
                     eec=eo(j1)
                     endif   ! kco(j)
                   if(ff.le.1.d-36) ff=1.111d-36
                   if(ssc.le.1.d-36) ssc=1.111d-36
                   if(rrc.le.1.d-36) rrc=1.111d-36
                   if(ttc.le.1.d-36) ttc=1.111d-36
                   if(eec.le.1.d-36) eec=1.111d-36
                   WRITE(14,1087)hnuev,sep,ff,sep,rr,sep,ss,sep,tt,sep,
     &                   rrc,sep,ssc,sep,kc,sep,kr,sep,nomrai(kr)
                  goto 1300
                  endif
        if(kco(j).lt.1000) then                                  ! continu
             kc=kco(j)
             kr=0
             jc=j
             ff=fo(j)
             ss=so(j)
             rr=ro(j)
             tt=to(j)
             ee=eo(j)
             if(ff.le.1.d-36) ff=1.111d-36                                !g1
             if(ss.le.1.d-36) ss=1.111d-36
             if(rr.le.1.d-36) rr=1.111d-36
             if(tt.le.1.d-36) tt=1.111d-36
             if(ee.le.1.d-36) ee=1.111d-36
             WRITE(14,1087) hnuev,sep,ff,sep,rr,sep,ss,sep,tt,sep,
     &                      rr,sep,ss,sep,kc,sep,kr,sep,nomrai(kr)
c
             else                                            ! raies
        dd=dfloat(lo(j)-lo(jc))/dfloat(lo(jc+1)-lo(jc))
        ssc=so(jc) + (so(jc+1)-so(jc))*dd
        rrc=ro(jc) + (ro(jc+1)-ro(jc))*dd
        ttc=to(jc) + (to(jc+1)-to(jc))*dd
        eec=eo(jc) + (eo(jc+1)-eo(jc))*dd
        ss=so(j) + ssc
        rr=ro(j) + rrc
        tt=to(j) + ttc
        ee=eo(j) + eec
        kr=kco(j) - 1000
        if(kco(j).gt.1000+ntab) kr=0
        kc=0
        if(ss.le.1.d-36) ss=1.111d-36
        if(rr.le.1.d-36) rr=1.111d-36
        if(tt.le.1.d-36) tt=1.111d-36
        if(ee.le.1.d-36) ee=1.111d-36
        if(ssc.le.1.d-36) ssc=1.111d-36                                  !g1 
        if(rrc.le.1.d-36) rrc=1.111d-36
        if(ttc.le.1.d-36) ttc=1.111d-36
        if(eec.le.1.d-36) eec=1.111d-36
        ff=fo(jc) + (fo(jc+1)-fo(jc))*dd
        if(ff.le.1.d-36) ff=1.111d-36
c        write(9,'(i4,5i8,1p3e12.4)') kc,lo(j),j,jc,lo(jc),lo(jc+1),
c     &                               ro(j),ro(jc),ro(jc+1)
         WRITE(14,1087) hnuev,sep,ff,sep,rr,sep,ss,sep,tt,sep,
     &                  rrc,sep,ssc,sep,kc,sep,kr,sep,nomrai(kr)
        endif
 1087   format(0pf13.5,6(a,1pe10.3),a,i3,a,i4,a,a7)
 1300                   continue
c  hnuev   ;nuFentrant;nuFreflechi;nuFsortant;nuFtransapp;nuFcontrefl;nuFcontsort;kc ;kr;ion
c 25000.78901; 1.345E+89; 1.345E+89; 1.345E+89; 1.345E+89; 1.345E+89; 1.345E+89;123;1234;1234567
c        WRITE(6,*)' nombre total de points du spectre=',ii
c
        return
        end                                                              !graf1
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine GRAF2(resol,tk)
c
c               CAS OU DLAMBDA EST CONSTANT ET LES RAIES GAUSSIENNES
c
        implicit double precision (a-h,o-z)
        parameter (nrais=4200)
        character sep*1,nomrai*7
        dimension fo(90000),so(90000),ro(90000),to(90000),eo(90000),
     &      soc(90000),roc(90000),toc(90000),p(4001)
        dimension iw(90000),
     &      lo(90000),kco(90000),kro(90000),kas(90000)
        dimension tlambc(322)
        common/grkcr/nraimax,ntab,kf,kcr(nrais)
        common/gr/s(nrais*3),r(nrais*3),t(nrais*3),e(nrais*3),
     &            fic(500),sc(500),rc(500),tc(500),ec(500)
        common/grc/thnuc(500),alo(nrais)
        common/grl/lambr(nrais*3),lambc(500),nk(500),ieqw,ila
        common/nom/nomrai(0:nrais)

c graf2
c        tab=';'
        if(ila.eq.0) sep=';'
        if(ila.eq.1) sep=' '
c si dlambda=pas*0.05  pas=0.01   99ev a 24.818kev => 0.5 a 125.11A
c     12462pts de continu - 17305pts ds les fichiers lo,so,... hnumin*dlambda=5
c il faut limiter a 90000pts - il y a au maximum 4000*3pts de raies (blends)
c il faut : 12400*5/(hnumin*dlambda)<78000
        dlambda=resol
        pas=resol/5.d0
        hnumin=99.d0
        if(hnumin*dlambda.lt.0.8d0) then
           hnumin=0.8d0/dlambda
           WRITE(6,*)
           WRITE(6,'(a,f7.1,a)')'because array dimension,'//
     &          'spectrum is computed beyond',hnumin,'eV'
           WRITE(14,'(a,f7.1,a)')'because array dimension,'//
     &          'spectrum is computed beyond',hnumin,'eV'
        endif
        alambdamax=12398.58d0/hnumin
c
        do k=1,90000
           lo(k)=0                                                         !g2
        enddo
        do kc=1,322
           tlambc(kc)=12398.58d0/thnuc(kc)
           if(thnuc(kc).lt.hnumin) then
              lambc(kc)=0.d0
           else if(kc.le.220) then   ! ln(10)
              lambc(kc)=nint(tlambc(kc)/pas)
           else
              lambc(kc)=lambc(kc-220)+1
           endif
        enddo
        do kr=1,nraimax
           if(alo(kr).ge.alambdamax) then
              lambr(kr)=0
           else
              lambr(kr)=nint(alo(kr)/pas)
           endif
        enddo
c
c TRAITEMENT DU CONTINU
c tri du continu brut                                                      !g2
        n=322
        call INDEXTRI(n,lambc,iw)
c        write(9,*)'1) continu juste apres tri  i,kc,hnu,lambda,lambc,rc'!
c        do i=1,n                                                        !
c           kc=iw(i)                                                     !
c           write(9,'(2i5,0p2f13.5,i8,1pe12.3)')                         !
c     &           i,kc,thnuc(kc),tlambc(kc),lambc(kc),rc(kc)             !234567
c        enddo                                                           !
c-----------------------
c rajout de points dans le continu - remplissage des trous
        j=0                                                                !g2
                        do 100 i=1,322
        kc=iw(i)
           if(lambc(kc).le.0) goto 100
           if(thnuc(kc).lt.hnumin) goto 100
        if(i.lt.322) then
           k1=iw(i+1)
           n=lambc(k1)-lambc(kc)
        endif   ! i<322
        dhnu=pas *thnuc(kc)**2 *2.41797d14/2.997925d18
        nkt=nint(tk*2.d0/dhnu)
             if(j.ge.1.and.lambc(kc).eq.lo(j))then ! kc=8 et 310
                km1=iw(i-1)
                if(kc.lt.km1) then
                   lo(j)=lambc(kc)
                   fo(j)=fic(kc)
                   so(j)=sc(kc)
                   ro(j)=rc(kc)
                   to(j)=tc(kc)
                   eo(j)=ec(kc)
                   kco(j)=kc
                endif
                goto 100                                                   !g2
             endif
c
c        if((kc.gt.102).or.n.lt.nkt) then       ! kc > 102 ou nusuivant-nu < 2kT
        j=j+1                                                   ! cas normal
        ll=lambc(kc)                  ! interpolation entre les 2 nu consecutifs
        ff=fic(kc)
        ss=sc(kc)
        rr=rc(kc)
        tt=tc(kc)
        ee=ec(kc)
        lo(j)=ll
        fo(j)=ff
        so(j)=ss
        ro(j)=rr
        to(j)=tt
        eo(j)=ee
        kco(j)=kc
        kas(j)=20                                                          !g2
c          if(i.eq.322) jcmax=j
          if(i.eq.322) goto 100
          if(n.le.1) goto 100
        df=(fic(k1)-ff)/dfloat(n)
        ds=(sc(k1)-ss)/dfloat(n)
        dr=(rc(k1)-rr)/dfloat(n)
        dt=(tc(k1)-tt)/dfloat(n)
        de=(ec(k1)-ee)/dfloat(n)
          do m=1,n-1
          j=j+1
          ll=ll+1
          ff=ff+df
          ss=ss+ds
          rr=rr+dr
          tt=tt+dt
          ee=ee+de
          lo(j)=ll
          fo(j)=ff
          so(j)=ss
          ro(j)=rr
          to(j)=tt
          eo(j)=ee
          kco(j)=0
          kas(j)=21                                                        !g2
          enddo
       goto 100
c       endif   ! interpolation
c pour       if((kc.le.102).and.n.ge.nkt) then        ! nusuivant-nu > 2kT
c pas fait                               ! rajout de points en exp(-dnu/kt)
c peut etre utile pour les edges des fe19a21 & si13-14 (1450-2750ev) !
c c est complique: les rajouts se font a l'envers, pour les j plus petits
 100                        continue
        jcmax=j                     ! et non: nint(alambdamax/pas)
c        WRITE(9,*) ' nombre de points de continu en lambda=',jcmax
        if(jcmax.gt.80000) then
        write(6,*)' trop de pts continus rajoutes - STOP - jcmax=',jcmax
        write(6,*) ' REDIMENSIONNER LES TABLEAUX'
        endif
c-----------------------
c        write(9,*) '2) continu tous les pts  cas,j,kco,lo,ro,hnu,lambda'    !
c        do j=1,jcmax                                                        !
c           alambda=float(lo(j))*pas                                         !
c           hnuev=12398.58d0/alambda                                         !
c        if(kco(j).ne.0) write(9,'(i4,i7,i5,i7,1pe12.3,0p2f10.2,0pf9.3)')    !
c     &          kas(j),j,kco(j),lo(j),ro(j),hnuev,thnuc(kco(j)),alambda     !
c        if(kco(j).eq.0) write(9,'(i4,i7,i5,i7,1pe12.3,0pf10.2,0pf9.3)')     !
c     &          kas(j),j,kco(j),lo(j),ro(j),hnuev,alambda                   !
c        enddo                                                               !
c        write(9,*) ' continu tous les pts   cas,j,kc,lo,ro,hnu,lambda'      !
c-----------------------
c
c TRAITEMENT DES RAIES
c
c mise en table et rajout de points de chaque cote de la raie               !g2
c        write(9,*)'3) raies : kr lambda lambr r et avant  et apres'         !
                             do 25 kr=2,nraimax
            if(s(kr).eq.0.d0.and.r(kr).eq.0.d0) goto 25
            if(alo(kr).ge.alambdamax) goto 25
        lambr(kr+ntab)=lambr(kr) - 1                    ! ntab=nrais=4200
        s(kr+ntab)=0.d0
        r(kr+ntab)=0.d0
        t(kr+ntab)=0.d0
        e(kr+ntab)=0.d0
        lambr(kr+ntab*2)=lambr(kr) + 1
        s(kr+ntab*2)=0.d0
        r(kr+ntab*2)=0.d0
        t(kr+ntab*2)=0.d0
        e(kr+ntab*2)=0.d0
c---
c       write(9,'(i4,0pf9.3,i8,1pe12.4,2(i10,1pe10.2))')kr,alo(kr),lambr!234567
c     &(kr),r(kr),lambr(kr+ntab),r(kr+ntab),lambr(kr+ntab*2),r(kr+ntab*2)   !
c---
 25                              continue   ! do kr
c-----------------------
c tri des raies et sommation des points de meme frequence
c puis on les range a la suite du continu                                  !g2
        n=ntab*3
        call INDEXTRI(n,lambr,iw)
c---
c        write(9,*) '3+) raies juste apres tri   i,kr,lambr,lambda,r'       !
c        do i=1,n                                                           !
c        kr=iw(i)                                                           !
c           if(kr.le.nrais) then                                            !
c        write(9,'(2i6,i7,0pf11.3,1pe13.4)') i,kr,lambr(kr),alo(kr),r(kr)   !
c           else if(kr.le.nrais*2) then                                     !
c        write(9,'(2i6,i7,0pf11.3,1pe13.4)')                                !
c     &        i,kr,lambr(kr),alo(kr-nrais),r(kr)                           !
c           else if(kr.le.nrais*3) then                                     !
c        write(9,'(2i6,i7,0pf11.3,1pe13.4)')                                !
c     &        i,kr,lambr(kr),alo(kr-nrais*2),r(kr)                         !
c           else                                                            !
c              r(kr)=1.1111d0                                               !
c        write(9,'(2i6,i7,0pf11.3,1pe13.4)') i,kr,lambr(kr),alo(kr),r(kr)   !
c           endif                                                           !
c        enddo                                                              !
c        write(9,*)                                                         !
c     &  '4) toutes les raies triees avec sommation i,kr,kro,lnu,r'         !
c---
        ii=jcmax                                                           !g2
                        do 210 i=1,n
        kr=iw(i)
        if(lambr(kr).le.0) goto 210
        if(lambr(kr).eq.lo(ii)) then                  ! passages suivants
           if(abs(r(kr)).gt.abs(ro(ii))) kro(ii)=kr
           so(ii)=so(ii)+s(kr)
           ro(ii)=ro(ii)+r(kr)
           to(ii)=to(ii)+t(kr)
           eo(ii)=eo(ii)+e(kr)
           else
        ii=ii+1                                      ! 1er passage a ce lnu
        lo(ii)=lambr(kr)
        so(ii)=s(kr)
        ro(ii)=r(kr)
        to(ii)=t(kr)
        eo(ii)=e(kr)
        kco(ii)=0
        kro(ii)=kr                                  ! kro va de 1 a ntab*3
        endif
        imax=ii
c        write(9,'(2i7,2i8,1pe13.4)') ii,kr,kro(ii),lo(ii),ro(ii)          !
 210                        continue
c        WRITE(6,*) ' nombre de points du tableau=',imax
        if(imax.gt.90000) then
        write(6,*) ' trop de pts - STOP - imax=',imax                      !g2
        write(6,*) ' REDIMENSIONNER LES TABLEAUX'
        endif
c
c        write(9,*)                                                        !
c     &'4+) continu ordonne puis raies ordonnees et sommees  i,kco,lo,ro'  !
c        do i=1,imax+2                                                     !
c        write(9,'(2i7,i8,1pe12.4)') i,kco(i),lo(i),ro(i)                  !
c        enddo                                                             !
c---------------------
c
c MELANGE des raies et du continu
c
        n=imax
        call INDEXTRI(n,lo,iw)
c        write(9,*) '5) entrelacement    i,j,kco,kro,lo,ro'           !
c        do i=1,n                                                     !
c        j=iw(i)                                                      !
c        if(kco(j).gt.0) write(9,'(2i7,3i8,1pe12.4,0pf13.5)')         !
c     &     i,j,kco(j),kro(j),lo(j),ro(j),thnuc(kco(j))               !
c        if(kco(j).eq.0) write(9,'(2i7,3i8,1pe12.4)')                 !
c     &      i,j,kco(j),kro(j),lo(j),ro(j)
c        enddo                                                        !
c                                                                          !g2
c sommation du continu et des raies
c
c        write(9,*) '6) melange    i,jc,jr,kco,kro,lo,ro,lambda'       !
                       do 300 i=1,n-1
        j=iw(i)
        if(lo(j).eq.0) goto 300
        if(i.gt.1.and.lo(j).eq.lo(iw(i-1))) goto 300         ! deja traite
        j1=iw(i+1)
        if(lo(j).eq.lo(j1)) then            ! forcement 1 raie et 1 continu
                  jc=min(j,j1)
                  jr=max(j,j1)
                  soc(jc)=so(jc)
                  roc(jc)=ro(jc)
                  toc(jc)=to(jc)
                  so(jc)=so(j) + so(j1)
                  ro(jc)=ro(j) + ro(j1)
                  to(jc)=to(j) + to(j1)
                  eo(jc)=eo(j) + eo(j1)                                   !g2
                  kro(jc)=kro(jr)
c ne surtout pas mettre: if(so(jc)<0) so(jc)=0.  if(ro(jc)<0) ro(jc)=0.
c        write(9,'(3i7,3i8,1pe12.4,0pf10.4)') i,jc,jr,kco(jc),kro(jc),
c     &                                       lo(j),ro(jc)
                  else
                  soc(j)=so(j)
                  roc(j)=ro(j)
                  toc(j)=to(j)
c        write(9,'(3i7,3i8,1pe12.4,0pf10.4)') i,j,j,kco(j),kro(j),
c     &                         lo(j),ro(j),pas*float(lo(j))
                  endif
 300                                 continue
c---------------------
c
c MOYENNE MOBILE en forme de gaussienne pour obtenir la resolution demandee
c
        Tmoy=tk/8.6171d-5
           if(ila.eq.0) then
        WRITE(14,80) dlambda
        if(ieqw.eq.1) WRITE(14,*) '2) CONTINU ET RAIES : nuF(nu)'
        if(ieqw.eq.0) WRITE(14,*) ' CONTINU ET RAIES : nuF(nu)'
        WRITE(14,92) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep,sep        !TITAN
           else                                                            !g2
        WRITE(14,81) dlambda
        if(ieqw.eq.1)WRITE(14,'(a)')'#2) CONTINUUM AND LINES : nuF(nu)'
        if(ieqw.eq.0)WRITE(14,'(a)') '# CONTINUUM AND LINES : nuF(nu)'
        WRITE(14,93) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep,sep
           endif   ! ila
 80    format(/' largeur de raie (FWHM)=',0pf10.1,'  (graftit105v03)') !1234567
 81    format(/'# line width (FWHM)=',0pf10.4)
 92    format('  lambda  ',a,'hnuev ',a,'nuFentrant',a,
     &     'nuFreflechi',a,'nuFsortant',a,'nuFtransapp',a,
     &     'nuFcontrefl',a,'nuFcontsort',a,'kc ',a,'kr ',a,'ion',a)
 93    format('# lambda  ',a,'hnuev ',a,'nuFincident',a,
     &     'nuFreflected',a,'nuFoutward',a,'nuFtransapp',a,
     &     'nuFcontref',a,'nuFcontout',a,'kc ',a,'kr ',a,'ion',a)
c
        dlr=dlambda/pas/2.d0        ! 2.5
        j0=nint(dlr*2)+1            ! dlr*2+1=6
        mm=j0*2-1                   ! dlr*4+1=11
        p(j0)=1.d0
        som=1.d0                                                            !g2
c        jcmax voisin de nint(alambdamax/pas)  defini plus haut
                        do 410 j=j0+1,mm
c  p(j)=exp(- (lamb-lamb0)**2/(dlambda**2/(4*Ln2)) ) si dlambda=fwhm
        p(j)=exp(-(float(j-j0)*0.8325546d0/dlr)**2)              ! sqrt(Ln2)
        p(2*j0-j)=p(j)
        som=som+2.d0*p(j)
 410                        continue
        fm=som
c        type*,(p(j),j=1,mm),som
                        do 400 i=1,jcmax-mm+1
        alambda=pas*float(lo(i+j0-1))
        hnuev=12398.54d0/alambda
        ff=0.d0
        ss=0.d0
        rr=0.d0
        ssc=0.d0
        rrc=0.d0
        tt=0.d0
        ee=0.d0
        kc=0
        kr=0
        kk=0
           do 401 j=1,mm
           ff=ff+fo(i+j-1)*p(j)
           ss=ss+so(i+j-1)*p(j)
           rr=rr+ro(i+j-1)*p(j)
           ssc=ssc+soc(i+j-1)*p(j)
           rrc=rrc+roc(i+j-1)*p(j)
           tt=tt+to(i+j-1)*p(j)
           ee=ee+eo(i+j-1)*p(j)
           if(kco(i+j-1).gt.0) then
              kk=kco(i+j-1)
              if(kc.eq.0) kc=kk
              kc=min(kc,kk)
              endif
           if(kro(i+j-1).gt.0.and.kro(i+j-1).le.ntab) then
              kkr=kro(i+j-1)                                              !g2
              if(kr.eq.0) kr=kkr
              kr=min(kr,kkr)
           endif
 401       continue
        ff=ff/fm
        ss=ss/fm
        rr=rr/fm
        ssc=ssc/fm
        rrc=rrc/fm
        tt=tt/fm
        ee=ee/fm
        if(ff.le.1.d-36) ff=1.111d-36
        if(ss.le.1.d-36) ss=1.111d-36
        if(rr.le.1.d-36) rr=1.111d-36
        if(ssc.le.1.d-36) ssc=1.111d-36
        if(rrc.le.1.d-36) rrc=1.111d-36
        if(tt.le.1.d-36) tt=1.111d-36
        if(ee.le.1.d-36) ee=1.111d-36
        WRITE(14,88) alambda,sep,hnuev,sep,ff,sep,rr,sep,ss,sep,tt,sep,
     &               rrc,sep,ssc,sep,kc,sep,kr,sep,nomrai(kr)
 400                        continue
 88     format(0pf9.3,a,0pf9.2,6(a,1pe10.3),a,i3,a,i4,a,a7)
c lambda  ;hnuev ;nuFentrant;nuFreflechi;nuFsortant;nuFtransapp;nuFcontrefl;nuFcontsort;kc ;kr ;ion
c124.789; 25000.89; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90;123;1234;1234567
c        WRITE(6,*)' nombre total de points du spectre=',jcmax
c
        return
        end                                                              !graf2
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine GRAF3(resol,tk)
c
c               CAS OU DLAMBDA EST CONSTANT ET LES RAIES GAUSSIENNES
c
        implicit double precision (a-h,o-z)
        parameter (nrais=4200)
        character sep*1,nomrai*7
        dimension fo(90000),so(90000),ro(90000),to(90000),eo(90000),
     &      soc(90000),roc(90000),toc(90000),p(4001)
        dimension iw(90000),
     &      lo(90000),kco(90000),kro(90000),kas(90000)
        dimension tlambc(322)
        common/grkcr/nraimax,ntab,kf,kcr(nrais)
        common/gr/s(nrais*3),r(nrais*3),t(nrais*3),e(nrais*3),
     &            fic(500),sc(500),rc(500),tc(500),ec(500)
        common/grc/thnuc(500),alo(nrais)
        common/grl/lambr(nrais*3),lambc(500),nk(500),ieqw,ila
        common/nom/nomrai(0:nrais)
c graf3
c        tab=';'
        if(ila.eq.0) sep=';'
        if(ila.eq.1) sep=' '
        pas=resol
        if(ila.eq.0) then
           WRITE(6,*) 
     &'  entrer 5 parametres:'
           WRITE(6,*)
     &'       la largeur de raie minimum DLAMBDA(Angstrom - FWHM),',
     &'                             (elargissement dlambda=cst)',
     &'       la vitesse de (micro ou macro)TURBULENCE(km/s- HW a 1/e)',
     &'                             (elargissement dlambda/lambda=cst)',
     &'       la vitesse d''EJECTION du nuage (km/s- HW)',
     &'  et   l etendue du spectre HNUMIN HNUMAX(=<20000) (en eV)'
        else
           WRITE(6,*)
     &'  enter 5 parameters:'
           WRITE(6,*)
     &'      the minimum line width DLAMBDA(Angstrom - FWHM),', 
     &'                            (widening dlambda=constant)',
     &'      the (micro or macro) TURBULENCE velocity(km/s - HW at 1/e)'
     &,'                            (widening dlambda/lambda=constant)',
     &'      the OUTFLOW velocity (km/s)',
     &'  and the spectrum range:  HNUMIN  HNUMAX(<25000) (in eV)'
        endif
        READ(5,*) dlambda,vturbkm,vejectkm,hnumin,hnumax
        if(hnumin.lt.0.d0) hnumin=100.d0
        if(hnumax.lt.0.d0) hnumax=26000.d0                                 !g3
        hnumin=hnumin*0.9d0                ! pour inclure les points
        hnumax=hnumax*1.25d0                ! juste au dela des bornes
c si dlambda=pas*0.05  pas=0.01   99ev a 24.818kev => 0.5 a 125.11A
c     12462pts de continu - 17305pts ds les fichiers lo,so,... hnumin*dlambda=5
c il faut limiter a 90000pts - il y a au maximum 4000*3pts de raies (blends)
c il faut : 12400*5/(hnumin*dlambda)<78000
        if(hnumin*dlambda.lt.0.8d0) then
           hnumin=0.8d0/dlambda
           WRITE(6,*)
           if(ila.eq.0) then
              WRITE(6,'(a,f8.1,a)') 'a cause de la dimension des '//
     &           'tableaux, le spectre est calcule avec',
     &           '                      HNUMIN=',hnumin/0.9d0,'eV'
              WRITE(14,'(a,f8.1,a)')'a cause de la dimension des '//
     &           'tableaux, le spectre est calcule avec',
     &           '                      HNUMIN=',hnumin/0.9d0,'eV'
           else
              WRITE(6,'(a,f8.1,a)') 'because array dimension,'//
     &          'spectrum is computed only beyond',hnumin/0.9d0,'eV'
              WRITE(14,'(a,f8.1,a)')'because array dimension,'//
     &          'spectrum is computed only beyond',hnumin/0.9d0,'eV'
           endif
        endif   ! hnumin
        alambdamax=12398.58d0/hnumin
        alambdamin=12398.58d0/hnumax
        lmin=nint(alambdamin/pas)-10
c
        do k=1,90000
           lo(k)=0                                                        !g3
        enddo
        do kc=1,322
           tlambc(kc)=12398.58d0/thnuc(kc)
           if(thnuc(kc).lt.hnumin.or.thnuc(kc).gt.hnumax) then
              lambc(kc)=0.d0
           else if(kc.le.220) then                                  ! ln(10)
              lambc(kc)=nint(tlambc(kc)/pas) -lmin
           else
              lambc(kc)=lambc(kc-220)+1
           endif
        enddo
        do kr=1,nraimax
           if(alo(kr).gt.alambdamax.or.alo(kr).lt.alambdamin) then
              lambr(kr)=0                                                  !g3
           else
              lambr(kr)=nint(alo(kr)/pas) -lmin
           endif
        enddo
c
c TRAITEMENT DU CONTINU
c tri du continu brut
        n=322
        call INDEXTRI(n,lambc,iw)
c        write(9,*)'1) continu juste apres tri  i,kc,hnu,lambda,lambc,rc'!
c        do i=1,n                                                        !
c           kc=iw(i)                                                     !
c           write(9,'(2i5,0p2f13.5,i8,1pe12.3)')                         !
c     &           i,kc,thnuc(kc),tlambc(kc),lambc(kc),rc(kc)             !234567
c        enddo                                                           !
c-----------------------
c rajout de points dans le continu - remplissage des trous
        j=0                                                                !g3
                        do 100 i=1,322
        kc=iw(i)
           if(lambc(kc).le.0) goto 100
           if(thnuc(kc).lt.hnumin.or.thnuc(kc).gt.hnumax) goto 100
        if(i.lt.322) then
           k1=iw(i+1)
           n=lambc(k1)-lambc(kc)
        endif   ! i<322
        dhnu=pas *thnuc(kc)**2 *2.41797d14/2.997925d18
        nkt=nint(tk*2.d0/dhnu)
             if(j.ge.1.and.lambc(kc).eq.lo(j))then ! kc=8 et 310
                km1=iw(i-1)
                if(kc.lt.km1) then
                   lo(j)=lambc(kc)
                   fo(j)=fic(kc)
                   so(j)=sc(kc)
                   ro(j)=rc(kc)
                   to(j)=tc(kc)
                   eo(j)=ec(kc)
                   kco(j)=kc
                endif
                goto 100                                                   !g3
             endif
c
c        if((kc.gt.102).or.n.lt.nkt) then       ! kc > 102 ou nusuivant-nu < 2kT
        j=j+1                                                   ! cas normal
        ll=lambc(kc)                  ! interpolation entre les 2 nu consecutifs
        ff=fic(kc)
        ss=sc(kc)
        rr=rc(kc)
        tt=tc(kc)
        ee=ec(kc)
        lo(j)=ll
        fo(j)=ff
        so(j)=ss
        ro(j)=rr
        to(j)=tt
        eo(j)=ee
        kco(j)=kc
        kas(j)=20                                                          !g3
          if(i.eq.322) goto 100        
          if(n.le.1) goto 100
        df=(fic(k1)-ff)/dfloat(n)
        ds=(sc(k1)-ss)/dfloat(n)
        dr=(rc(k1)-rr)/dfloat(n)
        dt=(tc(k1)-tt)/dfloat(n)
        de=(ec(k1)-ee)/dfloat(n)
          do m=1,n-1
          j=j+1
          ll=ll+1
          ff=ff+df
          ss=ss+ds
          rr=rr+dr
          tt=tt+dt
          ee=ee+de
          lo(j)=ll
          fo(j)=ff
          so(j)=ss
          ro(j)=rr
          to(j)=tt
          eo(j)=ee
          kco(j)=0
          kas(j)=21                                                        !g3
          enddo
       goto 100
c       endif   ! interpolation
c       if((kc.le.102).and.n.ge.nkt) then              ! nusuivant-nu > 2kT
c pas fait                               ! rajout de points en exp(-dnu/kt)
c peut etre utile pour les edges des fe19a21 & si13-14 (1450-2750ev) !
c c est complique: les rajouts se font a l'envers, pour les j plus petits
 100                     continue
        jcmax=j                     ! et non: nint(alambdamax/pas)
c        WRITE(9,*) ' nombre de points de continu en lambda=',jcmax
        if(jcmax.gt.80000) then
	write(6,*)' trop de pts continus rajoutes - STOP - jcmax=',jcmax
        write(6,*) ' REDIMENSIONNER LES TABLEAUX'
	endif
c-----------------------
c        write(9,*) '2) continu tous les pts  cas,j,kco,lo,ro,hnu,lambda'    !
c        do j=1,jcmax                                                        !
c           alambda=float(lo(j)+lmin)*pas                                    !
c           hnuev=12398.58d0/alambda                                         !
c        if(kco(j).ne.0) write(9,'(i4,i7,i5,i7,1pe12.3,0p2f10.2,0pf9.3)')    !
c     &          kas(j),j,kco(j),lo(j),ro(j),hnuev,thnuc(kco(j)),alambda     !
c        if(kco(j).eq.0) write(9,'(i4,i7,i5,i7,1pe12.3,0pf10.2,0pf9.3)')     !
c     &          kas(j),j,kco(j),lo(j),ro(j),hnuev,alambda                   !
c        enddo                                                               !
c        write(9,*) ' continu tous les pts   cas,j,kc,lo,ro,hnu,lambda'      !
c-----------------------
c
c TRAITEMENT DES RAIES
c
c mise en table et rajout de points de chaque cote de la raie               !g3
c        write(9,*)'3) raies : kr lambda lambr r et avant  et apres'         !
                             do 25 kr=2,nraimax
            if(s(kr).le.0.d0.and.r(kr).le.0.d0) goto 25
            if(alo(kr).gt.alambdamax.or.alo(kr).lt.alambdamin) goto 25
        lambr(kr+ntab)=lambr(kr) - 1                    ! ntab=nrais=4200
        s(kr+ntab)=0.d0
        r(kr+ntab)=0.d0
        t(kr+ntab)=0.d0
        e(kr+ntab)=0.d0
        lambr(kr+ntab*2)=lambr(kr) + 1
        s(kr+ntab*2)=0.d0
        r(kr+ntab*2)=0.d0
        t(kr+ntab*2)=0.d0
        e(kr+ntab*2)=0.d0
c---
c       write(9,'(i4,0pf9.3,i8,1pe12.4,2(i10,1pe10.2))')kr,alo(kr),lambr!234567
c     &(kr),r(kr),lambr(kr+ntab),r(kr+ntab),lambr(kr+ntab*2),r(kr+ntab*2)   !
c--- 
 25                         continue   ! do kr
c-----------------------
c tri des raies et sommation des points de meme frequence
c puis on les range a la suite du continu                                  !g3
        n=ntab*3
        call INDEXTRI(n,lambr,iw)
c---
c        write(9,*) '3+) raies juste apres tri   i,kr,lambr,lambda,r'       !
c        do i=1,n                                                           !
c        kr=iw(i)                                                           !
c           if(kr.le.nrais) then                                            !
c        write(9,'(2i6,i7,0pf11.3,1pe13.4)') i,kr,lambr(kr),alo(kr),r(kr)   !
c           else if(kr.le.nrais*2) then                                     !
c        write(9,'(2i6,i7,0pf11.3,1pe13.4)')                                !
c     &        i,kr,lambr(kr),alo(kr-nrais),r(kr)                           !
c           else if(kr.le.nrais*3) then                                     !
c        write(9,'(2i6,i7,0pf11.3,1pe13.4)')                                !
c     &        i,kr,lambr(kr),alo(kr-nrais*2),r(kr)                         !
c           else                                                            !
c              r(kr)=1.1111d0                                               !
c        write(9,'(2i6,i7,0pf11.3,1pe13.4)') i,kr,lambr(kr),alo(kr),r(kr)   !
c           endif                                                           !
c        enddo                                                              !
c        write(9,*)                                                         !
c     &  '4) toutes les raies triees avec sommation i,kr,kro,lnu,r'         !
c---                                                                       !g3
        ii=jcmax
                        do 210 i=1,n
        kr=iw(i)
        if(lambr(kr).le.0) goto 210
        if(lambr(kr).eq.lo(ii)) then                  ! passages suivants
           if(abs(r(kr)).gt.abs(ro(ii))) kro(ii)=kr
           so(ii)=so(ii)+s(kr)
           ro(ii)=ro(ii)+r(kr)
           to(ii)=to(ii)+t(kr)
           eo(ii)=eo(ii)+e(kr)
           else
        ii=ii+1                                      ! 1er passage a ce lnu
        lo(ii)=lambr(kr)
        so(ii)=s(kr)
        ro(ii)=r(kr)
        to(ii)=t(kr)
        eo(ii)=e(kr)
        kco(ii)=0
        kro(ii)=kr                                  ! kro va de 1 a ntab*3
        endif
        imax=ii                                                            !g3
c        write(9,'(2i7,2i8,1pe13.4)') ii,kr,kro(ii),lo(ii),ro(ii)          !
 210                    continue
c        WRITE(9,*) ' nombre de points du tableau=',imax
        if(imax.gt.90000) then
	write(6,*) ' trop de pts - STOP - imax=',imax
        write(6,*) ' REDIMENSIONNER LES TABLEAUX'
	endif
c
c        write(9,*)                                                        !
c     &'4+) continu ordonne puis raies ordonnees et sommees  i,kco,lo,ro'  !
c        do i=1,imax+2                                                     !
c        write(9,'(2i7,i8,1pe12.4)') i,kco(i),lo(i),ro(i)                  !
c        enddo                                                             !
c---------------------
c
c MELANGE des raies et du continu
c                                                                         !g3
        n=imax
        call INDEXTRI(n,lo,iw)
c        write(9,*) '5) entrelacement    i,j,kco,kro,lo,ro'           !
c        do i=1,n                                                     !
c        j=iw(i)                                                      !
c        if(kco(j).gt.0) write(9,'(2i7,3i8,1pe12.4,0pf13.5)')         !
c     &     i,j,kco(j),kro(j),lo(j),ro(j),thnuc(kco(j))               !
c        if(kco(j).eq.0) write(9,'(2i7,3i8,1pe12.4)')                 !
c     &      i,j,kco(j),kro(j),lo(j),ro(j)
c        enddo                                                        !
c
c sommation du continu et des raies
c
c        write(9,*) '6) melange    i,jc,jr,kco,kro,lo,ro,lambda'       !
                       do 300 i=1,n-1
        j=iw(i)
        if(lo(j).eq.0) goto 300
        if(i.gt.1.and.lo(j).eq.lo(iw(i-1))) goto 300         ! deja traite
        j1=iw(i+1)
        if(lo(j).eq.lo(j1)) then            ! forcement 1 raie et 1 continu
                  jc=min(j,j1)
                  jr=max(j,j1)
                  soc(jc)=so(jc)
                  roc(jc)=ro(jc)
                  toc(jc)=to(jc)
                  so(jc)=so(j) + so(j1)
                  ro(jc)=ro(j) + ro(j1)
                  to(jc)=to(j) + to(j1)
                  eo(jc)=eo(j) + eo(j1)                                   !g3
                  kro(jc)=kro(jr)
c ne surtout pas mettre: if(so(jc)<0) so(jc)=0.  if(ro(jc)<0) ro(jc)=0.
c        write(9,'(3i7,3i8,1pe12.4,0pf10.4)') i,jc,jr,kco(jc),kro(jc),
c     &                                       lo(j),ro(jc)
                  else
                  soc(j)=so(j)
                  roc(j)=ro(j)
                  toc(j)=to(j)
c        write(9,'(3i7,3i8,1pe12.4,0pf10.4)') i,j,j,kco(j),kro(j),
c     &                         lo(j),ro(j),pas*float(lo(j))
                  endif
 300                   continue
c---------------------
c
c MOYENNE MOBILE en forme de gaussienne pour obtenir la resolution demandee
c
        Tmoy=tk/8.6171d-5
           if(ila.eq.0) then
        WRITE(14,80) dlambda
        WRITE(14,81) vturbkm
        if(vejectkm.gt.0.d0) WRITE(14,84) vejectkm
        if(ieqw.eq.1) WRITE(14,*) '2) CONTINU ET RAIES : nuF(nu)'
        if(ieqw.eq.0) WRITE(14,*) ' CONTINU ET RAIES : nuF(nu)'
        WRITE(14,92) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep,sep,sep    !TITAN
           else                                                            !g3
        WRITE(14,82) dlambda
        WRITE(14,83) vturbkm
        if(vejectkm.gt.0.d0) WRITE(14,85) vejectkm
        if(ieqw.eq.1)WRITE(14,'(a)')'#2) CONTINUUM AND LINES : nuF(nu)'
        if(ieqw.eq.0)WRITE(14,'(a)') '# CONTINUUM AND LINES : nuF(nu)'
        WRITE(14,93) sep,sep,sep,sep,sep,sep,sep,sep,sep,sep,sep,sep
           endif   ! ila
 80    format(/' largeur de raie minimum (FWHM)=',0pf10.4,
     &         'Angstrom        (graftit105v03)')
c largeur de raie minimum (FWHM)=1234567890Angstrom        (graftit105v03)'
 81     format(' vitesse de TURBULENCE=',0pf10.2,' km/s')
 82     format(/'# minimum line width (FWHM)=',0pf10.4,' Angstrom')
 83     format('# TURBULENCE velocity=',0pf10.2,' km/s')
 84     format(' vitesse d''EJECTION du nuage',0pf10.2,' km/s')
 85     format('# OUTFLOW velocity=',0pf10.2,' km/s')
 92     format('  lambda  ',a,'hnuev ',a,'nuFentrant',a,
     &     'nuFreflechi',a,'nuFsortant',a,'nuFtransapp',a,
     &     'nuFcontrefl',a,'nuFcontsort',a,'kc ',a,'kr ',a,'ion',a,
     &     'lambdaaurepos',a)
 93     format('# lambda  ',a,'hnuev ',a,'nuFincident',a,
     &     'nuFreflected',a,'nuFoutward',a,'nuFtransapp',a,
     &     'nuFcontref',a,'nuFcontout',a,'kc ',a,'kr ',a,'ion',a,
     &     'restlambda',a)
c
        qeject=1.d0
        if(vejectkm.ne.0.d0) qeject=1.d0 - vejectkm/2.997925d5
                             if(vturbkm.eq.0.d0) then
        dlr=dlambda/pas/2.d0 ! 2.5
        j0=nint(dlambda/pas)+1        ! dlr*2+1=6
        mm=j0*2-1               ! dlr*4+1=11
        p(j0)=1.d0
        som=1.d0                                                          !g3
c        jcmax voisin de nint(alambdamax/pas)  defini plus haut
        do j=j0+1,mm
c  p(j)=exp(- (lamb-lamb0)**2/(dlambda**2/(4*Ln2)) ) si dlambda=fwhm
           p(j)=exp(-(float(j-j0)*0.8325546d0/dlr)**2)           ! sqrt(Ln2)
           p(2*j0-j)=p(j)
           som=som+2.d0*p(j)
        enddo
           fm=som
c        type*,(p(j),j=1,mm),som
                        do 400 i=1,jcmax-mm+1
        alambda=pas*float(lo(i+j0-1)+lmin)
        hnuev=12398.54d0/alambda
        if(hnuev.lt.hnumin/0.94d0.or.hnuev.gt.hnumax/1.2d0) goto 400
        ff=0.d0
        ss=0.d0
        rr=0.d0
        ssc=0.d0
        rrc=0.d0
        tt=0.d0
        ee=0.d0
        kc=0
        kr=0
        kk=0
           do 401 j=1,mm
           ff=ff+fo(i+j-1)*p(j)
           ss=ss+so(i+j-1)*p(j)
           rr=rr+ro(i+j-1)*p(j)                                           !g3
           ssc=ssc+soc(i+j-1)*p(j)
           rrc=rrc+roc(i+j-1)*p(j)
           tt=tt+to(i+j-1)*p(j)
           ee=ee+eo(i+j-1)*p(j)
           if(kco(i+j-1).gt.0) then
              kk=kco(i+j-1)
              if(kc.eq.0) kc=kk
              kc=min(kc,kk)
              endif
           if(kro(i+j-1).gt.0.and.kro(i+j-1).le.ntab) then
              kkr=kro(i+j-1)
              if(kr.eq.0) kr=kkr
              kr=min(kr,kkr)
              endif
 401       continue
        ff=ff/fm
        ss=ss/fm
        rr=rr/fm
        ssc=ssc/fm
        rrc=rrc/fm
        tt=tt/fm
        ee=ee/fm
        if(ff.le.1.d-36) ff=1.111d-36
        if(ss.le.1.d-36) ss=1.111d-36                                     !g3
        if(rr.le.1.d-36) rr=1.111d-36
        if(ssc.le.1.d-36) ssc=1.111d-36
        if(rrc.le.1.d-36) rrc=1.111d-36
        if(tt.le.1.d-36) tt=1.111d-36
        if(ee.le.1.d-36) ee=1.111d-36
        ala=alambda*qeject
        WRITE(14,88) ala,sep,hnuev,sep,ff,sep,rr,sep,ss,sep,tt,sep,
     &        rrc,sep,ssc,sep,kc,sep,kr,sep,nomrai(kr),sep,alambda
 400                  continue
c
                             else   ! vturb non nul
        j0=nint(dlambda/pas)+1
        mm=j0*2-1
                        do 500 i=1,jcmax
        if(i.gt.jcmax-mm) goto 500
        alambda=pas*float(lo(i+j0-1)+lmin)
        dla2=(dlambda**2/2.7725887d0 + (vturbkm*alambda/2.997925d5)**2) !4*Ln2 c
     &       /pas**2
           dfwp=sqrt(dla2)*1.66511d0                 ! =fwhm/pas ! 2*sqrt(Ln2)
c        if(lo(i+j0-1).eq.2170)write(9,'(4f11.5)')alambda,sqrt(dla2),dfwp
           j0=nint(dfwp)+1
           mm=j0*2-1
           p(j0)=1.d0
           som=1.d0                                                       !g3
c definition de la gaussienne
c  p(j)=exp(-(lamb-lamb0)**2/d**2 ) avec              (dla2=(d/pas)**2)
c d**2=(dlambda**2/(4*Ln2)) + (vturb*lambda/c)**2 si dlambda=fwhm et
           do j=j0+1,mm
              p(j)=exp(-float(j-j0)**2/dla2)
              p(2*j0-j)=p(j)
              som=som+2.d0*p(j)
           enddo
           fm=som
c        type*,(p(j),j=1,mm),som
        alambda=pas*float(lo(i+j0-1)+lmin)
        hnuev=12398.54d0/alambda
c        if(lo(i+j0-1).eq.2170) then       ! pres de lambda=22.1 kr= 367 OVII
c        write(9,'(6i7,2f8.5)')i,lo(i+j0-1),lmin,j0,mm,jcmax,pas,dlambda
c           write(9,'(4f11.5)')alambda,hnuev,sqrt(dla2),dfwp
c           write(9,'(1p8e9.2)')(p(j),j=1,mm)
c        endif
        if(hnuev.lt.hnumin/0.94d0.or.hnuev.gt.hnumax/1.2d0) goto 500
        ff=0.d0
        ss=0.d0
        rr=0.d0
        ssc=0.d0
        rrc=0.d0
        tt=0.d0
        ee=0.d0
        kc=0
        kr=0
        kk=0
           do 501 j=1,mm
           ff=ff+fo(i+j-1)*p(j)                                            !g3
           ss=ss+so(i+j-1)*p(j)
           rr=rr+ro(i+j-1)*p(j)
           ssc=ssc+soc(i+j-1)*p(j)
           rrc=rrc+roc(i+j-1)*p(j)
           tt=tt+to(i+j-1)*p(j)
           ee=ee+eo(i+j-1)*p(j)
           if(kco(i+j-1).gt.0) then
              kk=kco(i+j-1)
              if(kc.eq.0) kc=kk
              kc=min(kc,kk)
              endif
           if(kro(i+j-1).gt.0.and.kro(i+j-1).le.ntab) then
              kkr=kro(i+j-1)
              if(kr.eq.0) kr=kkr
              kr=min(kr,kkr)
              endif
 501       continue
        ff=ff/fm                                                           !g3
        ss=ss/fm
        rr=rr/fm
        ssc=ssc/fm
        rrc=rrc/fm
        tt=tt/fm
        ee=ee/fm
        if(ff.le.1.d-36) ff=1.111d-36
        if(ss.le.1.d-36) ss=1.111d-36
        if(rr.le.1.d-36) rr=1.111d-36
        if(ssc.le.1.d-36) ssc=1.111d-36
        if(rrc.le.1.d-36) rrc=1.111d-36
        if(tt.le.1.d-36) tt=1.111d-36
        if(ee.le.1.d-36) ee=1.111d-36
        ala=alambda*qeject
        WRITE(14,88) ala,sep,hnuev,sep,ff,sep,rr,sep,ss,sep,tt,sep,
     &             rrc,sep,ssc,sep,kc,sep,kr,sep,nomrai(kr),sep,alambda
 500                        continue
                          endif   ! vturb
 88     format(0pf9.3,a,0pf9.2,6(a,1pe10.3),a,i3,a,i4,a,a7,a,0pf9.3)
c lambda  ;hnuev ;nuFentrant;nuFreflechi;nuFsortant;nuFtransapp;nuFcontrefl;nuFcontsort;kc ;kr ;ion;lambdaaurepos
c124.789; 25000.89; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90; 2.456E+90;123;1234;1234567
c        WRITE(6,*)' nombre total de points du spectre=',jcmax
c
        return
         end                                                             !graf3
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine INDEXTRI(n,larrin,indx)
c
c tri par ordre croissant des energies de photons
c       Subroutine INDEXX de Numerical Recipes p233
c       (arrin et q modifies en larrin et lq)
c
        implicit double precision (a-h,o-z)
        dimension larrin(n),indx(n)
c
        do 11 j=1,n
           indx(j)=j
 11        continue
        l=n/2+1
        ir=n
 10     continue
        if(l.gt.1) then
           l=l-1
           indxt=indx(l)
           lq=larrin(indxt)
        else
           indxt=indx(ir)
           lq=larrin(indxt)
           indx(ir)=indx(1)
           ir=ir-1
           if(ir.eq.1) then
              indx(1)=indxt
              return
           endif
        endif
        i=l
        j=l+l
 20     if(j.le.ir) then
           if(j.lt.ir) then
              if(larrin(indx(j)).lt.larrin(indx(j+1))) j=j+1
           endif
           if(lq.lt.larrin(indx(j))) then
              indx(i)=indx(j)
              i=j
              j=j+j
           else
              j=ir+1
           endif
        goto 20
        endif
        indx(i)=indxt
        goto 10
        end
c============================================================================
        subroutine TABSIGext

c recherche du continu correspondant aux raies
c fabrique avec atomic13.hhe15libeo4fe    le 31 jan 2005

       implicit double precision (a-h,o-z)
       parameter (nrais=4200)
         common/gtab/thnur(nrais)
         common/grc/thnuc(500),alo(nrais)
         common/grkcr/nraimax,ntab,kf,kcr(nrais)
         dimension tde(220),tdeb(220)
       data (tde(k),k=1,200)
     & /0.59,0.86,1.9,1.035,0.59,1.785,2.45,6.625,8.27,                 ! 1-9
     & 0.71,1.425,0.3,1.365,1.04,21.15,28.2,0.465,1.5,2.1,1.235,4.855,  ! 10-21
     & 7.465,28.8,35.,1.65,2.5,2.745,2.795,6.57,7.835,9.895,6.405,62.5,  ! 22-33
     & 28.5,0.5319,0.83,3.265,4.935,6.47,9.75,13.2,9.445,12.865,8.975,  ! 34-44
     & 55.,20.7,0.49,0.655,1.5,2.145,9.05,5.85,11.45,18.8,15.1,15.4,    ! 45-56
     & 16.13,23.136,140.,85.,0.83,1.41,0.825,1.15,3.3,4.15,12.17,1.45,  ! 57-68
     & 12.194,17.,16.93,27.464,29.915,36.23,185.,90.,0.255,0.655,1.205, ! 69-79
     & 0.245,2.445,3.055,3.65,8.335,7.15,9.775,11.04,11.08,8.37,4.6065, ! 80-90
     & 14.5,6.92,40.,24.8,86.,110.,89.9,44.1,31.35,47.5,75.6,100.,0.725,! 91-103
     & 0.755,1.17,1.23,1.3,1.38,1.61,1.745,1.405,2.98,2.03,3.255,4.305, !104-115
     & 4.005,3.955,4.495,5.095,5.555,9.88,9.31,22.85,42.5,25.355,27.5,  !116-126
     & 40.56,21.94,27.5,42.5,77.5,97.91,33.19,49.1,53.5,50.5,54.3,41.55,!127-138
     & 52.1,67.5,143.81,71.19,78.,82.,90.,196.9,78.1,91.,79.,120.,330., !139-152
     & 550.,600.,600.,600.,600.,376.,275.,131.,136.,156.,146.,130.,185.,!153-165
     & 103.,150.45,70.95,75.,90.,114.,76.,90.,137.18,406.96,1002.4,     !165-175
     & 1102.7,1212.9,1334.2,1467.6,1614.4,1775.8,1953.4,2148.8,2363.6,  !176-184
     & 1.314,0.98485,0.8207,0.6839,0.5699,0.475,0.3958,0.3298,0.27485,  !185-193
     & 0.229,0.19087,0.1591,0.13255,0.11046,0.09205,0.0767/             !194-200
       data (tde(k),k=201,220) /0.0639,0.0533,0.0444,0.03699,0.03083,   !201-205
     & 0.02569,0.02141,0.01784,0.01487,0.0124,0.01032,8.6d-3,7.17d-3,   !206-213
     & 5.974d-3,4.98d-3,4.15d-3,3.457d-3,2.88d-3,2.4d-3,2.0d-3/        !214-220

       data tdeb/5.e-3,0.76,0.2,0.585,0.1,1.555,1.955,0.0565,7.98,0.25, ! 1-10
     & 0.53,0.23,1.325,0.605,6.964,20.285,0.46,1.445,2.055,0.04,2.565,  ! 11-21
     & 1.555,12.855,9.44,0.92,0.965,0.495,0.435,5.91,4.415,8.85,3.605,  ! 22-32
     & 14.59,0.8,0.115,0.58,1.94,2.29,4.915,4.505,4.35,7.515,0.45,5.625,! 33-44
     & 18.6,18.65,0.35,0.58,0.77,1.08,4.635,1.045,7.845,12.415,5.02,    ! 45-55
     & 10.85,6.63,14.186,36.19,42.,0.45,0.49,0.055,0.07,1.25,2.135,     ! 56-66
     & 4.655,1.,6.5685,5.,8.95,20.5,7.915,15.945,38.1,39.,0.14,0.075,   ! 67-78
     & 0.675,0.045,1.195,2.45,0.66,3.42,2.8,1.93,6.385,10.08,3.35,4.55, ! 79-90
     & 9.5,0.29,21.4,24.,56.5,53.5,36.4,25.5,2.05,24.6,35.95,36.,0.375, ! 91-103
     & 0.38,0.585,0.65,0.65,0.73,0.85,0.895,0.73,1.535,1.065,1.7,2.25,  !104-115
     & 2.05,2.015,2.36,2.645,2.99,5.245,4.805,12.,22.,12.5,15.,25.56,   !116-127
     & 12.5,15.,27.5,50.,47.91,18.6,27.7,29.5,25.,29.3,22.9,27.5,40.,   !128-140
     & 103.81,35.,43.,40.,50.,146.9,40.,51.,40.,80.,250.,300.,300.,300.,!141-154
     & 300.,300.,76.,127.,65.5,68.,78.,73.,65.,92.5,51.5,80.45,35.,40., !155-168
     & 50.,64.,40.,50.,87.18,194.14,501.21,551.33,606.46,667.11,733.82, !169-179
     & 807.2,887.92,976.71,1074.4,1181.8,0.7231,0.49242,0.41035,0.34196,!180-188
     & 0.285,0.2375,0.1979,0.16491,0.1374,0.1145,0.0954,0.07953,0.06627,!189-197
     & 0.0552,0.04602,0.03835,0.03196,0.02663,0.022195,0.01849,0.01541, !198-205
     & 0.01284,0.0107,8.92d-3,7.43d-3,6.19d-3,5.16d-3,4.3d-3,3.585d-3,  !206-213
     & 2.987d-3,2.49d-3,2.074d-3,1.729d-3,1.44d-3,1.2d-3,1.0d-3/        !214-220

c tabsig
c        write(9,*) 'TABSIG  kr  hnurai  kc  hnuc'
        do 11 kr=1,nrais
c          alambda=alo(kr)
c                                if(alambda.le.1.01d0) then
c                                thnur(kr)=0.d0
c                                kcr(kr)=220
c                                goto 11
c                                endif
c          hnuev=hcse/alambda
c                thnur(kr) = hnuev
        hnuev=thnur(kr)
                                if(hnuev.le.0.d0) then
                                kcr(kr)=220
                                goto 11
                                endif
                                if(hnuev.le.0.01078d0) then
                                kcr(kr)=220
c        write(9,71) kr,hnuev,kcr(kr),thnuc(220)
 71     format(i4,f12.4,i6,f12.4)
                                goto 11
                                endif
c       recherche du kc du continu correspondant a chaque raie
                        do kc=1,322
                        if(kc.le.102) then
                        hnuc=thnuc(kc)
                        d1=0.d0
                        d2=tdeb(kc)
                        else if(kc.le.220) then
                        hnuc=thnuc(kc)
                        d1=tde(kc)-tdeb(kc)
                        d2=tdeb(kc)
                        else
                        hnuc=thnuc(kc-220)
                        d1=tde(kc-220)-tdeb(kc-220)
                        d2=0.d0
                        endif
        if(hnuev.ge.(hnuc-d1)*0.9999d0.and.hnuev.lt.(hnuc+d2)) then
        kcr(kr)=kc
                        goto 13
                        endif
                        enddo
                        write(6,*) 'pas de kc pour kr & hnuev=',kr,hnuev
                        kcr(kr)=220
 13                     continue
c        write(9,71) kr,hnuev,kcr(kr),hnuc
 11             continue                                        ! kr
c verification - a supprimer
c                        do kc=1,322
c                        if(kc.le.102) then
c                        hnuc=thnuc(kc)
c                        d1=0.d0
c                        d2=tdeb(kc)
c                        else if(kc.le.220) then
c                        hnuc=thnuc(kc)
c                        d1=tde(kc)-tdeb(kc)
c                        d2=tdeb(kc)
c                        else
c                        hnuc=thnuc(kc-220)
c                        d1=tde(kc-220)-tdeb(kc-220)
c                        d2=0.d0
c                        endif
c        if(kc.le.220) write(15,'(f13.5,i4,4f13.5)') hnuc,kc,tde(kc),
c     &                               tdeb(kc),hnuc-d1,hnuc+d2
c        if(kc.gt.220) write(15,'(f13.5,i4,4f13.5)') hnuc,kc,tde(kc-220),
c     &                               tdeb(kc-220),hnuc-d1,hnuc+d2
c                        enddo
c
        return
        end                                                             !tabsig
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        block data ionraies

        implicit double precision (a-h,o-z)
        parameter (nrais=4200)
        character nomrai*7
        common/nom/nomrai(0:nrais)

        data (nomrai(k),k=0,931) /'       ',
c @lines1
     &  'HI     ','HI     ','HI     ','HI     ','HI     ','HI     ',
     &  'HI     ','HI     ','HI     ','HI     ','HI     ','HI     ',
     &  'HI     ','HI     ','HI     ','HeI    ','HeI    ','HeI    ',
     &  'HeI    ','HeI    ','HeI    ','HeI    ','HeI    ','HeI    ',
     &  'HeI    ','HeI    ','HeI    ','HeI    ','HeI    ','HeI    ',
     &  'HeI    ','HeI    ','HeI    ','HeI    ','HeI    ','HeI    ',
     &  'HeI    ','HeI    ','HeI    ','HeI    ','HeI    ','HeI    ',
     &  'HeI    ','HeI    ','HeI    ','HeII   ','HeII   ','HeII   ',
     &  'HeII   ','HeII   ','HeII   ','HeII   ','HeII   ','HeII   ',
     &  'HeII   ','CI     ','CI     ','CI     ','CI     ','CI     ',
     &  'CI     ','CI     ','CII    ','CII    ','CII    ','CII    ',
     &  'CII    ','CIII   ','CIII   ','CIII   ','CIII   ','CIII   ',
     &  'CIII   ','CIII   ','CIII   ','CIII   ','CIII   ','CIII   ',
     &  'CIII   ','CIII   ','CIII   ','CIII   ','CIII   ','CIV    ',
     &  'CIV    ','CIV    ','CIV    ','CIV    ','CIV    ','CIV    ',
     &  'CIV    ','CIV    ','CIV    ','CV     ','CV     ','CV     ',
     &  'CV     ','CV     ','CV     ','CV     ','CV     ','CV     ',
     &  'CV     ','CV     ','CV     ','CV     ','CV     ','CV     ',
     &  'CV     ','CV     ','CV     ','CV     ','CV     ','CV     ',
     &  'CV     ','CV     ','CV     ','CV     ','CV     ','CV     ',
     &  'CV     ','CV     ','CV     ','CV     ','CV     ','CV     ',
     &  'CV     ','CV     ','CV     ','CV     ','CV     ','CV     ',
     &  'CV     ','CV     ','CV     ','CV     ','CV     ','CV     ',
     &  'CV     ','CV     ','CV     ','CV     ','CVI    ','CVI    ',
     &  'CVI    ','CVI    ','CVI    ','CVI    ','CVI    ','CVI    ',
     &  'CVI    ','CVI    ','NI     ','NI     ','NI     ','NII    ',
     &  'NII    ','NII    ','NII    ','NII    ','NII    ','NIII   ',
     &  'NIII   ','NIII   ','NIII   ','NIII   ','NIV    ','NIV    ',
     &  'NIV    ','NIV    ','NIV    ','NIV    ','NIV    ','NIV    ',
     &  'NV     ','NV     ','NV     ','NV     ','NV     ','NV     ',
     &  'NV     ','NV     ','NV     ','NV     ','NVI    ','NVI    ',
     &  'NVI    ','NVI    ','NVI    ','NVI    ','NVI    ','NVI    ',
     &  'NVI    ','NVI    ','NVI    ','NVI    ','NVI    ','NVI    ',
     &  'NVI    ','NVI    ','NVI    ','NVI    ','NVI    ','NVI    ',
     &  'NVI    ','NVI    ','NVI    ','NVI    ','NVI    ','NVI    ',
     &  'NVI    ','NVI    ','NVI    ','NVI    ','NVI    ','NVI    ',
     &  'NVI    ','NVI    ','NVI    ','NVI    ','NVI    ','NVI    ',
     &  'NVI    ','NVI    ','NVI    ','NVI    ','NVI    ','NVI    ',
     &  'NVI    ','NVI    ','NVI    ','NVI    ','NVI    ','NVII   ',
     &  'NVII   ','NVII   ','NVII   ','NVII   ','NVII   ','NVII   ',
     &  'NVII   ','NVII   ','NVII   ','OI     ','OI     ','OI     ',
     &  'OI     ','OI     ','OII    ','OII    ','OII    ','OIII   ',
     &  'OIII   ','OIII   ','OIII   ','OIII   ','OIII   ','OIV    ',
     &  'OIV    ','OIV    ','OIV    ','OIV    ','OIV    ','OIV    ',
     &  'OIV    ','OIV    ','OIV    ','OIV    ','OIV    ','OIV    ',
     &  'OIV    ','OIV    ','OIV    ','OIV    ','OIV    ','OIV    ',
     &  'OIV    ','OIV    ','OIV    ','OV     ','OV     ','OV     ',
     &  'OV     ','OV     ','OV     ','OV     ','OV     ','OV     ',
     &  'OV     ','OV     ','OV     ','OV     ','OV     ','OV     ',
     &  'OV     ','OV     ','OV     ','OV     ','OV     ','OV     ',
     &  'OV     ','OV     ','OV     ','OV     ','OV     ','OV     ',
     &  'OV     ','OV     ','OV     ','OV     ','OV     ','OV     ',
     &  'OV     ','OV     ','OV     ','OV     ','OV     ','OV     ',
     &  'OVI    ','OVI    ','OVI    ','OVI    ','OVI    ','OVI    ',
     &  'OVI    ','OVI    ','OVI    ','OVI    ','OVII   ','OVII   ',
     &  'OVII   ','OVII   ','OVII   ','OVII   ','OVII   ','OVII   ',
     &  'OVII   ','OVII   ','OVII   ','OVII   ','OVII   ','OVII   ',
     &  'OVII   ','OVII   ','OVII   ','OVII   ','OVII   ','OVII   ',
     &  'OVII   ','OVII   ','OVII   ','OVII   ','OVII   ','OVII   ',
     &  'OVII   ','OVII   ','OVII   ','OVII   ','OVII   ','OVII   ',
     &  'OVII   ','OVII   ','OVII   ','OVII   ','OVII   ','OVII   ',
     &  'OVII   ','OVII   ','OVII   ','OVII   ','OVII   ','OVII   ',
     &  'OVII   ','OVII   ','OVII   ','OVII   ','OVII   ','OVII   ',
     &  'OVIII  ','OVIII  ','OVIII  ','OVIII  ','OVIII  ','OVIII  ',
     &  'OVIII  ','OVIII  ','OVIII  ','OVIII  ','NeI    ','NeI    ',
     &  'NeII   ','NeII   ','NeIII  ','NeIII  ','NeIII  ','NeIII  ',
     &  'NeIII  ','NeIII  ','NeIV   ','NeIV   ','NeIV   ','NeIV   ',
     &  'NeV    ','NeV    ','NeV    ','NeV    ','NeV    ','NeV    ',
     &  'NeVI   ','NeVI   ','NeVI   ','NeVI   ','NeVI   ','NeVII  ',
     &  'NeVII  ','NeVIII ','NeVIII ','NeVIII ','NeVIII ','NeVIII ',
     &  'NeVIII ','NeVIII ','NeVIII ','NeVIII ','NeVIII ','NeIX   ',
     &  'NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ',
     &  'NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ',
     &  'NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ',
     &  'NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ',
     &  'NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ',
     &  'NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ','NeIX   ',
     &  'NeX    ','NeX    ','NeX    ','NeX    ','NeX    ','NeX    ',
     &  'NeX    ','NeX    ','NeX    ','NeX    ','MgI    ','MgI    ',
     &  'MgII   ','MgIII  ','MgIII  ','MgIII  ','MgIII  ','MgIV   ',
     &  'MgIV   ','MgV    ','MgV    ','MgVI   ','MgVI   ','MgVII  ',
     &  'MgVII  ','MgVII  ','MgVII  ','MgVIII ','MgVIII ','MgVIII ',
     &  'MgVIII ','MgVIII ','MgIX   ','MgIX   ','MgX    ','MgX    ',
     &  'MgX    ','MgX    ','MgX    ','MgX    ','MgX    ','MgX    ',
     &  'MgX    ','MgX    ','MgXI   ','MgXI   ','MgXI   ','MgXI   ',
     &  'MgXI   ','MgXI   ','MgXI   ','MgXI   ','MgXI   ','MgXI   ',
     &  'MgXI   ','MgXI   ','MgXI   ','MgXI   ','MgXI   ','MgXI   ',
     &  'MgXI   ','MgXI   ','MgXI   ','MgXI   ','MgXI   ','MgXI   ',
     &  'MgXI   ','MgXI   ','MgXI   ','MgXI   ','MgXI   ','MgXI   ',
     &  'MgXI   ','MgXI   ','MgXI   ','MgXI   ','MgXI   ','MgXI   ',
     &  'MgXI   ','MgXI   ','MgXI   ','MgXII  ','MgXII  ','MgXII  ',
     &  'MgXII  ','MgXII  ','MgXII  ','MgXII  ','MgXII  ','MgXII  ',
     &  'MgXII  ','SiI    ','SiI    ','SiI    ','SiII   ','SiII   ',
     &  'SiII   ','SiII   ','SiII   ','SiII   ','SiII   ','SiIII  ',
     &  'SiIII  ','SiIV   ','SiIV   ','SiV    ','SiV    ','SiV    ',
     &  'SiV    ','SiVI   ','SiVI   ','SiVII  ','SiVII  ','SiVIII ',
     &  'SiVIII ','SiIX   ','SiIX   ','SiIX   ','SiIX   ','SiX    ',
     &  'SiX    ','SiX    ','SiX    ','SiXI   ','SiXI   ','SiXII  ',
     &  'SiXII  ','SiXII  ','SiXII  ','SiXII  ','SiXII  ','SiXII  ',
     &  'SiXII  ','SiXII  ','SiXII  ','SiXIII ','SiXIII ','SiXIII ',
     &  'SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIII ',
     &  'SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIII ',
     &  'SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIII ',
     &  'SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIII ',
     &  'SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIII ',
     &  'SiXIII ','SiXIII ','SiXIII ','SiXIII ','SiXIV  ','SiXIV  ',
     &  'SiXIV  ','SiXIV  ','SiXIV  ','SiXIV  ','SiXIV  ','SiXIV  ',
     &  'SiXIV  ','SiXIV  ','SI     ','SI     ','SI     ','SI     ',
     &  'SI     ','SI     ','SI     ','SII    ','SIII   ','SIII   ',
     &  'SIII   ','SIV    ','SIV    ','SIV    ','SIV    ','SIV    ',
     &  'SV     ','SVI    ','SVI    ','SVI    ','SVII   ','SVII   ',
     &  'SVII   ','SVII   ','SVIII  ','SVIII  ','SIX    ','SIX    ',
     &  'SX     ','SX     ','SXI    ','SXI    ','SXI    ','SXI    ',
     &  'SXII   ','SXII   ','SXII   ','SXII   ','SXIII  ','SXIII  ',
     &  'SXIV   ','SXIV   ','SXIV   ','SXIV   ','SXIV   ','SXIV   ',
     &  'SXIV   ','SXIV   ','SXIV   ','SXIV   ','SXV    ','SXV    ',
     &  'SXV    ','SXV    ','SXV    ','SXV    ','SXV    ','SXV    ',
     &  'SXV    ','SXV    ','SXV    ','SXV    ','SXV    ','SXV    ',
     &  'SXV    ','SXV    ','SXV    ','SXV    ','SXV    ','SXV    ',
     &  'SXV    ','SXV    ','SXV    ','SXV    ','SXV    ','SXV    ',
     &  'SXV    ','SXV    ','SXV    ','SXV    ','SXV    ','SXV    ',
     &  'SXV    ','SXV    ','SXV    ','SXV    ','SXV    ','SXV    ',
     &  'SXVI   ','SXVI   ','SXVI   ','SXVI   ','SXVI   ','SXVI   ',
     &  'SXVI   ','SXVI   ','SXVI   ','SXVI   ','FeI    ','FeI    ',
     &  'FeI    ','FeI    ','FeII   ','FeII   ','FeII   ','FeIII  ',
     &  'FeIV   ','FeV    ','FeVI   ','FeVII  ','FeVII  ','FeVII  ',
     &  'FeVII  ','FeVIII ','FeVIII ','FeVIII ','FeVIII ','FeIX   ',
     &  'FeIX   ','FeIX   ','FeX    ','FeX    ','FeX    ','FeX    ',
     &  'FeX    ','FeXI   ','FeXI   ','FeXI   ','FeXI   ','FeXII  ',
     &  'FeXII  ','FeXII  ','FeXII  ','FeXIII ','FeXIII ','FeXIII ',
     &  'FeXIII ','FeXIV  ','FeXIV  ','FeXIV  ','FeXIV  ','FeXV   ',
     &  'FeXV   ','FeXV   ','FeXVI  ','FeXVI  ','FeXVI  ','FeXVII ',
     &  'FeXVII ','FeXVII ','FeXVII ','FeXVII ','FeXVII ','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXIX  ','FeXIX  ','FeXIX  ',
     &  'FeXIX  ','FeXX   ','FeXX   ','FeXX   ','FeXX   ','FeXXI  ',
     &  'FeXXI  ','FeXXI  ','FeXXI  ','FeXXI  ','FeXXI  ','FeXXI  ',
     &  'FeXXI  ','FeXXI  ','FeXXI  ','FeXXII ','FeXXII ','FeXXII ',
     &  'FeXXII ','FeXXII ','FeXXII ','FeXXII ','FeXXII ','FeXXIII',
     &  'FeXXIII','FeXXIII','FeXXIII','FeXXIII','FeXXIII','FeXXIII',
     &  'FeXXIII','FeXXIII','FeXXIV ','FeXXIV ','FeXXIV ','FeXXIV ',
     &  'FeXXIV ','FeXXIV ','FeXXIV ','FeXXIV ','FeXXIV ','FeXXIV ',
     &  'FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ',
     &  'FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ',
     &  'FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ',
     &  'FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ',
     &  'FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ',
     &  'FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ',
     &  'FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ',
     &  'FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ','FeXXV  ',
     &  'FeXXV  ','FeXXV  ','FeXXV  ','FeXXVI ','FeXXVI ','FeXXVI ',
     &  'FeXXVI ','FeXXVI ','FeXXVI ','FeXXVI ','FeXXVI ','FeXXVI ',
     &  'FeXXVI ',                                                         907
     &  'FeIk   ','FeIIk  ','FeIIIk ','FeIVk  ','FeVk   ','FeVIk  ',      6
     &  'FeVIIk ','FeVIIIk','FeIXk  ','FeXk   ','FeXIk  ','FeXIIk ',     12
     &  'FeXIIIk','FeXIVk ','FeXVk  ','FeXVIk ','FeXVIIk','FeXVIII',     18
     &  'FeXIXk ','FeXXk  ','FeXXIk ','FeXXIIk','FeXXIII','FeXXIVk'/     24 931
c uta-lines
         data (nomrai(k),k=932,1023)
     & /'NeIIu  ','NeIIIu ','NeIVu  ','NeVu   ','NeVu   ','NeVIu  ',      6
     &  'NeVIu  ','NeVIIu ','NeVIIu ','NeVIIIu','NeVIIIu','MgIVu  ',     12
     &  'MgVu   ','MgVIu  ','MgVIIu ','MgVIIu ','MgVIIIu','MgVIIIu',     18
     &  'MgIXu  ','MgIXu  ','MgXu   ','MgXu   ','SiVIu  ','SiVIIu ',     24
     &  'SiVIIIu','SiIXu  ','SiIXu  ','SiXu   ','SiXu   ','SiXIu  ',     30
     &  'SiXIu  ','SiXIIu ','SiXIIu ','SVIIIu ','SIXu   ','SXu    ',     36
     &  'SXIu   ','SXIu   ','SXIIu  ','SXIIu  ','SXIIIu ','SXIIIu ',     42
     &  'SXIVu  ','SXIVu  ','FeIu   ','FeIu   ','FeIIu  ','FeIIu  ',     48
     &  'FeIIIu ','FeIIIu ','FeIVu  ','FeIVu  ','FeVu   ','FeVu   ',     54
     &  'FeVIu  ','FeVIu  ','FeVIIu ','FeVIIu ','FeVIIIu','FeVIIIu',     60
     &  'FeIXu  ','FeIXu  ','FeXu   ','FeXu   ','FeXIu  ','FeXIu  ',     66
     &  'FeXIu  ','FeXIIu ','FeXIIu ','FeXIIu ','FeXIIIu','FeXIIIu',     72
     &  'FeXIIIu','FeXIVu ','FeXIVu ','FeXIVu ','FeXVu  ','FeXVu  ',     78
     &  'FeXVu  ','FeXVIu ','FeXVIu ','FeXVIu ','FeXVIII','FeXIXu ',     84
     &  'FeXXu  ','FeXXu  ','FeXXIu ','FeXXIIu','FeXXIII','FeXXIII',     90
     &  'FeXXIVu','FeXXIVu'/                                            92 1023
c lines set 2
         data (nomrai(k),k=1024,2709)
     & /'CIs    ','CIs    ','CIs    ','CIs    ','CIs    ','CIs    ',
     &  'CIs    ','CIs    ','CIs    ','CIs    ','CIs    ','CIs    ',
     &  'CIs    ','CIs    ','CIs    ','CIs    ','CIs    ','CIs    ',
     &  'CIs    ','CIs    ','CIs    ','CIs    ','CIs    ','CIs    ',
     &  'CIs    ','CIs    ','CIs    ','CIs    ','CIs    ','CIs    ',
     &  'CIs    ','CIs    ','CIs    ','CIIs   ','CIIs   ','CIIs   ',     33 
     &  'CIIs   ','CIIs   ','CIIs   ','CIIs   ','CIIs   ','CIIs   ',
     &  'CIIs   ','CIIs   ','CIIs   ','CIIs   ','CIIs   ','CIIs   ',
     &  'CIIs   ','CIIs   ','CIIs   ','CIIs   ','CIIs   ','CIIs   ',
     &  'CIIs   ','CIIs   ','CIIs   ','CIIIs  ','CIIIs  ','CIIIs  ',     24
     &  'CIIIs  ','CIIIs  ','CIIIs  ','CIIIs  ','CIIIs  ','CIIIs  ',     9 1089
     &  'CIVs   ','CIVs   ','CIVs   ','CIVs   ','CIVs   ','CVs    ',      5
     &  'CVs    ','CVs    ','CVs    ','CVs    ','CVs    ','CVIs   ',      6
     &  'CVIs   ','CVIs   ','CVIs   ','CVIs   ','NIs    ','NIs    ',      5
     &  'NIs    ','NIs    ','NIs    ','NIs    ','NIs    ','NIs    ',
     &  'NIs    ','NIs    ','NIs    ','NIs    ','NIs    ','NIs    ',
     &  'NIs    ','NIs    ','NIs    ','NIIs   ','NIIs   ','NIIs   ',     17
     &  'NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ',
     &  'NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ',
     &  'NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ',
     &  'NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ',
     &  'NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ',
     &  'NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ',
     &  'NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ',
     &  'NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ',
     &  'NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIIs   ','NIVs   ',     56
     &  'NIVs   ','NIVs   ','NIVs   ','NIVs   ','NIVs   ','NIVs   ',
     &  'NIVs   ','NIVs   ','NIVs   ','NIVs   ','NVs    ','NVs    ',     11
     &  'NVs    ','NVs    ','NVs    ','NVIs   ','NVIs   ','NVIs   ',      5
     &  'NVIs   ','NVIs   ','NVIs   ','NVIIs  ','NVIIs  ','NVIIs  ',      6
     &  'NVIIs  ','NVIIs  ','OIs    ','OIs    ','OIs    ','OIs    ',      5
     &  'OIs    ','OIs    ','OIs    ','OIs    ','OIs    ','OIs    ',
     &  'OIs    ','OIs    ','OIs    ','OIs    ','OIs    ','OIs    ',
     &  'OIs    ','OIs    ','OIs    ','OIs    ','OIs    ','OIs    ',
     &  'OIs    ','OIs    ','OIs    ','OIIs   ','OIIs   ','OIIs   ',     25
     &  'OIIs   ','OIIs   ','OIIs   ','OIIs   ','OIIs   ','OIIs   ',
     &  'OIIs   ','OIIs   ','OIIs   ','OIIs   ','OIIs   ','OIIs   ',    15 1245
     &  'OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ',
     &  'OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ',
     &  'OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ',
     &  'OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ',
     &  'OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ',
     &  'OIIIs  ','OIIIs  ','OIIIs  ','OIIIs  ','OIVs   ','OIVs   ',     34
     &  'OIVs   ','OIVs   ','OIVs   ','OIVs   ','OIVs   ','OIVs   ',
     &  'OIVs   ','OIVs   ','OIVs   ','OIVs   ','OIVs   ','OIVs   ',
     &  'OIVs   ','OIVs   ','OIVs   ','OIVs   ','OIVs   ','OIVs   ',
     &  'OIVs   ','OIVs   ','OIVs   ','OIVs   ','OIVs   ','OIVs   ',
     &  'OIVs   ','OVs    ','OVs    ','OVs    ','OVs    ','OVs    ',     27
     &  'OVs    ','OVs    ','OVs    ','OVs    ','OVs    ','OVs    ',    11 1317
     &  'OVIs   ','OVIs   ','OVIs   ','OVIs   ','OVIs   ','OVIIs  ',      5
     &  'OVIIs  ','OVIIs  ','OVIIs  ','OVIIs  ','OVIIs  ','OVIIIs ',      6
     &  'OVIIIs ','OVIIIs ','OVIIIs ','OVIIIs ','NeIs   ','NeIs   ',      5
     &  'NeIs   ','NeIs   ','NeIs   ','NeIs   ','NeIs   ','NeIs   ',
     &  'NeIs   ','NeIs   ','NeIs   ','NeIs   ','NeIs   ','NeIs   ',
     &  'NeIs   ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ',     15
     &  'NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ',
     &  'NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ',
     &  'NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ',
     &  'NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ',
     &  'NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ','NeIIs  ',
     &  'NeIIs  ','NeIIs  ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ',     37
     &  'NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ',
     &  'NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ',
     &  'NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ',
     &  'NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ',
     &  'NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ','NeIIIs ',    34 1419
     &  'NeIVs  ','NeIVs  ','NeIVs  ','NeIVs  ','NeIVs  ','NeIVs  ',
     &  'NeIVs  ','NeIVs  ','NeIVs  ','NeIVs  ','NeIVs  ','NeIVs  ',
     &  'NeIVs  ','NeIVs  ','NeIVs  ','NeIVs  ','NeIVs  ','NeVs   ',     17
     &  'NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ',
     &  'NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ',
     &  'NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ',
     &  'NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ',
     &  'NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ',
     &  'NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ',
     &  'NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ','NeVs   ',
     &  'NeVs   ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ',      44
     &  'NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ',
     &  'NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ',
     &  'NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ',
     &  'NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ',
     &  'NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ',
     &  'NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ',
     &  'NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ','NeVIs  ',
     &  'NeVIs  ','NeVIs  ','NeVIIs ','NeVIIs ','NeVIIs ','NeVIIs ',      49
     &  'NeVIIs ','NeVIIs ','NeVIIs ','NeVIIs ','NeVIIs ','NeVIIs ',
     &  'NeVIIs ','NeVIIs ','NeVIIs ','NeVIIIs','NeVIIIs','NeVIIIs',      13
     &  'NeVIIIs','NeVIIIs','NeIXs  ','NeIXs  ','NeIXs  ','NeIXs  ',    VIII 5
     &  'NeIXs  ','NeIXs  ','NeXs   ','NeXs   ','NeXs   ','NeXs   ',      IX 6
     &  'NeXs   ','MgIs   ','MgIs   ','MgIs   ','MgIs   ','MgIs   ',       X 5
     &  'MgIs   ','MgIs   ','MgIIs  ','MgIIs  ','MgIIs  ','MgIIs  ',       I 7
     &  'MgIIs  ','MgIIs  ','MgIIs  ','MgIIIs ','MgIIIs ','MgIIIs ',      II 7
     &  'MgIIIs ','MgIIIs ','MgIIIs ','MgIIIs ','MgIIIs ','MgIIIs ',
     &  'MgIIIs ','MgIIIs ','MgIIIs ','MgIVs  ','MgIVs  ','MgIVs  ',    III 12
     &  'MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ',
     &  'MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ',
     &  'MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ',
     &  'MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ',
     &  'MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ',
     &  'MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ',
     &  'MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ','MgIVs  ',
     &  'MgIVs  ','MgIVs  ','MgIVs  ','MgVs   ','MgVs   ','MgVs   ',      48
     &  'MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ',
     &  'MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ',
     &  'MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ',
     &  'MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ',
     &  'MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ',
     &  'MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ',
     &  'MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ',
     &  'MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ','MgVs   ',
     &  'MgVs   ','MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ',      52
     &  'MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ',
     &  'MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ',
     &  'MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ','MgVIs  ',    23 1707
     &  'MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ',
     &  'MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ',
     &  'MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ',
     &  'MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ',
     &  'MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ',
     &  'MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ',
     &  'MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ',
     &  'MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ',
     &  'MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ',
     &  'MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ','MgVIIs ',
     &  'MgVIIs ','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs',      61
     &  'MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs',
     &  'MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs',
     &  'MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs',
     &  'MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs',
     &  'MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs',
     &  'MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs',
     &  'MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs',
     &  'MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs','MgVIIIs',
     &  'MgVIIIs','MgIXs  ','MgIXs  ','MgIXs  ','MgIXs  ','MgIXs  ',      54
     &  'MgIXs  ','MgIXs  ','MgIXs  ','MgIXs  ','MgIXs  ','MgIXs  ',
     &  'MgIXs  ','MgIXs  ','MgIXs  ','MgXs   ','MgXs   ','MgXs   ',    IX 14
     &  'MgXs   ','MgXs   ','MgXIs  ','MgXIs  ','MgXIs  ','MgXIs  ',     X  5
     &  'MgXIs  ','MgXIs  ','MgXIIs ','MgXIIs ','MgXIIs ','MgXIIs ',    XI  6
     &  'MgXIIs ','SiIs   ','SiIs   ','SiIs   ','SiIs   ','SiIs   ',    XII 5
     &  'SiIs   ','SiIs   ','SiIs   ','SiIs   ','SiIs   ','SiIs   ',
     &  'SiIs   ','SiIs   ','SiIs   ','SiIs   ','SiIs   ','SiIs   ',
     &  'SiIs   ','SiIs   ','SiIs   ','SiIs   ','SiIs   ','SiIs   ',
     &  'SiIs   ','SiIs   ','SiIs   ','SiIs   ','SiIs   ','SiIs   ',
     &  'SiIs   ','SiIs   ','SiIIs  ','SiIIs  ','SiIIs  ','SiIIs  ',      31
     &  'SiIIs  ','SiIIs  ','SiIIs  ','SiIIs  ','SiIIs  ','SiIIs  ',
     &  'SiIIs  ','SiIIs  ','SiIIs  ','SiIIs  ','SiIIs  ','SiIIs  ',
     &  'SiIIs  ','SiIIs  ','SiIIs  ','SiIIs  ','SiIIs  ','SiIIIs ',        21
     &  'SiIIIs ','SiIIIs ','SiIIIs ','SiIIIs ','SiIIIs ','SiIIIs ',
     &  'SiIIIs ','SiIVs  ','SiIVs  ','SiIVs  ','SiIVs  ','SiIVs  ',    III 8
     &  'SiIVs  ','SiIVs  ','SiVs   ','SiVs   ','SiVs   ','SiVs   ',    IV  7
     &  'SiVs   ','SiVs   ','SiVs   ','SiVs   ','SiVs   ','SiVs   ',
     &  'SiVs   ','SiVs   ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ',      12
     &  'SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ',
     &  'SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ',
     &  'SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ',
     &  'SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ',
     &  'SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ',
     &  'SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ',
     &  'SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ',
     &  'SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ',
     &  'SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ','SiVIs  ',    58 1989
     &  'SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ',
     &  'SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ',
     &  'SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ',
     &  'SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ',
     &  'SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ',
     &  'SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ',
     &  'SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ',
     &  'SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ',
     &  'SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ',
     &  'SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ','SiVIIs ',
     &  'SiVIIs ','SiVIIs ','SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs',       62
     &  'SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs',
     &  'SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs',
     &  'SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs',
     &  'SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs','SiVIIIs','SiIXs  ',       27
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ',
     &  'SiIXs  ','SiIXs  ','SiIXs  ','SiIXs  ','SiXs   ','SiXs   ',       71
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ',
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ',
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ',
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ',
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ',
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ',
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ',
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ',
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ',
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXs   ',
     &  'SiXs   ','SiXs   ','SiXs   ','SiXs   ','SiXIs  ','SiXIs  ',       66
     &  'SiXIs  ','SiXIs  ','SiXIs  ','SiXIs  ','SiXIs  ','SiXIs  ',
     &  'SiXIs  ','SiXIs  ','SiXIs  ','SiXIs  ','SiXIs  ','SiXIs  ',     XI  15
     &  'SiXIs  ','SiXIIs ','SiXIIs ','SiXIIs ','SiXIIs ','SiXIIs ', ! XII 2235
     &  'SiXIIIs','SiXIIIs','SiXIIIs','SiXIIIs','SiXIIIs','SiXIIIs',    XIII 6
     &  'SiXIVs ','SiXIVs ','SiXIVs ','SiXIVs ','SiXIVs ','SIs    ',    XIV  5
     &  'SIs    ','SIs    ','SIs    ','SIs    ','SIs    ','SIs    ',
     &  'SIs    ','SIs    ','SIs    ','SIs    ','SIs    ','SIs    ',
     &  'SIs    ','SIs    ','SIs    ','SIs    ','SIs    ','SIs    ',
     &  'SIs    ','SIs    ','SIIs   ','SIIs   ','SIIs   ','SIIs   ',      21
     &  'SIIs   ','SIIs   ','SIIs   ','SIIs   ','SIIs   ','SIIs   ',
     &  'SIIs   ','SIIs   ','SIIs   ','SIIs   ','SIIs   ','SIIs   ',    16 2283
     &  'SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ',
     &  'SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ',
     &  'SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ',
     &  'SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ',
     &  'SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ',
     &  'SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ','SIIIs  ',
     &  'SIIIs  ','SIVs   ','SIVs   ','SIVs   ','SIVs   ','SIVs   ',       37
     &  'SIVs   ','SIVs   ','SIVs   ','SIVs   ','SIVs   ','SIVs   ',
     &  'SIVs   ','SIVs   ','SIVs   ','SIVs   ','SIVs   ','SIVs   ',
     &  'SIVs   ','SIVs   ','SIVs   ','SIVs   ','SIVs   ','SIVs   ',
     &  'SIVs   ','SIVs   ','SIVs   ','SIVs   ','SIVs   ','SIVs   ',
     &  'SIVs   ','SIVs   ','SIVs   ','SIVs   ','SIVs   ','SVs    ',      34
     &  'SVs    ','SVs    ','SVs    ','SVs    ','SVs    ','SVs    ',
     &  'SVs    ','SVs    ','SVs    ','SVs    ','SVIs   ','SVIs   ',      11
     &  'SVIs   ','SVIs   ','SVIs   ','SVIIs  ','SVIIs  ','SVIIs  ',       5
     &  'SVIIs  ','SVIIs  ','SVIIs  ','SVIIs  ','SVIIs  ','SVIIs  ',
     &  'SVIIs  ','SVIIs  ','SVIIs  ','SVIIs  ','SVIIIs ','SVIIIs ',      13
     &  'SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ',
     &  'SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ',
     &  'SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ',
     &  'SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ',
     &  'SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ',
     &  'SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ',
     &  'SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ',
     &  'SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ',
     &  'SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ',
     &  'SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SVIIIs ','SIXs   ',      61
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ',
     &  'SIXs   ','SIXs   ','SIXs   ','SIXs   ','SIXs   ','SXs    ',      72
     &  'SXs    ','SXs    ','SXs    ','SXs    ','SXs    ','SXs    ',
     &  'SXs    ','SXs    ','SXs    ','SXs    ','SXs    ','SXs    ',
     &  'SXs    ','SXs    ','SXs    ','SXs    ','SXs    ','SXs    ',
     &  'SXs    ','SXs    ','SXs    ','SXs    ','SXs    ','SXs    ',
     &  'SXs    ','SXs    ','SXs    ','SXIs   ','SXIs   ','SXIs   ',      28
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ','SXIs   ',
     &  'SXIs   ','SXIs   ','SXIs   ','SXIIs  ','SXIIs  ','SXIIs  ',      78
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ',
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ',
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ',
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ',
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ',
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ',
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ',
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ',
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ',
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ',
     &  'SXIIs  ','SXIIs  ','SXIIs  ','SXIIs  ','SXIIIs ','SXIIIs ',      67
     &  'SXIIIs ','SXIIIs ','SXIIIs ','SXIIIs ','SXIIIs ','SXIIIs ',
     &  'SXIIIs ','SXIIIs ','SXIIIs ','SXIIIs ','SXIIIs ','SXIIIs ',    XIII 15
     &  'SXIIIs ','SXIVs  ','SXIVs  ','SXIVs  ','SXIVs  ','SXIVs  '/!XIV 5 2709
         data (nomrai(k),k=2710,4141)
     & /'SXVs   ','SXVs   ','SXVs   ','SXVs   ','SXVs   ','SXVs   ',    XV 6
     &  'SXVIs  ','SXVIs  ','SXVIs  ','SXVIs  ','SXVIs  ','FeIs   ',    XVI 5
     &  'FeIs   ','FeIs   ','FeIs   ','FeIs   ','FeIs   ','FeIs   ',
     &  'FeIs   ','FeIs   ','FeIIs  ','FeIIs  ','FeIIs  ','FeIIs  ',     I  9
     &  'FeIIs  ','FeIIs  ','FeIIs  ','FeIIIs ','FeIIIs ','FeIIIs ',     II 7
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ',
     &  'FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIIIs ','FeIVs  ',      74
     &  'FeIVs  ','FeIVs  ','FeIVs  ','FeIVs  ','FeIVs  ','FeIVs  ',
     &  'FeIVs  ','FeIVs  ','FeIVs  ','FeIVs  ','FeIVs  ','FeIVs  ',
     &  'FeIVs  ','FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ',      14
     &  'FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ',
     &  'FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ',
     &  'FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ',
     &  'FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ',
     &  'FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ',
     &  'FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ',
     &  'FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ',
     &  'FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ',
     &  'FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVs   ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ','FeVIs  ',
     &  'FeVIs  ','FeVIs  ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ',
     &  'FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ',
     &  'FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ',
     &  'FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ',
     &  'FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ',
     &  'FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ',
     &  'FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ',
     &  'FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ',
     &  'FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ','FeVIIs ',
     &  'FeVIIs ','FeVIIs ','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs',
     &  'FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs',
     &  'FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs',
     &  'FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs',
     &  'FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs',
     &  'FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs','FeVIIIs',
     &  'FeVIIIs','FeVIIIs','FeVIIIs','FeIXs  ','FeIXs  ','FeIXs  ',
     &  'FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ',
     &  'FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ',
     &  'FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ',
     &  'FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ',
     &  'FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ','FeIXs  ',
     &  'FeIXs  ','FeIXs  ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',
     &  'FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ','FeXs   ',     3231
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ',
     &  'FeXIs  ','FeXIs  ','FeXIs  ','FeXIs  ','FeXIIs ','FeXIIs ',
     &  'FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ',
     &  'FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ',
     &  'FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ',
     &  'FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ',
     &  'FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ',
     &  'FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ',
     &  'FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIs ','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIII ','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs','FeXIIIs',
     &  'FeXIIIs','FeXIIIs','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ','FeXIVs ',
     &  'FeXIVs ','FeXIVs ','FeXVs  ','FeXVs  ','FeXVs  ','FeXVs  ',
     &  'FeXVs  ','FeXVs  ','FeXVs  ','FeXVs  ','FeXVs  ','FeXVs  ',
     &  'FeXVs  ','FeXVs  ','FeXVs  ','FeXVs  ','FeXVs  ','FeXVs  ',
     &  'FeXVs  ','FeXVIs ','FeXVIs ','FeXVIs ','FeXVIs ','FeXVIs ',
     &  'FeXVIIs','FeXVIIs','FeXVIIs','FeXVIIs','FeXVIIs','FeXVIIs',
     &  'FeXVIIs','FeXVIIs','FeXVIIs','FeXVIIs','FeXVIIs','FeXVIIs',
     &  'FeXVIIs','FeXVIIs','FeXVIIs','FeXVIIs','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII','FeXVIII',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ',
     &  'FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXIXs ','FeXXs  ',
     &  'FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ',
     &  'FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ',
     &  'FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ',
     &  'FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ',
     &  'FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ',
     &  'FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXs  ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ','FeXXIs ',
     &  'FeXXIs ','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs','FeXXIIs',
     &  'FeXXIIs','FeXXIII','FeXXIII','FeXXIII','FeXXIII','FeXXIII',
     &  'FeXXIII','FeXXIII','FeXXIII','FeXXIII','FeXXIII','FeXXIII',
     &  'FeXXIII','FeXXIII','FeXXIII','FeXXIII','FeXXIII','FeXXIII',
     &  'FeXXIVs','FeXXIVs','FeXXIVs','FeXXIVs','FeXXIVs','FeXXVs ',
     &  'FeXXVs ','FeXXVs ','FeXXVs ','FeXXVs ','FeXXVs ','FeXXVIs',
     &  'FeXXVIs','FeXXVIs','FeXXVIs','FeXXVI'/                           4141
       end
c==============================================================================
c234567890123456789012345678901234567890123456789012345678901234567890123456789
c        1         2         3         4         5         6         7         
