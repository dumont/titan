TITAN is a computer program for calculating the interactions of a dilute medium with electromagnetic radiation. It includes all atomic processes: absorption, excitation, deexcitation of atoms and ions, heating and cooling of the gas, and of radiation transfer : transmission, reflection and diffusion, in order to obtain the spectra reemitted by the medium. 
It handles plan parallel slabs in non LTE steady state, for various physical conditions and various illuminations, valid in many astrophysical situations. It is specifically designed for warm-hot and thick media emitting and absorbing in the X-ray range. It computes the physical parameters, ionisation degrees, temperature, density, and the spectrum of the radiated light in each point of the slab, by solving simultaneously the ionisation equations, the equations of statistical equilibrium, the thermal equations, eventually the pressure equilibrium equations, and the radiation transfer, using iteration processes (cf.  Dumont et al. 2000, 2003).

The most important features are :
- the radiation transfer equation is solved for the continuum and for the lines (without resorting to escape probabilities), using a competitive method called “accelerated lambda iteration” (ALI, cf. Paletou 1995), which assures an accurate result for the transfer inside thick clouds, at the expense of a long computation time;
- the radiation transfer equation is solved for several directions, thus permitting various direction illuminations;
- the whole specific intensity is treated, without distinction between diffuse radiation and incident absorbed radiation.
- it solves the full matrix statistical equations, taking account recombinations onto the excited levels
- geometry is plane-parallel.

Titan has two modes :
- a mode "given density" (GD), i.e. a constant density, or a density varying as a power law, or any density distribution given in a file, see the user guide "Given Density"
- and a mode "constant pressure" (CP), i.e. a constant gaseous pressure, or a constant total pressure, please carefully see the user guide "Constant Pressure" Titan does not include molecules, so it cannot compute thermal equilibrium if the temperature decreases beyond about 8 000K; owing to this, Titan cannot compute very thick models, in particular in the case of constant pressure models; it is strongly advisable to begin by a model with a moderate column-density, and gradually increase the thickness. If one don't take care, one can obtain abnormal results.

Titan has two possibilities for the input file :
- an easy input file for "given density" mode (with keywords), see the guide "Given Density"
(to run with  ./executable  <your_input_file  >log_file )
- OR a file named  TICPinput.dat  for "constant pressure" mode (and "given density" mode also), see the guide "Constant Pressure", the z-grid must be carefully chosen.
(to run with  ./executable  >log_file )

In some cases of given density in very thick clouds, one can encounter difficulties to get convergence of the models : in these cases it is necessary to refine the grid specifications in a file  TICPinput.dat , and so use the second mode of Titan, see the guide "Constant Pressure".

Available options :
-	various incident spectral distributions;
-	various directions of the incident flux, or an isotropic flux;
-	the possibility to set an incident flux on the two faces of the slab (one being black body);
-	a constant density, or a density varying as a power law, or any density distribution given in a file;
-	a constant gaseous pressure, or a constant total pressure
-	computation of the temperature, or a constant temperature;
-	micro-turbulence velocity;
-	the choice of the line resolution in the spectra output files, to obtain a graph.

Contact emails: Agata Rozanska <agata@camk.edu.pl> or Anne-Marie Dumont <annemdumont@wanadoo.fr>

January 2019
